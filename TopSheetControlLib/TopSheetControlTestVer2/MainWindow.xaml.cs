﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TopSheetControlTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        TopSheetControlLib.TopSheetMain _topSheetMain;
        public static string connectionString = "Provider=SQLOLEDB.1;Server=localhost;Database=UniMail_Milano;Uid=sa;Pwd=UniMail2017";
        //public static string connectionString = "Provider=SQLOLEDB.1;Server=localhost;Database=UniMail_Milano_Karlstad;Uid=sa;Pwd=DinCare2018";
        //public static string connectionStringLabelDesign = "Provider=SQLOLEDB.1;Server=localhost;Database=UniMail_Milano_Karlstad;Uid=sa;Pwd=DinCare2018";


        public MainWindow()
        {
            InitializeComponent();
            this.Title = "TopSheet control test project";
            this.Closed += MainWindow_Closed;
        }

        void MainWindow_Closed(object sender, EventArgs e)
        {
            try
            {
                if (_topSheetMain != null)
                {
                    _topSheetMain.Stop();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        public void PrintTestTopsheet(string ioIpTopSheet, 
            int ioPortTopSheet, string printerName1, string printerName2,
            string connectionString,int printerNumber)
        {
            try
            {
                if (_topSheetMain == null)
                {
                    TopSheetControlLib.TopSheetMain.IcpSlotTypes[] slotTypes =
                        new TopSheetControlLib.TopSheetMain.IcpSlotTypes[]
                        {
                            TopSheetControlLib.TopSheetMain.IcpSlotTypes.TSLOT_DI16,
                            TopSheetControlLib.TopSheetMain.IcpSlotTypes.TSLOT_DI16,
                            TopSheetControlLib.TopSheetMain.IcpSlotTypes.TSLOT_DI16,
                            TopSheetControlLib.TopSheetMain.IcpSlotTypes.TSLOT_DO16,
                            TopSheetControlLib.TopSheetMain.IcpSlotTypes.TSLOT_DO16
                        };

                    _topSheetMain = new TopSheetControlLib.TopSheetMain(connectionString,
                         1,
                        ioIpTopSheet, ioPortTopSheet,
                        true, printerName1, printerName2, "10.110.0.41", "10.110.0.42", 9100, 9100, 0,
                        slotTypes
                        );
                }
                _topSheetMain.TestPrint(printerNumber);
            }
            catch
            {

            }
        }

        private void uxStartButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_topSheetMain == null)
                {
                    string ioIpTopSheet = "10.110.0.44";
                    int ioPortTopSheet = 10000;
                    string printerName1 = "ECOSYS P3060dn C1";
                    string printerName2 = "ECOSYS P3060dn C2";
                    //string connectionString = "Provider=SQLOLEDB.1;Server=localhost;Database=UniMail_Milano_Karlstad;Uid=sa;Pwd=DinCare2018";
                    //string connectionStringLabelDesign = "Provider=SQLOLEDB.1;Server=localhost;Database=UniMail_Milano_Karlstad;Uid=sa;Pwd=DinCare2018";

                    TopSheetControlLib.TopSheetMain.IcpSlotTypes[] slotTypes =
                        new TopSheetControlLib.TopSheetMain.IcpSlotTypes[]
                        {
                            TopSheetControlLib.TopSheetMain.IcpSlotTypes.TSLOT_DI16,
                            TopSheetControlLib.TopSheetMain.IcpSlotTypes.TSLOT_DI16,
                            TopSheetControlLib.TopSheetMain.IcpSlotTypes.TSLOT_DI16,
                            TopSheetControlLib.TopSheetMain.IcpSlotTypes.TSLOT_DO16,
                            TopSheetControlLib.TopSheetMain.IcpSlotTypes.TSLOT_DO16
                        };

                    //string connectionString = TopSheetControlLib.DBTools.GetConnectionStringSqlServer(
                    //    "unimail.sudouest.com", "UniMail_Packing", false, "sa", "Lubbus83");
                    //_topSheetMain = new TopSheetControlLib.TopSheetMain(connectionString, 1);
                    _topSheetMain = new TopSheetControlLib.TopSheetMain(connectionString,
                         1,
                        ioIpTopSheet, ioPortTopSheet,
                        true, printerName1, printerName2, "10.110.0.41", "10.110.0.42", 9100, 9100, 0,
                        slotTypes
                        );
                }

                _topSheetMain.Start(1, 1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void uxStopButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_topSheetMain != null)
                {
                    _topSheetMain.Stop();
                    _topSheetMain = null;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void uxTest_Click(object sender, RoutedEventArgs e)
        {

            PrintTestTopsheet("10.110.0.44",
            10000, "ECOSYS P3060dn C1", "ECOSYS P3060dn C2",
            connectionString
            , 1);
            
        }

        private void uxTest2_Click(object sender, RoutedEventArgs e)
        {
            PrintTestTopsheet("10.110.0.44",
            10000, "ECOSYS P3060dn C1", "ECOSYS P3060dn C2",
            connectionString
            , 2);
           
        }
    }

   
}
