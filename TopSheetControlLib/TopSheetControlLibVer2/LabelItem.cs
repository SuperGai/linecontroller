﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using ZXing;

namespace TopSheetControlLib
{
    public class LabelItem
    {
        public static String connsql = 
            "Data Source="+ TopSheetMain._connectionString_cheng[0] + ";" +
            "Initial Catalog=" + TopSheetMain._connectionString_cheng[1] +";" +
            "Integrated Security=False;" +
            "User ID=" + TopSheetMain._connectionString_cheng[2] +";" +
            "Password=" + TopSheetMain._connectionString_cheng[3] + "";
        //public const String connsql = "Data Source=localhost;Initial Catalog=UniMail_Milano_Karlstad;Integrated Security=False;User ID=sa;Password=DinCare2018";

        public enum ResultTypes
        {
            Ok,
            Unknown,
            DataError,
            ConnectionError,
            PrintingTimeout
        };

        public enum printerResultTypes
        {
            Ok = 0,
            Unknown = 1,
            NoDataConnection = 2,
            ConnectionTypeNotSupported = 3,
            ConnectionError = 4,
            DataError = 5,
            NotEditMode = 6,
            LabelNotSelected = 7,
            LabelNotFound = 8,
            LabelLocked = 9,
            LabelAlreadyExists = 10,
            DrawDesignError = 11,
            CircularParentLabelReference = 12,
            PrintingTimeout = 13,
            IncorrectParameters = 14,
            UserAborted = 15,
            STDNoTop = 16,
            InvalidBundleId = 17
        };

        //private string _LabelGroup;
        //private string _LabelName;

        //public string LabelGroup
        //{
        //    get
        //    {
        //        return _LabelGroup;
        //    }
        //    set
        //    {
        //        _LabelGroup = value;
        //    }
        //}

        //public string LabelName
        //{
        //    get
        //    {
        //        return _LabelName;
        //    }
        //    set
        //    {
        //        _LabelName = value;
        //    }
        //}
        public static ResultTypes GetLabel( string labelGroup, string labelName, out Dictionary<string, List<string>> labelItem)
        {

            DataTable GetLableShow = new DataTable();

            labelItem = new Dictionary<string, List<string>>();

            SqlConnection conn = new SqlConnection(connsql);
            try
            {
                conn.Open();
                string GetLastVersion = "select top 1 Version from _unifront_LabelFieldDesign where LabelName='" + labelName + "' AND LabelGroup = '" + labelGroup + "' order by Version desc";

                SqlCommand cmd = new SqlCommand(GetLastVersion, conn);
                string LastVersion = cmd.ExecuteScalar().ToString();
                string GetLabelInfo = "select * from _unifront_LabelFieldDesign where LabelName='" + labelName + "' AND LabelGroup = '" + labelGroup + "'";

                SqlDataAdapter da = new SqlDataAdapter(GetLabelInfo, conn);
                da.Fill(GetLableShow);

                foreach (DataRow LabelList in GetLableShow.Rows)
                {
                    List<string> LabelValue = new List<string>();

                    for (int i = 0; i <= 19; i++)
                    {
                        if (LabelList[i] != null)
                        {
                            LabelValue.Add(LabelList[i].ToString());
                        }
                    }
                    //if (labelItem.ContainsKey(LabelList[5].ToString()) != true)
                    //{
                    //    if (LabelList[5].ToString() == "")
                    //    {
                    //        labelItem.Add(LabelList[4].ToString(), LabelValue);
                    //    }
                    //    else
                    //    {
                    //        labelItem.Add(LabelList[5].ToString(), LabelValue);
                    //    }

                    //}

                    if (labelItem.ContainsKey(LabelList[3].ToString()) != true)
                    {
                        labelItem.Add(LabelList[3].ToString(), LabelValue);
                    }

                }
                return ResultTypes.Ok;
            }
            catch
            {
                return ResultTypes.DataError;
            }
        }

        public static void DrawString(Graphics g, string key, Dictionary<string, List<string>> labelItem)
        {
            string DrawString = labelItem[key][4];
            Font StringFont = new Font(labelItem[key][7], int.Parse(labelItem[key][8]) - 5);
            SolidBrush StringBrush = new SolidBrush(Color.Black);
            float x = float.Parse(labelItem[key][16]);
            float y = float.Parse(labelItem[key][18]);
            StringFormat StringFormat = new StringFormat();
            g.DrawString(DrawString, StringFont, StringBrush, x, y, StringFormat);
        }

        public static ResultTypes DrawLabel(Graphics g, DataTable LabelShow, Dictionary<string, List<string>> labelItem, TopSheetMain.TopSheetTypes topSheetTypes)
        {
            //string drawString = "Cheng Test";
            //System.Drawing.Font drawFont = new System.Drawing.Font("Arial", 16);
            //System.Drawing.SolidBrush drawBrush = new System.Drawing.SolidBrush(System.Drawing.Color.Black);
            //float x = 250.0F;
            //float y = 50.0F;
            //System.Drawing.StringFormat drawFormat = new System.Drawing.StringFormat();
            //g.DrawString(drawString, drawFont, drawBrush, x, y, drawFormat);

            //GetLabel(LabelGroup, LabelName, out labelItem);
            try
            {
                //Get Barcode
                string CurBarcodeStr = "0";
                string drawString = "";
                foreach (DataRow LabelList in LabelShow.Rows)
                {
                    if (LabelList[2].ToString() == "DockBarcode")
                    {
                        CurBarcodeStr = LabelList[3].ToString();
                    }
                }

                foreach (string key in labelItem.Keys)
                {
                    //Draw Four Label
                    for (int i = 0; i < labelItem.Count; i++)
                    {
                        if (labelItem[key][4] != "" && labelItem[key][5] == "")
                        {
                            DrawString(g, key, labelItem);
                        }
                        else
                        {
                            if (int.Parse(labelItem[key][6]) == 0 && labelItem[key][4].ToString() == "0")//FieldType is 0, Barcode's type is 270
                            {
                                foreach (DataRow LabelList in LabelShow.Rows)
                                {
                                    if (LabelList[2].ToString() == labelItem[key][5].ToString())
                                    {
                                        if (LabelList[2].ToString() == "isDoubleTopsheet" && topSheetTypes == TopSheetMain.TopSheetTypes.DOUBLETOPSHEET)
                                        {
                                            drawString = "2";
                                        }
                                        else
                                        {
                                            drawString = LabelList[3].ToString();
                                        }

                                        Font drawFont = new Font(labelItem[key][7], int.Parse(labelItem[key][8]) - 5);
                                        SolidBrush drawBrush = new SolidBrush(Color.Black);

                                        float x = float.Parse(labelItem[key][16]);
                                        float y = float.Parse(labelItem[key][18]);

                                        StringFormat drawFormat = new StringFormat();
                                        g.DrawString(drawString, drawFont, drawBrush, x, y, drawFormat);
                                    }
                                }
                            }
                            else if (int.Parse(labelItem[key][6]) == 274)
                            {
                                var content = CurBarcodeStr; //LabelList[3].ToString();
                                var writer = new BarcodeWriter
                                {
                                    Format = BarcodeFormat.ITF
                                };

                                writer.Options = new ZXing.Common.EncodingOptions
                                {
                                    PureBarcode = false,//Set the value with the barcode
                                    Width = 200,
                                    Height = 300,
                                };

                                var bitmap = writer.Write(content);
                                //Rotate the bitmap then rotated the barcode
                                bitmap.RotateFlip(RotateFlipType.Rotate270FlipNone);
                                int Bitx = int.Parse(labelItem[key][16]);
                                int Bity = int.Parse(labelItem[key][18]);

                                g.DrawImage(bitmap, Bitx, Bity);
                            }
                            else if(int.Parse(labelItem[key][6]) == 10)
                            {
                                // Draw rectangle representing size of string.
                                g.DrawRectangle(new Pen(Color.Black, 1), float.Parse(labelItem[key][16]), float.Parse(labelItem[key][18]), float.Parse(labelItem[key][19]), float.Parse(labelItem[key][15]));
                            }
                        }
                    }
                }
            }
            catch
            {
            }
            return ResultTypes.Ok;
        }
    }
}
