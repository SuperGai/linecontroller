﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TopSheetControlLib
{
    public class DatabaseEnums
    {
        public enum ConnectionTypes
        {
            MsSql,
            Oracle,
            Excel
        }

        public enum OperationTypes
        {
            ExecuteNonQuery,
            ExecuteReader,
            ExecuteScalar,
            Fill,
            FillSchema,
            GetSchemaTable,
            Update,
            GetInsertCommand,
            GetUpdateCommand,
        }
    }
}