﻿using System;

namespace TopSheetControlLib
{
    public class LogItem
    {
        public LogLevels LogLevel { get; set; }
        public string Message { get; set; }
        public DateTime LogTime { get; set; }
    }
}
