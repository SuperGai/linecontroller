﻿using System;
using System.Text;
using System.Threading;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Net.Sockets;
using System.Collections.Generic;
using SnmpSharpNet;
using System.Net;

namespace TopSheetControlLib
{
    public class PrintronixPrint
    {
        public enum ResultTypes
        {
            Ok = 0,
            Unknown = 1,
            NoDataConnection = 2,
            ConnectionTypeNotSupported = 3,
            ConnectionError = 4,
            DataError = 5,
            NotEditMode = 6,
            LabelNotSelected = 7,
            LabelNotFound = 8,
            LabelLocked = 9,
            LabelAlreadyExists = 10,
            DrawDesignError = 11,
            CircularParentLabelReference = 12,
            PrintingTimeout = 13,
            IncorrectParameters = 14,
            UserAborted = 15,
            STDNoTop = 16
        }

        public LabelItem.printerResultTypes PrintResult { get; set; }
        public string LogFolder { get; set; }
        public string Ip { get; set; }
        public int Port { get; set; }
        public bool IsRun { get; set; }
        public bool CanReset { get; set; }
        public bool IsSnooper { get; set; }

        public bool IsP8000 { get; set; }
        public bool IsBusy { get; set; }
        //public bool IsReadyToPrint { get; set; }
        public bool IsConnected { get; set; }
        public DateTime LastReadyTime { get; set; }
        public DateTime LastStatusCheck { get; set; }
        private string connectionString;
        private OleDbConnection printDataConnection;
        private const int Len_Tx = 100;
        private const int Len_Rx = 100;
        public TcpClient _tcpClient;
        public NetworkStream networkStream;
        public BinaryReader binaryReader;
        public BinaryWriter binaryWriter;
        //public fixed byte TxBuffer[20];
        //public fixed byte RxBuffer[100];
        public byte[] TxBuffer;
        public byte[] RxBuffer;
        public string LogError = "";

        //public string TxTopsheetStr;//Topsheet going to print

        public string TopsheetCreateTemplate;
        public string TopsheetExecContent;
        public string CurTopsheetStr;

        public byte[] ErrorKode;
        public bool AckRecevied;
        public bool isEnabled;
        public bool ResetDone;
        public string _lastPrinterMessage;
        public int _lastPrintingStatus;

        //private bool isStatusCheck = false;
        //private Thread _StatusCheckThread = null;
        public List<string> printerStatusList;

        public PrinterStatus _printerStatus { get; set; }

        public PrintronixPrint(string _printerIp, int _printerPort, string _connectionString, string _logFolder)
        {
            LogFolder = _logFolder;
            Ip = _printerIp;
            Port = _printerPort;
            connectionString = _connectionString;
            IsSnooper = false;
            AckRecevied = false;
            _tcpClient = null;
            TxBuffer = new byte[Len_Tx];//fixed size 20 bytes
            RxBuffer = new byte[Len_Rx];//fixed size 100 bytes
            ErrorKode = new byte[4];
            IsRun = false;
            isEnabled = false;
            IsBusy = false;
            IsConnected = true;
            //IsReadyToPrint = true;
            IsP8000 = false;
            CanReset = false;
            ResetDone = false;
            _lastPrinterMessage = "";
            _lastPrintingStatus = 3;
            //isStatusCheck = true;
            _printerStatus = new PrinterStatus();
            printerStatusList = new List<string>();

            //IsP8000 = GetPrintronixModel(_printerIp);

        }

        public void StartStatusCheck()
        {//comment for further test //Jie 2018.06.13
            //try
            //{
            //    isStatusCheck = true;
            //    if (_StatusCheckThread == null)
            //    {
            //        _StatusCheckThread = new Thread(new ThreadStart(PrinterStatusCheck));
            //        _StatusCheckThread.Start();
            //        LastStatusCheck = DateTime.Now;
            //        LastReadyTime = DateTime.Now;
            //    }
            //}
            //catch
            //{
            //    Logging.AddToLog(LogLevels.Warning, "StopStatusCheck Exception");
            //}
        }
        public void StopStatusCheck()
        {//comment for further test //Jie 2018.06.13
            //try
            //{
            //    isStatusCheck = false;
            //    Thread.Sleep(100);
            //    if (_StatusCheckThread != null)// && _icpThread.ThreadState == ThreadState.Running)
            //    {
            //        _StatusCheckThread.Interrupt();
            //        _StatusCheckThread = null;
            //    }
            //}
            //catch
            //{
            //    Logging.AddToLog(LogLevels.Warning, "StopStatusCheck Exception");
            //}
        }

        //public void PrinterStatusCheck()
        //{
        //    try
        //    {
        //        while (isStatusCheck)
        //        {
        //            try
        //            {
        //                if (IsBusy == false) //&& IsConnected == false)
        //                {
        //                    LastReadyTime = DateTime.Now;
        //                    if (LastStatusCheck < DateTime.Now.AddSeconds(-15))
        //                    {
        //                        LastStatusCheck = DateTime.Now;
        //                        IsBusy = true;
        //                        DoStartConnect();
        //                        Thread.Sleep(200);
        //                        Disconnect();
        //                        Thread.Sleep(200);
        //                        IsBusy = false;
        //                    }
        //                }
        //                else
        //                { //If Printer is busy
        //                    LastStatusCheck = DateTime.Now; //When it is busy, set statusCheck time.
        //                    if (LastReadyTime < DateTime.Now.AddSeconds(-15))
        //                    {//If printer is busy for more than 10 seconds
        //                        LastReadyTime = DateTime.Now;
        //                        IsBusy = true;
        //                        DoStartConnect();
        //                        Thread.Sleep(200);
        //                        Disconnect();
        //                        Thread.Sleep(200);
        //                        IsBusy = false;
        //                    }
        //                }
        //            }
        //            catch
        //            {
        //                //IsConnected = false;
        //                Logging.AddToLog(LogLevels.Warning, "PrinterStatusCheck whileloop Exception");
        //            }
        //        }

        //        //out of loop.
        //        //_StatusCheckThread = null;
        //    }
        //    catch
        //    {
        //        Logging.AddToLog(LogLevels.Warning, "PrinterStatusCheck Exception");
        //        //_StatusCheckThread = null;
        //    }
        //} // Add it Later Cheng 20190823

        private void DoStartConnect()
        {
            try
            {
                try
                {
                    if (Ip != null && Ip.Length > 0 && Port > 0)
                    {
                        _tcpClient = new TcpClient
                        {
                            ReceiveTimeout = 2100,
                            SendTimeout = 2100
                        };
                        _tcpClient.Connect(Ip, Port);

                        networkStream = _tcpClient.Client.Connected ? _tcpClient.GetStream() : null;
                        binaryWriter = networkStream != null ? new BinaryWriter(networkStream) : null;
                        binaryReader = networkStream != null ? new BinaryReader(networkStream) : null;

                        IsConnected = true;
                    }
                }
                catch(Exception ex)
                {
                    IsConnected = false;
                    Logging.AddToLog(LogFolder, LogLevels.Warning, "DoStartConnect Exception: " + ex.ToString());
                }
                
            }
            catch //(Exception ex)
            {
            }
        }
        private void ReConnectPrintronixT5kr()
        {
            Disconnect();
            Thread.Sleep(100);
            DoStartConnect();
        }
        public void ConnectToPrintronixT5kr()
        {
            try
            {
                if (_tcpClient == null)
                {
                    DoStartConnect();
                }
                //else //reconnect
                //{
                //    ReConnectPrintronixT5kr();
                //}
            }
            catch //(Exception ex)
            {
                //StaticInfo.LogError(ex);
                //AddToLog(ex);
                //IsConnected = false;
            }
        }

        //public void SendToPrintronixT5kr(string StrPrint)
        //{
        //    try
        //    {
        //        DateTime CurTime = DateTime.Now;

        //        while (_tcpClient == null)
        //        {
        //            if (CurTime.AddSeconds(2000) >= DateTime.Now)
        //            {
        //                PrintResult = ResultTypes.ConnectionError;
        //                break;
        //            }
        //            else
        //            {
        //                Disconnect();
        //                Thread.Sleep(100);
        //                ConnectToPrintronixT5kr();
        //                Thread.Sleep(100);
        //            }
        //        }

        //        if (_tcpClient != null && networkStream != null && binaryWriter != null)
        //        {
        //            binaryWriter.Write(StrPrint);
        //            //RecvFromPrintronixT5kr();
        //        }
        //        Thread.Sleep(100);
        //    }
        //    catch //(Exception ex)
        //    {
        //        //StaticInfo.LogError(ex);
        //        PrintResult = ResultTypes.Unknown;
        //    }
        //}
        public LabelItem.printerResultTypes SendToPrintronixT5kr()
        {
            try
            {
                DateTime CurTime = DateTime.Now;
                PrintResult = LabelItem.printerResultTypes.Ok;

                while (_tcpClient == null)
                {
                    if (CurTime.AddSeconds(2000) < DateTime.Now)
                    {
                        PrintResult = LabelItem.printerResultTypes.ConnectionError;
                        break;
                    }
                    else
                    {
                        Disconnect();
                        Thread.Sleep(50);
                        ConnectToPrintronixT5kr();
                    }
                }

                if (_tcpClient != null && networkStream != null && binaryWriter != null)
                {
                    //Logging.AddToLog(LogFolder, LogLevels.Debug, TopsheetCreateTemplate + TopsheetExecContent);

                    //binaryWriter.Write(TopsheetCreateTemplate + TopsheetExecContent);

                    binaryWriter.Write(Encoding.UTF8.GetBytes(TopsheetCreateTemplate + TopsheetExecContent));
                    //binaryWriter.Write(TxTopsheetStr);
                    //RecvFromPrintronixT5kr();
                    Thread.Sleep(1000);
                    LogError = "";
                    PrintResult = LabelItem.printerResultTypes.Ok;
                }

                Thread.Sleep(1000);
            }
            catch(Exception ex)
            {
                PrintResult = LabelItem.printerResultTypes.Unknown;
                //StaticInfo.LogError(ex);
                Disconnect();
                LogError = "SendToPrintronixT5kr Error: " + ex.ToString();
                Logging.AddToLog(LogFolder, LogLevels.Error, "SendToPrintronixT5kr Error: " + ex.ToString());
                Logging.AddToLog(LogFolder, LogLevels.Error, TopsheetCreateTemplate + TopsheetExecContent);
            }

            return PrintResult;
        }

        //public string RecvFromPrintronixT5kr()
        //{
        //    try
        //    {
        //        //RxBuffer = binaryReader.ReadBytes(Len_Rx);//Read 100 bytes
        //        //byte[] mRxBuffer = binaryReader.ReadBytes(Len_Rx);//Read 100 bytes
        //        //while (mRxBuffer.Length )
        //        //if (networkStream != null && networkStream.DataAvailable)
        //        if (_tcpClient != null && _tcpClient.Client != null && _tcpClient.Client.Connected && networkStream != null && binaryReader != null)
        //        {
        //            byte[] mRxBuffer = new byte[Len_Rx];
        //            int count = 0;

        //            try
        //            {
        //                while (_tcpClient.Client != null && _tcpClient.Client.Connected)
        //                {
        //                    mRxBuffer[count] = binaryReader.ReadByte();
        //                    ++count;
        //                }
        //            }
        //            catch
        //            {
        //            }

        //            ASCIIEncoding ascii = new ASCIIEncoding();
        //            string statusString = ascii.GetString(mRxBuffer, 1, count);//Strekkode is from byte index 9 to 89.
        //            statusString = statusString.Trim('\0');

        //            return statusString;
        //        }

        //    }
        //    catch //(Exception ex)
        //    {
        //        //StaticInfo.LogError(ex);
        //        ReConnectPrintronixT5kr();
        //        return null;
        //    }

        //    return null;
        //}
        public void Start()
        {
            //TxBuffer[1] = (byte)(TxBuffer[1] & 0xDF);//remove stop
            //TxBuffer[1] = (byte)((byte)(TxBuffer[1] & 0xEF) + 0x10);
            //SendToPrintronixT5kr();

            //TxBuffer[1] = (byte)(TxBuffer[1] & 0xEF);//remove start
            //SendToPrintronixT5kr();
        }
        public void Stop()
        {
            //TxBuffer[1] = (byte)(TxBuffer[1] & 0xEF);//remove start
            //TxBuffer[1] = (byte)((byte)(TxBuffer[1] & 0xDF) + 0x20);
            //SendToPrintronixT5kr();

            //TxBuffer[1] = (byte)(TxBuffer[1] & 0xDF);//remove stop
            //SendToPrintronixT5kr();
        }
        public void Disconnect()
        {
            //IsRun = false;
            try
            {
                try
                {
                    if (binaryWriter != null)
                    {
                        binaryWriter.Close();
                        binaryWriter.Dispose();
                    }
                }
                catch { }

                try
                {
                    if (binaryReader != null)
                    {
                        binaryReader.Close();
                        binaryReader.Dispose();
                    }
                }
                catch { }

                try
                {
                    if (networkStream != null)
                    {
                        networkStream.Close();
                        networkStream.Dispose();
                    }
                }
                catch { }

                try
                {
                    if (_tcpClient != null)
                    {
                        _tcpClient.Close();
                        _tcpClient = null;
                    }
                }
                catch { }

                Logging.AddToLog(LogFolder, LogLevels.Debug, "Printronix Printer Disconnected!");
            }
            catch (Exception ex)
            { 
                Logging.AddToLog(LogFolder, LogLevels.Error, "Printronix Printer Disconnected Fature: " + ex.ToString());
            }
           

        }
        //public void EnableSnooperMode()
        //{
        //    string SnooperStr =
        //        "^CONFIG\r\n" +
        //        "SNOOP;STATUS;PAR\n" +
        //        "END\n";

        //    SendToPrintronixT5kr(SnooperStr);
        //    IsSnooper = true;
        //}

        //public void DisableSnooperMode()
        //{
        //    string SnooperStr =
        //        "^CONFIG\n" +
        //        "SNOOP;OFF\n" +
        //        "END\n";

        //    SendToPrintronixT5kr(SnooperStr);
        //    IsSnooper = false;
        //}

        //public string CheckPrinterStatus()
        //{
        //    if (!IsSnooper)
        //    {
        //        EnableSnooperMode();
        //        Thread.Sleep(500);
        //    }

        //    string StatusCheckStr =
        //            "^STATUS\n";

        //    SendToPrintronixT5kr(StatusCheckStr);
        //    Thread.Sleep(500);
        //    return RecvFromPrintronixT5kr();
        //}

        //public void PrintBundle(ushort BundleTableId)
        //{
        //    IsBusy = true;
        //    GetLabelField(BundleTableId);
        //    Thread.Sleep(400);
        //    ConnectToPrintronixT5kr();
        //    Thread.Sleep(100);
        //    SendToPrintronixT5kr();
        //    Thread.Sleep(400);
        //    Disconnect();
        //    Thread.Sleep(50);
        //    //RecvFromPrintronixT5kr();
        //    IsBusy = false;
        //}

        public LabelItem.printerResultTypes PrintBundleByBundleId(short TitleGroupId,string IssueDate, uint BundleId, bool isSTDWithoutTop, TopSheetMain.TopSheetTypes topSheetType)
        {
            try
            {
                //bool GetResult = false;
                bool isSTDBundle = false;
                IsBusy = true;
                if ((BundleId > 10000 && BundleId < 65000) || topSheetType == TopSheetMain.TopSheetTypes.WHITEPAPER)
                {
                    PrintResult = GetLabelFieldByBundleId(TitleGroupId, IssueDate, BundleId, topSheetType, out isSTDBundle);
                    if (PrintResult == LabelItem.printerResultTypes.Ok)
                    {
                        if (isSTDBundle && isSTDWithoutTop)
                        {
                            PrintResult = LabelItem.printerResultTypes.STDNoTop;
                        }
                        else
                        {
                            Thread.Sleep(400);
                            ConnectToPrintronixT5kr();
                            Thread.Sleep(50);
                            PrintResult = SendToPrintronixT5kr();
                            //Thread.Sleep(500);
                            //Disconnect();
                            //Thread.Sleep(50);
                        }
                    }
                    else
                    {
                        PrintResult = LabelItem.printerResultTypes.LabelNotFound;
                    }
                }
                else
                {
                    PrintResult = LabelItem.printerResultTypes.InvalidBundleId;
                }
                IsBusy = false;
            }
            catch(Exception ex)
            {
                Logging.AddToLog(LogFolder,LogLevels.Warning, "Exception Printronix Print Bundle: "+ BundleId .ToString() + " Exception: " + ex.ToString());
                PrintResult = LabelItem.printerResultTypes.ConnectionError;
                IsBusy = false;
            }
            Logging.AddToLog(LogFolder, LogLevels.Normal,"Printronix Print Bundle: "+ BundleId.ToString() +" Result: " + PrintResult.ToString());
            return PrintResult;
        }
        public string GetTopsheetExecContent(bool IsStandardBundle, string AFs)
        {
            string returnResult = "";
            try
            {
                if (AFs == null || AFs.Length == 0)
                {
                    AFs = "\n";
                }

                if (IsStandardBundle)
                {
                    returnResult = 
                        "^EXECUTE;DB_6\r\n" +
                        AFs +"\r\n" + 
                        "\r\n" + 
                        "^NORMAL\r\n"; //For STD
                }
                else
                {
                    returnResult = 
                        "^EXECUTE;DB_5\r\n" +
                        AFs + "\r\n" + 
                        "\r\n" + 
                        "^NORMAL\r\n"; //For RST
                }
            }
            catch(Exception ex)
            {
                Logging.AddToLog(LogFolder, LogLevels.Warning, "GetTopsheetExecContent Error: " + ex.ToString() + 
                    " - IsSTD: " + IsStandardBundle.ToString() +
                    " - AFs: " + AFs);
            }
            return returnResult;
        }

        public string GetTopsheetCreateTemplate(bool IsStandardBundle,
            string StrBarcode, string StrOrderCopies, string StrBundleId,
            string StrNumBundles, string EditionTitleGroup,string TitleName,
            string Load, string IssueDateYYYY, string TitleText, string OrderName,
            string Bnum, string Dock, string STDNo, string RestCopy,
            string GripperLineDescription, string StackerLineDescription)
        {
            string returnResult = "";
            try
            {
                if (!IsStandardBundle)
                {
                    string bundleNo = Bnum + "/" + StrNumBundles;//+ "   " + GripperLineDescription + "-" + StackerLineDescription;

                    returnResult =
                        "^CREATE;DB_5;792\r\n" +
                        "BARCODE\r\n" +
                        "I-2/5;VSCAN;X2;H32;DARK;41;101\r\n" +
                        "*" + StrBarcode + "*\r\n" +
                        "PDF\r\n" +
                        "STOP\r\n" +
                        "BARCODE\r\n" +
                        "I-2/5;VSCAN;X2;H32;DARK;2;3\r\n" +
                        "*" + StrBarcode + "*\r\n" +
                        "PDF\r\n" +
                        "STOP\r\n" +
                        "ALPHA\r\n" +
                        "2;43;1;1;*Pub Code*\r\n" +
                        "4;43;2;2;*" + EditionTitleGroup + "*\r\n" +
                        "2;55;1;1;*Product Code*\r\n" +
                        "4;55;2;2;*" + TitleName + "*\r\n" +
                        "2;75;1;1;*Edition     *\r\n" +
                        "2;97;1;1;*Route No.*\r\n" +
                        "5;97;4;4;*" + Load + "*\r\n" +
                        "2;110;1;1;*Dppt No.*\r\n" +
                        "7;43;2;1;*" + IssueDateYYYY + "*\r\n" +
                        "7;63;2;1;*" + TitleText + "*\r\n" +
                        "10;43;4;4;*" + OrderName + "*\r\n" +
                        "11;97;2;1;*Bundle No.: " + bundleNo + "*\r\n" +//+ "   " + GripperLineDescription + "-" + StackerLineDescription + "*\r\n" +
                        "49;63;2;1;*Bay    No.: " + Dock + "*\r\n" +
                        "42;5;2;1;*Total Orders       : " + StrOrderCopies + "*\r\n" +
                        "44;5;2;1;*No. of STD-Bundles : " + STDNo + "*\r\n" +
                        "46;5;2;1;*Balance Bld Copies : " + RestCopy + "*\r\n" +
                        "42;38;1;1;*Vendor      : *\r\n" +
                        "44;38;1;1;*Subscribers : *\r\n" +
                        "46;38;1;1;*Flist Smpcps: *\r\n" +
                        "42;63;1;1;*Management Adds  : *\r\n" +
                        "44;63;1;1;*Promotional Adds : *\r\n" +
                        "46;63;1;1;*Direct Outlets   : *\r\n" +
                        "51;63;1;1;*Control No. : " + StrBundleId + "*\r\n" +
                        "AF3;6;4;75;2;2\r\n" +
                        "AF4;3;5;120;4;4\r\n" +
                        "AF5;5;42;54;1;1\r\n" +
                        "AF6;5;44;54;1;1\r\n" +
                        "AF7;5;46;54;1;1\r\n" +
                        "AF8;5;42;81;1;1\r\n" +
                        "AF9;5;44;81;1;1\r\n" +
                        "AF10;5;46;81;1;1\r\n" +
                        "AF11;100;22;5;1;1\r\n" +
                        "AF12;100;24;5;1;1\r\n" +
                        "AF13;100;26;5;1;1\r\n" +
                        "AF14;100;28;5;1;1\r\n" +
                        "AF15;100;30;5;1;1\r\n" +
                        "AF16;100;32;5;1;1\r\n" +
                        "AF17;100;34;5;1;1\r\n" +
                        "AF18;100;36;5;1;1\r\n" +
                        "AF19;100;38;5;1;1\r\n" +
                        //"AF20;100;40;5;1;1\r\n" +
                        "AF20;20;15;43;4;4\r\n" +
                        "STOP\r\n" +
                        "END\r\n";
                }
                else
                {
                    returnResult =
                        "^CREATE;DB_6;792\r\n" +
                        "BARCODE\r\n" +
                        "I-2/5;VSCAN;X2;H35;DARK;41;101\r\n" +
                        "*" + StrBarcode + "*\r\n" +
                        "PDF\r\n" +
                        "STOP\r\n" +
                        "BARCODE\r\n" +
                        "I-2/5;VSCAN;X2;H35;DARK;2;3\r\n" +
                        "*" + StrBarcode + "*\r\n" +
                        "PDF\r\n" +
                        "STOP\r\n" +
                        "ALPHA\r\n" +
                        "2;43;1;1;*Pub Code*\r\n" +
                        "4;43;2;2;*" + TitleName + "*\r\n" +
                        "2;55;1;1;*Product Code*\r\n" +
                        "4;55;2;2;*" + TitleName + "*\r\n" +
                        "2;75;1;1;*Edition     *\r\n" +
                        "2;97;1;1;*Route No.*\r\n" +
                        "5;97;4;4;*" + Load + "*\r\n" +
                        "7;43;2;1;*" + IssueDateYYYY + "*\r\n" +
                        "7;63;2;1;*" + TitleText + "*\r\n" +
                        "10;43;4;4;*" + OrderName + "*\r\n" +
                        "11;97;2;1;*Bundle No. :  " + Bnum + "/" + StrNumBundles + "*\r\n" +
                        "52;63;2;1;*Bay    No. :  " + Dock + "*\r\n" +
                        "54;63;1;1;*Control No. : " + StrBundleId + "*\r\n" +
                        "52;38;1;1;*UTR         : " + GripperLineDescription + " *\r\n" +
                        "54;38;1;1;*ABL Group   : " + StackerLineDescription + " *\r\n" +
                        "AF3;6;4;75;2;2\r\n" +
                        "STOP\r\n" +
                        "END\r\n";
                }
            }
            catch(Exception ex)
            {
                Logging.AddToLog(LogFolder, LogLevels.Warning, "GetTopsheetCreateTemplate Error: " + ex.ToString());
            }

            return returnResult;
            
        }

        public string GetTopsheetCreateP7000Template(bool IsStandardBundle,
            string StrBarcode, string StrOrderCopies, string StrBundleId,
            string StrNumBundles, string EditionTitleGroup, string TitleName,
            string Load, string IssueDateYYYY, string TitleText, string OrderName,
            string Bnum, string Dock, string STDNo, string RestCopy,
            string GripperLineDescription, string StackerLineDescription)
        {
            string returnResult = "";
            try
            {
                if (!IsStandardBundle)
                {
                    string bundleNo = Bnum + "/" + StrNumBundles;//+ "   " + GripperLineDescription + "-" + StackerLineDescription;

                    returnResult =
                        "^CREATE;DB_5;792\r\n" +
                        "BARCODE\r\n" +
                        "I-2/5;VSCAN;X2;H32;DARK;48;101\r\n" +
                        "*" + StrBarcode + "*\r\n" +
                        "PDF\r\n" +
                        "STOP\r\n" +
                        "BARCODE\r\n" +
                        "I-2/5;VSCAN;X2;H32;DARK;9;10\r\n" +
                        "*" + StrBarcode + "*\r\n" +
                        "PDF\r\n" +
                        "STOP\r\n" +
                        "ALPHA\r\n" +
                        "9;50;1;1;*Pub Code*\r\n" +
                        "11;50;2;2;*" + EditionTitleGroup + "*\r\n" +
                        "9;62;1;1;*Product Code*\r\n" +
                        "11;62;2;2;*" + TitleName + "*\r\n" +
                        "9;82;1;1;*Edition     *\r\n" +
                        "9;104;1;1;*Route No.*\r\n" +
                        "12;104;4;4;*" + Load + "*\r\n" +
                        "9;117;1;1;*Dppt No.*\r\n" +
                        "14;50;2;1;*" + IssueDateYYYY + "*\r\n" +
                        "14;70;2;1;*" + TitleText + "*\r\n" +
                        "17;50;4;4;*" + OrderName + "*\r\n" +
                        "18;104;2;1;*Bundle No.: " + bundleNo + "*\r\n" +//+ "   " + GripperLineDescription + "-" + StackerLineDescription + "*\r\n" +
                        "56;70;2;1;*Bay    No.: " + Dock + "*\r\n" +
                        "49;12;2;1;*Total Orders       : " + StrOrderCopies + "*\r\n" +
                        "51;12;2;1;*No. of STD-Bundles : " + STDNo + "*\r\n" +
                        "53;12;2;1;*Balance Bld Copies : " + RestCopy + "*\r\n" +
                        "49;45;1;1;*Vendor      : *\r\n" +
                        "51;45;1;1;*Subscribers : *\r\n" +
                        "53;45;1;1;*Flist Smpcps: *\r\n" +
                        "49;70;1;1;*Management Adds  : *\r\n" +
                        "51;70;1;1;*Promotional Adds : *\r\n" +
                        "53;70;1;1;*Direct Outlets   : *\r\n" +
                        "58;70;1;1;*Control No. : " + StrBundleId + "*\r\n" +
                        "AF3;6;11;82;2;2\r\n" +
                        "AF4;3;12;120;4;4\r\n" +
                        "AF5;5;49;61;1;1\r\n" +
                        "AF6;5;51;61;1;1\r\n" +
                        "AF7;5;53;61;1;1\r\n" +
                        "AF8;5;49;88;1;1\r\n" +
                        "AF9;5;51;88;1;1\r\n" +
                        "AF10;5;53;88;1;1\r\n" +
                        "AF11;100;29;12;1;1\r\n" +
                        "AF12;100;31;12;1;1\r\n" +
                        "AF13;100;33;12;1;1\r\n" +
                        "AF14;100;35;12;1;1\r\n" +
                        "AF15;100;37;12;1;1\r\n" +
                        "AF16;100;39;12;1;1\r\n" +
                        "AF17;100;41;12;1;1\r\n" +
                        "AF18;100;43;12;1;1\r\n" +
                        "AF19;100;45;12;1;1\r\n" +
                        //"AF20;100;47;12;1;1\r\n" +
                        "AF20;20;22;50;4;4\r\n" +
                        "STOP\r\n" +
                        "END\r\n";
                }
                else
                {
                    returnResult =
                        "^CREATE;DB_6;792\r\n" +
                        "BARCODE\r\n" +
                        "I-2/5;VSCAN;X2;H35;DARK;48;101\r\n" +
                        "*" + StrBarcode + "*\r\n" +
                        "PDF\r\n" +
                        "STOP\r\n" +
                        "BARCODE\r\n" +
                        "I-2/5;VSCAN;X2;H35;DARK;9;10\r\n" +
                        "*" + StrBarcode + "*\r\n" +
                        "PDF\r\n" +
                        "STOP\r\n" +
                        "ALPHA\r\n" +
                        "9;50;1;1;*Pub Code*\r\n" +
                        "11;50;2;2;*" + TitleName + "*\r\n" +
                        "9;62;1;1;*Product Code*\r\n" +
                        "11;62;2;2;*" + TitleName + "*\r\n" +
                        "9;82;1;1;*Edition     *\r\n" +
                        "9;104;1;1;*Route No.*\r\n" +
                        "12;104;4;4;*" + Load + "*\r\n" +
                        "14;50;2;1;*" + IssueDateYYYY + "*\r\n" +
                        "14;70;2;1;*" + TitleText + "*\r\n" +
                        "17;50;4;4;*" + OrderName + "*\r\n" +
                        "18;104;2;1;*Bundle No. :  " + Bnum + "/" + StrNumBundles + "*\r\n" +
                        "59;70;2;1;*Bay    No. :  " + Dock + "*\r\n" +
                        "61;70;1;1;*Control No. : " + StrBundleId + "*\r\n" +
                        "59;45;1;1;*UTR         : " + GripperLineDescription + " *\r\n" +
                        "61;45;1;1;*ABL Group   : " + StackerLineDescription + " *\r\n" +
                        "AF3;6;11;82;2;2\r\n" +
                        "STOP\r\n" +
                        "END\r\n";
                }
            }
            catch (Exception ex)
            {
                Logging.AddToLog(LogFolder, LogLevels.Warning, "GetTopsheetCreateTemplate Error: " + ex.ToString());
            }

            return returnResult;

        }

        public void GetTopSheetStringsByDataTable(DataTable CurDataTable, out bool IsSTD)
        {
            string StrBarcode = "";
            string StrOrderCopies = "";
            string GripperLineName = "";
            string StackerLineDescription = "";
            string StrBundleId = "";
            string StrNumBundles = "";
            string TitleName = "";
            string Load = "";
            string IssueDateYYYY = "";
            string TitleText = "";
            string EditionTitleGroup = "";
            string OrderName = "";
            string Bnum = "";
            string Dock = "";
            string STDNo = "";
            string RestCopy = "";
            string AFs = "";
            //bool IsFirstBundleInRoute = false;
            IsSTD = false;

            try
            {
                foreach (DataRow row in CurDataTable.Rows)
                {
                    // "FieldName", "FieldValue"
                    switch (row["FieldName"].ToString())
                    {
                        case "IsSTD":
                            if (row["FieldValue"].ToString() == "1")
                            {
                                IsSTD = true;
                            }
                            else
                            {
                                IsSTD = false;
                            }
                            break;

                        case "DockBarcode":
                            StrBarcode = row["FieldValue"].ToString();
                            break;

                        case "Ocp":
                            StrOrderCopies = row["FieldValue"].ToString();
                            break;

                        case "Bid":
                            StrBundleId = row["FieldValue"].ToString();
                            break;

                        case "TitleName":
                            TitleName = row["FieldValue"].ToString();
                            break;

                        case "Load":
                            Load = row["FieldValue"].ToString();
                            break;

                        case "TitleGroup":
                            EditionTitleGroup = row["FieldValue"].ToString();
                            break;

                        case "IssueDateYYYY":
                            IssueDateYYYY = row["FieldValue"].ToString();
                            break;

                        case "TitleText":
                            TitleText = row["FieldValue"].ToString();
                            break;

                        case "OrderName":
                            OrderName = row["FieldValue"].ToString();
                            break;

                        case "Dock":
                            Dock = row["FieldValue"].ToString();
                            break;

                        case "STDNo":
                            STDNo = row["FieldValue"].ToString();
                            break;

                        case "Bnum":
                            Bnum = row["FieldValue"].ToString();
                            break;

                        case "RestCopy":
                            RestCopy = row["FieldValue"].ToString();
                            break;

                        case "Lct": //09
                            StrNumBundles = row["FieldValue"].ToString();
                            break;

                        case "LineId":
                            GripperLineName = row["FieldValue"].ToString();
                            break;

                        case "ABLId":
                            StackerLineDescription = row["FieldValue"].ToString();
                            break;

                        case "3":
                            AFs += GetAFString(row["FieldValue"].ToString(), "3", 6, IsSTD);
                            break;

                        case "4":
                            AFs += GetAFString(row["FieldValue"].ToString(), "4", 3, IsSTD);
                            break;

                        case "5":
                            AFs += GetAFString(row["FieldValue"].ToString(), "5", 5, IsSTD);
                            break;

                        case "6":
                            AFs += GetAFString(row["FieldValue"].ToString(), "6", 5, IsSTD);
                            break;

                        case "7":
                            AFs += GetAFString(row["FieldValue"].ToString(), "7", 5, IsSTD);
                            break;

                        case "8":
                            AFs += GetAFString(row["FieldValue"].ToString(), "8", 5, IsSTD);
                            break;

                        case "9":
                            AFs += GetAFString(row["FieldValue"].ToString(), "9", 5, IsSTD);
                            break;

                        case "10":
                            AFs += GetAFString(row["FieldValue"].ToString(), "10", 5, IsSTD);
                            break;

                        case "11":
                            AFs += GetAFString(row["FieldValue"].ToString(), "11", 100, IsSTD);
                            break;

                        case "12":
                            AFs += GetAFString(row["FieldValue"].ToString(), "12", 100, IsSTD);
                            break;

                        case "13":
                            AFs += GetAFString(row["FieldValue"].ToString(), "13", 100, IsSTD);
                            break;

                        case "14":
                            AFs += GetAFString(row["FieldValue"].ToString(), "14", 100, IsSTD);
                            break;

                        case "15":
                            AFs += GetAFString(row["FieldValue"].ToString(), "15", 100, IsSTD);
                            break;

                        case "16":
                            AFs += GetAFString(row["FieldValue"].ToString(), "16", 100, IsSTD);
                            break;

                        case "17":
                            AFs += GetAFString(row["FieldValue"].ToString(), "17", 100, IsSTD);
                            break;

                        case "18":
                            AFs += GetAFString(row["FieldValue"].ToString(), "18", 100, IsSTD);
                            break;

                        case "19":
                            AFs += GetAFString(row["FieldValue"].ToString(), "19", 100, IsSTD);
                            break;

                        case "20":
                            AFs += GetAFString(row["FieldValue"].ToString(), "20", 20, IsSTD);
                            break;
                    }
                }

                if(IsP8000)
                {
                    TopsheetCreateTemplate = GetTopsheetCreateTemplate(IsSTD, StrBarcode,
                    StrOrderCopies, StrBundleId, StrNumBundles, EditionTitleGroup,
                    TitleName, Load, IssueDateYYYY, TitleText, OrderName, Bnum, Dock, STDNo, RestCopy,
                    GripperLineName, StackerLineDescription);
                }
                else
                {
                    TopsheetCreateTemplate = GetTopsheetCreateP7000Template(IsSTD, StrBarcode,
                   StrOrderCopies, StrBundleId, StrNumBundles, EditionTitleGroup,
                   TitleName, Load, IssueDateYYYY, TitleText, OrderName, Bnum, Dock, STDNo, RestCopy,
                   GripperLineName, StackerLineDescription);
                }
                

                TopsheetExecContent = GetTopsheetExecContent(IsSTD, AFs);
            }
            catch (Exception ex)
            {
                Logging.AddToLog(LogFolder, LogLevels.Warning, "GetTopSheetStringsByDataTable Error: " + ex.ToString());
            }
        }

        //public bool GetLabelFieldByBundleId(ushort TitleGroupId,string IssueDate, uint BundleId, TopSheetMain.TopSheetTypes topSheetType, out bool isSTDBundle)
        //{
        //    bool GetResult = false;
        //    isSTDBundle = false;
        //    try
        //    {
        //        if (printDataConnection == null || printDataConnection.State != ConnectionState.Open)
        //        {
        //            try
        //            {
        //                printDataConnection = new OleDbConnection(connectionString);
        //                printDataConnection.Open();
        //            }
        //            catch
        //            {
        //                printDataConnection = null;
        //                GetResult = false;
        //                return GetResult;
        //            }
        //        }

        //        if (printDataConnection != null && printDataConnection.State == ConnectionState.Open)
        //        {
        //            //Dictionary<string, string> dictionary = new Dictionary<string, string>();
        //            try
        //            {
        //                DataTable labelData = null;

        //                if (topSheetType == TopSheetMain.TopSheetTypes.NORMAL)
        //                {
        //                    string sql = string.Format("select * from UniStack_GetLabelData({0},'{1}', -1, -1, -1, {2})", TitleGroupId, IssueDate, BundleId);
        //                    OleDbDataAdapter da = new OleDbDataAdapter(sql, printDataConnection);
        //                    Logging.AddToLog(LogFolder, LogLevels.Warning, "GetLableData: " + sql);

        //                    labelData = new DataTable();
        //                    da.FillSchema(labelData, SchemaType.Source);
        //                    da.Fill(labelData);
        //                    GetTopSheetStringsByDataTable(labelData, out isSTDBundle);

        //                    GetResult = true;
        //                    return GetResult;
        //                }
        //                else if(topSheetType == TopSheetMain.TopSheetTypes.WHITEPAPER)
        //                {
        //                    TopsheetCreateTemplate =
        //                        "^CREATE;DB_6;792\r\n" +
        //                        "ALPHA\r\n" +
        //                        "15;100;4;1;*INIT PAGE     *\r\n" +
        //                        "30;100;4;1;*INIT PAGE*\r\n" +
        //                        "AF3;6;7;75;2;2\r\n" +
        //                        "STOP\r\n" +
        //                        "END\r\n";
        //                    TopsheetExecContent = "^EXECUTE;DB_6\r\n^AF3;*      *\r\n\r\n^NORMAL\r\n";

        //                    return true;
        //                }
        //            }
        //            catch(Exception ex)
        //            {
        //                Logging.AddToLog(LogFolder, LogLevels.Warning, "GetLableDataError: " + ex.ToString());

        //                GetResult = false;
        //                return GetResult;
        //            }
        //        }
        //        return GetResult;
        //    }
        //    catch
        //    {
        //        GetResult = false;
        //        return GetResult;
        //    }
        //}
        public LabelItem.printerResultTypes GetLabelFieldByBundleId(short TitleGroupId, string IssueDate, uint BundleId, TopSheetMain.TopSheetTypes topSheetType, out bool isSTDBundle)
        {
            LabelItem.printerResultTypes GetResult = LabelItem.printerResultTypes.Unknown;
            isSTDBundle = false;
            try
            {
                if (printDataConnection == null || printDataConnection.State != ConnectionState.Open)
                {
                    try
                    {
                        printDataConnection = new OleDbConnection(connectionString);
                        printDataConnection.Open();
                    }
                    catch
                    {
                        printDataConnection = null;
                        GetResult = LabelItem.printerResultTypes.ConnectionError;
                        return GetResult;
                    }
                }

                if (printDataConnection != null && printDataConnection.State == ConnectionState.Open)
                {
                    //Dictionary<string, string> dictionary = new Dictionary<string, string>();
                    try
                    {
                        DataTable labelData = null;

                        if (topSheetType != TopSheetMain.TopSheetTypes.WHITEPAPER)
                        {
                            string sql = string.Format("select * from UniStack_GetLabelData({0},'{1}', -1, -1, -1, {2})", TitleGroupId, IssueDate, BundleId);
                            OleDbDataAdapter da = new OleDbDataAdapter(sql, printDataConnection);
                            Logging.AddToLog(LogFolder, LogLevels.Warning, "GetLableData: " + sql);

                            labelData = new DataTable();
                            da.FillSchema(labelData, SchemaType.Source);
                            da.Fill(labelData);
                            GetTopSheetStringsByDataTable(labelData, out isSTDBundle);

                            GetResult = LabelItem.printerResultTypes.Ok;
                            return GetResult;
                        }
                        else
                        {
                            TopsheetCreateTemplate =
                                "^CREATE;DB_7;792\r\n" +
                                "ALPHA\r\n" +
                                "2;43;1;1;*WHITE PAPER*\r\n" +
                                "2;55;1;1;*WHITE PAPER*\r\n" +
                                "AF3;6;7;75;2;2\r\n" +
                                "STOP\r\n" +
                                "END\r\n";
                            TopsheetExecContent = 
                                "^EXECUTE;DB_7\r\n" +
                                "^AF3;*      *\r\n" +
                                "\r\n" +
                                "^NORMAL\r\n";

                            GetResult = LabelItem.printerResultTypes.Ok;
                            return GetResult;
                        }
                    }
                    catch (Exception ex)
                    {
                        Logging.AddToLog(LogFolder, LogLevels.Warning, "GetLableDataError: " + ex.ToString());

                        TopsheetCreateTemplate =
                                "^CREATE;DB_8;792\r\n" +
                                "ALPHA\r\n" +
                                "2;40;2;2;*Topsheet Error, Please Check and Try to Manual Print!*\r\n" +
                                "2;60;3;3;*BundleId: " + BundleId.ToString() + "*\r\n" +
                                "AF3;6;7;75;2;2\r\n" +
                                "STOP\r\n" +
                                "END\r\n";
                        TopsheetExecContent =
                                "^EXECUTE;DB_8\r\n" +
                                "^AF3;*      *\r\n" +
                                "\r\n" +
                                "^NORMAL\r\n";

                        GetResult = LabelItem.printerResultTypes.Ok;


                        //GetResult = LabelItem.printerResultTypes.DataError;
                        return GetResult;
                    }
                }
            }
            catch
            {
                GetResult = LabelItem.printerResultTypes.Unknown;
                return GetResult;
            }
            Logging.AddToLog(LogFolder, LogLevels.Warning,"Bundle: " + BundleId + " GetLableData: " + GetResult.ToString());

            return GetResult;

        }


        private string GetAFString(string AFStringIn, string AFNr, int StrLength, bool IsSTD)
        {
            string CurAFStr = "";

            try
            {
                if (!IsSTD)
                {
                    if (AFStringIn.ToString().Length > 0)
                    {
                        if (AFStringIn.ToString().Length <= StrLength)
                        {
                            CurAFStr = "^AF" + AFNr + ";*" + AFStringIn.ToString().ToUpper().PadRight(StrLength) + "*\r\n";
                        }
                        else
                        {
                            CurAFStr = "^AF" + AFNr + ";*" + AFStringIn.ToString().ToUpper().Substring(0, StrLength) + "*\r\n";
                        }
                    }
                    else
                    {
                        CurAFStr = "^AF" + AFNr + ";*".PadLeft(StrLength + 2) + "*\r\n";
                    }
                }
            }
            catch(Exception ex)
            {
                Logging.AddToLog(LogFolder, LogLevels.Warning, "GetAFString " + AFStringIn.ToString() + " Error: " + ex.ToString());
            }
            return CurAFStr;
        }
    }
}
