﻿using System;
using System.Collections.Generic;
using System.Printing;

namespace TopSheetControlLib
{
    public class PrinterStatus
    {
        #region Properties
        //public bool TrapTonerOk { get; set; }
        //public bool TrapDoorOk { get; set; }
        //public bool TrapPaperOk { get; set; }
        //public string WasteTonerStatus { get; set; }
        //public int TonerPercent { get; set; }
        //public int TopPaperPercent { get; set; }
        //public int BottomPaperPercent { get; set; }

        public byte PrintingStatus { get; set; }
        public byte StatusPrinter { get; set; }
        public string RibbonLife { get; set; }
        public string StatusMessage { get; set; }

        public bool IsP8000 { get; set; }

        public string PrinterOnNet { get; set; }
        //public bool TrapPrinterOk { get; set; }
        //public string PrintingDocumentName { get; set; }
        //public bool DoorOpen { get; set; }
        public bool IsError { get; set; }
        public bool IsConnected { get; set; }
        public DateTime ReadyTime { get; set; }
        public DateTime LastStatusCheck { get; set; }
        public int InitializeSeconds { get; set; }
        //public int PrintQueueCount { get; set; }
        //public List<SnmpAlertMessage> AlertList { get; set; }

        //public PrintQueue _printqueue = (PrintQueue)null;

        //public bool IsReadyToPrint
        //{
        //    get
        //    {
        //        bool flag = IsConnected && !IsError &&
        //            (TrapPaperOk && TrapDoorOk) && TrapTonerOk && TrapPrinterOk;
        //        if (flag && !_wasReady)
        //        {
        //            ReadyTime = DateTime.Now;
        //        }
        //        _wasReady = flag;
        //        if (IsInitializing)
        //        {
        //            flag = false;
        //        }
        //        return flag;
        //    }
        //}

        public bool IsInitializing
        {
            get
            {
                return DateTime.Now < ReadyTime.AddSeconds(InitializeSeconds);
            }

        }
        #endregion

        #region Constructor
        public PrinterStatus()
        {
            LastStatusCheck = DateTime.MinValue;
            ReadyTime = DateTime.MinValue;
            InitializeSeconds = 10;
            //TrapPaperOk = true;
            //TrapDoorOk = true;
            //TrapTonerOk = true;
            //TrapPrinterOk = true;
            PrintingStatus = 0;
            StatusPrinter = 0;
            //RibbonLife = "";
            StatusMessage = "";
            IsP8000 = false;
            //AlertList = new List<SnmpAlertMessage>();
            //PrintQueueCount = _printqueue.NumberOfJobs;
        }
        #endregion
    }
}
