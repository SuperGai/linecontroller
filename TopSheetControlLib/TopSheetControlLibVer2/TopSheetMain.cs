﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Data.OleDb;
using System.Net.Sockets;
using System.IO;
using System.Runtime.InteropServices;
using System.Data;
using System.Net;
using SnmpSharpNet;
using System.Diagnostics;
using System.Windows;
using System.Net.NetworkInformation;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace TopSheetControlLib
{
	public class TopSheetMain : IDisposable
	{
		#region Constants
		private const byte SOH = 0x01;
		private const byte EOT = 0x04;
		private const byte MAX_LABEL_POSITIONS = 3;//4;
		private const byte MAX_BUNDLE_POSITIONS = 15; //9;//14;//10;
		private const byte MAX_BUNDLE_HANDLED = 20;
		//private const byte MAX_PRINTERS = 2;
		//private const byte MAX_SLOTS = 4;
		private const byte NUM_DBH = 2;
		private const byte SLOT_COUNT = 4;
		private const byte STB_SLOT_COUNT = 8;
		//private const byte NUM_ABLGROUP = 4;
		//private const byte MAX_BUNDLE_EjectedQueue = 3; //2 //1 //2 //6	//Max number of bundles between ABL and PSS
		//private const byte MAX_BUNDLE_Stop_POSITIONS = 8; //Positions between Entrance and labeling
		//private const byte MAX_LABEL_QUEUE = 30;        //20 //10 //5
		//private const byte MAX_BUNDLE_QUEUE = 30;       //20 //10 //5 //4	//Harstad Line 1: 4, Line 2: 3

		private const byte NUM_ABL = 3;
		private const byte NUM_STB_POS = 4;
		private const byte NUM_THERMO = 2;
		private const byte NUM_PZF_VALVE = 2;
		//private const byte MAX_BUNDLE_EjectedQueue = 8; //Max number of bundles between ABL and PZF table
		//private const byte MAX_BUNDLE_QUEUE = 30;  //


		#endregion

		#region STBStructs
		private struct DbCommandStuct
		{
			public DbCommandTypes dbCommandType;
			public string sql;
			public object oParams;
		}

		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct BundleHandledStruct
		{
			public byte cCode;
			//public ushort EditionTableId;
			public uint bundleId; // uint bundleId;//2
			//Added by Jie. 2019.10.12
			//public ushort EditionId;
			//public ushort RouteId;
			//public ushort OrderId;
			//public ushort BundleSeqInRoute;
			//public ushort BundleSeqInOrder;

			public ushort iConveyorBundleIndex;
			public ushort iLabelBundleIndex;
			public ushort iRejectedBundleIndex;
			public byte cPosition;
			//public byte PrinterIndex;
		}

		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct StatusLabelPositionStruct
		{
			public byte State;
			//public byte ReadyToReceive;
			public uint Tick;
			public uint ExpectedMinTick;
			public uint ExpectedMaxTick;
			public uint RemovedTick;

			public ushort ExpectedBundleIndex;
			public ushort CurrentBundleIndex;
			public ushort LeavingBundleIndex;

			public ushort CurBundleId;

		}

		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct StatusBundlePositionStruct
		{
			public byte State;
			public byte IsStop; //IsStoped or not.
			public byte ReadyToReceive;
			public byte IsNextPosBusy;
			public byte IsCurPosBusy;
			public int LastCurPosBusyTick;
			public int Tick;
			public int ExpectedMinTick;
			public int ExpectedMaxTick;
			public int RemovedTick;
			public short ExpectedBundleIndex;
			public short CurrentBundleIndex;
			public short LeavingBundleIndex;
		}

		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct StatusStruct
		{
			public byte SOH;
			public byte TelegramId;
			public byte Command;
			public ushort Length;

			[MarshalAs(UnmanagedType.ByValArray, SizeConst = STB_SLOT_COUNT)]
			public ushort[] DigitalInputs;
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = STB_SLOT_COUNT)]
			public ushort[] DigitalOutputs;

			public byte cIsSTBReady;
			public byte cIsResetSTB;

			public byte cSTBStartStopState;
			//public byte cDBHStartStopState;
			public byte cSTBStopPosLState; //1: Stop last position; 0: not set.
			//public byte cBundleStopDState;
			//public byte cBundleStopSState;
			public byte cDBHArmState;
			//public byte cStartStopState;
			//public byte cApplicatorStopConveyor;
			public byte cApplicationState;
			public byte cDBHBundleComingState;

			public ushort cApplyingLabelIndex;
			//public ushort CurArmEditionId;
			public ushort CurArmBundleId;
			public ushort CurArmBundleIndex;

			[MarshalAs(UnmanagedType.ByValArray, SizeConst = NUM_DBH)]
			public byte[] cDBHMainRollerState;

			[MarshalAs(UnmanagedType.ByValArray, SizeConst = NUM_DBH)]
			public byte[] cIsDBHMainRollerRunning;

			[MarshalAs(UnmanagedType.ByValArray, SizeConst = NUM_DBH)]
			public byte[] cIsDBHPaperTransportRunning;

			[MarshalAs(UnmanagedType.ByValArray, SizeConst = NUM_DBH)] //1: full; 0: not full
			public byte[] cLabelPositionState;

			[MarshalAs(UnmanagedType.ByValArray, SizeConst = NUM_DBH)]
			public byte[] cRejectState;

			[MarshalAs(UnmanagedType.ByValArray, SizeConst = NUM_DBH)]
			public byte[] cLabelQueueCount;

			[MarshalAs(UnmanagedType.ByValArray, SizeConst = NUM_DBH)]
			public byte[] cIsPaperJamOrMissing; //If paper jamed or is missing to entrance


			public byte cNextMachineReadyState;
			public byte cBundlesHandledCount;
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = MAX_BUNDLE_HANDLED)]
			public BundleHandledStruct[] cBundlesHandled;

			[MarshalAs(UnmanagedType.ByValArray, SizeConst = MAX_BUNDLE_POSITIONS)]
			public StatusBundlePositionStruct[] BundlePositions;

			[MarshalAs(UnmanagedType.ByValArray, SizeConst = NUM_DBH * MAX_LABEL_POSITIONS)]
			public StatusLabelPositionStruct[] LabelPositions;

			//[MarshalAs(UnmanagedType.ByValArray, SizeConst = MAX_LABEL_POSITIONS)]
			//public StatusLabelPositionStruct[] LabelPositions2;

			public byte EOT;
		}

		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct SetupPositionStruct
		{
			//public byte Position;
			//public byte Position;
			public byte EnteringInputSlot;
			public byte EnteringInputSignal;
			public byte EnteringInputOnHigh;
			public byte PositionInputSlot;
			public byte PositionInputSignal;
			public byte PositionInputOnHigh;
			public byte ConveyorStoppedInputSlot;
			public byte ConveyorStoppedInputSignal;
			public byte ConveyorStoppedInputOnHigh;
			public byte GateReadyInputSlot;
			public byte GateReadyInputSignal;
			public byte GateReadyInputOnHigh;
			public short ReceiveExpected;
			public short ReceiveTimeout;
			public byte VakuumOutputSlot;
			public byte VakuumOutputSignal;
			public byte EnteringTransportOutputSlot;
			public byte EnteringTransportOutputSignal;
			public byte TransportOutputSlot;
			public byte TransportOutputSignal;
			public byte BrakeOutputSlot;
			public byte BrakeOutputSignal;
			public short DelayBeforeBrake;
			public short BrakeTime;
			public short StopDelay;
			public short StartDelay;
			public short StopDelayLeave;
			public byte EncoderChannel;
			public byte EncoderInputSlot;
			public byte EncoderInputSignal;
			public byte AlarmInputSlot;
			public byte AlarmInputSignal;
			public byte AlarmInputOnHigh;
			public byte UsePulseTiming;
			public byte RejectOutputSlot;
			public byte RejectOutputSignal;
			public byte RejectPosInputSlot;
			public byte RejectPosInputSignal;
			public byte RejectPosInputOnHigh;
			public byte SidePlatesOutputSlot;
			public byte SidePlatesOutputSignal;
			public byte SidePlatesMode;
		}

		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct SetupStruct
		{
			public byte LineNumber;
			public byte ApplicatorType;
			public byte CurrentStackerPosition;
			public byte LabelingConveyorPosition;
			public byte FirstConveyorControlPosition;
			public byte ReadyToPreviousControlPosition;

			public short InputSignalsFilter;

			public byte ButtonStartInputSlot;
			public byte ButtonStartInputSignal;
			public byte ButtonStartInputOnHigh;

			public byte ButtonStopInputSlot;
			public byte ButtonStopInputSignal;
			public byte ButtonStopInputOnHigh;

			public byte ButtonResetInputSlot;
			public byte ButtonResetInputSignal;
			public byte ButtonResetInputOnHigh;

			public byte ButtonStartConvInputSlot;
			public byte ButtonStartConvInputSignal;
			public byte ButtonStartConvInputOnHigh;

			public byte ButtonStopConvInputSlot;
			public byte ButtonStopConvInputSignal;
			public byte ButtonStopConvInputOnHigh;

			public byte StackerExitInputSlot;
			public byte StackerExitInputSignal;
			public byte StackerExitInputOnHigh;

			public byte Function1InputSlot;
			public byte Function2InputSlot;
			public byte Function3InputSlot;

			public byte Function1InputSignal;
			public byte Function2InputSignal;
			public byte Function3InputSignal;

			public byte Function1InputOnHigh;
			public byte Function2InputOnHigh;
			public byte Function3InputOnHigh;

			public byte ArmUpInputSlot;
			public byte ArmUpInputSignal;
			public byte ArmUpInputOnHigh;

			public byte ArmOnBundleInputSlot;
			public byte ArmOnBundleInputSignal;
			public byte ArmOnBundleInputOnHigh;

			public byte RejectReadyInputSlot;
			public byte RejectReadyInputSignal;
			public byte RejectReadyInputOnHigh;

			public byte RejectBackInputSlot;
			public byte RejectBackInputSignal;
			public byte RejectBackInputOnHigh;

			public byte AlarmThermoInputSlot;
			public byte AlarmThermo2InputSlot;

			public byte AlarmThermoInputSignal;
			public byte AlarmThermo2InputSignal;

			public byte AlarmThermoInputOnHigh;
			public byte AlarmThermo2InputOnHigh;

			public byte EmergencyOffInputSlot;
			public byte EmergencyOffInputSignal;
			public byte EmergencyOffInputOnHigh;

			public byte EmergencyOffConvInputSlot;
			public byte EmergencyOffConvInputSignal;
			public byte EmergencyOffConvInputOnHigh;

			public byte EmergencyOffOutputSlot;
			public byte EmergencyOffOutputSignal;

			public byte EmergencyCircuitInputSlot;
			public byte EmergencyCircuitInputSignal;
			public byte EmergencyCircuitInputOnHigh;

			public byte NextMachineReadySlot;
			public byte NextMachineReadySignal;
			public byte NextMachineReadyOnHigh;

			public byte PreviousMachineReadyOutputSlot;
			public byte PreviousMachineReadyOutputSignal;

			public byte PrevTransNotReadyOutputSlot;
			public byte PrevTransNotReadyOutputSignal;

			public byte NextMachineStandbyOutputSlot;
			public byte NextMachineStandbyOutputSignal;

			public byte ReadyPowerOutputSlot;
			public byte ReadyPowerOutputSignal;
			public byte ReadyAirOutputSlot;
			public byte ReadyAirOutputSignal;
			public byte ReadyConveyorsOutputSlot;
			public byte ReadyConveyorsOutputSignal;
			public byte ReadyBeltsOutputSlot;
			public byte ReadyBeltsOutputSignal;

			public byte StartLastConvButtonLightOutputSlot;
			public byte StartLastConvButtonLightOutputSignal;
			public byte StopLastConvButtonLightOutputSlot;
			public byte StopLastConvButtonLightOutputSignal;
			public byte ButtonStartLastConvInputSlot;
			public byte ButtonStartLastConvInputSignal;
			public byte ButtonStartLastConvInputOnHigh;
			public byte ButtonStopLastConvInputSlot;
			public byte ButtonStopLastConvInputSignal;
			public byte ButtonStopLastConvInputOnHigh;

			public byte ArmDownOutputSlot;
			public byte ArmDownOutputSignal;
			public byte ArmUpOutputSlot;
			public byte ArmUpOutputSignal;
			public byte ArmBackOutputSlot;
			public byte ArmBackOutputSignal;

			public short ArmDownAfterLeaveDelay;
			public short ArmDownMintime;
			public short ArmBackTimeout;
			public short ArmDownTimeout;
			public short ArmUpTimeout;

			public byte RejectDownOutputSlot;
			public byte RejectDownOutputSignal;
			public byte RejectBackOutputSlot;
			public byte RejectBackOutputSignal;

			public short RejectDownTime;
			public short RejectUpTime;
			public short RejectBackTimeout;
			public short RejectFrontTimeout;
			public short RejectWaitForLabelTimeout;
			public short RejectWaitForLabelGoneTimeout;

			public byte StartButtonLightOutputSlot;
			public byte StartButtonLightOutputSignal;
			public byte StopButtonLightOutputSlot;
			public byte StopButtonLightOutputSignal;
			public byte ResetButtonLightOutputSlot;
			public byte ResetButtonLightOutputSignal;

			public byte StartConvButtonLightOutputSlot;
			public byte StartConvButtonLightOutputSignal;
			public byte StopConvButtonLightOutputSlot;
			public byte StopConvButtonLightOutputSignal;

			public byte GreenLightOutputSlot;
			public byte GreenLightOutputSignal;
			public byte YellowLightOutputSlot;
			public byte YellowLightOutputSignal;
			public byte RedLightOutputSlot;
			public byte RedLightOutputSignal;
			public byte HornOutputSlot;
			public byte HornOutputSignal;

			public byte Printer1ActivateInputSlot;
			public byte Printer2ActivateInputSlot;

			public byte Printer1ActivateInputSignal;
			public byte Printer2ActivateInputSignal;

			public byte Printer1ActivateInputOnHigh;
			public byte Printer2ActivateInputOnHigh;

			public byte Printer1ActiveOutputSlot;
			public byte Printer2ActiveOutputSlot;

			public byte Printer1ActiveOutputSignal;
			public byte Printer2ActiveOutputSignal;

			public byte Gate1DownInputSlot;
			public byte Gate1DownInputSignal;
			public byte Gate1DownInputOnHigh;

			public byte Gate2DownInputSlot;
			public byte Gate2DownInputSignal;
			public byte Gate2DownInputOnHigh;

			public byte Gate2DownConfirmedInputSlot;
			public byte Gate2DownConfirmedInputSignal;
			public byte Gate2DownConfirmedInputOnHigh;

			public short StartGreenOnTime;
			public short StartGreenOffTime;
			public short AlarmYellowOnTime;
			public short AlarmYellowOffTime;
			public short EmergencyRedOnTime;
			public short EmergencyRedOffTime;
			public short AlarmHornOnTime;
			public short AlarmHornOffTime;

			public byte BundlePositionCount;
			public byte LabelPositionCountPrinter1;
			public byte LabelPositionCountPrinter2;
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = MAX_BUNDLE_POSITIONS)]
			public SetupPositionStruct[] BundlePositions;
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = MAX_LABEL_POSITIONS)]
			public SetupPositionStruct[] LabelPositionsPrinter1;
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = MAX_LABEL_POSITIONS)]
			public SetupPositionStruct[] LabelPositionsPrinter2;
		}

		#endregion

		#region PZFStructs

		//private struct PzfDbCommandStuct
		//{
		//    public DbCommandTypes dbCommandType;
		//    public string sql;
		//    public object oParams;
		//}

		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct PzfBundleHandledStruct
		{
			public byte cCode; //1: PZF Entrance 10: Go to PZF Table
			public byte ablId;

			//public ushort editionTableId;
			public ushort bundleId;
			
		}

		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct PzfStatusPositionStruct
		{
			public byte State;
			public byte ReadyToReceive;
			public uint Tick;
			public uint ExpectedMinTick;
			public uint ExpectedMaxTick;
			public uint RemovedTick;
			public ushort ExpectedBundleIndex;
			public ushort CurrentBundleIndex;
			public ushort LeavingBundleIndex;
		}

		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct PzfStatusBundlePositionStruct
		{
			public byte State;
			public byte IsStop; //IsStoped or not.
			public byte ReadyToReceive;
			public byte IsNextPosBusy;
			public byte IsCurPosBusy;
			public int LastCurPosBusyTick;
			public int Tick;
			public int ExpectedMinTick;
			public int ExpectedMaxTick;
			public int RemovedTick;
			public short CurrentBundleId;
		}

		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct PzfStatusStruct
		{
			public byte SOH;
			public byte TelegramId;
			public byte Command;
			public ushort Length;

			[MarshalAs(UnmanagedType.ByValArray, SizeConst = SLOT_COUNT)]
			public ushort[] DigitalInputs;
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = SLOT_COUNT)]
			public ushort[] DigitalOutputs;

			//Input state
			public byte IsEmergencyStop;
			public byte NextMachineReady;
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = NUM_ABL)]
			public byte[] BundleStrobeABL;

			[MarshalAs(UnmanagedType.ByValArray, SizeConst = NUM_THERMO)]
			public byte[] ThermoM; //Thermo Moter. If moter is overload

			//Output state
			public byte STBMotor;
			public byte PZFMotor;
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = NUM_ABL)]
			public byte[] PZFReadyABL;

			[MarshalAs(UnmanagedType.ByValArray, SizeConst = NUM_ABL * NUM_STB_POS)] //ValvePosY[NUM_ABL][NUM_STB_POS];
			public byte[] ValvePosY;

			public byte BundleStrobeToSTB;

			[MarshalAs(UnmanagedType.ByValArray, SizeConst = NUM_PZF_VALVE)]
			public byte[] ValveToPos;

			[MarshalAs(UnmanagedType.ByValArray, SizeConst = NUM_PZF_VALVE)]
			public byte[] ValveFromPos;

			//Status
			public byte TableSwitchPoint;
			public byte IsReadyToSwitch;
			public byte IsTableRunning;
			public byte cBundlesHandledCount;

			[MarshalAs(UnmanagedType.ByValArray, SizeConst = MAX_BUNDLE_HANDLED)]
			public PzfBundleHandledStruct[] cBundlesHandled;

			[MarshalAs(UnmanagedType.ByValArray, SizeConst = NUM_ABL * NUM_STB_POS)]
			public PzfStatusBundlePositionStruct[] BundlePositions;

			public byte EOT;
		}


		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct PzfSetupPositionStruct
		{
			public byte SidePlatesMode;
		}

		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct PzfSetupStruct
		{
			public byte LineNumber;
			public byte CurrentStackerPosition;
			public short InputSignalsFilter;

			public byte ButtonStartInputSlot;
			public byte ButtonStartInputSignal;
			public byte ButtonStartInputOnHigh;

			public byte ButtonStopInputSlot;
			public byte ButtonStopInputSignal;
			public byte ButtonStopInputOnHigh;

			public byte ButtonResetInputSlot;
			public byte ButtonResetInputSignal;
			public byte ButtonResetInputOnHigh;

			public byte NextMachineReadySlot;
			public byte NextMachineReadySignal;
			public byte NextMachineReadyOnHigh;

			public byte PreviousMachineReadyOutputSlot;
			public byte PreviousMachineReadyOutputSignal;

			public byte GreenLightOutputSlot;
			public byte YellowLightOutputSlot;
			public byte RedLightOutputSlot;

			public short StartGreenOnTime;
			public short StartGreenOffTime;
			public short AlarmYellowOnTime;
			public short AlarmYellowOffTime;
			public short EmergencyRedOnTime;
			public short EmergencyRedOffTime;

			public byte BundlePositionCount;
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = MAX_BUNDLE_POSITIONS)]
			public PzfSetupPositionStruct[] BundlePositions;
		}
		#endregion

		#region Enums

		public enum TopSheetApplicatorTypes : byte
		{
			UNKNOWN = 0,
			TABA_SINGLE = 1,
			TABA_DOUBLE = 2,
			WAMACBA_SINGLE = 3,
			FERAGDBH_SINGLE = 4,
			FERAGDBH_DOUBLE = 5
		}

		public enum EnumBundleInRoute : byte
		{
			Normal = 0,
			FirstBundle = 1,
			LastBundle = 2,
			OverFlow = 3
		}

		public enum TopSheetTypes
		{
			NORMAL = 0,
			INKERROR = 1,
			DOUBLETOPSHEET = 2,
			WHITEPAPER = 3,
			MANUALPRINT = 4,
			RESUMEPRINT = 5
		}

		public enum DbCommandTypes
		{
			None,
			GetNextCommand,
			ExecuteNonQuery,
			GetSetup,
			GetItems
		}

		public enum PrinterControlStates
		{
			Unknown = 0,
			NotRunning = 1,
			Running = 2
		}

		public enum SnmpControlStates
		{
			Unknown = 0,
			NotRunning = 1,
			Running = 2,
			Disconnected = 3,
			Listening = 4
		}

		public enum IcpConnectionStates
		{
			Unknown = 0,
			NotRunning = 1,
			Disconnected = 2,
			Connecting = 3,
			Connected = 4
		}

		public enum DbConnectionStates
		{
			Unknown = 0,
			NotRunning = 1,
			Disconnected = 2,
			Connecting = 3,
			Connected = 4
		}

		public enum IcpTelegramTypes
		{
			RequestStatus,
			SendSetup,
			SendEjectedBundleIndex,
			SendPrintedBundleIndex,
			Reset,
			SendSignal,
			//SetDigitalOutput,
			EnableSimulationMode,
			DisableSimulationMode,
			SendStopApplication,

			StartPaperTransport,
			StopPaperTransport,

			EnableCutter,
			ClearLabelQueue,
			//ResumeClearLabelQueue
			ResetPrinter,
			PrinterLightAlarmOn,
			PrinterLightAlarmOff,

			ArmDownParameterSet


		}

		public enum IcpSlotTypes
		{
			TSLOT_EMPTY = 0,
			TSLOT_DI16 = 1,
			TSLOT_DO16 = 2,
			TSLOT_DI8DO8 = 3,
			TSLOT_PULSE = 4,
			TSLOT_AIN = 5,
			TSLOT_AOUT = 6,
			TSLOT_RSCOM = 7
		}

		public enum TopSheetActionEnum
		{
			Unknown = 99,
			BundleLabeledApproved = 0,
			LabelRejectNotTracked = 1,
			BundleRejectNotTracked = 2,
			BundleRejectLessThanLabel = 3,
			LabelRejectLessThanBundle = 4,
			LabelAndBundleReject = 5,
			LabelMissing = 6,
			BundleMissing = 7,
			OnSTB = 8,
			STBAbort = 9,
			PZFAbort = 10,
			ManualRemoveArmPos = 11,//	UnableToPrint = 11,
			BundleNotReportedPrintQueue = 12,
			Reset = 13,
			PrintQueueFullRemoved = 14,
			PrintedListFullRemoved = 15,
			BundleNotReportedPrintedList = 16,
			AddedBundleAlreadyExistsQueue = 17,
			AddedBundleAlreadyExistsPrinted = 18,
			ArmPosition = 19,
			UnableToPrint = 20,
			AutomaticPrint = 50,
			AutomaticPrintRemovedFromQueue = 51,
			ManualPrintReject = 52,
			ManagerManualPrintReject = 53,
			ResumeTopsheetToBackupPrinter = 54
		}

		public enum TopSheetApplicatorStates
		{
			ApplicatorNotActive = 0,
			ApplicatorOffline = 1,
			ApplicatorReady = 2,
			ApplicatorWarning = 3,
			ApplicatorStopped = 4,
			ApplicatorError = 5,
			ApplicatorStandardBundles = 6,
			ApplicatorInitializing = 7,
			ApplicatorProductionFinished = 8,
			ApplicatorPaperError1 = 9,
			ApplicatorPaperError2 = 10,
			ApplicatorPaperError = 11,
			ApplicatorPaused = 12,
			ApplicatorPausedByOperator = 13,
			ApplicatorAlarmThermo = 14,
			ApplicatorAlarmArm = 15,
			ApplicatorUnknown = 16
		}

		public enum TopSheetPrinterStates
		{
			PrinterUnknown = 0,
			PrinterOffline = 1,
			PrinterInitializing = 2,
			PrinterReady = 3,
			PrinterPaperLow = 4,
			PrinterTonerLow = 5,
			PrinterWasteWarning = 6,
			PrinterError = 7,
			PrinterErrorPaper = 8,
			PrinterErrorToner = 9,
			PrinterErrorDoor = 10,
			PrinterPaused = 11,
			PrinterPausedByOperator = 12,
			PrinterAlarmThermo = 14,

			PrinterPlatenCloseError = 15,
			PrinterLoadPaperError = 16,
			PrinterWarning = 17,
			PrinterTesting = 18,
			PrinterSystemError = 19


		}

		public enum NextMachineStates
		{
			Unknown = 0,
			Ready = 1,
			NotReady = 2
		}

		public enum TsQControlStates
		{
			Unknown = 0,
			NotRunning = 1,
			Running = 2
		}
		#endregion

		#region PZF Enums
		public enum PZFIcpTelegramTypes
		{
			RequestStatus,
			SendSetup,
			SendEjectedBundleId,
			SendEjectedAblBundleId,
			SendEjectedAblBundleIdNew,
			SendEjectedToSTBBundleIndex,
			Reset,
			StopPZF,
			SendSignal,
			SendStopApplication,
			EnableSimulation,
			DisableSimulation,
			SendBundleMessage,
		}

		#endregion

		#region Static Variables

		public TopSheetMain ST_snmpWatcher;
		public List<TopSheetMain> ST_snmpWatcherTopSheetLines;
		private object ST_lock_snmpWatcherTopSheetLines = new object();
		public Thread ST_snmpThread;
		public Socket ST_snmpSocket;
		public ushort ST_SNMPListeningPort;
		//public bool IsLineControllerStarted { get; set; }
		private byte DefaultPrinter;
		private byte BackupPrinter;


		//public Queue<ushort> TsBundleQueue = new Queue<ushort>(); //{ get; set; }
		//public List<ushort> PZFBundleList = new List<ushort>(); //{ get; set; }
		//public Queue<uint> TsBundleIndexQueue = new Queue<uint>(); //{ get; set; }
		public Dictionary<string, string>[] _printerStatus; // = new Dictionary<string, string>();
		public TopSheetPrinterStates[] PrinterStates;
		//public ushort curEditionTableId;

		public Queue<TopSheetItem> TsBundleQueue;
		private object _lock_TsBundleQueue = new object();

		public List<TopSheetItem> TsBundleQueuePrintedResume;//Used For Topsheet Not Cutted But Printer Crashed, Send Topsheet To Backup Printer
		private object _lock_TsBundleQueuePrintedResume = new object();

		public List<TopSheetItem> TsBundleQueueAvoidTwoTopAtSametime;// SPH MAX 5 Topsheet each Side
		private object _lock_TsBundleQueueAvoidTwoTopAtSametime = new object();

		public List<TopSheetItem> PZFBundleList;
		//public List<TopSheetItem> _topSheetEjectedQueue;
		private readonly object _lock_PZFBundleList = new object();

		//public string[] _lastPrinterMessage;

		#endregion

		#region Variables
		private int _gripperLineTableId;
		private int _topSheetLineTableId;
		private int _lastGripperLineId;

		private int _currentStackerPositionId;
		public string _connectionString;
		public string _conn;
		public static string[] _connectionString_cheng;

		private readonly string _logFolder;
		private bool _enableLogFile;
		private bool _enableLogDebug;
		private List<LogItem> _logItemList;
		private readonly object _lock_logItemList = new object();

		private Thread _icpThread;
		private Thread _dbThread;
		private Thread _printerThread;
		//private Thread _manualPrinterThread;
		private Thread _logThread;
		private Thread _tsQThread;

		//public List<string> printerStatusList = new List<string>();

		private object _lockDbCommandList = new object();
		private object _lockSTBTelegramListIcp = new object();
		private object _lockTelegram = new object();
		private OleDbConnection _con;
		private TcpClient _tcpClientIcp;
		private NetworkStream _networkStreamIcp;
		private BinaryReader _binaryReaderIcp;
		private BinaryWriter _binaryWriterIcp;
		private List<byte[]> _telegramListIcp;
		private List<DbCommandStuct> _dbCommandList;
		private byte _pcStatus;
		private byte _telegramId;
		private byte[] _telegramRequestStatus;
		private string _ioIp;
		private readonly int _ioPort;
		private readonly string[] _printerName;
		//private readonly string[] _printerManualName;
		private readonly string[] _printerIp;
		private readonly int[] _printerPort;
		private bool _doReconnectIcp;
		private bool _doReconnectDb;
		private bool _doReconnectPrinter;
		private bool _setupReceived;
		public bool isSTDWithoutTop;
		//public bool isNoTopsheet;
		public bool isDefaultPrintChanged;

		private SetupStruct _setup;
		private readonly int _setupStructSize;
		private StatusStruct _status;
		private readonly int _statusStructSize;

		private ushort _nextBundleIndex;
		private List<TopSheetItem> _topSheetQueue;
		private object _lock_topSheetQueue = new object();
		private List<TopSheetItem> _topSheetPrinted;
		private object _lock_topSheetPrinted = new object();
		private List<TopSheetItem> _topSheetFirstBundleQueue;
		private object _lock_topsheetFirstLastBundleQueue = new object();
		private List<uint> _noTopSheetBundleQueue;
		private object _lock_noTopsheetBundleQueue = new object();


		//public PrinterV2[] _printer;
		//private readonly PrinterV2[] _printerManual;
		private DateTime _lastLCCMessage;
		private DateTime _lastResetTime;

		private readonly DateTime[] _lastPrint;
		//private readonly DateTime[] lastPrintQueueUpdate;
		private readonly bool _useTwoPrinters;
		private bool _resetDone;
		private uint _lastBundleIdResetCheck = 0;
		private uint _lastBundleIdApplicatorCheck = 0;
		public PrintronixPrint[] PrintronixPrinter;
		#endregion

		#region PZFVariables

		private Thread PZFIcpThread;
		//private Thread _PzfdbThread;
	  
		//private readonly object _PzflockDbCommandList = new object();
		private object PZFLockTelegramListIcp = new object();
		private object PZFLockTelegram = new object();
		//private OleDbConnection _Pzfcon;
		private TcpClient PZFTcpClientIcp;
		private NetworkStream PZFNetworkStreamIcp;
		private BinaryReader PZFBinaryReaderIcp;
		private BinaryWriter PZFBinaryWriterIcp;
		private List<byte[]> PZFTelegramListIcp;
		//private List<DbCommandStuct> _PzfdbCommandList;
		private byte PZFTelegramId;
		private byte[] PZFTelegramRequestStatus;
		private readonly string PZFIoIp;
		private readonly int PZFIoPort;

		private bool PZFDoReconnectIcp;

		private PzfSetupStruct PZFSetup;
		private int PZFSetupStructSize;
		private PzfStatusStruct PZFStatus;
		private int PZFSatusStructSize;

		//private ushort _nextBundleIndex;
		//private List<PZFItem> _PzfBundleList;  //Bundles to To STB
		//private readonly object _lock_PzfBundleList = new object();
		//private readonly List<PZFItem>[] _PZFBundleEjected;  //Bundles from ABLs
		//private readonly object _lock_PZFBundleEjected = new object();
		//private List<PZFItem> _PZFBundleManualPrint;  //Bundles to Manual Printer Queue
		//private readonly object _lock_PZFBundleManualPrint = new object();

		//private readonly DateTime _PzflastLCCMessage;
		//private DateTime _lastPrint1;
		//private DateTime _lastPrint2;
		//private DateTime _lastEject;
		//private DateTime _EjectedToStbTime;

		//private readonly DateTime _nextMachineReadyTime;
		//private DateTime _stackerExitReadyTime;
		//private DateTime _pzfLastOnline;
		//private bool _isWaitingToSendPrintToIcp;

		private uint _lastResetBundleId;
		private uint _lastRemoveBundleId;
		

		#endregion

		#region Constructor
		public TopSheetMain(string connectionString,
			int topSheetLineTableId, string ioIp, int ioPort,
			bool useTwoPrinters, string printerName1, string printerName2, 
			//string printerManual1, string printerManual2,
			string printerIp1, string printerIp2, int printerPort1, int printerPort2,
			ushort snmpListeningPort, string[] ConnectionString,string pzfIp, int pzfPort,
			byte _defaultPrinter, byte _backupPrinter)
		{
			try
			{
				ST_SNMPListeningPort = snmpListeningPort;
				_logItemList = new List<LogItem>();

				_enableLogFile = true;
				_enableLogDebug = true;
				
				if (snmpListeningPort > 0)
				{
					if (ST_snmpWatcher == null)
					{
						ST_snmpWatcher = this;
					}

					lock (ST_lock_snmpWatcherTopSheetLines)
					{
						if (ST_snmpWatcherTopSheetLines == null)
						{
							ST_snmpWatcherTopSheetLines = new List<TopSheetMain>();
						}

						ST_snmpWatcherTopSheetLines.Add(this);
					}
				}

				string exeName = System.Reflection.Assembly.GetExecutingAssembly().GetModules()[0].FullyQualifiedName;
				string exePath = Path.GetDirectoryName(exeName);
				_logFolder = exePath + "\\Log_Topsheet_" + topSheetLineTableId.ToString();

				_connectionString = connectionString;
				_connectionString_cheng = ConnectionString;

				_conn = "Data Source=" + _connectionString_cheng[0] + ";" +
						"Initial Catalog=" + _connectionString_cheng[1] + ";" +
						"Integrated Security=False;" +
						"User ID=" + _connectionString_cheng[2] + ";" +
						"Password=" + _connectionString_cheng[3] + "";

				//if(_lastGripperLineId != topSheetLineTableId && _lastGripperLineId != 0)
				//{
				//    StopPZF();
				//}

				_lastGripperLineId = topSheetLineTableId;
				_topSheetLineTableId = topSheetLineTableId;

				_con = null;
				IsStarted = false;
				IsProductionStarted = false;
				IcpConnectionState = IcpConnectionStates.NotRunning;
				DbConnectionState = DbConnectionStates.NotRunning;
				_telegramListIcp = new List<byte[]>();
				_dbCommandList = new List<DbCommandStuct>();
				_telegramRequestStatus = null;
				_pcStatus = 3;//no label
				_ioIp = ioIp;
				_ioPort = ioPort;

				_printerName = new string[NUM_DBH];
				_printerName[0] = printerName1;
				_printerName[1] = printerName2;

				//_printerManualName = new string[NUM_DBH];
				//_printerManualName[0] = printerManual1;
				//_printerManualName[1] = printerManual2;

				//_lastPrinterMessage = new string[NUM_DBH];
				//_lastPrinterMessage[0] = "";
				//_lastPrinterMessage[1] = "";

				_printerIp = new string[NUM_DBH];
				_printerIp[0] = printerIp1;
				_printerIp[1] = printerIp2;

				_printerPort = new int[NUM_DBH];
				_printerPort[0] = printerPort1;
				_printerPort[1] = printerPort2;

				_doReconnectIcp = false;
				_setupReceived = false;

				DefaultPrinter = _defaultPrinter;
				BackupPrinter = _backupPrinter;
				
				_setup = new SetupStruct
				{
					BundlePositions = new SetupPositionStruct[MAX_BUNDLE_POSITIONS],
					LabelPositionsPrinter1 = new SetupPositionStruct[MAX_LABEL_POSITIONS],
					LabelPositionsPrinter2 = new SetupPositionStruct[MAX_LABEL_POSITIONS]
				};
				_status = new StatusStruct();

				_setupStructSize = Marshal.SizeOf(typeof(SetupStruct));
				_statusStructSize = Marshal.SizeOf(typeof(StatusStruct));

				_topSheetQueue = new List<TopSheetItem>();
				_topSheetPrinted = new List<TopSheetItem>();
				_topSheetFirstBundleQueue = new List<TopSheetItem>();
				_noTopSheetBundleQueue = new List<uint>();

				//_topSheetEjectedQueue = new List<TopSheetItem>();
				TsBundleQueue = new Queue<TopSheetItem>();
				TsBundleQueuePrintedResume = new List<TopSheetItem>();
				TsBundleQueueAvoidTwoTopAtSametime = new List<TopSheetItem>();
				PZFBundleList = new List<TopSheetItem>();

				_nextBundleIndex = 11;

				_useTwoPrinters = useTwoPrinters;

				//_printer = new PrinterV2[NUM_DBH];
				//_printerManual = new PrinterV2[NUM_DBH];
				PrintronixPrinter = new PrintronixPrint[NUM_DBH];
				_lastPrint = new DateTime[NUM_DBH];
				//lastPrintQueueUpdate = new DateTime[NUM_DBH];
				_printerStatus = new Dictionary<string, string>[NUM_DBH];
				PrinterStates = new TopSheetPrinterStates[NUM_DBH];

				for (int i = 0; i < NUM_DBH; i++)
				{
					
					PrintronixPrinter[i] = new PrintronixPrint(_printerIp[i], _printerPort[i], _connectionString, _logFolder);

					_lastPrint[i] = DateTime.Now;

					_printerStatus[i] = new Dictionary<string, string>
					{
						{ "PrintingStatus", "" },
						{ "PrinterStatus", "" },
						//{ "RibbonLife", "" },
						{ "StatusMessage", "OFFLINE" }
					};

					PrinterStates[i] = TopSheetPrinterStates.PrinterUnknown;
				}

				_lastLCCMessage = DateTime.Now;
				//_nextMachineReadyTime = DateTime.Now;
				//_stackerExitReadyTime = DateTime.Now;
				//_isWaitingToSendPrintToIcp = false;
				RequestStackerReset = false;
				isDefaultPrintChanged = false;

				#region PZF Main
				_lastResetBundleId = 0;
				_lastRemoveBundleId = 0;

				//_Pzfcon = null;
				PZFIsStarted = false;
				PZFIcpConnectionState = IcpConnectionStates.NotRunning;
				//PzfDbConnectionState = DbConnectionStates.NotRunning;
				PZFTelegramListIcp = new List<byte[]>();
				//_PzfdbCommandList = new List<DbCommandStuct>();
				PZFTelegramRequestStatus = null;
				PZFPcStatus = 3;
				PZFIoIp = pzfIp;
				PZFIoPort = pzfPort;

				PZFDoReconnectIcp = false;
				//_setupReceived = false;

				PZFSetup = new PzfSetupStruct
				{
					BundlePositions = new PzfSetupPositionStruct[MAX_BUNDLE_POSITIONS]
				};
				PZFStatus = new PzfStatusStruct();

				PZFSetupStructSize = Marshal.SizeOf(typeof(PzfSetupStruct));
				PZFSatusStructSize = Marshal.SizeOf(typeof(PzfStatusStruct));

				//_PzfBundleList = new List<PZFItem>();
				//_PZFBundleEjected = new List<PZFItem>[NUM_ABL];
				//_PZFBundleManualPrint = new List<PZFItem>();
				//_nextBundleIndex = 11;

				//_PzflastLCCMessage = DateTime.Now;
				//_nextMachineReadyTime = DateTime.Now;
				//_stackerExitReadyTime = DateTime.Now;
				PZFRequestStackerReset = false;
				#endregion
			}
			catch (Exception ex)
			{
				AddToLog(LogLevels.Warning, "Init Topsheet & PZF Main Error: " + ex.ToString());
			}

		}
		#endregion

		#region Properties
		public bool IsStarted { get; set; }
		public bool IsProductionStarted { get; set; }
		public short TitleGroupId { get; set; }
		public string IssueDateString { get; set; }
		public IcpConnectionStates IcpConnectionState { get; set; }
		public DbConnectionStates DbConnectionState { get; set; }
		public PrinterControlStates PrinterControlState { get; set; }
		//public PrinterControlStates ManualPrinterControlState { get; set; }
		public SnmpControlStates SnmpControlState { get; set; }
		public TsQControlStates TsQControlState { get; set; }

		//public string ThreadInfoIcpControl
		//{
		//    get
		//    {
		//        string result = "";// GetThreadInfo(_icpThread, _icpSystemThreadId);
		//        return result;
		//    }
		//}

		public bool ResetDone
		{
			get { return _resetDone; }// && IsLineControllerStarted; }
			set { _resetDone = value; }
		}

		//public string ThreadInfoDbControl
		//{
		//    get
		//    {
		//        string result = "";// GetThreadInfo(_dbThread, _dbSystemThreadId);
		//        return result;
		//    }
		//}

		//public string ThreadInfoPrinterControl
		//{
		//    get
		//    {
		//        string result = "";// GetThreadInfo(_printerThread, _printerSystemThreadId);
		//        return result;
		//    }
		//}

		//public string ThreadInfoLogControl
		//{
		//    get
		//    {
		//        string result = "";// GetThreadInfo(_logThread, _logSystemThreadId);
		//        return result;
		//    }
		//}

		//public uint LastEjectedBundleId { get; set; }

		//public string PrinterIp1
		//{
		//    get
		//    {
		//        string result = _printerIp[0];
		//        return result;
		//    }
		//}

		//public string PrinterIp2
		//{
		//    get
		//    {
		//        string result = _printerIp[1];
		//        return result;
		//    }
		//}

		//public PrinterV2 Printer1
		//{
		//    get
		//    {
		//        PrinterV2 result = _printer[0];
		//        return result;
		//    }
		//}

		//public PrinterV2 Printer2
		//{
		//    get
		//    {
		//        PrinterV2 result = _printer[1];
		//        return result;
		//    }
		//}

		//public PrinterV2 PrinterManual1
		//{
		//    get
		//    {
		//        PrinterV2 result = _printerManual[0];
		//        return result;
		//    }
		//}

		//public PrinterV2 PrinterManual2
		//{
		//    get
		//    {
		//        PrinterV2 result = _printerManual[1];
		//        return result;
		//    }
		//}

		public bool IsSnmpWatcher
		{
			get
			{
				bool result = this == ST_snmpWatcher;
				return result;
			}
		}

		public int TopSheetLineTableId
		{
			get
			{
				int result = _topSheetLineTableId;
				return result;
			}
		}

		public byte STBStatus
		{
			get { return _status.cSTBStartStopState; }
		}

		//public IcpConnectionStates _topsheetIcpConnectionStates
		//{
		//    get { return IcpConnectionState; }
		//}

		public bool UseTwoPrinters
		{
			get
			{
				bool result = _useTwoPrinters;
				return result;
			}
		}

		public byte PcStatus
		{
			get { return _pcStatus; }
			set { _pcStatus = value; }
		}

		//public byte StbStatus
		//{
		//    get { return _status.cSTBStartStopState; }
		//}

		public byte PcStatus_printer1
		{
			get
			{
				byte result = GetPcStatusPrinter(1, out bool isPausedByOperator);
				return result;
			}
		}

		public byte PcStatus_Printer2
		{
			get
			{
				byte result = GetPcStatusPrinter(2, out bool isPausedByOperator);
				return result;
			}
		}

		public bool IsPausedByOperator1
		{
			get
			{
				GetPcStatusPrinter(1, out bool isPausedByOperator);
				return isPausedByOperator;
			}
		}

		public bool IsPausedByOperator2
		{
			get
			{
				GetPcStatusPrinter(2, out bool isPausedByOperator);
				return isPausedByOperator;
			}
		}

		public bool IsPaused1 { get; set; }
		public bool IsPaused2 { get; set; }

		public bool WaitTransportLabelUntilBundleArrived1 { get; set; }
		public bool WaitTransportLabelUntilBundleArrived2 { get; set; }

		public ushort[] LabelPositionBundleId
		{
			get
			{
				ushort[] result = new ushort[2];

				for (int i = 0; i < 2; i++)
				{
					result[i] = _status.LabelPositions[i * MAX_LABEL_POSITIONS].CurBundleId;
				}

				return result;
			}
		}

		public ushort ApplicatorPositionBundleId
		{
			get
			{
				return _status.CurArmBundleId;
			}
		}

		public StatusStruct Status
		{
			get { return _status; }
		}

		public SetupStruct Setup
		{
			get { return _setup; }
		}

		//public int PrinterQueueCount1
		//{
		//    get
		//    {
		//        return _printer[0] != null && _printer[0]._printerStatus != null ?
		//            _printer[0]._printerStatus.PrintQueueCount : 0;

		//    }
		//}


		//public int PrinterQueueCount2
		//{
		//    get
		//    {
		//        //return _printer[1] != null && _printer[1].PrinterStatus != null ?
		//        //    _printer[1].PrinterStatus.PrintQueueCount : 0;
		//        return 1;
		//    }
		//}

		public byte TopSheetQueue1
		{
			get
			{
				return _status.cLabelQueueCount[0]; //cheng 09-18
			}
		}

		public byte TopSheetQueue2
		{
			get
			{
				return _status.cLabelQueueCount[1]; //cheng 09-18
			}
		}

		//public TopSheetApplicatorStates TopSheetApplicatorState
		//{
		//    get
		//    {
		//        TopSheetApplicatorStates result = TopSheetApplicatorStates.ApplicatorNotActive;

		//        if (IsStarted)
		//        {
		//            if (IcpConnectionState != IcpConnectionStates.Connected)
		//            {
		//                result = TopSheetApplicatorStates.ApplicatorOffline;
		//            }
		//            else
		//            {
		//                if (IsPrinterReadyToPrint(1) == TopSheetPrinterStates.PrinterError && IsPrinterReadyToPrint(2) == TopSheetPrinterStates.PrinterError)
		//                {
		//                    result = TopSheetApplicatorStates.ApplicatorPaperError;
		//                }
		//                else if (IsPrinterReadyToPrint(1) == TopSheetPrinterStates.PrinterError)
		//                {
		//                    result = TopSheetApplicatorStates.ApplicatorPaperError1;
		//                }
		//                else if (IsPrinterReadyToPrint(2) == TopSheetPrinterStates.PrinterError)
		//                {
		//                    result = TopSheetApplicatorStates.ApplicatorPaperError2;
		//                }
		//                else if (_status.cSTBStartStopState == 10) // StartStopState == 1) // cheng 09-18
		//                {
		//                    if (_pcStatus == 3)
		//                    {
		//                        result = TopSheetApplicatorStates.ApplicatorStandardBundles;
		//                    }
		//                    else
		//                    {
		//                        result = TopSheetApplicatorStates.ApplicatorReady;
		//                    }
		//                }
		//                else if (_status.cSTBStartStopState == 0) // StartStopState == 0) // cheng 09-18
		//                {
		//                    result = TopSheetApplicatorStates.ApplicatorStopped;
		//                }
		//                else
		//                {
		//                    result = TopSheetApplicatorStates.ApplicatorInitializing;
		//                }
		//            }
		//        }

		//        return result;
		//    }
		//}

		//public int PrintQueueWaiting
		//{
		//    get
		//    {
		//        int count = 0;

		//        lock (_lock_topSheetQueue)
		//        {
		//            for (int i = 0; i < _topSheetQueue.Count; i++)
		//            {
		//                if (_topSheetQueue[i].PrintedTime == DateTime.MinValue)
		//                {
		//                    ++count;
		//                }
		//            }
		//        }

		//        return count;
		//    }
		//}

		//public int CurrentStackerPositionId
		//{
		//    get { return _currentStackerPositionId; }
		//}

		public bool RequestStackerReset { get; set; }

		#endregion

		#region PZFProperties
		public bool PZFIsStarted { get; set; }
		public IcpConnectionStates PZFIcpConnectionState { get; set; }
		public byte PZFRunning
		{
			get { return PZFStatus.STBMotor; }
		}
		public byte PZFPcStatus { get; set; }
		public bool PZFResetDone { get; set; }
		public uint PZFLastEjectedBundleId { get; set; }
		public bool PZFRequestStackerReset { get; set; }
		#endregion

		#region IDisposable

		// Dispose() calls Dispose(true)
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
		// NOTE: Leave out the finalize altogether if this class doesn't 
		// own unmanaged resources itself, but leave the other methods
		// exactly as they are. 
		~TopSheetMain()
		{
			// Finalize calls Dispose(false)
			Dispose(false);
		}
		// The bulk of the clean-up code is implemented in Dispose(bool)
		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
			{
				// free managed resources
				if (_binaryReaderIcp != null)
				{
					try { _binaryReaderIcp.Dispose(); } catch { }
				}

				if (_binaryWriterIcp != null)
				{
					try { _binaryWriterIcp.Dispose(); } catch { }
				}

				if (_con != null)
				{
					try { _con.Dispose(); } catch { }
				}

				if (_tcpClientIcp != null)
				{
					try { _tcpClientIcp.Close(); } catch { }
				}

				// free managed resources
				if (PZFBinaryReaderIcp != null)
				{
					try { PZFBinaryReaderIcp.Dispose(); } catch { }
				}

				if (PZFBinaryWriterIcp != null)
				{
					try { PZFBinaryWriterIcp.Dispose(); } catch { }
				}

				//if (_Pzfcon != null)
				//{
				//    try { _Pzfcon.Dispose(); } catch { }
				//}

				if (PZFTcpClientIcp != null)
				{
					try { PZFTcpClientIcp.Close(); } catch { }
				}
			}
			// free native resources if there are any.
		}

		#endregion

		#region Public methods

		public void Start(int lineId, int currentStackerPositionId)
		{
			if (IsStarted)
			{
				Stop();
			}

			IcpConnectionState = IcpConnectionStates.NotRunning;
			DbConnectionState = DbConnectionStates.NotRunning;
			PrinterControlState = PrinterControlStates.NotRunning;
			//ManualPrinterControlState = PrinterControlStates.NotRunning;
			SnmpControlState = SnmpControlStates.NotRunning;
			TsQControlState = TsQControlStates.NotRunning;

			_gripperLineTableId = currentStackerPositionId;
			//_topSheetLineTableId = lineId;

			//_ioIp = null;
			//_ioPort = 0;
			_doReconnectIcp = false;
			_setupReceived = false;
			_currentStackerPositionId = currentStackerPositionId;

			_resetDone = false;
			AddToLog(LogLevels.Debug, "TopsheetMain Start Reset Applicator!");
			ResetApplicator(true);
			_resetDone = false;

			IsStarted = true;

			_icpThread = new Thread(new ThreadStart(IcpControl))
			{
				Priority = ThreadPriority.Highest
			};
			_icpThread.Start();

			_dbThread = new Thread(new ThreadStart(DbControl));
			_dbThread.Start();

			_printerThread = new Thread(new ThreadStart(PrinterControl));
			_printerThread.Start();

			_tsQThread = new Thread(new ThreadStart(TsQControl));
			_tsQThread.Start();

			_logThread = new Thread(new ThreadStart(LogControl));
			_logThread.Start();

			try
			{
				if (PZFIsStarted)
				{
					PzfStop();
				}

				//_PzfBundleList.Clear();
				//_PZFBundleManualPrint.Clear();

				PZFIcpConnectionState = IcpConnectionStates.NotRunning;
				//PzfDbConnectionState = DbConnectionStates.NotRunning;
				//PZFControlState = PZFControlStates.NotRunning;
				PZFDoReconnectIcp = false;

				PZFResetDone = false;
				ResetPZF(true);
				PZFIsStarted = true;

				PZFIcpThread = new Thread(new ThreadStart(PZFIcpControl))
				{
					Priority = ThreadPriority.Highest
				};
				PZFIcpThread.Start();

			}
			catch
			{
				throw;
			}
		}

		private async void ResetPrinter(int printerNumber)
		{
			try
			{
				await Task.Run(
					() => TestPrint((byte)printerNumber)
				);

				await Task.Run(
					() => TestPrint((byte)printerNumber)
				);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
		}

		public void ResetPZF(bool clearQueues)
		{
			try
			{
				if (!PZFResetDone)
				{
					if (PZFIcpConnectionState == IcpConnectionStates.Connected)
					{
						PZFIcpGetOrAddTelegram(PZFIcpTelegramTypes.Reset, null, true);
					}
					PZFResetDone = true;
				}

				lock(_lock_PZFBundleList)
				{
					if (clearQueues && PZFBundleList.Count != 0)
					{
						for (int i = 0; i < PZFBundleList.Count; i++)
						{
							if (PZFBundleList[i].BundleId != _lastResetBundleId)
							{
								PZFBundleList[i].LabelState = 51;
								PZFBundleList[i].TitleGroupId = TitleGroupId;
								PZFBundleList[i].IssueDateString = IssueDateString;
								PZFBundleList[i].TopSheetType = TopSheetTypes.NORMAL;

								if (PZFBundleList[i].BundleId >= 10000 && PZFBundleList[i].BundleId < 50000)
								{
									//_topSheetEjectedQueue.Add(PZFBundleList[i]);
									lock (_lock_TsBundleQueue)
									{ 
										TsBundleQueue.Enqueue(PZFBundleList[i]);
									}
									AddToLog(LogLevels.Debug, "Reset PZF Added Eject Bundle " + PZFBundleList[i].BundleId.ToString() + " to TsBundleQueue. Bundle LableState: " + PZFBundleList[i].LabelState.ToString());
								}

								_lastResetBundleId = PZFBundleList[i].BundleId;
							}
						}

						lock (_lock_TsBundleQueue)
						{
							TopSheetItem restItem = new TopSheetItem
							{
								BundleId = 65500,
								BundleIndex = 0,
								PrinterNumber = DefaultPrinter,
								TopSheetType = TopSheetTypes.WHITEPAPER,
								LabelState = 50
							};

							TsBundleQueue.Enqueue(restItem);
							TsBundleQueue.Enqueue(restItem);
						}
					}
				}
			}
			catch
			{
			}
		}

		public void StopPZF()
		{
			if (PZFIcpConnectionState == IcpConnectionStates.Connected)
			{
				PZFIcpGetOrAddTelegram(PZFIcpTelegramTypes.StopPZF, null, true);
			}
		}

		public void EnableSimulation()
		{
			if (PZFIcpConnectionState == IcpConnectionStates.Connected)
			{
				PZFIcpGetOrAddTelegram(PZFIcpTelegramTypes.EnableSimulation, null, true);
			}
		}

		public void LCCSendBundleMessage(uint bundleId, ushort RouteSortIndex, ushort BundleSortIndex,byte ABLId)
		{
			try
			{
				TopSheetItem queueItem = new TopSheetItem
				{
					BundleId = bundleId,
					RouteSortIndex = RouteSortIndex,
					BundleSortIndex = BundleSortIndex,
					AblId = ABLId
				};


				if (PZFIcpConnectionState == IcpConnectionStates.Connected)
				{
					PZFIcpGetOrAddTelegram(PZFIcpTelegramTypes.SendBundleMessage, queueItem, true);
					AddToLog(LogLevels.Debug, "LCC Send Bundle " + queueItem.BundleId +
						" to PZF, RouteSortIndex = " + queueItem.RouteSortIndex +
						" BundleSortIndex = " + queueItem.BundleSortIndex +
						" ABL = " + queueItem.AblId);

				}
			}
			catch
			{
			}
		   
		}

		public void TsQControl()
		{
			try
			{
				AddToLog(LogLevels.Debug, "TsQControl started");

				TsQControlState = TsQControlStates.Running;

				int count = 0;

				while (IsStarted &&
					_tsQThread != null &&
					_tsQThread.ManagedThreadId == Thread.CurrentThread.ManagedThreadId)
				{
				   
					try
					{
						//uint curBundleId, bundleIndex;
						TopSheetItem curBundle = new TopSheetItem();
						//uint bundleIndex;
						lock (_lock_TsBundleQueue)
						{
							while (TsBundleQueue.Count > 0) //&& TitleGroupId > 0
															//&& IcpConnectionState == IcpConnectionStates.Connected)
							{
								count = 0;
								//curBundleId = TsBundleQueue.Dequeue().BundleId;
								curBundle = TsBundleQueue.Dequeue();

								if (curBundle.TopSheetType != TopSheetTypes.WHITEPAPER)
								{
									curBundle.BundleIndex = _nextBundleIndex++;

									if (curBundle.BundleIndex <= 10)
									{
										AddToLog(LogLevels.Warning, "Bundle Index: " + curBundle.BundleIndex.ToString() + " out of range - reset");
										ResetApplicator(true);
										_nextBundleIndex = 11;
									}

									AddToLog(LogLevels.Debug, "TsQControl Eject to Bundle Queue" +
										" - Bundle: " + curBundle.BundleId.ToString() +
										" - BundleIndex: " + curBundle.BundleIndex.ToString() +
										" - LabelState: " + curBundle.LabelState.ToString() +
										" - TitleGroupId: " + curBundle.TitleGroupId.ToString() +
										" - IsNoTopsheet: " + curBundle.IsNoTopsheet.ToString() +
										" - RouteSortIndex: " + curBundle.RouteSortIndex.ToString() +
										" - BundleSortIndex: " + curBundle.BundleSortIndex.ToString() +
										" - Time: " + DateTime.Now);


									if (DateTime.Now > curBundle.EjectTime.AddMilliseconds(4000))
									{
										curBundle.LabelState = 51;

										AddToLog(LogLevels.Debug, "TsQControl - Bundle Timeout to Reject: " + curBundle.BundleId.ToString() +
										" - LabelState: " + curBundle.LabelState.ToString());

										if (curBundle.BundleId >= 10000 && curBundle.BundleId < 50000 && !curBundle.IsNoTopsheet)
										{
											PrintBundle(curBundle);
										}
									}
									else
									{
										if (curBundle.LabelState == 0) // Normal Bundle
										{
											AddToLog(LogLevels.Debug, "TsQControl" +
												" - Normal Bundle to Reject: " + curBundle.BundleId.ToString() +
												" - LabelState: " + curBundle.LabelState.ToString() +
												" - IsNoTopsheet: " + curBundle.IsNoTopsheet);

											BundleEjected(curBundle);

											if (curBundle.BundleId >= 10000 && curBundle.BundleId < 50000 && !curBundle.IsNoTopsheet)
											{
												PrintBundle(curBundle);
											}
										}
										else // Bundle Need Reject
										{
											AddToLog(LogLevels.Debug, "TsQControl - Bundle Need Reject: " + curBundle.BundleId.ToString() +
											" - LabelState: " + curBundle.LabelState.ToString());

											if (curBundle.BundleId >= 10000 && curBundle.BundleId < 50000 && !curBundle.IsNoTopsheet)
											{
												PrintBundle(curBundle);//.BundleId, bundleIndex);
											}
										}
									}
								}
								else
								{
									AddToLog(LogLevels.Debug, "TsQControl - Reset Bundle Need Reject - LabelState: " + curBundle.LabelState.ToString());
									PrintBundle(curBundle);//.BundleId, bundleIndex);
								}
							}
						}
							
						if (count == 0 && IcpConnectionState != IcpConnectionStates.Connected)
						{
							AddToLog(LogLevels.Warning, "TsQControl Warning, IcpConnectionState: " + IcpConnectionState);
							count++;
						}

					}
					catch (Exception ex)
					{
						AddToLog(LogLevels.Warning, ex.ToString());
						Thread.Sleep(1000);
					}
					finally
					{
						Thread.Sleep(100);
					}
					
				}

				if (_tsQThread.ManagedThreadId != Thread.CurrentThread.ManagedThreadId)
				{
					AddToLog(LogLevels.Debug, "TsQControl old thread exited");
				}
				else
				{
					TsQControlState = TsQControlStates.NotRunning;

					AddToLog(LogLevels.Debug, "TsQControl stopped");
				}
			}
			catch
			{
			}
		}

		public void SetStackerPosition(int currentStackerPositionId)
		{
			if (currentStackerPositionId != _currentStackerPositionId || currentStackerPositionId != _setup.CurrentStackerPosition)
			{
				_currentStackerPositionId = currentStackerPositionId;
				//_setup.CurrentStackerPosition = (byte)_currentStackerPositionId;

				DbGetSetup();
				DbGetItems();
				//_setupReceived = false;
				//_doReconnectIcp = true;
			}
		}

		//For STB&DBH or BLC&BA only
		public ushort GetDigitalInputs(ushort SlotID)
		{
			try
			{
				if ((_status.DigitalInputs != null) && (SlotID < STB_SLOT_COUNT))
				{
					return _status.DigitalInputs[SlotID];
				}
				else
				{
					return 0;
				}
			}
			catch
			{

			}
			return 0;
		}

		public void Stop()
		{
			IsStarted = false;

			int counter = 0;
			while (++counter < 10 &&
				((IcpConnectionState != IcpConnectionStates.NotRunning && IcpConnectionState != IcpConnectionStates.Unknown) ||
				(DbConnectionState != DbConnectionStates.NotRunning && DbConnectionState != DbConnectionStates.Unknown) ||
				(PrinterControlState != PrinterControlStates.NotRunning && PrinterControlState != PrinterControlStates.Unknown) ||
				(TsQControlState != TsQControlStates.NotRunning && TsQControlState != TsQControlStates.Unknown) ||
				(SnmpControlState != SnmpControlStates.NotRunning && SnmpControlState != SnmpControlStates.Unknown)))
			{
				Thread.Sleep(100);
			}

			if (IcpConnectionState != IcpConnectionStates.NotRunning)
			{
				IcpDisconnect();
				StopThreadCheck(_icpThread);
				StopThreadCheck(_printerThread);
			}

			if (DbConnectionState != DbConnectionStates.NotRunning)
			{
				DbDisconnect();
				StopThreadCheck(_dbThread);
			}

			if (SnmpControlState != SnmpControlStates.NotRunning)
			{
				SnmpDisconnect();
				StopThreadCheck(ST_snmpThread);
			}

			if (TsQControlState != TsQControlStates.NotRunning)
			{
				StopThreadCheck(_tsQThread);
			}

			PzfStop();
		}

		private void StopThreadCheck(Thread _tmpThread)
		{
			try
			{
				if (_tmpThread != null)
				{
					_tmpThread.Interrupt();
				}
			}
			catch
			{
			}
		}

		public void PzfStop()
		{
			try
			{
				PZFIsStarted = false;

				int counter = 0;
				while (++counter < 5 &&
					((PZFIcpConnectionState != IcpConnectionStates.NotRunning && 
					PZFIcpConnectionState != IcpConnectionStates.Unknown)))
				{
					Thread.Sleep(100);
				}

				if (PZFIcpConnectionState != IcpConnectionStates.NotRunning)
				{
					PZFIcpDisconnect();
					StopThreadCheck(PZFIcpThread);
				}

				//if (PzfDbConnectionState != DbConnectionStates.NotRunning)
				//{
				//    PzfDbDisconnect();
				//    StopThreadCheck(_PzfdbThread);
				//}
			}
			catch
			{
			}
		}

		public void EnableDisableSimulation(byte SimulationStatus)// 0,1,2
		{
			if (SimulationStatus > 0)
			{
				PZFIcpGetOrAddTelegram(PZFIcpTelegramTypes.EnableSimulation, null, true);
			}
			else
			{
				PZFIcpGetOrAddTelegram(PZFIcpTelegramTypes.DisableSimulation, null, true);
			}
		}

		public void AddIntoBundleQueue(TopSheetItem queueItem)// 0,1,2
		{
			try
			{
				lock (_lock_TsBundleQueue)
				{
					TsBundleQueue.Enqueue(queueItem);
				}
			}
			catch
			{
			}
			

		}


		public void StartProduction()
		{
			//PcStatus = 2;
			//DbGetSetup();
			//DbGetItems();
			//ResetApplicator(true);
			try
			{
				AddToLog(LogLevels.Debug, "Start production" +
					" - TopsheetQueue: " + _topSheetQueue.Count +
					" - TopsheetPrintedQueue: " + _topSheetPrinted.Count + 
					" - TsBundleQueue: " + TsBundleQueue.Count +
					" - TopSheetFirstBundleQueue: " + _topSheetFirstBundleQueue.Count +
					" - TsBundleQueuePrintedResume: " + TsBundleQueuePrintedResume.Count +
					" - TsBundleQueueAvoidTwoTopAtSametime: " + TsBundleQueueAvoidTwoTopAtSametime.Count +
					" - Backup Printer " + BackupPrinter + " Set To Clear Mode!");

				IsProductionStarted = true;
				_topSheetQueue.Clear();
				_topSheetPrinted.Clear();
				_topSheetFirstBundleQueue.Clear();
				_noTopSheetBundleQueue.Clear();


				TsBundleQueue.Clear();
				TsBundleQueueAvoidTwoTopAtSametime.Clear();
				TsBundleQueuePrintedResume.Clear();

				//IcpGetOrAddTelegram(IcpTelegramTypes.ClearLabelQueue, BackupPrinter, true);

				//ManualPrinterControlState = PrinterControlStates.NotRunning;
				//_manualPrinterThread = new Thread(new ThreadStart(ManualPrinterControl));
				//_manualPrinterThread.Start();

			}
			catch
			{
			}
		}

		public void StopProduction()
		{
			try
			{
				//try
				//{
				//    if (_manualPrinterThread != null)
				//    {
				//        _manualPrinterThread.Interrupt();
				//    }
				//}
				//catch
				//{
				//}

				AddToLog(LogLevels.Debug, "Stop production and Paper Transport" +
				" - TopsheetQueue: " + _topSheetQueue.Count +
				" - TopsheetPrintedQueue: " + _topSheetPrinted.Count +
				" - TsBundleQueue: " + TsBundleQueue.Count +
				" - TopSheetFirstBundleQueue: " + _topSheetFirstBundleQueue.Count +
				" - TsBundleQueuePrintedResume: " + TsBundleQueuePrintedResume.Count +
				" - TsBundleQueueAvoidTwoTopAtSametime: " + TsBundleQueueAvoidTwoTopAtSametime.Count);

				//ResetPrinter(DefaultPrinter);
				PrintronixPrinter[DefaultPrinter - 1].Disconnect();
				IsProductionStarted = false;

				//IcpGetOrAddTelegram(IcpTelegramTypes.StopPaperTransport, (byte)1, true);
				//IcpGetOrAddTelegram(IcpTelegramTypes.StopPaperTransport, (byte)2, true);

				_topSheetQueue.Clear();
				_topSheetPrinted.Clear();
				_topSheetFirstBundleQueue.Clear();
				_noTopSheetBundleQueue.Clear();

				TsBundleQueue.Clear();
				TsBundleQueueAvoidTwoTopAtSametime.Clear();
				TsBundleQueuePrintedResume.Clear();

			}
			catch
			{
			}
		}

		public void PutIntoEjectQueue(short titleGroupId, string titleIssueDate, 
			byte ablId, byte code, uint BundleId, ushort RouteSortIndex, ushort BundleSortIndex, 
			EnumBundleInRoute bundleSq, bool IsNoTopsheet, bool IsSTDBundle,byte RouteModeId)
		{
			try
			{
				TitleGroupId = titleGroupId;
				IssueDateString = titleIssueDate;
				//isNoTopsheet = IsNoTopsheet;

				BundleEjectedFromABL(BundleId, ablId, code, RouteSortIndex, BundleSortIndex, bundleSq, IsNoTopsheet, IsSTDBundle, RouteModeId);
			}
			catch
			{
			}
		}


		//public void PutIntoEjectQueue(byte titleGroupId, string titleIssueDate, uint bundleId, byte ablId, byte code, EnumBundleInRoute bundleSq)
		//{
		//    try
		//    {
		//        TitleGroupId = titleGroupId;
		//        IssueDateString = titleIssueDate;
		//        BundleEjectedFromABL(bundleId, ablId, code, bundleSq);
		//    }
		//    catch
		//    {
		//    }
		//}

		public bool BundleEjectedFromABL(uint bundleId, byte ablId, byte code, 
			ushort RouteSortIndex, ushort BundleSortIndex, EnumBundleInRoute bundleSq, bool isNoTopsheet, bool isSTDBundle, byte routeModeId)
		{
			bool result = false;
			try
			{
				if (bundleId >= 10000)
				{
					PZFLastEjectedBundleId = bundleId;

					TopSheetItem queueItem = new TopSheetItem
					{
						BundleId = bundleId,
						AblId = ablId,
						RouteSortIndex = RouteSortIndex,
						BundleSortIndex = BundleSortIndex,
						FirstLastBundle = bundleSq,
						IsNoTopsheet = isNoTopsheet,
						IsSTDBundle = isSTDBundle,
						RouteModeId = routeModeId,
						HandledCode = code
					};

					//if (bundleSq == EnumBundleInRoute.FirstBundle || isNoTopsheet)
					lock(_lock_topsheetFirstLastBundleQueue)
					{
						_topSheetFirstBundleQueue.Add(queueItem);
						AddToLog(LogLevels.Normal, "Bundle: " + queueItem.BundleId + 
							" Send to FirstBundle Queue, Bundle Sq: " + queueItem.FirstLastBundle + 
							", IsNoTopsheet: " + queueItem.IsNoTopsheet +
							", IsSTDBundle: " + queueItem.IsSTDBundle +
							", RouteModeId: " + queueItem.RouteModeId);
					}

					//if (!isNoTopsheet || (queueItem.BundleId >= 50000))
					//if (_topSheetLineTableId == 2)
					//{
					PZFIcpGetOrAddTelegram(PZFIcpTelegramTypes.SendEjectedAblBundleIdNew, queueItem, true);
					//}
					//else
					//{
					//    PZFIcpGetOrAddTelegram(PZFIcpTelegramTypes.SendEjectedAblBundleId, queueItem, true);

					//}

					AddToLog(LogLevels.Normal, "Bundle: " + queueItem.BundleId + 
						" Ejected From ABL: " + queueItem.AblId + 
						" IsFirstBundle: " + bundleSq + 
						" RouteSortIndex: " + queueItem.RouteSortIndex + 
						" BundleSortIndex: " + queueItem.BundleSortIndex + 
						" IsNoTopsheet: " + queueItem.IsNoTopsheet + 
						" IsSTDBundle: " + queueItem.IsSTDBundle + 
						" RouteModeId: " + queueItem.RouteModeId);

					result = true;
				}
			}
			catch
			{
			}
			return result;
		}

		//public void SendSetupBA(List<byte> bl)
		//{
		//    IcpGetOrAddTelegramHauk(IcpTelegramTypes.SendSetup, bl, true);
		//}

		//public void SendTestSignalToBA(List<byte> list)
		//{
		//    IcpGetOrAddTelegramHauk(IcpTelegramTypes.SendSignal, list, true);
		//}

		//private byte[] IcpGetOrAddTelegramHauk(IcpTelegramTypes icpTelegramType, object oParameters, bool doAddToList)
		//{
		//    List<byte> haukByteList = (List<byte>)oParameters;
		//    byte[] result = null;
		//    byte pcStatus = _pcStatus;// 2;
		//    List<byte> byteList;

		//    int size;
		//    byte sizeL;
		//    byte sizeH;

		//    //byte printerStatus1 = _printer[0] != null ? (byte)1 : (byte)0;
		//    //byte printerStatus2 = _printer[1] != null ? (byte)1 : (byte)0;

		//    if (_tcpClientIcp != null && _tcpClientIcp.Client != null && _tcpClientIcp.Client.Connected)
		//    {
		//        if (_telegramId < 99)
		//        {
		//            ++_telegramId;
		//        }
		//        else
		//        {
		//            _telegramId = 1;
		//        }

		//        switch (icpTelegramType)
		//        {
		//            case IcpTelegramTypes.SendSignal: //G
		//                size = 7 + 2;
		//                sizeL = (byte)(size % 256);
		//                sizeH = (byte)(size / 256);
		//                byteList = new List<byte>
		//                {
		//                    SOH,
		//                    _telegramId,
		//                    pcStatus,
		//                    (byte)'G',
		//                    sizeL,
		//                    sizeH,
		//                    haukByteList[0],
		//                    haukByteList[1],
		//                    EOT
		//                };
		//                result = byteList.ToArray();
		//                break;

		//            case IcpTelegramTypes.SendSetup://D

		//                size = 7 + 64;
		//                sizeL = (byte)(size % 256);
		//                sizeH = (byte)(size / 256);
		//                byteList = new List<byte>
		//                {
		//                    SOH,
		//                    _telegramId,
		//                    pcStatus,
		//                    (byte)'D',
		//                    sizeL,
		//                    sizeH
		//                };

		//                foreach (byte b in haukByteList)
		//                {
		//                    byteList.Add(b);
		//                    Console.WriteLine(b);
		//                }

		//                byteList.Add(EOT);

		//                result = byteList.ToArray();
		//                break;

		//        }

		//        if (doAddToList)
		//        {
		//            IcpAddTelegram(result);
		//        }
		//    }
		  
		//    return result;
		//}

		//private void IcpAddTelegram(byte[] telegram)
		//{
		//    if (telegram != null && telegram.Length > 0)
		//    {
		//        lock (_lockTelegramListIcp)
		//        {
		//            _telegramListIcp.Add(telegram);
		//        }
		//    }
		//}

		public bool PrintBundle(TopSheetItem topSheetItem)//uint bundleId, uint bundleIndex)
		{
			//bool result = PrintBundleTopSheet(bundleId, bundleIndex, false, TopSheetTypes.NORMAL);
			bool result = PrintBundleTopSheet(topSheetItem, false);

			return result;
		}

		private bool PrintBundleTopSheet( TopSheetItem topSheetItem,//uint bundleId, uint bundleIndex, 
			bool removeTracking)
		{
			bool result = false;
			_lastLCCMessage = DateTime.Now;
			AddToLog(LogLevels.Normal, "PrintBundleTopSheet - TitleGroupId:"+ TitleGroupId.ToString() +
				" - BundleId " + topSheetItem.BundleId.ToString() +
				" - LableState " + topSheetItem.LabelState.ToString() +
				" - BundleIndex " + topSheetItem.BundleIndex.ToString());

			try
			{
				if (removeTracking)
				{
					topSheetItem.BundleIndex = 1;
				}

				lock (_lock_topSheetQueue) 
				{
					AddToLog(LogLevels.Normal, "PrintBundleTopSheet - TopSheetQueue:" + _topSheetQueue.Count.ToString() +
						" - Bundle Id: " + topSheetItem.BundleId.ToString() +
						" - Label State: " + topSheetItem.LabelState.ToString() +
						" - BundleIndex: " + topSheetItem.BundleIndex.ToString());

					for (int i = _topSheetQueue.Count - 1; i >= 0; i--)
					{
						if (_topSheetQueue[i].BundleId == topSheetItem.BundleId && topSheetItem.TopSheetType != TopSheetTypes.WHITEPAPER)
						{
							string sql = string.Format("exec UniStack_TopSheetBundleAction_SPH {0}, {1}, {2},'{3}', {4}, {5}, {6}, {7},{8}"
								, _gripperLineTableId
								, _topSheetLineTableId
								, TitleGroupId
								, IssueDateString
								, _topSheetQueue[i].BundleId
								, _topSheetQueue[i].BundleIndex
								, (int)TopSheetActionEnum.AddedBundleAlreadyExistsQueue
								, -1
								, -1
								);

							DbAddCommandToQueue(DbCommandTypes.ExecuteNonQuery, sql, null, true);

							AddToLog(LogLevels.Warning,
								"PrintBundle remove existing from _topSheetQueue" +
								" - TitleGroup:"+ TitleGroupId.ToString() +
								" - BundleIndex:" + _topSheetQueue[i].BundleIndex.ToString() +
								" - BundleId: " + _topSheetQueue[i].BundleId.ToString());

							_topSheetQueue.RemoveAt(i);
						}
					}
				}

				lock (_lock_topSheetPrinted)
				{
					for (int i = _topSheetPrinted.Count - 1; i >= 0; i--)
					{
						if (_topSheetPrinted[i].BundleId == topSheetItem.BundleId && 
							topSheetItem.TopSheetType != TopSheetTypes.WHITEPAPER)
						{
							string sql = string.Format("exec UniStack_TopSheetBundleAction_SPH {0}, {1}, {2},'{3}', {4}, {5}, {6}, {7}, {8}"
								, _gripperLineTableId
								, _topSheetLineTableId
								, TitleGroupId
								, IssueDateString
								, _topSheetPrinted[i].BundleId
								, _topSheetPrinted[i].BundleIndex
								, (int)TopSheetActionEnum.AddedBundleAlreadyExistsPrinted
								, -1
								, -1
								);

							DbAddCommandToQueue(DbCommandTypes.ExecuteNonQuery, sql, null, true);

							AddToLog(LogLevels.Warning,
								"ERROR: PrintBundle remove existing from _topSheetPrinted" +
								" - TitleGroup: " + TitleGroupId.ToString() + 
								" - Bundleid: " + _topSheetPrinted[i].BundleId.ToString());

							_topSheetPrinted.RemoveAt(i);
						}
					}
				}

				lock (_lock_topSheetQueue)
				{
					_topSheetQueue.Add(topSheetItem);

					AddToLog(LogLevels.Debug, 
						"PrintBundle add to TopSheetQueue - TitleGroup: " + TitleGroupId.ToString() + 
						" - BundleId: " + topSheetItem.BundleId.ToString() +
						" - BundleIndex: " + topSheetItem.BundleIndex.ToString() +
						" - LabelState: " + topSheetItem.LabelState.ToString() +
						" - Topsheet Queue Count: " + _topSheetQueue.Count.ToString());

					while (_topSheetQueue.Count > 130) //To be fixed later. Jie 20181019
					{
						topSheetItem = _topSheetQueue[0];

						string sql = string.Format("exec UniStack_TopSheetBundleAction_SPH {0}, {1}, {2},'{3}', {4}, {5}, {6}, {7},{8}"
							, _gripperLineTableId
							, _topSheetLineTableId
							, TitleGroupId
							, IssueDateString
							, topSheetItem.BundleId
							, topSheetItem.BundleIndex
							, (int)TopSheetActionEnum.PrintQueueFullRemoved
							, -1
							, -1
							);

						DbAddCommandToQueue(DbCommandTypes.ExecuteNonQuery, sql, null, true);

						AddToLog(LogLevels.Warning,
							"ERROR: PrintBundle remove from _topSheetQueue (full) - TitleGroup: " + TitleGroupId.ToString() +" - Bundleid: "
							+ topSheetItem.BundleId.ToString());

						_topSheetQueue.RemoveAt(0);
						PrintBundleRemovedFromQueue(topSheetItem);
					}
				}

				lock (_lock_topSheetPrinted)
				{
					while (_topSheetPrinted.Count > 130)
					{
						topSheetItem = _topSheetPrinted[0];

						string sql = string.Format("exec UniStack_TopSheetBundleAction_SPH {0}, {1}, {2},'{3}', {4}, {5}, {6}, {7},{8}"
							, _gripperLineTableId
							, _topSheetLineTableId
							, TitleGroupId
							, IssueDateString
							, topSheetItem.BundleId
							, topSheetItem.BundleIndex
							, (int)TopSheetActionEnum.PrintedListFullRemoved
							, -1
							, topSheetItem.PrinterNumber - 1
							);

						DbAddCommandToQueue(DbCommandTypes.ExecuteNonQuery, sql, null, true);

						AddToLog(LogLevels.Debug,
							"ERROR: PrintBundle remove from _topSheetPrinted (full) - TitleGroup:" + TitleGroupId.ToString()+" bundleid: "
							+ topSheetItem.BundleId.ToString());

						_topSheetPrinted.RemoveAt(0);
						//PrintBundleRemovedFromQueue(queueItem.BundleId);
					}
				}
			}
			catch (Exception ex)
			{
				AddToLog(ex);
			}

			return result;
		}

		public bool PrintBundleRemovedFromQueue(TopSheetItem _topsheetItem)
		{
			bool result = false;

			byte printerNumber = 0;
			uint bundleIndex = 1;
			bool isReady1 = PrintronixPrinter[0] != null;//&& _printer[0].PrinterStatus != null && _printer[0].PrinterStatus.IsReadyToPrint;
			bool isReady2 = UseTwoPrinters && PrintronixPrinter[1] != null;//&& _printer[1].PrinterStatus != null && _printer[1].PrinterStatus.IsReadyToPrint;
			try
			{
				if (isReady1)
				{
					printerNumber = 1;
				}
				else if (isReady2)
				{
					printerNumber = 2;
				}

				if (printerNumber > 0)
				{
					//string documentName = "TopSheet(" + bundleIndex.ToString()
					//    + ") " + bundleId.ToString();

					//PrintTopSheetPrintronix(printerNumber ,bundleId, topSheetType);

					LabelItem.printerResultTypes printResult = PrintLogic(_topsheetItem);
					//TsBundleQueue.Enqueue(_topsheetItem);

					//PrintManualTopsheet(printerNumber, documentName, bundleId, topSheetType);

					if (printResult == LabelItem.printerResultTypes.Ok)
					{
						result = true;

						string sql = string.Format("exec UniStack_TopSheetBundleAction_SPH {0}, {1}, {2},'{3}', {4}, {5}, {6}, {7},{8}"
							, _gripperLineTableId
							, _topSheetLineTableId
							, _topsheetItem.TitleGroupId
							, _topsheetItem.IssueDateString
							, _topsheetItem.BundleId
							, bundleIndex
							, (int)TopSheetActionEnum.AutomaticPrintRemovedFromQueue
							, -1
							, printerNumber - 1
							);

						DbAddCommandToQueue(DbCommandTypes.ExecuteNonQuery, sql, null, true);

						AddToLog(LogLevels.Normal,
							"PrinterControl print ok " +
							"-TitleGroup:" + _topsheetItem.TitleGroupId.ToString() +
							"-Bundleid: " + _topsheetItem.BundleId.ToString() + 
							"-BundleIndex: " + bundleIndex.ToString());

					}
					else
					{
						string sql = string.Format("exec UniStack_TopSheetBundleAction_SPH {0}, {1}, {2},'{3}', {4}, {5}, {6}, {7},{8}"
							, _gripperLineTableId
							, _topSheetLineTableId
							, _topsheetItem.TitleGroupId
							, _topsheetItem.IssueDateString
							, _topsheetItem.BundleId
							, bundleIndex
							, (int)TopSheetActionEnum.UnableToPrint
							, -1
							, printerNumber - 1
							);

						DbAddCommandToQueue(DbCommandTypes.ExecuteNonQuery, sql, null, true);

						AddToLog(LogLevels.Warning,
							"ERROR: PrinterControl print failed " +
							"-TitleGroup: " + _topsheetItem.TitleGroupId.ToString() +
							"-Bundleid: "+ _topsheetItem.BundleId.ToString() +
							"-BundleIndex: " + bundleIndex.ToString());
					}
				}
			}
			catch
			{
			}

			return result;
		}

		//public bool BundleEjectedDoNotTrack(uint bundleId)
		//{
		//    bool result = false;
		//    try
		//    {
		//        if (bundleId >= 10000)
		//        {
		//            LastEjectedBundleId = bundleId;

		//            TopSheetItem queueItem = new TopSheetItem
		//            {
		//                BundleId = bundleId,
		//                BundleIndex = 1,
		//                PrinterNumber = 0
		//            };
		//            IcpGetOrAddTelegram(IcpTelegramTypes.SendEjectedBundleIndex, queueItem, true);
		//            result = true;
		//        }
		//    }
		//    catch
		//    {
		//    }

		//    return result;
		//}

		public bool BundleEjected(TopSheetItem BundleItem)//, uint bundleIndex)
		{
			bool result = false;

			try
			{
				BundleItem.FirstLastBundle = EnumBundleInRoute.Normal;
				BundleItem.BundleSortIndex = 0;
				BundleItem.RouteSortIndex = 0;
				BundleItem.IsNoTopsheet = false;

				lock(_lock_topsheetFirstLastBundleQueue)
				{
					if (_topSheetFirstBundleQueue.Count > 0)
					{
						for (int i = 0; i < _topSheetFirstBundleQueue.Count; i++)
						{
							if (BundleItem.BundleId == _topSheetFirstBundleQueue[i].BundleId)
							{
								BundleItem.FirstLastBundle = _topSheetFirstBundleQueue[i].FirstLastBundle;
								BundleItem.IsNoTopsheet = _topSheetFirstBundleQueue[i].IsNoTopsheet;
								BundleItem.RouteSortIndex = _topSheetFirstBundleQueue[i].RouteSortIndex;
								BundleItem.BundleSortIndex = _topSheetFirstBundleQueue[i].BundleSortIndex;
								BundleItem.IsSTDBundle = _topSheetFirstBundleQueue[i].IsSTDBundle;
								BundleItem.RouteModeId = _topSheetFirstBundleQueue[i].RouteModeId;


								_topSheetFirstBundleQueue.RemoveAt(i);
							}
						}
					}
				}


				lock (_lock_noTopsheetBundleQueue)
				{
					if (BundleItem.IsNoTopsheet)
					{
						_noTopSheetBundleQueue.Add(BundleItem.BundleId);
					}
				}

				IcpGetOrAddTelegram(IcpTelegramTypes.SendEjectedBundleIndex, BundleItem, true);

				AddToLog(LogLevels.Debug, "BundleEjected TitleGroup: " + TitleGroupId.ToString() +
					" - BundleId: " + BundleItem.BundleId.ToString() + 
					" - BundleIndex: " + BundleItem.BundleIndex.ToString() +
					" - RouteSortIndex: " + BundleItem.RouteSortIndex.ToString() +
					" - BundleSortIndex: " + BundleItem.BundleSortIndex.ToString() +
					" - IsStandardBundle: " + BundleItem.IsSTDBundle.ToString() +
					" - RouteModeId: " + BundleItem.RouteModeId.ToString() +
					" - LableState: " + BundleItem.LabelState.ToString());

				result = true;
			}
			catch(Exception ex)
			{
				AddToLog(LogLevels.Error, "BundleEjected Error: " + ex.ToString() +
					" - TitleGroup: " + TitleGroupId.ToString() +
					" - BundleId: " + BundleItem.BundleId.ToString() +
					" - BundleIndex: " + BundleItem.BundleIndex.ToString() +
					" - RouteSortIndex: " + BundleItem.RouteSortIndex.ToString() +
					" - BundleSortIndex: " + BundleItem.BundleSortIndex.ToString() +
					" - IsStandardBundle: " + BundleItem.IsSTDBundle.ToString() +
					" - LableState: " + BundleItem.LabelState.ToString());
			}

			return result;
		}

		//public bool BundleEjected(uint bundleId, uint bundleIndex)
		//{
		//    bool result = false;

		//    try
		//    {
		//        TopSheetItem queueItem = new TopSheetItem
		//        {
		//            AddedTime = DateTime.Now,
		//            BundleId = bundleId,
		//            BundleIndex = bundleIndex,
		//            EjectedTime = DateTime.Now,
		//            HandledCode = -1,
		//            PrintedTime = DateTime.MinValue,
		//            LabelState = 0,
		//            TopSheetType = TopSheetTypes.NORMAL
		//        };

		//        IcpGetOrAddTelegram(IcpTelegramTypes.SendEjectedBundleIndex, queueItem, true);

		//        AddToLog(LogLevels.Debug, "BundleEjected TitleGroup: " + TitleGroupId.ToString() + 
		//            " - BundleId: " + bundleId.ToString() + " - BundleIndex: " + bundleIndex.ToString() + 
		//            " - LableState: " + queueItem.LabelState.ToString());

		//        result = true;

		//    }
		//    catch
		//    {
		//    }

		//    return result;
		//}

		//public void ClearPrintQueueWaiting()
		//{
		//    try
		//    {
		//        lock (_lock_topSheetQueue)
		//        {
		//            for (int i = _topSheetQueue.Count - 1; i >= 0; i--)
		//            {
		//                if (_topSheetQueue[i].PrintedTime == DateTime.MinValue)
		//                {
		//                    _topSheetQueue.RemoveAt(i);
		//                }
		//            }
		//        }

		//    }
		//    catch
		//    {
		//    }
		//}

		//public void RemoveFromPrintQueueWaiting(uint bundleId)
		//{
		//    try
		//    {
		//        lock (_lock_topSheetQueue)
		//        {
		//            if (_topSheetQueue.Count > 0)
		//            {
		//                bool isFound = false;
		//                int index = -1;
		//                while (!isFound && ++index < _topSheetQueue.Count)
		//                {
		//                    if (_topSheetQueue[index].PrintedTime == DateTime.MinValue)
		//                    {
		//                        if (_topSheetQueue[index].BundleId == bundleId)
		//                        {
		//                            _topSheetQueue.RemoveAt(index);
		//                            isFound = true;
		//                        }
		//                    }
		//                }
		//            }
		//        }

		//    }
		//    catch
		//    {
		//    }
		//}

		public void ResetApplicator(bool clearQueues)
		{
			try
			{
				_lastResetTime = DateTime.Now;
				//_resetDone = false;
				//if (!_resetDone)
				{
					if (PrintronixPrinter[0] != null)//&& _printer[0].PrinterStatus != null)
					{
						PrintronixPrinter[0]._printerStatus.ReadyTime = DateTime.Now;
					}

					if (PrintronixPrinter[1] != null)// && _printer[1].PrinterStatus != null)
					{
						PrintronixPrinter[0]._printerStatus.ReadyTime = DateTime.Now;
					}

					if (IcpConnectionState == IcpConnectionStates.Connected)
					{
						IcpGetOrAddTelegram(IcpTelegramTypes.Reset, null, true);
						
						AddToLog("ResetApplicator STB&DBH! Time: " + _lastResetTime);// Clear TsBundleQueueAvoidTwoTopAtSametime Counter: " + TsBundleQueueAvoidTwoTopAtSametime.Count);
						//TsBundleQueueAvoidTwoTopAtSametime.Clear();
					}
					//_resetDone = true;
				}

				if (clearQueues)
				{
					//_topSheetPrinted.Clear();
					//_topSheetQueue.Clear();

					lock (_lock_topSheetPrinted)
					{
						while (_topSheetPrinted.Count > 0)
						{
							TopSheetItem tempItem = _topSheetPrinted[0];
							_topSheetPrinted.RemoveAt(0);

							AddToLog("ResetApplicator " +
								"-TitleGroup: " + TitleGroupId.ToString() +
								"-Bundleid: " + tempItem.BundleId.ToString() + 
								"-BundleIndex: " + tempItem.BundleIndex.ToString());

							lock (_lock_TsBundleQueuePrintedResume)
							{
								//int tempCount = TsBundleQueuePrintedResume.Count;

								for (int i = 0; i < TsBundleQueuePrintedResume.Count; i++)
								{
									if (tempItem.BundleId == TsBundleQueuePrintedResume[i].BundleId)
									{
										TsBundleQueuePrintedResume.RemoveAt(i);

										AddToLog(LogLevels.Debug,
											"Bundle: " + tempItem.BundleId.ToString()
											+ " remove from TsBundleQueuePrintedResume Due To Reset, Count: "
											+ TsBundleQueuePrintedResume.Count.ToString());

										break;
									}
								}
							}

							string sql = string.Format("exec UniStack_TopSheetBundleAction_SPH {0}, {1}, {2},'{3}', {4}, {5}, {6}, {7},{8}"
								 , _gripperLineTableId
								 , _topSheetLineTableId
								 , TitleGroupId
								 , IssueDateString
								 , tempItem.BundleId
								 , tempItem.BundleIndex
								 , (int)TopSheetActionEnum.Reset// 13//reset
								 , -1
								 , tempItem.PrinterNumber - 1
								 );

							DbAddCommandToQueue(DbCommandTypes.ExecuteNonQuery, sql, null, true);

							//PrintBundleRemovedFromQueue(tempItem.BundleId, tempItem.TopSheetType); // 11-06  cheng
						}
					}

					lock (_lock_TsBundleQueueAvoidTwoTopAtSametime)
					{
						if (TsBundleQueueAvoidTwoTopAtSametime.Count > 0)
						{
							AddToLog("Reset Applicator" +
								" Clear TsBundleQueueAvoidTwoTopAtSametime Counter: " + TsBundleQueueAvoidTwoTopAtSametime.Count +
								" to Print TopsheetQueue Topsheet");

							TsBundleQueueAvoidTwoTopAtSametime.Clear();
						}
					}

					lock (_lock_topSheetQueue)
					{ 
						AddToLog("ResetApplicator Added Ejected Bundle From _topSheetQueue, _topSheetQueue.count: " + _topSheetQueue.Count);
						while (_topSheetQueue.Count > 0)
						{
							TopSheetItem tempItem = _topSheetQueue[0];

							if (tempItem.BundleId != _lastBundleIdResetCheck)
							{
								if (tempItem.BundleId >= 10000 && tempItem.BundleId < 50000)
								{
									tempItem.LabelState = 51;
									tempItem.TitleGroupId = TitleGroupId;
									tempItem.IssueDateString = IssueDateString;
									tempItem.TopSheetType = TopSheetTypes.NORMAL;
									//_topSheetEjectedQueue.Add(tempItem); // Top Print
									
									TsBundleQueue.Enqueue(tempItem);
									AddToLog("ResetApplicator Added Ejected Bundle: "+ tempItem.BundleId.ToString() +
											 " to TsBundleQueue, Lable State: " + tempItem.LabelState.ToString());
								}
								_lastBundleIdResetCheck = tempItem.BundleId;
							}

							_topSheetQueue.RemoveAt(0);

							string sql = string.Format("exec UniStack_TopSheetBundleAction_SPH {0}, {1}, {2},'{3}', {4}, {5}, {6}, {7}, {8}"
								 , _gripperLineTableId
								 , _topSheetLineTableId
								 , TitleGroupId
								 , IssueDateString
								 , tempItem.BundleId
								 , tempItem.BundleIndex
								 , (int)TopSheetActionEnum.Reset//13//reset
								 , -1
								 , tempItem.PrinterNumber - 1
								 );

							DbAddCommandToQueue(DbCommandTypes.ExecuteNonQuery, sql, null, true);
							//PrintBundleRemovedFromQueue(tempItem.BundleId, tempItem.TopSheetType);
						}
					}


					lock (_lock_TsBundleQueue)
					{
						if (TsBundleQueue.Count < 2 &&
						//TsBundleQueueAvoidTwoTopAtSametime.Count > 0 &&
						IcpConnectionState == IcpConnectionStates.Connected)
						{
							if (IsProductionStarted)
							{
								TopSheetItem restItem = new TopSheetItem
								{
									BundleId = 65500,
									BundleIndex = 0,
									PrinterNumber = DefaultPrinter,
									TopSheetType = TopSheetTypes.WHITEPAPER,
									LabelState = 50
								};

								TsBundleQueue.Enqueue(restItem);
								TsBundleQueue.Enqueue(restItem);

								AddToLog(LogLevels.Debug, "ResetApplicator Added Two WhitePaper, TsBundleQueue Count: " + TsBundleQueue.Count);
							}
						}
						else
						{
							AddToLog(LogLevels.Debug, "ResetApplicator No Need Added Two WhitePaper, " +
								" - TsBundleQueue Count: " + TsBundleQueue.Count +
								" - TsBundleQueueAvoidTwoTopAtSametime Count: " + TsBundleQueueAvoidTwoTopAtSametime.Count +
								" - IcpConnectionState: " + IcpConnectionState +
								" - IsProductionStarted: " + IsProductionStarted);
						}
					}
				}
			}
			catch (Exception ex)
			{
				AddToLog("ResetApplicator Failed: " + ex.ToString());
			}
		}

		public bool TestPrint(byte printerNumber)
		{
			bool result = false;
			LabelItem.printerResultTypes printResult = LabelItem.printerResultTypes.Unknown;

			try
			{
				if (PrintronixPrinter[printerNumber - 1] != null)
				{
					TopSheetItem testItem = new TopSheetItem
					{
						BundleId = 65500,
						BundleIndex = 0,
						PrinterNumber = printerNumber,
						TopSheetType = TopSheetTypes.WHITEPAPER,
						LabelState = 50
					};

					if (PrintronixPrinter[printerNumber - 1] != null)
					{
						if (PrintronixPrinter[printerNumber - 1]._printerStatus != null)
						{
							//while (PrintronixPrinter[printerNumber - 1]._printerStatus.PrintingStatus != 3)
							//{
							//    AddToLog("Test Print Bundle: " + testItem.BundleId.ToString() +
							//        " PrintingStatus is "+ PrintronixPrinter[printerNumber - 1]._printerStatus.PrintingStatus + ", Sleep 10 ms to Avoid Effect Next One!");

							//    if (!PrintronixPrinter[printerNumber - 1]._printerStatus.StatusMessage.ToLower().Contains("online"))
							//    {
							//        break;
							//    }

							//    Thread.Sleep(10);
							//}

							//Printing Status: 3(idle) -> 4(printing) -> 3(idle)
							if (PrintronixPrinter[printerNumber - 1]._printerStatus.PrintingStatus == 3)
							{
								IcpGetOrAddTelegram(IcpTelegramTypes.StartPaperTransport, printerNumber, true);
								IcpGetOrAddTelegram(IcpTelegramTypes.SendPrintedBundleIndex, testItem, true);
								printResult = PrintTopSheetPrintronix(printerNumber, testItem);

								AddToLog("Test Printer: " + printerNumber + " - Lable State: " + testItem.LabelState.ToString());
								Thread.Sleep(2500);
							}

							AddToLog("PrinterStatusList After Printing: " + PrintronixPrinter[printerNumber - 1]._printerStatus.PrintingStatus);

							while (PrintronixPrinter[printerNumber - 1]._printerStatus.PrintingStatus == 4)
							{
								//PrintronixPrinter[printerNumber - 1].IsReadyToPrint = false;
								//AddToLog("PrinterStatusList2: " + PrintronixPrinter[printerNumber - 1]._printerStatus.PrintingStatus);
								Thread.Sleep(10);
								if (!PrintronixPrinter[printerNumber - 1]._printerStatus.StatusMessage.ToLower().Contains("online"))
								{
									break;
								}
							}

							if (printResult == LabelItem.printerResultTypes.Ok)
							{
								AddToLog("PrinterStatusList Before Cutting: " + PrintronixPrinter[printerNumber - 1]._printerStatus.PrintingStatus);
								IcpGetOrAddTelegram(IcpTelegramTypes.EnableCutter, printerNumber, true);
								Thread.Sleep(500);
							}
							else
							{
								AddToLog("Printer Failture: " + printResult);
							}

							//PrintronixPrinter[printerNumber - 1].IsReadyToPrint = true;

						}
						else
						{
							AddToLog("Printer Already Running!");
						}
					}
					else
					{
						AddToLog(LogLevels.Warning, "PrintronixPrinter is NULL, Bundle: " + testItem.BundleId.ToString().ToString() +
								   " - Printer " + printerNumber.ToString());
					}

					if (printResult == LabelItem.printerResultTypes.Ok)
					{
						result = true;
					}
				}

				AddToLog("PrinterStatus Result: " + printResult.ToString());
			}
			catch
			{
				AddToLog(LogLevels.Error, "Test Print Error Result: " + printResult.ToString());
				result = false;
			}
			
			return result;
		}

		//public void RequestEnableLogFile(bool doEnable)
		//{
		//    _enableLogFile = doEnable;
		//}

		//public void RequestEnableLogDebug(bool doEnable)
		//{
		//    _enableLogDebug = doEnable;
		//}

		#endregion

		#region Icp communication
		uint lastBundleIdOnSTB = 0;
		private void HandleBundleMessage(BundleHandledStruct bundleHandled)
		{
			try
			{
				uint bundleIndex = bundleHandled.iConveyorBundleIndex;
				uint bundleId = bundleHandled.bundleId;
				string sql;
				//TopSheetItem topSheetItem = GetTopSheetItemPrinted(bundleId, false);//, out listIndex);

				AddToLog("HandleBundleMessage" +
					" -TitleGroup: " + TitleGroupId.ToString() +
					" -Bundleid: " + bundleId.ToString() +
					" -BbundleIndex: " + bundleIndex.ToString() +
					" -cCode: " + bundleHandled.cCode.ToString());

				if (bundleHandled.cCode == 0)   //labeling ok
				{
					AddToLog("IcpControl labeled ok bundleid: " + bundleId.ToString() +
						" - bundleIndex: " + bundleIndex.ToString() +
						" - cCode: " + bundleHandled.cCode +
						" - TimeStamp: " + DateTime.Now);

					//SaveBundleInfoToSTBScreen(bundleId, TopSheetLineTableId);

					lock (_lock_topSheetPrinted)
					{
						for (int j = _topSheetPrinted.Count - 1; j >= 0; j--)
						{
							TopSheetItem tempItem = _topSheetPrinted[j];
							if (tempItem.BundleIndex >= 11 && tempItem.BundleIndex <= bundleIndex)
							{
								//sql = string.Format("exec UniStack_TopSheetBundleAction {0}, {1}, {2}, {3}, {4}, {5}, {6}"
								//    , _gripperLineTableId
								//    , _topSheetLineTableId
								//    , tempItem.BundleId
								//    , tempItem.BundleIndex
								//    , (int)TopSheetActionEnum.BundleNotReportedPrintedList//16
								//    , -1
								//    , tempItem.PrinterNumber - 1
								//    );//bundle not reported

								//DbAddCommandToQueue(DbCommandTypes.ExecuteNonQuery, sql, null, true);

								//AddToLog(LogLevels.Warning,
								//    "IcpControl not reported (printed) bundleid: "
								//    + tempItem.BundleId.ToString()
								//    + " - bundleIndex: " + tempItem.BundleIndex.ToString());

								_topSheetPrinted.RemoveAt(j);

							}
						}
					}

					lock (_lock_topSheetQueue)
					{
						for (int j = _topSheetQueue.Count - 1; j >= 0; j--)
						{
							TopSheetItem tempItem = _topSheetQueue[j];
							if (tempItem.BundleIndex >= 11 && tempItem.BundleIndex <= bundleIndex)
							{
								//sql = string.Format("exec UniStack_TopSheetBundleAction {0}, {1}, {2}, {3}, {4}, {5}, {6}"
								//    , _gripperLineTableId
								//    , _topSheetLineTableId
								//    , tempItem.BundleId
								//    , tempItem.BundleIndex
								//    , (int)TopSheetActionEnum.BundleNotReportedPrintQueue//12
								//    , -1
								//    , tempItem.PrinterNumber - 1
								//    );//bundle not reported

								//DbAddCommandToQueue(DbCommandTypes.ExecuteNonQuery, sql, null, true);

								//AddToLog(LogLevels.Warning,
								//    "ERROR: IcpControl not reported (not printed) bundleid: "
								//    + tempItem.BundleId.ToString()
								//    + " - bundleIndex: " + tempItem.BundleIndex.ToString());

								_topSheetQueue.RemoveAt(j);
							}
						}
					}

					lock (_lock_TsBundleQueueAvoidTwoTopAtSametime)
					{
						//int tempCount = TsBundleQueueAvoidTwoTopAtSametime.Count;
						for (int i = 0; i < TsBundleQueueAvoidTwoTopAtSametime.Count; i++)
						{
							if (bundleId == TsBundleQueueAvoidTwoTopAtSametime[i].BundleId)
							{
								TsBundleQueueAvoidTwoTopAtSametime.RemoveAt(i);

								AddToLog(LogLevels.Debug,
									"Bundle: " + bundleId.ToString()
									+ " remove from TsBundleQueueAvoidTwoTopAtSametime Due To Labeled, Count: "
									+ TsBundleQueueAvoidTwoTopAtSametime.Count.ToString());
								break;
							}
						}
					}

					lock (_lock_TsBundleQueuePrintedResume)
					{
						//int tempCount = TsBundleQueuePrintedResume.Count;
						for (int i = 0; i < TsBundleQueuePrintedResume.Count; i++)
						{
							if (bundleId == TsBundleQueuePrintedResume[i].BundleId)
							{
								TsBundleQueuePrintedResume.RemoveAt(i);

								AddToLog(LogLevels.Debug,
									"Bundle: " + bundleId.ToString()
									+ " remove from TsBundleQueuePrintedResume Due To Labeled, Count: "
									+ TsBundleQueuePrintedResume.Count.ToString());

								break;
							}
						}
					}


					//GetTopSheetItemPrinted(bundleIndex, true);
				}
				else if (bundleHandled.cCode == 5) // Reject on DBH
				{
					AddToLog("HandleBundleMessage RejectOnDBH TitleGroup: " + TitleGroupId.ToString() + " bundleid: "
						+ bundleId.ToString() + " - bundleIndex: " + bundleIndex.ToString()
						+ " - cCode: " + bundleHandled.cCode.ToString());
					GetTopSheetItemPrinted(bundleIndex, true);

					lock (_lock_TsBundleQueueAvoidTwoTopAtSametime)
					{
						//int tempCount = TsBundleQueueAvoidTwoTopAtSametime.Count;
						for (int i = 0; i < TsBundleQueueAvoidTwoTopAtSametime.Count; i++)
						{
							if (bundleId == TsBundleQueueAvoidTwoTopAtSametime[i].BundleId)
							{
								TsBundleQueueAvoidTwoTopAtSametime.RemoveAt(i);

								AddToLog(LogLevels.Debug,
									" Bundle: " + bundleId.ToString() +
									" Remove from TsBundleQueueAvoidTwoTopAtSametime Due To Reject on DBH, Count: "
									+ TsBundleQueueAvoidTwoTopAtSametime.Count.ToString());

								break;
								//i = tempCount;// Only exec one time
							}
						}
					}


					lock (_lock_TsBundleQueuePrintedResume)
					{
						//int tempCount = TsBundleQueuePrintedResume.Count;

						for (int i = 0; i < TsBundleQueuePrintedResume.Count; i++)
						{
							if (bundleId == TsBundleQueuePrintedResume[i].BundleId)
							{
								TsBundleQueuePrintedResume.RemoveAt(i);

								AddToLog(LogLevels.Debug,
									"Bundle: " + bundleId.ToString()
									+ " remove from TsBundleQueuePrintedResume Due To Reject on DBH, Count: "
									+ TsBundleQueuePrintedResume.Count.ToString());
								break;
								//i = tempCount;
							}
						}
					}
				}
				else if (bundleHandled.cCode == 6) //Label Timeout
				{
					AddToLog("HandleBundleMessage Label Timout TitleGroup: " + TitleGroupId.ToString() + " bundleid: "
						+ bundleId.ToString() + " - bundleIndex: " + bundleIndex.ToString()
						+ " - cCode: " + bundleHandled.cCode.ToString());

					lock (_lock_TsBundleQueueAvoidTwoTopAtSametime)
					{
						//int tempCount = TsBundleQueueAvoidTwoTopAtSametime.Count;
						for (int i = 0; i < TsBundleQueueAvoidTwoTopAtSametime.Count; i++)
						{
							if (bundleId == TsBundleQueueAvoidTwoTopAtSametime[i].BundleId)
							{
								TsBundleQueueAvoidTwoTopAtSametime.RemoveAt(i);

								AddToLog(LogLevels.Debug,
									" Bundle: " + bundleId.ToString() +
									" Remove from TsBundleQueueAvoidTwoTopAtSametime Due To Label Timeout, Count: "
									+ TsBundleQueueAvoidTwoTopAtSametime.Count.ToString());
								break;
								//i = tempCount;// Only exec one time
							}
						}
					}

					lock (_lock_TsBundleQueuePrintedResume)
					{
						//int tempCount = TsBundleQueuePrintedResume.Count;

						for (int i = 0; i < TsBundleQueuePrintedResume.Count; i++)
						{
							if (bundleId == TsBundleQueuePrintedResume[i].BundleId)
							{
								TsBundleQueuePrintedResume.RemoveAt(i);

								AddToLog(LogLevels.Debug,
									"Bundle: " + bundleId.ToString()
									+ " remove from TsBundleQueuePrintedResume Due To Label Timeout, Count: "
									+ TsBundleQueuePrintedResume.Count.ToString());

								break;
							}
						}
					}
				}
				else if (bundleHandled.cCode == 8) // On STB Pos1
				{

					if (bundleId != lastBundleIdOnSTB)
					{
						TopSheetItem restItem = new TopSheetItem
						{
							BundleId = 65500,
							BundleIndex = 0,
							TopSheetType = TopSheetTypes.WHITEPAPER,
							LabelState = 50
						};

						if (lastBundleIdOnSTB > 10000 && lastBundleIdOnSTB < 50000 && bundleId >= 50000)// || bundleId < 10000)
						{
							lock (_lock_TsBundleQueue)
							{
								TsBundleQueue.Enqueue(restItem);
								TsBundleQueue.Enqueue(restItem);
							}

							AddToLog(LogLevels.Debug, "Bundle : " + bundleId + " Automatic Labeled Because Last Bundle is: " + lastBundleIdOnSTB);
						}

						if (!_noTopSheetBundleQueue.Contains(lastBundleIdOnSTB) && 
							 _noTopSheetBundleQueue.Contains(bundleId))
						{
							lock (_lock_TsBundleQueue)
							{
								TsBundleQueue.Enqueue(restItem);
								TsBundleQueue.Enqueue(restItem);
							}

							AddToLog(LogLevels.Debug, "Bundle : " + bundleId + " Automatic Labeled Because Topsheet/NoTopsheet Comes together");
						}

						AddToLog(LogLevels.Debug, "Cur Bundle : " + bundleId + " ;Last Bundle is: " + lastBundleIdOnSTB);
						lastBundleIdOnSTB = bundleId;
					}

					AddToLog("HandleBundleMessage OnSTB TitleGroup: " + TitleGroupId.ToString() + " bundleid: "
						+ bundleId.ToString() + " - LastBundleId: " + lastBundleIdOnSTB + " - bundleIndex: " + bundleIndex.ToString()
						+ " - cCode: " + bundleHandled.cCode.ToString());
				}
				else if (bundleHandled.cCode == 9) // Removed from STB
				{
					AddToLog("HandleBundleMessage remove from STB TitleGroup: " + TitleGroupId.ToString() + " bundleid: "
						+ bundleId.ToString() + " - bundleIndex: " + bundleIndex.ToString()
						+ " - PosId: " + bundleHandled.cPosition.ToString()
						+ " - cCode: " + bundleHandled.cCode.ToString());
				}
				else if (bundleHandled.cCode == 10) // Removed from PZF Table
				{
					AddToLog("HandleBundleMessage remove from PZFTable TitleGroup: " + TitleGroupId.ToString() + " bundleid: "
						+ bundleId.ToString() + " - bundleIndex: " + bundleIndex.ToString()
						+ " - cCode: " + bundleHandled.cCode.ToString());
				}
				else if (bundleHandled.cCode == 11) // Removed from ArmPosition
				{
					AddToLog("HandleBundleMessage manual remove from ArmPostion TitleGroup: " + TitleGroupId.ToString() + " bundleid: "
						+ bundleId.ToString() + " - bundleIndex: " + bundleIndex.ToString()
						+ " - cCode: " + bundleHandled.cCode.ToString());

					lock (_lock_TsBundleQueueAvoidTwoTopAtSametime)
					{
						//int tempCount = TsBundleQueueAvoidTwoTopAtSametime.Count;
						for (int i = 0; i < TsBundleQueueAvoidTwoTopAtSametime.Count; i++)
						{
							if (bundleId == TsBundleQueueAvoidTwoTopAtSametime[i].BundleId)
							{
								TsBundleQueueAvoidTwoTopAtSametime.RemoveAt(i);

								AddToLog(LogLevels.Debug,
									" Bundle: " + bundleId.ToString() +
									" Remove from TsBundleQueueAvoidTwoTopAtSametime Due To Label Timeout, Count: "
									+ TsBundleQueueAvoidTwoTopAtSametime.Count.ToString());
								break;
								//i = tempCount;// Only exec one time
							}
						}
					}

					lock (_lock_TsBundleQueuePrintedResume)
					{
						//int tempCount = TsBundleQueuePrintedResume.Count;

						for (int i = 0; i < TsBundleQueuePrintedResume.Count; i++)
						{
							if (bundleId == TsBundleQueuePrintedResume[i].BundleId)
							{
								TsBundleQueuePrintedResume.RemoveAt(i);

								AddToLog(LogLevels.Debug,
									"Bundle: " + bundleId.ToString()
									+ " remove from TsBundleQueuePrintedResume Due To Label Timeout, Count: "
									+ TsBundleQueuePrintedResume.Count.ToString());

								break;
							}
						}
					}
				}
				else if (bundleHandled.cCode == 19) // On Arm Position
				{
					SaveBundleInfoToSTBScreen(bundleId, TopSheetLineTableId);

					AddToLog("HandleBundleMessage On Arm Position TitleGroup: " + TitleGroupId.ToString() + " bundleid: "
						+ bundleId.ToString() + " - bundleIndex: " + bundleIndex.ToString()
						+ " - cCode: " + bundleHandled.cCode.ToString());
				}
				else if (bundleHandled.cCode == 52) // Under Applicator
				{
					AddToLog("IcpControl Under Applicator Reject TitleGroup: " + TitleGroupId.ToString() + " bundleid: "
						+ bundleId.ToString() + " - bundleIndex: " + bundleIndex.ToString()
						+ " - cCode: " + bundleHandled.cCode.ToString());

					if (bundleId != _lastBundleIdApplicatorCheck)
					{
						TopSheetItem queueItem = new TopSheetItem
						{
							BundleId = bundleId,
							BundleIndex = bundleIndex,
							TitleGroupId = TitleGroupId,
							IssueDateString = IssueDateString,
							LabelState = 51,
							TopSheetType = TopSheetTypes.DOUBLETOPSHEET
						};

						if (queueItem.BundleId >= 10000 && queueItem.BundleId < 50000)
						{
							//_topSheetEjectedQueue.Add(queueItem);
							//TsBundleQueue.Enqueue(queueItem);
							AddToLog("Under Applicator Reject TitleGroup: " + TitleGroupId.ToString() + " bundleid: " + queueItem.BundleId.ToString() + "Added to Eject TsBundleQueue - cCode: " + bundleHandled.cCode.ToString());
						}
						_lastBundleIdApplicatorCheck = bundleId;
					}

					GetTopSheetItemPrinted(bundleIndex, true);
				}

				sql = string.Format("exec UniStack_TopSheetBundleAction_SPH {0}, {1}, {2},'{3}', {4}, {5}, {6}, {7},{8}"
					, _gripperLineTableId
					, _topSheetLineTableId
					, TitleGroupId
					, IssueDateString
					, bundleHandled.bundleId
					, bundleHandled.iConveyorBundleIndex
					, bundleHandled.cCode
					, bundleHandled.cPosition == 255 ? -1 : bundleHandled.cPosition
					, -1//bundleHandled.PrinterIndex == 255 ? -1 : bundleHandled.PrinterIndex //cheng 09-18
					);

				try
				{
					string sAction = bundleHandled.cCode + "-";
					try
					{
						if (Enum.IsDefined(typeof(TopSheetActionEnum), (int)bundleHandled.cCode))
						{
							sAction += ((TopSheetActionEnum)(int)bundleHandled.cCode).ToString();
						}
					}
					catch
					{
					}

					string sLog = "Action reported to db - " + sAction + " - " + sql;
					if (bundleHandled.cCode == 0)
					{
						AddToLog(sLog);
					}
					else
					{
						AddToLog(LogLevels.Warning, sLog);
					}
				}
				catch
				{
				}

				DbAddCommandToQueue(DbCommandTypes.ExecuteNonQuery, sql, null, true);
			}
			catch
			{
			}
		}

		private void SaveBundleInfoToSTBScreen(uint bundleId, int topSheetLineTableId)
		{
			try
			{
				using (SqlConnection connection = new SqlConnection(_conn))
				{
					SqlCommand command = new SqlCommand("[dbo].[UniMail_SetBundleStatusOnSTBScreen_SPH]", connection)
					{
						CommandText = "[UniMail_SetBundleStatusOnSTBScreen_SPH]",
						CommandType = CommandType.StoredProcedure
					};
					command.Parameters.Add(new SqlParameter("@LineId", _gripperLineTableId));
					command.Parameters.Add(new SqlParameter("@TitleGroupId", TitleGroupId));
					command.Parameters.Add(new SqlParameter("@BundleId", Convert.ToInt32(bundleId)));
					command.Parameters.Add(new SqlParameter("@BundleStatusTableId", 15));
					command.Parameters.Add(new SqlParameter("@StackerLineId", topSheetLineTableId));

					connection.Open();
					SqlDataReader reader = command.ExecuteReader();
					reader.Close();
					connection.Close();
				}
				
			}
			catch(Exception ex)
			{
			}
		}

		public void UpdateBundleStatus(int topSheetLineTableId)
		{
			try
			{
				using (SqlConnection connection = new SqlConnection(_conn))
				{
					SqlCommand command = new SqlCommand("[dbo].[UniStack_StartStopUTRStackerLineUpdateStatus]", connection)
					{
						CommandText = "[UniStack_StartStopUTRStackerLineUpdateStatus]",
						CommandType = CommandType.StoredProcedure
					};
					command.Parameters.Add(new SqlParameter("@StackerLineId", topSheetLineTableId));
					command.Parameters.Add(new SqlParameter("@GripperLineId", _gripperLineTableId));


					connection.Open();
					SqlDataReader reader = command.ExecuteReader();
					reader.Close();
					connection.Close();
				}

			}
			catch (Exception ex)
			{
			}
		}

		//private void PZFHandleBundleMessage(BundleHandledStruct bundleHandled)
		//{
		//    try
		//    {
		//        uint bundleIndex = bundleHandled.iConveyorBundleIndex;
		//        uint bundleId = bundleHandled.bundleId;
		//        string sql;
		//        //TopSheetItem topSheetItem = GetTopSheetItemPrinted(bundleId, false);//, out listIndex);

		//        AddToLog("HandleBundleMessage" +
		//            " -TitleGroup: " + TitleGroupId.ToString() +
		//            " -Bundleid: " + bundleId.ToString() +
		//            " -BbundleIndex: " + bundleIndex.ToString() +
		//            " -cCode: " + bundleHandled.cCode.ToString());

		//        if (bundleHandled.cCode == 0)   //labeling ok
		//        {
		//            AddToLog("IcpControl labeled ok bundleid: " + bundleId.ToString() +
		//                " - bundleIndex: " + bundleIndex.ToString() +
		//                " - cCode: " + bundleHandled.cCode);

		//            lock (_lock_topSheetPrinted)
		//            {
		//                for (int j = _topSheetPrinted.Count - 1; j >= 0; j--)
		//                {
		//                    TopSheetItem tempItem = _topSheetPrinted[j];
		//                    if (tempItem.BundleIndex >= 11 && tempItem.BundleIndex < bundleIndex)
		//                    {
		//                        //sql = string.Format("exec UniStack_TopSheetBundleAction {0}, {1}, {2}, {3}, {4}, {5}, {6}"
		//                        //    , _gripperLineTableId
		//                        //    , _topSheetLineTableId
		//                        //    , tempItem.BundleId
		//                        //    , tempItem.BundleIndex
		//                        //    , (int)TopSheetActionEnum.BundleNotReportedPrintedList//16
		//                        //    , -1
		//                        //    , tempItem.PrinterNumber - 1
		//                        //    );//bundle not reported

		//                        //DbAddCommandToQueue(DbCommandTypes.ExecuteNonQuery, sql, null, true);

		//                        //AddToLog(LogLevels.Warning,
		//                        //    "IcpControl not reported (printed) bundleid: "
		//                        //    + tempItem.BundleId.ToString()
		//                        //    + " - bundleIndex: " + tempItem.BundleIndex.ToString());

		//                        _topSheetPrinted.RemoveAt(j);

		//                    }
		//                }
		//            }

		//            lock (_lock_topSheetQueue)
		//            {
		//                for (int j = _topSheetQueue.Count - 1; j >= 0; j--)
		//                {
		//                    TopSheetItem tempItem = _topSheetQueue[j];
		//                    if (tempItem.BundleIndex >= 11 && tempItem.BundleIndex < bundleIndex)
		//                    {
		//                        //sql = string.Format("exec UniStack_TopSheetBundleAction {0}, {1}, {2}, {3}, {4}, {5}, {6}"
		//                        //    , _gripperLineTableId
		//                        //    , _topSheetLineTableId
		//                        //    , tempItem.BundleId
		//                        //    , tempItem.BundleIndex
		//                        //    , (int)TopSheetActionEnum.BundleNotReportedPrintQueue//12
		//                        //    , -1
		//                        //    , tempItem.PrinterNumber - 1
		//                        //    );//bundle not reported

		//                        //DbAddCommandToQueue(DbCommandTypes.ExecuteNonQuery, sql, null, true);

		//                        //AddToLog(LogLevels.Warning,
		//                        //    "ERROR: IcpControl not reported (not printed) bundleid: "
		//                        //    + tempItem.BundleId.ToString()
		//                        //    + " - bundleIndex: " + tempItem.BundleIndex.ToString());

		//                        _topSheetQueue.RemoveAt(j);
		//                    }
		//                }
		//            }

		//            lock (_lock_TsBundleQueueAvoidTwoTopAtSametime)
		//            {
		//                int tempCount = TsBundleQueueAvoidTwoTopAtSametime.Count;
		//                for (int i = 0; i < tempCount; i++)
		//                {
		//                    if (bundleId == TsBundleQueueAvoidTwoTopAtSametime[i].BundleId)
		//                    {
		//                        TsBundleQueueAvoidTwoTopAtSametime.RemoveAt(i);

		//                        AddToLog(LogLevels.Debug,
		//                            "Bundle: " + bundleId.ToString()
		//                            + " remove from TsBundleQueueAvoidTwoTopAtSametime Due To Labeled, Count: "
		//                            + TsBundleQueueAvoidTwoTopAtSametime.Count.ToString());
		//                        i = tempCount;
		//                    }
		//                }
		//            }

		//            lock (_lock_TsBundleQueuePrintedResume)
		//            {
		//                int tempCount = TsBundleQueuePrintedResume.Count;
		//                for (int i = 0; i < tempCount; i++)
		//                {
		//                    if (bundleId == TsBundleQueuePrintedResume[i].BundleId)
		//                    {
		//                        TsBundleQueuePrintedResume.RemoveAt(i);

		//                        AddToLog(LogLevels.Debug,
		//                            "Bundle: " + bundleId.ToString()
		//                            + " remove from TsBundleQueuePrintedResume Due To Labeled, Count: "
		//                            + TsBundleQueuePrintedResume.Count.ToString());

		//                        i = tempCount;
		//                    }
		//                }
		//            }

		//            //GetTopSheetItemPrinted(bundleIndex, true);
		//        }
		//        else if (bundleHandled.cCode == 5) // Reject on DBH
		//        {
		//            AddToLog("HandleBundleMessage RejectOnDBH TitleGroup: " + TitleGroupId.ToString() + " bundleid: "
		//                + bundleId.ToString() + " - bundleIndex: " + bundleIndex.ToString()
		//                + " - cCode: " + bundleHandled.cCode.ToString());
		//            GetTopSheetItemPrinted(bundleIndex, true);

		//            lock (_lock_TsBundleQueueAvoidTwoTopAtSametime)
		//            {
		//                int tempCount = TsBundleQueueAvoidTwoTopAtSametime.Count;
		//                for (int i = 0; i < tempCount; i++)
		//                {
		//                    if (bundleId == TsBundleQueueAvoidTwoTopAtSametime[i].BundleId)
		//                    {
		//                        TsBundleQueueAvoidTwoTopAtSametime.RemoveAt(i);

		//                        AddToLog(LogLevels.Debug,
		//                            " Bundle: " + bundleId.ToString() +
		//                            " Remove from TsBundleQueueAvoidTwoTopAtSametime Due To Reject on DBH, Count: "
		//                            + TsBundleQueueAvoidTwoTopAtSametime.Count.ToString());

		//                        i = tempCount;// Only exec one time
		//                    }
		//                }
		//            }


		//            lock (_lock_TsBundleQueuePrintedResume)
		//            {
		//                int tempCount = TsBundleQueuePrintedResume.Count;

		//                for (int i = 0; i < tempCount; i++)
		//                {
		//                    if (bundleId == TsBundleQueuePrintedResume[i].BundleId)
		//                    {
		//                        TsBundleQueuePrintedResume.RemoveAt(i);

		//                        AddToLog(LogLevels.Debug,
		//                            "Bundle: " + bundleId.ToString()
		//                            + " remove from TsBundleQueuePrintedResume Due To Reject on DBH, Count: "
		//                            + TsBundleQueuePrintedResume.Count.ToString());

		//                        i = tempCount;
		//                    }
		//                }
		//            }
		//        }
		//        else if (bundleHandled.cCode == 6) //Label Timeout
		//        {
		//            AddToLog("HandleBundleMessage Label Timout TitleGroup: " + TitleGroupId.ToString() + " bundleid: "
		//                + bundleId.ToString() + " - bundleIndex: " + bundleIndex.ToString()
		//                + " - cCode: " + bundleHandled.cCode.ToString());
		//        }
		//        else if (bundleHandled.cCode == 8) // On STB Pos1
		//        {
		//            AddToLog("HandleBundleMessage OnSTB TitleGroup: " + TitleGroupId.ToString() + " bundleid: "
		//                + bundleId.ToString() + " - bundleIndex: " + bundleIndex.ToString()
		//                + " - cCode: " + bundleHandled.cCode.ToString());
		//        }
		//        else if (bundleHandled.cCode == 9) // Removed from STB
		//        {
		//            AddToLog("HandleBundleMessage remove from STB TitleGroup: " + TitleGroupId.ToString() + " bundleid: "
		//                + bundleId.ToString() + " - bundleIndex: " + bundleIndex.ToString()
		//                + " - PosId: " + bundleHandled.cPosition.ToString()
		//                + " - cCode: " + bundleHandled.cCode.ToString());
		//        }
		//        else if (bundleHandled.cCode == 10) // Removed from PZF Table
		//        {
		//            AddToLog("HandleBundleMessage remove from PZFTable TitleGroup: " + TitleGroupId.ToString() + " bundleid: "
		//                + bundleId.ToString() + " - bundleIndex: " + bundleIndex.ToString()
		//                + " - cCode: " + bundleHandled.cCode.ToString());
		//        }
		//        else if (bundleHandled.cCode == 52) // Under Applicator
		//        {
		//            AddToLog("IcpControl Under Applicator Reject TitleGroup: " + TitleGroupId.ToString() + " bundleid: "
		//                + bundleId.ToString() + " - bundleIndex: " + bundleIndex.ToString()
		//                + " - cCode: " + bundleHandled.cCode.ToString());

		//            if (bundleId != _lastBundleIdApplicatorCheck)
		//            {
		//                TopSheetItem queueItem = new TopSheetItem
		//                {
		//                    BundleId = bundleId,
		//                    BundleIndex = bundleIndex,
		//                    TitleGroupId = TitleGroupId,
		//                    IssueDateString = IssueDateString,
		//                    LabelState = 51,
		//                    TopSheetType = TopSheetTypes.DOUBLETOPSHEET
		//                };

		//                if (queueItem.BundleId >= 10000 && queueItem.BundleId < 50000)
		//                {
		//                    //_topSheetEjectedQueue.Add(queueItem);
		//                    //TsBundleQueue.Enqueue(queueItem);
		//                    AddToLog("Under Applicator Reject TitleGroup: " + TitleGroupId.ToString() + " bundleid: " + queueItem.BundleId.ToString() + "Added to Eject TsBundleQueue - cCode: " + bundleHandled.cCode.ToString());
		//                }
		//                _lastBundleIdApplicatorCheck = bundleId;
		//            }

		//            GetTopSheetItemPrinted(bundleIndex, true);
		//        }

		//        sql = string.Format("exec UniStack_TopSheetBundleAction_SPH {0}, {1}, {2},'{3}', {4}, {5}, {6}, {7},{8}"
		//            , _gripperLineTableId
		//            , _topSheetLineTableId
		//            , TitleGroupId
		//            , IssueDateString
		//            , bundleHandled.bundleId
		//            , bundleHandled.iConveyorBundleIndex
		//            , bundleHandled.cCode
		//            , bundleHandled.cPosition == 255 ? -1 : bundleHandled.cPosition
		//            , -1//bundleHandled.PrinterIndex == 255 ? -1 : bundleHandled.PrinterIndex //cheng 09-18
		//            );

		//        try
		//        {
		//            string sAction = bundleHandled.cCode + "-";
		//            try
		//            {
		//                if (Enum.IsDefined(typeof(TopSheetActionEnum), (int)bundleHandled.cCode))
		//                {
		//                    sAction += ((TopSheetActionEnum)(int)bundleHandled.cCode).ToString();
		//                }
		//            }
		//            catch
		//            {
		//            }

		//            string sLog = "Action reported to db - " + sAction + " - " + sql;
		//            if (bundleHandled.cCode == 0)
		//            {
		//                AddToLog(sLog);
		//            }
		//            else
		//            {
		//                AddToLog(LogLevels.Warning, sLog);
		//            }
		//        }
		//        catch
		//        {
		//        }

		//        DbAddCommandToQueue(DbCommandTypes.ExecuteNonQuery, sql, null, true);
		//    }
		//    catch
		//    {
		//    }
		//}

		//private void HandleBundleMessage(BundleHandledStruct bundleHandled)
		//{
		//    try
		//    {
		//        //BundleHandledStruct bundleHandled = _status.cBundlesHandled[i];
		//        uint bundleIndex = bundleHandled.iConveyorBundleIndex;
		//        uint bundleId = bundleHandled.bundleId;
		//        //ushort editionTableId = bundleHandled.EditionTableId;
		//        string sql;
		//        //int listIndex;
		//        //TopSheetItem topSheetItem = GetTopSheetItemPrinted(bundleIndex, false);//, out listIndex);
		//        TopSheetItem topSheetItem = GetTopSheetItemPrinted(bundleId, false);//, out listIndex);
		//        //TopSheetItem topSheetItem = GetTopSheetItemEjectFromPZF(bundleId, false);//, out listIndex);

		//        //AddToLog("HandleBundleMessage bundleid: "
		//        //       + bundleId.ToString() + " - bundleIndex: " + bundleIndex.ToString()
		//        //       + " - cCode: " + bundleHandled.cCode.ToString());

		//        if (topSheetItem != null)
		//        {
		//            topSheetItem.HandledCode = bundleHandled.cCode;
		//            bundleId = topSheetItem.BundleId;
		//            //editionTableId = topSheetItem.EditionTableId;

		//            AddToLog("HandleBundleMessage" +
		//                " -TitleGroup: " + TitleGroupId.ToString() +
		//                " -Bundleid: " + topSheetItem.BundleId.ToString() +
		//                " -BbundleIndex: " + topSheetItem.BundleIndex.ToString() +
		//                " -cCode: " + bundleHandled.cCode.ToString());

		//            if (bundleHandled.cCode == 0)   //labeling ok
		//            {
		//                //AddToLog("IcpControl labeled ok bundleid: "
		//                //    + topSheetItem.BundleId.ToString() + " - bundleIndex: " + topSheetItem.BundleIndex.ToString());

		//                lock (_lock_topSheetPrinted)
		//                {
		//                    for (int j = _topSheetPrinted.Count - 1; j >= 0; j--)
		//                    {
		//                        TopSheetItem tempItem = _topSheetPrinted[j];
		//                        if (tempItem.BundleIndex >= 11 && tempItem.BundleIndex < bundleIndex)
		//                        {
		//                            //sql = string.Format("exec UniStack_TopSheetBundleAction {0}, {1}, {2}, {3}, {4}, {5}, {6}"
		//                            //    , _gripperLineTableId
		//                            //    , _topSheetLineTableId
		//                            //    , tempItem.BundleId
		//                            //    , tempItem.BundleIndex
		//                            //    , (int)TopSheetActionEnum.BundleNotReportedPrintedList//16
		//                            //    , -1
		//                            //    , tempItem.PrinterNumber - 1
		//                            //    );//bundle not reported

		//                            //DbAddCommandToQueue(DbCommandTypes.ExecuteNonQuery, sql, null, true);

		//                            //AddToLog(LogLevels.Warning,
		//                            //    "IcpControl not reported (printed) bundleid: "
		//                            //    + tempItem.BundleId.ToString()
		//                            //    + " - bundleIndex: " + tempItem.BundleIndex.ToString());

		//                            _topSheetPrinted.RemoveAt(j);

		//                        }
		//                    }
		//                }

		//                lock (_lock_topSheetQueue)
		//                {
		//                    for (int j = _topSheetQueue.Count - 1; j >= 0; j--)
		//                    {
		//                        TopSheetItem tempItem = _topSheetQueue[j];
		//                        if (tempItem.BundleIndex >= 11 && tempItem.BundleIndex < bundleIndex)
		//                        {
		//                            //sql = string.Format("exec UniStack_TopSheetBundleAction {0}, {1}, {2}, {3}, {4}, {5}, {6}"
		//                            //    , _gripperLineTableId
		//                            //    , _topSheetLineTableId
		//                            //    , tempItem.BundleId
		//                            //    , tempItem.BundleIndex
		//                            //    , (int)TopSheetActionEnum.BundleNotReportedPrintQueue//12
		//                            //    , -1
		//                            //    , tempItem.PrinterNumber - 1
		//                            //    );//bundle not reported

		//                            //DbAddCommandToQueue(DbCommandTypes.ExecuteNonQuery, sql, null, true);

		//                            //AddToLog(LogLevels.Warning,
		//                            //    "ERROR: IcpControl not reported (not printed) bundleid: "
		//                            //    + tempItem.BundleId.ToString()
		//                            //    + " - bundleIndex: " + tempItem.BundleIndex.ToString());

		//                            _topSheetQueue.RemoveAt(j);
		//                        }
		//                    }
		//                }

		//                lock (_lock_TsBundleQueueAvoidTwoTopAtSametime)
		//                {
		//                    int tempCount = TsBundleQueueAvoidTwoTopAtSametime.Count;
		//                    for (int i = 0; i < tempCount; i++)
		//                    {
		//                        if (bundleId == TsBundleQueueAvoidTwoTopAtSametime[i].BundleId)
		//                        {
		//                            TsBundleQueueAvoidTwoTopAtSametime.RemoveAt(i);

		//                            AddToLog(LogLevels.Debug,
		//                                "Bundle: " + bundleId.ToString()
		//                                + " remove from TsBundleQueueAvoidTwoTopAtSametime Due To Labeled, Count: "
		//                                + TsBundleQueueAvoidTwoTopAtSametime.Count.ToString());
		//                            i = tempCount;
		//                        }
		//                    }
		//                }

		//                lock (_lock_TsBundleQueuePrintedResume)
		//                {
		//                    int tempCount = TsBundleQueuePrintedResume.Count;
		//                    for (int i = 0; i < tempCount; i++)
		//                    {
		//                        if (bundleId == TsBundleQueuePrintedResume[i].BundleId)
		//                        {
		//                            TsBundleQueuePrintedResume.RemoveAt(i);

		//                            AddToLog(LogLevels.Debug,
		//                                "Bundle: " + bundleId.ToString()
		//                                + " remove from TsBundleQueuePrintedResume Due To Labeled, Count: "
		//                                + TsBundleQueuePrintedResume.Count.ToString());

		//                            i = tempCount;
		//                        }
		//                    }
		//                }

		//                GetTopSheetItemPrinted(bundleIndex, true);
		//            }
		//            else if (bundleHandled.cCode == 5) // Reject on DBH
		//            {
		//                AddToLog("HandleBundleMessage RejectOnDBH TitleGroup: " + TitleGroupId.ToString() + " bundleid: "
		//                   + topSheetItem.BundleId.ToString() + " - bundleIndex: " + topSheetItem.BundleIndex.ToString()
		//                   + " - cCode: " + bundleHandled.cCode.ToString());
		//                GetTopSheetItemPrinted(bundleIndex, true);

		//                lock (_lock_TsBundleQueueAvoidTwoTopAtSametime)
		//                {
		//                    int tempCount = TsBundleQueueAvoidTwoTopAtSametime.Count;
		//                    for (int i = 0; i < tempCount; i++)
		//                    {
		//                        if (bundleId == TsBundleQueueAvoidTwoTopAtSametime[i].BundleId)
		//                        {
		//                            TsBundleQueueAvoidTwoTopAtSametime.RemoveAt(i);

		//                            AddToLog(LogLevels.Debug,
		//                                "Bundle: " + bundleId.ToString()
		//                                + " remove from TsBundleQueueAvoidTwoTopAtSametime Due To Reject on DBH, Count: "
		//                                + TsBundleQueueAvoidTwoTopAtSametime.Count.ToString());

		//                            i = tempCount;// Only exec one time
		//                        }
		//                    }
		//                }


		//                lock (_lock_TsBundleQueuePrintedResume)
		//                {
		//                    int tempCount = TsBundleQueuePrintedResume.Count;

		//                    for (int i = 0; i < tempCount; i++)
		//                    {
		//                        if (bundleId == TsBundleQueuePrintedResume[i].BundleId)
		//                        {
		//                            TsBundleQueuePrintedResume.RemoveAt(i);

		//                            AddToLog(LogLevels.Debug,
		//                                "Bundle: " + bundleId.ToString()
		//                                + " remove from TsBundleQueuePrintedResume Due To Reject on DBH, Count: "
		//                                + TsBundleQueuePrintedResume.Count.ToString());

		//                            i = tempCount;
		//                        }
		//                    }
		//                }
		//            }
		//            else if (bundleHandled.cCode == 6) //Label Timeout
		//            {
		//                AddToLog("HandleBundleMessage Label Timout TitleGroup: " + TitleGroupId.ToString() + " bundleid: "
		//                   + topSheetItem.BundleId.ToString() + " - bundleIndex: " + topSheetItem.BundleIndex.ToString()
		//                   + " - cCode: " + bundleHandled.cCode.ToString());
		//            }
		//            else if (bundleHandled.cCode == 8) // On STB Pos1
		//            {
		//                AddToLog("HandleBundleMessage OnSTB TitleGroup: " + TitleGroupId.ToString() + " bundleid: "
		//                   + topSheetItem.BundleId.ToString() + " - bundleIndex: " + topSheetItem.BundleIndex.ToString()
		//                   + " - cCode: " + bundleHandled.cCode.ToString());
		//            }
		//            else if (bundleHandled.cCode == 9) // Removed from STB
		//            {
		//                AddToLog("HandleBundleMessage remove from STB TitleGroup: " + TitleGroupId.ToString() + " bundleid: "
		//                   + topSheetItem.BundleId.ToString() + " - bundleIndex: " + topSheetItem.BundleIndex.ToString()
		//                   + " - PosId: " + bundleHandled.cPosition.ToString()
		//                   + " - cCode: " + bundleHandled.cCode.ToString());
		//            }
		//            else if (bundleHandled.cCode == 10) // Removed from PZF Table
		//            {
		//                AddToLog("HandleBundleMessage remove from PZFTable TitleGroup: " + TitleGroupId.ToString() + " bundleid: "
		//                   + topSheetItem.BundleId.ToString() + " - bundleIndex: " + topSheetItem.BundleIndex.ToString()
		//                   + " - cCode: " + bundleHandled.cCode.ToString());
		//            }
		//            else if (bundleHandled.cCode == 52) // Under Applicator
		//            {
		//                AddToLog("IcpControl Under Applicator Reject TitleGroup: " + TitleGroupId.ToString() + " bundleid: "
		//                   + topSheetItem.BundleId.ToString() + " - bundleIndex: " + topSheetItem.BundleIndex.ToString()
		//                   + " - cCode: " + bundleHandled.cCode.ToString());

		//                if (bundleId != _lastBundleIdApplicatorCheck)
		//                {
		//                    TopSheetItem queueItem = new TopSheetItem
		//                    {
		//                        BundleId = bundleId,
		//                        BundleIndex = topSheetItem.BundleIndex,
		//                        TitleGroupId = TitleGroupId,
		//                        IssueDateString = IssueDateString,
		//                        LabelState = 51,
		//                        TopSheetType = TopSheetTypes.DOUBLETOPSHEET
		//                    };

		//                    if (queueItem.BundleId >= 10000 && queueItem.BundleId < 50000)
		//                    {
		//                        //_topSheetEjectedQueue.Add(queueItem);
		//                        //TsBundleQueue.Enqueue(queueItem);
		//                        AddToLog("Under Applicator Reject TitleGroup: " + TitleGroupId.ToString() + " bundleid: " + queueItem.BundleId.ToString() + "Added to Eject TsBundleQueue - cCode: " + bundleHandled.cCode.ToString());
		//                    }
		//                    _lastBundleIdApplicatorCheck = bundleId;
		//                }

		//                GetTopSheetItemPrinted(bundleIndex, true);
		//            }
		//        }
		//        else
		//        {
		//            AddToLog("HandleBundleMessage TopsheetItem is Null, TitleGroup: " + TitleGroupId.ToString() + " _topSheetPrinted.Count = " + _topSheetPrinted.Count + " - bundleId: "
		//                   + bundleId.ToString() + " - bundleIndex: " + bundleIndex.ToString()
		//                   + " - cCode: " + bundleHandled.cCode.ToString());
		//        }

		//        sql = string.Format("exec UniStack_TopSheetBundleAction_SPH {0}, {1}, {2},'{3}', {4}, {5}, {6}, {7},{8}"
		//            , _gripperLineTableId
		//            , _topSheetLineTableId
		//            , TitleGroupId
		//            , IssueDateString
		//            , bundleHandled.bundleId
		//            , bundleHandled.iConveyorBundleIndex
		//            , bundleHandled.cCode
		//            , bundleHandled.cPosition == 255 ? -1 : bundleHandled.cPosition
		//            , -1//bundleHandled.PrinterIndex == 255 ? -1 : bundleHandled.PrinterIndex //cheng 09-18
		//            );

		//        try
		//        {
		//            string sAction = bundleHandled.cCode + "-";
		//            try
		//            {
		//                if (Enum.IsDefined(typeof(TopSheetActionEnum), (int)bundleHandled.cCode))
		//                {
		//                    sAction += ((TopSheetActionEnum)(int)bundleHandled.cCode).ToString();
		//                }
		//            }
		//            catch
		//            {
		//            }

		//            string sLog = "Action reported to db - " + sAction + " - " + sql;
		//            if (bundleHandled.cCode == 0)
		//            {
		//                AddToLog(sLog);
		//            }
		//            else
		//            {
		//                AddToLog(LogLevels.Warning, sLog);
		//            }
		//        }
		//        catch
		//        {
		//        }

		//        DbAddCommandToQueue(DbCommandTypes.ExecuteNonQuery, sql, null, true);
		//    }
		//    catch
		//    {
		//    }
		//}

		private TopSheetItem GetTopSheetItemEjectFromPZF(uint bundleId, bool doRemove)//, out int listIndex)
		{
			TopSheetItem result = null;

			bool isFound = false;
			int listIndex = -1;

			//lock (_topSheetQueue)
			try
			{
				lock (_lock_topSheetQueue)
				{
					while (!isFound && ++listIndex < _topSheetQueue.Count)
					{
						TopSheetItem queueItem = _topSheetQueue[listIndex];
						if (queueItem.BundleId == bundleId)
						{
							result = queueItem;
							isFound = true;

							if (doRemove)
							{
								_topSheetQueue.RemoveAt(listIndex);
							}
						}
					}
				}

				return result;
			}
			catch
			{
			}
			return result;
		}

		private void IcpControl()
		{
			try
			{
				IcpConnectionState = IcpConnectionStates.Disconnected;
				byte[] response = null;
				int readLength = _statusStructSize;
				bool statusReceived = false;
				bool readOK = false;
				DateTime CurTime = DateTime.Now;
				DateTime lastSTBPACCycleTime = DateTime.Now;

				AddToLog(LogLevels.Debug, "TopsheetMain: IcpControl started");

				while (IsStarted && _icpThread.ManagedThreadId == Thread.CurrentThread.ManagedThreadId)
				{
					try
					{
						lastSTBPACCycleTime = DateTime.Now;

						try
						{
							if (_tcpClientIcp == null || _tcpClientIcp.Client == null || !_tcpClientIcp.Client.Connected)
							{
								RequestStackerReset = false;

								IcpConnectionState = IcpConnectionStates.Disconnected;
								AddToLog(LogLevels.Debug, "Icp Connect State: " + IcpConnectionState);

								//_isWaitingToSendPrintToIcp = false;

								if (_setupReceived && _ioIp != null && _ioIp.Length > 0 && _ioPort > 0)
								{
									AddToLog(LogLevels.Debug, "Icp connecting ...");

									if (_doReconnectIcp)
									{
										_doReconnectIcp = false;
									}

									statusReceived = false;

									_tcpClientIcp = new TcpClient
									{
										ReceiveTimeout = 1000,
										SendTimeout = 1000
									};

									//_tcpClientIcp.LingerState = new LingerOption(true, 0);
									IcpConnectionState = IcpConnectionStates.Connecting;

									if (PingTest(_ioIp))
									{
										_tcpClientIcp.Connect(_ioIp, _ioPort);

										_networkStreamIcp = _tcpClientIcp.GetStream();
										_networkStreamIcp.ReadTimeout = 1000;
										_networkStreamIcp.WriteTimeout = 1000;

										_binaryReaderIcp = new BinaryReader(_networkStreamIcp);
										_binaryWriterIcp = new BinaryWriter(_networkStreamIcp);

										IcpConnectionState = IcpConnectionStates.Connected;
										AddToLog(LogLevels.Debug, "Ip: " + _ioIp + ", Port: " + _ioPort + " Icp connected");

										//lock (_lockTelegramListIcp)
										//{
										//_telegramListIcp.Clear();
										//}
										lock (_lockSTBTelegramListIcp)
										{
											IcpTelegramList_Clear();

										}

										//DbGetSetup();
										//DbGetItems();
										IcpGetOrAddTelegram(IcpTelegramTypes.SendSetup, null, true);

										int bytesCleared = 0;
										try
										{
											while (_tcpClientIcp.Client != null && _tcpClientIcp.Client.Connected)
											{
												_binaryReaderIcp.ReadByte();
												++bytesCleared;
											}
										}
										catch
										{
										}

										if (bytesCleared > 0)
										{
											AddToLog(LogLevels.Warning, "Icp buffer cleared - num bytes = " + bytesCleared.ToString());
										}
									}
								}
							}

							if (_tcpClientIcp != null && _tcpClientIcp.Client != null && _tcpClientIcp.Client.Connected)
							{
								byte[] telegram = null;

								if (statusReceived && IcpTelegramList_GetCount() > 0)
								{
									telegram = IcpTelegramList_GetNext();

									if (telegram != null && telegram.Length > 10 && telegram[3] == (byte)'B')
									{
										AddToLog(LogLevels.Warning, "Get Next B Telegram[1]: " + telegram[1] + " Telegram[3]: " + telegram[3] + " Bundle ID: " + (int.Parse(Convert.ToString(telegram[8], 10)) + int.Parse(Convert.ToString(telegram[9] * 0x100, 10))));
									}
									else if (telegram != null && telegram.Length > 4 && telegram[3] == (byte)'J')
									{
										AddToLog(LogLevels.Warning, "Get Next J Telegram[1]: " + telegram[1]);
									}
									else if (telegram != null && telegram.Length > 4 && telegram[3] != (byte)'B' && telegram[3] != (byte)'J')
									{
										AddToLog(LogLevels.Warning, "Get Not B Telegram[1]: " + telegram[1]);
									}
								}

								if (telegram == null)
								{
									telegram = IcpGetOrAddTelegram(IcpTelegramTypes.RequestStatus, null, false);
									//if (telegram != null)
									//{
									//    AddToLog(LogLevels.Warning, "Request Status Telegram[1]: " + telegram[1]);
									//}
								}

								try
								{
									_binaryWriterIcp.Write(telegram);
									_binaryWriterIcp.Flush();

									//if (telegram != null)
									//{
									//    AddToLog(LogLevels.Warning, "_binaryWriterIcp 1 Telegram[1]: " + telegram[1]);
									//}
								}
								catch (Exception ex)
								{
									AddToLog(LogLevels.Warning, "ICP Write Error " + ": " + ex.ToString());
								}

								readOK = false;
								CurTime = DateTime.Now;

								int ReadErrorTime = 0;

								while (!readOK && CurTime.AddMilliseconds(2500) > DateTime.Now)
								{
									try
									{
										//ResponseFromPAC(response, telegram, readLength, statusReceived);
										response = _binaryReaderIcp.ReadBytes(readLength);

										if (response != null
											&& response.Length == readLength
											&& response[0] == telegram[0]
											&& response[1] == telegram[1]
											&& response[2] == telegram[3]
											&& response[readLength - 1] == 4)
										{
											readOK = true;
											//AddToLog(LogLevels.Warning, "BinaryWriterIcp  Telegram[1]: " + telegram[1] + " Telegram[3]: " + telegram[3]);
										}
										else
										{
											_binaryWriterIcp.Write(telegram);
											_binaryWriterIcp.Flush();
											AddToLog(LogLevels.Warning, "BinaryWriterIcp Telegram[1]: " + telegram[1] + " ,Telegram[3]: " + telegram[3]);

											if (response != null)
											{
												AddToLog(LogLevels.Warning, "Error in ICP response header - not in sync, " +
													"  Response Length: " + response.Length +
													"  Read Length: " + readLength +
													", response[0]: " + response[0] +
													", response[1]: " + response[1] +
													", response[2]: " + response[2] +
													", response[readLength-1]: " + response[readLength - 1] +
													", telegram[0]: " + telegram[0] +
													", telegram[1]: " + telegram[1] +
													", telegram[3]: " + telegram[3]);
											}
											else
											{
												AddToLog(LogLevels.Warning, "Response is Null, BinaryReader Failed!");
											}

											Thread.Sleep(50);
										}
									}
									catch (Exception ex)
									{
										ReadErrorTime++;
										AddToLog(LogLevels.Warning, "ICP Response Error " + ReadErrorTime + ": " + ex.ToString() + " , Try to read again!");
										Thread.Sleep(10);
									}
								}

								if (response != null
									&& response.Length == readLength
									&& response[0] == telegram[0]
									&& response[1] == telegram[1]
									&& response[2] == telegram[3]
									&& response[readLength - 1] == 4)
								{
									//lock (_lockStatus)
									{
										GCHandle gch = GCHandle.Alloc(response, GCHandleType.Pinned);
										try
										{
											_status = (StatusStruct)Marshal.PtrToStructure(gch.AddrOfPinnedObject(), typeof(StatusStruct));

											if (_status.cBundlesHandledCount > 0)
											{
												for (int i = 0; i < _status.cBundlesHandledCount; i++)
												{
													HandleBundleMessage(_status.cBundlesHandled[i]);
												}
											}
											statusReceived = true;

											if (telegram[3] == (byte)'B')
											{
												AddToLog(LogLevels.Warning, "B StatusReceived Telegram[3]: " + telegram[3] + " Bundle ID: " + (int.Parse(Convert.ToString(telegram[8], 10)) + int.Parse(Convert.ToString(telegram[9] * 0x100, 10))));
											}
											else if (telegram[3] == (byte)'J')
											{
												AddToLog(LogLevels.Warning, "J StatusReceived Telegram[3]: " + telegram[3]);

											}
										}
										finally
										{
											gch.Free();
										}

										//if (_status.Length != readLength)
										//{
										//    AddToLog(LogLevels.Warning, "Error in ICP response header - wrong length");

										//    IcpDisconnect();
										//}   //cheng 09-18
									}
								}
								else
								{
									if (response != null && telegram != null)
									{
										AddToLog(LogLevels.Warning, "Error in ICP response header - not in sync, Response Length: "
										+ response.Length + " Read Length: " + readLength + ", response[0]: " + response[0]
										+ ", response[1]: " + response[1] + "response[2]: " + response[2] + " response[readLength-1]: " + response[readLength - 1]
										+ "telegram[0]: " + telegram[0] + " telegram[1]: " + telegram[1] + " telegram[3]: " + telegram[3]);
									}
									else
									{
										AddToLog(LogLevels.Warning, "Response is Null, BinaryReader Failed!");
									}

									IcpDisconnect();

								}

								if (_doReconnectIcp)
								{
									_doReconnectIcp = false;
									IcpDisconnect();
								}
							}
							else if (_tcpClientIcp == null)
							{
								AddToLog(LogLevels.Warning, "_tcpClientIcp is null");
							}
							else if (_tcpClientIcp.Client == null)
							{
								AddToLog(LogLevels.Warning, "_tcpClientIcp.Client is null");
							}
							else if (!_tcpClientIcp.Client.Connected)
							{
								AddToLog(LogLevels.Warning, "_tcpClientIcp.Client DisConnected");
							}
						}
						catch (Exception ex)
						{
							AddToLog(LogLevels.Warning, ex.ToString());
							IcpDisconnect();
							Thread.Sleep(1000);
						}
						finally
						{
							int index = 0;
							do
							{
								Thread.Sleep(10);
							}
							while (IcpTelegramList_GetCount() == 0 && ++index < 20);

							//Thread.Sleep(200);
						}

						if (DateTime.Now > lastSTBPACCycleTime.AddSeconds(3))
						{
							Logging.AddToLog(LogLevels.Warning, "STB PAC 3 seconds timeout " + lastSTBPACCycleTime.ToString());
							Thread.Sleep(100);
						}
					}
					catch
					{
					}
				}

				if (_icpThread.ManagedThreadId != Thread.CurrentThread.ManagedThreadId)
				{
					AddToLog(LogLevels.Debug, "IcpControl old thread exited");
				}
				else
				{
					IcpDisconnect();
					IcpConnectionState = IcpConnectionStates.NotRunning;
					AddToLog(LogLevels.Debug, "IcpControl stopped");
				}
			}
			catch
			{
			}
			finally
			{
				Thread.Sleep(1);
			}
		}

		//private void ResponseFromPAC(byte[] response, byte[] telegram, int readLength, bool statusReceived)
		//{
		//    response = _binaryReaderIcp.ReadBytes(readLength);
		//    if (response.Length == readLength
		//        && response[0] == telegram[0]
		//        && response[1] == telegram[1]
		//        && response[2] == telegram[3]
		//        && response[readLength - 1] == 4)
		//    {
		//        //lock (_lockStatus)
		//        {
		//            GCHandle gch = GCHandle.Alloc(response, GCHandleType.Pinned);
		//            try
		//            {
		//                _status = (StatusStruct)Marshal.PtrToStructure(gch.AddrOfPinnedObject(), typeof(StatusStruct));

		//                if (_status.cBundlesHandledCount > 0)
		//                {
		//                    for (int i = 0; i < _status.cBundlesHandledCount; i++)
		//                    {
		//                        HandleBundleMessage(_status.cBundlesHandled[i]);
		//                    }
		//                }
		//                statusReceived = true;
		//            }
		//            finally
		//            {
		//                gch.Free();
		//            }

		//            //if (_status.Length != readLength)
		//            //{
		//            //    AddToLog(LogLevels.Warning, "Error in ICP response header - wrong length");

		//            //    IcpDisconnect();
		//            //}   //cheng 09-18
		//        }
		//    }
		//    else
		//    {
		//        AddToLog(LogLevels.Warning, "Error in ICP response header - not in sync");
		//        IcpDisconnect();
		//    }
		//}

		private void PZFIcpControl()
		{
			try
			{
				PZFIcpConnectionState = IcpConnectionStates.Disconnected;
				byte[] PZFResponse = null;
				int PZFReadLength = PZFSatusStructSize;
				bool PZFStatusReceived = false;
				bool isPZFReady = false;

				bool PZFReadOK = false;
				DateTime PZFCurTime = DateTime.Now;

				AddToLog(LogLevels.Debug, "PZFIcpControl started");

				while (PZFIsStarted &&
					PZFIcpThread.ManagedThreadId == Thread.CurrentThread.ManagedThreadId)
				{
					try
					{
						try
						{
							if (PZFTcpClientIcp == null ||
								PZFTcpClientIcp.Client == null ||
								!PZFTcpClientIcp.Client.Connected)
							{
								PZFRequestStackerReset = false;
								PZFIcpConnectionState = IcpConnectionStates.Disconnected;

								if (//_setupReceived &&
									PZFIoIp != null && PZFIoIp.Length > 0 && PZFIoPort > 0)
								{
									AddToLog(LogLevels.Debug, "PZFIcp Connecting ...");
									if (PZFDoReconnectIcp)
									{
										PZFDoReconnectIcp = false;
									}
									PZFStatusReceived = false;

									PZFTcpClientIcp = new TcpClient
									{
										ReceiveTimeout = 1000,
										SendTimeout = 1000
									};

									PZFIcpConnectionState = IcpConnectionStates.Connecting;

									if (PingTest(PZFIoIp))
									{
										PZFTcpClientIcp.Connect(PZFIoIp, PZFIoPort);

										PZFNetworkStreamIcp = PZFTcpClientIcp.GetStream();
										PZFNetworkStreamIcp.ReadTimeout = 1000;
										PZFNetworkStreamIcp.WriteTimeout = 1000;

										PZFBinaryReaderIcp = new BinaryReader(PZFNetworkStreamIcp);
										PZFBinaryWriterIcp = new BinaryWriter(PZFNetworkStreamIcp);

										PZFIcpConnectionState = IcpConnectionStates.Connected;
										AddToLog(LogLevels.Debug, "PZF Ip: " + PZFIoIp + ", Port: " + PZFIoPort + " Icp connected");

										PZFIcpTelegramList_Clear();

										//IcpGetOrAddTelegram(IcpTelegramTypes.SendSetup, null, true);

										int PzfbytesCleared = 0;
										try
										{
											while (PZFTcpClientIcp.Client != null && PZFTcpClientIcp.Client.Connected)
											{
												PZFBinaryReaderIcp.ReadByte();
												++PzfbytesCleared;
											}
										}
										catch
										{
										}

										if (PzfbytesCleared > 0)
										{
											AddToLog(LogLevels.Warning, "PZF Icp buffer cleared - num bytes = " + PzfbytesCleared.ToString());
										}
									}
								}
							}

							if (PZFTcpClientIcp != null && PZFTcpClientIcp.Client != null && PZFTcpClientIcp.Client.Connected)
							{
								byte[] PZFTelegram = null;

								if (PZFStatusReceived && PzfIcpTelegramList_GetCount() > 0)
								{
									PZFTelegram = PzfIcpTelegramList_GetNext();
								}

								if (PZFTelegram == null)
								{
									PZFTelegram = PZFIcpGetOrAddTelegram(PZFIcpTelegramTypes.RequestStatus, null, false);
								}

								PZFBinaryWriterIcp.Write(PZFTelegram);
								PZFBinaryWriterIcp.Flush();

								PZFReadOK = false;
								PZFCurTime = DateTime.Now;

								int ReadErrorTime = 0;

								while (!PZFReadOK && PZFCurTime.AddMilliseconds(2500) > DateTime.Now)
								{
									try
									{
										//ResponseFromPAC(response, telegram, readLength, statusReceived);
										PZFResponse = PZFBinaryReaderIcp.ReadBytes(PZFReadLength);
										if (PZFResponse != null
											&& PZFResponse.Length == PZFReadLength
											&& PZFResponse[0] == PZFTelegram[0]
											&& PZFResponse[1] == PZFTelegram[1]
											&& PZFResponse[2] == PZFTelegram[3]
											&& PZFResponse[PZFReadLength - 1] == 4)
										{
											PZFReadOK = true;
										}
										else
										{
											PZFBinaryWriterIcp.Write(PZFTelegram);
											PZFBinaryWriterIcp.Flush();
											AddToLog(LogLevels.Warning, "PZFBinaryWriterIcp 2 Telegram[1]: " + PZFTelegram[1] + " ,Telegram[3]: " + PZFTelegram[3]);

											if (PZFResponse != null)
											{
												AddToLog(LogLevels.Warning, "Error in PZF ICP response header - not in sync, PZFResponse Length: " + PZFResponse.Length +
												" Read Length: " + PZFReadLength +
												", PZFResponse[0]: " + PZFResponse[0] +
												", PZFResponse[1]: " + PZFResponse[1] +
												", PZFResponse[2]: " + PZFResponse[2] +
												", PZFResponse[readLength-1]: " + PZFResponse[PZFReadLength - 1] +
												", telegram[0]: " + PZFTelegram[0] +
												", telegram[1]: " + PZFTelegram[1] +
												", telegram[3]: " + PZFTelegram[3]);
											}
											else
											{
												AddToLog(LogLevels.Warning, "PZFResponse is Null, BinaryReader Failed!");
											}
											Thread.Sleep(50); //10
										}
									}
									catch (Exception ex)
									{
										ReadErrorTime++;
										Thread.Sleep(50);
										AddToLog(LogLevels.Warning, "PZF ICP Response Error " + ReadErrorTime + ": " + ex.ToString() + " , Try to read again!");
									}
								}

								//PZFResponse = PZFBinaryReaderIcp.ReadBytes(PzfreadLength);
								if (PZFResponse != null
									&& PZFResponse.Length == PZFReadLength
									&& PZFResponse[0] == PZFTelegram[0]
									&& PZFResponse[1] == PZFTelegram[1]
									&& PZFResponse[2] == PZFTelegram[3]
									&& PZFResponse[PZFReadLength - 1] == 4)
								{
									GCHandle gch = GCHandle.Alloc(PZFResponse, GCHandleType.Pinned);
									try
									{
										PZFStatus = (PzfStatusStruct)Marshal.PtrToStructure(gch.AddrOfPinnedObject(), typeof(PzfStatusStruct));
										//_pzfLastOnline = DateTime.Now;

										if (PZFStatus.cBundlesHandledCount > 0)
										{
											for (int i = 0; i < PZFStatus.cBundlesHandledCount; i++)
											{
												PzfBundleHandledStruct PZFbundleHandled = PZFStatus.cBundlesHandled[i];

												TopSheetItem tempItem = new TopSheetItem
												{
													BundleId = PZFbundleHandled.bundleId,
													TitleGroupId = TitleGroupId,
													IssueDateString = IssueDateString,
													TopSheetType = TopSheetTypes.NORMAL,
													LabelState = 0
												};

												//lock (_lock_TsBundleQueue)
												{
													bool PZFExisted = TsBundleQueue.Contains(tempItem);

													AddToLog("PZFICP Control GripperLineId: " + _gripperLineTableId +
														" - TitleGroupId: " + TitleGroupId.ToString() +
														" - BundleId: " + PZFbundleHandled.bundleId.ToString() +
														" - cCode: " + PZFbundleHandled.cCode.ToString());

													if (!PZFExisted && PZFbundleHandled.cCode == 10) //1: PZF entrance; 10: go to PZF table
													{
														lock (_lock_TsBundleQueue)
														{
															TsBundleQueue.Enqueue(tempItem);
															//curEditionTableId = PzfbundleHandled.editionTableId;

															tempItem.EjectTime = DateTime.Now;
															try
															{
																AddToLog("PZF Eject Bundle " + tempItem.BundleId.ToString() +
															  " RouteSortIndex " + tempItem.RouteSortIndex.ToString() +
															  " BundleSortIndex " + tempItem.BundleSortIndex.ToString() +
															  " BundleIndex " + tempItem.BundleIndex.ToString() +
															  " into TsBundleQueue, TsBundleQueueCount: " + TsBundleQueue.Count +
															  "  - LableStates: " + tempItem.LabelState.ToString() +
															  "  - EjectedTime: " + tempItem.EjectTime);
															}
															catch
															{
															}
														}

														lock (_lock_PZFBundleList)
														{
															for (int temp = 0; temp < PZFBundleList.Count; temp++)
															{
																if (PZFBundleList[temp].BundleId == PZFbundleHandled.bundleId)
																{
																	PZFBundleList.RemoveAt(temp);
																	break;
																}
															}
														}

														//Debug.WriteLine("InsideInfo " + PzfbundleHandled.bundleId.ToString() + " - bundleId: " + PzfbundleHandled.ablId + "   ablId  " + PzfbundleHandled.cCode + "   cCode  ");

														TopsheetMainDBSetBundleStatus(PZFbundleHandled.bundleId, 12); // on Topsheet Line

														//string sql = string.Format("exec UniStack_LCCSetBundleStatus {0}, {1}, {2}, '{3}', {4}, {5}"
														//    , _gripperLineTableId
														//    , _topSheetLineTableId
														//    , TitleGroupId
														//    , IssueDateString
														//    , PzfbundleHandled.bundleId
														//    , 12
														//    );

														//PzfDbAddCommandToQueue(PzfDbCommandTypes.ExecuteNonQuery, sql, null, true);
													}
													else if (PZFbundleHandled.cCode == 1)
													{
														PZFBundleList.Add(tempItem);
														TopsheetMainDBSetBundleStatus(PZFbundleHandled.bundleId, 11); // On PZF

														//string sql = string.Format("exec UniStack_LCCSetBundleStatus {0}, {1}, {2}, '{3}', {4},{5}"
														//    , _gripperLineTableId
														//    , _topSheetLineTableId
														//    , TitleGroupId
														//    , IssueDateString
														//    , PzfbundleHandled.bundleId
														//    , 11
														//    );

														//PzfDbAddCommandToQueue(PzfDbCommandTypes.ExecuteNonQuery, sql, null, true);
													}
													else if (PZFbundleHandled.cCode == 20)
													{
														TopsheetMainDBSetBundleStatus(PZFbundleHandled.bundleId, 110); // PZF Error

														//string sql = string.Format("exec UniStack_LCCSetBundleStatus {0}, {1}, {2}, '{3}', {4},{5}"
														//    , _gripperLineTableId
														//    , _topSheetLineTableId
														//    , TitleGroupId
														//    , IssueDateString
														//    , PzfbundleHandled.bundleId
														//    , 110
														//    );

														AddToLog("PZF cCode 20 TitleGroup: " + TitleGroupId.ToString() +
															" bundleid: " + PZFbundleHandled.bundleId.ToString()
														   + " - cCode: " + PZFbundleHandled.cCode.ToString());

														//PzfDbAddCommandToQueue(PzfDbCommandTypes.ExecuteNonQuery, sql, null, true);

														if (PZFbundleHandled.bundleId >= 10000 && PZFbundleHandled.bundleId < 50000)
														{

															lock (_lock_PZFBundleList)
															{
																if (PZFBundleList != null)
																{
																	for (int temp = 0; temp < PZFBundleList.Count; temp++)
																	{
																		if (PZFBundleList[temp].BundleId == PZFbundleHandled.bundleId)
																		{
																			PZFBundleList.RemoveAt(temp);
																			break;
																		}
																	}
																}
															}

															if (PZFbundleHandled.bundleId != _lastRemoveBundleId)
															{
																TopSheetItem queueItem = new TopSheetItem
																{
																	BundleId = PZFbundleHandled.bundleId,
																	BundleIndex = 0,
																	LabelState = 51,
																	TitleGroupId = TitleGroupId,
																	IssueDateString = IssueDateString,
																	TopSheetType = TopSheetTypes.NORMAL
																};

																TopSheetItem clearItem = new TopSheetItem
																{
																	BundleId = 65500,
																	BundleIndex = 0,
																	PrinterNumber = DefaultPrinter,
																	TopSheetType = TopSheetTypes.WHITEPAPER,
																	LabelState = 50
																};

																lock (_lock_TsBundleQueue)
																{
																	TsBundleQueue.Enqueue(queueItem);
																	TsBundleQueue.Enqueue(clearItem);
																	TsBundleQueue.Enqueue(clearItem);
																}
																//_topSheetEjectedQueue.Add(queueItem);

																AddToLog("PZF Bundle Error, HandleCode 20, Ejected Bundle: " + queueItem.BundleId + " to TsBundleQueue.");
																_lastRemoveBundleId = PZFbundleHandled.bundleId;
															}

														}
													}
													else if (PZFExisted)
													{
														AddToLog(LogLevels.Warning, "PZFICP Control HandleBundleMessage" +
														" - BundleId: " + PZFbundleHandled.bundleId.ToString() +
														" - cCode: " + PZFbundleHandled.cCode.ToString() +
														" Already Existed in TsBundleQueue");
													}
												}
											}
										}
										PZFStatusReceived = true;
									}
									catch (Exception ex)
									{
										Debug.WriteLine(ex.ToString());

										throw (ex);
									}
									finally
									{
										gch.Free();
									}

								}
								else
								{
									//AddToLog(LogLevels.Warning, "Error in PZF ICP response header - not in sync");
									if (PZFResponse != null && PZFTelegram != null)
									{
										AddToLog(LogLevels.Warning, "Error in PZF ICP response header - not in sync, PZFResponse Length: "
										+ PZFResponse.Length + " Read Length: " + PZFReadLength + ", PZFResponse[0]: " + PZFResponse[0]
										+ ", PZFResponse[1]: " + PZFResponse[1] + "PZFResponse[2]: " + PZFResponse[2] + " PZFResponse[readLength-1]: " + PZFResponse[PZFReadLength - 1]
										+ "telegram[0]: " + PZFTelegram[0] + " telegram[1]: " + PZFTelegram[1] + " telegram[3]: " + PZFTelegram[3]);
									}
									else
									{
										AddToLog(LogLevels.Warning, "PZFResponse is Null, BinaryReader Failed!");
									}

									PZFIcpDisconnect();
								}

								if (PZFDoReconnectIcp)
								{
									PZFDoReconnectIcp = false;
									PZFIcpDisconnect();
								}
							}

							if (PZFIcpConnectionState == IcpConnectionStates.Connected &&
								PZFStatus.STBMotor == 1)
							{
								if (!isPZFReady)
								{
									isPZFReady = true;
								}
							}
							else
							{
								if (isPZFReady)
								{
									ResetPZF(true);
								}
								isPZFReady = false;
							}
						}
						catch (Exception ex)
						{
							AddToLog(LogLevels.Warning, ex.ToString());
							PZFIcpDisconnect();
							Thread.Sleep(1000);
						}
						finally
						{
							int index = 0;
							do
							{
								Thread.Sleep(10);
							}
							while (PzfIcpTelegramList_GetCount() == 0 && ++index < 20);
						}
					}
					catch
					{
					}
				}

				if (PZFIcpThread.ManagedThreadId != Thread.CurrentThread.ManagedThreadId)
				{
					AddToLog(LogLevels.Debug, "PZF IcpControl old thread exited");
				}
				else
				{
					PZFIcpDisconnect();
					PZFIcpConnectionState = IcpConnectionStates.NotRunning;
					AddToLog(LogLevels.Debug, "PZF IcpControl stopped");
				}
			}
			catch
			{
			}
			finally
			{
				Thread.Sleep(1);
			}
		}

		private void TopsheetMainDBSetBundleStatus(ushort bundleId, int BundleStatusCode)
		{
			string sql = string.Format("exec UniStack_LCCSetBundleStatus {0}, {1}, {2}, '{3}', {4}, {5}"
				, _gripperLineTableId
				, _topSheetLineTableId
				, TitleGroupId
				, IssueDateString
				, bundleId
				, BundleStatusCode);

			AddToLog(LogLevels.Debug, "TopsheetMainDBSetBundleStatus, BundleId: " + bundleId + " BundleStatusCode: " + BundleStatusCode);
			DbAddCommandToQueue(DbCommandTypes.ExecuteNonQuery, sql, null, true);
		}

		#region Add telegram

		//public void TestSendSetupBA()
		//{
		//    IcpGetOrAddTelegram(IcpTelegramTypes.SendSetup, null, true);
		//    //IcpGetOrAddTelegramHauk(IcpTelegramTypes.SendSetup, null, true);
		//}

		//public void IcpAddTelegramSendEjectedBundleIndex(uint bundleIndex)
		//{
		//    object oParameters = bundleIndex;
		//    IcpGetOrAddTelegram(IcpTelegramTypes.SendEjectedBundleIndex, oParameters, true);
		//}

		//public void IcpAddTelegramSendPrintedBundleIndex(uint bundleIndex, byte printerNumber)
		//{
		//    object oParameters = new object[] { bundleIndex, printerNumber };
		//    IcpGetOrAddTelegram(IcpTelegramTypes.SendPrintedBundleIndex, oParameters, true);
		//}

		public byte[] IcpGetOrAddTelegram(IcpTelegramTypes icpTelegramType,
			object oParameters, bool doAddToList)
		{
			byte[] result = null;

			//byte[] bParameters;
			byte pcStatus = _pcStatus;// 2;
		   

			if ((IsPaused1 || IsPausedByOperator1) && (!UseTwoPrinters || IsPaused2 || IsPausedByOperator2))
			{
				pcStatus = 3;
			}

			//pcStatus = 6; //just for test

			/*
				LabelWithoutTracking = 1,
				LabelWithTracking = 2,
				NoLabel = 3
				SimulateBundleIndex = 4,
				SimulateLabelIndex = 5,
				SimulateBundleAndLabelIndex = 6
			*/

			List<byte> byteList;
			int size;
			byte sizeL;
			byte sizeH;
			byte[] bytes;
			uint bundleIndex;
			uint bundleId;
			uint ArmDownTime;
			byte printerNumber;
			//byte AlarmStatus;
			byte low;
			byte high;
			//object[] oParameterArray;
			TopSheetItem topSheetItem;
			//byte printerStatus1 = _printer[0] != null && _printer[0].PrinterStatus != null &&
			//    _printer[0].PrinterStatus.IsReadyToPrint ? (byte)1 : (byte)0;
			//byte printerStatus2 = _printer[1] != null && _printer[1].PrinterStatus != null &&
			//    _printer[1].PrinterStatus.IsReadyToPrint ? (byte)1 : (byte)0;

			byte printerStatus1 = PcStatus_printer1;
			byte printerStatus2 = PcStatus_Printer2;

			if (_tcpClientIcp != null && _tcpClientIcp.Client != null && _tcpClientIcp.Client.Connected)
			{
				if (_telegramId < 99)
				{
					++_telegramId;
				}
				else
				{
					_telegramId = 1;
				}

				switch (icpTelegramType)
				{
					case IcpTelegramTypes.RequestStatus://A
						//pcStatus = 5;//request packets and positions status

						if (_telegramRequestStatus == null)
						{
							int expectedStructSize = _statusStructSize - 6;
							_telegramRequestStatus = new byte[] {
								SOH, _telegramId, pcStatus, (byte)'A', 11, 0,
								(byte)(expectedStructSize % 256),
								(byte)(expectedStructSize / 256),
								printerStatus1, printerStatus2, EOT };
						}
						else
						{
							_telegramRequestStatus[1] = _telegramId;
							_telegramRequestStatus[2] = pcStatus;
							_telegramRequestStatus[8] = printerStatus1;
							_telegramRequestStatus[9] = printerStatus2;
						}

						result = _telegramRequestStatus;
						break;

					case IcpTelegramTypes.SendEjectedBundleIndex://B
						//bundleIndex = (ushort)oParameters;
						_resetDone = false;
						topSheetItem = (TopSheetItem)oParameters;
						byte _isNoTopsheet;
						byte _isSTDBundle;

						if (topSheetItem.IsNoTopsheet)
						{
							_isNoTopsheet = 1;
						}
						else
						{
							_isNoTopsheet = 0;
						}

						if (topSheetItem.IsSTDBundle)
						{
							_isSTDBundle = 1;
						}
						else
						{
							_isSTDBundle = 0;
						}
						bundleIndex = topSheetItem.BundleIndex;
						bundleId = topSheetItem.BundleId;
						low = (byte)(bundleIndex & 0xFF);
						high = (byte)(bundleIndex / 0x100);
						result = new byte[] { SOH, GetTelegramId(), (byte)pcStatus,
							(byte)'B', 19, 0, low, high,
							(byte)(bundleId & 0xFF),
							(byte)(bundleId / 0x100),
							0,//(byte)topSheetItem.FirstLastBundle, // Default as Normal 
							_isNoTopsheet,
							//(byte)(TitleGroupId & 0xFF),
							//(byte)(TitleGroupId / 0x100),
							(byte)(topSheetItem.RouteSortIndex & 0xFF),
							(byte)(topSheetItem.RouteSortIndex / 0x100),
							(byte)(topSheetItem.BundleSortIndex & 0xFF),
							(byte)(topSheetItem.BundleSortIndex / 0x100),
							_isSTDBundle,
							(byte)topSheetItem.RouteModeId,
							//(byte)(topSheetItem.BundleSeqInRoute & 0xFF),
							//(byte)(topSheetItem.BundleSeqInRoute / 0x100),
							//(byte)(topSheetItem.BundleSeqInOrder & 0xFF),
							//(byte)(topSheetItem.BundleSeqInOrder / 0x100),
							EOT };
						break;

					case IcpTelegramTypes.SendPrintedBundleIndex://C
						_resetDone = false;
						topSheetItem = (TopSheetItem)oParameters;
						bundleIndex = topSheetItem.BundleIndex;// (ushort)oParameterArray[0];
						bundleId = topSheetItem.BundleId;
						printerNumber = topSheetItem.PrinterNumber;
						low = (byte)(bundleIndex & 0xFF);
						high = (byte)(bundleIndex / 0x100);
						result = new byte[] { SOH, GetTelegramId(), (byte)pcStatus, (byte)'C', 13, 0,
							low, high,
							//BoolToByte(topSheetItem.IsReject),
							(byte)(bundleId & 0xFF),
							(byte)(bundleId / 0x100),
							//(byte)(topSheetItem.EditionTableId & 0xFF),
							//(byte)(topSheetItem.EditionTableId / 0x100),
							printerNumber,
							topSheetItem.LabelState,
							EOT };
						break;

					case IcpTelegramTypes.SendSetup://D
						bytes = GetSetupStructBytes();
						size = 7 + bytes.Length;
						sizeL = (byte)(size % 256);
						sizeH = (byte)(size / 256);
						byteList = new List<byte>
						{
							SOH,
							_telegramId,
							pcStatus,
							(byte)'D',
							sizeL,
							sizeH
						};

						byteList.AddRange(bytes);

						byteList.Add(EOT);

						result = byteList.ToArray();
						break;

					case IcpTelegramTypes.Reset://E
						result = new byte[] { SOH, GetTelegramId(), (byte)pcStatus, (byte)'E', 7, 0, EOT };
						break;

					//case IcpTelegramTypes.SetDigitalOutput://G
					//    //SOH + ID + PCSTAT + G + LEN + SLOT + UTGANG + STATE + EOT
					//    bParameters = (byte[])oParameters;
					//    byte slot = bParameters[0];
					//    byte output = bParameters[1];
					//    byte state = bParameters[2];
					//    result = new byte[] { SOH, GetTelegramId(), pcStatus, (byte)'G', 10, 0, slot, output, state, EOT };
					//    break;

					case IcpTelegramTypes.SendStopApplication://H
						result = new byte[] { SOH, GetTelegramId(), pcStatus, (byte)'H', 7, 0, EOT };
						break;

					case IcpTelegramTypes.StartPaperTransport: //I
						printerNumber = (byte)oParameters;
						result = new byte[] { SOH, GetTelegramId(), pcStatus, (byte)'I', 9, 0, printerNumber, 1, EOT };
						break;

					case IcpTelegramTypes.StopPaperTransport: //I
						printerNumber = (byte)oParameters;
						result = new byte[] { SOH, GetTelegramId(), pcStatus, (byte)'I', 9, 0, printerNumber, 0, EOT };
						break;

					case IcpTelegramTypes.EnableCutter: //J
						printerNumber = (byte)oParameters;
						result = new byte[] { SOH, GetTelegramId(), pcStatus, (byte)'J', 8, 0, printerNumber,EOT };
						break;

					case IcpTelegramTypes.ClearLabelQueue: //K
						printerNumber = (byte)oParameters;
						result = new byte[] { SOH, GetTelegramId(), pcStatus, (byte)'K', 9, 0, printerNumber, 1, EOT };
						//result = new byte[] { SOH, GetTelegramId(), pcStatus, (byte)'K', 8, 0, printerNumber, EOT };

						break;

					//case IcpTelegramTypes.ResumeClearLabelQueue: //K
					//    printerNumber = (byte)oParameters;
					//    result = new byte[] { SOH, GetTelegramId(), pcStatus, (byte)'K', 9, 0, printerNumber, 2, EOT };
					//    break;

					case IcpTelegramTypes.EnableSimulationMode: //S
						result = new byte[] { SOH, GetTelegramId(), pcStatus, (byte)'S', 7, 0, EOT };
						break;

					case IcpTelegramTypes.DisableSimulationMode: //T
						result = new byte[] { SOH, GetTelegramId(), pcStatus, (byte)'T', 7, 0, EOT };
						break;

					case IcpTelegramTypes.ResetPrinter: //U
						printerNumber = (byte)oParameters;
						result = new byte[] { SOH, GetTelegramId(), pcStatus, (byte)'U', 8, 0, printerNumber, EOT };
						break;

					case IcpTelegramTypes.PrinterLightAlarmOn: //V 1: Light blink 0: Light blink off
						printerNumber = (byte)oParameters;   
						result = new byte[] { SOH, GetTelegramId(), pcStatus, (byte)'V', 9, 0, printerNumber,1, EOT };
						break;
					case IcpTelegramTypes.PrinterLightAlarmOff: //V 1: Light blink 0: Light blink off
						printerNumber = (byte)oParameters;
						result = new byte[] { SOH, GetTelegramId(), pcStatus, (byte)'V', 9, 0, printerNumber, 0, EOT };
						break;
					case IcpTelegramTypes.ArmDownParameterSet: //W 1: Light blink 0: Light blink off
						ArmDownTime = (uint)oParameters;

						byte armDownhigh = (byte)(ArmDownTime & 0xFF);
						byte armDownLow = (byte)(ArmDownTime / 0x100);

						result = new byte[] { SOH, GetTelegramId(), pcStatus, (byte)'W', 9, 0, armDownhigh,armDownLow, EOT };
						break;

				}

				if (doAddToList)
				{
					IcpTelegramList_Add(result);

				}
			}

			return result;
		}

		private byte[] PZFIcpGetOrAddTelegram(PZFIcpTelegramTypes PzficpTelegramType,
			object oParameters, bool PzfdoAddToList)
		{
			byte[] result = null;
			byte pcStatus = PZFPcStatus;
			List<byte> byteList;
			int size;
			byte sizeL;
			byte sizeH;
			byte[] bytes;
			//uint bundleIndex;
			uint bundleId;
			byte isNoTopsheet;
			byte bundleIdLow;
			byte bundleIdHigh;
			//PZFItem pzfItem;
			ushort routeSortIndex;
			ushort bundleSortIndex;
			TopSheetItem bundleItem;

			try
			{
				if (PZFTcpClientIcp != null && PZFTcpClientIcp.Client != null && PZFTcpClientIcp.Client.Connected)
				{
					if (PZFTelegramId < 99)
					{
						++PZFTelegramId;
					}
					else
					{
						PZFTelegramId = 1;
					}

					switch (PzficpTelegramType)
					{
						case PZFIcpTelegramTypes.RequestStatus://A
							if (PZFTelegramRequestStatus == null)
							{
								int expectedStructSize = PZFSatusStructSize - 6;
								PZFTelegramRequestStatus = new byte[] {
								SOH, PZFTelegramId, pcStatus, (byte)'A', 9, 0,
								(byte)(expectedStructSize % 256),
								(byte)(expectedStructSize / 256),
								EOT };
							}
							else
							{
								PZFTelegramRequestStatus[1] = PZFTelegramId;
								PZFTelegramRequestStatus[2] = pcStatus;
							}

							result = PZFTelegramRequestStatus;
							break;

						//case PZFIcpTelegramTypes.SendEjectedAblBundleId://B
						//    PZFResetDone = false;
						//    bundleItem = (TopSheetItem)oParameters;
						//    bundleId = bundleItem.BundleId;
						//    routeSortIndex = bundleItem.RouteSortIndex;
						//    bundleSortIndex = bundleItem.BundleSortIndex;
						//    bundleIdLow = (byte)(bundleId & 0xFF);//bundleIndex
						//    bundleIdHigh = (byte)(bundleId / 0x100);
						//    result = new byte[] { SOH, PZFGetTelegramId(),
						//        pcStatus,
						//        (byte)'B', 15, 0,
						//        bundleItem.AblId,
						//        bundleIdLow,
						//        bundleIdHigh,
						//        (byte)(routeSortIndex & 0xFF),
						//        (byte)(routeSortIndex / 0x100),
						//        (byte)(bundleSortIndex & 0xFF),
						//        (byte)(bundleSortIndex  / 0x100),
						//        (byte)bundleItem.FirstLastBundle,
						//        EOT };
						//    break;

						case PZFIcpTelegramTypes.SendEjectedAblBundleIdNew://B
							PZFResetDone = false;
							bundleItem = (TopSheetItem)oParameters;
							bundleId = bundleItem.BundleId;
							routeSortIndex = bundleItem.RouteSortIndex;
							bundleSortIndex = bundleItem.BundleSortIndex;
							bundleIdLow = (byte)(bundleId & 0xFF);//bundleIndex
							bundleIdHigh = (byte)(bundleId / 0x100);
							if (bundleItem.IsNoTopsheet)
							{
								isNoTopsheet = 1;
							}
							else
							{
								isNoTopsheet = 0;
							}

							result = new byte[] { SOH, PZFGetTelegramId(),
								pcStatus,
								(byte)'B', 16, 0,
								bundleItem.AblId,
								bundleIdLow,
								bundleIdHigh,
								(byte)(routeSortIndex & 0xFF),
								(byte)(routeSortIndex / 0x100),
								(byte)(bundleSortIndex & 0xFF),
								(byte)(bundleSortIndex  / 0x100),
								(byte)bundleItem.FirstLastBundle,
								(byte)isNoTopsheet,
								EOT };
							break;

						case PZFIcpTelegramTypes.SendSetup://D
							bytes = PZFGetSetupStructBytes();
							size = 7 + bytes.Length;
							sizeL = (byte)(size % 256);
							sizeH = (byte)(size / 256);
							byteList = new List<byte>
							{
								SOH,
								PZFTelegramId,
								pcStatus,
								(byte)'D',
								sizeL,
								sizeH
							};

							byteList.AddRange(bytes);
							byteList.Add(EOT);
							result = byteList.ToArray();
							break;

						case PZFIcpTelegramTypes.Reset://E
							result = new byte[] { SOH, PZFGetTelegramId(), pcStatus, (byte)'E', 7, 0, EOT };
							break;

						case PZFIcpTelegramTypes.StopPZF://F
							result = new byte[] { SOH, PZFGetTelegramId(), pcStatus, (byte)'F', 7, 0, EOT };
							break;

						case PZFIcpTelegramTypes.EnableSimulation:// I
							result = new byte[] { SOH, PZFGetTelegramId(), pcStatus, (byte)'I', 7, 0, EOT };
							break;

						case PZFIcpTelegramTypes.DisableSimulation:// J
							result = new byte[] { SOH, PZFGetTelegramId(), pcStatus, (byte)'J', 7, 0, EOT };
							break;
						//case IcpTelegramTypes.SendStopApplication://H
						//    result = new byte[] { SOH, GetTelegramId(), pcStatus, (byte)'H', 7, 0, EOT };
						//    break;
						case PZFIcpTelegramTypes.SendBundleMessage:// K
							bundleItem = (TopSheetItem)oParameters;
							bundleId = bundleItem.BundleId;
							routeSortIndex = bundleItem.RouteSortIndex;
							bundleSortIndex = bundleItem.BundleSortIndex;
							bundleIdLow = (byte)(bundleId & 0xFF);//bundleIndex
							bundleIdHigh = (byte)(bundleId / 0x100);
							result = new byte[] { SOH, PZFGetTelegramId(),
								pcStatus,
								(byte)'K', 14, 0,
								bundleItem.AblId,
								bundleIdLow,
								bundleIdHigh,
								(byte)(routeSortIndex & 0xFF),
								(byte)(routeSortIndex / 0x100),
								(byte)(bundleSortIndex & 0xFF),
								(byte)(bundleSortIndex  / 0x100),
								EOT };
							break;

					}

					if (PzfdoAddToList)
					{
						PzfIcpTelegramList_Add(result);
					}
				}
			}
			catch
			{
			}

			return result;
		}

		private void IcpTelegramList_Add(byte[] telegram)
		{
			try
			{
				if (telegram != null && telegram.Length > 3)
				{
					lock (_lockSTBTelegramListIcp)
					{
						_telegramListIcp.Add(telegram);

						if (telegram[3] == (byte)'B')
						{
							AddToLog(LogLevels.Warning, "IcpTelegramList_Add 'B', Count: " + _telegramListIcp.Count + " ,BundleId: " + (int.Parse(Convert.ToString(telegram[8], 10)) + int.Parse(Convert.ToString(telegram[9] * 0x100, 10))));
						}
						else
						{
							AddToLog(LogLevels.Warning, "IcpTelegramList_Add " + telegram[3].ToString() + ", Count: " + _telegramListIcp.Count);
						}
					}
				}
			}
			catch
			{
			}
		}

		private void PzfIcpTelegramList_Add(byte[] Pzftelegram)
		{
			try
			{
				if (Pzftelegram != null && Pzftelegram.Length > 0)
				{
					lock (PZFLockTelegramListIcp)
					{
						PZFTelegramListIcp.Add(Pzftelegram);
					}
				}
			}
			catch
			{
			}

		}

		private byte[] IcpTelegramList_GetNext()
		{
			byte[] result = null;

			lock (_lockSTBTelegramListIcp)
			{
				if (_telegramListIcp.Count > 0)
				{
					//for (int i = 0; i < _telegramListIcp.Count; i++)
					//{
					//    if (_telegramListIcp[i][3] == (byte)'J')
					//    {
					//        result = _telegramListIcp[i];
					//        _telegramListIcp.RemoveAt(i);
					//        break;
					//    }
					//    else
					//    {
							result = _telegramListIcp[0];
							_telegramListIcp.RemoveAt(0);
					//        break;
					//    }
					//}
				}
			}

			return result;
		}

		private byte[] PzfIcpTelegramList_GetNext()
		{
			byte[] result = null;
			try
			{
				lock (PZFLockTelegramListIcp)
				{
					if (PZFTelegramListIcp.Count > 0)
					{
						result = PZFTelegramListIcp[0];
						PZFTelegramListIcp.RemoveAt(0);
					}
				}
			}
			catch
			{
			}

			return result;
		}

		private void IcpTelegramList_Clear()
		{
			lock (_lockSTBTelegramListIcp)
			{
				_telegramListIcp.Clear();
			}
		}

		private void PZFIcpTelegramList_Clear()
		{
			lock (PZFLockTelegramListIcp)
			{
				PZFTelegramListIcp.Clear();
			}
		}

		private int IcpTelegramList_GetCount()
		{
			int result;
			//lock (_lockTelegramListIcp)
			{
				result = _telegramListIcp.Count;
			}
			return result;
		}

		private int PzfIcpTelegramList_GetCount()
		{
			int result = 0;
			try
			{
				//lock (PZFLockTelegramListIcp)
				{
					result = PZFTelegramListIcp.Count;
				}

				return result;
			}
			catch
			{
			}
			return result;
		}

		#endregion

		#region GetTelegramId

		private byte GetTelegramId()
		{
			byte result;

			lock (_lockTelegram)
			{
				if (_telegramId == 255)
				{
					_telegramId = 0;
				}
				result = _telegramId++;
			}

			return result;
		}

		private byte PZFGetTelegramId()
		{
			byte result;

			lock (PZFLockTelegram)
			{
				if (PZFTelegramId == 255)
				{
					PZFTelegramId = 0;
				}
				result = PZFTelegramId++;
			}

			return result;
		}
		#endregion

		#region Get struct bytes

		private byte[] GetSetupStructBytes()
		{
			int len = _setupStructSize;
			byte[] result = new byte[len];

			IntPtr ptr = Marshal.AllocHGlobal(len);
			Marshal.StructureToPtr(_setup, ptr, true);
			Marshal.Copy(ptr, result, 0, len);

			Marshal.FreeHGlobal(ptr);

			return result;
		}

		private byte[] PZFGetSetupStructBytes()
		{
			try
			{
				int len = PZFSetupStructSize;
				byte[] result = new byte[len];

				IntPtr ptr = Marshal.AllocHGlobal(len);
				Marshal.StructureToPtr(PZFSetup, ptr, true);
				Marshal.Copy(ptr, result, 0, len);

				Marshal.FreeHGlobal(ptr);

				return result;
			}
			catch
			{
				throw;
			}
		}
		#endregion

		private void IcpDisconnect()
		{
			AddToLog(LogLevels.Warning, "IcpDisconnect");

			IcpConnectionState = IcpConnectionStates.Disconnected;

			//_telegramListIcp.Clear();
			IcpTelegramList_Clear();
			//_doReconnectDb = true;
			_setupReceived = false;
			try
			{
				if (_con != null && _con.State == ConnectionState.Open)
				{
					DbGetSetup();
					DbGetItems();
				}
			}
			catch
			{
			}

			if (_binaryReaderIcp != null)
			{
				try
				{
					_binaryReaderIcp.Close();
					_binaryReaderIcp.Dispose();
				}
				catch
				{
				}
				finally
				{
					_binaryReaderIcp = null;
				}
			}

			if (_binaryWriterIcp != null)
			{
				try
				{
					_binaryWriterIcp.Close();
					_binaryWriterIcp.Dispose();
				}
				catch
				{
				}
				finally
				{
					_binaryWriterIcp = null;
				}
			}

			if (_networkStreamIcp != null)
			{
				try
				{
					_networkStreamIcp.Close();
					_networkStreamIcp.Dispose();
				}
				catch
				{
				}
				finally
				{
					_networkStreamIcp = null;
				}
			}

			if (_tcpClientIcp != null)
			{
				try
				{
					_tcpClientIcp.Client.Close();
				}
				catch
				{
				}

				try
				{
					_tcpClientIcp.Close();
				}
				catch
				{
				}
				finally
				{
					_tcpClientIcp = null;
				}
			}
		}

		private void PZFIcpDisconnect()
		{
			try
			{
				AddToLog(LogLevels.Warning, "PZFIcpDisconnect");

				PZFIcpConnectionState = IcpConnectionStates.Disconnected;

				//_telegramListIcp.Clear();
				PZFIcpTelegramList_Clear();
				//_doReconnectDb = true;
				//_setupReceived = false;
				//try
				//{
				//    if (_Pzfcon != null && _Pzfcon.State == ConnectionState.Open)
				//    {
				//        //DbGetSetup();
				//        //DbGetItems();
				//    }
				//}
				//catch
				//{
				//}

				if (PZFBinaryReaderIcp != null)
				{
					try
					{
						PZFBinaryReaderIcp.Close();
						PZFBinaryReaderIcp.Dispose();
					}
					catch
					{
					}
					finally
					{
						PZFBinaryReaderIcp = null;
					}
				}

				if (PZFBinaryWriterIcp != null)
				{
					try
					{
						PZFBinaryWriterIcp.Close();
						PZFBinaryWriterIcp.Dispose();
					}
					catch
					{
					}
					finally
					{
						PZFBinaryWriterIcp = null;
					}
				}

				if (PZFNetworkStreamIcp != null)
				{
					try
					{
						PZFNetworkStreamIcp.Close();
						PZFNetworkStreamIcp.Dispose();
					}
					catch
					{
					}
					finally
					{
						PZFNetworkStreamIcp = null;
					}
				}

				if (PZFTcpClientIcp != null)
				{
					try
					{
						PZFTcpClientIcp.Client.Close();
					}
					catch
					{
					}

					try
					{
						PZFTcpClientIcp.Close();
					}
					catch
					{
					}
					finally
					{
						PZFTcpClientIcp = null;
					}
				}
			}
			catch
			{
				throw;
			}

		}
		#endregion

		#region Database communication

		public void DbReconnect()
		{
			_doReconnectDb = true;
		}

		private void DbControl()
		{
			try
			{
				string sql;
				object oParams;
				//OleDbCommand cmd;
				byte sleepCounter;
				DbCommandStuct dbCommand;
				DateTime lastRequestCommand = DateTime.MinValue;
				DbCommandTypes currentCommand;

				AddToLog(LogLevels.Debug, "DbControl started");

				while (IsStarted && 
					_dbThread.ManagedThreadId == Thread.CurrentThread.ManagedThreadId)
				{
					try
					{
						try
						{
							if (_con == null || _con.State != ConnectionState.Open)
							{
								DbConnectionState = DbConnectionStates.Disconnected;

								if (_connectionString != null && _connectionString.Length > 0)
								{
									_con = new OleDbConnection(_connectionString);

									DbConnectionState = DbConnectionStates.Connecting;

									_con.Open();

									if (_con != null && _con.State == ConnectionState.Open)
									{
										DbConnectionState = DbConnectionStates.Connected;

										//sql = "select top 1 TopSheetIOIP from UniStack_TopSheetLineTable where Id = " + _topSheetLineTableId.ToString();
										//cmd = new OleDbCommand(sql, _con);
										//_ioIp = "192.168.2.12";// (string)cmd.ExecuteScalar();

										//sql = "select top 1 TopSheetIOPort from UniStack_TopSheetLineTable where Id = " + _topSheetLineTableId.ToString();
										//cmd = new OleDbCommand(sql, _con);
										//_ioPort = 10000;// (int)cmd.ExecuteScalar();

										_setupReceived = false;
										//if (!_setupReceived)
										{
											DbGetSetup();
											DbGetItems();
										}

										_doReconnectIcp = true;
									}
								}
							}

							if (_con != null && _con.State == ConnectionState.Open)
							{
								sql = null;
								currentCommand = DbCommandTypes.None;
								oParams = null;

								if (lastRequestCommand < DateTime.Now.AddMilliseconds(-1000))
								{
									lastRequestCommand = DateTime.Now;

									sql = string.Format("DECLARE @return_value int;declare @IOTopSheetCommand int;"
										+ " declare @TopSheetLineTableId int; set @TopSheetLineTableId={0}"
										+ " EXEC @return_value = UniStack_GetNextTopSheetCommand"
										+ " @TopSheetLineTableId,"
										+ " @IOTopSheetCommand output;SELECT 'Return Value' = @IOTopSheetCommand",
										_topSheetLineTableId);
									currentCommand = DbCommandTypes.GetNextCommand;
								}
								else
								{
									lock (_lockDbCommandList)
									{
										if (_dbCommandList.Count > 0)
										{
											dbCommand = _dbCommandList[0];
											_dbCommandList.RemoveAt(0);
											currentCommand = dbCommand.dbCommandType;
											sql = dbCommand.sql;
											oParams = dbCommand.oParams;
										}
									}
								}

								if (sql != null && currentCommand != DbCommandTypes.None)
								{
									DbExecuteCommand(currentCommand, sql, oParams);
								}

								sleepCounter = 0;
								do
								{
									Thread.Sleep(50);
								}
								while (++sleepCounter < 20 && _dbCommandList.Count == 0);

								if (_doReconnectDb)
								{
									_doReconnectDb = false;
									DbDisconnect();
								}
							}
						}
						catch (Exception ex)
						{
							AddToLog(ex);

							DbDisconnect();
						}
						finally
						{
							Thread.Sleep(10);
						}
					}
					catch
					{
					}
				}

				if (_dbThread.ManagedThreadId != Thread.CurrentThread.ManagedThreadId)
				{
					AddToLog(LogLevels.Debug, "DbControl old thread exited");
				}
				else
				{
					DbDisconnect();
					AddToLog(LogLevels.Debug, "DbControl stopped");
					DbConnectionState = DbConnectionStates.NotRunning;
				}
			}
			catch
			{
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
		private void DbExecuteCommand(DbCommandTypes commandType, string sql, object oParams)
		{
			OleDbCommand cmd;
			OleDbDataReader dr = null;

			switch (commandType)
			{
				case DbCommandTypes.ExecuteNonQuery:
					cmd = new OleDbCommand(sql, _con);
					DBTools.HandleBusyExecuteNonQuery(cmd);
					break;

				case DbCommandTypes.GetNextCommand:
					cmd = new OleDbCommand(sql, _con);
					object o = DBTools.HandleBusyExecuteScalar(cmd);
					if (o != null && !(o is DBNull))
					{
						//string nextCommand = o as string;
						//if (nextCommand != null && nextCommand.Length > 0)
						//{
						//    char ioCommand = nextCommand[0];
						//    AddCommandFromDB(ioCommand);
						//}

						int iResult = (int)o;
						if (iResult > 0)
						{
							byte requestCommand = (byte)(iResult % 256);//bits 0-7 reserved (1-255) for request command
							if (requestCommand > 0)
							{
								char ioCommand = (char)requestCommand;
								AddCommandFromDB(ioCommand);
							}
						}

						byte printerStates = (byte)((iResult & 0xFF00) / 0x100);//bits 8-15
						IsPaused1 = (printerStates & 1) > 0;
						IsPaused2 = (printerStates & 2) > 0;
					}
					break;

				case DbCommandTypes.GetSetup:
					cmd = new OleDbCommand(sql, _con);
					dr = DBTools.HandleBusyExecuteReader(cmd);

					if (dr.HasRows)
					{
						bool hasData = dr.Read();
						if (hasData)
						{
							_setup.LineNumber = (byte)_topSheetLineTableId;
							_setup.ApplicatorType = (byte)dr["TopSheetApplicatorTypeId"];

							_setup.CurrentStackerPosition = (byte)_currentStackerPositionId;// (byte)dr["CurrentStackerPosition"];

							_setup.LabelingConveyorPosition = (byte)dr["LabelingConveyorPosition"];
							_setup.FirstConveyorControlPosition = (byte)dr["FirstConveyorControlPosition"];
							_setup.ReadyToPreviousControlPosition = (byte)dr["ReadyToPreviousControlPosition"];

							_setup.InputSignalsFilter = (short)dr["InputSignalsFilter"];

							_setup.ButtonStartInputSlot = (byte)dr["ButtonStartInputSlot"];
							_setup.ButtonStartInputSignal = (byte)dr["ButtonStartInputSignal"];
							_setup.ButtonStartInputOnHigh = (byte)(((bool)dr["ButtonStartInputOnHigh"]) ? 1 : 0);

							_setup.ButtonStopInputSlot = (byte)dr["ButtonStopInputSlot"];
							_setup.ButtonStopInputSignal = (byte)dr["ButtonStopInputSignal"];
							_setup.ButtonStopInputOnHigh = (byte)(((bool)dr["ButtonStopInputOnHigh"]) ? 1 : 0);

							_setup.ButtonResetInputSlot = (byte)dr["ButtonResetInputSlot"];
							_setup.ButtonResetInputSignal = (byte)dr["ButtonResetInputSignal"];
							_setup.ButtonResetInputOnHigh = (byte)(((bool)dr["ButtonResetInputOnHigh"]) ? 1 : 0);

							_setup.ButtonStartConvInputSlot = (byte)dr["ButtonStartConvInputSlot"];
							_setup.ButtonStartConvInputSignal = (byte)dr["ButtonStartConvInputSignal"];
							_setup.ButtonStartConvInputOnHigh = (byte)(((bool)dr["ButtonStartConvInputOnHigh"]) ? 1 : 0);

							_setup.ButtonStopConvInputSlot = (byte)dr["ButtonStopConvInputSlot"];
							_setup.ButtonStopConvInputSignal = (byte)dr["ButtonStopConvInputSignal"];
							_setup.ButtonStopConvInputOnHigh = (byte)(((bool)dr["ButtonStopConvInputOnHigh"]) ? 1 : 0);

							_setup.StackerExitInputSlot = (byte)dr["StackerExitInputSlot"];
							_setup.StackerExitInputSignal = (byte)dr["StackerExitInputSignal"];
							_setup.StackerExitInputOnHigh = (byte)(((bool)dr["StackerExitInputOnHigh"]) ? 1 : 0);

							_setup.Function1InputSlot = (byte)dr["Function1InputSlot"];
							_setup.Function2InputSlot = (byte)dr["Function2InputSlot"];
							_setup.Function3InputSlot = (byte)dr["Function3InputSlot"];

							_setup.Function1InputSignal = (byte)dr["Function1InputSignal"];
							_setup.Function2InputSignal = (byte)dr["Function2InputSignal"];
							_setup.Function3InputSignal = (byte)dr["Function3InputSignal"];

							_setup.Function1InputOnHigh = (byte)(((bool)dr["Function1InputOnHigh"]) ? 1 : 0);
							_setup.Function2InputOnHigh = (byte)(((bool)dr["Function2InputOnHigh"]) ? 1 : 0);
							_setup.Function3InputOnHigh = (byte)(((bool)dr["Function3InputOnHigh"]) ? 1 : 0);

							_setup.ArmUpInputSlot = (byte)dr["ArmUpInputSlot"];
							_setup.ArmUpInputSignal = (byte)dr["ArmUpInputSignal"];
							_setup.ArmUpInputOnHigh = (byte)(((bool)dr["ArmUpInputOnHigh"]) ? 1 : 0);

							_setup.ArmOnBundleInputSlot = (byte)dr["ArmOnBundleInputSlot"];
							_setup.ArmOnBundleInputSignal = (byte)dr["ArmOnBundleInputSignal"];
							_setup.ArmOnBundleInputOnHigh = (byte)(((bool)dr["ArmOnBundleInputOnHigh"]) ? 1 : 0);

							_setup.RejectReadyInputSlot = (byte)dr["RejectReadyInputSlot"];
							_setup.RejectReadyInputSignal = (byte)dr["RejectReadyInputSignal"];
							_setup.RejectReadyInputOnHigh = (byte)(((bool)dr["RejectReadyInputOnHigh"]) ? 1 : 0);

							//_setup.Reject2ReadyInputSlot = (byte)dr["Reject2ReadyInputSlot"];
							//_setup.Reject2ReadyInputSignal = (byte)dr["Reject2ReadyInputSignal"];
							//_setup.Reject2ReadyInputOnHigh = (byte)(((bool)dr["Reject2ReadyInputOnHigh"]) ? 1 : 0);

							//_setup.RejectSheetInPosInputSlot= (byte)dr["RejectSheetInPosInputSlot"];
							//_setup.RejectSheetInPosInputSignal = (byte)dr["RejectSheetInPosInputSignal"];
							//_setup.RejectSheetInPosInputOnHigh = (byte)(((bool)dr["RejectSheetInPosInputOnHigh"]) ? 1 : 0);

							//_setup.Reject2SheetInPosInputSlot = (byte)dr["Reject2SheetInPosInputSlot"];
							//_setup.Reject2SheetInPosInputSignal = (byte)dr["Reject2SheetInPosInputSignal"];
							//_setup.Reject2SheetInPosInputOnHigh = (byte)(((bool)dr["Reject2SheetInPosInputOnHigh"]) ? 1 : 0);

							_setup.RejectBackInputSlot = (byte)dr["RejectBackInputSlot"];
							_setup.RejectBackInputSignal = (byte)dr["RejectBackInputSignal"];
							_setup.RejectBackInputOnHigh = (byte)(((bool)dr["RejectBackInputOnHigh"]) ? 1 : 0);

							_setup.AlarmThermoInputSlot = (byte)dr["AlarmThermoInputSlot"];
							_setup.AlarmThermoInputSignal = (byte)dr["AlarmThermoInputSignal"];
							_setup.AlarmThermoInputOnHigh = (byte)(((bool)dr["AlarmThermoInputOnHigh"]) ? 1 : 0);

							_setup.AlarmThermo2InputSlot = (byte)dr["AlarmThermo2InputSlot"];
							_setup.AlarmThermo2InputSignal = (byte)dr["AlarmThermo2InputSignal"];
							_setup.AlarmThermo2InputOnHigh = (byte)(((bool)dr["AlarmThermo2InputOnHigh"]) ? 1 : 0);

							_setup.EmergencyOffInputSlot = (byte)dr["EmergencyOffInputSlot"];
							_setup.EmergencyOffInputSignal = (byte)dr["EmergencyOffInputSignal"];
							_setup.EmergencyOffInputOnHigh = (byte)(((bool)dr["EmergencyOffInputOnHigh"]) ? 1 : 0);

							_setup.EmergencyOffConvInputSlot = (byte)dr["EmergencyOffConvInputSlot"];
							_setup.EmergencyOffConvInputSignal = (byte)dr["EmergencyOffConvInputSignal"];
							_setup.EmergencyOffConvInputOnHigh = (byte)(((bool)dr["EmergencyOffConvInputOnHigh"]) ? 1 : 0);

							_setup.EmergencyOffOutputSlot = (byte)dr["EmergencyOffOutputSlot"];
							_setup.EmergencyOffOutputSignal = (byte)dr["EmergencyOffOutputSignal"];

							_setup.EmergencyCircuitInputSlot = (byte)dr["EmergencyCircuitInputSlot"];
							_setup.EmergencyCircuitInputSignal = (byte)dr["EmergencyCircuitInputSignal"];
							_setup.EmergencyCircuitInputOnHigh = (byte)(((bool)dr["EmergencyCircuitInputOnHigh"]) ? 1 : 0);

							_setup.NextMachineReadySlot = (byte)dr["NextMachineReadySlot"];
							_setup.NextMachineReadySignal = (byte)dr["NextMachineReadySignal"];
							_setup.NextMachineReadyOnHigh = (byte)(((bool)dr["NextMachineReadyOnHigh"]) ? 1 : 0);

							_setup.PreviousMachineReadyOutputSlot = (byte)dr["PreviousMachineReadyOutputSlot"];
							_setup.PreviousMachineReadyOutputSignal = (byte)dr["PreviousMachineReadyOutputSignal"];

							_setup.PrevTransNotReadyOutputSlot = (byte)dr["PrevTransNotReadyOutputSlot"];
							_setup.PrevTransNotReadyOutputSignal = (byte)dr["PrevTransNotReadyOutputSignal"];

							_setup.NextMachineStandbyOutputSlot = (byte)dr["NextMachineStandbyOutputSlot"];
							_setup.NextMachineStandbyOutputSignal = (byte)dr["NextMachineStandbyOutputSignal"];

							_setup.ReadyPowerOutputSlot = (byte)dr["ReadyPowerOutputSlot"];
							_setup.ReadyPowerOutputSignal = (byte)dr["ReadyPowerOutputSignal"];

							_setup.ReadyAirOutputSlot = (byte)dr["ReadyAirOutputSlot"];
							_setup.ReadyAirOutputSignal = (byte)dr["ReadyAirOutputSignal"];

							_setup.ReadyConveyorsOutputSlot = (byte)dr["ReadyConveyorsOutputSlot"];
							_setup.ReadyConveyorsOutputSignal = (byte)dr["ReadyConveyorsOutputSignal"];

							_setup.ReadyBeltsOutputSlot = (byte)dr["ReadyBeltsOutputSlot"];
							_setup.ReadyBeltsOutputSignal = (byte)dr["ReadyBeltsOutputSignal"];

							_setup.StartLastConvButtonLightOutputSlot = (byte)dr["StartLastConvButtonLightOutputSlot"];
							_setup.StartLastConvButtonLightOutputSignal = (byte)dr["StartLastConvButtonLightOutputSignal"];

							_setup.StopLastConvButtonLightOutputSlot = (byte)dr["StopLastConvButtonLightOutputSlot"];
							_setup.StopLastConvButtonLightOutputSignal = (byte)dr["StopLastConvButtonLightOutputSignal"];

							_setup.ButtonStartLastConvInputSlot = (byte)dr["ButtonStartLastConvInputSlot"];
							_setup.ButtonStartLastConvInputSignal = (byte)dr["ButtonStartLastConvInputSignal"];
							_setup.ButtonStartLastConvInputOnHigh = (byte)(((bool)dr["ButtonStartLastConvInputOnHigh"]) ? 1 : 0);

							_setup.ButtonStopLastConvInputSlot = (byte)dr["ButtonStopLastConvInputSlot"];
							_setup.ButtonStopLastConvInputSignal = (byte)dr["ButtonStopLastConvInputSignal"];
							_setup.ButtonStopLastConvInputOnHigh = (byte)(((bool)dr["ButtonStopLastConvInputOnHigh"]) ? 1 : 0);

							_setup.ArmDownOutputSlot = (byte)dr["ArmDownOutputSlot"];
							_setup.ArmDownOutputSignal = (byte)dr["ArmDownOutputSignal"];

							_setup.ArmUpOutputSlot = (byte)dr["ArmUpOutputSlot"];
							_setup.ArmUpOutputSignal = (byte)dr["ArmUpOutputSignal"];

							_setup.ArmBackOutputSlot = (byte)dr["ArmBackOutputSlot"];
							_setup.ArmBackOutputSignal = (byte)dr["ArmBackOutputSignal"];

							_setup.ArmDownAfterLeaveDelay = (short)dr["ArmDownAfterLeaveDelay"];
							_setup.ArmDownMintime = (short)dr["ArmDownMintime"];
							_setup.ArmBackTimeout = (short)dr["ArmBackTimeout"];
							_setup.ArmDownTimeout = (short)dr["ArmDownTimeout"];
							_setup.ArmUpTimeout = (short)dr["ArmUpTimeout"];

							_setup.RejectDownOutputSlot = (byte)dr["RejectDownOutputSlot"];
							_setup.RejectDownOutputSignal = (byte)dr["RejectDownOutputSignal"];

							_setup.RejectBackOutputSlot = (byte)dr["RejectBackOutputSlot"];
							_setup.RejectBackOutputSignal = (byte)dr["RejectBackOutputSignal"];

							//_setup.Reject2DownOutputSlot = (byte)dr["Reject2DownOutputSlot"];
							//_setup.Reject2DownOutputSignal = (byte)dr["Reject2DownOutputSignal"];

							//_setup.Reject2BackOutputSlot = (byte)dr["Reject2BackOutputSlot"];
							//_setup.Reject2BackOutputSignal = (byte)dr["Reject2BackOutputSignal"];

							_setup.RejectDownTime = (short)dr["RejectDownTime"];
							_setup.RejectUpTime = (short)dr["RejectUpTime"];
							_setup.RejectBackTimeout = (short)dr["RejectBackTimeout"];
							_setup.RejectFrontTimeout = (short)dr["RejectFrontTimeout"];
							_setup.RejectWaitForLabelTimeout = (short)dr["RejectWaitForLabelTimeout"];
							_setup.RejectWaitForLabelGoneTimeout = (short)dr["RejectWaitForLabelGoneTimeout"];

							_setup.StartButtonLightOutputSlot = (byte)dr["StartButtonLightOutputSlot"];
							_setup.StartButtonLightOutputSignal = (byte)dr["StartButtonLightOutputSignal"];

							_setup.StopButtonLightOutputSlot = (byte)dr["StopButtonLightOutputSlot"];
							_setup.StopButtonLightOutputSignal = (byte)dr["StopButtonLightOutputSignal"];

							_setup.ResetButtonLightOutputSlot = (byte)dr["ResetButtonLightOutputSlot"];
							_setup.ResetButtonLightOutputSignal = (byte)dr["ResetButtonLightOutputSignal"];

							_setup.StartConvButtonLightOutputSlot = (byte)dr["StartConvButtonLightOutputSlot"];
							_setup.StartConvButtonLightOutputSignal = (byte)dr["StartConvButtonLightOutputSignal"];

							_setup.StopConvButtonLightOutputSlot = (byte)dr["StopConvButtonLightOutputSlot"];
							_setup.StopConvButtonLightOutputSignal = (byte)dr["StopConvButtonLightOutputSignal"];

							_setup.GreenLightOutputSlot = (byte)dr["GreenLightOutputSlot"];
							_setup.GreenLightOutputSignal = (byte)dr["GreenLightOutputSignal"];
							_setup.YellowLightOutputSlot = (byte)dr["YellowLightOutputSlot"];
							_setup.YellowLightOutputSignal = (byte)dr["YellowLightOutputSignal"];
							_setup.RedLightOutputSlot = (byte)dr["RedLightOutputSlot"];
							_setup.RedLightOutputSignal = (byte)dr["RedLightOutputSignal"];
							_setup.HornOutputSlot = (byte)dr["HornOutputSlot"];
							_setup.HornOutputSignal = (byte)dr["HornOutputSignal"];

							_setup.Printer1ActivateInputSlot = (byte)dr["Printer1ActivateInputSlot"];
							_setup.Printer1ActivateInputSignal = (byte)dr["Printer1ActivateInputSignal"];
							_setup.Printer1ActivateInputOnHigh = (byte)(((bool)dr["Printer1ActivateInputOnHigh"]) ? 1 : 0);

							_setup.Printer2ActivateInputSlot = (byte)dr["Printer2ActivateInputSlot"];
							_setup.Printer2ActivateInputSignal = (byte)dr["Printer2ActivateInputSignal"];
							_setup.Printer2ActivateInputOnHigh = (byte)(((bool)dr["Printer2ActivateInputOnHigh"]) ? 1 : 0);

							_setup.Printer1ActiveOutputSlot = (byte)dr["Printer1ActiveOutputSlot"];
							_setup.Printer1ActiveOutputSignal = (byte)dr["Printer1ActiveOutputSignal"];

							_setup.Printer2ActiveOutputSlot = (byte)dr["Printer2ActiveOutputSlot"];
							_setup.Printer2ActiveOutputSignal = (byte)dr["Printer2ActiveOutputSignal"];

							_setup.Gate1DownInputSlot = (byte)dr["Gate1DownInputSlot"];
							_setup.Gate1DownInputSignal = (byte)dr["Gate1DownInputSignal"];
							_setup.Gate1DownInputOnHigh = (byte)(((bool)dr["Gate1DownInputOnHigh"]) ? 1 : 0);

							_setup.Gate2DownInputSlot = (byte)dr["Gate2DownInputSlot"];
							_setup.Gate2DownInputSignal = (byte)dr["Gate2DownInputSignal"];
							_setup.Gate2DownInputOnHigh = (byte)(((bool)dr["Gate2DownInputOnHigh"]) ? 1 : 0);

							_setup.Gate2DownConfirmedInputSlot = (byte)dr["Gate2DownConfirmedInputSlot"];
							_setup.Gate2DownConfirmedInputSignal = (byte)dr["Gate2DownConfirmedInputSignal"];
							_setup.Gate2DownConfirmedInputOnHigh = (byte)(((bool)dr["Gate2DownConfirmedInputOnHigh"]) ? 1 : 0);

							_setup.StartGreenOnTime = (short)dr["StartGreenOnTime"];
							_setup.StartGreenOffTime = (short)dr["StartGreenOffTime"];
							_setup.AlarmYellowOnTime = (short)dr["AlarmYellowOnTime"];
							_setup.AlarmYellowOffTime = (short)dr["AlarmYellowOffTime"];
							_setup.EmergencyRedOnTime = (short)dr["EmergencyRedOnTime"];
							_setup.EmergencyRedOffTime = (short)dr["EmergencyRedOffTime"];
							_setup.AlarmHornOnTime = (short)dr["AlarmHornOnTime"];
							_setup.AlarmHornOffTime = (short)dr["AlarmHornOffTime"];

							//_setupReceived = true;
							//IcpGetOrAddTelegram(IcpTelegramTypes.SendSetup, null, true);
						}
					}
					break;

				case DbCommandTypes.GetItems:
					cmd = new OleDbCommand(sql, _con);
					dr = DBTools.HandleBusyExecuteReader(cmd);

					//for (int i = 0; i < _setup.LabelPositionsPrinter1.Length; i++)
					//{
					//    _setup.LabelPositionsPrinter1[i].IsEnabled = 0;
					//}

					//for (int i = 0; i < _setup.LabelPositionsPrinter2.Length; i++)
					//{
					//    _setup.LabelPositionsPrinter2[i].IsEnabled = 0;
					//}

					//for (int i = 0; i < _setup.BundlePositions.Length; i++)
					//{
					//    _setup.BundlePositions[i].IsEnabled = 0;
					//}

					int labelPositionPrinter1Index = 0;
					int labelPositionPrinter2Index = 0;
					int bundlePositionIndex = 0;

					_setup.LabelPositionCountPrinter1 = 0;
					_setup.LabelPositionCountPrinter2 = 0;
					_setup.BundlePositionCount = 0;

					int rowIndex = -1;
					bool isOk = true;

					if (dr.HasRows)
					{
						//while (labelPositionIndex < MAX_LABEL_POSITIONS || bundlePositionIndex < MAX_BUNDLE_POSITIONS)
						{
							//if (dr.Read())
							while (dr.Read())
							{
								++rowIndex;
								try
								{
									bool isEnabled = (bool)dr["IsEnabled"];
									if (isEnabled)
									{
										bool isLabelPosition = (bool)dr["IsLabelPosition"];
										bool isLabelPositionPrinter1 = isLabelPosition && ((byte)dr["PrinterNumber"]) == 1;
										bool isLabelPositionPrinter2 = isLabelPosition && ((byte)dr["PrinterNumber"]) == 2;

										if ((isLabelPositionPrinter1 && labelPositionPrinter1Index < MAX_LABEL_POSITIONS) ||
											(isLabelPositionPrinter2 && labelPositionPrinter2Index < MAX_LABEL_POSITIONS) ||
											(!isLabelPosition && bundlePositionIndex < MAX_BUNDLE_POSITIONS))
										{
											SetupPositionStruct setupPosition = new SetupPositionStruct
											{
												//setupPosition.CurrentStackerPosition = (byte)dr["StackerPositionId"];
												//setupPosition.Position = (byte)dr["LabelOrBundlePosition"];
												//setupPosition.IsEnabled = 1;
												EnteringInputSlot = (byte)dr["EnteringInputSlot"],
												EnteringInputSignal = (byte)dr["EnteringInputSignal"],
												EnteringInputOnHigh = (byte)(((bool)dr["EnteringInputOnHigh"]) ? 1 : 0),
												PositionInputSlot = (byte)dr["PositionInputSlot"],
												PositionInputSignal = (byte)dr["PositionInputSignal"],
												PositionInputOnHigh = (byte)(((bool)dr["PositionInputOnHigh"]) ? 1 : 0),
												ConveyorStoppedInputSlot = (byte)dr["ConveyorStoppedInputSlot"],
												ConveyorStoppedInputSignal = (byte)dr["ConveyorStoppedInputSignal"],
												ConveyorStoppedInputOnHigh = (byte)(((bool)dr["ConveyorStoppedInputOnHigh"]) ? 1 : 0),
												GateReadyInputSlot = (byte)dr["GateReadyInputSlot"],
												GateReadyInputSignal = (byte)dr["GateReadyInputSignal"],
												GateReadyInputOnHigh = (byte)(((bool)dr["GateReadyInputOnHigh"]) ? 1 : 0),
												ReceiveExpected = (short)dr["ReceiveExpected"],
												ReceiveTimeout = (short)dr["ReceiveTimeout"],
												VakuumOutputSlot = (byte)dr["VakuumOutputSlot"],
												VakuumOutputSignal = (byte)dr["VakuumOutputSignal"],
												EnteringTransportOutputSlot = (byte)dr["EnteringTransportOutputSlot"],
												EnteringTransportOutputSignal = (byte)dr["EnteringTransportOutputSignal"],
												TransportOutputSlot = (byte)dr["TransportOutputSlot"],
												TransportOutputSignal = (byte)dr["TransportOutputSignal"],
												BrakeOutputSlot = (byte)dr["BrakeOutputSlot"],
												BrakeOutputSignal = (byte)dr["BrakeOutputSignal"],
												DelayBeforeBrake = (short)dr["DelayBeforeBrake"],
												BrakeTime = (short)dr["BrakeTime"],
												StopDelay = (short)dr["StopDelay"],
												StartDelay = (short)dr["StartDelay"],
												StopDelayLeave = (short)dr["StopDelayLeave"],
												EncoderChannel = (byte)dr["EncoderChannel"],
												EncoderInputSlot = (byte)dr["EncoderInputSlot"],
												EncoderInputSignal = (byte)dr["EncoderInputSignal"],
												AlarmInputSlot = (byte)dr["AlarmInputSlot"],
												AlarmInputSignal = (byte)dr["AlarmInputSignal"],
												AlarmInputOnHigh = (byte)(((bool)dr["AlarmInputOnHigh"]) ? 1 : 0),
												UsePulseTiming = (byte)(((bool)dr["UsePulseTiming"]) ? 1 : 0),

												RejectOutputSlot = (byte)dr["RejectOutputSlot"],
												RejectOutputSignal = (byte)dr["RejectOutputSignal"],
												RejectPosInputSlot = (byte)dr["RejectPosInputSlot"],
												RejectPosInputSignal = (byte)dr["RejectPosInputSignal"],
												RejectPosInputOnHigh = (byte)(((bool)dr["RejectPosInputOnHigh"]) ? 1 : 0),

												SidePlatesOutputSlot = (byte)dr["SidePlatesOutputSlot"],
												SidePlatesOutputSignal = (byte)dr["SidePlatesOutputSignal"],
												SidePlatesMode = (byte)dr["SidePlatesMode"]
											};

											if (isLabelPositionPrinter1)
											{
												_setup.LabelPositionsPrinter1[labelPositionPrinter1Index] = setupPosition;
												++labelPositionPrinter1Index;
											}
											else if (isLabelPositionPrinter2)
											{
												_setup.LabelPositionsPrinter2[labelPositionPrinter2Index] = setupPosition;
												++labelPositionPrinter2Index;
											}
											else
											{
												_setup.BundlePositions[bundlePositionIndex] = setupPosition;
												++bundlePositionIndex;
											}
										}
										else
										{
											isOk = false;
											if (isLabelPositionPrinter1)
											{
												AddToLog(LogLevels.Warning, "To many label positions defined printer 1");
											}
											else if (isLabelPositionPrinter2)
											{
												AddToLog(LogLevels.Warning, "To many label positions defined printer 2");
											}
											else
											{
												AddToLog(LogLevels.Warning, "To many bundle positions defined");
											}
										}
									}
								}
								catch (Exception ex)
								{
									//_setupItems[index].IsEnabled = 0;
									AddToLog(LogLevels.Error, "Error in topSheet item rowIndex " + rowIndex.ToString() +
										" - sql=" + sql +
										" - " + ex.ToString());
								}
							}
							//else
							//{
							//    _setupItems[index].IsEnabled = 0;
							//}
						}
					}

					_setup.LabelPositionCountPrinter1 = (byte)labelPositionPrinter1Index;
					_setup.LabelPositionCountPrinter2 = (byte)labelPositionPrinter2Index;
					_setup.BundlePositionCount = (byte)bundlePositionIndex;

					if (labelPositionPrinter1Index > 0 && labelPositionPrinter1Index < MAX_LABEL_POSITIONS &&
						(_setup.LabelPositionsPrinter1[labelPositionPrinter1Index - 1].BrakeTime == 1 ||
						_setup.LabelPositionsPrinter1[labelPositionPrinter1Index - 1].BrakeTime == 2)
						)
					{
						WaitTransportLabelUntilBundleArrived1 = true;
					}
					else
					{
						WaitTransportLabelUntilBundleArrived1 = false;
					}

					if (labelPositionPrinter2Index > 0 && labelPositionPrinter2Index < MAX_LABEL_POSITIONS &&
						(_setup.LabelPositionsPrinter2[labelPositionPrinter2Index - 1].BrakeTime == 1 ||
						_setup.LabelPositionsPrinter2[labelPositionPrinter2Index - 1].BrakeTime == 2)
						)
					{
						WaitTransportLabelUntilBundleArrived2 = true;
					}
					else
					{
						WaitTransportLabelUntilBundleArrived2 = false;
					}

					if (IcpConnectionState == IcpConnectionStates.Connected)
					{
						//IcpGetOrAddTelegram(IcpTelegramTypes.SendSetup, null, true);
					}
					_setupReceived = isOk;
					break;
			}

			if (dr != null && !dr.IsClosed)
			{
				dr.Close();
			}
		}

		#region DbAddCommandToQueue, AddCommandFromDB

		private void DbAddCommandToQueue(DbCommandTypes commandType, string sql, object oParams, bool acceptDuplicates)
		{
			if (!acceptDuplicates)
			{
				lock (_lockDbCommandList)
				{
					for (int i = _dbCommandList.Count - 1; i >= 0; i--)
					{
						if (commandType == _dbCommandList[i].dbCommandType)
						{
							_dbCommandList.RemoveAt(i);
						}
					}
				}
			}

			DbCommandStuct dbCommand = new DbCommandStuct
			{
				dbCommandType = commandType,
				sql = sql,
				oParams = oParams
			};

			lock (_lockDbCommandList)
			{
				_dbCommandList.Add(dbCommand);
			}
		}

		private void AddCommandFromDB(char ioCommand)
		{
			switch (ioCommand)
			{
				case 'B': //Send positions to IO
					DbGetItems();
					break;
				case 'D': //Send setup to IO
					DbGetSetup();
					break;
				case 'H': //Stop application
					IcpGetOrAddTelegram(IcpTelegramTypes.SendStopApplication, null, true);
					break;
			}
		}

		#endregion

		#region DbGetSetup

		private void DbGetSetup()
		{
			string sql = string.Format("select top 1 * from UniStack_TopSheetApplicatorSetupTable"
				+ " where (StackerLineTableId = 0 or StackerLineTableId = {0})"
				+ "   and (CurrentStackerPosition = 0 or CurrentStackerPosition = {1})"
				+ " order by StackerLineTableId desc, CurrentStackerPosition desc"
				, _topSheetLineTableId, _currentStackerPositionId);

			DbAddCommandToQueue(DbCommandTypes.GetSetup, sql, null, false);
		}

		private void DbGetItems()
		{
			string sql = string.Format("select * from UniStack_GetTopSheetPositionSetup({0}, {1})"
				+ " order by PrinterNumber, IsLabelPosition, LabelOrBundlePosition"
				, _topSheetLineTableId, _currentStackerPositionId);

			DbAddCommandToQueue(DbCommandTypes.GetItems, sql, null, false);
		}

		#endregion

		private void DbDisconnect()
		{
			try
			{
				DbConnectionState = DbConnectionStates.Disconnected;

				if (_con != null)
				{
					try
					{
						if (_con.State == ConnectionState.Open)
						{
							_con.Close();
						}
					}
					catch
					{
					}
					finally
					{
						_con = null;
					}
				}
			}
			catch
			{

			}
		}

		#endregion

		#region Printer communication
		private void PrinterControl()
		{
			try
			{
				AddToLog(LogLevels.Debug, "PrinterControl started");

				PrinterControlState = PrinterControlStates.Running;
				//DateTime beforeLastPrint1 = DateTime.MinValue;
				//DateTime beforeLastPrint2 = DateTime.MinValue;

				DateTime LastPrint = DateTime.MinValue;

				bool wasApplicatorReady = false;
				bool wasSnmpWatcher = false;

				int ResumePrinterCount = 0;

				// byte CurPrinterNumber = 0;

				while (IsStarted &&
					_printerThread != null &&
					_printerThread.ManagedThreadId == Thread.CurrentThread.ManagedThreadId)
				{
					try
					{
						try
						{
							if (ST_SNMPListeningPort > 0)
							{
								lock (ST_lock_snmpWatcherTopSheetLines)
								{
									if (ST_snmpWatcher == null)
									{
										ST_snmpWatcher = this;
									}

									if (!wasSnmpWatcher &&   //cheng 09-24
										IsSnmpWatcher)
									{
										wasSnmpWatcher = true;
										AddToLog(LogLevels.Debug, "SnmpWatcher set to line " + TopSheetLineTableId.ToString());

										if (IsSnmpWatcher)
										{
											AddToLog(LogLevels.Debug, "Starting snmpWatcher thread on line "
												+ TopSheetLineTableId.ToString());
										   
											ST_snmpThread = new Thread(new ThreadStart(SnmpControl));
											ST_snmpThread.Priority = ThreadPriority.AboveNormal;
											ST_snmpThread.Start();
										}
									}

									if (!IsSnmpWatcher)
									{
										wasSnmpWatcher = false;
									}
								}
							}

							byte printerNumber = 0;

							if (IcpConnectionState == IcpConnectionStates.Connected &&
								 _status.cSTBStartStopState >= 10)
							{
								if (!wasApplicatorReady)
								{
									if (PrintronixPrinter[0] != null)//&& _printer[0].PrinterStatus != null)
									{
										PrintronixPrinter[0]._printerStatus.ReadyTime = DateTime.Now;
									}
									if (PrintronixPrinter[1] != null)//&& _printer[1].PrinterStatus != null)
									{
										PrintronixPrinter[1]._printerStatus.ReadyTime = DateTime.Now;
									}
									wasApplicatorReady = true;
								}
							}
							else
							{
								wasApplicatorReady = false;

							}

							if (DateTime.Now > _lastResetTime.AddMilliseconds(3000))
							{
								if (wasApplicatorReady && _status.cIsResetSTB == 1)
								{
									AddToLog(LogLevels.Debug, "Printer Control Reset Applicator," +
										" _status.cSTBStartStopState: " + _status.cSTBStartStopState +
										" _status.cIsResetSTB: " + _status.cIsResetSTB +
										" _lastResetTime: " + _lastResetTime);
									ResetApplicator(true);
								}
							}
							

								//if (DateTime.Now > _lastPrint[0].AddHours(6) &&
								//    DateTime.Now > _lastPrint[1].AddHours(6) &&
								//    _nextBundleIndex > 11)
								//{
								//    _nextBundleIndex = 11;
								//    ResetApplicator(true);
								//}
							

							//if ((DateTime.Now > _lastPrint[0].AddHours(6) &&
							//    DateTime.Now > _lastPrint[1].AddHours(6) &&
							//    DateTime.Now > _lastLCCMessage.AddHours(6) &&
							//    _nextBundleIndex > 11) ||
							//    _nextBundleIndex >= 65000)
							//{
							//    _lastPrint[0] = DateTime.Now;
							//    _lastPrint[1] = DateTime.Now;
							//    AddToLog(LogLevels.Debug, "Printer Control Reset Applicator, NextBundleIndex: " + _nextBundleIndex.ToString());

							//    _nextBundleIndex = 11;
							//    ResetApplicator(true);
							//}

							//if (!isNoTopsheet)
							{
								TopSheetItem topSheetItem = null;
								//PrintBundle(10001);
								//bool doInitialTestPrint = false;

								//lock (_lock_topSheetQueue)
								{
									if (_topSheetQueue.Count > 0 || 
										TsBundleQueuePrintedResume.Count > 0)
									{

										//bool isPrinter1Ready = CanPrintNow(1);
										//bool isPrinter2Ready = CanPrintNow(2);
										//int count = 0;

										bool isPrinterReady = CanPrintNow(DefaultPrinter);
										//isPrinter1Ready = true;
										//if (isPrinter1Ready && isPrinter2Ready)
										//{
										//    printerNumber = DefaultPrinter;
										//}
										//else if (isPrinter1Ready)
										//{
										//    printerNumber = 1;
										//}
										//else if (isPrinter2Ready)
										//{
										//    printerNumber = 2;
										//}
										//else
										//{
										//    printerNumber = 0;
										//}

										if (isPrinterReady)
										{
											printerNumber = DefaultPrinter;
											isDefaultPrintChanged = false;
										}
										else if (!PrintronixPrinter[DefaultPrinter - 1]._printerStatus.StatusMessage.ToUpper().Contains("ONLINE"))
										{
											bool isBackupPrinterReady = CanPrintNow(BackupPrinter);

											if (isBackupPrinterReady)
											{
												IcpGetOrAddTelegram(IcpTelegramTypes.ClearLabelQueue, DefaultPrinter, true);
												//IcpGetOrAddTelegram(IcpTelegramTypes.ResumeClearLabelQueue, BackupPrinter, true);
												AddToLog(LogLevels.Debug, "Default Printer: " + DefaultPrinter.ToString() + " Set to Clear Mode, Backup: " + BackupPrinter.ToString() + " Back to Normal!");

												TsBundleQueueAvoidTwoTopAtSametime.Clear();
												PrintronixPrinter[DefaultPrinter - 1].Disconnect();

												printerNumber = BackupPrinter;
												AddToLog(LogLevels.Debug, "Default Printer: " + DefaultPrinter.ToString() + " Cleared, Changed to Backup: " + BackupPrinter.ToString() + " ,Backup Resume Clear!" );

												DefaultPrinter = BackupPrinter;
												BackupPrinter = (byte)(3 - DefaultPrinter);

												AddToLog(LogLevels.Debug, "Current Default Printer: " + DefaultPrinter.ToString());
												isDefaultPrintChanged = true;
											}
											else
											{
												printerNumber = 0;
												isDefaultPrintChanged = false;
												AddToLog(LogLevels.Warning, "Two Printer Can Not Use! Code 1");
											}
										}
										else
										{
											printerNumber = 0;
											isDefaultPrintChanged = false;
											//while(count == 0)
											//{
											//    AddToLog(LogLevels.Warning, "Two Printer Can Not Use! Code 2");
											//    count++;
											//}
										}

										//if (!doInitialTestPrint)
										//if (CanPrintNow(printerNumber))
										if (printerNumber > 0)
										{
											//lock (_lock_TsBundleQueuePrintedResume)
											
											if (isDefaultPrintChanged && TsBundleQueuePrintedResume.Count > 0)
											{
												ResumePrinterCount = TsBundleQueuePrintedResume.Count;
												TsBundleQueueAvoidTwoTopAtSametime.Clear();
												AddToLog(LogLevels.Warning, "Default Printer Changed, ResumePrinterCount:  " + TsBundleQueuePrintedResume.Count
													+ ", TsBundleQueueAvoidTwoTopAtSametime Clear!");
											}

											if (ResumePrinterCount > 0)
											{
												if(TsBundleQueuePrintedResume.Count > 0)
												{
													lock(_lock_TsBundleQueuePrintedResume)
													{
														topSheetItem = TsBundleQueuePrintedResume[0];
														TsBundleQueuePrintedResume.RemoveAt(0);
														ResumePrinterCount = TsBundleQueuePrintedResume.Count;

													}


													if (topSheetItem.BundleId > 10000 &&
														topSheetItem.BundleId < 50000)
													{
														topSheetItem.TopSheetType = TopSheetTypes.RESUMEPRINT;
													}
													else
													{
														topSheetItem.TopSheetType = TopSheetTypes.WHITEPAPER;
													}

													if (topSheetItem != null && topSheetItem.BundleId > 0)
													{
														lock(_lock_TsBundleQueueAvoidTwoTopAtSametime)
														{
															TsBundleQueueAvoidTwoTopAtSametime.Add(topSheetItem);
														}

														AddToLog(LogLevels.Debug, "Resume Bundle: " + topSheetItem.BundleId.ToString() +
															" From TsBundleQueuePrintedResume, Count: " + TsBundleQueuePrintedResume.Count +
															" and send SendPrintedBundleIndex to PAC, Printer:  " + printerNumber +
															" When Topsheet Printed But Printer Crashed, LableState: " + topSheetItem.LabelState.ToString() +
															" send to TsBundleQueueAvoidTwoTopAtSametime, Count: " + TsBundleQueueAvoidTwoTopAtSametime.Count.ToString());
													}

													topSheetItem.PrinterNumber = printerNumber;
													IcpGetOrAddTelegram(IcpTelegramTypes.SendPrintedBundleIndex, topSheetItem, true);

													if (topSheetItem != null)
													{
														if (printerNumber > 0 && printerNumber <= 2)
														{
															_lastPrint[printerNumber-1] = DateTime.Now;
														}


														AddToLog(LogLevels.Debug,
															"Before Resume Print: CurPrinterNumber: " + printerNumber.ToString()
															+ ", LabelQC: " + _status.cLabelQueueCount[printerNumber - 1].ToString()
															+ ", TitleGroup: " + TitleGroupId.ToString()
															+ ", Bundle: " + topSheetItem.BundleId.ToString()
															+ ", LableState: " + topSheetItem.LabelState.ToString());

														LabelItem.printerResultTypes printResult = PrintLogic(topSheetItem);

														AddToLog(LogLevels.Debug,
															"After Resume Print: CurPrinterNumber: " + printerNumber.ToString()
															+ ", LabelQC: " + _status.cLabelQueueCount[printerNumber - 1].ToString()
															+ ", LableState: " + topSheetItem.LabelState.ToString()
															+ ", Print Result: " + printResult.ToString());

														if (topSheetItem.BundleId != 0)
														{
															if (printResult == LabelItem.printerResultTypes.Ok)
															{
																int _topsheetActionCode = 0;
																LastPrint = DateTime.Now;


																if (topSheetItem.TopSheetType == TopSheetTypes.RESUMEPRINT)
																{
																	_topsheetActionCode = (int)TopSheetActionEnum.ResumeTopsheetToBackupPrinter;
																}

																string sql = string.Format("exec UniStack_TopSheetBundleAction_SPH {0}, {1}, {2},'{3}', {4}, {5}, {6}, {7}, {8}"
																	, _gripperLineTableId
																	, _topSheetLineTableId
																	, topSheetItem.TitleGroupId
																	, topSheetItem.IssueDateString
																	, topSheetItem.BundleId
																	, topSheetItem.BundleIndex
																	, _topsheetActionCode//(int)TopSheetActionEnum.AutomaticPrint
																	, -1
																	, printerNumber - 1
																	);

																DbAddCommandToQueue(DbCommandTypes.ExecuteNonQuery, sql, null, true);

																AddToLog("PrinterControl Resume Print Ok TitleGroup: " + topSheetItem.TitleGroupId.ToString()
																	+ " - TopSheetAction: " + _topsheetActionCode.ToString()
																	+ " - Bundleid: " + topSheetItem.BundleId.ToString()
																	+ " - BundleIndex: " + topSheetItem.BundleIndex.ToString()
																	+ " - PrinterID " + topSheetItem.PrinterNumber.ToString());
															}
															else
															{
																string sql = string.Format("exec UniStack_TopSheetBundleAction_SPH {0}, {1}, {2},'{3}', {4}, {5}, {6}, {7}, {8}"
																	, _gripperLineTableId
																	, _topSheetLineTableId
																	, TitleGroupId
																	, IssueDateString
																	, topSheetItem.BundleId
																	, topSheetItem.BundleIndex
																	, (int)TopSheetActionEnum.UnableToPrint
																	, -1
																	, printerNumber - 1
																	);

																DbAddCommandToQueue(DbCommandTypes.ExecuteNonQuery, sql, null, true);

																AddToLog(LogLevels.Warning,
																	"ERROR: PrinterControl Resume Print Failed TitleGroup: " + TitleGroupId.ToString() +
																	" - Bundleid: " + topSheetItem.BundleId.ToString() +
																	" - BundleIndex: " + topSheetItem.BundleIndex.ToString());

																//topSheetItem.LabelState = 51;
																//TsBundleQueue.Enqueue(topSheetItem);
															}
														}
													}
												}
												else
												{
													AddToLog(LogLevels.Warning, "ResumePrinterCount:  " + ResumePrinterCount
													+ "TsBundleQueuePrintedResume Count: " + TsBundleQueuePrintedResume.Count);
													ResumePrinterCount = 0;
												}
											}
											else
											{
												lock (_lock_topSheetQueue)
												{
													if (_topSheetQueue.Count > 0)
													{
														topSheetItem = _topSheetQueue[0];
														_topSheetQueue.RemoveAt(0);
													}
												}
												   
												if (topSheetItem != null &&
												topSheetItem.BundleId != 0)//topsheetItem.BundleIndex > 10) // 04/09/2019 Cheng
												{
													lock (_lock_topSheetPrinted)
													{
														if (topSheetItem.BundleId > 10000 && topSheetItem.BundleId < 60000)
														{
															_topSheetPrinted.Add(topSheetItem);
														}
													}

													lock (_lock_TsBundleQueueAvoidTwoTopAtSametime)
													{
														TsBundleQueueAvoidTwoTopAtSametime.Add(topSheetItem);
													}

													lock (_lock_TsBundleQueuePrintedResume)
													{
														if (topSheetItem.BundleId > 10000 && topSheetItem.BundleId < 60000)
														{
															if (!TsBundleQueuePrintedResume.Contains(topSheetItem))
															{
																TsBundleQueuePrintedResume.Add(topSheetItem);
																AddToLog("Bundle " + topSheetItem.BundleId + " Send to TsBundleQueuePrintedResume, Count: " + TsBundleQueuePrintedResume.Count.ToString());
															}
															else
															{
																AddToLog(LogLevels.Warning, "Bundle " + topSheetItem.BundleId + " Send to TsBundleQueuePrintedResume Failed Due To Already Existed, Count: " + TsBundleQueuePrintedResume.Count.ToString());
															}

														}
													}

													AddToLog("Bundle " + topSheetItem.BundleId +
														" Send to TopsheetPrinted, Count: " + _topSheetPrinted.Count.ToString() +
														" Send to TsBundleQueueAvoidTwoTopAtSametime, Count: " + TsBundleQueueAvoidTwoTopAtSametime.Count.ToString());

												}

												if (topSheetItem != null)
												{
													topSheetItem.PrinterNumber = printerNumber;
													IcpGetOrAddTelegram(IcpTelegramTypes.SendPrintedBundleIndex, topSheetItem, true);
												}

												if (topSheetItem != null)
												{
													if (printerNumber == 1)
													{
														//_printQueueUpdated1 = false;
														_lastPrint[0] = DateTime.Now;
													}
													else
													{
														//_printQueueUpdated2 = false;
														_lastPrint[1] = DateTime.Now;
													}

													AddToLog(LogLevels.Debug,
														"Before print: CurPrinterNumber: " + printerNumber.ToString()
														+ ", LabelQC: " + _status.cLabelQueueCount[printerNumber - 1].ToString()
														+ ", TitleGroup: " + TitleGroupId.ToString()
														+ ", Bundle: " + topSheetItem.BundleId.ToString()
														+ ", LableState: " + topSheetItem.LabelState.ToString());

													//LabelItem.printerResultTypes printResult = LabelItem.printerResultTypes.Unknown;
													//printResult = PrintTopSheet(printerNumber, documentName, topSheetItem.BundleId, topSheetItem.TopSheetType);
													//printResult = PrintTopSheetPrintronix(printerNumber, topSheetItem.BundleId, topSheetItem.TopSheetType);

													LabelItem.printerResultTypes printResult = PrintLogic(topSheetItem);

													//LabelItem.ResultTypes printResult =
													//    PrintTopSheet(printerNumber, documentName, topSheetItem.BundleId, topSheetItem.TopSheetType);

													AddToLog(LogLevels.Debug,
														"After print: CurPrinterNumber: " + printerNumber.ToString()
														+ ", LabelQC: " + _status.cLabelQueueCount[printerNumber - 1].ToString()
														+ ", LableState: " + topSheetItem.LabelState.ToString()
														+ ", Print Result: " + printResult.ToString());

													if (topSheetItem.BundleId != 0)
													{
														if (printResult == LabelItem.printerResultTypes.Ok)
														{
															int _topsheetActionCode = 0;
															LastPrint = DateTime.Now;

															//TsBundleQueuePrintedResume.Add(topSheetItem);
															//TsBundleQueueAvoidTwoTopAtSametime.Add(topSheetItem);

															//AddToLog("Bundle " + topSheetItem.BundleId + 
															//    " Send to TsBundleQueueAvoidTwoTopAtSametime, Count: " + TsBundleQueueAvoidTwoTopAtSametime.Count.ToString() +
															//    " Send to TsBundleQueuePrintedResume, Count: " + TsBundleQueuePrintedResume.Count.ToString());

															if (topSheetItem.TopSheetType == TopSheetTypes.MANUALPRINT)
															{
																_topsheetActionCode = (int)TopSheetActionEnum.ManagerManualPrintReject;
															}
															else
															{
																_topsheetActionCode = (int)TopSheetActionEnum.AutomaticPrint;
															}

															//IcpGetOrAddTelegram(IcpTelegramTypes.SendPrintedBundleIndex, topSheetItem, true);

															string sql = string.Format("exec UniStack_TopSheetBundleAction_SPH {0}, {1}, {2},'{3}', {4}, {5}, {6}, {7}, {8}"
																, _gripperLineTableId
																, _topSheetLineTableId
																, topSheetItem.TitleGroupId
																, topSheetItem.IssueDateString
																, topSheetItem.BundleId
																, topSheetItem.BundleIndex
																, _topsheetActionCode//(int)TopSheetActionEnum.AutomaticPrint
																, -1
																, printerNumber - 1
																);

															DbAddCommandToQueue(DbCommandTypes.ExecuteNonQuery, sql, null, true);

															AddToLog("PrinterControl Print Ok TitleGroup: " + topSheetItem.TitleGroupId.ToString()
																+ " - TopSheetAction: " + _topsheetActionCode.ToString()
																+ " - Bundleid: " + topSheetItem.BundleId.ToString()
																+ " - BundleIndex: " + topSheetItem.BundleIndex.ToString()
																+ " - PrinterID " + topSheetItem.PrinterNumber.ToString());
														}
														else
														{
															string sql = string.Format("exec UniStack_TopSheetBundleAction_SPH {0}, {1}, {2},'{3}', {4}, {5}, {6}, {7}, {8}"
																, _gripperLineTableId
																, _topSheetLineTableId
																, TitleGroupId
																, IssueDateString
																, topSheetItem.BundleId
																, topSheetItem.BundleIndex
																, (int)TopSheetActionEnum.UnableToPrint
																, -1
																, printerNumber - 1
																);

															DbAddCommandToQueue(DbCommandTypes.ExecuteNonQuery, sql, null, true);

															lock (_lock_TsBundleQueueAvoidTwoTopAtSametime)
															{
																int tempCount = TsBundleQueueAvoidTwoTopAtSametime.Count;
																for (int i = 0; i < tempCount; i++)
																{
																	if (topSheetItem.BundleId == TsBundleQueueAvoidTwoTopAtSametime[i].BundleId)
																	{
																		TsBundleQueueAvoidTwoTopAtSametime.RemoveAt(i);

																		AddToLog(LogLevels.Debug,
																			"Bundle: " + topSheetItem.BundleId.ToString()
																			+ " Remove From TsBundleQueueAvoidTwoTopAtSametime Due To Print Failure, Count: "
																			+ TsBundleQueueAvoidTwoTopAtSametime.Count.ToString());

																		i = tempCount;
																	}
																}
															}

															AddToLog(LogLevels.Warning,
																"ERROR: PrinterControl print failed TitleGroup: " + TitleGroupId.ToString() +
																" - Bundleid: " + topSheetItem.BundleId.ToString() +
																" - BundleIndex: " + topSheetItem.BundleIndex.ToString());

															AddToLog(LogLevels.Warning,
																"PrinterControl print resent to TsBundleQueue - Bundleid: " + topSheetItem.BundleId.ToString() +
																" - BundleIndex: " + topSheetItem.BundleIndex.ToString());

															//topSheetItem.LabelState = 51;
															//TsBundleQueue.Enqueue(topSheetItem);
														}
													}
												}
											}
										}
									}

									if (DateTime.Now > LastPrint.AddMilliseconds(70000) || !IsProductionStarted)
									{

										if (_topSheetQueue.Count == 0 &&
											TsBundleQueue.Count == 0 &&
											PZFBundleList.Count == 0 &&
											//TsBundleQueueAvoidTwoTopAtSametime.Count <= 2 &&
											TsBundleQueueAvoidTwoTopAtSametime.Count > 0)
										{
											for (int i = 0; i < TsBundleQueueAvoidTwoTopAtSametime.Count; i++)
											{
												if (TsBundleQueueAvoidTwoTopAtSametime[i].BundleId < 60000)
												{
													TopSheetItem restItem = new TopSheetItem
													{
														BundleId = 65500,
														BundleIndex = 0,
														TopSheetType = TopSheetTypes.WHITEPAPER,
														LabelState = 50
													};

													lock(_lock_TsBundleQueue)
													{
														TsBundleQueue.Enqueue(restItem);
													}
													AddToLog(LogLevels.Debug, "Bundle : " + TsBundleQueueAvoidTwoTopAtSametime[i].BundleId + " Automatic Labeled Last Two Topsheet.");
												}
											}
										}
									}
								}

								if (_doReconnectPrinter)
								{
									_doReconnectPrinter = false;
									//_printer[0].StopStatusCheck();
									//if (_useTwoPrinters)
									//{
									//    _printer[1].StopStatusCheck();
									//}
								}
							}
						}
						catch (Exception ex)
						{
							AddToLog(LogLevels.Warning, ex.ToString());
							Thread.Sleep(1000);
						}
						finally
						{
							Thread.Sleep(100);
						}
					}
					catch
					{
					}
				}

				if (_printerThread.ManagedThreadId != Thread.CurrentThread.ManagedThreadId)
				{
					AddToLog(LogLevels.Debug, "PrinterControl old thread exited");
				}
				else
				{
					//for (int i = 0; i < NUM_DBH; i++)
					//{
					//    if (PrintronixPrinter[i] != null)
					//    {
					//        PrintronixPrinter[i].StopStatusCheck();
					//    }
					//}

					PrinterControlState = PrinterControlStates.NotRunning;
					AddToLog(LogLevels.Debug, "PrinterControl stopped");
				}
			}
			catch
			{
			}
		}

		private LabelItem.printerResultTypes PrintLogic(TopSheetItem topsheetPrintItem)
		{
			LabelItem.printerResultTypes printResult = LabelItem.printerResultTypes.Unknown;
			byte printerNumber = topsheetPrintItem.PrinterNumber;
			try
			{
				if (PrintronixPrinter[printerNumber - 1] != null)
				{
					if (PrintronixPrinter[printerNumber - 1]._printerStatus != null)
					 //&& PrintronixPrinter[printerNumber - 1].IsReadyToPrint)
					{
						//while (PrintronixPrinter[printerNumber - 1]._printerStatus.PrintingStatus != 3)
						//{
						//    AddToLog("Bundle: " + topsheetPrintItem.BundleId.ToString() +
						//        " PrintingStatus is " + PrintronixPrinter[printerNumber - 1]._printerStatus.PrintingStatus + 
						//        " Sleep 10 ms to Avoid Destory Next One!");

						//    if (!PrintronixPrinter[printerNumber - 1]._printerStatus.StatusMessage.ToLower().Contains("online"))
						//    {
						//        break;
						//    }

						//    Thread.Sleep(10);
						//}

						//Printing Status: 3(idle) -> 4(printing) -> 3(idle)
						if (PrintronixPrinter[printerNumber - 1]._printerStatus.PrintingStatus == 3)
						{
							AddToLog("Bundle: " + topsheetPrintItem.BundleId.ToString() + " StartPaperTransport at Printer " + printerNumber.ToString());
							IcpGetOrAddTelegram(IcpTelegramTypes.StartPaperTransport, printerNumber, true);
							printResult = PrintTopSheetPrintronix(printerNumber, topsheetPrintItem);
							AddToLog("After Print Before Cutter Bundle: " + topsheetPrintItem.BundleId.ToString() + " LableQueueCount " + _status.cLabelQueueCount[printerNumber - 1].ToString());

							Thread.Sleep(800); //1000 //2000 //2500 //3000 //4000
						}
						AddToLog("Bundle: " + topsheetPrintItem.BundleId.ToString() + " Before 4 PrintingStatus: " + PrintronixPrinter[printerNumber - 1]._printerStatus.PrintingStatus + " - Printer Result: " + printResult);

						while (PrintronixPrinter[printerNumber - 1]._printerStatus.PrintingStatus == 4)
						{
							Thread.Sleep(10);
							//AddToLog("Bundle: " + topsheetPrintItem.BundleId.ToString() + " Print " + (printerNumber - 1) + " Printing Status is 4, need Sleep 10 ms; ");

							if (!PrintronixPrinter[printerNumber - 1]._printerStatus.StatusMessage.ToLower().Contains("online"))
							{
								AddToLog("Bundle: " + topsheetPrintItem.BundleId.ToString() + " Crashed When Printing on Printer: " + (printerNumber - 1));
								break;
							}
						}

						//if (printResult == LabelItem.printerResultTypes.Ok)
						//{
						AddToLog("Bundle: " + topsheetPrintItem.BundleId.ToString() + " After 4 PrintingStatus: " + PrintronixPrinter[printerNumber - 1]._printerStatus.PrintingStatus);

						if (PrintronixPrinter[printerNumber - 1]._printerStatus.PrintingStatus == 3)
						{
							IcpGetOrAddTelegram(IcpTelegramTypes.EnableCutter, printerNumber, true);
							AddToLog("Bundle: " + topsheetPrintItem.BundleId.ToString().ToString() +
								" - EnableCutter at Printer " + printerNumber.ToString() +
								" - LabelQueueCount: " + _status.cLabelQueueCount[printerNumber - 1].ToString());

						}
						//PrintronixPrinter[printerNumber - 1].IsReadyToPrint = true;

						//Thread.Sleep(500);
					}
					else
					{
						AddToLog(LogLevels.Error, "PrinterStatus is null!");// or Printer Ready States: " + PrintronixPrinter[printerNumber - 1].IsReadyToPrint.ToString());
					}
				}
				else
				{
					AddToLog(LogLevels.Error,"PrintronixPrinter is NULL " +
						" - Bundle: " + topsheetPrintItem.BundleId.ToString().ToString() +
						" - Printer " + printerNumber.ToString());
				}
			}
			catch (Exception ex)
			{
				//PrintronixPrinter[printerNumber - 1].IsReadyToPrint = true;
				printResult = LabelItem.printerResultTypes.PrintingTimeout;
				AddToLog(LogLevels.Error, "Print Logic Error, Printing Timeout for: " + ex.ToString());
			}

			return printResult;
		}

		//private void ManualPrinterControl()
		//{
		//    try
		//    {
		//        AddToLog(LogLevels.Debug, "Manual PrinterControl Started");

		//        ManualPrinterControlState = PrinterControlStates.Running;

		//        while (IsStarted && !IsProductionStarted && TsBundleManualPrintQueue.Count > 0 &&
		//            _manualPrinterThread != null &&
		//            _manualPrinterThread.ManagedThreadId == Thread.CurrentThread.ManagedThreadId)
		//        {
					
		//        }

		//        if (_manualPrinterThread.ManagedThreadId != Thread.CurrentThread.ManagedThreadId)
		//        {
		//            AddToLog(LogLevels.Debug, "Manual PrinterControl old thread exited");
		//        }
		//        else
		//        {

		//            ManualPrinterControlState = PrinterControlStates.NotRunning;

		//            AddToLog(LogLevels.Debug, "Manual PrinterControl stopped");
		//        }
		//    }
		//    catch
		//    {
		//    }
		//}

		//public bool CanPrintNowManual(uint printerNumber)
		//{
		//    try
		//    {
		//        if (PrintronixPrinter[printerNumber - 1].ResetDone &&
		//            printerNumber > 0 && printerNumber <= 2 &&
		//            PrinterStates[printerNumber - 1] == TopSheetPrinterStates.PrinterReady &&
		//            TsBundleQueueAvoidTwoTopAtSametime.Count < 5)
		//        {
		//            if (DateTime.Now > _lastPrint[printerNumber - 1].AddMilliseconds(5000))  //7300, 6000,2000  wait 2 seconds to check
		//            {
		//                return true;
		//            }
		//        }
		//    }
		//    catch
		//    {
		//    }

		//    return false;
		//}

		public int lastLabelQueueCount = 0;
		public int lastPosState = 0;
		public int _topCount = 0;


		public bool CanPrintNow(uint printerNumber)
		{
			byte PosState;
			int labelQueueCount;
			bool STBRunWithProduction = false;
			//int printjobs = 0;

			try
			{
				//If Prodcution Started, STB Sould Run, 
				//If Production Not Started, Doesn't Matter About STB Status
				//Used For Production Print and Manual Print
				if (IsProductionStarted && _status.cSTBStartStopState == 10)
				{
					STBRunWithProduction = true;
				}
				else if (IsProductionStarted && _status.cSTBStartStopState != 10)
				{
					STBRunWithProduction = false;
				}
				else if (!IsProductionStarted)
				{
					STBRunWithProduction = true;
				}

				if (printerNumber > 0 && printerNumber <= 2)
				{
					//printjobs = _printer[printerNumber - 1].NumberOfPrintJobs;

					PosState = _status.LabelPositions[(printerNumber - 1) * MAX_LABEL_POSITIONS].State;
					labelQueueCount = _status.cLabelQueueCount[printerNumber - 1];
					PrinterStates[printerNumber - 1] = IsPrinterReadyToPrint(printerNumber);

					if (labelQueueCount != lastLabelQueueCount || 
						lastPosState != PosState)
					{
						AddToLog(LogLevels.Debug,
						"CanPrintNow: CurPrinterNumber: " + printerNumber.ToString()
						+ ", Printer State: " + PrinterStates[printerNumber - 1].ToString()
						+ ", PosState: " + PosState.ToString()
						+ ", LastPosState: " + lastPosState.ToString()
						+ ", LabelQueueCount: " + labelQueueCount.ToString()
						+ ", LastLabelQueueCount: " + lastLabelQueueCount.ToString()
						+ ", TsBundleQueueAvoidTwoTopAtSametime: " + TsBundleQueueAvoidTwoTopAtSametime.Count.ToString());

						lastLabelQueueCount = labelQueueCount;
						lastPosState = PosState;
					}

					if (_status.cIsPaperJamOrMissing[printerNumber - 1] != 0)
					{
						AddToLog(LogLevels.Debug,
					   "CanPrintNow: Printer" + printerNumber + "Jamed! cIsPaperJamOrMissing: "+ _status.cIsPaperJamOrMissing[printerNumber - 1]);
					}

					//AddToLog(LogLevels.Debug, "CanPrintNow: CurPrinterNumber: " + printerNumber.ToString() + " TsBundleQueueAvoidTwoTopAtSametime Count: " + TsBundleQueueAvoidTwoTopAtSametime.Count);

					//if (TsBundleQueueAvoidTwoTopAtSametime.Count >= 4 & _topCount <= 10)
					//{
					//    _topCount++;
					//}

					//if (_topCount >= 10)
					//{
					//    MessageBox.Show("Printer Error! Please Press Reset Button!");
					//    _topCount = 0;
					//}


					if (STBRunWithProduction && //_status.cSTBStartStopState == 10 &&
						PrintronixPrinter[printerNumber - 1].ResetDone &&
						PrinterStates[printerNumber - 1] == TopSheetPrinterStates.PrinterReady &&
						//_status.cLabelPositionState[printerNumber - 1] == 0 &&
						//topState == TopSheetPrinterStates.PrinterReady &&
						PosState == 0 &&
						_status.cIsPaperJamOrMissing[printerNumber - 1] == 0 &&
						//printjobs < 1 &&
						TsBundleQueueAvoidTwoTopAtSametime.Count < 4 )//5 //SPH Printer can Reserve 5 Copies each side
						//DateTime.Now > lastPrintQueueUpdate[printerNumber - 1].AddMilliseconds(2500))// &&
						//labelQueueCount <= 1) // avoid print more at the same time
					{
						if (DateTime.Now > _lastPrint[printerNumber - 1].AddMilliseconds(7000))  //7300, 6000,2000  wait 2 seconds to check
						{
							return true;
						}
					}
				}
			}
			catch
			{
			}
			return false;
		}

		//public bool CanPrintNowOld2(int printerNumber)
		//{
		//    bool result = false;
		//    DateTime lastPrint = _lastPrint[0];

		//    if (//_status.cSTBStartStopState == 10 && 
		//        DateTime.Now > lastPrint.AddMilliseconds(3000) &&
		//        (printerNumber == 1 || printerNumber == 2) &&
		//        _status.cLabelPositionState[1] == 0 // DBH 2
		//        )
		//    {
		//        result = true;
		//    }
		//    return result;
		//}

		//[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
		//private LabelItem.ResultTypes PrintTopSheet(
		//    uint printerNumber, string documentName, uint bundleId,
		//    TopSheetTypes topSheetType)
		//{
		//    LabelItem.ResultTypes printResult =
		//        LabelItem.ResultTypes.Unknown;

		//    try
		//    {
		//        DataTable labelData = null;
		//        string labelGroup = null;
		//        string labelName = null;
		//        string paperFormat = null;

		//        try
		//        {
		//            OleDbConnection con = new OleDbConnection(_connectionString);
		//            con.Open();

		//            string sql = string.Format("select * from UniStack_GetLabelNameBundle(-1, {0})"
		//                , bundleId);
		//            OleDbCommand cmd = new OleDbCommand(sql, con);
		//            OleDbDataReader dr = cmd.ExecuteReader();
		//            if (dr.Read())
		//            {
		//                labelGroup = (string)dr["LabelGroup"];
		//                labelName = (string)dr["LabelName"];
		//            }
		//            dr.Close();

		//            if (labelGroup != null && labelGroup.Length > 0 &&
		//                labelName != null && labelName.Length > 0)
		//            {
		//                sql = string.Format("select * from UniStack_GetLabelData({0}, -1, -1, {1})"
		//                    , (byte)topSheetType, bundleId);

		//                OleDbDataAdapter da = new OleDbDataAdapter(sql, con);
		//                labelData = new DataTable();
		//                da.FillSchema(labelData, SchemaType.Source);
		//                da.Fill(labelData);
		//            }
		//            con.Close();
		//        }
		//        catch (Exception ex)
		//        {
		//            printResult = LabelItem.ResultTypes.DataError;
		//            _doReconnectDb = true;
		//            AddToLog(LogLevels.Warning, ex.ToString());
		//        }
		//        if (printResult == LabelItem.ResultTypes.Unknown)
		//        {
		//            if (printerNumber > 0 && printerNumber <= 2)
		//            {
		//                printResult = _printer[printerNumber - 1].PrintWindowsPrinter(labelData, 
		//                        labelGroup, labelName, documentName, paperFormat, topSheetType);
		//                AddToLog(LogLevels.Debug, "Print Topsheet BundleID: " + bundleId + "- PrinterNumber: " + printerNumber + "- Time: " + DateTime.Now);
		//                lastPrintQueueUpdate[printerNumber - 1] = DateTime.Now;
		//            }
		//        }
		//    }
		//    catch (Exception ex)
		//    {
		//        printResult = LabelItem.ResultTypes.Unknown;
		//        AddToLog(LogLevels.Warning, ex.ToString());

		//    }

		//    return printResult;
		//}

		//[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
		//private LabelItem.printerResultTypes PrintManualTopsheet(
		//   uint printerNumber, string documentName, uint bundleId,
		//   TopSheetTypes topSheetType)
		//{
		//    LabelItem.printerResultTypes printResult =
		//        LabelItem.printerResultTypes.Unknown;

		//    //Dictionary<string, List<string>> labelItem = new Dictionary<string, List<string>>();
		//    //labelItem = null;
		//    try
		//    {
		//        DataTable labelData = null;
		//        OleDbConnection labelDesignConnection = null;
		//        string labelGroup = null;
		//        string labelName = null;
		//        //string paperFormat = null;

		//        try
		//        {
		//            OleDbConnection con = new OleDbConnection(_connectionString);
		//            con.Open();

		//            string sql = string.Format("select * from UniStack_GetLabelNameBundle(-1, {0})"
		//                , bundleId);
		//            OleDbCommand cmd = new OleDbCommand(sql, con);
		//            OleDbDataReader dr = cmd.ExecuteReader();
		//            if (dr.Read())
		//            {
		//                labelGroup = (string)dr["LabelGroup"];
		//                labelName = (string)dr["LabelName"];
		//            }
		//            dr.Close();

		//            if (labelGroup != null && labelGroup.Length > 0 &&
		//                labelName != null && labelName.Length > 0)
		//            {
		//                sql = string.Format("select * from UniStack_GetLabelData(-1, -1, -1, {0})"
		//                    ,  bundleId);

		//                OleDbDataAdapter da = new OleDbDataAdapter(sql, con);
		//                labelData = new DataTable();
		//                da.FillSchema(labelData, SchemaType.Source);
		//                da.Fill(labelData);
		//            }
		//            con.Close();
		//        }
		//        catch (Exception ex)
		//        {
		//            printResult = LabelItem.printerResultTypes.DataError;

		//            _doReconnectDb = true;
		//            AddToLog(LogLevels.Warning, ex.ToString());
		//        }

		//        try
		//        {
		//            labelDesignConnection = new OleDbConnection(_connectionString);
		//            labelDesignConnection.Open();
		//        }
		//        catch (Exception ex)
		//        {
		//            labelDesignConnection = null;
		//            AddToLog(LogLevels.Warning, ex.ToString());
		//        }

		//        if (printResult == LabelItem.printerResultTypes.Unknown)
		//        {
		//            if (labelDesignConnection != null &&
		//                labelDesignConnection.State == ConnectionState.Open)
		//            {
		//                /*  printResult = PrintronixPrinter[0].PrintBundleByBundleId(bundleId, isSTDWithoutTop, topSheetType);*/
		//                //PrintWindowsPrinter(labelData,
		//                //    labelGroup, labelName, documentName, paperFormat, topSheetType);

		//                //PrintronixPrinter[printerNumber - 1].PrintBundleByBundleId(bundleId, isSTDWithoutTop, topSheetType);
		//                printResult = (LabelItem.printerResultTypes)PrintronixPrinter[printerNumber - 1].PrintResult;
		//            }
		//            else
		//            {
		//                printResult = LabelItem.printerResultTypes.ConnectionError;
		//            }
		//        }
		//    }
		//    catch (Exception ex)
		//    {
		//        printResult = LabelItem.printerResultTypes.Unknown;
		//        AddToLog(LogLevels.Warning, ex.ToString());

		//    }
		//    return printResult;
		//}

		//[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
		//private LabelItem.ResultTypes WakeUpManualTopsheet(
		//   uint printerNumber, string documentName)
		//{
		//    LabelItem.ResultTypes printResult =
		//        LabelItem.ResultTypes.Unknown;

		//    try
		//    {
		//        string paperFormat = null;
		//        int printingTimeoutSeconds = 10;

		//        if (printResult == LabelItem.ResultTypes.Unknown)
		//        {
					
		//            if (printerNumber == 1)
		//            {
		//                printResult = _printerManual[0].ActivateWindowsPrinter(documentName, paperFormat, printingTimeoutSeconds);
		//            }
		//            else
		//            {
		//                printResult = _printerManual[1].ActivateWindowsPrinter(documentName, paperFormat, printingTimeoutSeconds);
		//            }
		//        }
		//    }
		//    catch (Exception ex)
		//    {
		//        printResult = LabelItem.ResultTypes.Unknown;
		//        AddToLog(LogLevels.Warning, ex.ToString());

		//    }
		//    return printResult;
		//}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
		private LabelItem.printerResultTypes PrintTopSheetPrintronix(uint printerNumber, TopSheetItem topsheetItem)
		{
			LabelItem.printerResultTypes printResult;

			try
			{
				printResult = PrintronixPrinter[printerNumber - 1].PrintBundleByBundleId(topsheetItem.TitleGroupId, topsheetItem.IssueDateString, topsheetItem.BundleId, isSTDWithoutTop, topsheetItem.TopSheetType);
				//printResult = (LabelItem.printerResultTypes)PrintronixPrinter[printerNumber - 1].PrintResult;
				AddToLog(LogLevels.Warning, "PrintTopSheetPrintronix TitleGroup: " + topsheetItem.TitleGroupId.ToString() +
					" - IssueDate: " + topsheetItem.IssueDateString +
					" - TopsheetType: " + topsheetItem.TopSheetType.ToString() +
					" - BundleId: " + topsheetItem.BundleId.ToString() +
					" - isSTDWithoutTop: " + isSTDWithoutTop.ToString());

			}
			catch (Exception ex)
			{
				printResult = LabelItem.printerResultTypes.Unknown;
				AddToLog(LogLevels.Warning, "PrintTopSheetPrintronix Error:" + ex.ToString());
			}

			return printResult;
		}
		#endregion

		#region SNMP traps

		public List<string> GetPrinterStatus(string ip)
		{

			List<string> statusList = new List<string>();
			try
			{
				OctetString community = new OctetString("public");
				AgentParameters param = new AgentParameters(community)
				{
					Version = SnmpVersion.Ver1
				};
				IpAddress agent = new IpAddress(ip);
				//Construct target  
				UdpTarget target = new UdpTarget((IPAddress)agent, 161, 2000, 1);
				//Pdu class used for all requests  
				Pdu pdu = new Pdu(PduType.Get);
				//pdu.VbList.Add("1.3.6.1.2.1.1.1.0"); //sysDescr  
				//pdu.VbList.Add("1.3.6.1.2.1.1.2.0"); //sysObjectID  
				//pdu.VbList.Add("1.3.6.1.2.1.1.3.0"); //sysUpTime  
				//pdu.VbList.Add("1.3.6.1.2.1.1.4.0"); //sysContact  
				//pdu.VbList.Add("1.3.6.1.2.1.1.5.0"); //sysName  

				//pdu.VbList.Add("1.3.6.1.2.1.25.3.2.1.5.1");
				//pdu.VbList.Add("1.3.6.1.2.1.43.11.1.1.6.1.1"); //Toner container
				//pdu.VbList.Add("1.3.6.1.2.1.43.16.5.1.2.1.1");  //Ready

				//pdu.VbList.Add("1.3.6.1.2.1.43.11.1.1.9.1.1"); //current Toner 
				//pdu.VbList.Add("1.3.6.1.2.1.43.11.1.1.8.1.1");//Max Toner
				//pdu.VbList.Add(".1.3.6.1.2.1.43.8.2.1.10.1.2"); //Cassette 1 paper
				//pdu.VbList.Add(".1.3.6.1.2.1.43.8.2.1.10.1.3");//Cassette 2 paper
				//pdu.VbList.Add(".1.3.6.1.4.1.1347.43.18.2.1.2.1.1"); //Printer Status

				pdu.VbList.Add("1.3.6.1.2.1.25.3.5.1.1.1"); //printing status 1, other   2, unknown 3, Idle   4, printing 5, warmup
				pdu.VbList.Add("1.3.6.1.2.1.25.3.2.1.5.1");// printer  status 1, unknown 2, running 3, waring 4, testing  5, down
				//pdu.VbList.Add("1.3.6.1.2.1.43.16.5.1.2.1.7");//Ribbon life
				pdu.VbList.Add("1.3.6.1.2.1.43.16.5.1.2.1.1"); //Printer Message
				pdu.VbList.Add("1.3.6.1.2.1.43.5.1.1.1.1"); // Printer Config. Already Manual Set 1: Ferag 2: UniMail
															//pdu.VbList.Add("1.3.6.1.2.1.43.16.5.1.2.1.5"); //ETHERNET/PGL/LP+
				
				pdu.VbList.Add("1.3.6.1.4.1.2699.1.2.1.2.1.1.3.0"); // P7000 or P8000
				
				//Make SNMP request  
				SnmpV1Packet result = (SnmpV1Packet)target.Request(pdu, param);

				if (result != null)
				{
					// ErrorStatus other then 0 is an error returned by   
					// the Agent - see SnmpConstants for error definitions  
					if (result.Pdu.ErrorStatus != 0)
					{
						// agent reported an error with the request  
						// txtContent.Text += string.Format("Error in SNMP reply. Error {0} index {1} \r\n",
						//   result.Pdu.ErrorStatus,
						//  result.Pdu.ErrorIndex);
					}
					else
					{
						// Reply variables are returned in the same order as they were added  
						//  to the VbList  
						//string s = "";
						for (int i = 0; i < result.Pdu.VbList.Count; i++)
						{
							//s += string.Format("({0}) ({1}): {2} \r\n",
							// result.Pdu.VbList[i].Oid.ToString(), SnmpConstants.GetTypeName(result.Pdu.VbList[i].Value.Type),
							//  result.Pdu.VbList[i].Value.ToString());
							statusList.Add(result.Pdu.VbList[i].Value.ToString());

						}
					}
					target.Dispose();
				}
				else
				{
					//txtContent.Text += string.Format("No response received from SNMP agent. \r\n");
					target.Dispose();
					return null;
				}

				return statusList;
			}
			catch
			{
				return null; // statusList;
			}
		}

		public List<string> GetP7000NoModePrinterStatus(string ip)
		{

			List<string> statusList = new List<string>();
			try
			{
				OctetString community = new OctetString("public");
				AgentParameters param = new AgentParameters(community)
				{
					Version = SnmpVersion.Ver1
				};
				IpAddress agent = new IpAddress(ip);
				//Construct target  
				UdpTarget target = new UdpTarget((IPAddress)agent, 161, 2000, 1);
				//Pdu class used for all requests  
				Pdu pdu = new Pdu(PduType.Get);
				//pdu.VbList.Add("1.3.6.1.2.1.1.1.0"); //sysDescr  
				//pdu.VbList.Add("1.3.6.1.2.1.1.2.0"); //sysObjectID  
				//pdu.VbList.Add("1.3.6.1.2.1.1.3.0"); //sysUpTime  
				//pdu.VbList.Add("1.3.6.1.2.1.1.4.0"); //sysContact  
				//pdu.VbList.Add("1.3.6.1.2.1.1.5.0"); //sysName  

				//pdu.VbList.Add("1.3.6.1.2.1.25.3.2.1.5.1");
				//pdu.VbList.Add("1.3.6.1.2.1.43.11.1.1.6.1.1"); //Toner container
				//pdu.VbList.Add("1.3.6.1.2.1.43.16.5.1.2.1.1");  //Ready

				//pdu.VbList.Add("1.3.6.1.2.1.43.11.1.1.9.1.1"); //current Toner 
				//pdu.VbList.Add("1.3.6.1.2.1.43.11.1.1.8.1.1");//Max Toner
				//pdu.VbList.Add(".1.3.6.1.2.1.43.8.2.1.10.1.2"); //Cassette 1 paper
				//pdu.VbList.Add(".1.3.6.1.2.1.43.8.2.1.10.1.3");//Cassette 2 paper
				//pdu.VbList.Add(".1.3.6.1.4.1.1347.43.18.2.1.2.1.1"); //Printer Status

				pdu.VbList.Add("1.3.6.1.2.1.25.3.5.1.1.1"); //printing status 1, other   2, unknown 3, Idle   4, printing 5, warmup
				pdu.VbList.Add("1.3.6.1.2.1.25.3.2.1.5.1");// printer  status 1, unknown 2, running 3, waring 4, testing  5, down
														   //pdu.VbList.Add("1.3.6.1.2.1.43.16.5.1.2.1.7");//Ribbon life
				pdu.VbList.Add("1.3.6.1.2.1.43.16.5.1.2.1.1"); //Printer Message
				pdu.VbList.Add("1.3.6.1.2.1.43.5.1.1.1.1"); // Printer Config. Already Manual Set 1: Ferag 2: UniMail
															//pdu.VbList.Add("1.3.6.1.2.1.43.16.5.1.2.1.5"); //ETHERNET/PGL/LP+
				//Make SNMP request  
				SnmpV1Packet result = (SnmpV1Packet)target.Request(pdu, param);

				if (result != null)
				{
					// ErrorStatus other then 0 is an error returned by   
					// the Agent - see SnmpConstants for error definitions  
					if (result.Pdu.ErrorStatus != 0)
					{
						// agent reported an error with the request  
						// txtContent.Text += string.Format("Error in SNMP reply. Error {0} index {1} \r\n",
						//   result.Pdu.ErrorStatus,
						//  result.Pdu.ErrorIndex);
					}
					else
					{
						// Reply variables are returned in the same order as they were added  
						//  to the VbList  
						//string s = "";
						for (int i = 0; i < result.Pdu.VbList.Count; i++)
						{
							//s += string.Format("({0}) ({1}): {2} \r\n",
							// result.Pdu.VbList[i].Oid.ToString(), SnmpConstants.GetTypeName(result.Pdu.VbList[i].Value.Type),
							//  result.Pdu.VbList[i].Value.ToString());
							statusList.Add(result.Pdu.VbList[i].Value.ToString());

						}
					}
					target.Dispose();
				}
				else
				{
					//txtContent.Text += string.Format("No response received from SNMP agent. \r\n");
					target.Dispose();
					return null;
				}

				return statusList;
			}
			catch
			{
				return null; // statusList;
			}
		}


		private TopSheetPrinterStates IsPrinterReadyToPrint(uint PrinterId)
		{
			TopSheetPrinterStates result = TopSheetPrinterStates.PrinterUnknown;
			uint i = PrinterId - 1;
			PrintronixPrint printer = PrintronixPrinter[i];
			try
			{
				if (printer._printerStatus.StatusMessage != "" &&
					!printer._printerStatus.StatusMessage.ToLower().Contains("offline") &&
					(printer._printerStatus.PrintingStatus == 3 || //Idle
					printer._printerStatus.PrintingStatus == 4 || //Printing
					printer._printerStatus.PrintingStatus == 5) && // warmup
					(printer._printerStatus.StatusPrinter == 2 || //running
					printer._printerStatus.StatusPrinter == 3 || //warning
					printer._printerStatus.StatusPrinter == 4)) //testing
				{

					if (printer._printerStatus.StatusPrinter == 3)
					{
						result = TopSheetPrinterStates.PrinterWarning;
					}
					else if (printer._printerStatus.StatusPrinter == 4)
					{
						result = TopSheetPrinterStates.PrinterTesting;
					}
					else if (printer._printerStatus.StatusMessage.ToLower().Contains("jam") ||
						printer._printerStatus.StatusMessage.ToLower().Contains("error"))
					{
						result = TopSheetPrinterStates.PrinterError;
					}
					else if (printer._printerStatus.StatusMessage.ToUpper().Contains("CLOSE PLATEN"))
					{
						result = TopSheetPrinterStates.PrinterPlatenCloseError;
					}
					else if (printer._printerStatus.StatusMessage.ToUpper().Contains("LOAD PAPER"))
					{
						result = TopSheetPrinterStates.PrinterLoadPaperError;
					}
					else if (printer._printerStatus.StatusMessage.ToLower().Contains("ONLINE"))
					{
						result = TopSheetPrinterStates.PrinterReady;
					}
					else if (printer._printerStatus.StatusMessage.ToUpper().Contains("OLD SYSTEM"))
					{
						result = TopSheetPrinterStates.PrinterSystemError;
					}
					else
					{
						result = TopSheetPrinterStates.PrinterReady;
					}
				}
				else
				{
					result = TopSheetPrinterStates.PrinterOffline;
				}
				return result;
			}
			catch
			{
			}
			return result;
		}

		//private void SnmpControl()
		//{
		//    try
		//    {
		//        // Construct a socket and bind it to the trap manager port 162 
		//        IPEndPoint ipep;
		//        EndPoint ep;
		//        int inlen = -1;
		//        bool isError = false;
		//        AddToLog(LogLevels.Debug, "SnmpControl started");

		//        SnmpControlState = SnmpControlStates.Running;

		//        while (IsStarted && ST_snmpThread != null && 
		//            ST_snmpThread.ManagedThreadId == Thread.CurrentThread.ManagedThreadId &&
		//            SnmpControlState != SnmpControlStates.Disconnected)
		//        {
		//            try
		//            {
		//                try
		//                {
		//                    if (ST_snmpSocket == null)
		//                    {
		//                        ST_snmpSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
		//                        ipep = new IPEndPoint(IPAddress.Any, ST_SNMPListeningPort); //normal port = 162
		//                        ep = (EndPoint)ipep;
		//                        ST_snmpSocket.Bind(ep);
		//                        // Disable timeout processing. Just block until packet is received 
		//                        ST_snmpSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 0);
		//                    }

		//                    byte[] indata = new byte[16 * 1024];
		//                    // 16KB receive buffer int inlen = 0;
		//                    IPEndPoint peer = new IPEndPoint(IPAddress.Any, 0);
		//                    EndPoint inep = (EndPoint)peer;
		//                    try
		//                    {
		//                        SnmpControlState = SnmpControlStates.Listening;

		//                        Console.WriteLine("Snmp listening, port=" + ST_SNMPListeningPort.ToString());
		//                        AddToLog(LogLevels.Normal, "Snmp listening, port=" + ST_SNMPListeningPort.ToString());

		//                        inlen = ST_snmpSocket.ReceiveFrom(indata, ref inep);

		//                        Console.WriteLine("Received snmp trap, length=" + inlen.ToString());
		//                        AddToLog(LogLevels.Normal, "Received snmp trap, length=" + inlen.ToString());

		//                        if (inlen > 0)
		//                        {
		//                            // Check protocol version int 
		//                            //string hexString = DataConverting.HexConverting.ToHex(indata, inlen, " ");
		//                            //Console.WriteLine(hexString);
		//                            SnmpAlertMessage alertMessage = new SnmpAlertMessage();
		//                            alertMessage.Registered = DateTime.Now;

		//                            int ver = SnmpPacket.GetProtocolVersion(indata, inlen);
		//                            if (ver == (int)SnmpVersion.Ver1)
		//                            {
		//                                // Parse SNMP Version 1 TRAP packet 
		//                                SnmpV1TrapPacket pkt = new SnmpV1TrapPacket();
		//                                pkt.decode(indata, inlen);


		//                                alertMessage.IpAddress = pkt.Pdu.AgentAddress.ToString();
		//                                alertMessage.TrapSpecific = pkt.Pdu.Specific;

		//                                Console.WriteLine("** SNMP Version 1 TRAP received from {0}:", inep.ToString());
		//                                Console.WriteLine("*** Trap generic: {0}", pkt.Pdu.Generic);
		//                                Console.WriteLine("*** Trap specific: {0}", pkt.Pdu.Specific);
		//                                Console.WriteLine("*** Agent address: {0}", pkt.Pdu.AgentAddress.ToString());
		//                                Console.WriteLine("*** Timestamp: {0}", pkt.Pdu.TimeStamp.ToString());
		//                                Console.WriteLine("*** VarBind count: {0}", pkt.Pdu.VbList.Count);
		//                                Console.WriteLine("*** VarBind content:");

		//                                AddToLog(LogLevels.Debug, string.Format("SnmpControl started"));
		//                                AddToLog(LogLevels.Debug, string.Format("** SNMP Version 1 TRAP received from {0}:", inep.ToString()));
		//                                AddToLog(LogLevels.Debug, string.Format("*** Trap generic: {0}", pkt.Pdu.Generic));
		//                                AddToLog(LogLevels.Debug, string.Format("*** Trap specific: {0}", pkt.Pdu.Specific));
		//                                AddToLog(LogLevels.Debug, string.Format("*** Agent address: {0}", pkt.Pdu.AgentAddress.ToString()));
		//                                AddToLog(LogLevels.Debug, string.Format("*** Timestamp: {0}", pkt.Pdu.TimeStamp.ToString()));
		//                                AddToLog(LogLevels.Debug, string.Format("*** VarBind count: {0}", pkt.Pdu.VbList.Count));
		//                                AddToLog(LogLevels.Debug, string.Format("*** VarBind content:"));

		//                                foreach (Vb v in pkt.Pdu.VbList)
		//                                {
		//                                    Console.WriteLine("**** {0} {1}: {2}",
		//                                        v.Oid.ToString(), SnmpConstants.GetTypeName(v.Value.Type), v.Value.ToString());

		//                                    AddToLog(LogLevels.Debug, string.Format("**** {0} {1}: {2}",
		//                                        v.Oid.ToString(), SnmpConstants.GetTypeName(v.Value.Type), v.Value.ToString()));

		//                                    if (v.Oid.Length >= 13 &&
		//                                        v.Oid[0] == 1 &&
		//                                        v.Oid[1] == 3 &&
		//                                        v.Oid[2] == 6 &&
		//                                        v.Oid[3] == 1 &&
		//                                        (v.Oid[4] == 2 || v.Oid[4] == 4) &&
		//                                        v.Oid[5] == 1 &&
		//                                        (v.Oid[6] == 43 || v.Oid[6] == 1536) &&
		//                                        (v.Oid[7] == 18 || v.Oid[7] == 1) &&
		//                                        (v.Oid[8] == 1 || v.Oid[8] == 3) &&
		//                                        (v.Oid[9] == 1 || v.Oid[9] == 2)
		//                                        )
		//                                    {
		//                                        switch (v.Oid[10])
		//                                        {
		//                                            case 1:
		//                                                try
		//                                                {
		//                                                    string value = v.Value.ToString();

		//                                                    if (value == "PaperJam\r\n")
		//                                                    {
		//                                                        alertMessage.TrapSpecific = 1;
		//                                                        alertMessage.AlertSeverityLevel = 3;
		//                                                        alertMessage.AlertTrainingLevel = 6;
		//                                                        alertMessage.AlertGroup = 8;
		//                                                        alertMessage.AlertGroupIndex = 2;
		//                                                        alertMessage.AlertLocation = 0;
		//                                                        isError = true;
		//                                                    }
		//                                                    else if (value == "CoverOpen\r\n")
		//                                                    {
		//                                                        alertMessage.TrapSpecific = 1;
		//                                                        alertMessage.AlertTrainingLevel = 3;
		//                                                        alertMessage.AlertGroup = 6;
		//                                                        alertMessage.AlertGroupIndex = 1;
		//                                                        alertMessage.AlertLocation = 0;
		//                                                        isError = true;
		//                                                    }
		//                                                    else
		//                                                    {
		//                                                        alertMessage.TrapSpecific = 0;
		//                                                        alertMessage.AlertSeverityLevel = 0;
		//                                                        alertMessage.TrapSpecific = 0;
		//                                                        alertMessage.AlertTrainingLevel = 0;
		//                                                        alertMessage.AlertGroup = 0;
		//                                                        alertMessage.AlertGroupIndex = 0;
		//                                                        alertMessage.AlertLocation = 0;
		//                                                    }
		//                                                }
		//                                                catch { }

		//                                                break;
		//                                            case 2:
		//                                                alertMessage.AlertSeverityLevel = int.Parse(v.Value.ToString());
		//                                                break;

		//                                            case 3:
		//                                                alertMessage.AlertTrainingLevel = int.Parse(v.Value.ToString());
		//                                                break;

		//                                            case 4:
		//                                                alertMessage.AlertGroup = int.Parse(v.Value.ToString());
		//                                                break;

		//                                            case 5:
		//                                                alertMessage.AlertGroupIndex = int.Parse(v.Value.ToString());
		//                                                break;

		//                                            case 6:
		//                                                alertMessage.AlertLocation = int.Parse(v.Value.ToString());
		//                                                break;

		//                                            case 7:
		//                                                alertMessage.AlertCode = int.Parse(v.Value.ToString());
		//                                                break;
		//                                        }
		//                                    }
		//                                }

		//                                if (!isError)
		//                                {
		//                                    /*
		//                                    alertMessage.TrapSpecific = 0;
		//                                    alertMessage.AlertSeverityLevel = 0;
		//                                    alertMessage.TrapSpecific = 0;
		//                                    alertMessage.AlertTrainingLevel = 0;
		//                                    alertMessage.AlertGroup = 0;
		//                                    alertMessage.AlertGroupIndex = 0;
		//                                    alertMessage.AlertLocation = 0;
		//                                    */
		//                                }

		//                                isError = false;
		//                                Console.WriteLine("** End of SNMP Version 1 TRAP data.");
		//                                AddToLog(LogLevels.Debug, "** End of SNMP Version 1 TRAP data.");
		//                            }
		//                            else
		//                            {
		//                                // Parse SNMP Version 2 TRAP packet 
		//                                SnmpV2Packet pkt = new SnmpV2Packet();
		//                                pkt.decode(indata, inlen);
		//                                Console.WriteLine("** SNMP Version 2 TRAP received from {0}:", inep.ToString());
		//                                AddToLog(LogLevels.Debug, string.Format("** SNMP Version 2 TRAP received from {0}:", inep.ToString()));
		//                                if ((SnmpSharpNet.PduType)pkt.Pdu.Type != PduType.V2Trap)
		//                                {
		//                                    Console.WriteLine("*** NOT an SNMPv2 trap ****");
		//                                    AddToLog(LogLevels.Debug, "*** NOT an SNMPv2 trap ****");
		//                                }
		//                                else
		//                                {
		//                                    Console.WriteLine("*** Community: {0}", pkt.Community.ToString());
		//                                    Console.WriteLine("*** VarBind count: {0}", pkt.Pdu.VbList.Count);
		//                                    Console.WriteLine("*** VarBind content:");

		//                                    AddToLog(LogLevels.Debug, string.Format("*** Community: {0}", pkt.Community.ToString()));
		//                                    AddToLog(LogLevels.Debug, string.Format("*** VarBind count: {0}", pkt.Pdu.VbList.Count));
		//                                    AddToLog(LogLevels.Debug, "*** VarBind content:");

		//                                    foreach (Vb v in pkt.Pdu.VbList)
		//                                    {
		//                                        Console.WriteLine("**** {0} {1}: {2}",
		//                                           v.Oid.ToString(), SnmpConstants.GetTypeName(v.Value.Type), v.Value.ToString());
		//                                        AddToLog(LogLevels.Debug, string.Format("**** {0} {1}: {2}",
		//                                           v.Oid.ToString(), SnmpConstants.GetTypeName(v.Value.Type), v.Value.ToString()));
		//                                    }
		//                                    Console.WriteLine("** End of SNMP Version 2 TRAP data.");
		//                                    AddToLog(LogLevels.Debug, "** End of SNMP Version 2 TRAP data.");
		//                                }
		//                            }


		//                            //int offset = 0;

		//                            //UniDevLib.Gui.UserControls.UniLabel.SnmpAlertMessage alertMessage =
		//                            //    new UniDevLib.Gui.UserControls.UniLabel.SnmpAlertMessage();
		//                            //alertMessage.Registered = DateTime.Now;
		//                            //alertMessage.IpAddress = indata[29 + offset].ToString()
		//                            //    + "." + indata[30 + offset].ToString()
		//                            //    + "." + indata[31 + offset].ToString()
		//                            //    + "." + indata[32 + offset].ToString();
		//                            //alertMessage.TrapSpecific = indata[38 + offset];
		//                            //alertMessage.AlertSeverityLevel = indata[84 + offset];
		//                            //alertMessage.AlertTrainingLevel = indata[103 + offset];
		//                            //alertMessage.AlertGroup = indata[122 + offset];
		//                            //alertMessage.AlertGroupIndex = indata[141 + offset];
		//                            //alertMessage.AlertLocation = indata[160 + offset];
		//                            //alertMessage.AlertCode = indata[179 + offset] * 256 + indata[180 + offset];

		//                            PrinterStatus printerStatus = null;

		//                            AddToLog(LogLevels.Debug, "SNMP search for IP address " +
		//                                alertMessage.IpAddress != null ? alertMessage.IpAddress : "NULL");

		//                            if (alertMessage.IpAddress != null)
		//                            {
		//                                bool isFound = false;
		//                                int index = -1;
		//                                lock (ST_lock_snmpWatcherTopSheetLines)
		//                                {
		//                                    while (!isFound && ++index < ST_snmpWatcherTopSheetLines.Count)
		//                                    {
		//                                        TopSheetMain topSheetLine = ST_snmpWatcherTopSheetLines[index];
		//                                        string ip1 = topSheetLine.PrinterIp1;

		//                                        if (alertMessage.IpAddress.Equals(ip1))
		//                                        {
		//                                            printerStatus = null; // topSheetLine.Printer1.PrinterStatus;
		//                                            isFound = true;
		//                                        }
		//                                    }
		//                                }
		//                            }

		//                            if (printerStatus != null)
		//                            {
		//                                AddToLog(LogLevels.Debug,
		//                                    string.Format("SNMP printer found IP address {0}"
		//                                    + " - AlertSeverityLevel={1}"
		//                                    + " - AlertTrainingLevel={2}"
		//                                    + " - AlertGroup={3}"
		//                                    + " - AlertGroupIndex={4}"
		//                                    + " - AlertLocation={5}"
		//                                    + " - TrapSpecific={6}"
		//                                    , alertMessage.IpAddress
		//                                    , alertMessage.AlertSeverityLevel
		//                                    , alertMessage.AlertTrainingLevel
		//                                    , alertMessage.AlertGroup
		//                                    , alertMessage.AlertGroupIndex
		//                                    , alertMessage.AlertLocation
		//                                    , alertMessage.TrapSpecific
		//                                    ));

		//                                printerStatus.AlertList.Add(alertMessage);
		//                                while (printerStatus.AlertList.Count > 100)
		//                                {
		//                                    printerStatus.AlertList.RemoveAt(0);
		//                                }


		//                                if (alertMessage.AlertSeverityLevel == 3 &&
		//                                    alertMessage.AlertTrainingLevel == 6 &&
		//                                    alertMessage.AlertGroup == 8 &&
		//                                    alertMessage.AlertGroupIndex == 2 &&
		//                                    alertMessage.AlertLocation == 0)
		//                                {
		//                                    if (alertMessage.TrapSpecific == 1)
		//                                    {
		//                                        printerStatus.TrapPaperOk = false;
		//                                    }
		//                                    else// if (alertMessage.TrapSpecific == 2)
		//                                    {
		//                                        printerStatus.TrapPaperOk = true;
		//                                    }
		//                                }
		//                                else if (alertMessage.AlertSeverityLevel == 3 &&
		//                                        alertMessage.AlertTrainingLevel == 3 &&
		//                                        alertMessage.AlertGroup == 6 &&
		//                                        alertMessage.AlertGroupIndex == 1 &&
		//                                        alertMessage.AlertLocation == 0)
		//                                {
		//                                    if (alertMessage.TrapSpecific == 1)
		//                                    {
		//                                        printerStatus.TrapDoorOk = false;
		//                                    }
		//                                    else// if (alertMessage.TrapSpecific == 2)
		//                                    {
		//                                        printerStatus.TrapDoorOk = true;
		//                                    }
		//                                }
		//                                else if (alertMessage.AlertSeverityLevel == 3 &&
		//                                        alertMessage.AlertTrainingLevel == 3 &&
		//                                        alertMessage.AlertGroup == 11 &&
		//                                        alertMessage.AlertGroupIndex == 1 &&
		//                                        alertMessage.AlertLocation == 0)
		//                                {
		//                                    if (alertMessage.TrapSpecific == 1)
		//                                    {
		//                                        printerStatus.TrapTonerOk = false;
		//                                    }
		//                                    else// if (alertMessage.TrapSpecific == 2)
		//                                    {
		//                                        printerStatus.TrapTonerOk = true;
		//                                    }
		//                                }
		//                                else
		//                                {
		//                                    if (alertMessage.TrapSpecific == 1)
		//                                    {
		//                                        printerStatus.TrapPrinterOk = false;
		//                                    }
		//                                    else
		//                                    {
		//                                        printerStatus.TrapPrinterOk = true;
		//                                    }
		//                                }
		//                            }

		//                            /*
		//                            Console.Write(indata[29].ToString());
		//                            Console.Write("." + indata[30].ToString());
		//                            Console.Write("." + indata[31].ToString());
		//                            Console.Write("." + indata[32].ToString());
		//                            Console.Write("-" + indata[38].ToString());
		//                            Console.Write("-" + indata[84 + offset].ToString());
		//                            Console.Write(" " + indata[103 + offset].ToString());
		//                            Console.Write(" " + indata[122 + offset].ToString());
		//                            Console.Write(" " + indata[141 + offset].ToString());
		//                            Console.Write(" " + indata[160 + offset].ToString());
		//                            Console.Write(" " + (indata[179 + offset] * 256 + indata[180 + offset]).ToString());
		//                            Console.WriteLine();
		//                            */
		//                        }
		//                        else
		//                        {
		//                            if (inlen == 0)
		//                            {
		//                                Console.WriteLine("Zero length packet received.");
		//                                AddToLog(LogLevels.Debug, "Zero length packet received.");
		//                            }
		//                        }

		//                        //TopSheetPrinterStates test = ST_snmpWatcherTopSheetLines[1].TopSheetPrinterState2;
		//                    }
		//                    catch (Exception ex)
		//                    {
		//                        AddToLog(LogLevels.Warning, ex.ToString());
		//                        try
		//                        {
		//                            if (ST_snmpSocket != null)
		//                            {
		//                                ST_snmpSocket.Close();
		//                                ST_snmpSocket = null;
		//                            }
		//                        }
		//                        catch
		//                        {
		//                        }
		//                        finally
		//                        {

		//                        }
		//                    }
		//                }
		//                catch (Exception ex)
		//                {
		//                    AddToLog(LogLevels.Warning, ex.ToString());
		//                }
		//            }
		//            catch
		//            {
		//            }
		//        }

		//        if (ST_snmpThread.ManagedThreadId != Thread.CurrentThread.ManagedThreadId)
		//        {
		//            AddToLog(LogLevels.Debug, "SnmpControl old thread exited");
		//        }
		//        else
		//        {

		//            SnmpControlState = SnmpControlStates.NotRunning;
		//            ST_snmpWatcher = null;
		//            AddToLog(LogLevels.Debug, "SnmpControl stopped");
		//        }
		//    }
		//    catch
		//    {
		//    }
		//}

		private void SnmpControl()
		{
			try
			{
				AddToLog(LogLevels.Debug, "SnmpControl Started");
				SnmpControlState = SnmpControlStates.Running;

				//string[] lastPrintingStatus = new string[] { "0", "0", "0", "0" };

				while (IsStarted && 
					ST_snmpThread != null  &&
					ST_snmpThread.ManagedThreadId == Thread.CurrentThread.ManagedThreadId)
				{
					try
					{
						for (int i = 0; i < NUM_DBH; i++)
						{
							if (IcpConnectionState == IcpConnectionStates.Connected)
							{
								if (PingTest(_printerIp[i]))
								{
									PrintronixPrinter[i].CanReset = true;
									PrintronixPrinter[i].printerStatusList = GetPrinterStatus(_printerIp[i]);

									if(PrintronixPrinter[i].printerStatusList.Count == 0) // One Special P7000 Printer No Mode SNMP Code
                                    {
										PrintronixPrinter[i].printerStatusList = GetP7000NoModePrinterStatus(_printerIp[i]);

										if (PrintronixPrinter[i].printerStatusList.Count != 0)
                                        {
											if (PrintronixPrinter[i].printerStatusList[2].Contains("PGL"))
											{
												PrintronixPrinter[i].printerStatusList.Add("P7000");
											}
										}
									}
								}
								else
								{
									PrintronixPrinter[i].CanReset = false;
									PrintronixPrinter[i].printerStatusList = null;
									AddToLog(LogLevels.Error, "Printer " + (i + 1).ToString() + " Failed " + _printerIp[i]);
								}
							}

							if (PrintronixPrinter[i].printerStatusList != null && 
								PrintronixPrinter[i].printerStatusList.Count >= 4  &&
								//PrintronixPrinter[i].printerStatusList[4].Trim().ToUpper().Contains("ETHERNET"))
								PrintronixPrinter[i].printerStatusList[3].Trim() == "2")
							{
								PrintronixPrinter[i]._printerStatus.PrintingStatus = byte.Parse(PrintronixPrinter[i].printerStatusList[0]);
								PrintronixPrinter[i]._printerStatus.StatusPrinter = byte.Parse(PrintronixPrinter[i].printerStatusList[1]);
								//PrintronixPrinter[i]._printerStatus.RibbonLife = PrintronixPrinter[i].printerStatusList[2].Substring(12);

								
								try
								{
                                    if (PrintronixPrinter[i].printerStatusList[4].Trim().ToUpper().Contains("P7000"))
                                    {
                                        PrintronixPrinter[i].IsP8000 = false;
                                    }
                                    else
                                    {
                                        PrintronixPrinter[i].IsP8000 = true;
                                    }
                                    //AddToLog(LogLevels.Debug, "Printer " + (i + 1).ToString() + " isP8000: " + PrintronixPrinter[i].IsP8000 + " " + PrintronixPrinter[i].printerStatusList[4].ToUpper());

                                    if (PrintronixPrinter[i].LogError != "")
									{
										PrintronixPrinter[i]._printerStatus.StatusMessage = "Communication Lost";
									}
									else if (_status.cIsPaperJamOrMissing != null)
									{
										if (_status.cIsPaperJamOrMissing[i] != 0)
										{ 
											PrintronixPrinter[i]._printerStatus.StatusMessage = "Jamed Or Missing";
										}
									}
									else
									{
										PrintronixPrinter[i]._printerStatus.StatusMessage = PrintronixPrinter[i].printerStatusList[2].Trim();
									}
								}
								catch(Exception ex)
								{
									AddToLog(LogLevels.Warning, "TopsheetMain Status Exception" + ex.ToString());
									PrintronixPrinter[i]._printerStatus.StatusMessage = PrintronixPrinter[i].printerStatusList[2].Trim();
								}

								//PrintronixPrinter[i]._printerStatus.PrinterOnNet = PrintronixPrinter[i].printerStatusList[4].Trim();
								//AddToLog(LogLevels.Warning, "Printer " + (i + 1) + " Status: " + PrintronixPrinter[i]._printerStatus.StatusMessage);
								if (PrintronixPrinter[i]._printerStatus.StatusMessage.ToLower().Contains("online"))
								{
									PrintronixPrinter[i]._lastPrinterMessage = "OFFLINE";
								}
								else
								{
									PrintronixPrinter[i]._lastPrinterMessage = "ONLINE";
								}

								//SavePrinterStatus(_connectionString_cheng, _printerIp[i], _printer[i]._printerStatus.TonerPercent,
								//    _printer[i]._printerStatus.PaperPercent, _printer[i]._printerStatus.StatusMessage);
							}
							else //Printer offline
							{
								if (PrintronixPrinter[i].printerStatusList != null &&
									PrintronixPrinter[i].printerStatusList.Count != 0 &&
									//PrintronixPrinter[i].printerStatusList[4].Trim().ToUpper().Contains("PARL"))
									PrintronixPrinter[i].printerStatusList[3].Trim() != "2")
								{
									if (PrintronixPrinter[i].printerStatusList[3].Trim() == "1")
									{
										PrintronixPrinter[i]._printerStatus.StatusMessage = "OLD SYSTEM";
									}
									else
									{ 
										PrintronixPrinter[i]._printerStatus.StatusMessage = "Error Need Restart";
									}
								}
								else
								{
									PrintronixPrinter[i]._printerStatus.StatusMessage = "OFFLINE";
								}
								//AddToLog(LogLevels.Warning, "Printer " + (i + 1) + " Status: " + PrintronixPrinter[i]._printerStatus.StatusMessage);
								//AddToLog(LogLevels.Warning, "printerStatusList is null" + _printerIp[i]);
								PrintronixPrinter[i]._lastPrinterMessage = "ONLINE";
							}

							PrinterStates[i] = IsPrinterReadyToPrint((uint)(i + 1));

							_printerStatus[i]["PrintingStatus"] = PrintronixPrinter[i]._printerStatus.PrintingStatus.ToString();
							_printerStatus[i]["PrinterStatus"] = PrintronixPrinter[i]._printerStatus.StatusPrinter.ToString();
							//_printerStatus[i]["RibbonLife"] = PrintronixPrinter[i]._printerStatus.RibbonLife.ToString();
							_printerStatus[i]["StatusMessage"] = PrintronixPrinter[i]._printerStatus.StatusMessage;

							if (PrintronixPrinter[i]._printerStatus.PrintingStatus != PrintronixPrinter[i]._lastPrintingStatus)
							{
								PrintronixPrinter[i]._lastPrintingStatus = PrintronixPrinter[i]._printerStatus.PrintingStatus;

								AddToLog(LogLevels.Warning, "Printer " + (i + 1) + 
									" Printing Status: " + PrintronixPrinter[i]._printerStatus.PrintingStatus.ToString() + 
									" Last Printing Status: " + PrintronixPrinter[i]._lastPrintingStatus.ToString());
							}


							//if (lastPrintingStatus[i] != PrintronixPrinter[i]._printerStatus.PrintingStatus.ToString())
							//{
							//    AddToLog(LogLevels.Warning, "Printer " + (i+1) + " Printing Status: " + PrintronixPrinter[i]._printerStatus.PrintingStatus.ToString());
							//    lastPrintingStatus[i] = PrintronixPrinter[i]._printerStatus.PrintingStatus.ToString();
							//}
						}

						PrintronixPrinterReset();

					}
					catch (Exception ex)
					{
						AddToLog(LogLevels.Warning, "SNMPControl Exception" + ex.ToString());
					}

					Thread.Sleep(1000);
				}

				if (ST_snmpThread.ManagedThreadId != Thread.CurrentThread.ManagedThreadId)
				{
					AddToLog(LogLevels.Debug, "SnmpControl old thread exited");
				}
				else
				{
					SnmpControlState = SnmpControlStates.NotRunning;
					ST_snmpWatcher = null;
					AddToLog(LogLevels.Debug, "SnmpControl stopped");
				}
			}
			catch
			{
			}
		}

		private async void PrintronixPrinterReset()
		{
			try
			{
				if (IcpConnectionState == IcpConnectionStates.Connected)
				{
					for (int printerNo = 0; printerNo < NUM_DBH; printerNo++)
					{
						if (PrintronixPrinter[printerNo].CanReset)
						{
							// Printer Online/Jam/Communication Lost -> Offline
							if ((PrintronixPrinter[printerNo]._printerStatus.StatusMessage.ToLower().Contains("offline") ||
								PrintronixPrinter[printerNo]._printerStatus.StatusMessage.ToLower().Contains("communication") ||
								PrintronixPrinter[printerNo]._printerStatus.StatusMessage.ToLower().Contains("jam") ||
								PrintronixPrinter[printerNo].printerStatusList[3].Trim().ToLower().Contains("offline")) &&
								PrintronixPrinter[printerNo]._lastPrinterMessage.ToLower().Contains("online") &&
								PrintronixPrinter[printerNo].ResetDone)
							{
								PrintronixPrinter[printerNo].ResetDone = false;

								if (PrintronixPrinter[printerNo]._lastPrintingStatus == 4)
								{
									//Enable Cutter Reject to Waste After Printer Online -> Offline
									//To Do: Enable Waste Order, All LableState Before 50 will Reject to Waste.
									IcpGetOrAddTelegram(IcpTelegramTypes.EnableCutter, (byte)(printerNo + 1), true);
									AddToLog(LogLevels.Warning, "Printer " + (printerNo + 1) + " Status: " + PrintronixPrinter[printerNo]._printerStatus.PrintingStatus +
									   " - LastPrintingStatus: " + PrintronixPrinter[printerNo]._lastPrintingStatus + " - Enable Cutter");
								}
								else
								{
									AddToLog(LogLevels.Warning, "Printer " + (printerNo + 1) + " Status: " + PrintronixPrinter[printerNo]._printerStatus.PrintingStatus +
									   " - LastPrintingStatus: " + PrintronixPrinter[printerNo]._lastPrintingStatus + " Not Cutter");
								}

								try
								{
									IcpGetOrAddTelegram(IcpTelegramTypes.PrinterLightAlarmOn, (byte)(printerNo+1), true);
									AddToLog(LogLevels.Debug, "Printer " + (printerNo + 1) + " Light Alarm Off ");

								}
								catch (Exception ex)
								{
									AddToLog(LogLevels.Error, "Printer " + (printerNo + 1) + " Light Alarm Off Error: " + ex.ToString());
								}

								PrintronixPrinter[printerNo].LogError = "";
								PrintronixPrinter[printerNo].Disconnect();

								AddToLog(LogLevels.Warning, "Connected Printer " + (printerNo + 1) + " ResetDone: False, LogError Cleared and PrintronixPrinter Disconnected!");
								AddToLog(LogLevels.Warning, "Printer " + (printerNo + 1) + " StatusMessage: " + PrintronixPrinter[printerNo]._printerStatus.StatusMessage.ToUpper() +
									   " - LastPrinterMessage: " + PrintronixPrinter[printerNo]._lastPrinterMessage.ToUpper() +
									   " - Status: " + PrintronixPrinter[printerNo]._printerStatus.PrintingStatus);
							}

							// Printer Offline -> Online && (Printer Idle || Printer Printing)
							if ((PrintronixPrinter[printerNo]._printerStatus.StatusMessage.ToLower().Contains("online") ||
								PrintronixPrinter[printerNo].printerStatusList[3].Trim().ToLower().Contains("online")) &&
								(PrintronixPrinter[printerNo]._printerStatus.PrintingStatus == 3 ||
								PrintronixPrinter[printerNo]._printerStatus.PrintingStatus == 4 ) &&
								PrintronixPrinter[printerNo]._lastPrinterMessage.ToLower().Contains("offline") &&
								!PrintronixPrinter[printerNo].ResetDone)
							{
								PrintronixPrinter[printerNo].ResetDone = true;

								TopSheetItem restItem = new TopSheetItem
								{
									BundleId = 65500,
									BundleIndex = 0,
									PrinterNumber = (byte)(printerNo + 1),
									TopSheetType = TopSheetTypes.WHITEPAPER,
									LabelState = 50
								};
								//TestPrint((byte)(printerNo + 1));
								try
								{
									IcpGetOrAddTelegram(IcpTelegramTypes.PrinterLightAlarmOff, (byte)(printerNo + 1), true);
									AddToLog(LogLevels.Debug, "Printer " + (printerNo + 1) + " Light Alarm On ");

								}
								catch (Exception ex)
								{
								}

								try
								{
									IcpGetOrAddTelegram(IcpTelegramTypes.ResetPrinter, (byte)(printerNo + 1), true);
								}
								catch (Exception ex)
								{
									AddToLog(LogLevels.Error, "Printer " + (printerNo + 1) + " Reset Error: " + ex.ToString());

								}

								if (IsProductionStarted)
								{
									// Avoid Textprint conflict with Normal print logic
									if ((printerNo + 1) == DefaultPrinter)
									{
										//PrintronixPrinter[printerNo].ResetDone = true;
										lock (_lock_TsBundleQueue)
										{
											TsBundleQueue.Enqueue(restItem);
											TsBundleQueue.Enqueue(restItem);
										}
										
										AddToLog(LogLevels.Warning, "Printer " + (printerNo + 1) + " Add Two White Paper Due To Reset!");

									}
									else
									{
										await Task.Run(
										() => ResetPrinter(printerNo + 1)
										);
										AddToLog(LogLevels.Warning, "Backup Printer " + (printerNo + 1) + " Test Printer Twice Due To Reset!");

									}
								}
								else
								{
									await Task.Run(
										() => ResetPrinter(printerNo + 1)
									);
									AddToLog(LogLevels.Warning, "Printer " + (printerNo + 1) + " Test Printer Twice Due To Reset!");

									//ResetPrinter(printerNo + 1);
								}

								AddToLog(LogLevels.Warning, "Printer " + (printerNo + 1) + " ResetDone: " + PrintronixPrinter[printerNo].ResetDone);
								AddToLog(LogLevels.Warning, "Printer "+ (printerNo + 1) + " Status: " + PrintronixPrinter[printerNo]._printerStatus.StatusMessage.ToUpper() +
										" - LastPrinterMessage: " + PrintronixPrinter[printerNo]._lastPrinterMessage.ToUpper());
							}
						}
						else
						{
							//AddToLog(LogLevels.Warning, "Disconnected Printer ResetDone : False");
							//AddToLog(LogLevels.Warning, "Printer Status: " + PrintronixPrinter[printerNo]._printerStatus.StatusMessage.ToUpper() +
							//               " - LastPrinterMessage: " + PrintronixPrinter[printerNo]._lastPrinterMessage.ToUpper());

							PrintronixPrinter[printerNo].ResetDone = false;

						}
					}
				}
			}
			catch
			{
			}
		}

		private void SnmpDisconnect()
		{
			AddToLog(LogLevels.Warning, "SnmpDisconnect");

			SnmpControlState = SnmpControlStates.Disconnected;

			if (ST_snmpSocket != null)
			{
				try
				{
					ST_snmpSocket.Close();
				}
				catch
				{
				}
				finally
				{
					ST_snmpSocket = null;
				}
			}
		}

		private bool PingTest(string ipAddress)
		{
			Ping ping = new Ping();
			try
			{
				PingReply pingStatus = ping.Send(IPAddress.Parse(ipAddress), 1000);

				if (pingStatus.Status == IPStatus.Success)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch
			{
				return false;
			}
		}

		private byte BoolToByte(bool isBool)
		{
			if (isBool)
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}

		#endregion

		#region Tools
		//private TopSheetItem GetTopSheetItemPrinted(uint bundleIndex, bool doRemove)//, out int listIndex)
		//{
		//    TopSheetItem result = null;

		//    bool isFound = false;
		//    int listIndex = -1;

		//    //lock (_topSheetQueue)
		//    try
		//    {
		//        lock (_lock_topSheetPrinted)
		//        {
		//            while (!isFound && ++listIndex < _topSheetPrinted.Count)
		//            {
		//                TopSheetItem queueItem = _topSheetPrinted[listIndex];
		//                if (queueItem.BundleIndex == bundleIndex)
		//                {
		//                    result = queueItem;
		//                    isFound = true;

		//                    if (doRemove)
		//                    {
		//                        _topSheetPrinted.RemoveAt(listIndex);
		//                    }
		//                }
		//            }
		//        }

		//        return result;
		//    }
		//    catch
		//    {
		//    }
		//    return result;
		//}

		private TopSheetItem GetTopSheetItemPrinted(uint bundleId, bool doRemove)//, out int listIndex)
		{
			TopSheetItem result = null;

			bool isFound = false;
			int listIndex = -1;

			//lock (_topSheetQueue)
			try
			{
				lock (_lock_topSheetPrinted)
				{
					while (!isFound && ++listIndex < _topSheetPrinted.Count)
					{
						TopSheetItem queueItem = _topSheetPrinted[listIndex];
						if (queueItem.BundleId == bundleId)
						{
							result = queueItem;
							isFound = true;

							if (doRemove)
							{
								_topSheetPrinted.RemoveAt(listIndex);
							}
						}
					}
				}

				return result;
			}
			catch
			{
			}
			return result;
		}

		public byte GetPcStatusPrinter(int printerNumber, out bool isPausedByOperator)
		{
			byte result = 0;
			isPausedByOperator = false;

			PrintronixPrint printer =
				printerNumber == 1 ? PrintronixPrinter[0] : PrintronixPrinter[1];

			try
			{
				if (printer != null)
				{
					byte activeState = 0;

					//if (IcpConnectionState == IcpConnectionStates.Connected)
					//{
					//    //activeState = printerNumber == 1 ? _status.Printer1ActiveState : _status.Printer2ActiveState; //cheng 09-18
					//}

					if (activeState == 2)//de-activated
					{
						isPausedByOperator = true;

						result = 2;
					}
					else if (activeState >= 20)//de-activating
					{
						isPausedByOperator = true;

						int printerQueueCount =
							printerNumber == 1 ?
							_status.cLabelQueueCount[0] : _status.cLabelQueueCount[1]; // cheng 09-18
						DateTime lastPrint = printerNumber == 1 ? _lastPrint[0] : _lastPrint[1];

						if (printerQueueCount == 0 && lastPrint < DateTime.Now.AddSeconds(-12))
						{
							result = 2;//de-activated
						}
					}
					//else if (activeState >= 10)//activating
					//{
					//    if (printer.PrinterStatus != null && printer.PrinterStatus.IsReadyToPrint)
					//    {
					//        result = 1;//activated
					//    }
					//    else
					//    {
					//        result = 2;
					//    }
					//}

					if (result == 0)
					{
						//if (printer.PrinterStatus != null && printer.PrinterStatus.IsReadyToPrint)
						//{
						result = 1;
						//}
					}
				}
				else
				{
					if (!UseTwoPrinters && printerNumber == 2)
					{
						result = 1;//set as ok to icpcon
					}
				}
				return result;
			}
			catch
			{
			}

			return result;
		}

		#endregion

		#region Logging
		public void AddToLog(Exception ex)
		{
			try
			{
				if (ex != null)
				{
					AddToLog(LogLevels.Error, ex.ToString());
				}
			}
			catch
			{
			}
		}

		public void AddToLog(string message)
		{
			try
			{
				if (message != null)
				{
					AddToLog(LogLevels.Normal, message);
				}
			}
			catch
			{
			}
		}

		public void AddToLog(LogLevels logLevel, string message)
		{
			try
			{
				if (_enableLogDebug) // || logLevel != LogLevels.Debug)
				{
					if (_enableLogFile)
					{
						Logging.AddToLog(logLevel, message);
						lock (_lock_logItemList)
						{
							LogItem logItem = new LogItem
							{
								LogLevel = logLevel,
								Message = message,
								LogTime = DateTime.Now
							};

							_logItemList.Add(logItem);
						}
					}
				}
			}
			catch
			{
			}
		}

		public void LogControl()
		{
			try
			{
				DateTime lastLogToFile = DateTime.MinValue;

				while (IsStarted && _logThread != null && _logThread.ManagedThreadId == Thread.CurrentThread.ManagedThreadId)
				{
					try
					{
						if (lastLogToFile < DateTime.Now.AddSeconds(-10))
						{
							lastLogToFile = DateTime.Now;

							LogItem[] logItemArray = null;

							lock (_lock_logItemList)
							{
								if (_logItemList.Count > 0)
								{
									logItemArray = _logItemList.ToArray();
									_logItemList.Clear();
								}
							}

							if (logItemArray != null)
							{
								Logging.AddToLog(_logFolder, logItemArray);
							}
						}
					}
					catch
					{
					}
					finally
					{
						Thread.Sleep(500);
					}
				}
			}
			catch
			{
			}
		}
		#endregion
	}
}
