﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Data.OleDb;
using System.Net.Sockets;
using System.IO;
using System.Runtime.InteropServices;
using System.Data;
using System.Net;
using SnmpSharpNet;

namespace TopSheetControlLib
{
    public class TopSheetMain : IDisposable
    {
        //public static List<string> LogList;
        //public void AddToLogList(string text)
        //{
        //    LogList.Add(DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff - ") + text);
        //}

        #region Constants

        private const byte SOH = 0x01;
        private const byte EOT = 0x04;
        private const byte MAX_LABEL_POSITIONS = 4;
        private const byte MAX_BUNDLE_POSITIONS = 10;
        private const byte MAX_BUNDLE_HANDLED = 20;
        private const byte MAX_PRINTERS = 2;
        private const byte MAX_SLOTS = 4;

        #endregion

        #region Structs

        private struct DbCommandStuct
        {
            public DbCommandTypes dbCommandType;
            public string sql;
            public object oParams;
        }

        //public struct DBTrackingCommand
        //{
        //    public byte ALSLineNumber;
        //    public DBTrackingCommandEnum Command;
        //    public ushort BundleNumber;
        //    public ushort BundleIndex;
        //    public ushort LabelBundleIndex;
        //    public byte BAHandledCode;
        //}

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct BundleHandledStruct
        {
            public byte Code;
            public ushort ConveyorBundleIndex;
            public ushort LabelBundleIndex;
            public ushort RejectedBundleIndex;
            public byte Position;
            public byte PrinterIndex;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct StatusPositionStruct
        {
            public byte State;
            public byte RejectPosState;
            public byte ReadyToReceive;
            public byte ReadyToTransport;
            public uint LastBundleTick;
            public uint LastNotReadyTick;
	        public uint LastStartTransportTick;
            public uint Tick;
            public uint RejectPosTick;
            public uint ExpectedMinTick;
            public uint ExpectedMaxTick;
            public uint RemovedTick;
            public ushort ExpectedBundleIndex;
            public ushort CurrentBundleIndex;
            public ushort LeavingBundleIndex;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct StatusStruct
        {
            public byte SOH;
            public byte TelegramId;
            public byte Command;
            public ushort Length;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = MAX_SLOTS)]
            public ushort[] DigitalISlot;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = MAX_SLOTS)]
            public ushort[] DigitalOSlot;

            public byte StartStopState;
            public byte StartStopConvState;
            public byte StartStopLastConvState;

            public byte AlarmThermoState1;
            public byte AlarmThermoState2;

            public byte Printer1ActiveState;
            public byte Printer2ActiveState;

            public byte NextLabelingPrinterNumber;
            public byte StackerExitState;

            public byte FunctionInputState1;
            public byte FunctionInputState2;
            //public byte FunctionInputState3;
            public byte AlarmTemperatureState;

            public byte ApplicatorStopConveyor;
            public byte ApplicationState;
            public ushort ApplyingLabelIndex;
            public byte RejectState;

            public byte ReadyToPreviousApplicatorState;
            public byte ReadyToPreviousConveyorState;
            public byte NextMachineReadyState;

            public byte DetectedStackerPosition;

            public byte BundleQueueCount;
            public byte LabelQueueCountPrinter1;
            public byte LabelQueueCountPrinter2;

            public byte BundlesHandledCount;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = MAX_BUNDLE_HANDLED)]
            public BundleHandledStruct[] BundlesHandled;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = MAX_BUNDLE_POSITIONS)]
            public StatusPositionStruct[] ConveyorPositions;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = MAX_LABEL_POSITIONS)]
            public StatusPositionStruct[] LabelPositionsPrinter1;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = MAX_LABEL_POSITIONS)]
            public StatusPositionStruct[] LabelPositionsPrinter2;

            public byte EOT;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct SetupPositionStruct
        {
            //public byte Position;
            //public byte Position;
            public byte EnteringInputSlot;
            public byte EnteringInputSignal;
            public byte EnteringInputOnHigh;
            public byte PositionInputSlot;
            public byte PositionInputSignal;
            public byte PositionInputOnHigh;
            public byte ConveyorStoppedInputSlot;
            public byte ConveyorStoppedInputSignal;
            public byte ConveyorStoppedInputOnHigh;
            public byte GateReadyInputSlot;
            public byte GateReadyInputSignal;
            public byte GateReadyInputOnHigh;
            public short ReceiveExpected;
            public short ReceiveTimeout;
            public byte VakuumOutputSlot;
            public byte VakuumOutputSignal;
            public byte EnteringTransportOutputSlot;
            public byte EnteringTransportOutputSignal;
            public byte TransportOutputSlot;
            public byte TransportOutputSignal;
            public byte BrakeOutputSlot;
            public byte BrakeOutputSignal;
            public short DelayBeforeBrake;
            public short BrakeTime;
            public short StopDelay;
            public short StartDelay;
            public short StopDelayLeave;
            public byte EncoderChannel;
            public byte EncoderInputSlot;
            public byte EncoderInputSignal;
            public byte AlarmInputSlot;
            public byte AlarmInputSignal;
            public byte AlarmInputOnHigh;
            public byte UsePulseTiming;
            public byte RejectOutputSlot;
            public byte RejectOutputSignal;
            public byte RejectPosInputSlot;
            public byte RejectPosInputSignal;
            public byte RejectPosInputOnHigh;
            public byte SidePlatesOutputSlot;
            public byte SidePlatesOutputSignal;
            public byte SidePlatesMode;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct SetupStruct
        {
            public byte LineNumber;
            public byte ApplicatorType;
            public byte CurrentStackerPosition;
            public byte LabelingConveyorPosition;
            public byte FirstConveyorControlPosition;
            public byte ReadyToPreviousControlPosition;

            public short InputSignalsFilter;

            public byte ButtonStartInputSlot;
            public byte ButtonStartInputSignal;
            public byte ButtonStartInputOnHigh;

            public byte ButtonStopInputSlot;
            public byte ButtonStopInputSignal;
            public byte ButtonStopInputOnHigh;

            public byte ButtonResetInputSlot;
            public byte ButtonResetInputSignal;
            public byte ButtonResetInputOnHigh;

            public byte ButtonStartConvInputSlot;
            public byte ButtonStartConvInputSignal;
            public byte ButtonStartConvInputOnHigh;

            public byte ButtonStopConvInputSlot;
            public byte ButtonStopConvInputSignal;
            public byte ButtonStopConvInputOnHigh;

            public byte StackerExitInputSlot;
            public byte StackerExitInputSignal;
            public byte StackerExitInputOnHigh;

            public byte Function1InputSlot;
            public byte Function2InputSlot;
            public byte Function3InputSlot;

            public byte Function1InputSignal;
            public byte Function2InputSignal;
            public byte Function3InputSignal;

            public byte Function1InputOnHigh;
            public byte Function2InputOnHigh;
            public byte Function3InputOnHigh;

            //public byte AlarmTemperatureInputSlot;
            //public byte AlarmTemperatureInputSignal;
            //public byte AlarmTemperatureInputOnHigh;

            public byte ArmUpInputSlot;
            public byte ArmUpInputSignal;
            public byte ArmUpInputOnHigh;

            public byte ArmFrontInputSlot;
            public byte ArmFrontInputSignal;
            public byte ArmFrontInputOnHigh;

            public byte ArmBackInputSlot;
            public byte ArmBackInputSignal;
            public byte ArmBackInputOnHigh;

            public byte ArmOnBundleInputSlot;
            public byte ArmOnBundleInputSignal;
            public byte ArmOnBundleInputOnHigh;

            public byte RejectReadyInputSlot;
            public byte RejectReadyInputSignal;
            public byte RejectReadyInputOnHigh;

            public byte RejectBackInputSlot;
            public byte RejectBackInputSignal;
            public byte RejectBackInputOnHigh;

            public byte DoorClosedInputSlot;
            public byte DoorClosedInputSignal;
            public byte DoorClosedInputOnHigh;

            public byte AlarmThermoInputSlot;
            public byte AlarmThermo2InputSlot;

            public byte AlarmThermoInputSignal;
            public byte AlarmThermo2InputSignal;

            public byte AlarmThermoInputOnHigh;
            public byte AlarmThermo2InputOnHigh;

            public byte AlarmFuseInputSlot;
            public byte AlarmFuseInputSignal;
            public byte AlarmFuseInputOnHigh;

            public byte EmergencyOffInputSlot;
            public byte EmergencyOffInputSignal;
            public byte EmergencyOffInputOnHigh;

            public byte EmergencyOffConvInputSlot;
            public byte EmergencyOffConvInputSignal;
            public byte EmergencyOffConvInputOnHigh;

            public byte EmergencyOffOutputSlot;
            public byte EmergencyOffOutputSignal;

            public byte EmergencyCircuitInputSlot;
            public byte EmergencyCircuitInputSignal;
            public byte EmergencyCircuitInputOnHigh;

            public byte NextMachineReadySlot;
            public byte NextMachineReadySignal;
            public byte NextMachineReadyOnHigh;

            public byte PreviousMachineReadyOutputSlot;
            public byte PreviousMachineReadyOutputSignal;

            public byte PrevTransNotReadyOutputSlot;
            public byte PrevTransNotReadyOutputSignal;

            public byte NextMachineStandbyOutputSlot;
            public byte NextMachineStandbyOutputSignal;

            public byte ReadyPowerOutputSlot;
            public byte ReadyPowerOutputSignal;
            public byte ReadyAirOutputSlot;
            public byte ReadyAirOutputSignal;
            public byte ReadyConveyorsOutputSlot;
            public byte ReadyConveyorsOutputSignal;
            public byte ReadyBeltsOutputSlot;
            public byte ReadyBeltsOutputSignal;

            public byte StartLastConvButtonLightOutputSlot;
            public byte StartLastConvButtonLightOutputSignal;
            public byte StopLastConvButtonLightOutputSlot;
            public byte StopLastConvButtonLightOutputSignal;
            public byte ButtonStartLastConvInputSlot;
            public byte ButtonStartLastConvInputSignal;
            public byte ButtonStartLastConvInputOnHigh;
            public byte ButtonStopLastConvInputSlot;
            public byte ButtonStopLastConvInputSignal;
            public byte ButtonStopLastConvInputOnHigh;

            public byte ArmDownOutputSlot;
            public byte ArmDownOutputSignal;
            public byte ArmUpOutputSlot;
            public byte ArmUpOutputSignal;
            public byte ArmBackOutputSlot;
            public byte ArmBackOutputSignal;

            public short ArmDownAfterLeaveDelay;
            public short ArmDownMintime;
            public short ArmBackTimeout;
            public short ArmDownTimeout;
            public short ArmUpTimeout;

            public byte RejectDownOutputSlot;
            public byte RejectDownOutputSignal;
            public byte RejectBackOutputSlot;
            public byte RejectBackOutputSignal;

            public short RejectDownTime;
            public short RejectUpTime;
            public short RejectBackTimeout;
            public short RejectFrontTimeout;
            public short RejectWaitForLabelTimeout;
            public short RejectWaitForLabelGoneTimeout;

            public byte StartButtonLightOutputSlot;
            public byte StartButtonLightOutputSignal;
            public byte StopButtonLightOutputSlot;
            public byte StopButtonLightOutputSignal;
            public byte ResetButtonLightOutputSlot;
            public byte ResetButtonLightOutputSignal;

            public byte StartConvButtonLightOutputSlot;
            public byte StartConvButtonLightOutputSignal;
            public byte StopConvButtonLightOutputSlot;
            public byte StopConvButtonLightOutputSignal;

            public byte GreenLightOutputSlot;
            public byte GreenLightOutputSignal;
            public byte YellowLightOutputSlot;
            public byte YellowLightOutputSignal;
            public byte RedLightOutputSlot;
            public byte RedLightOutputSignal;
            public byte HornOutputSlot;
            public byte HornOutputSignal;

            public byte Printer1ActivateInputSlot;
            public byte Printer2ActivateInputSlot;

            public byte Printer1ActivateInputSignal;
            public byte Printer2ActivateInputSignal;

            public byte Printer1ActivateInputOnHigh;
            public byte Printer2ActivateInputOnHigh;

            public byte Printer1ActiveOutputSlot;
            public byte Printer2ActiveOutputSlot;

            public byte Printer1ActiveOutputSignal;
            public byte Printer2ActiveOutputSignal;

            public byte Gate1DownInputSlot;
            public byte Gate1DownInputSignal;
            public byte Gate1DownInputOnHigh;

            public byte Gate2DownInputSlot;
            public byte Gate2DownInputSignal;
            public byte Gate2DownInputOnHigh;

            public byte Gate2DownConfirmedInputSlot;
            public byte Gate2DownConfirmedInputSignal;
            public byte Gate2DownConfirmedInputOnHigh;

            public short StartGreenOnTime;
            public short StartGreenOffTime;
            public short AlarmYellowOnTime;
            public short AlarmYellowOffTime;
            public short EmergencyRedOnTime;
            public short EmergencyRedOffTime;
            public short AlarmHornOnTime;
            public short AlarmHornOffTime;

            public byte BundlePositionCount;
            public byte LabelPositionCountPrinter1;
            public byte LabelPositionCountPrinter2;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = MAX_BUNDLE_POSITIONS)]
            public SetupPositionStruct[] BundlePositions;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = MAX_LABEL_POSITIONS)]
            public SetupPositionStruct[] LabelPositionsPrinter1;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = MAX_LABEL_POSITIONS)]
            public SetupPositionStruct[] LabelPositionsPrinter2;
        }

        #endregion

        #region Enums

        public enum DbCommandTypes
        {
            None,
            GetNextCommand,
            ExecuteNonQuery,
            GetSetup,
            GetItems
        }

        public enum PrinterControlStates
        {
            Unknown = 0,
            NotRunning = 1,
            Running = 2
        }

        public enum SnmpControlStates
        {
            Unknown = 0,
            NotRunning = 1,
            Running = 2,
            Disconnected = 3,
            Listening = 4
        }

        public enum IcpConnectionStates
        {
            Unknown = 0,
            NotRunning = 1,
            Disconnected = 2,
            Connecting = 3,
            Connected = 4
        }

        public enum DbConnectionStates
        {
            Unknown = 0,
            NotRunning = 1,
            Disconnected = 2,
            Connecting = 3,
            Connected = 4
        }

        public enum IcpTelegramTypes
        {
            RequestStatus,
            SendSetup,
            SendEjectedBundleIndex,
            SendPrintedBundleIndex,
            Reset,
            //SetDigitalOutput,
            SendStopApplication
        }

        public enum IcpSlotTypes
        {
            TSLOT_EMPTY = 0,
            TSLOT_DI16 = 1,
            TSLOT_DO16 = 2,
            TSLOT_DI8DO8 = 3,
            TSLOT_PULSE = 4,
            TSLOT_AIN = 5,
            TSLOT_AOUT = 6,
            TSLOT_RSCOM = 7
        }

        public enum TopSheetActionEnum
        {
            Unknown = 99,
            BundleLabeledApproved = 0,
            LabelRejectNotTracked = 1,
            BundleRejectNotTracked = 2,
            BundleRejectLessThanLabel = 3,
            LabelRejectLessThanBundle = 4,
            LabelAndBundleReject = 5,
            UnableToPrint = 11,
            BundleNotReportedPrintQueue = 12,
            Reset = 13,
            PrintQueueFullRemoved = 14,
            PrintedListFullRemoved = 15,
            BundleNotReportedPrintedList = 16,
            AddedBundleAlreadyExistsQueue = 17,
            AddedBundleAlreadyExistsPrinted = 18,
            AutomaticPrint = 50,
            AutomaticPrintRemovedFromQueue = 51
        }

        //public enum DBTrackingCommandEnum
        //{
        //    Alive = 0,
        //    EjectedBundle = 1,
        //    LabeledBundle = 2,
        //    BundleReleasedToStacker = 3,
        //    ErrorBundle = 9,
        //    ServerAlive = 10
        //}

        public enum TopSheetApplicatorStates
        {
            ApplicatorNotActive = 0,
            ApplicatorOffline = 1,
            ApplicatorReady = 2,
            ApplicatorWarning = 3,
            ApplicatorStopped = 4,
            ApplicatorError = 5,
            ApplicatorStandardBundles = 6,
            ApplicatorInitializing = 7,
            ApplicatorProductionFinished = 8,
            ApplicatorPaperError1 = 9,
            ApplicatorPaperError2 = 10,
            ApplicatorPaperError = 11,
            ApplicatorPaused = 12,
            ApplicatorPausedByOperator = 13,
            ApplicatorAlarmThermo = 14,
            ApplicatorAlarmArm = 15
        }

        public enum TopSheetPrinterStates
        {
            PrinterUnknown = 0,
            PrinterOffline = 1,
            PrinterInitializing = 2,
            PrinterReady = 3,
            PrinterPaperLow = 4,
            PrinterTonerLow = 5,
            PrinterWasteWarning = 6,
            PrinterError = 7,
            PrinterErrorPaper = 8,
            PrinterErrorToner = 9,
            PrinterErrorDoor = 10,
            PrinterPaused = 11,
            PrinterPausedByOperator = 12,
            PrinterAlarmThermo = 14
        }

        public enum NextMachineStates
        {
            Unknown = 0,
            Ready = 1,
            NotReady = 2
        }

        #endregion

        #region Static Variables

        public static TopSheetMain ST_snmpWatcher;
        public static List<TopSheetMain> ST_snmpWatcherTopSheetLines;
        public static readonly object ST_lock_snmpWatcherTopSheetLines = new object();
        public static Thread ST_snmpThread;
        public static Socket ST_snmpSocket;
        public static ushort ST_SNMPListeningPort;

        #endregion

        #region Variables

        private int _gripperLineTableId;
        private int _topSheetLineTableId;
        private int _currentStackerPositionId;
        private string _connectionString;
        private string _connectionStringLabelDesign;

        private IcpSlotTypes[] _slotTypes;

        private string _logFolder;
        private bool _enableLogFile;
        private bool _enableLogDebug;
        private List<LogItem> _logItemList;
        private readonly object _lock_logItemList = new object();

        private Thread _icpThread;
        private Thread _dbThread;
        private Thread _printerThread;
        private Thread _logThread;

        //private DateTime _icpLastGetThreadId;
        //private DateTime _dbLastGetThreadId;
        //private DateTime _printerLastGetThreadId;
        //private DateTime _logLastGetThreadId;

        //private int _icpSystemThreadId;
        //private int _dbSystemThreadId;
        //private int _printerSystemThreadId;
        //private int _logSystemThreadId;

        private readonly object _lockDbCommandList = new object();
        private readonly object _lockTelegramListIcp = new object();
        private readonly object _lockTelegram = new object();
        private OleDbConnection _con;
        private TcpClient _tcpClientIcp;
        private NetworkStream _networkStreamIcp;
        private BinaryReader _binaryReaderIcp;
        private BinaryWriter _binaryWriterIcp;
        private List<byte[]> _telegramListIcp;
        private List<DbCommandStuct> _dbCommandList;
        private byte _pcStatus;
        private byte _telegramId;
        private byte[] _telegramRequestStatus;
        private string _ioIp;
        private int _ioPort;
        private string _printerName1;
        private string _printerName2;
        private string _printerIp1;
        private string _printerIp2;
        private int _printerPort1;
        private int _printerPort2;
        private bool _doReconnectIcp;
        private bool _doReconnectDb;
        private bool _doReconnectPrinter;
        private bool _setupReceived;

        private SetupStruct _setup;
        private int _setupStructSize;
        private StatusStruct _status;
        private int _statusStructSize;

        private ushort _nextBundleIndex;
        private List<TopSheetItem> _topSheetQueue;
        private readonly object _lock_topSheetQueue = new object();
        private List<TopSheetItem> _topSheetPrinted;
        private readonly object _lock_topSheetPrinted = new object();
        private UniDevLib.Gui.UserControls.UniLabel.PrinterVer2 _printer1;
        private UniDevLib.Gui.UserControls.UniLabel.PrinterVer2 _printer2;
        private DateTime _lastLCCMessage;
        private DateTime _lastPrint1;
        private DateTime _lastPrint2;
        private bool _useTwoPrinters;
        private bool _resetDone;

        private DateTime _nextMachineReadyTime;
        private DateTime _stackerExitReadyTime;
        private DateTime _topSheetLastOnline;
        //private bool _isWaitingToSendPrintToIcp;

        private bool _printQueueUpdated1;
        private bool _printQueueUpdated2;

        #endregion

        #region Constructor
        public TopSheetMain(string connectionString, string connectionStringLabelDesign,
            int topSheetLineTableId, string ioIp, int ioPort,
            bool useTwoPrinters, string printerName1, string printerName2,
            string printerIp1, string printerIp2, int printerPort1, int printerPort2,
            ushort snmpListeningPort, IcpSlotTypes[] slotTypes)
        {
            ST_SNMPListeningPort = snmpListeningPort;
            _logItemList = new List<LogItem>();

            _slotTypes = slotTypes;

            if (snmpListeningPort > 0)
            {
                if (ST_snmpWatcher == null)
                {
                    ST_snmpWatcher = this;
                }

                lock (ST_lock_snmpWatcherTopSheetLines)
                {
                    if (ST_snmpWatcherTopSheetLines == null)
                    {
                        ST_snmpWatcherTopSheetLines = new List<TopSheetMain>();
                    }

                    ST_snmpWatcherTopSheetLines.Add(this);
                }
            }

            //LogList = new List<string>();
            string exeName = System.Reflection.Assembly.GetExecutingAssembly().GetModules()[0].FullyQualifiedName;
            string exePath = System.IO.Path.GetDirectoryName(exeName);
            _logFolder = exePath + "\\Log_Topsheet_" + topSheetLineTableId.ToString();

            _connectionString = connectionString;
            _connectionStringLabelDesign = connectionStringLabelDesign;
            _topSheetLineTableId = topSheetLineTableId;

            //_lockDbCommandList = new object();
            //_lockTelegramListIcp = new object();
            //_lockTelegram = new object();
            _con = null;
            IsStarted = false;
            IcpConnectionState = IcpConnectionStates.NotRunning;
            DbConnectionState = DbConnectionStates.NotRunning;
            _telegramListIcp = new List<byte[]>();
            _dbCommandList = new List<DbCommandStuct>();
            _telegramRequestStatus = null;
            _pcStatus = 3;//no label
            _ioIp = ioIp;
            _ioPort = ioPort;
            _printerName1 = printerName1;
            _printerName2 = printerName2;
            _printerIp1 = printerIp1;
            _printerIp2 = printerIp2;
            _printerPort1 = printerPort1;
            _printerPort2 = printerPort2;
            _doReconnectIcp = false;
            _setupReceived = false;

            _setup = new SetupStruct();
            _setup.BundlePositions = new SetupPositionStruct[MAX_BUNDLE_POSITIONS];
            _setup.LabelPositionsPrinter1 = new SetupPositionStruct[MAX_LABEL_POSITIONS];
            _setup.LabelPositionsPrinter2 = new SetupPositionStruct[MAX_LABEL_POSITIONS];
            _status = new StatusStruct();

            _setupStructSize = Marshal.SizeOf(typeof(SetupStruct));
            _statusStructSize = Marshal.SizeOf(typeof(StatusStruct));

            _topSheetQueue = new List<TopSheetItem>();
            _topSheetPrinted = new List<TopSheetItem>();
            _nextBundleIndex = 11;

            _useTwoPrinters = useTwoPrinters;

            _printer1 = new UniDevLib.Gui.UserControls.UniLabel.PrinterVer2(
                UniDevLib.Gui.UserControls.UniLabel.PrinterVer2.PrinterTypes.WindowsPrinter,
                printerName1, printerIp1, printerPort1);
            _printer1.PrinterStatus.InitializeSeconds = 5;

            if (_useTwoPrinters)
            {
                _printer2 = new UniDevLib.Gui.UserControls.UniLabel.PrinterVer2(
                    UniDevLib.Gui.UserControls.UniLabel.PrinterVer2.PrinterTypes.WindowsPrinter,
                    printerName2, printerIp2, printerPort2);
                _printer2.PrinterStatus.InitializeSeconds = 5;
            }

            _lastPrint1 = DateTime.UtcNow;
            _lastPrint2 = DateTime.UtcNow;
            _lastLCCMessage = DateTime.UtcNow;
            _nextMachineReadyTime = DateTime.UtcNow;
            _stackerExitReadyTime = DateTime.UtcNow;
            //_isWaitingToSendPrintToIcp = false;
            RequestStackerReset = false;
        }
        #endregion

        #region Properties

        public bool IsStarted { get; set; }
        public IcpConnectionStates IcpConnectionState { get; set; }
        public DbConnectionStates DbConnectionState { get; set; }
        public PrinterControlStates PrinterControlState { get; set; }
        public SnmpControlStates SnmpControlState { get; set; }

        public string ThreadInfoIcpControl
        {
            get
            {
                string result = "";// GetThreadInfo(_icpThread, _icpSystemThreadId);
                return result;
            }
        }

        public string ThreadInfoDbControl
        {
            get
            {
                string result = "";// GetThreadInfo(_dbThread, _dbSystemThreadId);
                return result;
            }
        }

        public string ThreadInfoPrinterControl
        {
            get
            {
                string result = "";// GetThreadInfo(_printerThread, _printerSystemThreadId);
                return result;
            }
        }

        public string ThreadInfoLogControl
        {
            get
            {
                string result = "";// GetThreadInfo(_logThread, _logSystemThreadId);
                return result;
            }
        }

        public string ThreadInfoPrinterStatusControl1
        {
            get
            {
                string result = "";

                UniDevLib.Gui.UserControls.UniLabel.PrinterVer2 printer = _printer1;

                if (printer != null)
                {
                    result = printer.ThreadInfoPrinterStatusControl;
                }
                else
                {
                    result = "Not in use";
                }

                return result;
            }
        }

        public string ThreadInfoPrinterStatusControl2
        {
            get
            {
                string result = "";

                if (UseTwoPrinters)
                {
                    UniDevLib.Gui.UserControls.UniLabel.PrinterVer2 printer = _printer2;

                    if (printer != null)
                    {
                        result = printer.ThreadInfoPrinterStatusControl;
                    }
                    else
                    {
                        result = "Not in use";
                    }
                }
                else
                {
                    result = "Not in use";
                }

                return result;
            }
        }

        public ushort LastEjectedBundleId { get; set; }

        public string PrinterIp1
        {
            get
            {
                string result = _printerIp1;
                return result;
            }
        }

        public string PrinterIp2
        {
            get
            {
                string result = _printerIp2;
                return result;
            }
        }

        public UniDevLib.Gui.UserControls.UniLabel.PrinterVer2 Printer1
        {
            get
            {
                UniDevLib.Gui.UserControls.UniLabel.PrinterVer2 result = _printer1;
                return result;
            }
        }

        public UniDevLib.Gui.UserControls.UniLabel.PrinterVer2 Printer2
        {
            get
            {
                UniDevLib.Gui.UserControls.UniLabel.PrinterVer2 result = _printer2;
                return result;
            }
        }

        public bool IsSnmpWatcher
        {
            get
            {
                bool result = this == ST_snmpWatcher;
                return result;
            }
        }

        public int TopSheetLineTableId
        {
            get
            {
                int result = _topSheetLineTableId;
                return result;
            }
        }

        public bool UseTwoPrinters
        {
            get
            {
                bool result = _useTwoPrinters;
                return result;
            }
        }

        public byte PcStatus
        {
            get { return _pcStatus; }
            set { _pcStatus = value; }
        }

        public byte PcStatus_Printer1
        {
            get
            {
                bool isPausedByOperator;
                byte result = GetPcStatusPrinter(1, out isPausedByOperator);
                return result;
            }
        }

        public byte PcStatus_Printer2
        {
            get
            {
                bool isPausedByOperator;
                byte result = GetPcStatusPrinter(2, out isPausedByOperator);
                return result;
            }
        }

        public bool IsPausedByOperator1
        {
            get
            {
                bool isPausedByOperator;
                GetPcStatusPrinter(1, out isPausedByOperator);
                return isPausedByOperator;
            }
        }

        public bool IsPausedByOperator2
        {
            get
            {
                bool isPausedByOperator;
                GetPcStatusPrinter(2, out isPausedByOperator);
                return isPausedByOperator;
            }
        }

        public bool IsPaused1 { get; set; }

        public bool IsPaused2 { get; set; }

        public bool ArmAlarm
        {
            get
            {
                bool result = false;

                if (IcpConnectionState == IcpConnectionStates.Connected)
                {
                    if (_status.StartStopState == 0)
                    {
                        if (_status.ApplicationState > 0)
                        {
                            result = true;
                        }
                        else
                        {
                            byte inputSlot = _setup.ArmUpInputSlot;
                            byte inputSignal = _setup.ArmUpInputSignal;
                            byte inputOnHigh = _setup.ArmUpInputOnHigh;
                            int signalValue = GetDigitalInputState(inputSlot, inputSignal);
                            if (signalValue >= 0 && signalValue != inputOnHigh)//arm not in upper position
                            {
                                result = true;
                            }
                        }
                    }
                }

                return result;
            }
        }

        public bool ThermoAlarm1
        {
            get
            {
                bool result = false;

                if (IcpConnectionState == IcpConnectionStates.Connected)
                {
                    if (_status.AlarmThermoState1 > 0)
                    {
                        result = true;
                    }
                }

                return result;
            }
        }

        public bool ThermoAlarm2
        {
            get
            {
                bool result = false;

                if (IcpConnectionState == IcpConnectionStates.Connected)
                {
                    if (_status.AlarmThermoState2 > 0)
                    {
                        result = true;
                    }
                }

                return result;
            }
        }

        public bool PaperJam1
        {
            get
            {
                bool result = false;

                if (IcpConnectionState == IcpConnectionStates.Connected)
                {
                    if (_status.LabelPositionsPrinter1 != null)
                    {
                        if (_status.LabelPositionsPrinter1.Length ==
                            _setup.LabelPositionCountPrinter1)
                        {
                            int index = -1;
                            while (!result && ++index < _setup.LabelPositionCountPrinter1)
                            {
                                if (_status.LabelPositionsPrinter1[index].State == 7)
                                {
                                    result = true;
                                }
                            }
                        }
                    }
                }

                return result;
            }
        }

        public bool PaperJam2
        {
            get
            {
                bool result = false;

                if (IcpConnectionState == IcpConnectionStates.Connected)
                {
                    if (_status.LabelPositionsPrinter2 != null)
                    {
                        if (_status.LabelPositionsPrinter2.Length ==
                            _setup.LabelPositionCountPrinter2)
                        {
                            int index = -1;
                            while (!result && ++index < _setup.LabelPositionCountPrinter2)
                            {
                                if (_status.LabelPositionsPrinter2[index].State == 7)
                                {
                                    result = true;
                                }
                            }
                        }
                    }
                }

                return result;
            }
        }

        public bool WaitTransportLabelUntilBundleArrived1 { get; set; }
        public bool WaitTransportLabelUntilBundleArrived2 { get; set; }

        public TopSheetApplicatorStates TopSheetApplicatorState
        {
            get
            {
                TopSheetApplicatorStates result = TopSheetApplicatorStates.ApplicatorNotActive;

                if (IsStarted)
                {
                    if (IcpConnectionState != IcpConnectionStates.Connected)
                    {
                        result = TopSheetApplicatorStates.ApplicatorOffline;
                    }
                    else
                    {
                        if (ArmAlarm)
                        {
                            result = TopSheetApplicatorStates.ApplicatorAlarmArm;
                        }
                        else if (PaperJam1 && PaperJam2)
                        {
                            result = TopSheetApplicatorStates.ApplicatorPaperError;
                        }
                        else if (ThermoAlarm1 && ThermoAlarm2)
                        {
                            result = TopSheetApplicatorStates.ApplicatorAlarmThermo;
                        }
                        else if (IsPaused1 && (!UseTwoPrinters || IsPaused2))
                        {
                            result = TopSheetApplicatorStates.ApplicatorPaused;
                        }
                        else if (IsPausedByOperator1 && (!UseTwoPrinters || IsPausedByOperator2))
                        {
                            result = TopSheetApplicatorStates.ApplicatorPausedByOperator;
                        }
                        else if (PaperJam1)
                        {
                            result = TopSheetApplicatorStates.ApplicatorPaperError1;
                        }
                        else if (PaperJam2)
                        {
                            result = TopSheetApplicatorStates.ApplicatorPaperError2;
                        }
                        else if (_status.StartStopState == 1)
                        {
                            if (_pcStatus == 3)
                            {
                                result = TopSheetApplicatorStates.ApplicatorStandardBundles;
                            }
                            else
                            {
                                result = TopSheetApplicatorStates.ApplicatorReady;
                            }
                        }
                        else if (_status.StartStopState == 0)
                        {
                            result = TopSheetApplicatorStates.ApplicatorStopped;
                        }
                        else
                        {
                            result = TopSheetApplicatorStates.ApplicatorInitializing;
                        }
                    }
                }

                return result;
            }
        }

        public string LabelsOnApplicator1
        {
            get
            {
                string result = GetLabelsOnApplicator(1);
                return result;
            }
        }

        public string LabelsOnApplicator2
        {
            get
            {
                string result = GetLabelsOnApplicator(2);
                return result;
            }
        }

        public bool ReadyForTestPrint1
        {
            get
            {
                bool result = CheckIfReadyForTestPrint(1);
                return result;
            }
        }

        public bool ReadyForTestPrint2
        {
            get
            {
                bool result = CheckIfReadyForTestPrint(2);
                return result;
            }
        }

        public TopSheetPrinterStates TopSheetPrinterState1
        {
            get
            {
                TopSheetPrinterStates result = GetPrinterState(1);
                return result;
            }
        }

        public TopSheetPrinterStates TopSheetPrinterState2
        {
            get
            {
                TopSheetPrinterStates result = GetPrinterState(2);
                return result;
            }
        }

        public NextMachineStates NextMachineState
        {
            get
            {
                NextMachineStates result = NextMachineStates.Unknown;

                if (IcpConnectionState == IcpConnectionStates.Connected)
                {
                    if (_status.NextMachineReadyState == 1)
                    {
                        //_nextMachineReadyTime = DateTime.UtcNow;
                        result = NextMachineStates.Ready;
                    }
                    else// if (_status.NextMachineReadyState == 0)
                    {
                        if (DateTime.UtcNow > _nextMachineReadyTime.AddSeconds(10))
                        {
                            result = NextMachineStates.NotReady;
                        }
                        else
                        {
                            result = NextMachineStates.Ready;
                        }
                    }
                }

                return result;
            }
        }

        public StatusStruct Status
        {
            get { return _status; }
        }

        public SetupStruct Setup
        {
            get { return _setup; }
        }

        public int PrinterQueueCount1
        {
            get
            {
                return _printer1 != null && _printer1.PrinterStatus != null ?
                    _printer1.PrinterStatus.PrintQueueCount : 0;
            }
        }

        public int PrinterQueueCount2
        {
            get
            {
                return _printer2 != null && _printer2.PrinterStatus != null ?
                    _printer2.PrinterStatus.PrintQueueCount : 0;
            }
        }

        public bool BundleAtStackerExit
        {
            get
            {
                bool result = false;

                if (IcpConnectionState == IcpConnectionStates.Connected)
                {
                    //if (_status.FunctionInputState1 > 0)
                    if (_status.StackerExitState > 0)
                    {
                        result = true;
                    }
                }

                return result;
            }
        }

        public bool ReadyForProduction
        {
            get
            {
                bool result = false;

                if (IcpConnectionState == IcpConnectionStates.Connected)
                {
                    if (_status.StartStopState == 1)
                    {
                        if (_setup.BundlePositionCount > 0)
                        {
                            if ((_printer1 != null && _printer1.PrinterStatus != null &&
                                _printer1.PrinterStatus.IsReadyToPrint && !PaperJam1 && !ThermoAlarm1 &&
                                !IsPaused1 && !IsPausedByOperator1)
                                ||
                                (_useTwoPrinters && _printer2 != null && _printer2.PrinterStatus != null &&
                                _printer2.PrinterStatus.IsReadyToPrint && !PaperJam2 && !ThermoAlarm2 &&
                                !IsPaused2 && !IsPausedByOperator2)
                                )
                            {
                                //if (_status.ConveyorPositions[0].ReadyToReceive == 1 ||
                                //    _status.ConveyorPositions[1].ReadyToReceive == 1 ||
                                //    _status.ConveyorPositions[2].ReadyToReceive == 1 ||
                                //    _status.ConveyorPositions[3].ReadyToReceive == 1)
                                {
                                    result = true;
                                }
                            }
                        }
                    }
                }

                return result;
            }

        }

        public bool ReadyToReceive
        {
            get
            {
                bool result = false;

                if (IcpConnectionState == IcpConnectionStates.Connected)
                {
                    if (DateTime.UtcNow < _stackerExitReadyTime.AddMilliseconds(1000))
                    {
                        result = true;
                    }
                    
                    /*
                    if (_status.StartStopState == 1)
                    {
                        if (_setup.BundlePositionCount > 0)
                        {
                            if ((_printer1 != null && _printer1.PrinterStatus != null &&
                                _printer1.PrinterStatus.IsReadyToPrint)
                                ||
                                (_useTwoPrinters && _printer2 != null && _printer2.PrinterStatus != null &&
                                _printer2.PrinterStatus.IsReadyToPrint)
                                )
                            {
                                if (_status.ConveyorPositions[0].ReadyToReceive == 1 ||
                                    _status.ConveyorPositions[1].ReadyToReceive == 1 ||
                                    _status.ConveyorPositions[2].ReadyToReceive == 1 ||
                                    _status.ConveyorPositions[3].ReadyToReceive == 1)
                                {
                                    result = true;
                                }
                            }
                        }
                    }
                    */
                }

                return result;
            }
        }

        public int TopSheetQueue1
        {
            get
            {
                return _status.LabelQueueCountPrinter1;
            }
        }
        public int TopSheetQueue2
        {
            get
            {
                return _status.LabelQueueCountPrinter2;
            }
        }

        //public int TopSheetCountSent
        //{
        //    get
        //    {
        //        int result = _isWaitingToSendPrintToIcp ? 1 : 0;

        //        return result;
        //    }
        //}

        public int PrintQueueWaiting
        {
            get
            {
                int count = 0;

                lock (_lock_topSheetQueue)
                {
                    for (int i = 0; i < _topSheetQueue.Count; i++)
                    {
                        if (_topSheetQueue[i].PrintedTime == DateTime.MinValue)
                        {
                            ++count;
                        }
                    }
                }

                return count;
            }
        }

        public int CurrentStackerPositionId
        {
            get { return _currentStackerPositionId; }
        }

        public bool RequestStackerReset { get; set; }

        #endregion

        #region IDisposable

        // Dispose() calls Dispose(true)
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        // NOTE: Leave out the finalizer altogether if this class doesn't 
        // own unmanaged resources itself, but leave the other methods
        // exactly as they are. 
        ~TopSheetMain()
        {
            // Finalizer calls Dispose(false)
            Dispose(false);
        }
        // The bulk of the clean-up code is implemented in Dispose(bool)
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
                if (_binaryReaderIcp != null)
                {
                    try { _binaryReaderIcp.Dispose(); } catch { }
                }

                if (_binaryWriterIcp != null)
                {
                    try { _binaryWriterIcp.Dispose(); } catch { }
                }

                if (_con != null)
                {
                    try { _con.Dispose(); } catch { }
                }

                if (_tcpClientIcp != null)
                {
                    try { _tcpClientIcp.Close(); } catch { }
                }
            }
            // free native resources if there are any.
        }

        #endregion

        #region Public methods

        public void Start(int lineId, int currentStackerPositionId)
        {
            if (IsStarted)
            {
                Stop();
            }

            IcpConnectionState = IcpConnectionStates.NotRunning;
            DbConnectionState = DbConnectionStates.NotRunning;
            PrinterControlState = PrinterControlStates.NotRunning;
            SnmpControlState = SnmpControlStates.NotRunning;

            _gripperLineTableId = lineId;
            //_topSheetLineTableId = lineId;

            //_ioIp = null;
            //_ioPort = 0;
            _doReconnectIcp = false;
            _setupReceived = false;
            _currentStackerPositionId = currentStackerPositionId;

            _resetDone = false;
            ResetApplicator(true);
            _resetDone = false;

            //_topSheetQueue.Clear();
            //_topSheetPrinted.Clear();

            IsStarted = true;

            _icpThread = new Thread(new ThreadStart(IcpControl));
            _icpThread.Start();

            _dbThread = new Thread(new ThreadStart(DbControl));
            _dbThread.Start();

            _printerThread = new Thread(new ThreadStart(PrinterControl));
            _printerThread.Start();

            _logThread = new Thread(new ThreadStart(LogControl));
            _logThread.Start();
        }

        public void SetStackerPosition(int currentStackerPositionId)
        {
            if (currentStackerPositionId != _currentStackerPositionId || currentStackerPositionId != _setup.CurrentStackerPosition)
            {
                _currentStackerPositionId = currentStackerPositionId;
                //_setup.CurrentStackerPosition = (byte)_currentStackerPositionId;

                DbGetSetup();
                DbGetItems();
                //_setupReceived = false;
                //_doReconnectIcp = true;
            }
        }

        public void Stop()
        {
            IsStarted = false;

            int counter = 0;
            while (++counter < 20 &&
                ((IcpConnectionState != IcpConnectionStates.NotRunning && IcpConnectionState != IcpConnectionStates.Unknown) ||
                (DbConnectionState != DbConnectionStates.NotRunning && DbConnectionState != DbConnectionStates.Unknown) ||
                (PrinterControlState != PrinterControlStates.NotRunning && PrinterControlState != PrinterControlStates.Unknown) ||
                (SnmpControlState != SnmpControlStates.NotRunning && SnmpControlState != SnmpControlStates.Unknown)))
            {
                Thread.Sleep(100);
            }

            if (IcpConnectionState != IcpConnectionStates.NotRunning)
            {
                IcpDisconnect();

                try
                {
                    if (_icpThread != null)// && _icpThread.ThreadState == ThreadState.Running)
                    {
                        _icpThread.Interrupt();
                    }
                }
                catch
                {
                }
            }

            if (DbConnectionState != DbConnectionStates.NotRunning)
            {
                DbDisconnect();

                try
                {
                    if (_dbThread != null)// && _dbThread.ThreadState == ThreadState.Running)
                    {
                        _dbThread.Interrupt();
                    }
                }
                catch
                {
                }
            }

            if (SnmpControlState != SnmpControlStates.NotRunning)
            {
                SnmpDisconnect();

                try
                {
                    if (ST_snmpThread != null)// && _dbThread.ThreadState == ThreadState.Running)
                    {
                        ST_snmpThread.Interrupt();
                    }
                }
                catch
                {
                }
            }

            //counter = 0;
            //while (++counter < 20 &&
            //    (IcpConnectionState != IcpConnectionStates.NotRunning ||
            //    DbConnectionState != DbConnectionStates.NotRunning))
            //{
            //    Thread.Sleep(100);
            //}

            //if (IcpConnectionState != IcpConnectionStates.NotRunning)
            //{
            //    AddToLog(LogLevels.Debug, "Unable to close ICP communication - aborting thread");
            //    try
            //    {
            //        if (_icpThread != null)// && _icpThread.ThreadState == ThreadState.Running)
            //        {
            //            _icpThread.Abort();
            //        }
            //    }
            //    catch
            //    {
            //    }
            //}

            //if (DbConnectionState != DbConnectionStates.NotRunning)
            //{
            //    AddToLog(LogLevels.Debug, "Unable to close DB communication - aborting thread");
            //    try
            //    {
            //        if (_dbThread != null)// && _dbThread.ThreadState == ThreadState.Running)
            //        {
            //            _dbThread.Abort();
            //        }
            //    }
            //    catch
            //    {
            //    }
            //}
        }

        public void StartProduction()
        {
            PcStatus = 2;
            DbGetSetup();
            DbGetItems();
            ResetApplicator(true);
        }

        public void StopProduction()
        {
            PcStatus = 3;
            ResetApplicator(true);
        }


        public bool PrintBundle(ushort bundleId, out ushort bundleIndex)
        {
            bool result;

            bundleIndex = _nextBundleIndex++;
            if (bundleIndex <= 10)
            {
                AddToLog(LogLevels.Warning, "Bundle index out of range - reset");
                ResetApplicator(true);
                _nextBundleIndex = 11;
            }

            result = PrintBundle(bundleId, bundleIndex);

            return result;
        }

        private bool PrintBundleRemovedFromQueue(ushort bundleId)
        {
            bool result = false;

            int printerNumber = 0;
            ushort bundleIndex = 1;
            bool isReady1 = _printer1 != null && _printer1.PrinterStatus != null && _printer1.PrinterStatus.IsReadyToPrint;
            bool isReady2 = UseTwoPrinters && _printer2 != null && _printer2.PrinterStatus != null && _printer2.PrinterStatus.IsReadyToPrint;
            if (isReady1)
            {
                printerNumber = 1;
            }
            else if (isReady2)
            {
                printerNumber = 2;
            }

            if (printerNumber > 0)
            {
                string documentName = "TopSheet(" + bundleIndex.ToString()
                    + ") " + bundleId.ToString();

                UniDevLib.Gui.UserControls.UniLabel.ResultTypes printResult =
                    PrintTopSheet(printerNumber, documentName, bundleId);

                if (printResult == UniDevLib.Gui.UserControls.UniLabel.ResultTypes.Ok)
                {
                    result = true;

                    string sql = string.Format("exec UniStack_TopSheetBundleAction {0}, {1}, {2}, {3}, {4}, {5}, {6}"
                        , _gripperLineTableId
                        , _topSheetLineTableId
                        , bundleId
                        , bundleIndex
                        , (int)TopSheetActionEnum.AutomaticPrintRemovedFromQueue
                        , -1
                        , printerNumber - 1
                        );

                    DbAddCommandToQueue(DbCommandTypes.ExecuteNonQuery, sql, null, true);

                    //AddToLogList("PrinterControl print ok bundleid: "
                    //    + topSheetItem.BundleId.ToString()
                    //    + " - bundleIndex: " + topSheetItem.BundleIndex.ToString());

                }
                else
                {
                    string sql = string.Format("exec UniStack_TopSheetBundleAction {0}, {1}, {2}, {3}, {4}, {5}, {6}"
                        , _gripperLineTableId
                        , _topSheetLineTableId
                        , bundleId
                        , bundleIndex
                        , (int)TopSheetActionEnum.UnableToPrint
                        , -1
                        , printerNumber - 1
                        );

                    DbAddCommandToQueue(DbCommandTypes.ExecuteNonQuery, sql, null, true);


                    //AddToLogList("ERROR: PrinterControl print failed bundleid: "
                    //    + topSheetItem.BundleId.ToString()
                    //    + " - bundleIndex: " + topSheetItem.BundleIndex.ToString());
                }

            }

            return result;
        }

        public bool PrintErrorBundle(ushort bundleId)
        {
            bool result;

            ushort bundleIndex = 1;
            result = PrintBundle(bundleId, bundleIndex);

            return result;
        }

        private bool PrintBundle(ushort bundleId, ushort bundleIndex)
        {
            bool result = false;
            UniDevLib.StaticInfo.GlobalTick = Environment.TickCount;
            _lastLCCMessage = DateTime.UtcNow;

            AddToLog(LogLevels.Normal, "PrintBundle - bundleId " + bundleId.ToString()
                + " bundleIndex " + bundleIndex.ToString());

            //AddToLogList("PrintBundle called - bundleid: " + bundleId.ToString());
            try
            {
                TopSheetItem queueItem = new TopSheetItem();
                queueItem.AddedTime = DateTime.UtcNow;
                queueItem.BundleId = bundleId;
                queueItem.BundleIndex = bundleIndex;
                queueItem.EjectedTime = DateTime.MinValue;
                queueItem.HandledCode = -1;
                queueItem.PrintedTime = DateTime.MinValue;
                queueItem.PrinterNumber = 1;

                lock (_lock_topSheetQueue)
                {
                    for (int i = _topSheetQueue.Count - 1; i >= 0; i--)
                    {
                        if (_topSheetQueue[i].BundleId == bundleId)
                        {
                            string sql = string.Format("exec UniStack_TopSheetBundleAction {0}, {1}, {2}, {3}, {4}, {5}, {6}"
                                , _gripperLineTableId
                                , _topSheetLineTableId
                                , _topSheetQueue[i].BundleId
                                , _topSheetQueue[i].BundleIndex
                                , (int)TopSheetActionEnum.AddedBundleAlreadyExistsQueue
                                , -1
                                , -1
                                );

                            DbAddCommandToQueue(DbCommandTypes.ExecuteNonQuery, sql, null, true);

                            //AddToLogList("ERROR: PrintBundle remove existing from _topSheetQueue - bundleid: "
                            //    + _topSheetQueue[i].BundleId.ToString());

                            _topSheetQueue.RemoveAt(i);
                        }
                    }
                }

                lock (_lock_topSheetPrinted)
                {
                    for (int i = _topSheetPrinted.Count - 1; i >= 0; i--)
                    {
                        if (_topSheetPrinted[i].BundleId == bundleId)
                        {
                            string sql = string.Format("exec UniStack_TopSheetBundleAction {0}, {1}, {2}, {3}, {4}, {5}, {6}"
                                , _gripperLineTableId
                                , _topSheetLineTableId
                                , _topSheetPrinted[i].BundleId
                                , _topSheetPrinted[i].BundleIndex
                                , (int)TopSheetActionEnum.AddedBundleAlreadyExistsPrinted
                                , -1
                                , -1
                                );

                            DbAddCommandToQueue(DbCommandTypes.ExecuteNonQuery, sql, null, true);

                            //AddToLogList("ERROR: PrintBundle remove existing from _topSheetPrinted - bundleid: "
                            //    + _topSheetPrinted[i].BundleId.ToString());

                            _topSheetPrinted.RemoveAt(i);
                        }
                    }
                }

                lock (_lock_topSheetQueue)
                {
                    _topSheetQueue.Add(queueItem);
                    //AddToLogList("PrintBundle add to _topSheetQueue - bundleid: " + bundleId.ToString());

                    while (_topSheetQueue.Count > 30)
                    {
                        queueItem = _topSheetQueue[0];

                        string sql = string.Format("exec UniStack_TopSheetBundleAction {0}, {1}, {2}, {3}, {4}, {5}, {6}"
                            , _gripperLineTableId
                            , _topSheetLineTableId
                            , queueItem.BundleId
                            , queueItem.BundleIndex
                            , (int)TopSheetActionEnum.PrintQueueFullRemoved
                            , -1
                            , -1
                            );

                        DbAddCommandToQueue(DbCommandTypes.ExecuteNonQuery, sql, null, true);

                        //AddToLogList("ERROR: PrintBundle remove from _topSheetQueue - bundleid: " + _topSheetQueue[0].BundleId.ToString());

                        _topSheetQueue.RemoveAt(0);
                        PrintBundleRemovedFromQueue(queueItem.BundleId);
                    }
                }

                lock (_lock_topSheetPrinted)
                { 
                    while (_topSheetPrinted.Count > 30)
                    {
                        queueItem = _topSheetPrinted[0];

                        string sql = string.Format("exec UniStack_TopSheetBundleAction {0}, {1}, {2}, {3}, {4}, {5}, {6}"
                            , _gripperLineTableId
                            , _topSheetLineTableId
                            , queueItem.BundleId
                            , queueItem.BundleIndex
                            , (int)TopSheetActionEnum.PrintedListFullRemoved
                            , -1
                            , queueItem.PrinterNumber - 1
                            );

                        DbAddCommandToQueue(DbCommandTypes.ExecuteNonQuery, sql, null, true);

                        //AddToLogList("ERROR: PrintBundle remove from _topSheetPrinted - bundleid: " + _topSheetQueue[0].BundleId.ToString());

                        _topSheetPrinted.RemoveAt(0);
                        //PrintBundleRemovedFromQueue(queueItem.BundleId);
                    }
                }
            }
            catch (Exception ex)
            {
                AddToLog(ex);
            }

            return result;
        }

        public bool BundleEjectedDoNotTrack(ushort bundleId)
        {
            bool result = false;

            if (bundleId >= 10000)
            {
                LastEjectedBundleId = bundleId;

                TopSheetItem queueItem = new TopSheetItem();
                queueItem.AddedTime = DateTime.UtcNow;
                queueItem.BundleId = bundleId;
                queueItem.BundleIndex = 1;
                queueItem.EjectedTime = DateTime.UtcNow;
                queueItem.HandledCode = -1;
                queueItem.PrintedTime = DateTime.MinValue;
                queueItem.PrinterNumber = 0;

                IcpGetOrAddTelegram(IcpTelegramTypes.SendEjectedBundleIndex, queueItem, true);

                result = true;
            }

            return result;
        }

        public bool BundleEjected(ushort bundleId, out ushort bundleIndex)
        {
            bool result = false;
            //AddToLogList("BundleEjected called - bundleid: " + bundleId.ToString());
            bundleIndex = 0;
            _lastLCCMessage = DateTime.UtcNow;

            try
            {
                bool isFound = false;
                int index = -1;

                LastEjectedBundleId = bundleId;

                lock (_lock_topSheetPrinted)
                {
                    while (!isFound && ++index < _topSheetPrinted.Count)
                    {
                        TopSheetItem queueItem = _topSheetPrinted[index];
                        if (queueItem.BundleId == bundleId && queueItem.EjectedTime == DateTime.MinValue)
                        {
                            isFound = true;
                            queueItem.EjectedTime = DateTime.UtcNow;
                            IcpGetOrAddTelegram(IcpTelegramTypes.SendEjectedBundleIndex, queueItem, true);
                            bundleIndex = queueItem.BundleIndex;
                            result = true;

                            //AddToLogList("BundleEjected bundle found printed - bundleid: " + bundleId.ToString()
                            //    + " - bundleIndex: " + queueItem.BundleIndex.ToString());

                            //_topSheetPrinted.RemoveAt(index);
                        }
                    }
                }

                if (!isFound)
                {
                    index = -1;
                    lock (_lock_topSheetQueue)
                    {
                        while (!isFound && ++index < _topSheetQueue.Count)
                        {
                            TopSheetItem queueItem = _topSheetQueue[index];
                            if (queueItem.BundleId == bundleId && queueItem.EjectedTime == DateTime.MinValue)
                            {
                                isFound = true;
                                queueItem.EjectedTime = DateTime.UtcNow;
                                IcpGetOrAddTelegram(IcpTelegramTypes.SendEjectedBundleIndex, queueItem, true);
                                bundleIndex = queueItem.BundleIndex;
                                result = true;
                                //AddToLogList("BundleEjected bundle found not printed - bundleid: " + bundleId.ToString()
                                //    + " - bundleIndex: " + queueItem.BundleIndex.ToString());

                                //_topSheetPrinted.RemoveAt(index);
                            }
                        }
                    }
                }

                //if (!isFound)
                //{
                //    AddToLogList("ERROR: BundleEjected bundle not found - bundleid: " + bundleId.ToString());
                //}
            }
            catch (Exception ex)
            {
                AddToLog(ex);
            }

            return result;
        }

        public void ClearPrintQueueWaiting()
        {
            lock (_lock_topSheetQueue)
            {
                for (int i = _topSheetQueue.Count - 1; i >= 0; i--)
                {
                    if (_topSheetQueue[i].PrintedTime == DateTime.MinValue)
                    {
                        _topSheetQueue.RemoveAt(i);
                    }
                }
            }
        }

        public void RemoveFromPrintQueueWaiting(ushort bundleId)
        {
            lock (_lock_topSheetQueue)
            {
                if (_topSheetQueue.Count > 0)
                {
                    bool isFound = false;
                    int index = -1;
                    while (!isFound && ++index < _topSheetQueue.Count)
                    {
                        if (_topSheetQueue[index].PrintedTime == DateTime.MinValue)
                        {
                            if (_topSheetQueue[index].BundleId == bundleId)
                            {
                                _topSheetQueue.RemoveAt(index);
                                isFound = true;
                            }
                        }
                    }
                }
            }
        }

        public void ResetApplicator(bool clearQueues)
        {
            if (!_resetDone)
            {
                if (_printer1 != null && _printer1.PrinterStatus != null)
                {
                    _printer1.PrinterStatus.ReadyTime = DateTime.UtcNow;
                }

                if (_printer2 != null && _printer2.PrinterStatus != null)
                {
                    _printer2.PrinterStatus.ReadyTime = DateTime.UtcNow;
                }

                if (IcpConnectionState == IcpConnectionStates.Connected)
                {
                    IcpGetOrAddTelegram(IcpTelegramTypes.Reset, null, true);
                }
                
                _resetDone = true;
            }

            if (clearQueues)
            {
                //_topSheetPrinted.Clear();
                //_topSheetQueue.Clear();

                lock (_lock_topSheetPrinted)
                {
                    while (_topSheetPrinted.Count > 0)
                    {
                        TopSheetItem tempItem = _topSheetPrinted[0];

                        //AddToLogList("ResetApplicator bundleid: " + tempItem.BundleId.ToString()
                        //   + " - bundleIndex: " + tempItem.BundleIndex.ToString());

                        _topSheetPrinted.RemoveAt(0);

                        string sql = string.Format("exec UniStack_TopSheetBundleAction {0}, {1}, {2}, {3}, {4}, {5}, {6}"
                             , _gripperLineTableId
                             , _topSheetLineTableId
                             , tempItem.BundleId
                             , tempItem.BundleIndex
                             , (int)TopSheetActionEnum.Reset// 13//reset
                             , -1
                             , tempItem.PrinterNumber - 1
                             );

                        DbAddCommandToQueue(DbCommandTypes.ExecuteNonQuery, sql, null, true);

                        //PrintBundleRemovedFromQueue(tempItem.BundleId);
                    }
                }

                lock (_lock_topSheetQueue)
                {
                    while (_topSheetQueue.Count > 0)
                    {
                        TopSheetItem tempItem = _topSheetQueue[0];
                        _topSheetQueue.RemoveAt(0);

                        string sql = string.Format("exec UniStack_TopSheetBundleAction {0}, {1}, {2}, {3}, {4}, {5}, {6}"
                             , _gripperLineTableId
                             , _topSheetLineTableId
                             , tempItem.BundleId
                             , tempItem.BundleIndex
                             , (int)TopSheetActionEnum.Reset//13//reset
                             , -1
                             , tempItem.PrinterNumber - 1
                             );

                        DbAddCommandToQueue(DbCommandTypes.ExecuteNonQuery, sql, null, true);
                        PrintBundleRemovedFromQueue(tempItem.BundleId);
                    }
                }
            }
        }

        public bool TestPrint(int printerNumber)
        {
            bool result = false;

            //if (CheckIfReadyForTestPrint(printerNumber))
            {
                UniDevLib.Gui.UserControls.UniLabel.PrinterVer2 printer = null;
                if (printerNumber == 1)
                {
                    printer = _printer1;
                    _lastPrint1 = DateTime.UtcNow;
                }
                else if (printerNumber == 2)
                {
                    printer = _printer2;
                    _lastPrint2 = DateTime.UtcNow;
                }

                /*
                select * from UniStack_GetLabelData(-1, -1, -1, 65501)--test1
                select * from UniStack_GetLabelData(-1, -1, -1, 65502)--test2
                */

                if (printer != null && printer.PrinterStatus != null)
                {
                    string documentName = "Test " + printerNumber.ToString();
                    ushort bundleId = (ushort)(65500 + printerNumber);

                    //bool oldDoor = printer.PrinterStatus.TrapDoorOk;
                    //bool oldPaper = printer.PrinterStatus.TrapPaperOk;
                    //bool oldToner = printer.PrinterStatus.TrapTonerOk;

                    //printer.PrinterStatus.TrapDoorOk = true;
                    //printer.PrinterStatus.TrapPaperOk = true;
                    //printer.PrinterStatus.TrapTonerOk = true;

                    UniDevLib.Gui.UserControls.UniLabel.ResultTypes printResult =
                        PrintTopSheet(printerNumber, documentName, bundleId);

                    if (printResult == UniDevLib.Gui.UserControls.UniLabel.ResultTypes.Ok)
                    {
                        printer.PrinterStatus.TrapDoorOk = true;
                        printer.PrinterStatus.TrapPaperOk = true;
                        printer.PrinterStatus.TrapTonerOk = true;
                        printer.PrinterStatus.TrapPrinterOk = true;

                        result = true;
                    }
                    //else
                    //{
                    //    if (!oldDoor)
                    //    {
                    //        printer.PrinterStatus.TrapDoorOk = oldDoor;
                    //    }

                    //    if (!oldPaper)
                    //    {
                    //        printer.PrinterStatus.TrapPaperOk = oldPaper;
                    //    }

                    //    if (!oldToner)
                    //    {
                    //        printer.PrinterStatus.TrapTonerOk = oldToner;
                    //    }
                    //}

                    printer.PrinterStatus.ReadyTime = DateTime.UtcNow.AddSeconds(10);//set to initialize on test print
                }
            }

            return result;
        }

        public void RequestEnableLogFile(bool doEnable)
        {
            _enableLogFile = doEnable;
        }

        public void RequestEnableLogDebug(bool doEnable)
        {
            _enableLogDebug = doEnable;
        }

        #endregion

        #region Icp communication

        private void IcpControl()
        {
            try
            {
                IcpConnectionState = IcpConnectionStates.Disconnected;
                byte[] response;
                int readLength = _statusStructSize;
                bool statusReceived = false;

                AddToLog(LogLevels.Debug, "IcpControl started");

                while (UniDevLib.StaticInfo.IsRunning &&
                    IsStarted && _icpThread.ManagedThreadId == Thread.CurrentThread.ManagedThreadId)
                {
                    try
                    {
                        try
                        {
                            //try
                            //{
                            //    if (DateTime.UtcNow > _icpLastGetThreadId.AddSeconds(60))
                            //    {
                            //        _icpLastGetThreadId = DateTime.UtcNow;
                            //        _icpSystemThreadId = NativeMethods.GetCurrentThreadId();
                            //    }
                            //}
                            //catch
                            //{
                            //}

                            if (_tcpClientIcp == null || _tcpClientIcp.Client == null || !_tcpClientIcp.Client.Connected)
                            {
                                RequestStackerReset = false;

                                IcpConnectionState = IcpConnectionStates.Disconnected;
                                //_isWaitingToSendPrintToIcp = false;

                                if (_setupReceived &&
                                    _ioIp != null && _ioIp.Length > 0 && _ioPort > 0)
                                {
                                    AddToLog(LogLevels.Debug, "Icp connecting ...");

                                    if (_doReconnectIcp)
                                    {
                                        _doReconnectIcp = false;
                                    }

                                    statusReceived = false;

                                    _tcpClientIcp = new TcpClient();
                                    _tcpClientIcp.ReceiveTimeout = 1000;
                                    _tcpClientIcp.SendTimeout = 1000;

                                    //_tcpClientIcp.LingerState = new LingerOption(true, 0);

                                    IcpConnectionState = IcpConnectionStates.Connecting;

                                    _tcpClientIcp.Connect(_ioIp, _ioPort);

                                    _networkStreamIcp = _tcpClientIcp.GetStream();
                                    _networkStreamIcp.ReadTimeout = 1000;
                                    _networkStreamIcp.WriteTimeout = 1000;

                                    _binaryReaderIcp = new BinaryReader(_networkStreamIcp);
                                    _binaryWriterIcp = new BinaryWriter(_networkStreamIcp);

                                    IcpConnectionState = IcpConnectionStates.Connected;
                                    AddToLog(LogLevels.Debug, "Icp connected");

                                    //lock (_lockTelegramListIcp)
                                    {
                                        //_telegramListIcp.Clear();
                                    }
                                    IcpTelegramList_Clear();

                                    //DbGetSetup();
                                    //DbGetItems();
                                    IcpGetOrAddTelegram(IcpTelegramTypes.SendSetup, null, true);

                                    //try
                                    //{
                                    //    _networkStreamIcp.Flush();
                                    //}
                                    //catch
                                    //{
                                    //}

                                    int bytesCleared = 0;
                                    try
                                    {
                                        while (_tcpClientIcp.Client != null && _tcpClientIcp.Client.Connected)
                                        {
                                            _binaryReaderIcp.ReadByte();
                                            ++bytesCleared;
                                        }
                                    }
                                    catch
                                    {
                                    }

                                    if (bytesCleared > 0)
                                    {
                                        AddToLog(LogLevels.Warning, "Icp buffer cleared - num bytes = " + bytesCleared.ToString());
                                    }
                                }
                            }

                            if (_tcpClientIcp != null && _tcpClientIcp.Client != null && _tcpClientIcp.Client.Connected)
                            {
                                byte[] telegram = null;

                                /*
                                //lock (_lockTelegramListIcp)
                                {
                                    if (statusReceived && _telegramListIcp.Count > 0)
                                    {
                                        telegram = _telegramListIcp[0];
                                        //if (telegram[3] == 'C')
                                        //{
                                        //    _isWaitingToSendPrintToIcp = false;
                                        //}
                                        _telegramListIcp.RemoveAt(0);
                                    }
                                    else
                                    {
                                        telegram = IcpGetOrAddTelegram(IcpTelegramTypes.RequestStatus, null, false);
                                    }
                                    
                                }
                                */

                                
                                if (statusReceived && IcpTelegramList_GetCount() > 0)
                                {
                                    telegram = IcpTelegramList_GetNext();
                                }

                                if (telegram == null)
                                {
                                    telegram = IcpGetOrAddTelegram(IcpTelegramTypes.RequestStatus, null, false);
                                }

                                _binaryWriterIcp.Write(telegram);
                                _binaryWriterIcp.Flush();

                                response = _binaryReaderIcp.ReadBytes(readLength);
                                if (response.Length == readLength
                                    && response[0] == telegram[0]
                                    && response[1] == telegram[1]
                                    && response[2] == telegram[3]
                                    && response[readLength - 1] == 4)
                                {
                                    //lock (_lockStatus)
                                    {
                                        GCHandle gch = GCHandle.Alloc(response, GCHandleType.Pinned);
                                        try
                                        {
                                            _status = (StatusStruct)Marshal.PtrToStructure(gch.AddrOfPinnedObject(), typeof(StatusStruct));

                                            _topSheetLastOnline = DateTime.UtcNow;

                                            //if (_telegramListIcp.Count == 0)
                                            if (IcpTelegramList_GetCount() == 0)
                                            {
                                                _printQueueUpdated1 = true;
                                                _printQueueUpdated2 = true;
                                            }

                                            //if (_status.FunctionInputState1 == 0)
                                            if (!BundleAtStackerExit)
                                            {
                                                _stackerExitReadyTime = DateTime.UtcNow;
                                            }

                                            if (_status.NextMachineReadyState == 1)
                                            {
                                                _nextMachineReadyTime = DateTime.UtcNow;
                                            }

                                            if (_status.DetectedStackerPosition > 0 &&
                                                _status.DetectedStackerPosition != _currentStackerPositionId)
                                            {
                                                SetStackerPosition(_status.DetectedStackerPosition);
                                            }

                                            //if (telegram[3] == 'C')
                                            //{
                                            //    _isWaitingToSendPrintToIcp = false;
                                            //}

                                            if (_status.BundlesHandledCount > 0)
                                            {
                                                for (int i = 0; i < _status.BundlesHandledCount; i++)
                                                {
                                                    BundleHandledStruct bundleHandled = _status.BundlesHandled[i];
                                                    int bundleIndex;

                                                    if (bundleHandled.Code == 0)
                                                    {
                                                        bundleIndex = bundleHandled.ConveyorBundleIndex;
                                                    }
                                                    else
                                                    {
                                                        bundleIndex = bundleHandled.RejectedBundleIndex;
                                                    }

                                                    //if (bundleHandled.ConveyorBundleIndex < bundleHandled.LabelBundleIndex)
                                                    //{
                                                    //    bundleIndex = bundleHandled.ConveyorBundleIndex;
                                                    //}
                                                    //else
                                                    //{
                                                    //    bundleIndex = bundleHandled.LabelBundleIndex;
                                                    //}

                                                    string sql;
                                                    int bundleId = 0;
                                                    //int listIndex;
                                                    TopSheetItem topSheetItem = GetTopSheetItemPrinted(bundleIndex, false);//, out listIndex);
                                                    if (topSheetItem != null)
                                                    {
                                                        topSheetItem.HandledCode = bundleHandled.Code;
                                                        bundleId = topSheetItem.BundleId;

                                                        if (bundleHandled.PrinterIndex == 255)
                                                        {
                                                            bundleHandled.PrinterIndex = (byte)(topSheetItem.PrinterNumber - 1);
                                                        }

                                                        if (bundleHandled.Code == 0)//labeling ok
                                                        {
                                                            //AddToLogList("IcpControl labeled ok bundleid: "
                                                            //   + _topSheetPrinted[listIndex].BundleId.ToString()
                                                            //   + " - bundleIndex: " + _topSheetPrinted[listIndex].BundleIndex.ToString());

                                                            lock (_lock_topSheetPrinted)
                                                            {
                                                                for (int i2 = _topSheetPrinted.Count - 1; i2 >= 0; i2--)
                                                                {
                                                                    TopSheetItem tempItem = _topSheetPrinted[i2];
                                                                    if (tempItem.BundleIndex >= 11 && tempItem.BundleIndex < bundleIndex)
                                                                    {
                                                                        sql = string.Format("exec UniStack_TopSheetBundleAction {0}, {1}, {2}, {3}, {4}, {5}, {6}"
                                                                            , _gripperLineTableId
                                                                            , _topSheetLineTableId
                                                                            , tempItem.BundleId
                                                                            , tempItem.BundleIndex
                                                                            , (int)TopSheetActionEnum.BundleNotReportedPrintedList//16
                                                                            , -1
                                                                            , tempItem.PrinterNumber - 1
                                                                            );//bundle not reported

                                                                        DbAddCommandToQueue(DbCommandTypes.ExecuteNonQuery, sql, null, true);


                                                                        //AddToLogList("ERROR: IcpControl not reported (printed) bundleid: "
                                                                        //   + tempItem.BundleId.ToString()
                                                                        //   + " - bundleIndex: " + tempItem.BundleIndex.ToString());

                                                                        _topSheetPrinted.RemoveAt(i2);
                                                                    }
                                                                }
                                                            }

                                                            lock (_lock_topSheetQueue)
                                                            {
                                                                for (int i2 = _topSheetQueue.Count - 1; i2 >= 0; i2--)
                                                                {
                                                                    TopSheetItem tempItem = _topSheetQueue[i2];
                                                                    if (tempItem.BundleIndex >= 11 && tempItem.BundleIndex < bundleIndex)
                                                                    {
                                                                        sql = string.Format("exec UniStack_TopSheetBundleAction {0}, {1}, {2}, {3}, {4}, {5}, {6}"
                                                                            , _gripperLineTableId
                                                                            , _topSheetLineTableId
                                                                            , tempItem.BundleId
                                                                            , tempItem.BundleIndex
                                                                            , (int)TopSheetActionEnum.BundleNotReportedPrintQueue//12
                                                                            , -1
                                                                            , tempItem.PrinterNumber - 1
                                                                            );//bundle not reported

                                                                        DbAddCommandToQueue(DbCommandTypes.ExecuteNonQuery, sql, null, true);

                                                                        //AddToLogList("ERROR: IcpControl not reported (not printed) bundleid: "
                                                                        //   + tempItem.BundleId.ToString()
                                                                        //   + " - bundleIndex: " + tempItem.BundleIndex.ToString());

                                                                        _topSheetQueue.RemoveAt(i2);
                                                                    }
                                                                }
                                                            }

                                                        }

                                                        GetTopSheetItemPrinted(bundleIndex, true);

                                                        /*
                                                        GetTopSheetItemPrinted(bundleIndex, out listIndex);
                                                        if (listIndex >= 0)
                                                        {
                                                            //AddToLogList("IcpControl remove from printed list bundleid: "
                                                            //   + _topSheetPrinted[listIndex].BundleId.ToString()
                                                            //   + " - bundleIndex: " + _topSheetPrinted[listIndex].BundleIndex.ToString()
                                                            //   + " - code: " + bundleHandled.Code
                                                            //   + " - ConveyorBundleIndex: " + bundleHandled.ConveyorBundleIndex
                                                            //   + " - LabelBundleIndex: " + bundleHandled.LabelBundleIndex
                                                            //   + " - RejectedBundleIndex: " + bundleHandled.RejectedBundleIndex
                                                            //   + " - Position: " + bundleHandled.Position
                                                            //   + " - PrinterIndex: " + bundleHandled.PrinterIndex
                                                            //   );

                                                            _topSheetPrinted.RemoveAt(listIndex);
                                                        }
                                                        //else
                                                        //{
                                                        //    AddToLogList("ERROR: IcpControl remove from printed list not found bundleindex: "
                                                        //        + bundleIndex.ToString());
                                                        //}
                                                        */
                                                    }

                                                    sql = string.Format("exec UniStack_TopSheetBundleAction {0}, {1}, {2}, {3}, {4}, {5}, {6}"
                                                        , _gripperLineTableId
                                                        , _topSheetLineTableId
                                                        , bundleId
                                                        , bundleIndex
                                                        , bundleHandled.Code
                                                        , bundleHandled.Position == 255 ? -1 : bundleHandled.Position
                                                        , bundleHandled.PrinterIndex == 255 ? -1 : bundleHandled.PrinterIndex
                                                        );

                                                    DbAddCommandToQueue(DbCommandTypes.ExecuteNonQuery, sql, null, true);
                                                }
                                            }

                                            statusReceived = true;
                                        }
                                        finally
                                        {
                                            gch.Free();
                                        }

                                        if (_status.Length != readLength)
                                        {
                                            AddToLog(LogLevels.Warning, "Error in ICP response header - wrong length");

                                            IcpDisconnect();
                                        }
                                    }
                                }
                                else
                                {
                                    AddToLog(LogLevels.Warning, "Error in ICP response header - not in sync");

                                    IcpDisconnect();
                                }

                                //if (_telegramListIcp.Count == 0 || telegram[3] == (byte)'C')
                                //{
                                //    _waitingToSendProductionData = false;
                                //}

                                if (_doReconnectIcp)
                                {
                                    _doReconnectIcp = false;
                                    IcpDisconnect();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            AddToLog(LogLevels.Warning, ex.ToString());
                            IcpDisconnect();
                            Thread.Sleep(1000);
                        }
                        finally
                        {
                            int index = 0;
                            do
                            {
                                Thread.Sleep(10);
                            }
                            while (IcpTelegramList_GetCount() == 0 && ++index < 20);

                            //Thread.Sleep(200);
                        }
                    }
                    catch
                    {
                    }
                }

                if (_icpThread.ManagedThreadId != Thread.CurrentThread.ManagedThreadId)
                {
                    AddToLog(LogLevels.Debug, "IcpControl old thread exited");
                }
                else
                {
                    IcpDisconnect();
                    IcpConnectionState = IcpConnectionStates.NotRunning;
                    AddToLog(LogLevels.Debug, "IcpControl stopped");
                }
            }
            catch
            {
            }
        }

        #region Add telegram

        public void IcpAddTelegramSendEjectedBundleIndex(ushort bundleIndex)
        {
            object oParameters = bundleIndex;
            IcpGetOrAddTelegram(IcpTelegramTypes.SendEjectedBundleIndex, oParameters, true);
        }

        public void IcpAddTelegramSendPrintedBundleIndex(ushort bundleIndex, byte printerNumber)
        {
            object oParameters = new object[] { bundleIndex, printerNumber };
            IcpGetOrAddTelegram(IcpTelegramTypes.SendEjectedBundleIndex, oParameters, true);
        }

        private byte[] IcpGetOrAddTelegram(IcpTelegramTypes icpTelegramType,
            object oParameters, bool doAddToList)
        {
            byte[] result = null;

            //byte[] bParameters;
            byte pcStatus = _pcStatus;// 2;

            if ((IsPaused1 || IsPausedByOperator1) && (!UseTwoPrinters || IsPaused2 || IsPausedByOperator2))
            {
                pcStatus = 3;
            }

            //pcStatus = 6; //just for test

            /*
            LabelWithoutTracking = 1,
            LabelWithTracking = 2,
            NoLabel = 3
            SimulateBundleIndex = 4,
            SimulateLabelIndex = 5,
            SimulateBundleAndLabelIndex = 6
            */

            List<byte> byteList;
            int size;
            byte sizeL;
            byte sizeH;
            byte[] bytes;
            ushort bundleIndex;
            byte printerNumber;
            byte low;
            byte high;
            //object[] oParameterArray;
            TopSheetItem topSheetItem;
            //byte printerStatus1 = _printer1 != null && _printer1.PrinterStatus != null &&
            //    _printer1.PrinterStatus.IsReadyToPrint ? (byte)1 : (byte)0;
            //byte printerStatus2 = _printer2 != null && _printer2.PrinterStatus != null &&
            //    _printer2.PrinterStatus.IsReadyToPrint ? (byte)1 : (byte)0;

            byte printerStatus1 = PcStatus_Printer1;
            byte printerStatus2 = PcStatus_Printer2;

            if (_tcpClientIcp != null && _tcpClientIcp.Client != null && _tcpClientIcp.Client.Connected)
            {
                if (_telegramId < 99)
                {
                    ++_telegramId;
                }
                else
                {
                    _telegramId = 1;
                }

                switch (icpTelegramType)
                {
                    case IcpTelegramTypes.RequestStatus://A
                        //pcStatus = 5;//request packets and positions status

                        if (_telegramRequestStatus == null)
                        {
                            int expectedStructSize = _statusStructSize - 6;
                            _telegramRequestStatus = new byte[] {
                                SOH, _telegramId, pcStatus, (byte)'A', 11, 0,
                                (byte)(expectedStructSize % 256),
                                (byte)(expectedStructSize / 256),
                                printerStatus1, printerStatus2, EOT };
                        }
                        else
                        {
                            _telegramRequestStatus[1] = _telegramId;
                            _telegramRequestStatus[2] = pcStatus;
                            _telegramRequestStatus[8] = printerStatus1;
                            _telegramRequestStatus[9] = printerStatus2;
                        }

                        result = _telegramRequestStatus;
                        break;

                    case IcpTelegramTypes.SendEjectedBundleIndex://B
                        //bundleIndex = (ushort)oParameters;
                        _resetDone = false;
                        topSheetItem = (TopSheetItem)oParameters;
                        bundleIndex = topSheetItem.BundleIndex;
                        low = (byte)(bundleIndex & 0xFF);
                        high = (byte)(bundleIndex / 0x100);
                        result = new byte[] { SOH, GetTelegramId(), (byte)pcStatus, (byte)'B', 9, 0, low, high, EOT };
                        break;

                    case IcpTelegramTypes.SendPrintedBundleIndex://C
                        _resetDone = false;
                        topSheetItem = (TopSheetItem)oParameters;
                        bundleIndex = topSheetItem.BundleIndex;// (ushort)oParameterArray[0];
                        printerNumber = topSheetItem.PrinterNumber;
                        low = (byte)(bundleIndex & 0xFF);
                        high = (byte)(bundleIndex / 0x100);
                        result = new byte[] { SOH, GetTelegramId(), (byte)pcStatus, (byte)'C', 10, 0, low, high, printerNumber, EOT };
                        break;

                    case IcpTelegramTypes.SendSetup://D
                        bytes = GetSetupStructBytes();
                        size = 7 + bytes.Length;
                        sizeL = (byte)(size % 256);
                        sizeH = (byte)(size / 256);
                        byteList = new List<byte>();

                        byteList.Add(SOH);
                        byteList.Add(_telegramId);
                        byteList.Add(pcStatus);
                        byteList.Add((byte)'D');
                        byteList.Add(sizeL);
                        byteList.Add(sizeH);

                        byteList.AddRange(bytes);

                        byteList.Add(EOT);

                        result = byteList.ToArray();
                        break;

                    case IcpTelegramTypes.Reset://E
                        result = new byte[] { SOH, GetTelegramId(), (byte)pcStatus, (byte)'E', 7, 0, EOT };
                        break;
                    //case IcpTelegramTypes.SetDigitalOutput://G
                    //    //SOH + ID + PCSTAT + G + LEN + SLOT + UTGANG + STATE + EOT
                    //    bParameters = (byte[])oParameters;
                    //    byte slot = bParameters[0];
                    //    byte output = bParameters[1];
                    //    byte state = bParameters[2];
                    //    result = new byte[] { SOH, GetTelegramId(), pcStatus, (byte)'G', 10, 0, slot, output, state, EOT };
                    //    break;

                    case IcpTelegramTypes.SendStopApplication://H
                        result = new byte[] { SOH, GetTelegramId(), pcStatus, (byte)'H', 7, 0, EOT };
                        break;
                }

                if (doAddToList)
                {
                    IcpTelegramList_Add(result);
                }
            }

            return result;
        }

        private void IcpTelegramList_Add(byte[] telegram)
        {
            if (telegram != null && telegram.Length > 0)
            {
                lock (_lockTelegramListIcp)
                {
                    _telegramListIcp.Add(telegram);
                }
            }
        }

        private byte[] IcpTelegramList_GetNext()
        {
            byte[] result = null;

            lock (_lockTelegramListIcp)
            {
                if (_telegramListIcp.Count > 0)
                {
                    result = _telegramListIcp[0];
                    _telegramListIcp.RemoveAt(0);
                }
            }

            return result;
        }

        private void IcpTelegramList_Clear()
        {
            lock (_lockTelegramListIcp)
            {
                _telegramListIcp.Clear();
            }
        }

        private int IcpTelegramList_GetCount()
        {
            int result;
            lock (_lockTelegramListIcp)
            {
                result = _telegramListIcp.Count;
            }
            return result;
        }

        #endregion

        #region GetTelegramId

        private byte GetTelegramId()
        {
            byte result;

            lock (_lockTelegram)
            {
                if (_telegramId == 255)
                {
                    _telegramId = 0;
                }
                result = _telegramId++;
            }

            return result;
        }

        #endregion

        #region Get struct bytes
        
        private byte[] GetSetupStructBytes()
        {
            int len = _setupStructSize;
            byte[] result = new byte[len];

            IntPtr ptr = Marshal.AllocHGlobal(len);
            Marshal.StructureToPtr(_setup, ptr, true);
            Marshal.Copy(ptr, result, 0, len);

            Marshal.FreeHGlobal(ptr);

            return result;
        }
        
        #endregion

        private void IcpDisconnect()
        {
            AddToLog(LogLevels.Warning, "IcpDisconnect");

            IcpConnectionState = IcpConnectionStates.Disconnected;

            //_telegramListIcp.Clear();
            IcpTelegramList_Clear();
            //_doReconnectDb = true;
            _setupReceived = false;
            try
            {
                if (_con != null && _con.State == ConnectionState.Open)
                {
                    DbGetSetup();
                    DbGetItems();
                }
            }
            catch
            {
            }

            if (_binaryReaderIcp != null)
            {
                try
                {
                    _binaryReaderIcp.Close();
                    _binaryReaderIcp.Dispose();
                }
                catch
                {
                }
                finally
                {
                    _binaryReaderIcp = null;
                }
            }

            if (_binaryWriterIcp != null)
            {
                try
                {
                    _binaryWriterIcp.Close();
                    _binaryWriterIcp.Dispose();
                }
                catch
                {
                }
                finally
                {
                    _binaryWriterIcp = null;
                }
            }

            if (_networkStreamIcp != null)
            {
                try
                {
                    _networkStreamIcp.Close();
                    _networkStreamIcp.Dispose();
                }
                catch
                {
                }
                finally
                {
                    _networkStreamIcp = null;
                }
            }

            if (_tcpClientIcp != null)
            {
                try
                {
                    _tcpClientIcp.Close();
                }
                catch
                {
                }
                finally
                {
                    _tcpClientIcp = null;
                }
            }
        }

        #endregion

        #region Database communication

        public void DbReconnect()
        {
            _doReconnectDb = true;
        }

        private void DbControl()
        {
            try
            {
                string sql;
                object oParams;
                //OleDbCommand cmd;
                byte sleepCounter;
                DbCommandStuct dbCommand;
                DateTime lastRequestCommand = DateTime.MinValue;
                DbCommandTypes currentCommand;

                AddToLog(LogLevels.Debug, "DbControl started");

                while (UniDevLib.StaticInfo.IsRunning &&
                    IsStarted && _dbThread.ManagedThreadId == Thread.CurrentThread.ManagedThreadId)
                {
                    try
                    {
                        try
                        {
                            //try
                            //{
                            //    if (DateTime.UtcNow > _dbLastGetThreadId.AddSeconds(60))
                            //    {
                            //        _dbLastGetThreadId = DateTime.UtcNow;
                            //        _dbSystemThreadId = NativeMethods.GetCurrentThreadId();
                            //    }
                            //}
                            //catch
                            //{
                            //}

                            if (_con == null || _con.State != System.Data.ConnectionState.Open)
                            {
                                DbConnectionState = DbConnectionStates.Disconnected;

                                if (_connectionString != null && _connectionString.Length > 0)
                                {
                                    _con = new OleDbConnection(_connectionString);

                                    DbConnectionState = DbConnectionStates.Connecting;

                                    _con.Open();

                                    if (_con != null && _con.State == System.Data.ConnectionState.Open)
                                    {
                                        DbConnectionState = DbConnectionStates.Connected;

                                        //sql = "select top 1 TopSheetIOIP from UniStack_TopSheetLineTable where Id = " + _topSheetLineTableId.ToString();
                                        //cmd = new OleDbCommand(sql, _con);
                                        //_ioIp = "192.168.2.12";// (string)cmd.ExecuteScalar();

                                        //sql = "select top 1 TopSheetIOPort from UniStack_TopSheetLineTable where Id = " + _topSheetLineTableId.ToString();
                                        //cmd = new OleDbCommand(sql, _con);
                                        //_ioPort = 10000;// (int)cmd.ExecuteScalar();

                                        _setupReceived = false;
                                        //if (!_setupReceived)
                                        {
                                            DbGetSetup();
                                            DbGetItems();
                                        }

                                        _doReconnectIcp = true;
                                    }
                                }
                            }

                            if (_con != null && _con.State == System.Data.ConnectionState.Open)
                            {
                                sql = null;
                                currentCommand = DbCommandTypes.None;
                                oParams = null;

                                if (lastRequestCommand < DateTime.UtcNow.AddMilliseconds(-1000))
                                {
                                    lastRequestCommand = DateTime.UtcNow;

                                    sql = string.Format("DECLARE @return_value int;declare @IOTopSheetCommand int;"
                                        + " declare @TopSheetLineTableId int; set @TopSheetLineTableId={0}"
                                        + " EXEC @return_value = UniStack_GetNextTopSheetCommand"
                                        + " @TopSheetLineTableId,"
                                        + " @IOTopSheetCommand output;SELECT 'Return Value' = @IOTopSheetCommand",
                                        _topSheetLineTableId);
                                    currentCommand = DbCommandTypes.GetNextCommand;
                                }
                                else
                                {
                                    lock (_lockDbCommandList)
                                    {
                                        if (_dbCommandList.Count > 0)
                                        {
                                            dbCommand = _dbCommandList[0];
                                            _dbCommandList.RemoveAt(0);
                                            currentCommand = dbCommand.dbCommandType;
                                            sql = dbCommand.sql;
                                            oParams = dbCommand.oParams;
                                        }
                                    }
                                }

                                if (sql != null && currentCommand != DbCommandTypes.None)
                                {
                                    DbExecuteCommand(currentCommand, sql, oParams);
                                }

                                sleepCounter = 0;
                                do
                                {
                                    Thread.Sleep(50);
                                }
                                while (++sleepCounter < 20 && _dbCommandList.Count == 0);

                                if (_doReconnectDb)
                                {
                                    _doReconnectDb = false;
                                    DbDisconnect();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            AddToLog(ex);

                            DbDisconnect();
                        }
                        finally
                        {
                            Thread.Sleep(10);
                        }
                    }
                    catch
                    {
                    }
                }

                if (_dbThread.ManagedThreadId != Thread.CurrentThread.ManagedThreadId)
                {
                    AddToLog(LogLevels.Debug, "DbControl old thread exited");
                }
                else
                {
                    DbDisconnect();
                    AddToLog(LogLevels.Debug, "DbControl stopped");
                    DbConnectionState = DbConnectionStates.NotRunning;
                }
            }
            catch
            {
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        private void DbExecuteCommand(DbCommandTypes commandType, string sql, object oParams)
        {
            OleDbCommand cmd;
            OleDbDataReader dr = null;

            switch (commandType)
            {
                case DbCommandTypes.ExecuteNonQuery:
                    cmd = new OleDbCommand(sql, _con);
                    DBTools.HandleBusyExecuteNonQuery(cmd);
                    break;

                case DbCommandTypes.GetNextCommand:
                    cmd = new OleDbCommand(sql, _con);
                    object o = DBTools.HandleBusyExecuteScalar(cmd);
                    if (o != null && !(o is DBNull))
                    {
                        //string nextCommand = o as string;
                        //if (nextCommand != null && nextCommand.Length > 0)
                        //{
                        //    char ioCommand = nextCommand[0];
                        //    AddCommandFromDB(ioCommand);
                        //}

                        int iResult = (int)o;
                        if (iResult > 0)
                        {
                            byte requestCommand = (byte)(iResult % 256);//bits 0-7 reserved (1-255) for request command
                            if (requestCommand > 0)
                            {
                                char ioCommand = (char)requestCommand;
                                AddCommandFromDB(ioCommand);
                            }
                        }

                        byte printerStates = (byte)((iResult & 0xFF00) / 0x100);//bits 8-15
                        IsPaused1 = (printerStates & 1) > 0;
                        IsPaused2 = (printerStates & 2) > 0;
                    }
                    break;

                case DbCommandTypes.GetSetup:
                    cmd = new OleDbCommand(sql, _con);
                    dr = DBTools.HandleBusyExecuteReader(cmd);

                    if (dr.HasRows)
                    {
                        bool hasData = dr.Read();
                        if (hasData)
                        {
                            _setup.LineNumber = (byte)_topSheetLineTableId;
                            _setup.ApplicatorType = (byte)dr["TopSheetApplicatorTypeId"];

                            _setup.CurrentStackerPosition = (byte)_currentStackerPositionId;// (byte)dr["CurrentStackerPosition"];
                            
                            _setup.LabelingConveyorPosition = (byte)dr["LabelingConveyorPosition"];
                            _setup.FirstConveyorControlPosition = (byte)dr["FirstConveyorControlPosition"];
                            _setup.ReadyToPreviousControlPosition = (byte)dr["ReadyToPreviousControlPosition"];

                            _setup.InputSignalsFilter = (short)dr["InputSignalsFilter"];

                            _setup.ButtonStartInputSlot = (byte)dr["ButtonStartInputSlot"];
                            _setup.ButtonStartInputSignal = (byte)dr["ButtonStartInputSignal"];
                            _setup.ButtonStartInputOnHigh = (byte)(((bool)dr["ButtonStartInputOnHigh"]) ? 1 : 0);

                            _setup.ButtonStopInputSlot = (byte)dr["ButtonStopInputSlot"];
                            _setup.ButtonStopInputSignal = (byte)dr["ButtonStopInputSignal"];
                            _setup.ButtonStopInputOnHigh = (byte)(((bool)dr["ButtonStopInputOnHigh"]) ? 1 : 0);

                            _setup.ButtonResetInputSlot = (byte)dr["ButtonResetInputSlot"];
                            _setup.ButtonResetInputSignal = (byte)dr["ButtonResetInputSignal"];
                            _setup.ButtonResetInputOnHigh = (byte)(((bool)dr["ButtonResetInputOnHigh"]) ? 1 : 0);

                            _setup.ButtonStartConvInputSlot = (byte)dr["ButtonStartConvInputSlot"];
                            _setup.ButtonStartConvInputSignal = (byte)dr["ButtonStartConvInputSignal"];
                            _setup.ButtonStartConvInputOnHigh = (byte)(((bool)dr["ButtonStartConvInputOnHigh"]) ? 1 : 0);

                            _setup.ButtonStopConvInputSlot = (byte)dr["ButtonStopConvInputSlot"];
                            _setup.ButtonStopConvInputSignal = (byte)dr["ButtonStopConvInputSignal"];
                            _setup.ButtonStopConvInputOnHigh = (byte)(((bool)dr["ButtonStopConvInputOnHigh"]) ? 1 : 0);

                            _setup.StackerExitInputSlot = (byte)dr["StackerExitInputSlot"];
                            _setup.StackerExitInputSignal = (byte)dr["StackerExitInputSignal"];
                            _setup.StackerExitInputOnHigh = (byte)(((bool)dr["StackerExitInputOnHigh"]) ? 1 : 0);

                            _setup.Function1InputSlot = (byte)dr["Function1InputSlot"];
                            _setup.Function2InputSlot = (byte)dr["Function2InputSlot"];
                            _setup.Function3InputSlot = (byte)dr["Function3InputSlot"];
                            
                            _setup.Function1InputSignal = (byte)dr["Function1InputSignal"];
                            _setup.Function2InputSignal = (byte)dr["Function2InputSignal"];
                            _setup.Function3InputSignal = (byte)dr["Function3InputSignal"];
                            
                            _setup.Function1InputOnHigh = (byte)(((bool)dr["Function1InputOnHigh"]) ? 1 : 0);
                            _setup.Function2InputOnHigh = (byte)(((bool)dr["Function2InputOnHigh"]) ? 1 : 0);
                            _setup.Function3InputOnHigh = (byte)(((bool)dr["Function3InputOnHigh"]) ? 1 : 0);

                            //_setup.AlarmTemperatureInputSlot = 255;
                            //_setup.AlarmTemperatureInputSignal = 255;
                            //_setup.AlarmTemperatureInputSignal = 1;
                            //try
                            //{//new Lillestrøm 2016-10-31
                            //    _setup.AlarmTemperatureInputSlot = (byte)dr["AlarmTemperatureInputSlot"];
                            //    _setup.AlarmTemperatureInputSignal = (byte)dr["AlarmTemperatureInputSignal"];
                            //    _setup.AlarmTemperatureInputOnHigh = (byte)(((bool)dr["AlarmTemperatureInputOnHigh"]) ? 1 : 0);
                            //}
                            //catch
                            //{ }

                            _setup.ArmUpInputSlot = (byte)dr["ArmUpInputSlot"];
                            _setup.ArmUpInputSignal = (byte)dr["ArmUpInputSignal"];
                            _setup.ArmUpInputOnHigh = (byte)(((bool)dr["ArmUpInputOnHigh"]) ? 1 : 0);

                            _setup.ArmFrontInputSlot = (byte)dr["ArmFrontInputSlot"];
                            _setup.ArmFrontInputSignal = (byte)dr["ArmFrontInputSignal"];
                            _setup.ArmFrontInputOnHigh = (byte)(((bool)dr["ArmFrontInputOnHigh"]) ? 1 : 0);

                            _setup.ArmBackInputSlot = (byte)dr["ArmBackInputSlot"];
                            _setup.ArmBackInputSignal = (byte)dr["ArmBackInputSignal"];
                            _setup.ArmBackInputOnHigh = (byte)(((bool)dr["ArmBackInputOnHigh"]) ? 1 : 0);

                            _setup.ArmOnBundleInputSlot = (byte)dr["ArmOnBundleInputSlot"];
                            _setup.ArmOnBundleInputSignal = (byte)dr["ArmOnBundleInputSignal"];
                            _setup.ArmOnBundleInputOnHigh = (byte)(((bool)dr["ArmOnBundleInputOnHigh"]) ? 1 : 0);

                            _setup.RejectReadyInputSlot = (byte)dr["RejectReadyInputSlot"];
                            _setup.RejectReadyInputSignal = (byte)dr["RejectReadyInputSignal"];
                            _setup.RejectReadyInputOnHigh = (byte)(((bool)dr["RejectReadyInputOnHigh"]) ? 1 : 0);

                            //_setup.Reject2ReadyInputSlot = (byte)dr["Reject2ReadyInputSlot"];
                            //_setup.Reject2ReadyInputSignal = (byte)dr["Reject2ReadyInputSignal"];
                            //_setup.Reject2ReadyInputOnHigh = (byte)(((bool)dr["Reject2ReadyInputOnHigh"]) ? 1 : 0);

                            //_setup.RejectSheetInPosInputSlot= (byte)dr["RejectSheetInPosInputSlot"];
                            //_setup.RejectSheetInPosInputSignal = (byte)dr["RejectSheetInPosInputSignal"];
                            //_setup.RejectSheetInPosInputOnHigh = (byte)(((bool)dr["RejectSheetInPosInputOnHigh"]) ? 1 : 0);

                            //_setup.Reject2SheetInPosInputSlot = (byte)dr["Reject2SheetInPosInputSlot"];
                            //_setup.Reject2SheetInPosInputSignal = (byte)dr["Reject2SheetInPosInputSignal"];
                            //_setup.Reject2SheetInPosInputOnHigh = (byte)(((bool)dr["Reject2SheetInPosInputOnHigh"]) ? 1 : 0);

                            _setup.RejectBackInputSlot = (byte)dr["RejectBackInputSlot"];
                            _setup.RejectBackInputSignal = (byte)dr["RejectBackInputSignal"];
                            _setup.RejectBackInputOnHigh = (byte)(((bool)dr["RejectBackInputOnHigh"]) ? 1 : 0);

                            _setup.DoorClosedInputSlot = (byte)dr["DoorClosedInputSlot"];
                            _setup.DoorClosedInputSignal = (byte)dr["DoorClosedInputSignal"];
                            _setup.DoorClosedInputOnHigh = (byte)(((bool)dr["DoorClosedInputOnHigh"]) ? 1 : 0);

                            _setup.AlarmThermoInputSlot = (byte)dr["AlarmThermoInputSlot"];
                            _setup.AlarmThermoInputSignal = (byte)dr["AlarmThermoInputSignal"];
                            _setup.AlarmThermoInputOnHigh = (byte)(((bool)dr["AlarmThermoInputOnHigh"]) ? 1 : 0);

                            _setup.AlarmThermo2InputSlot = (byte)dr["AlarmThermo2InputSlot"];
                            _setup.AlarmThermo2InputSignal = (byte)dr["AlarmThermo2InputSignal"];
                            _setup.AlarmThermo2InputOnHigh = (byte)(((bool)dr["AlarmThermo2InputOnHigh"]) ? 1 : 0);

                            _setup.AlarmFuseInputSlot = (byte)dr["AlarmFuseInputSlot"];
                            _setup.AlarmFuseInputSignal = (byte)dr["AlarmFuseInputSignal"];
                            _setup.AlarmFuseInputOnHigh = (byte)(((bool)dr["AlarmFuseInputOnHigh"]) ? 1 : 0);

                            _setup.EmergencyOffInputSlot = (byte)dr["EmergencyOffInputSlot"];
                            _setup.EmergencyOffInputSignal = (byte)dr["EmergencyOffInputSignal"];
                            _setup.EmergencyOffInputOnHigh = (byte)(((bool)dr["EmergencyOffInputOnHigh"]) ? 1 : 0);

                            _setup.EmergencyOffConvInputSlot = (byte)dr["EmergencyOffConvInputSlot"];
                            _setup.EmergencyOffConvInputSignal = (byte)dr["EmergencyOffConvInputSignal"];
                            _setup.EmergencyOffConvInputOnHigh = (byte)(((bool)dr["EmergencyOffConvInputOnHigh"]) ? 1 : 0);

                            _setup.EmergencyOffOutputSlot = (byte)dr["EmergencyOffOutputSlot"];
                            _setup.EmergencyOffOutputSignal = (byte)dr["EmergencyOffOutputSignal"];

                            _setup.EmergencyCircuitInputSlot = (byte)dr["EmergencyCircuitInputSlot"];
                            _setup.EmergencyCircuitInputSignal = (byte)dr["EmergencyCircuitInputSignal"];
                            _setup.EmergencyCircuitInputOnHigh = (byte)(((bool)dr["EmergencyCircuitInputOnHigh"]) ? 1 : 0);

                            _setup.NextMachineReadySlot = (byte)dr["NextMachineReadySlot"];
                            _setup.NextMachineReadySignal = (byte)dr["NextMachineReadySignal"];
                            _setup.NextMachineReadyOnHigh = (byte)(((bool)dr["NextMachineReadyOnHigh"]) ? 1 : 0);

                            _setup.PreviousMachineReadyOutputSlot = (byte)dr["PreviousMachineReadyOutputSlot"];
                            _setup.PreviousMachineReadyOutputSignal = (byte)dr["PreviousMachineReadyOutputSignal"];

                            _setup.PrevTransNotReadyOutputSlot = (byte)dr["PrevTransNotReadyOutputSlot"];
                            _setup.PrevTransNotReadyOutputSignal = (byte)dr["PrevTransNotReadyOutputSignal"];

                            _setup.NextMachineStandbyOutputSlot = (byte)dr["NextMachineStandbyOutputSlot"];
                            _setup.NextMachineStandbyOutputSignal = (byte)dr["NextMachineStandbyOutputSignal"];

                            _setup.ReadyPowerOutputSlot = (byte)dr["ReadyPowerOutputSlot"];
                            _setup.ReadyPowerOutputSignal = (byte)dr["ReadyPowerOutputSignal"];

                            _setup.ReadyAirOutputSlot = (byte)dr["ReadyAirOutputSlot"];
                            _setup.ReadyAirOutputSignal = (byte)dr["ReadyAirOutputSignal"];

                            _setup.ReadyConveyorsOutputSlot = (byte)dr["ReadyConveyorsOutputSlot"];
                            _setup.ReadyConveyorsOutputSignal = (byte)dr["ReadyConveyorsOutputSignal"];

                            _setup.ReadyBeltsOutputSlot = (byte)dr["ReadyBeltsOutputSlot"];
                            _setup.ReadyBeltsOutputSignal = (byte)dr["ReadyBeltsOutputSignal"];

                            _setup.StartLastConvButtonLightOutputSlot = (byte)dr["StartLastConvButtonLightOutputSlot"];
                            _setup.StartLastConvButtonLightOutputSignal = (byte)dr["StartLastConvButtonLightOutputSignal"];

                            _setup.StopLastConvButtonLightOutputSlot = (byte)dr["StopLastConvButtonLightOutputSlot"];
                            _setup.StopLastConvButtonLightOutputSignal = (byte)dr["StopLastConvButtonLightOutputSignal"];

                            _setup.ButtonStartLastConvInputSlot = (byte)dr["ButtonStartLastConvInputSlot"];
                            _setup.ButtonStartLastConvInputSignal = (byte)dr["ButtonStartLastConvInputSignal"];
                            _setup.ButtonStartLastConvInputOnHigh = (byte)(((bool)dr["ButtonStartLastConvInputOnHigh"]) ? 1 : 0);

                            _setup.ButtonStopLastConvInputSlot = (byte)dr["ButtonStopLastConvInputSlot"];
                            _setup.ButtonStopLastConvInputSignal = (byte)dr["ButtonStopLastConvInputSignal"];
                            _setup.ButtonStopLastConvInputOnHigh = (byte)(((bool)dr["ButtonStopLastConvInputOnHigh"]) ? 1 : 0);

                            _setup.ArmDownOutputSlot = (byte)dr["ArmDownOutputSlot"];
                            _setup.ArmDownOutputSignal = (byte)dr["ArmDownOutputSignal"];

                            _setup.ArmUpOutputSlot = (byte)dr["ArmUpOutputSlot"];
                            _setup.ArmUpOutputSignal = (byte)dr["ArmUpOutputSignal"];

                            _setup.ArmBackOutputSlot = (byte)dr["ArmBackOutputSlot"];
                            _setup.ArmBackOutputSignal = (byte)dr["ArmBackOutputSignal"];

                            _setup.ArmDownAfterLeaveDelay = (short)dr["ArmDownAfterLeaveDelay"];
                            _setup.ArmDownMintime = (short)dr["ArmDownMintime"];
                            _setup.ArmBackTimeout = (short)dr["ArmBackTimeout"];
                            _setup.ArmDownTimeout = (short)dr["ArmDownTimeout"];
                            _setup.ArmUpTimeout = (short)dr["ArmUpTimeout"];

                            _setup.RejectDownOutputSlot = (byte)dr["RejectDownOutputSlot"];
                            _setup.RejectDownOutputSignal = (byte)dr["RejectDownOutputSignal"];

                            _setup.RejectBackOutputSlot = (byte)dr["RejectBackOutputSlot"];
                            _setup.RejectBackOutputSignal = (byte)dr["RejectBackOutputSignal"];

                            //_setup.Reject2DownOutputSlot = (byte)dr["Reject2DownOutputSlot"];
                            //_setup.Reject2DownOutputSignal = (byte)dr["Reject2DownOutputSignal"];

                            //_setup.Reject2BackOutputSlot = (byte)dr["Reject2BackOutputSlot"];
                            //_setup.Reject2BackOutputSignal = (byte)dr["Reject2BackOutputSignal"];

                            _setup.RejectDownTime = (short)dr["RejectDownTime"];
                            _setup.RejectUpTime = (short)dr["RejectUpTime"];
                            _setup.RejectBackTimeout = (short)dr["RejectBackTimeout"];
                            _setup.RejectFrontTimeout = (short)dr["RejectFrontTimeout"];
                            _setup.RejectWaitForLabelTimeout = (short)dr["RejectWaitForLabelTimeout"];
                            _setup.RejectWaitForLabelGoneTimeout = (short)dr["RejectWaitForLabelGoneTimeout"];

                            _setup.StartButtonLightOutputSlot = (byte)dr["StartButtonLightOutputSlot"];
                            _setup.StartButtonLightOutputSignal = (byte)dr["StartButtonLightOutputSignal"];

                            _setup.StopButtonLightOutputSlot = (byte)dr["StopButtonLightOutputSlot"];
                            _setup.StopButtonLightOutputSignal = (byte)dr["StopButtonLightOutputSignal"];

                            _setup.ResetButtonLightOutputSlot = (byte)dr["ResetButtonLightOutputSlot"];
                            _setup.ResetButtonLightOutputSignal = (byte)dr["ResetButtonLightOutputSignal"];

                            _setup.StartConvButtonLightOutputSlot = (byte)dr["StartConvButtonLightOutputSlot"];
                            _setup.StartConvButtonLightOutputSignal = (byte)dr["StartConvButtonLightOutputSignal"];

                            _setup.StopConvButtonLightOutputSlot = (byte)dr["StopConvButtonLightOutputSlot"];
                            _setup.StopConvButtonLightOutputSignal = (byte)dr["StopConvButtonLightOutputSignal"];

                            _setup.GreenLightOutputSlot = (byte)dr["GreenLightOutputSlot"];
                            _setup.GreenLightOutputSignal = (byte)dr["GreenLightOutputSignal"];
                            _setup.YellowLightOutputSlot = (byte)dr["YellowLightOutputSlot"];
                            _setup.YellowLightOutputSignal = (byte)dr["YellowLightOutputSignal"];
                            _setup.RedLightOutputSlot = (byte)dr["RedLightOutputSlot"];
                            _setup.RedLightOutputSignal = (byte)dr["RedLightOutputSignal"];
                            _setup.HornOutputSlot = (byte)dr["HornOutputSlot"];
                            _setup.HornOutputSignal = (byte)dr["HornOutputSignal"];

                            _setup.Printer1ActivateInputSlot = (byte)dr["Printer1ActivateInputSlot"];
                            _setup.Printer1ActivateInputSignal = (byte)dr["Printer1ActivateInputSignal"];
                            _setup.Printer1ActivateInputOnHigh = (byte)(((bool)dr["Printer1ActivateInputOnHigh"]) ? 1 : 0);

                            _setup.Printer2ActivateInputSlot = (byte)dr["Printer2ActivateInputSlot"];
                            _setup.Printer2ActivateInputSignal = (byte)dr["Printer2ActivateInputSignal"];
                            _setup.Printer2ActivateInputOnHigh = (byte)(((bool)dr["Printer2ActivateInputOnHigh"]) ? 1 : 0);

                            _setup.Printer1ActiveOutputSlot = (byte)dr["Printer1ActiveOutputSlot"];
                            _setup.Printer1ActiveOutputSignal = (byte)dr["Printer1ActiveOutputSignal"];

                            _setup.Printer2ActiveOutputSlot = (byte)dr["Printer2ActiveOutputSlot"];
                            _setup.Printer2ActiveOutputSignal = (byte)dr["Printer2ActiveOutputSignal"];

                            _setup.Gate1DownInputSlot = (byte)dr["Gate1DownInputSlot"];
                            _setup.Gate1DownInputSignal = (byte)dr["Gate1DownInputSignal"];
                            _setup.Gate1DownInputOnHigh = (byte)(((bool)dr["Gate1DownInputOnHigh"]) ? 1 : 0);

                            _setup.Gate2DownInputSlot = (byte)dr["Gate2DownInputSlot"];
                            _setup.Gate2DownInputSignal = (byte)dr["Gate2DownInputSignal"];
                            _setup.Gate2DownInputOnHigh = (byte)(((bool)dr["Gate2DownInputOnHigh"]) ? 1 : 0);

                            _setup.Gate2DownConfirmedInputSlot = (byte)dr["Gate2DownConfirmedInputSlot"];
                            _setup.Gate2DownConfirmedInputSignal = (byte)dr["Gate2DownConfirmedInputSignal"];
                            _setup.Gate2DownConfirmedInputOnHigh = (byte)(((bool)dr["Gate2DownConfirmedInputOnHigh"]) ? 1 : 0);

                            _setup.StartGreenOnTime = (short)dr["StartGreenOnTime"];
                            _setup.StartGreenOffTime = (short)dr["StartGreenOffTime"];
                            _setup.AlarmYellowOnTime = (short)dr["AlarmYellowOnTime"];
                            _setup.AlarmYellowOffTime = (short)dr["AlarmYellowOffTime"];
                            _setup.EmergencyRedOnTime = (short)dr["EmergencyRedOnTime"];
                            _setup.EmergencyRedOffTime = (short)dr["EmergencyRedOffTime"];
                            _setup.AlarmHornOnTime = (short)dr["AlarmHornOnTime"];
                            _setup.AlarmHornOffTime = (short)dr["AlarmHornOffTime"];

                            //_setupReceived = true;
                            //IcpGetOrAddTelegram(IcpTelegramTypes.SendSetup, null, true);
                        }
                    }
                    break;

                case DbCommandTypes.GetItems:
                    cmd = new OleDbCommand(sql, _con);
                    dr = DBTools.HandleBusyExecuteReader(cmd);

                    //for (int i = 0; i < _setup.LabelPositionsPrinter1.Length; i++)
                    //{
                    //    _setup.LabelPositionsPrinter1[i].IsEnabled = 0;
                    //}

                    //for (int i = 0; i < _setup.LabelPositionsPrinter2.Length; i++)
                    //{
                    //    _setup.LabelPositionsPrinter2[i].IsEnabled = 0;
                    //}

                    //for (int i = 0; i < _setup.BundlePositions.Length; i++)
                    //{
                    //    _setup.BundlePositions[i].IsEnabled = 0;
                    //}

                    int labelPositionPrinter1Index = 0;
                    int labelPositionPrinter2Index = 0;
                    int bundlePositionIndex = 0;

                    _setup.LabelPositionCountPrinter1 = 0;
                    _setup.LabelPositionCountPrinter2 = 0;
                    _setup.BundlePositionCount = 0;

                    int rowIndex = -1;
                    bool isOk = true;

                    if (dr.HasRows)
                    {
                        //while (labelPositionIndex < MAX_LABEL_POSITIONS || bundlePositionIndex < MAX_BUNDLE_POSITIONS)
                        {
                            //if (dr.Read())
                            while (dr.Read())
                            {
                                ++rowIndex;
                                try
                                {
                                    bool isEnabled = (bool)dr["IsEnabled"];
                                    if (isEnabled)
                                    {
                                        bool isLabelPosition = (bool)dr["IsLabelPosition"];
                                        bool isLabelPositionPrinter1 = isLabelPosition && ((byte)dr["PrinterNumber"]) == 1;
                                        bool isLabelPositionPrinter2 = isLabelPosition && ((byte)dr["PrinterNumber"]) == 2;

                                        if ((isLabelPositionPrinter1 && labelPositionPrinter1Index < MAX_LABEL_POSITIONS) ||
                                            (isLabelPositionPrinter2 && labelPositionPrinter2Index < MAX_LABEL_POSITIONS) ||
                                            (!isLabelPosition && bundlePositionIndex < MAX_BUNDLE_POSITIONS))
                                        {
                                            SetupPositionStruct setupPosition = new SetupPositionStruct();
                                            //setupPosition.CurrentStackerPosition = (byte)dr["StackerPositionId"];
                                            //setupPosition.Position = (byte)dr["LabelOrBundlePosition"];
                                            //setupPosition.IsEnabled = 1;
                                            setupPosition.EnteringInputSlot = (byte)dr["EnteringInputSlot"];
                                            setupPosition.EnteringInputSignal = (byte)dr["EnteringInputSignal"];
                                            setupPosition.EnteringInputOnHigh = (byte)(((bool)dr["EnteringInputOnHigh"]) ? 1 : 0);
                                            setupPosition.PositionInputSlot = (byte)dr["PositionInputSlot"];
                                            setupPosition.PositionInputSignal = (byte)dr["PositionInputSignal"];
                                            setupPosition.PositionInputOnHigh = (byte)(((bool)dr["PositionInputOnHigh"]) ? 1 : 0);
                                            setupPosition.ConveyorStoppedInputSlot = (byte)dr["ConveyorStoppedInputSlot"];
                                            setupPosition.ConveyorStoppedInputSignal = (byte)dr["ConveyorStoppedInputSignal"];
                                            setupPosition.ConveyorStoppedInputOnHigh = (byte)(((bool)dr["ConveyorStoppedInputOnHigh"]) ? 1 : 0);
                                            setupPosition.GateReadyInputSlot = (byte)dr["GateReadyInputSlot"];
                                            setupPosition.GateReadyInputSignal = (byte)dr["GateReadyInputSignal"];
                                            setupPosition.GateReadyInputOnHigh = (byte)(((bool)dr["GateReadyInputOnHigh"]) ? 1 : 0);
                                            setupPosition.ReceiveExpected = (short)dr["ReceiveExpected"];
                                            setupPosition.ReceiveTimeout = (short)dr["ReceiveTimeout"];
                                            setupPosition.VakuumOutputSlot = (byte)dr["VakuumOutputSlot"];
                                            setupPosition.VakuumOutputSignal = (byte)dr["VakuumOutputSignal"];
                                            setupPosition.EnteringTransportOutputSlot = (byte)dr["EnteringTransportOutputSlot"];
                                            setupPosition.EnteringTransportOutputSignal = (byte)dr["EnteringTransportOutputSignal"];
                                            setupPosition.TransportOutputSlot = (byte)dr["TransportOutputSlot"];
                                            setupPosition.TransportOutputSignal = (byte)dr["TransportOutputSignal"];
                                            setupPosition.BrakeOutputSlot = (byte)dr["BrakeOutputSlot"];
                                            setupPosition.BrakeOutputSignal = (byte)dr["BrakeOutputSignal"];
                                            setupPosition.DelayBeforeBrake = (short)dr["DelayBeforeBrake"];
                                            setupPosition.BrakeTime = (short)dr["BrakeTime"];
                                            setupPosition.StopDelay = (short)dr["StopDelay"];
                                            setupPosition.StartDelay = (short)dr["StartDelay"];
                                            setupPosition.StopDelayLeave = (short)dr["StopDelayLeave"];
                                            setupPosition.EncoderChannel = (byte)dr["EncoderChannel"];
                                            setupPosition.EncoderInputSlot = (byte)dr["EncoderInputSlot"];
                                            setupPosition.EncoderInputSignal = (byte)dr["EncoderInputSignal"];
                                            setupPosition.AlarmInputSlot = (byte)dr["AlarmInputSlot"];
                                            setupPosition.AlarmInputSignal = (byte)dr["AlarmInputSignal"];
                                            setupPosition.AlarmInputOnHigh = (byte)(((bool)dr["AlarmInputOnHigh"]) ? 1 : 0);
                                            setupPosition.UsePulseTiming = (byte)(((bool)dr["UsePulseTiming"]) ? 1 : 0);

                                            setupPosition.RejectOutputSlot = (byte)dr["RejectOutputSlot"];
                                            setupPosition.RejectOutputSignal = (byte)dr["RejectOutputSignal"];
                                            setupPosition.RejectPosInputSlot = (byte)dr["RejectPosInputSlot"];
                                            setupPosition.RejectPosInputSignal = (byte)dr["RejectPosInputSignal"];
                                            setupPosition.RejectPosInputOnHigh = (byte)(((bool)dr["RejectPosInputOnHigh"]) ? 1 : 0);

                                            setupPosition.SidePlatesOutputSlot = (byte)dr["SidePlatesOutputSlot"];
                                            setupPosition.SidePlatesOutputSignal = (byte)dr["SidePlatesOutputSignal"];
                                            setupPosition.SidePlatesMode = (byte)dr["SidePlatesMode"];

                                            if (isLabelPositionPrinter1)
                                            {
                                                _setup.LabelPositionsPrinter1[labelPositionPrinter1Index] = setupPosition;
                                                ++labelPositionPrinter1Index;
                                            }
                                            else if (isLabelPositionPrinter2)
                                            {
                                                _setup.LabelPositionsPrinter2[labelPositionPrinter2Index] = setupPosition;
                                                ++labelPositionPrinter2Index;
                                            }
                                            else
                                            {
                                                _setup.BundlePositions[bundlePositionIndex] = setupPosition;
                                                ++bundlePositionIndex;
                                            }
                                        }
                                        else
                                        {
                                            isOk = false;
                                            if (isLabelPositionPrinter1)
                                            {
                                                AddToLog(LogLevels.Warning, "To many label positions defined printer 1");
                                            }
                                            else if (isLabelPositionPrinter2)
                                            {
                                                AddToLog(LogLevels.Warning, "To many label positions defined printer 2");
                                            }
                                            else
                                            {
                                                AddToLog(LogLevels.Warning, "To many bundle positions defined");
                                            }
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    //_setupItems[index].IsEnabled = 0;
                                    AddToLog(LogLevels.Error, "Error in topSheet item rowIndex " + rowIndex.ToString() +
                                        " - sql=" + sql +
                                        " - " + ex.ToString());
                                }
                            }
                            //else
                            //{
                            //    _setupItems[index].IsEnabled = 0;
                            //}
                        }
                    }

                    _setup.LabelPositionCountPrinter1 = (byte)labelPositionPrinter1Index;
                    _setup.LabelPositionCountPrinter2 = (byte)labelPositionPrinter2Index;
                    _setup.BundlePositionCount = (byte)bundlePositionIndex;

                    if (labelPositionPrinter1Index > 0 && labelPositionPrinter1Index < MAX_LABEL_POSITIONS &&
                        (_setup.LabelPositionsPrinter1[labelPositionPrinter1Index - 1].BrakeTime == 1 ||
                        _setup.LabelPositionsPrinter1[labelPositionPrinter1Index - 1].BrakeTime == 2)
                        )
                    {
                        WaitTransportLabelUntilBundleArrived1 = true;
                    }
                    else
                    {
                        WaitTransportLabelUntilBundleArrived1 = false;
                    }

                    if (labelPositionPrinter2Index > 0 && labelPositionPrinter2Index < MAX_LABEL_POSITIONS &&
                        (_setup.LabelPositionsPrinter2[labelPositionPrinter2Index - 1].BrakeTime == 1 ||
                        _setup.LabelPositionsPrinter2[labelPositionPrinter2Index - 1].BrakeTime == 2)
                        )
                    {
                        WaitTransportLabelUntilBundleArrived2 = true;
                    }
                    else
                    {
                        WaitTransportLabelUntilBundleArrived2 = false;
                    }

                    if (IcpConnectionState == IcpConnectionStates.Connected)
                    {
                        IcpGetOrAddTelegram(IcpTelegramTypes.SendSetup, null, true);
                    }
                    _setupReceived = isOk;
                    break;
            }

            if (dr != null && !dr.IsClosed)
            {
                dr.Close();
            }
        }

        #region DbAddCommandToQueue, AddCommandFromDB

        private void DbAddCommandToQueue(DbCommandTypes commandType, string sql, object oParams, bool acceptDuplicates)
        {
            if (!acceptDuplicates)
            {
                lock (_lockDbCommandList)
                {
                    for (int i = _dbCommandList.Count - 1; i >= 0; i--)
                    {
                        if (commandType == _dbCommandList[i].dbCommandType)
                        {
                            _dbCommandList.RemoveAt(i);
                        }
                    }
                }
            }

            DbCommandStuct dbCommand = new DbCommandStuct();
            dbCommand.dbCommandType = commandType;
            dbCommand.sql = sql;
            dbCommand.oParams = oParams;

            lock (_lockDbCommandList)
            {
                _dbCommandList.Add(dbCommand);
            }
        }

        private void AddCommandFromDB(char ioCommand)
        {
            switch (ioCommand)
            {
                case 'B': //Send positions to IO
                    DbGetItems();
                    break;
                case 'D': //Send setup to IO
                    DbGetSetup();
                    break;
                case 'H': //Stop application
                    IcpGetOrAddTelegram(IcpTelegramTypes.SendStopApplication, null, true);
                    break;
            }
        }

        #endregion

        #region DbGetSetup

        private void DbGetSetup()
        {
            string sql = string.Format("select top 1 * from UniStack_TopSheetApplicatorSetupTable"
                + " where (StackerLineTableId = 0 or StackerLineTableId = {0})"
                + "   and (CurrentStackerPosition = 0 or CurrentStackerPosition = {1})"
                + " order by StackerLineTableId desc, CurrentStackerPosition desc"
                , _topSheetLineTableId, _currentStackerPositionId);

            DbAddCommandToQueue(DbCommandTypes.GetSetup, sql, null, false);
        }

        private void DbGetItems()
        {
            string sql = string.Format("select * from UniStack_GetTopSheetPositionSetup({0}, {1})"
                + " order by PrinterNumber, IsLabelPosition, LabelOrBundlePosition"
                , _topSheetLineTableId, _currentStackerPositionId);

            DbAddCommandToQueue(DbCommandTypes.GetItems, sql, null, false);
        }

        #endregion

        private void DbDisconnect()
        {
            try
            {
                DbConnectionState = DbConnectionStates.Disconnected;

                if (_con != null)
                {
                    try
                    {
                        if (_con.State == ConnectionState.Open)
                        {
                            _con.Close();
                        }
                    }
                    catch
                    {
                    }
                    finally
                    {
                        _con = null;
                    }
                }
            }
            catch
            {

            }
        }

        #endregion

        #region Printer communication

        private void PrinterControl()
        {
            try
            {
                AddToLog(LogLevels.Debug, "PrinterControl started");

                PrinterControlState = PrinterControlStates.Running;
                DateTime beforeLastPrint1 = DateTime.MinValue;
                DateTime beforeLastPrint2 = DateTime.MinValue;

                bool wasApplicatorReady = false;
                bool wasSnmpWatcher = false;

                while (UniDevLib.StaticInfo.IsRunning &&
                    IsStarted && _printerThread != null && _printerThread.ManagedThreadId == Thread.CurrentThread.ManagedThreadId)
                {
                    try
                    {
                        try
                        {
                            //try
                            //{
                            //    if (DateTime.UtcNow > _printerLastGetThreadId.AddSeconds(60))
                            //    {
                            //        _printerLastGetThreadId = DateTime.UtcNow;
                            //        _printerSystemThreadId = NativeMethods.GetCurrentThreadId();
                            //    }
                            //}
                            //catch
                            //{
                            //}

                            if (UseTwoPrinters)
                            {
                                if (ST_SNMPListeningPort > 0)
                                {
                                    lock (ST_lock_snmpWatcherTopSheetLines)
                                    {
                                        if (ST_snmpWatcher == null)
                                        {
                                            ST_snmpWatcher = this;
                                        }

                                        if (!wasSnmpWatcher && IsSnmpWatcher)
                                        {
                                            wasSnmpWatcher = true;
                                            AddToLog(LogLevels.Debug, "SnmpWatcher set to line " + TopSheetLineTableId.ToString());

                                            if (IsSnmpWatcher)
                                            {
                                                AddToLog(LogLevels.Debug, "Starting snmpWatcher thread on line "
                                                    + TopSheetLineTableId.ToString());
                                                try
                                                {
                                                    ST_snmpThread = new Thread(new ThreadStart(SnmpControl));
                                                    ST_snmpThread.Start();
                                                }
                                                catch (Exception ex)
                                                {
                                                    AddToLog(ex);
                                                }
                                            }
                                        }

                                        if (!IsSnmpWatcher)
                                        {
                                            wasSnmpWatcher = false;
                                        }
                                    }
                                }
                            }

                            //if (_status.StartStopState != 1)
                            //{
                            //    _topSheetQueue.Clear();
                            //    _topSheetPrinted.Clear();
                            //}

                            if (!_printer1.IsRunning)
                            {
                                _printer1.StartStatusCheck();
                            }

                            if (_useTwoPrinters && !_printer2.IsRunning)
                            {
                                _printer2.StartStatusCheck();
                            }

                            int printerNumber = 0;

                            //bool isPrinter1Ready = !UniDevLib.Gui.UserControls.UniLabel.PrinterVer2.HoldingPrint
                            //    && !UniDevLib.Gui.UserControls.UniLabel.PrinterVer2.IsPrinting;

                            //if (!UniDevLib.Gui.UserControls.UniLabel.PrinterVer2.IsPrinting)
                            //{
                            //    UniDevLib.Gui.UserControls.UniLabel.PrinterVer2.HoldPrint = !(
                            //        _status.LabelQueueCountPrinter1 < _setup.LabelPositionCountPrinter1 &&
                            //        _status.StartStopState == 1 &&
                            //            (
                            //            DateTime.UtcNow >
                            //            UniDevLib.Gui.UserControls.UniLabel.PrinterVer2.LastPrint.AddMilliseconds(5000)
                            //            ||
                            //                (
                            //                DateTime.UtcNow >
                            //                UniDevLib.Gui.UserControls.UniLabel.PrinterVer2.LastPrint.AddMilliseconds(2500) &&
                            //                _status.LabelQueueCountPrinter1 == 0
                            //                )
                            //            ));
                            //}

                            if (IcpConnectionState == IcpConnectionStates.Connected &&
                                (_status.StartStopState == 1 || _status.StartStopState == 2))
                            {
                                if (!wasApplicatorReady)
                                {
                                    if (_printer1 != null && _printer1.PrinterStatus != null)
                                    {
                                        _printer1.PrinterStatus.ReadyTime = DateTime.UtcNow;
                                    }
                                    if (_printer2 != null && _printer2.PrinterStatus != null)
                                    {
                                        _printer2.PrinterStatus.ReadyTime = DateTime.UtcNow;
                                    }
                                    wasApplicatorReady = true;
                                }
                            }
                            else
                            {
                                //if (DateTime.UtcNow > _topSheetLastOnline.AddSeconds(15))
                                {
                                    if (wasApplicatorReady)
                                    {
                                        if (_status.StartStopState == 0)
                                        {
                                            ResetApplicator(true);
                                        }
                                        else
                                        {
                                            ResetApplicator(false);
                                        }
                                    }

                                    wasApplicatorReady = false;

                                    //if (DateTime.UtcNow > _lastPrint1.AddHours(6) &&
                                    //    DateTime.UtcNow > _lastPrint2.AddHours(6) &&
                                    //    _nextBundleIndex > 11)
                                    //{
                                    //    _nextBundleIndex = 11;
                                    //    ResetApplicator(true);
                                    //}
                                }
                            }

                            if ((DateTime.UtcNow > _lastPrint1.AddHours(6) &&
                                DateTime.UtcNow > _lastPrint2.AddHours(6) &&
                                DateTime.UtcNow > _lastLCCMessage.AddHours(6) &&
                                _nextBundleIndex > 11) ||
                                _nextBundleIndex >= 65000
                                )
                            {
                                _lastPrint1 = DateTime.UtcNow;
                                _lastPrint2 = DateTime.UtcNow;

                                _nextBundleIndex = 11;
                                ResetApplicator(true);
                            }

                            bool isPrinter1Ready = CanPrintNow(1);
                            bool isPrinter2Ready = CanPrintNow(2);

                            //isPrinter1Ready = true;

                            if (isPrinter1Ready && isPrinter2Ready)
                            {
                                if (_lastPrint1 < _lastPrint2)
                                {
                                    printerNumber = 1;
                                }
                                else
                                {
                                    printerNumber = 2;
                                }
                            }
                            else if (isPrinter1Ready)
                            {
                                printerNumber = 1;
                            }
                            else if (isPrinter2Ready)
                            {
                                printerNumber = 2;
                            }

                            if (printerNumber > 0)
                            {

                                TopSheetItem topSheetItem = null;

                                //PrintBundle(10001);

                                //bool doInitialTestPrint = false;

                                lock (_lock_topSheetQueue)
                                {
                                    if (_topSheetQueue.Count > 0)
                                    {

                                        //if (!doInitialTestPrint)
                                        {
                                            topSheetItem = _topSheetQueue[0];
                                            _topSheetQueue.RemoveAt(0);
                                            topSheetItem.PrintedTime = DateTime.UtcNow;

                                            /*
                                            for (int i = _topSheetPrinted.Count - 1; i >= 0; i--)
                                            {
                                                if (_topSheetPrinted[i].BundleId == topSheetItem.BundleId)
                                                {
                                                    AddToLog(LogLevels.Debug,
                                                        string.Format("Duplicate bundle removed from printed list. BundleId={0}, Index={1}, New={2}"
                                                        , topSheetItem.BundleId, _topSheetPrinted[i].BundleIndex, topSheetItem.BundleIndex));

                                                    _topSheetPrinted.RemoveAt(i);
                                                }
                                            }
                                            */


                                            //AddToLogList("PrinterControl add to _topSheetPrinted bundleid: "
                                            //    + topSheetItem.BundleId.ToString()
                                            //    + " - bundleIndex: " + topSheetItem.BundleIndex.ToString());

                                            //_topSheetPrinted.Add(topSheetItem);
                                        }
                                    }
                                }

                                if (topSheetItem != null && topSheetItem.BundleIndex > 10)
                                {
                                    lock (_lock_topSheetPrinted)
                                    {
                                        _topSheetPrinted.Add(topSheetItem);
                                    }
                                }

                                //if (doInitialTestPrint)
                                //{
                                //    TestPrint(printerNumber);
                                //}

                                if (topSheetItem != null)
                                {
                                    topSheetItem.PrinterNumber = (byte)printerNumber;
                                    string documentName = "TopSheet(" + topSheetItem.BundleIndex.ToString()
                                        + ") " + topSheetItem.BundleId.ToString();

                                    UniDevLib.Gui.UserControls.UniLabel.ResultTypes printResult =
                                        PrintTopSheet(printerNumber, documentName, topSheetItem.BundleId);

                                    if (printResult == UniDevLib.Gui.UserControls.UniLabel.ResultTypes.Ok)
                                    {
                                        IcpGetOrAddTelegram(IcpTelegramTypes.SendPrintedBundleIndex, topSheetItem, true);

                                        if (printerNumber == 1)
                                        {
                                            _printQueueUpdated1 = false;
                                            _lastPrint1 = DateTime.UtcNow;
                                        }
                                        else
                                        {
                                            _printQueueUpdated2 = false;
                                            _lastPrint2 = DateTime.UtcNow;
                                        }

                                        string sql = string.Format("exec UniStack_TopSheetBundleAction {0}, {1}, {2}, {3}, {4}, {5}, {6}"
                                            , _gripperLineTableId
                                            , _topSheetLineTableId
                                            , topSheetItem.BundleId
                                            , topSheetItem.BundleIndex
                                            , (int)TopSheetActionEnum.AutomaticPrint
                                            , -1
                                            , printerNumber - 1
                                            );

                                        DbAddCommandToQueue(DbCommandTypes.ExecuteNonQuery, sql, null, true);

                                        //AddToLogList("PrinterControl print ok bundleid: "
                                        //    + topSheetItem.BundleId.ToString()
                                        //    + " - bundleIndex: " + topSheetItem.BundleIndex.ToString());

                                    }
                                    else
                                    {
                                        string sql = string.Format("exec UniStack_TopSheetBundleAction {0}, {1}, {2}, {3}, {4}, {5}, {6}"
                                            , _gripperLineTableId
                                            , _topSheetLineTableId
                                            , topSheetItem.BundleId
                                            , topSheetItem.BundleIndex
                                            , (int)TopSheetActionEnum.UnableToPrint
                                            , -1
                                            , printerNumber - 1
                                            );

                                        DbAddCommandToQueue(DbCommandTypes.ExecuteNonQuery, sql, null, true);


                                        //AddToLogList("ERROR: PrinterControl print failed bundleid: "
                                        //    + topSheetItem.BundleId.ToString()
                                        //    + " - bundleIndex: " + topSheetItem.BundleIndex.ToString());
                                    }
                                }
                            }

                            if (_doReconnectPrinter)
                            {
                                _doReconnectPrinter = false;
                                _printer1.StopStatusCheck();
                                if (_useTwoPrinters)
                                {
                                    _printer2.StopStatusCheck();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            AddToLog(LogLevels.Warning, ex.ToString());
                            try
                            {
                                _printer1.StopStatusCheck();
                                if (_useTwoPrinters)
                                {
                                    _printer2.StopStatusCheck();
                                }
                            }
                            catch
                            {
                            }

                            Thread.Sleep(1000);
                        }
                        finally
                        {
                            Thread.Sleep(100);
                        }
                    }
                    catch
                    {
                    }
                }

                if (_printerThread.ManagedThreadId != Thread.CurrentThread.ManagedThreadId)
                {
                    AddToLog(LogLevels.Debug, "PrinterControl old thread exited");
                }
                else
                {
                    if (_printer1 != null)
                    {
                        _printer1.StopStatusCheck();
                    }

                    if (_printer2 != null)
                    {
                        _printer2.StopStatusCheck();
                    }

                    PrinterControlState = PrinterControlStates.NotRunning;

                    AddToLog(LogLevels.Debug, "PrinterControl stopped");
                }
            }
            catch
            {
            }
        }

        public bool CanPrintNow(int printerNumber)
        {
            bool result = false;

            if (_setup.LabelPositionCountPrinter1 > 0 &&
                _setup.LabelPositionsPrinter1 != null &&
                _status.LabelPositionsPrinter1 != null &&
                (!UseTwoPrinters || (_setup.LabelPositionsPrinter2 != null && _status.LabelPositionsPrinter2 != null)) &&
                (
                    (printerNumber == 1 && _setup.LabelPositionCountPrinter1 > 0) ||
                    (printerNumber == 2 && _useTwoPrinters && _setup.LabelPositionCountPrinter2 > 0)
                )
                )
            {
                int posCount1 = _setup.LabelPositionCountPrinter1;
                int posCount2 = _setup.LabelPositionCountPrinter2;

                SetupPositionStruct setupLabelingPosition1 = _setup.LabelPositionsPrinter1[_setup.LabelPositionCountPrinter1 - 1];
                StatusPositionStruct statusLabelingPosition1 = _status.LabelPositionsPrinter1[_setup.LabelPositionCountPrinter1 - 1];

                SetupPositionStruct setupLabelingPosition2 = _useTwoPrinters ?
                    _setup.LabelPositionsPrinter2[_setup.LabelPositionCountPrinter2 - 1] : setupLabelingPosition1;
                StatusPositionStruct statusLabelingPosition2 = _useTwoPrinters ?
                    _status.LabelPositionsPrinter2[_setup.LabelPositionCountPrinter2 - 1] : statusLabelingPosition1;

                if (_useTwoPrinters)
                {
                    //if (_status.NextLabelingPrinterNumber != 1)
                    if ((_printer2.PrinterStatus.IsReadyToPrint && !PaperJam2 && !ThermoAlarm2 && !IsPaused2 && !IsPausedByOperator2) ||
                        //_status.NextLabelingPrinterNumber != 1
                        _status.NextLabelingPrinterNumber == 2 ||
                        _setup.ApplicatorType == 3//wamac ba
                        )
                    {
                        if (posCount1 > 0 && setupLabelingPosition1.BrakeTime != 1 && setupLabelingPosition1.BrakeTime != 2)
                        {
                            --posCount1;
                        }
                    }
                    //if (_status.NextLabelingPrinterNumber != 2)
                    if ((_printer1.PrinterStatus.IsReadyToPrint && !PaperJam1 && !ThermoAlarm1 && !IsPaused1 && !IsPausedByOperator1) ||
                        //_status.NextLabelingPrinterNumber != 2
                        _status.NextLabelingPrinterNumber == 1 ||
                        _setup.ApplicatorType == 3//wamac ba
                        )
                    {
                        if (posCount2 > 0 && setupLabelingPosition2.BrakeTime != 1 && setupLabelingPosition2.BrakeTime != 2)
                        {
                            --posCount2;
                        }
                    }
                }
                else if (_setup.ApplicatorType == 3)//wamac ba
                {
                    if (posCount1 > 0)
                    {
                        --posCount1;
                    }
                }

                //BrakeTime = 1 is used for keeping the label in the first position until the bundle has arrived
                if (posCount1 > 0)
                {
                    if ((setupLabelingPosition1.BrakeTime == 1 || setupLabelingPosition1.BrakeTime == 2) &&
                        statusLabelingPosition1.ExpectedBundleIndex == 0 &&
                        statusLabelingPosition1.CurrentBundleIndex == 0 &&
                        statusLabelingPosition1.State != 11)//label ready - waiting to apply
                    {
                        --posCount1;
                    }
                }

                if (posCount2 > 0)
                {
                    if ((setupLabelingPosition2.BrakeTime == 1 || setupLabelingPosition2.BrakeTime == 2) &&
                        statusLabelingPosition2.ExpectedBundleIndex == 0 &&
                        statusLabelingPosition2.CurrentBundleIndex == 0 &&
                        statusLabelingPosition2.State != 11)//label ready - waiting to apply
                    {
                        --posCount2;
                    }
                }

                int posCount = printerNumber == 1 ? posCount1 : posCount2;
                bool isPaperJam = printerNumber == 1 ? PaperJam1 : PaperJam2;
                bool isPaperJamOther = printerNumber == 1 ? PaperJam2 : PaperJam1;
                bool isThermoAlarm = printerNumber == 1 ? ThermoAlarm1 : ThermoAlarm2;
                bool isThermoAlarmOther = printerNumber == 1 ? ThermoAlarm2 : ThermoAlarm1;
                bool printQueueUpdated = printerNumber == 1 ? _printQueueUpdated1 : _printQueueUpdated2;
                bool isPaused = printerNumber == 1 ? IsPaused1 : IsPaused2;
                bool isPausedOther = printerNumber == 1 ? IsPaused2 : IsPaused1;
                bool isPausedByOperator = printerNumber == 1 ? IsPausedByOperator1 : IsPausedByOperator2;
                bool isPausedByOperatorOther = printerNumber == 1 ? IsPausedByOperator2 : IsPausedByOperator1;

                UniDevLib.Gui.UserControls.UniLabel.PrinterVer2 printer =
                    printerNumber == 1 ? _printer1 : _printer2;
                UniDevLib.Gui.UserControls.UniLabel.PrinterVer2 otherPrinter =
                    printerNumber == 1 ? _printer2 : _printer1;

                StatusPositionStruct[] labelPositions = printerNumber == 1 ?
                    _status.LabelPositionsPrinter1 : _status.LabelPositionsPrinter2;

                byte labelQueueCount = printerNumber == 1 ?
                    _status.LabelQueueCountPrinter1 : _status.LabelQueueCountPrinter2;

                DateTime lastPrint = printerNumber == 1 ? _lastPrint1 : _lastPrint2;

                if (
                        !isPaperJam &&
                        !isThermoAlarm &&
                        !isPaused &&
                        !isPausedByOperator &&
                        _status.StartStopState == 1 &&
                        printer.IsRunning &&
                        printer.PrinterStatus.IsReadyToPrint &&
                        (
                            labelPositions[0].State == 0
                            || !(otherPrinter != null && !isPaperJamOther && !isThermoAlarmOther &&
                                !isPausedOther && !isPausedByOperatorOther && 
                                otherPrinter.PrinterStatus.IsReadyToPrint)//print earlier if working alone
                        ) &&
                        labelQueueCount < posCount &&
                        (
                            DateTime.UtcNow > lastPrint.AddMilliseconds(2500) &&
                            printQueueUpdated
                        /*||
                        (
                            DateTime.UtcNow > lastPrint.AddMilliseconds(2000) &&
                            labelQueueCount == 1
                        )
                        ||
                        (
                            DateTime.UtcNow > lastPrint.AddMilliseconds(2000) &&
                            labelQueueCount == 0
                        )*/
                        )
                    )
                {
                    result = true;
                }
            }

            return result;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        private UniDevLib.Gui.UserControls.UniLabel.ResultTypes PrintTopSheet(
            int printerNumber, string documentName, ushort bundleId)//, TopSheetItem topSheetItem)
        {
            UniDevLib.Gui.UserControls.UniLabel.ResultTypes printResult =
                UniDevLib.Gui.UserControls.UniLabel.ResultTypes.Unknown;

            try
            {
                DataTable labelData = null;
                OleDbConnection labelDesignConnection = null;
                string labelGroup = null;
                string labelName = null;
                string paperFormat = null;
                int printingTimeoutSeconds = 10;

                try
                {
                    OleDbConnection con = new OleDbConnection(_connectionString);
                    con.Open();

                    string sql = string.Format("select * from UniStack_GetLabelNameBundle(-1, {0})"
                        , bundleId);
                    OleDbCommand cmd = new OleDbCommand(sql, con);
                    OleDbDataReader dr = cmd.ExecuteReader();
                    if (dr.Read())
                    {
                        labelGroup = (string)dr["LabelGroup"];
                        labelName = (string)dr["LabelName"];
                    }
                    dr.Close();

                    if (labelGroup != null && labelGroup.Length > 0 &&
                        labelName != null && labelName.Length > 0)
                    {
                        sql = string.Format("select * from UniStack_GetLabelData(-1, -1, -1, {0})"
                            , bundleId);

                        OleDbDataAdapter da = new OleDbDataAdapter(sql, con);
                        labelData = new DataTable();
                        da.FillSchema(labelData, SchemaType.Source);
                        da.Fill(labelData);
                    }

                    con.Close();
                }
                catch (Exception ex)
                {
                    printResult = UniDevLib.Gui.UserControls.UniLabel.ResultTypes.DataError;

                    _doReconnectDb = true;
                    AddToLog(LogLevels.Warning, ex.ToString());
                }

                try
                {
                    labelDesignConnection = new OleDbConnection(_connectionStringLabelDesign);
                    labelDesignConnection.Open();
                }
                catch (Exception ex)
                {
                    labelDesignConnection = null;
                    AddToLog(LogLevels.Warning, ex.ToString());
                }

                if (printResult == UniDevLib.Gui.UserControls.UniLabel.ResultTypes.Unknown)
                {
                    if (labelDesignConnection != null &&
                        labelDesignConnection.State == ConnectionState.Open)
                    {
                        if (printerNumber == 1)
                        {
                            printResult = _printer1.PrintWindowsPrinter(labelData, labelDesignConnection,
                                    labelGroup, labelName, documentName, paperFormat, printingTimeoutSeconds);
                        }
                        else
                        {
                            printResult = _printer2.PrintWindowsPrinter(labelData, labelDesignConnection,
                                    labelGroup, labelName, documentName, paperFormat, printingTimeoutSeconds);
                        }

                        UniDevLib.Gui.UserControls.UniLabel.LabelItem labelItem;
                        UniDevLib.Gui.UserControls.UniLabel.LabelItem.GetLabel(
                            labelDesignConnection, labelGroup, labelName, out labelItem);
                        //IcpGetOrAddTelegram(IcpTelegramTypes.SendPrintedBundleIndex, topSheetItem, true);
                    }
                    else
                    {
                        printResult = UniDevLib.Gui.UserControls.UniLabel.ResultTypes.ConnectionError;
                    }
                }
            }
            catch (Exception ex)
            {
                printResult = UniDevLib.Gui.UserControls.UniLabel.ResultTypes.Unknown;
                AddToLog(LogLevels.Warning, ex.ToString());

            }

            return printResult;
        }

        #endregion

        #region SNMP traps

        private void SnmpControl()
        {
            try
            {
                // Construct a socket and bind it to the trap manager port 162 
                IPEndPoint ipep;
                EndPoint ep;
                int inlen = -1;
                bool isError = false;
                AddToLog(LogLevels.Debug, "SnmpControl started");

                SnmpControlState = SnmpControlStates.Running;

                while (UniDevLib.StaticInfo.IsRunning &&
                    IsStarted && ST_snmpThread != null && ST_snmpThread.ManagedThreadId == Thread.CurrentThread.ManagedThreadId)
                {
                    try
                    {
                        try
                        {
                            if (ST_snmpSocket == null)
                            {
                                ST_snmpSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                                ipep = new IPEndPoint(IPAddress.Any, ST_SNMPListeningPort);//normal port = 162
                                ep = (EndPoint)ipep;
                                ST_snmpSocket.Bind(ep);
                                // Disable timeout processing. Just block until packet is received 
                                ST_snmpSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 0);
                            }

                            byte[] indata = new byte[16 * 1024];
                            // 16KB receive buffer int inlen = 0;
                            IPEndPoint peer = new IPEndPoint(IPAddress.Any, 0);
                            EndPoint inep = (EndPoint)peer;
                            try
                            {
                                SnmpControlState = SnmpControlStates.Listening;

                                inlen = ST_snmpSocket.ReceiveFrom(indata, ref inep);
                                Console.WriteLine("Received snmp trap, length=" + inlen.ToString());
                                AddToLog(LogLevels.Normal, "Received snmp trap, length=" + inlen.ToString());

                                if (inlen > 0)
                                {
                                    // Check protocol version int 
                                    //string hexString = DataConverting.HexConverting.ToHex(indata, inlen, " ");
                                    //Console.WriteLine(hexString);
                                    UniDevLib.Gui.UserControls.UniLabel.SnmpAlertMessage alertMessage =
                                        new UniDevLib.Gui.UserControls.UniLabel.SnmpAlertMessage();
                                    alertMessage.Registered = DateTime.UtcNow;

                                    int ver = SnmpPacket.GetProtocolVersion(indata, inlen);
                                    if (ver == (int)SnmpVersion.Ver1)
                                    {
                                        // Parse SNMP Version 1 TRAP packet 
                                        SnmpV1TrapPacket pkt = new SnmpV1TrapPacket();
                                        pkt.decode(indata, inlen);


                                        alertMessage.IpAddress = pkt.Pdu.AgentAddress.ToString();
                                        alertMessage.TrapSpecific = pkt.Pdu.Specific;

                                        Console.WriteLine("** SNMP Version 1 TRAP received from {0}:", inep.ToString());
                                        Console.WriteLine("*** Trap generic: {0}", pkt.Pdu.Generic);
                                        Console.WriteLine("*** Trap specific: {0}", pkt.Pdu.Specific);
                                        Console.WriteLine("*** Agent address: {0}", pkt.Pdu.AgentAddress.ToString());
                                        Console.WriteLine("*** Timestamp: {0}", pkt.Pdu.TimeStamp.ToString());
                                        Console.WriteLine("*** VarBind count: {0}", pkt.Pdu.VbList.Count);
                                        Console.WriteLine("*** VarBind content:");
                                        foreach (Vb v in pkt.Pdu.VbList)
                                        {
                                            Console.WriteLine("**** {0} {1}: {2}", v.Oid.ToString(), SnmpConstants.GetTypeName(v.Value.Type), v.Value.ToString());
                                            if (v.Oid.Length >= 13 &&
                                                v.Oid[0] == 1 &&
                                                v.Oid[1] == 3 &&
                                                v.Oid[2] == 6 &&
                                                v.Oid[3] == 1 &&
                                                (v.Oid[4] == 2 || v.Oid[4] == 4) &&
                                                v.Oid[5] == 1 &&
                                                (v.Oid[6] == 43 || v.Oid[6] == 1536) &&
                                                (v.Oid[7] == 18 || v.Oid[7] == 1) &&
                                                (v.Oid[8] == 1 || v.Oid[8] == 3) &&
                                                (v.Oid[9] == 1 || v.Oid[9] == 2)
                                                )
                                            {
                                                switch (v.Oid[10])
                                                {
                                                    case 1:
                                                        try
                                                        {
                                                            string value = v.Value.ToString();

                                                            if (value == "PaperJam\r\n")
                                                            {
                                                                alertMessage.TrapSpecific = 1;
                                                                alertMessage.AlertSeverityLevel = 3;
                                                                alertMessage.AlertTrainingLevel = 6;
                                                                alertMessage.AlertGroup = 8;
                                                                alertMessage.AlertGroupIndex = 2;
                                                                alertMessage.AlertLocation = 0;
                                                                isError = true;
                                                            }
                                                            else if (value == "CoverOpen\r\n")
                                                            {
                                                                alertMessage.TrapSpecific = 1;
                                                                alertMessage.AlertTrainingLevel = 3;
                                                                alertMessage.AlertGroup = 6;
                                                                alertMessage.AlertGroupIndex = 1;
                                                                alertMessage.AlertLocation = 0;
                                                                isError = true;
                                                            }
                                                            else
                                                            {
                                                                alertMessage.TrapSpecific = 0;
                                                                alertMessage.AlertSeverityLevel = 0;
                                                                alertMessage.TrapSpecific = 0;
                                                                alertMessage.AlertTrainingLevel = 0;
                                                                alertMessage.AlertGroup = 0;
                                                                alertMessage.AlertGroupIndex = 0;
                                                                alertMessage.AlertLocation = 0;
                                                            }
                                                        }
                                                        catch { }

                                                        break;
                                                    case 2:
                                                        alertMessage.AlertSeverityLevel = int.Parse(v.Value.ToString());
                                                        break;

                                                    case 3:
                                                        alertMessage.AlertTrainingLevel = int.Parse(v.Value.ToString());
                                                        break;

                                                    case 4:
                                                        alertMessage.AlertGroup = int.Parse(v.Value.ToString());
                                                        break;

                                                    case 5:
                                                        alertMessage.AlertGroupIndex = int.Parse(v.Value.ToString());
                                                        break;

                                                    case 6:
                                                        alertMessage.AlertLocation = int.Parse(v.Value.ToString());
                                                        break;

                                                    case 7:
                                                        alertMessage.AlertCode = int.Parse(v.Value.ToString());
                                                        break;
                                                }
                                            }
                                        }

                                        if (!isError)
                                        {
                                            alertMessage.TrapSpecific = 0;
                                            alertMessage.AlertSeverityLevel = 0;
                                            alertMessage.TrapSpecific = 0;
                                            alertMessage.AlertTrainingLevel = 0;
                                            alertMessage.AlertGroup = 0;
                                            alertMessage.AlertGroupIndex = 0;
                                            alertMessage.AlertLocation = 0;
                                        }

                                        isError = false;
                                        Console.WriteLine("** End of SNMP Version 1 TRAP data.");
                                    }
                                    else
                                    {
                                        // Parse SNMP Version 2 TRAP packet 
                                        SnmpV2Packet pkt = new SnmpV2Packet();
                                        pkt.decode(indata, inlen);
                                        Console.WriteLine("** SNMP Version 2 TRAP received from {0}:", inep.ToString());
                                        if ((SnmpSharpNet.PduType)pkt.Pdu.Type != PduType.V2Trap)
                                        {
                                            Console.WriteLine("*** NOT an SNMPv2 trap ****");
                                        }
                                        else
                                        {
                                            Console.WriteLine("*** Community: {0}", pkt.Community.ToString());
                                            Console.WriteLine("*** VarBind count: {0}", pkt.Pdu.VbList.Count);
                                            Console.WriteLine("*** VarBind content:");
                                            foreach (Vb v in pkt.Pdu.VbList)
                                            {
                                                Console.WriteLine("**** {0} {1}: {2}",
                                                   v.Oid.ToString(), SnmpConstants.GetTypeName(v.Value.Type), v.Value.ToString());
                                            }
                                            Console.WriteLine("** End of SNMP Version 2 TRAP data.");
                                        }
                                    }


                                    //int offset = 0;

                                    //UniDevLib.Gui.UserControls.UniLabel.SnmpAlertMessage alertMessage =
                                    //    new UniDevLib.Gui.UserControls.UniLabel.SnmpAlertMessage();
                                    //alertMessage.Registered = DateTime.UtcNow;
                                    //alertMessage.IpAddress = indata[29 + offset].ToString()
                                    //    + "." + indata[30 + offset].ToString()
                                    //    + "." + indata[31 + offset].ToString()
                                    //    + "." + indata[32 + offset].ToString();
                                    //alertMessage.TrapSpecific = indata[38 + offset];
                                    //alertMessage.AlertSeverityLevel = indata[84 + offset];
                                    //alertMessage.AlertTrainingLevel = indata[103 + offset];
                                    //alertMessage.AlertGroup = indata[122 + offset];
                                    //alertMessage.AlertGroupIndex = indata[141 + offset];
                                    //alertMessage.AlertLocation = indata[160 + offset];
                                    //alertMessage.AlertCode = indata[179 + offset] * 256 + indata[180 + offset];

                                    UniDevLib.Gui.UserControls.UniLabel.PrinterStatus printerStatus = null;

                                    if (alertMessage.IpAddress != null)
                                    {
                                        bool isFound = false;
                                        int index = -1;
                                        lock (ST_lock_snmpWatcherTopSheetLines)
                                        {
                                            while (!isFound && ++index < ST_snmpWatcherTopSheetLines.Count)
                                            {
                                                TopSheetMain topSheetLine = ST_snmpWatcherTopSheetLines[index];
                                                string ip1 = topSheetLine.PrinterIp1;
                                                string ip2 = topSheetLine.PrinterIp2;

                                                if (alertMessage.IpAddress.Equals(ip1))
                                                {
                                                    printerStatus = topSheetLine.Printer1.PrinterStatus;
                                                    isFound = true;
                                                }
                                                else if (alertMessage.IpAddress.Equals(ip2))
                                                {
                                                    printerStatus = topSheetLine.Printer2.PrinterStatus;
                                                    isFound = true;
                                                }
                                            }
                                        }
                                    }

                                    if (printerStatus != null)
                                    {
                                        printerStatus.AlertList.Add(alertMessage);
                                        while (printerStatus.AlertList.Count > 100)
                                        {
                                            printerStatus.AlertList.RemoveAt(0);
                                        }


                                        if (alertMessage.AlertSeverityLevel == 3 &&
                                            alertMessage.AlertTrainingLevel == 6 &&
                                            alertMessage.AlertGroup == 8 &&
                                            alertMessage.AlertGroupIndex == 2 &&
                                            alertMessage.AlertLocation == 0)
                                        {
                                            if (alertMessage.TrapSpecific == 1)
                                            {
                                                printerStatus.TrapPaperOk = false;
                                            }
                                            else// if (alertMessage.TrapSpecific == 2)
                                            {
                                                printerStatus.TrapPaperOk = true;
                                            }
                                        }
                                        else if (alertMessage.AlertSeverityLevel == 3 &&
                                                alertMessage.AlertTrainingLevel == 3 &&
                                                alertMessage.AlertGroup == 6 &&
                                                alertMessage.AlertGroupIndex == 1 &&
                                                alertMessage.AlertLocation == 0)
                                        {
                                            if (alertMessage.TrapSpecific == 1)
                                            {
                                                printerStatus.TrapDoorOk = false;
                                            }
                                            else// if (alertMessage.TrapSpecific == 2)
                                            {
                                                printerStatus.TrapDoorOk = true;
                                            }
                                        }
                                        else if (alertMessage.AlertSeverityLevel == 3 &&
                                                alertMessage.AlertTrainingLevel == 3 &&
                                                alertMessage.AlertGroup == 11 &&
                                                alertMessage.AlertGroupIndex == 1 &&
                                                alertMessage.AlertLocation == 0)
                                        {
                                            if (alertMessage.TrapSpecific == 1)
                                            {
                                                printerStatus.TrapTonerOk = false;
                                            }
                                            else// if (alertMessage.TrapSpecific == 2)
                                            {
                                                printerStatus.TrapTonerOk = true;
                                            }
                                        }
                                        else
                                        {
                                            if (alertMessage.TrapSpecific == 1)
                                            {
                                                printerStatus.TrapPrinterOk = false;
                                            }
                                            else
                                            {
                                                printerStatus.TrapPrinterOk = true;
                                            }
                                        }
                                    }

                                    /*
                                    Console.Write(indata[29].ToString());
                                    Console.Write("." + indata[30].ToString());
                                    Console.Write("." + indata[31].ToString());
                                    Console.Write("." + indata[32].ToString());
                                    Console.Write("-" + indata[38].ToString());
                                    Console.Write("-" + indata[84 + offset].ToString());
                                    Console.Write(" " + indata[103 + offset].ToString());
                                    Console.Write(" " + indata[122 + offset].ToString());
                                    Console.Write(" " + indata[141 + offset].ToString());
                                    Console.Write(" " + indata[160 + offset].ToString());
                                    Console.Write(" " + (indata[179 + offset] * 256 + indata[180 + offset]).ToString());
                                    Console.WriteLine();
                                    */
                                }
                                else
                                {
                                    if (inlen == 0)
                                        Console.WriteLine("Zero length packet received.");
                                }

                                //TopSheetPrinterStates test = ST_snmpWatcherTopSheetLines[1].TopSheetPrinterState2;
                            }
                            catch (Exception ex)
                            {
                                AddToLog(LogLevels.Warning, ex.ToString());
                                try
                                {
                                    if (ST_snmpSocket != null)
                                    {
                                        ST_snmpSocket.Close();
                                        ST_snmpSocket = null;
                                    }
                                }
                                catch
                                {
                                }
                                finally
                                {

                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            AddToLog(LogLevels.Warning, ex.ToString());
                        }
                    }
                    catch
                    {
                    }
                }

                if (ST_snmpThread.ManagedThreadId != Thread.CurrentThread.ManagedThreadId)
                {
                    AddToLog(LogLevels.Debug, "SnmpControl old thread exited");
                }
                else
                {

                    SnmpControlState = SnmpControlStates.NotRunning;
                    ST_snmpWatcher = null;
                    AddToLog(LogLevels.Debug, "SnmpControl stopped");
                }
            }
            catch
            {
            }
        }

        private void SnmpDisconnect()
        {
            AddToLog(LogLevels.Warning, "SnmpDisconnect");

            SnmpControlState = SnmpControlStates.Disconnected;

            if (ST_snmpSocket != null)
            {
                try
                {
                    ST_snmpSocket.Close();
                }
                catch
                {
                }
                finally
                {
                    ST_snmpSocket = null;
                }
            }
        }

        #endregion

        #region Tools

        //private string GetThreadInfo(Thread t, int systemThreadId)
        //{
        //    string result = "";

        //    try
        //    {
        //        if (t != null)
        //        {
        //            result = string.Format("Id={0}({1}), Alive={2}, Pri={3}, State={4}"
        //                , t.ManagedThreadId
        //                , systemThreadId
        //                , t.IsAlive
        //                , t.IsAlive ? t.Priority.ToString() : ""
        //                , t.ThreadState
        //                );
        //        }
        //        else
        //        {
        //            result = "Not in use";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        AddToLog(ex);
        //    }

        //    return result;
        //}


        private string GetLabelsOnApplicator(int printerNumber)
        {
            string result = "";

            StatusPositionStruct[] positions = printerNumber == 1 ?
                _status.LabelPositionsPrinter1 : _status.LabelPositionsPrinter2;
            int posCount = printerNumber == 1 ?
                _setup.LabelPositionCountPrinter1 : _setup.LabelPositionCountPrinter2;

            if (positions != null && posCount > 0 && posCount <= positions.Length)
            {//0:10003 1:10002 2:10001
                for (int i = 0; i < posCount; i++)
                {
                    ushort bundleIndex = positions[i].CurrentBundleIndex;

                    if (result.Length > 0)
                    {
                        result += " ";
                    }

                    result += i.ToString() + ":";
                    if (bundleIndex == 0)
                    {
                        result += "     ";
                    }
                    else if (bundleIndex < 10)
                    {
                        result += "xxxx" + bundleIndex.ToString();
                    }
                    else
                    {
                        //int listIndex;
                        TopSheetItem topSheetItem = GetTopSheetItemPrinted(bundleIndex, false);//, out listIndex);
                        if (topSheetItem != null)
                        {
                            result += topSheetItem.BundleId.ToString();
                        }
                        else
                        {
                            result += "?????";
                        }
                    }
                }
            }

            return result;
        }

        private bool CheckIfReadyForTestPrint(int printerNumber)
        {
            bool result = false;
            
            UniDevLib.Gui.UserControls.UniLabel.PrinterVer2 printer = null;

            if (printerNumber == 1)
            {
                printer = _printer1;
            }
            else if (printerNumber == 2)
            {
                printer = _printer2;
            }

            if (printer != null && printer.PrinterStatus != null)
            {
                if (//Status.StartStopState == 0 &&
                    //printer.LocalQueueCount == 0 &&
                    printer.PrinterStatus.IsConnected &&
                    printer.PrinterStatus.LastStatusCheck > DateTime.UtcNow.AddSeconds(-10))
                {
                    result = true;
                }
            }

            return result;
        }

        private TopSheetPrinterStates GetPrinterState(int printerNumber)
        {
            TopSheetPrinterStates result = TopSheetPrinterStates.PrinterUnknown;

            if (IsStarted)
            {
                UniDevLib.Gui.UserControls.UniLabel.PrinterVer2 printer = null;
                bool isPaused = false;
                bool isPausedByOperator = false;

                if (printerNumber == 1)
                {
                    printer = _printer1;
                    isPaused = IsPaused1;
                    isPausedByOperator = IsPausedByOperator1;
                }
                else if (printerNumber == 2)
                {
                    printer = _printer2;
                    isPaused = IsPaused2;
                    isPausedByOperator = IsPausedByOperator2;
                }

                if (printer != null && printer.PrinterStatus != null)
                {
                    if (printer.PrinterStatus.IsConnected &&
                        printer.PrinterStatus.LastStatusCheck > DateTime.UtcNow.AddSeconds(-10))
                    {
                        if (printer.PrinterStatus.PaperPercent == 0 ||
                            !printer.PrinterStatus.TrapPaperOk)
                        {
                            result = TopSheetPrinterStates.PrinterErrorPaper;
                        }
                        else if (printer.PrinterStatus.TonerPercent == 0 ||
                            !printer.PrinterStatus.TrapTonerOk)
                        {
                            result = TopSheetPrinterStates.PrinterErrorToner;
                        }
                        else if (printer.PrinterStatus.TonerPercent < 20)
                        {
                            result = TopSheetPrinterStates.PrinterTonerLow;
                        }
                        else if (printer.PrinterStatus.DoorOpen ||
                            !printer.PrinterStatus.TrapDoorOk)
                        {
                            result = TopSheetPrinterStates.PrinterErrorDoor;
                        }
                        else if (printer.PrinterStatus.IsError ||
                            !printer.PrinterStatus.TrapPrinterOk)
                        {
                            result = TopSheetPrinterStates.PrinterError;
                        }
                        else if (isPaused)
                        {
                            result = TopSheetPrinterStates.PrinterPaused;
                        }
                        else if (isPausedByOperator)
                        {
                            result = TopSheetPrinterStates.PrinterPausedByOperator;
                        }
                        else if (printer.PrinterStatus.IsInitializing)
                        {
                            result = TopSheetPrinterStates.PrinterInitializing;
                        }
                        else
                        {
                            result = TopSheetPrinterStates.PrinterReady;
                        }
                    }
                    else if (DateTime.UtcNow < printer.PrinterStatus.LastStatusCheck.AddSeconds(10))
                    {
                        result = TopSheetPrinterStates.PrinterOffline;
                    }
                    else
                    {
                        result = TopSheetPrinterStates.PrinterUnknown;
                    }
                }
            }

            return result;
        }

        private TopSheetItem GetTopSheetItemPrinted(int bundleIndex, bool doRemove)//, out int listIndex)
        {
            TopSheetItem result = null;

            bool isFound = false;
            int listIndex = -1;

            //lock (_topSheetQueue)
            lock (_lock_topSheetPrinted)
            {
                while (!isFound && ++listIndex < _topSheetPrinted.Count)
                {
                    TopSheetItem queueItem = _topSheetPrinted[listIndex];
                    if (queueItem.BundleIndex == bundleIndex)
                    {
                        result = queueItem;
                        isFound = true;

                        if (doRemove)
                        {
                            _topSheetPrinted.RemoveAt(listIndex);
                        }
                    }
                }
            }

            return result;
        }

        public byte GetPcStatusPrinter(int printerNumber, out bool isPausedByOperator)
        {
            byte result = 0;
            isPausedByOperator = false;

            UniDevLib.Gui.UserControls.UniLabel.PrinterVer2 printer =
                printerNumber == 1 ? _printer1 : _printer2;

            if (printer != null)
            {
                byte activeState = 0;

                if (IcpConnectionState == IcpConnectionStates.Connected)
                {
                    activeState = printerNumber == 1 ? _status.Printer1ActiveState : _status.Printer2ActiveState;
                }

                if (activeState == 2)//de-activated
                {
                    isPausedByOperator = true;

                    result = 2;
                }
                else if (activeState >= 20)//de-activating
                {
                    isPausedByOperator = true;

                    int printerQueueCount = printerNumber == 1 ?
                        _status.LabelQueueCountPrinter1 : _status.LabelQueueCountPrinter2;
                    DateTime lastPrint = printerNumber == 1 ? _lastPrint1 : _lastPrint2;

                    if (printerQueueCount == 0 && lastPrint < DateTime.UtcNow.AddSeconds(-12))
                    {
                        result = 2;//de-activated
                    }
                }
                //else if (activeState >= 10)//activating
                //{
                //    if (printer.PrinterStatus != null && printer.PrinterStatus.IsReadyToPrint)
                //    {
                //        result = 1;//activated
                //    }
                //    else
                //    {
                //        result = 2;
                //    }
                //}

                if (result == 0)
                {
                    if (printer.PrinterStatus != null && printer.PrinterStatus.IsReadyToPrint)
                    {
                        result = 1;
                    }
                }
            }

            return result;
        }

        public int GetDigitalInputState(int slot, int signal)
        {
            int result = -1;

            if (IcpConnectionState == IcpConnectionStates.Connected)
            {
                ushort[] slots = _status.DigitalISlot;
                if (slots != null)
                {
                    if (slot >= 0 && slot < 8 && slot < slots.Length &&
                        signal >= 0 && signal < 16)
                    {
                        ushort slotValue = slots[slot];
                        ushort signalBit = (ushort)Math.Pow(2, signal);
                        result = (slotValue & signalBit) > 0 ? 1 : 0;
                    }
                }
            }

            return result;
        }

        public int GetDigitalOutputState(int slot, int signal)
        {
            int result = -1;

            if (IcpConnectionState == IcpConnectionStates.Connected)
            {
                ushort[] slots = _status.DigitalOSlot;
                if (slot >= 0 && slot < 8 && slot < slots.Length &&
                    signal >= 0 && signal < 16)
                {
                    ushort slotValue = slots[slot];
                    ushort signalBit = (ushort)Math.Pow(2, signal);
                    result = (slotValue & signalBit) > 0 ? 1 : 0;
                }
            }

            return result;
        }

        public string[] GetStateValues()
        {
            string[] result = null;

            try
            {
                List<string> stateList = new List<string>();

                if (IcpConnectionState == IcpConnectionStates.Connected)
                {
                    string s;

                    s = string.Format("StartStopState = {0}, StartStopConvState = {1}, StartStopLastConvState = {2}"
                        , _status.StartStopState, _status.StartStopConvState, _status.StartStopLastConvState
                        );
                    stateList.Add(s);

                    s = string.Format("AlarmThermoState = {0} {1}"
                        , _status.AlarmThermoState1, _status.AlarmThermoState2
                        );
                    stateList.Add(s);

                    s = string.Format("PrinterActiveState = {0} {1}"
                        , _status.Printer1ActiveState, _status.Printer2ActiveState
                        );
                    stateList.Add(s);

                    s = string.Format("NextLabelingPrinterNumber = {0}, StackerExitState = {1}"
                        , _status.NextLabelingPrinterNumber, _status.StackerExitState
                        );
                    stateList.Add(s);

                    //s = string.Format("FunctionInputState1 = {0}, FunctionInputState2 = {1}, FunctionInputState3 = {2}"
                    //    , _status.FunctionInputState1, _status.FunctionInputState2, _status.FunctionInputState3
                    //    );
                    //stateList.Add(s);
                    s = string.Format("FunctionInputState1 = {0}, FunctionInputState2 = {1}, AlarmTemperatureState = {2}"
                        , _status.FunctionInputState1, _status.FunctionInputState2, _status.AlarmTemperatureState
                        );
                    stateList.Add(s);

                    s = string.Format("ApplicatorStopConveyor = {0}, ApplicationState = {1}"
                        , _status.ApplicatorStopConveyor, _status.ApplicationState
                        );
                    stateList.Add(s);

                    s = string.Format("ApplyingLabelIndex = {0}"
                        , _status.ApplyingLabelIndex
                        );
                    stateList.Add(s);

                    s = string.Format("RejectState = {0}"
                        , _status.RejectState
                        );
                    stateList.Add(s);

                    s = string.Format("ReadyToPreviousApplicatorState = {0}, ReadyToPreviousConveyorState = {1}, NextMachineReadyState = {2}"
                        , _status.ReadyToPreviousApplicatorState, _status.ReadyToPreviousConveyorState, _status.NextMachineReadyState
                        );
                    stateList.Add(s);

                    s = string.Format("DetectedStackerPosition = {0}, BundleQueueCount = {1}"
                        , _status.DetectedStackerPosition, _status.BundleQueueCount
                        );
                    stateList.Add(s);

                    s = string.Format("LabelQueueCountPrinter1 = {0}, LabelQueueCountPrinter2 = {1}"
                        , _status.LabelQueueCountPrinter1, _status.LabelQueueCountPrinter2
                        );
                    stateList.Add(s);

                    stateList.Add("");
                    stateList.Add("BundlePositions");
                    for (int index = 0; index < Setup.BundlePositionCount; index++)
                    {
                        s = string.Format("{0} S={1}, RS={2}, Ready={3}, Exp={4}, Cur={5}, Leaving={6}",
                            index,
                            Status.ConveyorPositions[index].State,
                            Status.ConveyorPositions[index].RejectPosState,
                            Status.ConveyorPositions[index].ReadyToReceive,
                            Status.ConveyorPositions[index].ExpectedBundleIndex,
                            Status.ConveyorPositions[index].CurrentBundleIndex,
                            Status.ConveyorPositions[index].LeavingBundleIndex
                            );
                        stateList.Add(s);
                    }

                    stateList.Add("");
                    stateList.Add("LabelPositions1");
                    for (int index = 0; index < Setup.LabelPositionCountPrinter1; index++)
                    {
                        s = string.Format("{0} S={1}, RS={2}, Ready={3}, Exp={4}, Cur={5}, Leaving={6}",
                            index,
                            Status.LabelPositionsPrinter1[index].State,
                            Status.LabelPositionsPrinter1[index].RejectPosState,
                            Status.LabelPositionsPrinter1[index].ReadyToReceive,
                            Status.LabelPositionsPrinter1[index].ExpectedBundleIndex,
                            Status.LabelPositionsPrinter1[index].CurrentBundleIndex,
                            Status.LabelPositionsPrinter1[index].LeavingBundleIndex
                        );
                        stateList.Add(s);
                    }

                    stateList.Add("");
                    stateList.Add("LabelPositions2");
                    for (int index = 0; index < Setup.LabelPositionCountPrinter2; index++)
                    {
                        s = string.Format("{0} S={1}, RS={2}, Ready={3}, Exp={4}, Cur={5}, Leaving={6}",
                            index,
                            Status.LabelPositionsPrinter2[index].State,
                            Status.LabelPositionsPrinter2[index].RejectPosState,
                            Status.LabelPositionsPrinter2[index].ReadyToReceive,
                            Status.LabelPositionsPrinter2[index].ExpectedBundleIndex,
                            Status.LabelPositionsPrinter2[index].CurrentBundleIndex,
                            Status.LabelPositionsPrinter2[index].LeavingBundleIndex
                        );
                        stateList.Add(s);
                    }
                }

                result = stateList.ToArray();
            }
            catch
            {
                result = null;
            }

            return result;
        }

        #endregion

        #region Logging

        public void AddToLog(Exception ex)
        {
            try
            {
                if (ex != null)
                {
                    AddToLog(LogLevels.Error, ex.ToString());
                }
            }
            catch
            {
            }
        }

        public void AddToLog(string message)
        {
            try
            {
                if (message != null)
                {
                    AddToLog(LogLevels.Normal, message);
                }
            }
            catch
            {
            }
        }

        public void AddToLog(LogLevels logLevel, string message)
        {
            try
            {
                if (_enableLogDebug || logLevel != LogLevels.Debug)
                {
                    if (_enableLogFile)
                    {
                        //Logging.AddToLog(logLevel, message);
                        lock (_lock_logItemList)
                        {
                            LogItem logItem = new LogItem();
                            logItem.LogLevel = logLevel;
                            logItem.Message = message;
                            logItem.LogTimeUTC = DateTime.UtcNow;

                            _logItemList.Add(logItem);
                        }
                    }
                }
            }
            catch
            {
            }
        }

        public void LogControl()
        {
            try
            {
                DateTime lastLogToFile = DateTime.MinValue;

                while (UniDevLib.StaticInfo.IsRunning && IsStarted &&
                    _logThread != null && _logThread.ManagedThreadId == Thread.CurrentThread.ManagedThreadId)
                {
                    try
                    {
                        //try
                        //{
                        //    if (DateTime.UtcNow > _logLastGetThreadId.AddSeconds(60))
                        //    {
                        //        _logLastGetThreadId = DateTime.UtcNow;
                        //        _logSystemThreadId = NativeMethods.GetCurrentThreadId();
                        //    }
                        //}
                        //catch
                        //{
                        //}

                        if (lastLogToFile < DateTime.UtcNow.AddSeconds(-10))
                        {
                            lastLogToFile = DateTime.UtcNow;

                            LogItem[] logItemArray = null;

                            lock (_lock_logItemList)
                            {
                                if (_logItemList.Count > 0)
                                {
                                    logItemArray = _logItemList.ToArray();
                                    _logItemList.Clear();
                                }
                            }

                            if (logItemArray != null)
                            {
                                Logging.AddToLog(_logFolder, logItemArray);
                            }
                        }
                    }
                    catch
                    { }
                    finally
                    {
                        Thread.Sleep(500);
                    }
                }
            }
            catch
            { }
        }

        #endregion

    }
}
