﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Data;
using System.Data.OleDb;
using System.Threading;
using System.Reflection;

namespace TopSheetControlLib
{
    public static class DBTools
    {
        #region Get connection string

        public static string GetConnectionStringSqlServer(string server, string database, bool useSSPI, string user, string password)
        {
            string result = null;

            string driver = "SQLOLEDB.1";//"SQLNCLI";

            if (useSSPI)
            {
                //result = String.Format("Provider={0};Data Source={1};Initial Catalog={2};Integrated Security=SSPI;",
                result = String.Format("Provider={0};Server={1};Database={2};Trusted_Connection=yes",
                    driver, server, database);
            }
            else
            {
                //result = String.Format("Provider={0};Data Source={1};Initial Catalog={2};UserId={3};Password={4};",
                result = String.Format("Provider={0};Server={1};Database={2};Uid={3};Pwd={4}",
                    driver, server, database, user, password);
            }

            return result;
        }

        #endregion

        #region ExecuteWithConnectionBusyHandling

        public static int HandleBusyFill(OleDbDataAdapter da, DataTable dt)
        {
            int intResult;
            OleDbDataReader drResult;
            object objectResult;
            DataTable dataTableResult;
            OleDbCommand oledbCommandResult;

            ExecuteWithConnectionBusyHandling(DatabaseEnums.OperationTypes.Fill,
                null, null, dt, da, SchemaType.Source, null, Guid.Empty, null, null, false,
                out intResult, out drResult, out objectResult, out dataTableResult, out oledbCommandResult);

            return intResult;
        }

        public static DataTable HandleBusyFillSchema(OleDbDataAdapter da, DataTable dt, SchemaType schemaType)
        {
            int intResult;
            OleDbDataReader drResult;
            object objectResult;
            DataTable dataTableResult;
            OleDbCommand oledbCommandResult;

            ExecuteWithConnectionBusyHandling(DatabaseEnums.OperationTypes.FillSchema,
                null, null, dt, da, schemaType, null, Guid.Empty, null, null, false,
                out intResult, out drResult, out objectResult, out dataTableResult, out oledbCommandResult);

            return dataTableResult;
        }

        public static DataTable HandleBusyGetSchemaTable(OleDbConnection con, System.Guid guid, object[] guidParameters)
        {
            int intResult;
            OleDbDataReader drResult;
            object objectResult;
            DataTable dataTableResult;
            OleDbCommand oledbCommandResult;

            bool executeResult = ExecuteWithConnectionBusyHandling(DatabaseEnums.OperationTypes.GetSchemaTable,
                con, null, null, null, SchemaType.Source, null, guid, guidParameters, null, false,
                out intResult, out drResult, out objectResult, out dataTableResult, out oledbCommandResult);

            return dataTableResult;
        }

        public static int HandleBusyExecuteNonQuery(OleDbCommand cmd)
        {
            return HandleBusyExecuteNonQuery(cmd, null);
        }

        public static int HandleBusyExecuteNonQuery(OleDbCommand cmd, OleDbParameter[] oleDbParameters)
        {
            int intResult;
            OleDbDataReader drResult;
            object objectResult;
            DataTable dataTableResult;
            OleDbCommand oledbCommandResult;

            bool executeResult = ExecuteWithConnectionBusyHandling(DatabaseEnums.OperationTypes.ExecuteNonQuery,
                null, cmd, null, null, SchemaType.Source, oleDbParameters, Guid.Empty, null, null, false,
                out intResult, out drResult, out objectResult, out dataTableResult, out oledbCommandResult);

            return intResult;
        }

        public static OleDbDataReader HandleBusyExecuteReader(OleDbCommand cmd)
        {
            int intResult;
            OleDbDataReader drResult;
            object objectResult;
            DataTable dataTableResult;
            OleDbCommand oledbCommandResult;

            bool executeResult = ExecuteWithConnectionBusyHandling(DatabaseEnums.OperationTypes.ExecuteReader,
                null, cmd, null, null, SchemaType.Source, null, Guid.Empty, null, null, false,
                out intResult, out drResult, out objectResult, out dataTableResult, out oledbCommandResult);

            return drResult;
        }

        public static object HandleBusyExecuteScalar(OleDbCommand cmd)
        {
            int intResult;
            OleDbDataReader drResult;
            object objectResult;
            DataTable dataTableResult;
            OleDbCommand oledbCommandResult;

            bool executeResult = ExecuteWithConnectionBusyHandling(DatabaseEnums.OperationTypes.ExecuteScalar,
                null, cmd, null, null, SchemaType.Source, null, Guid.Empty, null, null, false,
                out intResult, out drResult, out objectResult, out dataTableResult, out oledbCommandResult);

            return objectResult;
        }

        public static int HandleBusyUpdate(OleDbDataAdapter da, DataTable dt)
        {
            int intResult;
            OleDbDataReader drResult;
            object objectResult;
            DataTable dataTableResult;
            OleDbCommand oledbCommandResult;

            ExecuteWithConnectionBusyHandling(DatabaseEnums.OperationTypes.Update,
                null, null, dt, da, SchemaType.Source, null, Guid.Empty, null, null, false,
                out intResult, out drResult, out objectResult, out dataTableResult, out oledbCommandResult);

            return intResult;
        }

        public static OleDbCommand HandleBusyGetInsertCommand(OleDbCommandBuilder cmdBuilder, bool useColumnsForParameterNames)
        {
            int intResult;
            OleDbDataReader drResult;
            object objectResult;
            DataTable dataTableResult;
            OleDbCommand oledbCommandResult;

            ExecuteWithConnectionBusyHandling(DatabaseEnums.OperationTypes.GetInsertCommand,
                null, null, null, null, SchemaType.Source, null, Guid.Empty, null, cmdBuilder, useColumnsForParameterNames,
                out intResult, out drResult, out objectResult, out dataTableResult, out oledbCommandResult);

            return oledbCommandResult;
        }

        public static OleDbCommand HandleBusyGetUpdateCommand(OleDbCommandBuilder cmdBuilder, bool useColumnsForParameterNames)
        {
            int intResult;
            OleDbDataReader drResult;
            object objectResult;
            DataTable dataTableResult;
            OleDbCommand oledbCommandResult;

            ExecuteWithConnectionBusyHandling(DatabaseEnums.OperationTypes.GetUpdateCommand,
                null, null, null, null, SchemaType.Source, null, Guid.Empty, null, cmdBuilder, useColumnsForParameterNames,
                out intResult, out drResult, out objectResult, out dataTableResult, out oledbCommandResult);

            return oledbCommandResult;
        }

        private static bool ExecuteWithConnectionBusyHandling(DatabaseEnums.OperationTypes opType,
            OleDbConnection con, OleDbCommand cmd, DataTable dt,
            OleDbDataAdapter da, SchemaType schemaType, OleDbParameter[] oleDbParameters,
            System.Guid guid, object[] guidParameters, OleDbCommandBuilder cmdBuilder, bool useColumnsForParameterNames,
            out int intResult, out OleDbDataReader drResult, out object objectResult,
            out DataTable dataTableResult, out OleDbCommand oledbCommandResult)
        {
            bool result = false;
            intResult = -1;
            drResult = null;
            objectResult = null;
            dataTableResult = null;
            oledbCommandResult = null;
            int numRetries = 3;
            int numAttempts = 0;

            try
            {
                while (!result && numRetries >= ++numAttempts)
                {
                    try
                    {
                        if (numAttempts > 1)
                        {
                            if (cmd != null)
                            {
                                try { cmd.Prepare(); }
                                catch { }
                            }


                            if (da != null)
                            {
                                if (da.SelectCommand != null)
                                {
                                    try { da.SelectCommand.Prepare(); }
                                    catch { }
                                }
                                if (da.DeleteCommand != null)
                                {
                                    try { da.DeleteCommand.Prepare(); }
                                    catch { }
                                }
                                if (da.InsertCommand != null)
                                {
                                    try { da.InsertCommand.Prepare(); }
                                    catch { }
                                }
                                if (da.UpdateCommand != null)
                                {
                                    try { da.UpdateCommand.Prepare(); }
                                    catch { }
                                }
                            }

                            if (cmdBuilder != null && cmdBuilder.DataAdapter != null)
                            {
                                if (cmdBuilder.DataAdapter.SelectCommand != null)
                                {
                                    try { cmdBuilder.DataAdapter.SelectCommand.Prepare(); }
                                    catch { }
                                }
                                if (cmdBuilder.DataAdapter.DeleteCommand != null)
                                {
                                    try { cmdBuilder.DataAdapter.DeleteCommand.Prepare(); }
                                    catch { }
                                }
                                if (cmdBuilder.DataAdapter.InsertCommand != null)
                                {
                                    try { cmdBuilder.DataAdapter.InsertCommand.Prepare(); }
                                    catch { }
                                }
                                if (cmdBuilder.DataAdapter.UpdateCommand != null)
                                {
                                    try { cmdBuilder.DataAdapter.UpdateCommand.Prepare(); }
                                    catch { }
                                }
                            }
                        }

                        switch (opType)
                        {
                            case DatabaseEnums.OperationTypes.ExecuteReader:
                                cmd.CommandTimeout = 900;
                                drResult = cmd.ExecuteReader();
                                break;
                            case DatabaseEnums.OperationTypes.ExecuteNonQuery:
                                if (oleDbParameters != null && oleDbParameters.Length > 0)
                                {
                                    cmd.Parameters.AddRange(oleDbParameters);
                                }
                                intResult = cmd.ExecuteNonQuery();
                                break;
                            case DatabaseEnums.OperationTypes.Fill:
                                intResult = da.Fill(dt);
                                break;
                            case DatabaseEnums.OperationTypes.FillSchema:
                                dataTableResult = da.FillSchema(dt, schemaType);
                                break;
                            case DatabaseEnums.OperationTypes.ExecuteScalar:
                                objectResult = cmd.ExecuteScalar();
                                break;
                            case DatabaseEnums.OperationTypes.GetSchemaTable:
                                dataTableResult = con.GetOleDbSchemaTable(guid, guidParameters);
                                break;
                            case DatabaseEnums.OperationTypes.Update:
                                intResult = da.Update(dt);
                                break;
                            case DatabaseEnums.OperationTypes.GetInsertCommand:
                                oledbCommandResult = cmdBuilder.GetInsertCommand(useColumnsForParameterNames);
                                break;
                            case DatabaseEnums.OperationTypes.GetUpdateCommand:
                                oledbCommandResult = cmdBuilder.GetUpdateCommand(useColumnsForParameterNames);
                                break;
                        }

                        result = true;
                    }
                    catch (Exception ex)
                    {
                        if (!ex.ToString().ToLower().Contains("connection is busy") &&
                            !ex.ToString().ToLower().Contains("tilkoblingen er opptatt") &&
                            !ex.ToString().ToLower().Contains("command was not prepared") &&
                            !ex.ToString().ToLower().Contains("kommandoen var ikke forberedt") &&
                            !ex.ToString().ToLower().Contains("deadlock")
                            )
                        {
                            throw;
                        }
                        System.Threading.Thread.Sleep(50);
                    }

                }
            }
            catch
            {
                result = false;
                try
                {
                    con.Close();
                }
                catch
                {
                }
            }


            return result;
        }

        #endregion
    }
}
