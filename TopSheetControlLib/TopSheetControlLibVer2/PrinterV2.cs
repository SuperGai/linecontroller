﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.Drawing.Printing;
using System.Linq;
using System.Net.NetworkInformation;
using System.Printing;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TopSheetControlLib
{
    public class PrinterV2
    {
        //public long GlobalTick;
        public DateTime PrinterReadyTime;
        //public static DateTime Printer2ReadyTime;

        public int NumberOfPrintJobs;
        private readonly object _lock_static_PrinterQueue = new object();
        private readonly object _lock_printQueueList = new object();
        private readonly PrinterTypes _printerType;
        private string _printerName;
        private string _ipAddress;
        private readonly int _port;
        private List<PrintQueueItem> _printQueueList;
        public PrinterStatus _printerStatus { get; set; }
        private Thread _statusCheckThread;
        private bool _isRunning;
        private readonly DateTime _lastConnect;

        public string DefaultLabelGroup;
        public string DefaultLabelName;


        public PrinterV2(PrinterTypes printerType, string printerName, string ipAddress, int port)
        {
            _printerType = printerType;
            _printerName = printerName;
            _ipAddress = ipAddress;
            _port = port;
            _printQueueList = new List<PrintQueueItem>();
            _printerStatus = new PrinterStatus();
            _lastConnect = DateTime.MinValue;
            //StartStatusCheck();
        }

        public int LocalQueueCount
        {
            get
            {
                int count;
                lock (_lock_printQueueList)
                    count = _printQueueList.Count;
                return count;
            }
        }

        public bool IsRunning
        {
            get
            {
                return _statusCheckThread != null && _isRunning;
            }
        }

        public PrinterStatus PrinterStatus
        {
            get
            {
                return _printerStatus;
            }
        }

        public void StartStatusCheck()
        {
            try
            {
                if (_statusCheckThread != null && _isRunning)
                {
                    StopStatusCheck();
                    Thread.Sleep(5000);
                }

                _statusCheckThread = _printerType == PrinterTypes.WindowsPrinter ? new Thread(new ThreadStart(StatusPollWindowsPrinter)) : null;

                if (_statusCheckThread != null)
                {
                    _isRunning = true;
                    _statusCheckThread.Start();
                }
            }
            catch
            {
            }
        }

        public void StopStatusCheck()
        {
            try
            {
               _isRunning = false;
               _statusCheckThread.Interrupt();
            }
            catch
            {
            }
        }

        private void StatusPollWindowsPrinter()
        {
            try
            {
                Ping ping = new Ping();
                PingOptions options = new PingOptions
                {
                    DontFragment = true
                };
                byte[] bytes = Encoding.ASCII.GetBytes("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
                DateTime dateTime = DateTime.MinValue;
                while (_isRunning && _statusCheckThread != null && _statusCheckThread.ManagedThreadId == Thread.CurrentThread.ManagedThreadId)
                {
                    try
                    {
                        _printerStatus.LastStatusCheck = DateTime.Now;
                        if (DateTime.Now > dateTime.AddSeconds(10.0))
                        {
                            dateTime = DateTime.Now;
                            PrintQueue printQueue = (PrintQueue)null;
                            lock (_lock_static_PrinterQueue)
                            {
                                if (printQueue == null)
                                {
                                    LocalPrintServer localPrintServer = new LocalPrintServer();
                                    printQueue = _printerName == null || _printerName.Trim().Length <= 0 ? localPrintServer.DefaultPrintQueue : localPrintServer.GetPrintQueue(_printerName);
                                    
                                    NumberOfPrintJobs = printQueue.NumberOfJobs;
                                    
                                }
                            }
                        }
                    }
                    catch
                    {
                    }
                    finally
                    {
                        Thread.Sleep(10000);
                    }
                }
            }
            catch
            {
            }

            try
            {
                if (_statusCheckThread == null || _statusCheckThread.ManagedThreadId != Thread.CurrentThread.ManagedThreadId)
                    return;
                _isRunning = false;
            }
            catch
            {
            }
            
        }


        public enum PrinterTypes
        {
            Unknown,
            WindowsPrinter,
        }

        public LabelItem.ResultTypes PrintWindowsPrinter(DataTable labelData, 
            string labelGroup, string labelName, string documentName, 
            string paperFormat, TopSheetMain.TopSheetTypes _topSheetType)
        {
            LabelItem.ResultTypes resultTypes = LabelItem.GetLabel(labelGroup, labelName, out Dictionary<string, List<string>>  labelItem);
            DefaultLabelGroup = labelGroup;
            DefaultLabelName = labelName;
            try
            {
                if (resultTypes == LabelItem.ResultTypes.Ok)
                {
                    resultTypes = PrintWindowsPrinter(labelData, labelItem, labelName, documentName, paperFormat, _topSheetType);
                }
                return resultTypes;
            }
            catch
            {
            }
            return resultTypes;

        }

        private LabelItem.ResultTypes PrintWindowsPrinter(DataTable labelData, 
            Dictionary<string, List<string>> labelItem,  
            string labelName, string documentName, string paperFormat, 
             TopSheetMain.TopSheetTypes _topSheetType)
        {
            LabelItem.ResultTypes resultTypes = LabelItem.ResultTypes.Unknown;
            try
            {
                if (labelItem != null)
                {
                    resultTypes = LabelItem.ResultTypes.Ok;
                    PrintDocument printDocument = new PrintDocument();
                    if (paperFormat == null || paperFormat.Trim().Length == 0)
                    {
                        paperFormat = "A4";
                    }
                    if (documentName == null || documentName.Length == 0)
                    {
                        documentName = labelName;
                    }
                    printDocument.DefaultPageSettings.PaperSize = new PaperSize(paperFormat, 0, 0);
                    printDocument.DocumentName = documentName;
                    printDocument.PrintController = new StandardPrintController();

                    if (_printerName != null && _printerName.Length > 0)
                    {
                        printDocument.PrinterSettings.PrinterName = _printerName;
                    }
                    lock (_lock_printQueueList)
                    {
                        _printQueueList.Add(new PrintQueueItem(labelData, labelItem, _topSheetType));

                    }
                    DateTime Now = DateTime.Now;
                    printDocument.PrintPage += new PrintPageEventHandler(PrtDoc_PrintPage);
                    printDocument.Print();

                    return resultTypes;
                }
            }
            catch
            {
            }
            return resultTypes;
        }

        public LabelItem.ResultTypes ActivateWindowsPrinter(string documentName, string paperFormat,
            int printingTimeoutSeconds)
        {
            LabelItem.ResultTypes resultTypes = LabelItem.ResultTypes.Unknown;
            try
            {
                resultTypes = LabelItem.ResultTypes.Ok;
                PrintDocument printDocument = new PrintDocument();
                if (paperFormat == null || paperFormat.Trim().Length == 0)
                {
                    paperFormat = "A4";
                }
                if (documentName == null || documentName.Length == 0)
                {
                    documentName = "WakeUp";
                }
                printDocument.DefaultPageSettings.PaperSize = new PaperSize(paperFormat, 0, 0);
                printDocument.DocumentName = documentName;
                printDocument.PrintController = new StandardPrintController();

                if (_printerName != null && _printerName.Length > 0)
                {
                    printDocument.PrinterSettings.PrinterName = _printerName;
                }
                    
                DateTime Now = DateTime.Now;
                printDocument.Print();

                return resultTypes;
                
            }
            catch
            {
            }
            return resultTypes;
        }

        private void PrtDoc_PrintPage(object sender, PrintPageEventArgs e)
        {
            try
            {
                PrintQueueItem printQueueItem = null;
                lock (_lock_printQueueList)
                {
                    if (_printQueueList.Count > 0)
                    {
                        printQueueItem = _printQueueList[0];
                        _printQueueList.RemoveAt(0);
                    }
                }
                if (printQueueItem != null)
                {
                    LabelItem.ResultTypes printResult =
                           LabelItem.ResultTypes.Unknown;
                    DataTable labelData = printQueueItem.LabelData;
                    Dictionary<string, List<string>> labelItem = printQueueItem.LabelItem;
                    TopSheetMain.TopSheetTypes topsheetType = printQueueItem.TopSheetTypes;

                    string labelGroup = DefaultLabelGroup;//"Milano";
                    string labelName = DefaultLabelName;//"3";
                    if (labelData != null && labelItem != null)
                    {
                        printResult = LabelItem.DrawLabel(e.Graphics, labelData, labelItem, topsheetType);
                    }
                }
            }
            catch
            {
            }
        }
    }

    public class PrintQueueItem
    {
        public PrintQueueItem(DataTable labelData, Dictionary<string, List<string>> labelItem, TopSheetMain.TopSheetTypes topSheetTypes)
        {
            LabelData = labelData;
            LabelItem = labelItem;
            TopSheetTypes = topSheetTypes;
        }
        public DataTable LabelData { get; set; }
        public Dictionary<string, List<string>> LabelItem { get; set; }
        public TopSheetMain.TopSheetTypes TopSheetTypes { get; set; }
    }

    public class SnmpAlertMessage
    {
        public DateTime Registered { get; set; }
        public string IpAddress { get; set; }
        public int TrapSpecific { get; set; }
        public int AlertSeverityLevel { get; set; }
        public int AlertTrainingLevel { get; set; }
        public int AlertGroup { get; set; }
        public int AlertGroupIndex { get; set; }
        public int AlertLocation { get; set; }
        public int AlertCode { get; set; }
    }
}
