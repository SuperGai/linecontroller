﻿using System;

namespace TopSheetControlLib
{
    public class TopSheetItem
    {
        public byte AblId { get; set; }
        public short TitleGroupId { get; set; }
        public string IssueDateString { get; set; }
        public ushort RouteSortIndex { get; set; }
        public ushort BundleSortIndex { get; set; }
        public DateTime EjectTime { get; set; }
        public uint BundleId { get; set; }

        public uint BundleIndex { get; set; }
        public TopSheetMain.EnumBundleInRoute FirstLastBundle { get; set; }
        public bool IsSTDBundle { get; set; }

        public byte RouteModeId { get; set; }
        public bool IsNoTopsheet { get; set; }
        public byte PrinterNumber { get; set; }
        //public DateTime AddedTime { get; set; }
        //public DateTime PrintedTime { get; set; }
        //public DateTime EjectedTime { get; set; }
        public int HandledCode { get; set; }
        public byte LabelState { get; set; } // 0: Normal; 50: Blank Copy; 51: Reject to Hand Fly
        public TopSheetMain.TopSheetTypes TopSheetType { get; set; }
    }
}
