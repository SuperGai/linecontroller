﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LineControllerLib
{
    public class UTRMain
    {

        private Thread _icpUTRThread;
        private const byte SOH = 1;
        private const byte EOT = 4;

        #region Structs/ Setup classes
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct StatusStruct
        {

            public byte SOH;
            public byte TelegramId;
            public byte Command;
            public ushort Length;

            public int lBruttoCounter;
            public int lNettoCounter;

            public byte EOT;
        }

        #endregion
        public enum EnumIcpConnectionStates
        {
            Unknown = 0,
            NotRunning = 1,
            Disconnected = 2,
            Connecting = 3,
            Connected = 4
        }

        public enum IcpTelegramTypes
        {
            RequestStatus,
            Reset
        }

        public StatusStruct _status;
        private readonly int _statusStructSize;

        public bool IsStarted { get; set; }
        public EnumIcpConnectionStates IcpConnectionState { get; set; }
        public byte GripperLineId;
        private TcpClient _tcpClientIcp;
        private NetworkStream _networkStreamIcp;
        private BinaryReader _binaryReaderIcp;
        private BinaryWriter _binaryWriterIcp;
        private List<byte[]> _telegramListIcp;
        private byte _pcStatus;
        private byte _telegramId;
        private byte[] _telegramRequestStatus;
        private string _ioIp;
        private int _ioPort;
        private readonly object _lockTelegram = new object();
        private readonly object _lockDbCommandList = new object();
        private readonly object _lockTelegramListIcp = new object();

        public int BruttoCounter
        {
            get
            {
                return _status.lBruttoCounter;
            }
        }

        public int NettoCounter
        {
            get
            {
                return _status.lNettoCounter;
            }
        }

        #region Construct
        public UTRMain(string[] ConnectionString, byte _LineId, string ioIpGripperLine)
        {
            GripperLineId = _LineId;
            _ioIp = ioIpGripperLine;
            _ioPort = 10000;
            IcpConnectionState = EnumIcpConnectionStates.NotRunning;
            _telegramListIcp = new List<byte[]>();
            _telegramRequestStatus = null;
            _pcStatus = 3;//no label

            _status = new StatusStruct();
            _statusStructSize = Marshal.SizeOf(typeof(StatusStruct));

        }
        #endregion

        public void Start()
        {
            try
            {
                if (IsStarted)
                {
                    Stop();
                }

                IsStarted = true;

                _icpUTRThread = new Thread(new ThreadStart(IcpUTRControl));
                _icpUTRThread.Start();

            }
            catch (Exception ex)
            {
                Logging.AddToLog(LogLevels.Error, "Start UTR Control Error: " + ex.ToString());
            }

        }

        public void Stop()
        {
            try
            {

                IsStarted = false;

                int counter = 0;
                while (++counter < 20 && IcpConnectionState != EnumIcpConnectionStates.NotRunning)
                {
                    Thread.Sleep(100);
                }
            }
            catch
            {

            }

            if (IcpConnectionState != EnumIcpConnectionStates.NotRunning)
            {
                try
                {
                    if (_icpUTRThread != null)
                    {
                        _icpUTRThread.Interrupt();
                    }
                }
                catch
                {
                }
            }
        }

        private void IcpUTRControl()
        {
            IcpConnectionState = EnumIcpConnectionStates.Disconnected;
            byte[] response = null;
            int readLength = _statusStructSize;
            bool statusReceived = false;
            bool readOK = false;
            DateTime CurTime = DateTime.Now;

            while (IsStarted && _icpUTRThread != null && _icpUTRThread.ManagedThreadId == Thread.CurrentThread.ManagedThreadId)
            {
                try
                {

                    try
                    {
                        if (_tcpClientIcp == null || _tcpClientIcp.Client == null || !_tcpClientIcp.Client.Connected)
                        {

                            IcpConnectionState = EnumIcpConnectionStates.Disconnected;
                            //_isWaitingToSendPrintToIcp = false;

                            if (_ioIp != null && _ioIp.Length > 0)
                            {
                                Logging.AddToLog(LogLevels.Debug, "UTR Icp connecting ...");

                                _tcpClientIcp = new TcpClient
                                {
                                    ReceiveTimeout = 1000,
                                    SendTimeout = 1000
                                };

                                //_tcpClientIcp.LingerState = new LingerOption(true, 0);
                                IcpConnectionState = EnumIcpConnectionStates.Connecting;
                                _tcpClientIcp.Connect(_ioIp, _ioPort);

                                _networkStreamIcp = _tcpClientIcp.GetStream();
                                _networkStreamIcp.ReadTimeout = 1000;
                                _networkStreamIcp.WriteTimeout = 1000;

                                _binaryReaderIcp = new BinaryReader(_networkStreamIcp);
                                _binaryWriterIcp = new BinaryWriter(_networkStreamIcp);

                                IcpConnectionState = EnumIcpConnectionStates.Connected;
                                Logging.AddToLog(LogLevels.Debug, "UTR Icp connected");

                                int bytesCleared = 0;
                                try
                                {
                                    while (_tcpClientIcp.Client != null && _tcpClientIcp.Client.Connected)
                                    {
                                        _binaryReaderIcp.ReadByte();
                                        ++bytesCleared;
                                    }
                                }
                                catch
                                {
                                }

                            }
                        }

                        if (_tcpClientIcp != null && _tcpClientIcp.Client != null && _tcpClientIcp.Client.Connected)
                        {
                            byte[] telegram = null;

                            if (statusReceived && IcpTelegramList_GetCount() > 0)
                            {
                                telegram = IcpTelegramList_GetNext();
                            }

                            if (telegram == null)
                            {
                                telegram = IcpGetOrAddTelegram(IcpTelegramTypes.RequestStatus, null, true);
                            }

                            try
                            {
                                _binaryWriterIcp.Write(telegram);
                                _binaryWriterIcp.Flush();
                            }
                            catch (Exception ex)
                            {
                                Logging.AddToLog(LogLevels.Warning, "UTR ICP Write Error " + ": " + ex.ToString());
                            }

                            readOK = false;
                            CurTime = DateTime.Now;

                            int ReadErrorTime = 0;

                            while (!readOK && CurTime.AddSeconds(4) > DateTime.Now)
                            {
                                try
                                {
                                    response = _binaryReaderIcp.ReadBytes(readLength);

                                    if (response != null
                                        && response.Length == readLength
                                        && response[0] == telegram[0]
                                        && response[1] == telegram[1]
                                        && response[2] == telegram[3]
                                        && response[readLength - 1] == 4)
                                    {
                                        readOK = true;
                                    }
                                    else
                                    {
                                        _binaryWriterIcp.Write(telegram);
                                        _binaryWriterIcp.Flush();
                                        Logging.AddToLog(LogLevels.Warning, "UTR_binaryWriterIcp 2 Telegram[1]: " + telegram[1]);
                                        Thread.Sleep(10);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    ReadErrorTime++;
                                    Logging.AddToLog(LogLevels.Warning, "UTR ICP Response Error " + ReadErrorTime + ": " + ex.ToString() + " , Try to read again!");
                                    Thread.Sleep(10);
                                }
                            }

                            if (response != null
                                && response.Length == readLength
                                && response[0] == telegram[0]
                                && response[1] == telegram[1]
                                && response[2] == telegram[3]
                                && response[readLength - 1] == 4)
                            {
                                //lock (_lockStatus)
                                {
                                    GCHandle gch = GCHandle.Alloc(response, GCHandleType.Pinned);
                                    try
                                    {
                                        _status = (StatusStruct)Marshal.PtrToStructure(gch.AddrOfPinnedObject(), typeof(StatusStruct));


                                        statusReceived = true;
                                    }
                                    catch
                                    {
                                    }
                                    finally
                                    {
                                        gch.Free();
                                    }
                                }
                            }
                            else
                            {
                                Logging.AddToLog(LogLevels.Warning, "Error in UTR ICP response header - not in sync");
                                Logging.AddToLog(LogLevels.Warning, "Count - " + response.Length + "-" + readLength);
                                IcpDisconnect();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logging.AddToLog(LogLevels.Warning, ex.ToString());
                        IcpDisconnect();
                        Thread.Sleep(60000);
                    }
                    finally
                    {
                        int index = 0;
                        do
                        {
                            Thread.Sleep(10);
                        }
                        while (IcpTelegramList_GetCount() == 0 && ++index < 20);
                    }
                }
                catch
                {
                    Thread.Sleep(100);
                }
            }

            IcpConnectionState = EnumIcpConnectionStates.NotRunning;
            Logging.AddToLog(LogLevels.Warning, "UTR PacControl stopped");
        }

        #region Icp communication

        private void ResponseFromPAC(byte[] response, byte[] telegram, int readLength, bool statusReceived)
        {
            response = _binaryReaderIcp.ReadBytes(readLength);
            if (response.Length == readLength
                && response[0] == telegram[0]
                && response[1] == telegram[1]
                && response[2] == telegram[3]
                && response[readLength - 1] == 4)
            {
                //lock (_lockStatus)
                {
                    GCHandle gch = GCHandle.Alloc(response, GCHandleType.Pinned);
                    try
                    {
                        _status = (StatusStruct)Marshal.PtrToStructure(gch.AddrOfPinnedObject(), typeof(StatusStruct));

                        statusReceived = true;
                    }
                    finally
                    {
                        gch.Free();
                    }
                }
            }
            else
            {
                Logging.AddToLog(LogLevels.Warning, "Error in UTR ICP response header - not in sync");
                IcpDisconnect();
            }
        }


        #region Add telegram


        public byte[] IcpGetOrAddTelegram(IcpTelegramTypes icpTelegramType,
            object oParameters, bool doAddToList)
        {
            byte[] result = null;
            byte pcStatus = _pcStatus;// 2;

            try
            {
                if (_tcpClientIcp != null && _tcpClientIcp.Client != null && _tcpClientIcp.Client.Connected)
                {
                    if (_telegramId < 99)
                    {
                        ++_telegramId;
                    }
                    else
                    {
                        _telegramId = 1;
                    }

                    switch (icpTelegramType)
                    {
                        case IcpTelegramTypes.RequestStatus://A
                            if (_telegramRequestStatus == null)
                            {
                                int expectedStructSize = _statusStructSize - 6;
                                _telegramRequestStatus = new byte[] {
                                SOH, _telegramId, pcStatus, (byte)'A', 9, 0,
                                (byte)(expectedStructSize % 256),
                                (byte)(expectedStructSize / 256),
                                 EOT };
                            }
                            else
                            {
                                _telegramRequestStatus[1] = _telegramId;
                                _telegramRequestStatus[2] = pcStatus;
                            }

                            result = _telegramRequestStatus;
                            break;

                        case IcpTelegramTypes.Reset://R
                            result = new byte[] { SOH, GetTelegramId(), (byte)pcStatus, (byte)'R', 7, 0, EOT };
                            break;
                    }

                    if (doAddToList)
                    {
                        IcpTelegramList_Add(result);
                    }
                }
            }
            catch
            {
            }

            return result;
        }

        private void IcpTelegramList_Add(byte[] telegram)
        {
            if (telegram != null && telegram.Length > 0)
            {
                lock (_lockTelegramListIcp)
                {
                    _telegramListIcp.Add(telegram);
                }
            }
        }

        private byte[] IcpTelegramList_GetNext()
        {
            byte[] result = null;

            lock (_lockTelegramListIcp)
            {
                if (_telegramListIcp.Count > 0)
                {
                    result = _telegramListIcp[0];
                    _telegramListIcp.RemoveAt(0);
                }
            }

            return result;
        }

        private void IcpTelegramList_Clear()
        {
            lock (_lockTelegramListIcp)
            {
                _telegramListIcp.Clear();
            }
        }

        private int IcpTelegramList_GetCount()
        {
            int result;
            lock (_lockTelegramListIcp)
            {
                result = _telegramListIcp.Count;
            }
            return result;
        }
        #endregion

        #region GetTelegramId

        private byte GetTelegramId()
        {
            byte result;

            lock (_lockTelegram)
            {
                if (_telegramId == 255)
                {
                    _telegramId = 0;
                }
                result = _telegramId++;
            }

            return result;
        }

       
        #endregion

        private void IcpDisconnect()
        {
            Logging.AddToLog(LogLevels.Warning, "IcpDisconnect");
            IcpConnectionState = EnumIcpConnectionStates.Disconnected;
            IcpTelegramList_Clear();
            if (_binaryReaderIcp != null)
            {
                try
                {
                    _binaryReaderIcp.Close();
                    _binaryReaderIcp.Dispose();
                }
                catch
                {
                }
                finally
                {
                    _binaryReaderIcp = null;
                }
            }

            if (_binaryWriterIcp != null)
            {
                try
                {
                    _binaryWriterIcp.Close();
                    _binaryWriterIcp.Dispose();
                }
                catch
                {
                }
                finally
                {
                    _binaryWriterIcp = null;
                }
            }

            if (_networkStreamIcp != null)
            {
                try
                {
                    _networkStreamIcp.Close();
                    _networkStreamIcp.Dispose();
                }
                catch
                {
                }
                finally
                {
                    _networkStreamIcp = null;
                }
            }

            if (_tcpClientIcp != null)
            {
                try
                {
                    _tcpClientIcp.Client.Close();
                }
                catch
                {
                }

                try
                {
                    _tcpClientIcp.Close();
                }
                catch
                {
                }
                finally
                {
                    _tcpClientIcp = null;
                }
            }
        }
        #endregion
    }
}
