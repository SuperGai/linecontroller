﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LineControllerLib
{
    class Inkjet
    {
        #region SendImajeSetup

        //public bool SendImajeSetup()
        //{
        //    bool result = false;
        //    if (IcpConnectionState == EnumIcpConnectionStates.Connected && SetupLine.IsInkEnable == 1) // _deactivateInkPrint)// && !_deactivateInkSendSetup)
        //    {
        //        IcpGetOrAddTelegramGripper(EnumIcpTelegramTypes.SendImajeSetup, null, true);
        //        result = true;
        //    }
        //    return result;
        //}

        #endregion

        #region Imaje converting

        //public string IntToBinaryString(int number)
        //{
        //    const int mask = 1;
        //    var binary = string.Empty;
        //    while (number > 0)
        //    {
        //        // Logical AND the number and prepend it to the result string
        //        binary = (number & mask) + binary;
        //        number = number >> 1;
        //    }

        //    return binary;
        //}

        public Inkjet()
        {

        }

        //public static byte LineId; //Gripper line id

        public static byte[] AdressToStringImajeLinesWithoutLibary(string Line1, string Line2, string Line3, string Line4,
            out byte paperAddressWidth)
        {
            byte[] result = null;
            paperAddressWidth = 0;
            int jetNumber = 1;
            string sMessage = "";
            string sMessageLinje12;
            string sMessageLinje34;
            string sLength;
            string sData;
            string sCheckSum;
            string sId;
            //char checks2;

            try
            {
                string temp;
                temp = Line1; Line1 = Line2; Line2 = temp; //exchange line 1 and 2
                temp = Line3; Line3 = Line4; Line4 = temp; //exchange line 3 and 4

                Line1 = Line1.ToUpper().Trim();
                Line2 = Line2.ToUpper().Trim();
                Line3 = Line3.ToUpper().Trim();
                Line4 = Line4.ToUpper().Trim();

                int maxLength = 0;
                maxLength = Math.Max(Line1.Length, Line2.Length);
                maxLength = Math.Max(Line3.Length, maxLength);
                maxLength = Math.Max(Line4.Length, maxLength);

                if (maxLength > 25)
                {
                    maxLength = 25;
                }

                for (int i = 1; i <= 4; i++)
                {
                    switch (i)
                    {
                        case 1: Line1 = Line1.PadRight(maxLength, ' ').Substring(0, maxLength); break;
                        case 2: Line2 = Line2.PadRight(maxLength, ' ').Substring(0, maxLength); break;
                        case 3: Line3 = Line3.PadRight(maxLength, ' ').Substring(0, maxLength); break;
                        case 4: Line4 = Line4.PadRight(maxLength, ' ').Substring(0, maxLength); break;
                    }
                }

                sId = ((char)0xA).ToString();
                sData = ((char)jetNumber).ToString() +
                    (char)0xa + ((char)0x1).ToString() +
                    ((char)0x2C).ToString() +  //((char)0x2C).ToString() +
                    ReplaceSpecialCharacters(Line1) + (char)0xa + ((char)0x1).ToString() +
                   ((char)0x2C).ToString() +  //((char)0x2C).ToString() +
                    ReplaceSpecialCharacters(Line2) + (char)0xD;
                sLength = null;//UniDevLib.DataConverting.StringConverting.ConvertIntToBinaryString(sData.Length, 2);
                //sLength = IntToBinaryString(sData.Length);
                //sLength = sData.Length.ToString("X4");

                sCheckSum = CalculateCheckSum(sId + sLength + sData).ToString();
                //checks2 = Convert.ToChar(sCheckSum); //CalculateCheckSum(sId + sLength + sData);
                sMessageLinje12 = sId + sLength + sData + sCheckSum;
                //sMessageLinje12 = ((char)(byte)sMessageLinje12.Length).ToString() + sMessageLinje12;

                jetNumber++;

                //if (Line3.Length > 0 || Line4.Length > 0)
                {
                    sId = ((char)0xA).ToString();
                    sData = ((char)jetNumber).ToString() +
                        (char)0xa + ((char)0x1).ToString() +
                        ((char)0x2C).ToString() +  //((char)0x2C).ToString() +
                        ReplaceSpecialCharacters(Line3) + (char)0xa + ((char)0x1).ToString() +
                        ((char)0x2C).ToString() +  //((char)0x2C).ToString() +
                        ReplaceSpecialCharacters(Line4) + (char)0xD;
                    sLength = null;//UniDevLib.DataConverting.StringConverting.ConvertIntToBinaryString(sData.Length, 2); //sData.Length.ToString("X4"); 
                    sCheckSum = CalculateCheckSum(sId + sLength + sData).ToString();
                    //checks2 = Convert.ToChar(sCheckSum); //CalculateCheckSum(sId + sLength + sData);
                    sMessageLinje34 = sId + sLength + sData + sCheckSum;
                }
                sMessage = sMessageLinje12 + sMessageLinje34;// +sMessageLinje56 + sMessageLinje78;
                paperAddressWidth = (byte)maxLength;
            }
            catch (Exception ex)
            {
                Logging.AddToLog(LogLevels.Error, "Inkjet: " + ex.ToString());
            }

            result = new byte[sMessage.Length];
            for (int i = 0; i < sMessage.Length; i++)
            {
                result[i] = (byte)sMessage[i];
            }

            return result;
        }

        public static string ReplaceSpecialCharacters(string sData)
        {
            string sBuffer = null;

            try
            {
                sBuffer = sData;

                sBuffer = sBuffer.Replace('ä', (char)132);
                sBuffer = sBuffer.Replace('å', (char)134);
                sBuffer = sBuffer.Replace('Ä', (char)142);
                sBuffer = sBuffer.Replace('Å', (char)143);
                sBuffer = sBuffer.Replace('æ', (char)145);
                sBuffer = sBuffer.Replace('Æ', (char)146);
                sBuffer = sBuffer.Replace('ö', (char)148);
                sBuffer = sBuffer.Replace('Ö', (char)154);
                sBuffer = sBuffer.Replace('ø', (char)159); //0x9F
                sBuffer = sBuffer.Replace('Ø', (char)158); //0x9E
                sBuffer = sBuffer.Replace('é', (char)130); //0x82h
                sBuffer = sBuffer.Replace('É', (char)144); //0x90h
                sBuffer = sBuffer.Replace('ü', (char)151); //0x97h gemenform: 
                sBuffer = sBuffer.Replace('Ü', (char)155); //0x9Bh

                bool isChange = false;
                char[] buffer = sBuffer.ToCharArray();
                for (int i = 0; i < buffer.Length; i++)
                {
                    byte b = (byte)buffer[i];
                    if (b < 32 || (b > 126
                        && b != 132 && b != 134 && b != 142 && b != 143 && b != 145 && b != 146
                        && b != 148 && b != 154 && b != 159 && b != 158
                        && b != 130 && b != 144 && b != 151 && b != 155))
                    {
                        buffer[i] = '?';
                        isChange = true;
                    }
                }

                if (isChange)
                {
                    Logging.AddToLog(LogLevels.Error, "Inkjet: Unrecognized characters in string replaced with '?': " + sData);
                    sBuffer = new string(buffer);
                }
            }
            catch (Exception ex)
            {
                Logging.AddToLog(LogLevels.Error, "Inkjet: " + ex.ToString());
            }

            return sBuffer;
        }

        private static char CalculateCheckSum(string sData)
        {
            char cReturnChar = '\0';
            try
            {
                for (int ix = 0; ix < sData.Length; ix++)
                {
                    cReturnChar = (char)(cReturnChar ^ sData[ix]);
                }
            }
            catch (Exception ex)
            {
                Logging.AddToLog(LogLevels.Error, "Inkjet CalculateCheckSum: " + ex.ToString());
            }
            return cReturnChar;
        }

        private static byte CalculateCheckSum(byte[] bData)
        {
            byte result = 0;
            try
            {
                for (int ix = 0; ix < bData.Length; ix++)
                {
                    result = (byte)(result ^ bData[ix]);
                }
            }
            catch (Exception ex)
            {
                Logging.AddToLog(LogLevels.Error, "Inkjet CalculateCheckSum[]: " + ex.ToString());
            }
            return result;
        }

        private static byte[] GetImajeSetupTelegram(bool doInvertMessage, bool doInvertHorizontal,
            bool doInvertVertical, UInt16 tacoMeterDivision)
        {
            byte[] result;

            byte generalFirstByte = 16;//bit4 = tachometer mode

            if (!doInvertMessage)
            {
                generalFirstByte |= 128;
            }
            if (!doInvertHorizontal)
            {
                generalFirstByte |= 64;
            }
            if (!doInvertVertical)
            {
                generalFirstByte |= 32;
            }
            //System.Windows.MessageBox.Show("Setup sent:" + generalFirstByte.ToString());

            byte tacoMeterDivisionHigh = (byte)(tacoMeterDivision >> 8);
            byte tacoMeterDivisionLow = (byte)(tacoMeterDivision & 0xFF);

            byte variableFirstByte = 1;//bit3-0 = Number of figures printed
            byte b0 = (byte)'0';
            byte b1 = (byte)'1';

            List<byte> jetByteList = new List<byte>();
            jetByteList.Add(0x1B);//esc
            jetByteList.Add(0x00);//length high
            jetByteList.Add(0x26);//length low
            byte[] messageData = new byte[38] {//38 = 0x26
                //Jet number:
                0x01,

                //Message general:
                generalFirstByte,
                tacoMeterDivisionHigh,  //Printing speed/ tachometer division high
                tacoMeterDivisionLow,   //Printing speed/ tachometer division low
                0x00,   //Forward margin mm high
                0x01,   //Forward margin mm low
                0x00,   //Return margin mm high
                0x01,   //Return margin mm low
                0x00,   //Interval mm high
                0x01,   //Interval mm low
                0x01,   //Object top filter high
                0x00,   //Object top filter low
                
                //Variable elements:
                variableFirstByte,
                b0, b0, b0, b0, b0, b0, b0, b0, b0,//Initial value ascii "000000000"
                b0, b0, b0, b0, b0, b0, b0, b0, b0,//Final value ascii "000000000"
                b0, b1,//Step counter ascii "01"
                0x00, 0x00, 0x01,//Lot counter
                0x00, 0x00//Postdate interval
                };

            jetByteList.AddRange(messageData);

            byte[] bJetBytes = jetByteList.ToArray();
            byte controlByte = CalculateCheckSum(bJetBytes);

            List<byte> byteList = new List<byte>();
            byteList.AddRange(bJetBytes);
            byteList.Add(controlByte);

            bJetBytes[3] = 0x02;
            controlByte = CalculateCheckSum(bJetBytes);
            byteList.AddRange(bJetBytes);
            byteList.Add(controlByte);

            //bJetBytes[3] = 0x03;
            //controlByte = CalculateCheckSum(bJetBytes);
            //byteList.AddRange(bJetBytes);
            //byteList.Add(controlByte);

            //bJetBytes[3] = 0x04;
            //controlByte = CalculateCheckSum(bJetBytes);
            //byteList.AddRange(bJetBytes);
            //byteList.Add(controlByte);

            result = byteList.ToArray();

            return result;
        }

        /*
        private byte[] GetImajePrintAckTelegram()
        {
            byte[] result;

            List<byte> jetByteList = new List<byte>();
            jetByteList.Add(0x41);//identification
            jetByteList.Add(0x00);//length high
            jetByteList.Add(0x02);//length low
            jetByteList.Add(0x01);//Jet number
            jetByteList.Add((byte)226);//0x80 = 128 (bit 7 ack for each object printed)

            byte[] bJetBytes = jetByteList.ToArray();
            byte controlByte = CalculateCheckSum(bJetBytes);

            List<byte> byteList = new List<byte>();
            byteList.AddRange(bJetBytes);
            byteList.Add(controlByte);

            bJetBytes[3] = 0x02;
            controlByte = CalculateCheckSum(bJetBytes);
            byteList.AddRange(bJetBytes);
            byteList.Add(controlByte);

            bJetBytes[3] = 0x03;
            controlByte = CalculateCheckSum(bJetBytes);
            byteList.AddRange(bJetBytes);
            byteList.Add(controlByte);

            bJetBytes[3] = 0x04;
            controlByte = CalculateCheckSum(bJetBytes);
            byteList.AddRange(bJetBytes);
            byteList.Add(controlByte);

            result = byteList.ToArray();

            return result;
        }
        */

        #endregion
    }
}
