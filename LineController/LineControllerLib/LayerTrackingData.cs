﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LineControllerLib
{
    public class LayerTrackingData
    {
        public int stackerNumber { get; set; }
        public StackerLine stacker { get; set; }
        public int startGripperNumber { get; set; }
        public int endGripperNumber { get; set; }
        public ushort bundleId { get; set; }
        public int copiesInBundle { get; set; }
        public int copiesInLayer { get; set; }
        public int copiesInLastLayer { get; set; }
        public byte numLayersTracking { get; set; }
        public int countdownLayerCommandStart { get; set; }
        public int countdownLayerCommandEnd { get; set; }
        public int countdownOrderBundleABL { get; set; }
        public int stackerNumberLast { get; set; }
        public StackerLine stackerLast { get; set; }
        public bool workingAlone { get; set; }
    }
}
