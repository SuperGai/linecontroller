﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LineControllerLib
{
    public class BundleData
    {
        public enum LayerFlagTypes : byte
        {
            LAYERFLAG_EMPTY = 0,
            LAYERFLAG_START = 1,
            LAYERFLAG_PAUSE = 2,
            LAYERFLAG_RESUME = 4,
            LAYERFLAG_END = 8,
            LAYERFLAG_ROTATE = 16,
            LAYERFLAG_ENDOFBUNDLE = 32,
            LAYERFLAG_EJECTBACKSIDE = 64,
            LAYERFLAG_ABORT = 128
        }

        public ushort BundleId { get; set; }
        public ushort NumCopies { get; set; }
        public ushort NumCopiesCounted { get; set; }
        public byte Stacker { get; set; }
        public ushort CopiesInLayer { get; set; }
        public ushort MaxCopiesInLayer { get; set; }
        public ushort MinCopiesInLayer { get; set; }
        public ushort MinCopiesInBundle { get; set; }
        public LineControlMain.enumBundleStatus Status { get; set; }
        public List<LayerData> LayerCommandList { get; set; }
        public List<AddressData> AddressList { get; set; }
        public byte NumLayersInBundle { get; set; }
        public byte NextLayerIndex { get; set; }
        public byte NextAddressIndex { get; set; }

        public BundleData()
        {
            AddressList = new List<AddressData>();
            LayerCommandList = new List<LayerData>();
        }

        //Valid Address put coninuously to the last of bundle
        public void UpdateAddressList()
        {
            byte NumAddress;
            if (AddressList != null && LayerCommandList != null && AddressList.Count>0 && LayerCommandList.Count>0)
            {
                if (AddressList.Count > 0)
                {
                    int i = AddressList.Count - 1;
                    for (int j = LayerCommandList.Count-1; j >= 0; j--)
                    {
                        NumAddress = 0;
                        if (i >= 0)
                        {
                            for (int k = LayerCommandList[j].NumCopiesInLayer; k >= 1; k--)
                            {
                                //AddressList[i].AddressID = (byte)k; //K is 1,2,...
                                AddressList[i].CopyID = (byte)k; //K is 1,2,...
                                AddressList[i].LayerID = (byte)(j + 1); //j is 1,2,...
                                AddressList[i].BundlID = BundleId;
                                ++NumAddress;
                                i--;
                                if (i < 0)
                                {
                                    break;
                                }
                            }
                        }
                        LayerCommandList[j].NumAddress = NumAddress;
                    }
                } else {
                    for (int j = LayerCommandList.Count-1; j >= 0; j--)
                    {
                        LayerCommandList[j-1].NumAddress = 0;
                    }
                }
            }
        }

        public void SetAddressList(string addressData)
        {
            if (AddressList == null)
            {
                AddressList = new List<AddressData>();
            }
            
            NextAddressIndex = 0;
            if (addressData != null && addressData.Length > 0)
            {
                string[] lines = addressData.Split((char)10);

                if (lines.Length > 0)
                {
                    for (int i = 0; i < lines.Length; i++)
                    {
                        string line = lines[i].Trim();

                        if (line.Length == 0)
                        {
                            continue;
                        }

                        string[] s = line.Split((char)29);

                        AddressData a = new AddressData();
                        a.State = 1;
                        a.StackerNumber = Stacker;
                        a.BundlID = BundleId;
                        a.AddressLine1 = s[0];
                        a.AddressLine2 = s.Length > 1 ? s[1] : " ";
                        a.AddressLine3 = s.Length > 2 ? s[2] : " ";
                        a.AddressLine4 = s.Length > 3 ? s[3] : " ";
                        a.AddressLine4 = a.AddressLine4 + " " + BundleId.ToString();

                        if ((i == lines.Length - 1) || ((i < lines.Length - 1) && (lines[i + 1].Trim().Length == 0)))
                        {
                            a.AddressLine4 = a.AddressLine4 + " E";
                        }
                        else
                        {
                            a.AddressLine4 = a.AddressLine4 + "-" + (i + 1).ToString();
                        }
                        AddressList.Add(a);
                    }
                }
                else {
                    for (int i = 0; i < NumCopies; i++)
                    {
                        AddressData a = new AddressData();
                        a.State = 1;
                        a.StackerNumber = Stacker;
                        a.BundlID = BundleId;
                        a.AddressLine1 = " ";
                        a.AddressLine2 = " ";
                        a.AddressLine3 = " ";
                        a.AddressLine4 = " ";
                        a.AddressLine4 = BundleId.ToString();

                        if (i == NumCopies - 1)
                        {
                            a.AddressLine4 = a.AddressLine4 + " E";
                        }
                        else
                        {
                            a.AddressLine4 = a.AddressLine4 + "-" + (i + 1).ToString() + '/' + NumCopies.ToString();
                        }
                        AddressList.Add(a);
                    }               
                }
            }  else 
            {
                for (int i = 0; i < NumCopies; i++)
                {
                    AddressData a = new AddressData();
                    a.State = 1;
                    a.StackerNumber = Stacker;
                    a.BundlID = BundleId;
                    a.AddressLine1 = " ";
                    a.AddressLine2 = " ";
                    a.AddressLine3 = " ";
                    a.AddressLine4 = " ";
                    a.AddressLine4 = BundleId.ToString();

                    if (i == NumCopies - 1)
                    {
                        a.AddressLine4 = a.AddressLine4 + " E";
                    } else {
                        a.AddressLine4 = a.AddressLine4 + "-" + (i + 1).ToString() + '/' + NumCopies.ToString();
                    }
                    AddressList.Add(a);
                } 
            }

            //while (AddressList.Count < NumCopies)
            //{
            //    AddressData a = new AddressData();
            //    a.State = 3;
            //    a.BundlID = BundleId;
            //    a.AddressLine1 = "";
            //    a.AddressLine2 = "";
            //    a.AddressLine3 = "";
            //    a.AddressLine4 = "";
            //    AddressList.Add(a);
            //}

        }

        public AddressData GetNextAddressCommand(out bool IsLast)
        {
            AddressData result = null;

            if (NextAddressIndex < AddressList.Count)
            {
                result = AddressList[NextAddressIndex];
                ++NextAddressIndex;
            }

            IsLast = NextAddressIndex >= AddressList.Count;

            return result;
        }


        public bool CreateLayerList()
        {
            bool result = false;
            NumLayersInBundle = 0;
            //List<string> testList = new List<string>();
            try
            {
                //MinCopiesInLayer = 20;
                //MaxCopiesInLayer = 59;
                //MinCopiesInBundle = 8;
                //for (NumCopies = 0; NumCopies < 200; NumCopies++)
                {
                    List<LayerData> newLayerList = null;
                    LayerCommandList = null;
                    NextLayerIndex = 0;

                    try
                    {
                        if (NumCopies > 0 && CopiesInLayer > 0)
                        {
                            if (MinCopiesInLayer == 0)
                            {
                                MinCopiesInLayer = CopiesInLayer;
                            }

                            if (MinCopiesInBundle == 0)
                            {
                                MinCopiesInBundle = CopiesInLayer;
                            }

                            if (MaxCopiesInLayer == 0)
                            {
                                MaxCopiesInLayer = (ushort)(CopiesInLayer * 2 - 1);
                            }

                            if (NumCopies > 0 &&
                                (NumCopies >= MinCopiesInLayer || NumCopies >= MinCopiesInBundle) &&
                                CopiesInLayer > 0 && CopiesInLayer >= MinCopiesInLayer && CopiesInLayer <= MaxCopiesInLayer)
                            {
                                result = true;
                                List<int> copyCountList = new List<int>();
                                int copiesRemaining = NumCopies;
                                int MyMaxCopyInLayer = MaxCopiesInLayer;

                                if (MyMaxCopyInLayer < 2 * MinCopiesInLayer)
                                {//In mosjeon, MaxCopyInLayer is around double MinCpoiesInLayer
                                    MyMaxCopyInLayer = 2 * MinCopiesInLayer;
                                }

                                if (NumCopies <= MyMaxCopyInLayer)
                                {
                                    copyCountList.Add(NumCopies);
                                    copiesRemaining = 0;
                                }
                                else {
                                    int numLayers = NumCopies / CopiesInLayer;
                                    copiesRemaining = NumCopies - CopiesInLayer * numLayers;

                                    //if (numLayers == 0)
                                    //{
                                    //    if (NumCopies >= MinCopiesInBundle)// MinCopiesInLayer)
                                    //    {
                                    //        copyCountList.Add(NumCopies);
                                    //        copiesRemaining = 0;
                                    //    }
                                    //}
                                    //else
                                    //{
                                        for (int i = 0; i < numLayers; i++)
                                        {
                                            copyCountList.Add(CopiesInLayer);
                                            copiesRemaining -= CopiesInLayer;
                                        }
                                    //} 

                                        
                                
                                
                                }



                                if (copiesRemaining > 0)
                                {
                                    int copiesSplit = numLayers > 0 ? copiesRemaining / numLayers : 0;
                                    int restSplit = numLayers > 0 ? copiesRemaining % numLayers : copiesRemaining;

                                    if (copiesSplit > 0 && CopiesInLayer + copiesSplit + restSplit < MaxCopiesInLayer)
                                    {
                                        for (int i = 0; i < copyCountList.Count; i++)
                                        {
                                            copyCountList[i] += copiesSplit;
                                        }

                                        copyCountList[copyCountList.Count - 1] += restSplit;
                                    } else if (copiesSplit > 0 && CopiesInLayer - copiesSplit > MinCopiesInLayer)
                                    {
                                        copyCountList.Add(copiesRemaining);
                                        if (copiesRemaining < MinCopiesInLayer)
                                        {
                                            for (int i = 0; i < copyCountList.Count - 1; i++)
                                            {
                                                copyCountList[i] -= copiesSplit;
                                                copyCountList[copyCountList.Count - 1] += copiesSplit;
                                                copiesRemaining -= copiesSplit;
                                            }

                                            if (copyCountList[copyCountList.Count - 1] >= MinCopiesInLayer && copyCountList[copyCountList.Count - 1] <= MaxCopiesInLayer)
                                            { //it is ok, in this situation

                                            }
                                            else {
                                                if (copiesRemaining > 0)
                                                {
                                                    copyCountList[copyCountList.Count - 1] -= copiesRemaining;
                                                }                                            
                                            }
                                        }
                                    }
                                    else if (copiesRemaining > 0 && copiesRemaining >= MinCopiesInLayer)
                                    {
                                        copyCountList.Add(copiesRemaining);
                                        copiesRemaining = 0;
                                    }
                                    else
                                    {
                                        copyCountList[copyCountList.Count - 1] += copiesRemaining;
                                    }

                                    //Fix the issue, when copy is 37, it make only one layer with 37 in case of c/l 25 max 27 min 14
                                    if ((copyCountList.Count == 1) && (copyCountList[0] > MaxCopiesInLayer) && (copyCountList[0] - MinCopiesInLayer >= MinCopiesInLayer))
                                    {
                                        copyCountList[0] -= MinCopiesInLayer;
                                        copyCountList.Add(MinCopiesInLayer);
                                    }
                                }

                                int copyCheck = 0;
                                for (int i = 0; i < copyCountList.Count; i++)
                                {
                                    copyCheck += copyCountList[i];
                                }

                                if (copyCheck != NumCopies)
                                {
                                    Logging.AddToLog(LogLevels.Warning, "Layers setup not correct - fixing!");
                                    copyCountList.Clear();
                                    copiesRemaining = NumCopies; //CopiesInLayer;

                                    numLayers = NumCopies / CopiesInLayer;
                                    for (int i = 0; i < numLayers; i++)
                                    {
                                        copyCountList.Add(CopiesInLayer);
                                        copiesRemaining -= CopiesInLayer;
                                    }

                                    if (copiesRemaining > 0)
                                    {
                                        copyCountList[numLayers - 1] += copiesRemaining;
                                    }
                                }

                                numLayers = copyCountList.Count;

                                newLayerList = new List<LayerData>();
                                for (int i = 0; i < copyCountList.Count; i++)
                                {
                                    //byte startFlags = (byte)LayerFlagTypes.LAYERFLAG_START;
                                    //byte endFlags = (byte)LayerFlagTypes.LAYERFLAG_END;
                                    byte layerNumber = (byte)(i + 1);
                                    byte numCopiesInLayer = (byte)copyCountList[i];
                                    byte Flags = 0;

                                    //byte numLayers = (byte)(copyCountList.Count - 1);

                                    if (i == 0)
                                    {
                                        Flags |= (byte)LayerFlagTypes.LAYERFLAG_START;
                                    }

                                    if (i == copyCountList.Count - 1)
                                    {
                                        Flags |= (byte)LayerFlagTypes.LAYERFLAG_ENDOFBUNDLE;
                                    }

                                    //newLayerList.Add(new LayerData(BundleId, (byte)(i + 1),
                                    //    startFlags, numCopiesInLayer, NumCopies, (byte)numLayers));
                                    //newLayerList.Add(new LayerData(BundleId, (byte)(i + 1),
                                    //    endFlags, numCopiesInLayer, NumCopies, (byte)numLayers));
                                    newLayerList.Add(new LayerData(Stacker, BundleId, (byte)(i + 1), Flags,
                                        numCopiesInLayer, NumCopies, (byte)numLayers));

                                }
                                NumLayersInBundle = (byte)numLayers;
                            }
                           
                        }
                    }
                    catch (Exception ex)
                    {
                        Logging.AddToLog(LogLevels.Error, "Invalid bundle "
                            + BundleId.ToString() + ": " + ex.ToString());

                        newLayerList = null;
                    }

                    if (newLayerList != null && newLayerList.Count > 0)
                    {
                        LayerCommandList = newLayerList;
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }

                    /*
                    string sTest = string.Format("NumCopies={0}, Result={1}, NumLayers={2}," +
                        "{3}{4}{5}{6}{7}{8}{9}"
                        , NumCopies
                        , result ? 1 : 0
                        , newLayerList != null ? (newLayerList.Count / 2).ToString() : ""
                        , newLayerList != null && newLayerList.Count > 0 ? ", L1=" + newLayerList[0].NumCopiesInLayer.ToString() : ""
                        , newLayerList != null && newLayerList.Count > 2 ? ", L2=" + newLayerList[2].NumCopiesInLayer.ToString() : ""
                        , newLayerList != null && newLayerList.Count > 4 ? ", L3=" + newLayerList[4].NumCopiesInLayer.ToString() : ""
                        , newLayerList != null && newLayerList.Count > 6 ? ", L4=" + newLayerList[6].NumCopiesInLayer.ToString() : ""
                        , newLayerList != null && newLayerList.Count > 8 ? ", L5=" + newLayerList[8].NumCopiesInLayer.ToString() : ""
                        , newLayerList != null && newLayerList.Count > 10 ? ", L6=" + newLayerList[10].NumCopiesInLayer.ToString() : ""
                        , newLayerList != null && newLayerList.Count > 12 ? ", L7=" + newLayerList[12].NumCopiesInLayer.ToString() : ""
                        );
                    testList.Add(sTest);
                    */
                }
            }
            catch// (Exception ex)
            {
            }

            return result;
        }

        public LayerData GetNextLayerCommand(out bool IsLast)
        {
            LayerData result = null;

            if (NextLayerIndex < LayerCommandList.Count)
            {
                result = LayerCommandList[NextLayerIndex];
                ++NextLayerIndex;
            }

            IsLast = NextLayerIndex >= LayerCommandList.Count;

            return result;
        }


        public List<AddressData> GetAddressListOfCurLayer(int CurLayerIndex)
        {
            AddressData CurAddress = null;
            List<AddressData> CurAddressList = new List<AddressData>();
            bool IsFinishedAllAddress = false;
            for (int i = 0; i < LayerCommandList[CurLayerIndex].NumAddress; i++ )
            {
                CurAddress = GetNextAddressCommand(out IsFinishedAllAddress);
                CurAddressList.Add(CurAddress);
                if (IsFinishedAllAddress)
                {
                    break;
                }
            }
            return CurAddressList;
        }

    }
}
