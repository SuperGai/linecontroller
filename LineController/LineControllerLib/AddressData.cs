﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LineControllerLib
{
    public class AddressData
    {
        public byte State { get; set; }
        public byte StackerID { get; set; }
        //public byte AddressID { get; set; } //local id inside a layer, start from 0 to NumOfCopiesInLayer-1
        public byte CopyID { get; set; }    //local id inside a layer, start from NumOfCopiesInLayer to minimum 1
        public byte LayerID { get; set; }    //layer id inside a bundle, start from 0 to NumOfLayers-1
        public ushort BundlID { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string AddressLine4 { get; set; }
    }
}
