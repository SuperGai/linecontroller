﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace LineControllerLib
{
    public class LogItem
    {
        public LogLevels LogLevel { get; set; }
        public string Message { get; set; }
        public DateTime LogTime { get; set; }
    }
}
