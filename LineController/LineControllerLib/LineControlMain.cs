﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using TopSheetControlLib;
using static TopSheetControlLib.TopSheetMain;

namespace LineControllerLib
{
    public class LineControlMain
    {
        #region Constants
        private const byte SOH = 1;
        private const byte EOT = 4;
        //private const byte BUFSIZEGRIPPERMESSAGE = 10;
        private const byte MAXSTACKERLINES = 4;
        private const byte MAXABLPerGroup = 3;
        //private const byte MAXSTACKERS = 4;
        private const byte MAXBUNDLEMESSAGES = 30;
        //private const byte NUM_CHANNEL = 4;
        private const byte NUM_SLOT = 4;//3;
        private const byte NUM_ABL = 3;
        private const byte NUM_DBH = 2;
        private const byte NUM_STACKERLINE = 4;
        private const byte NUM_GRIPPERLINE = 8;

        #endregion

        #region Enums
        public enum EnumControlStates
        {
            Unknown = 0,
            NotRunning = 1,
            Running = 2,
            Producing = 3
        }

        public enum EnumIcpConnectionStates
        {
            Unknown = 0,
            NotRunning = 1,
            Disconnected = 2,
            Connecting = 3,
            Connected = 4
        }

        public enum EnumDbConnectionStates
        {
            DbUnknown,
            DbDisconnected,
            DbConnecting,
            DbConnected
        }

        public enum EnumIcpTelegramTypes
        {
            RequestStatus,
            SendSetup,
            SendBundleData,
            SendLayerCommand,
            SendAddressList,
            StartProduction,
            StopProduction,
            Reset,
            ClearCounters,
            //StartStopSimulation,
            StartLineController,
            StopLineController,
            PauseStacker,
            PauseOneStacker,

            ResumeStacker,
            ResumeOneStacker,
            StartStacker,
            StopStacker,


            SendStandardBundleSize,
            SendABLGroupSize
        }

        public enum PZFApplicatorStates
        {
            ApplicatorNotActive = 0,
            ApplicatorOffline = 1,
            ApplicatorReady = 2,
            ApplicatorWarning = 3,
            ApplicatorStopped = 4,
            ApplicatorError = 5,
            ApplicatorStandardBundles = 6,
            ApplicatorInitializing = 7,
            ApplicatorProductionFinished = 8,
            ApplicatorPaperError1 = 9,
            ApplicatorPaperError2 = 10,
            ApplicatorPaperError = 11,
            ApplicatorPaused = 12,
            ApplicatorPausedByOperator = 13,
            ApplicatorAlarmThermo = 14,
            ApplicatorAlarmArm = 15,
            ApplicatorUnknown = 16
        }

        public enum PZFStates
        {
            PZFUnknown = 0,
            PZFOffline = 1,
            PZFReady = 2,
            PZFError = 3,
            PZFPaused = 4,
            PZFPausedByOperator = 5,
            PZFAlarmThermo = 6
        }

        public enum STBStates
        {
            STBUnknown = 0,
            STBOffline = 1,
            STBReady = 2,
            STBError = 3,
            STBPaused = 4,
            STBAlarmThermo = 5
        }

        public enum EnumDataBaseCommands
        {
            dbCommNone = 0,
            dbCommSetBundleStatus = 1,
            dbCommGetProductionData = 2,
            dbCommGetBundleList = 3,
            //dbCommSetLineStatus = 4,
            //dbCommSetStackerStatus = 5,
            //dbCommStackerPause = 11,
            //dbCommStackerResume = 12,
            dbCommGetNextCommand = 52,
            dbCommGetSetup = 19,
            dbCommSetTopSheetApplicatorStatus = 20,
            dbCommSetTopSheetPrinterStatus = 21,
            dbCommStartLines = 24,
            dbCommStopLines = 25,
            dbCommClearCounters = 29,
            dbCommLCCGrippleLineInitial = 30,
            dbCommLCCManualPrint = 31,
            dbCommSaveStackerLineStatus = 74,
            dbCommSaveGripperLineStatus = 75,
            dbCommSaveGripperInfo = 76,
            dbCommSaveCopyInfo = 77
        }

        public enum EnumBundleStatus
        {
            bundleStatusReady = 0,
            //bundleStatusReservedGui = 1,
            bundleStatusReservedLCC = 1, //2,
            //bundleStatusProducing = 3,
            bundleStatusCounting = 5, //Counting finished
            bundleStatusReleaseFirst = 6, //Release Started
            bundleStatusReleaseLast = 7, //Release finished
            bundleStatusInStacker = 8, //4,
            bundleStatusAfterStacker = 10, //5,
            bundleStatusInkFailed = 106,
            bundleStatusStackerFailed = 108,
            bundleStatusBundleError = 150
        }

        

        public enum EnumLineStatus
        {
            elineUnknown = 0,
            elineNotAvailable = 1,
            elineReady = 2,
            elineProducing = 3,
            elineConnecting = 4,
            elineError = 91
        };

        public enum EnumMessageStatusTypes : byte
        {
            UNKNOWN = 0,
            COUNTFIRST = 1,
            COUNTLAST = 2,
            //ADDRESSFIRST = 3,
            //ADDRESSLAST = 4,
            RELEASEFIRST = 5,
            RELEASELAST = 6,
            INFEEDFIRST = 7,
            INFEEDLAST = 8,
            ENTRANCEFIRST = 9,
            ENTRANCELAST = 10,
            ATINTERCEPTFIRST = 11,
            ATINTERCEPTLAST = 12,
            BUNDLEINTABLE = 13,
            BUNDLEEJECTED = 14,
            PRODUCTIONFINISHED = 20,
            INCOMPLETETRANSFER = 50,
            //LOSTBEFOREINK = 51,
            ERRORINK1 = 52,
            //ERRORINK2 = 53,
            //LOSTBEFORETOSTACKER = 54,
            LOSTBEFORESTACKER = 55,
            LOSTBEFOREEJECTED = 56,
            STACKERABORT = 57,
            PRODUCTIONQUEUEFULL = 58,
            LOSTINTERCEPTWINDOW = 59,
            LAYERCONTROLABORT = 60,
            LCCABORT = 61,
            STACKERCYCLETIMEERROR = 62,
            EJECTARMNOTINPOSITION = 63,
            RELEACECYLINDERNOTINPOSITION = 64,
            SEPERATINGCYLINDERNOTINPOSITION = 65,
            RELEACEORSEPERATINGPOSITIONINCORRECT = 66,
            ARMBACKTIMEOUT = 67,
            ARMFRONTTIMEOUT = 68,
            ARMUPTIMEOUT = 69,
            SEPERATINGCYLINDERRETRACTTIMEOUT = 70,
            SEPERATINGCYLINDEREXTENDTIMEOUT = 71,
            RELEASECYLINDERRETRACTTIMEOUT = 72,
            RELEASECYLINDEREXTENDTIMEOUT = 73,
            TURNTABLELEFTTIMEOUT = 74,
            TURNTABLERIGHTTIMEOUT = 75,
            TURNTABLEPOSITIONUNKNOWN = 76,
            LAYERDATAMISSING = 77,
            RELEACECYLNOTINEXTEND = 78,
            RELEACECYLNOTRETRACTED = 79,
            SEPERATINGCYLNOTINEXTEND = 80,
            SEPERATINGCYLNOTINRETRACTED = 81,
            PAPERJAMPRESSING = 82,
            PAPERJAMCOUNTINGHEAD = 83,
            CONVERTERNOTREADY = 84,
            ARMDOWNTIMEOUT = 85,
            INTERCEPTTIMEOUT = 86,
            RESET = 87,
            CYCLETABLEPREPARENOTREADY = 88,
            CYCLETABLENOTREADY = 89
        }

        //public enum enumStackerLineStatus : byte
        //{
        //    Unknown = 0,
        //    NotAvailable = 1,
        //    Ready = 2,
        //    Producing = 3,
        //    Connecting = 4,
        //    Paused = 5,
        //    WaitingForData = 7,
        //    Stopped = 8,
        //    NotReady = 91
        //}

        //public enum enumStackerStatus : byte
        //{
        //    Unknown = 0,
        //    NotAvailable = 1,
        //    Ready = 2,
        //    Producing = 3,
        //    Connecting = 4,
        //    Paused = 5,
        //    WaitingForData = 7,
        //    Stopped = 8,
        //    NotReady = 91
        //}

        public enum EnumModes : byte
        {
            STANDARD = 0,//2,
            PROGRAMMED = 1//7
        }

        #endregion

        #region Structs/ Setup classes
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct BundleMessageStruct
        {
            //public uint EditionTableId;
            //public ushort RouteSortIndex;
            //public ushort BundleSortIndex;

            public uint BundleId;
            public byte StackerLineId;
            //public byte ABLGroupId;
            public byte ABLId;
            //public byte Status;
            public EnumMessageStatusTypes Status;
            public byte ExtraInfo;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct StackerLineStatusStruct
        {
            public byte Status; //0: No ABL working; 1: Some ABL are working
            public byte PosLineId;
            //public byte TempPadding1;
            //public byte TempPadding2;

            //unsigned char IsLayerPlanNotFinish;
            //unsigned char NumRequestLayer; //number of layers request from Line controller
            //unsigned int LastReceivedBundleID;  //unsigned int LastReceivedBundleID;
            public byte IsLayerPlanNotFinish;
            public byte NumRequestLayer;
            public uint LastReceivedBundleID;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = MAXABLPerGroup)]
            public byte[] State; //0: Stopped; 1: Ready: Infeedbelt is running 2: Not Running
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = MAXABLPerGroup)]
            public byte[] IsJam; // 0: No Jam; 1: Jam
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = MAXABLPerGroup)]
            public byte[] ReleaseStatus;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = MAXABLPerGroup)]
            public byte[] IsPaused;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = MAXABLPerGroup)]
            public uint[] IsLastLayerEndOfBundle;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = MAXABLPerGroup)]
            public uint[] LastLayerLastCopyGripperNr;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = MAXABLPerGroup)]
            public uint[] LastLayerLastCopyGripperPulse;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = MAXABLPerGroup)]
            public uint[] NumCopiesStackerOnWay;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = MAXABLPerGroup)]
            public uint[] NumCopiesStackerFinished;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = MAXABLPerGroup)]
            public uint[] CountersCopiesIn;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = MAXABLPerGroup)]
            public uint[] CountersCopiesOut;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = MAXABLPerGroup)]
            public uint[] CountersBundlesOut;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct _strGroupPara
        {   //Parameters per abl group
            public byte cNumABLs; //Number of ABLs per group.
            public byte cJamGrippersToStopABL;
            public byte cJamGrippersToStopGripper;
            public byte cTempPadding;

            public uint iCycleTimeLayer;
            public uint iCycleTimeBundle;

            public uint iDistToFirstABL;
            public uint iGapsABL; //standard 8

            public uint iDistToEdgeSupportCheckPoint;

            public uint iReleaseStartDelay;
            public uint iReleaseStopDelay;

            public uint iStrikeStartDelay;
            public uint iStrikeStopDelay;

            //unsigned int iStrikeStartEarlier;
            //unsigned int iStrikeStopEarlier;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct ProductionDataStruct
        {
            public ushort StandardBundle;
            public byte NumBatch;
            public byte NumCopiesLayer; //Number of copies in a layer for standard bundle
            public short TitleGroupId;
            public string IssueDate;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct StatusStruct
        {
            public byte SOH;
            public byte TelegramId;
            public byte Command;
            public ushort Length;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = NUM_SLOT)]
            public uint[] DigitalInputs;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = NUM_SLOT)]
            public uint[] DigitalOutputs;

            //public byte IsLayerPlanNotFinish;
            //public byte NumRequestLayer; //number of layers request from Line controller
            public byte isSimulate;
            public byte isReceivedBundleInfo; // 0: Not Receive Bundle Message 1: Received BundleMessage
            public byte TempPadding2;
            public byte TempPadding3;

            //public uint LastReceivedBundleID;  //unsigned int LastReceivedBundleID;

            public uint TaktCountGripper; //0 - forever
            public uint NumCopiesFinishedBundle; //0 - forever
            public uint NumCopiesMainCounter;	 //0 - forever
            public uint NumCopiesOverflow; //Overflow

            public uint GripperSpeedPrHour; //Gripper speed per hour
            //public ushort GripperPulses;

            public uint LastTaktGripperPulseLow;
            public uint LastTaktGripperPulseHigh;

            public uint GripperPulseLow;
            public uint GripperPulseHigh;

            public uint GripperPulsesPrSecond;

            public byte ActiveState; //For whole ICP state   ALS State 0: No Active
            public byte ProductionStarted;
            public byte GripperQueueStarted;
            public byte BundleMessageCount;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = MAXBUNDLEMESSAGES)]
            public BundleMessageStruct[] BundleMessage;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = MAXSTACKERLINES)]
            public StackerLineStatusStruct[] StackerLineStatus;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = MAXSTACKERLINES)]
            public _strGroupPara[] ABLGroupParameter;
            public byte EOT;
        }

        //[StructLayout(LayoutKind.Sequential, Pack = 1)]
        //public struct GripperLineSetupStruct
        public class GripperLineSetupStruct
        {
            public byte TaktDISlot = 255;
            public byte TaktDISignal = 255;
            public byte TaktDIOnHigh = 255;

            public byte CounterCopyDISlot = 255;
            public byte CounterCopyDISignal = 255;
            public byte CounterCopyDIOnHigh = 255;

            public byte GripperPercentDelayBeforeDetectCopy = 0;
            public byte PulseChannelGripper = 255;
            public ushort GripperPulsesPrMeter = 240;
            //public uint MMReleaseToInfeed = 0;//distance in millimeters
            //public ushort MarginMMReleaseToInfeed;//margin in millimeters
            public ushort MMCopyDistance = 100;//distance in millimeters between copies in gripper
        }

        //[StructLayout(LayoutKind.Sequential, Pack = 1)]
        //public struct StackerSetupStruct
        public class StackerSetupStruct
        {
            public byte IsActive;
            //High Speed Input
            public byte PulseChannelTaktRelease;
            public byte PulseChannelInfeedbelt;

            // 
            public byte EntranceDISlot;
            public byte EntranceDISignal;
            public byte EntranceDIOnHigh;

            //	char EntrancePulseChannel;	

            //Input
            public byte EjectBundleFinishDISlot;
            public byte EjectBundleFinishDISignal;
            public byte EjectBundleFinishOnHigh;

            //Output
            public byte EnableReleaseDOSlot;
            public byte EnableReleaseDOSignal;

            public byte ReleaseDOSlot;
            public byte ReleaseDOSignal;

            public byte BlockCounterDOSlot;
            public byte BlockCounterDOSignal;

            public byte MakeLayerDOSlot;
            public byte MakeLayerDOSignal;

            public byte EjectBundleDOSlot;
            public byte EjectBundleDOSignal;

            //Parameters
            public ushort ReleaseDelay; //number of gripper pulses
            public ushort StackerEjectMSDelay;
            public ushort CycleTimeMSecStacker;
            public ushort StandardCopiesInLayer;
            public byte StandardLayersInBundle;
            public byte StandardEjectArmBack;

            public ushort StackerPulsesPrMeter;

            public uint DPulseReleaseToEntrance; //Pulse distance from Release/Infeed to Entrance counter
            public uint DPulseEntranceToIntercept; //Pulse distance from Entrance counter to Intercept
        }

        //[StructLayout(LayoutKind.Sequential, Pack = 1)]
        //public struct SetupStruct
        public class SetupStruct
        {
            public EnumModes Mode = EnumModes.STANDARD;
            public byte CurrentLineId = 1;//gripper line (1-2)
            //public byte MaxBundlesNextMachineStopped = 1;
            //public short DelayMsBeforeReleaseBundleAfterStop;

            public byte IsPaused = 0;
            public byte IsSimulatingCopiesInGripper = 0;

            public byte IsInkEnable = 1;

            public byte IsRealcomEnable;

            public ushort ResetMMBeforeAction; //0 = pulses from laser counter distance

            public byte InkReadyDISlot;
            public byte InkReadyDISignal;
            public byte InkReadyOnHigh;

            public byte InkFinishDISlot;
            public byte InkFinishDISignal;
            public byte InkFinishOnHigh;

            public ushort InkFinishMSTimeout;
            public ushort ReadComMSTimeout;

            public byte StartInkDOSlot;
            public byte StartInkDOSignal;

            public byte EnableInkDOSlot;
            public byte EnableInkDOSignal;

            //public ushort StandardCopiesInLayer = 30;
            //public byte StandardLayersInBundle = 2;
            //public byte StandardEjectArmBack = 0;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public GripperLineSetupStruct GripperLines;
            //public GripperLineSetupStruct[] GripperLines;

            public byte NumStackers;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = MAXSTACKERLINES)]
            public StackerSetupStruct[] Stackers;

            #region Stacker internal control settings - commented
            #endregion
        }

        public struct DbCommandStruct
        {
            public uint CommandId;
            public EnumDataBaseCommands Command;
            public byte Status;
            public byte Status2;
            public byte LineID;
            public byte StackerLineId; //Abl group ID
            public byte AblId;
            //public ushort EditionTableId;
            //public byte PrinterId;
            public ushort BundleNumber;
            public uint TableId;
            public int CopyNumber;
        }
        #endregion

        #region Static variables
        //public static bool IsLineControllerStarted;
        public static string _connectionStringServer;
        public static string[] _connectionString;

        public static EnumMachineStatus ProductionUTRStatus;
        public static EnumMachineStatus ProductionALSStatus;
        //public static EnumMachineStatus[] ProductionABLStatus = new EnumMachineStatus[NUMABL];
        //public static EnumMachineStatus ProductionPZFStatus;
        //public static EnumMachineStatus ProductionSTBStatus;
        //public static EnumMachineStatus ProductionNextMachineStatus;
        //public static EnumMachineStatus[] ProductionPrinterStatus = new EnumMachineStatus[NUM_DBH];

        #endregion

        #region Variables
        public int CommandNumber;
        //private readonly uint _lastBadABLBundleId;
        private List<uint> _lastBadABLBundleIdList;
        //private UTRMain _ttrMain;

        //private readonly object _lockStatus;
        private readonly object _lockTelegramListIcp;
        private OleDbConnection _conServer;
        private Thread _ProductionControlAllThread;
        private Thread _TopsheetThread;
        private Thread _icpStackerThread;
        private Thread _dbThreadServer;
        private Thread _SimulationControlThread;
        private Thread _productionStatusCheckThread;
        private Thread _gripperInfoSaveThread;
        private Thread _ProductionStartThread;
        private Thread _productionCurrentModeThread;
        private Thread _firstLastCopySaveThread;

        private TcpClient _tcpClientIcpStacker;
        private NetworkStream _networkStreamIcpStacker;
        private BinaryReader _binaryReaderIcpStacker;
        private BinaryWriter _binaryWriterIcpStacker;
        private List<byte[]> _telegramListIcpStacker;

        private readonly byte _pcStatusStacker;
        private EnumControlStates _LCCStatus;
        private byte _telegramIdStacker;
        private byte[] _telegramRequestStatus;
        private byte[] _telegramStartProduction;
        private byte[] _telegramStopProduction;
        private byte[] _telegramStartLineController;
        private byte[] _telegramStopLineController;
        private byte[] _telegramReset;
        private byte[] _telegramClearCounters;
        private byte[] _telegramBundleSetting;

        private readonly string _ioIpGripperLine;
        private readonly string[] _ioIpTopSheet;
        private readonly string[] _ioIpPZF;
        private readonly string[] _ioIpUTR;
        private readonly string[] _printerName1;
        private readonly string[] _printerName2;
        //private readonly string[] _printerManual1;
        //private readonly string[] _printerManual2;
        private readonly string[] _printerIp1;
        private readonly string[] _printerIp2;
        private readonly int _ioPortGripperLine;
        private readonly int _printerPort1;
        private readonly int _printerPort2;
        private readonly int _ioPortPZF;
        private readonly int _ioPortTopSheet;

        public uint lastGripperSpeedUpdate = 1;
        public uint lastMainCopieCounterUpdate = 1;
        public uint lastOverflowCounterUpdate = 1;
        public int lastBruttoCounterUpdate = 1;
        public int lastNettoCounterUpdate = 1;
        public uint lastNumCopiesStackerOnWay = 1;
        public uint lastNumCopiesStackerFinished = 1;

        //private bool _doReconnectGripperPAC;
        private bool _doReconnectDbServer;
        //private bool _IsFirstBundle;

        private byte lastUTRStatus = 0;
        private byte lastALSStatus = 0;

        private bool isFirstCopySet = false;
        private byte isFirstTimeStamp = 0;

        private readonly uint[] lastABLCopieCounter = new uint[MAXSTACKERLINES];
        private readonly byte[] lastABL1Status = new byte[MAXSTACKERLINES];
        private readonly byte[] lastABL2Status = new byte[MAXSTACKERLINES];
        private readonly byte[] lastABL3Status = new byte[MAXSTACKERLINES];
        private readonly byte[] lastPZFStatus = new byte[MAXSTACKERLINES];
        private readonly byte[] lastSTBStatus = new byte[MAXSTACKERLINES];
        private readonly byte[] lastPrinter1Status = new byte[MAXSTACKERLINES];
        private readonly byte[] lastPrinter2Status = new byte[MAXSTACKERLINES];
        private readonly byte[] lastNextMachineStatus = new byte[MAXSTACKERLINES];
        private readonly bool[] lastProductionStartStatus = new bool[MAXSTACKERLINES];


        private SetupStruct _setup;
        public StatusStruct _status;
        //private int _setupStructSize;
        private readonly int _statusStructSize;
        private List<DbCommandStruct> _dbCommandListServer;
        private readonly object _lockDbCommandListServer;

        //private readonly List<BundleData> _newProductionQueue;
        //private readonly List<BundleData> production_list;

        //private readonly object _lockNewProductionQueue;
        //private readonly object _lockProductionList;

        private uint _nextDbCommandIdServer;
        //private DateTime _lastProductionDataSent;
        private readonly DateTime _lastLineStatusSent;
        private List<int> _dbProcessTicksServer;
        private bool _statusReceived;
        //private bool _layerCommandsReceived;
        //private bool _newBundlesReceived;


        //private List<BundleData> FirstLastRouteBundles;

        //private byte SeparationId;
        //private string _stackerId;
        private List<StackerLine> _stackerList = new List<StackerLine>();

        //private uint _lastBundleIDSent;
        //private byte[] _lastLayerIDSent;

        EnumControlStates TopSheetControlState;
        //EnumControlStates PZFControlState;

        //private byte CurJetID; // 1 or 2.

        //private uint _minCopiesInLayer; // For Stacker2 only
        //List<StackerLine> stacker_list = _stackerList;

        ProductionDataStruct CurProductionData;
        //private bool _ProductionStartReceived;
        private readonly byte LineId; //Gripperline ID

        public uint[] CopiesStacker;
        public bool EnableTopSheet;
        public int CurrentMode { get; set; }

        public int NumberofPages;
        public int NumberofStandardLayer;
        public int StandardBundleSize;
        public int CopyToPressLimit;
        public int CopiePerGripper;
        public int EdgeSupportPulseValue;

        public int StackerLineIdForABLGroupSetting = 0;
        public int CountABLDistance;
        public int ReleaseStartDelay;
        public int ReleaseStopDelay;
        public int StrikeStartDelay;
        public int StrikeStopDelay;
        public int LayerCycleTime;
        public int BundleCycleTime;
        public int ABLStopJam;
        public int UTRStopJam;

        public bool ABLGroupCommandReceived = false;
        public bool isSTDWithoutTop;
        public bool isNoTopsheet;

        #endregion

        #region Constructor
        public LineControlMain(byte _LineId, string ioIpGripperLine, string[] ConnectionString, bool _isStdWithoutTop, bool _isNoTopsheet)
        {
            CurrentMode = 1;
            EnableTopSheet = false;
            _LCCStatus = EnumControlStates.Unknown;
            LineId = _LineId;

            isSTDWithoutTop = _isStdWithoutTop;
            isNoTopsheet = _isNoTopsheet;

            string exeName = Assembly.GetExecutingAssembly().GetModules()[0].FullyQualifiedName;
            string exePath = Path.GetDirectoryName(exeName);
            Logging.LogFolder = exePath + "\\Log";

            _connectionStringServer = "Provider=SQLOLEDB.1;" +
                "Server=" + ConnectionString[0] + ";" +
                "Database=" + ConnectionString[1] + ";" +
                "Uid=" + ConnectionString[2] + ";" +
                "Pwd=" + ConnectionString[3] + "";

            _connectionString = ConnectionString;
            _statusReceived = false;

            _lastBadABLBundleIdList = new List<uint>();

            //_IsFirstBundle = true;
            //_lockNewProductionQueue = new object();
            //_lockProductionList = new object();

            //_lockStatus = new object();
            _lockTelegramListIcp = new object();

            _conServer = null;
            IsStarted = false;


            IcpConnectionState = EnumIcpConnectionStates.NotRunning;
            DbConnectionStateServer = EnumDbConnectionStates.DbUnknown;
            TopSheetControlState = EnumControlStates.NotRunning;

            _telegramListIcpStacker = new List<byte[]>();
            _telegramRequestStatus = null;
   
            _setup = new SetupStruct
            {
                GripperLines = new GripperLineSetupStruct(),
                Stackers = new StackerSetupStruct[MAXSTACKERLINES] //
            };

            for (int i = 0; i < _setup.Stackers.Length; i++) //
            {
                _setup.Stackers[i] = new StackerSetupStruct();
            }

            CurProductionData = new ProductionDataStruct
            {
                NumBatch = 4,
                StandardBundle = 100,
                NumCopiesLayer = 25,
                TitleGroupId = -1,
                IssueDate = ""
            };

            //_ProductionStartReceived = false;

            _status = new StatusStruct
            {
                ActiveState = 0,
                StackerLineStatus = new StackerLineStatusStruct[MAXSTACKERLINES]
            };

            for (int i = 0; i < MAXSTACKERLINES; i++)
            {
                _status.StackerLineStatus[i] = new StackerLineStatusStruct();
            }

            _statusStructSize = Marshal.SizeOf(typeof(StatusStruct));
            _dbCommandListServer = new List<DbCommandStruct>();
            _lockDbCommandListServer = new object();

            //_newProductionQueue = new List<BundleData>();
            //production_list = new List<BundleData>;

            //_lastBundleIDSent = 0;
            //_newBundlesReceived = false;
            //_lastProductionDataSent = new DateTime();

            _lastLineStatusSent = DateTime.MinValue;
            _dbProcessTicksServer = new List<int>();

            _ioIpGripperLine = ioIpGripperLine;  //"172.22.22.181"; //"172.22.22.180"; //"192.168.0.250"; 
            _ioPortGripperLine = 10000;
            _ioIpTopSheet = new string[MAXSTACKERLINES];
            _ioIpTopSheet[0] = "10.110.1.14";//"172.22.22.182"; //Small bundle for Stacker1
            _ioIpTopSheet[1] = "10.110.1.24";
            _ioIpTopSheet[2] = "10.110.1.34";
            _ioIpTopSheet[3] = "10.110.1.44";
            _ioPortTopSheet = 10000;

            _ioIpPZF = new string[MAXSTACKERLINES];
            _ioIpPZF[0] = "10.110.1.13";
            _ioIpPZF[1] = "10.110.1.23";
            _ioIpPZF[2] = "10.110.1.33";
            _ioIpPZF[3] = "10.110.1.43";
            _ioPortPZF = 10000;


            _ioIpUTR = new string[NUM_GRIPPERLINE];
            _ioIpUTR[0] = "10.110.0.81";
            _ioIpUTR[1] = "10.110.0.82";
            _ioIpUTR[2] = "10.110.0.83";
            _ioIpUTR[3] = "10.110.0.84";
            _ioIpUTR[4] = "10.110.0.85";
            _ioIpUTR[5] = "10.110.0.86";
            _ioIpUTR[6] = "10.110.0.87";
            _ioIpUTR[7] = "10.110.0.88";

            //_ttrMain = new UTRMain(ConnectionString, _LineId, _ioIpUTR[_LineId-1]);

            _printerName1 = new string[MAXSTACKERLINES];
            _printerName2 = new string[MAXSTACKERLINES];

            _printerName1[0] = "SPHPrinter1A";
            _printerName1[1] = "SPHPrinter1B";
            _printerName1[2] = "SPHPrinter1C";
            _printerName1[3] = "SPHPrinter1D";

            _printerName2[0] = "SPHPrinter2A";
            _printerName2[1] = "SPHPrinter2B";
            _printerName2[2] = "SPHPrinter2C";
            _printerName2[3] = "SPHPrinter2D";

            //_printerManual1 = new string[MAXSTACKERLINES];
            //_printerManual2 = new string[MAXSTACKERLINES];

            //_printerManual1[0] = "Manual_A1";
            //_printerManual1[1] = "Manual_B1";
            //_printerManual1[2] = "Manual_C1";
            //_printerManual1[3] = "Manual_D1";

            //_printerManual2[0] = "Manual_A2";
            //_printerManual2[1] = "Manual_B2";
            //_printerManual2[2] = "Manual_C2";
            //_printerManual2[3] = "Manual_D2";

            _printerIp1 = new string[MAXSTACKERLINES];
            _printerIp2 = new string[MAXSTACKERLINES];

            _printerIp1[0] = "10.110.1.11";
            _printerIp1[1] = "10.110.1.21";
            _printerIp1[2] = "10.110.1.31";
            _printerIp1[3] = "10.110.1.41";

            _printerIp2[0] = "10.110.1.12";
            _printerIp2[1] = "10.110.1.22";
            _printerIp2[2] = "10.110.1.32";
            _printerIp2[3] = "10.110.1.42";

            _printerPort1 = 9100; // 9100; //0
            _printerPort2 = 9100;
            _pcStatusStacker = 1;   //1=normal, 2=no ink, 3=paused, 4=stacker exit blocked

            //_stackerList = new List<StackerLine>();
            for (int i = 0; i < MAXSTACKERLINES; i++)
            {
                _stackerList.Add(new StackerLine((byte)(i + 1)));
            }

            for (int i = 0; i < MAXSTACKERLINES; i++)
            {
                _stackerList[i].PosLineID = 2;
                _stackerList[i].production_list = new List<BundleData>();
                _stackerList[i]._lockNewProductionQueue = new object();
                _stackerList[i]._lockProductionList = new object();
                _stackerList[i]._newProductionQueue = new List<BundleData>();
                _stackerList[i]._lastBundleIDSent = 0;
                _stackerList[i]._newBundlesReceived = false;
                _stackerList[i]._lastProductionDataSent = new DateTime();
                _stackerList[i].lastBundleCheck = DateTime.MinValue;
                _stackerList[i].RouteBundleSortindexList = new List<BundleData>();

                

                lastABLCopieCounter[i] = 0;
                lastABL1Status[i] = 0;
                lastABL2Status[i] = 0;
                lastABL3Status[i] = 0;
                lastPZFStatus[i] = 0;
                lastSTBStatus[i] = 0;
                lastPrinter1Status[i] = 0;
                lastPrinter2Status[i] = 0;
                lastNextMachineStatus[i] = 0;
                lastProductionStartStatus[i] = false;
            }

            _stackerList[0].DefaultStackerLinePrinter = 1;
            _stackerList[0].BackupStackerLinePrinter = 2;

            _stackerList[1].DefaultStackerLinePrinter = 2;
            _stackerList[1].BackupStackerLinePrinter = 1;

            _stackerList[2].DefaultStackerLinePrinter = 1;
            _stackerList[2].BackupStackerLinePrinter = 2;

            _stackerList[3].DefaultStackerLinePrinter = 2;
            _stackerList[3].BackupStackerLinePrinter = 1;


            //_stackerList[0].DefaultStackerLinePrinter = 1;
            //_stackerList[1].DefaultStackerLinePrinter = 1;
            //_stackerList[2].DefaultStackerLinePrinter = 1;
            //_stackerList[3].DefaultStackerLinePrinter = 1;

            //_stackerList[0].BackupStackerLinePrinter = 2;
            //_stackerList[1].BackupStackerLinePrinter = 1;
            //_stackerList[2].BackupStackerLinePrinter = 2;
            //_stackerList[3].BackupStackerLinePrinter = 1;


            //for(int i = 0; i < MAXSTACKERLINES; i++)
            //{
            //    _stackerList[i].IsStackerLineStarted = _isStackerLineStarted[i];
            //}
        }

        //public void TestSendSetupBAHauk()
        //{
        //    _stackerList[0].TopSheetMain.TestSendSetupBA();
        //}

        //public ushort GetInputSignal(int SlotId, int BAId)
        //{
        //    try
        //    {
        //        ushort id = (ushort)SlotId;
        //        return _stackerList[BAId - 1].TopSheetMain.GetDigitalInputs(id);
        //    }
        //    catch
        //    {

        //    }
        //    return 0;
        //}

        //public void SendTestSignalToBA(int BAID, List<byte> list)
        //{
        //    try
        //    {
        //        _stackerList[BAID - 1].TopSheetMain.SendTestSignalToBA(list);

        //    }
        //    catch
        //    {

        //    }
        //}


        //public string SendSetupBAHauk(int BAID, List<byte> bl)
        //{
        //    List<byte> bl1 = bl;
        //    try
        //    {
        //        _stackerList[BAID - 1].TopSheetMain.SendSetupBA(bl1);
        //        return null;
        //    }
        //    catch //(Exception e)
        //    {
        //        string s = "line Controller is NOT RUNNING!";
        //        return s;
        //    }
        //}

        private void ResetProduction()
        {
            try
            {
                _setup.IsPaused = 0;

                for (int i = 0; i < MAXSTACKERLINES; i++)
                {
                    if (_stackerList[i].PosLineID == LineId &&
                       _stackerList[i].TopSheetMain != null)
                    {
                        _stackerList[i]._lastProductionDataSent = DateTime.MinValue;
                        _stackerList[i]._newBundlesReceived = false;
                        _stackerList[i]._lastBundleIDSent = 10; // not zero, because status recevied from ICP is 0 by default.

                        lock (_stackerList[i]._lockProductionList)
                        {
                            if (_stackerList[i].production_list.Count > 0)
                            {
                                _stackerList[i].production_list.Clear();
                            }
                        }
                    }
                }
            }
            catch
            {
            }
        }

        //private BundleData GetNewBundle(uint bundleId, ushort numCopies)
        //{
        //    BundleData result = new BundleData
        //    {
        //        BundleId = bundleId,
        //        NumCopies = numCopies,
        //        StackerLineId = 0,
        //        Status = EnumBundleStatus.bundleStatusReady
        //    };

        //    return result;
        //}

        #endregion

        #region Properties
        public bool IsStarted { get; set; }
        public bool IsProductionStarted { get; set; }
        public bool IsStatusCheckStarted { get; set; }
        public int ProductionListSize { get; set; }
        public EnumControlStates ProductionControlState { get; set;}
        //public enumLineControlStates LineControlState { get; set; }
        //public EnumControlStates TopSheetControlState { get; set; }
        public EnumIcpConnectionStates IcpConnectionState { get; set; }
        public EnumDbConnectionStates DbConnectionStateServer { get; set; }

        //public EnumDbConnectionStates DbConnectionStateLocal { get; set; }
        //public uint CopiesPrHour { get; set; }

        public byte PcStatusStacker
        {
            get
            {
                return _pcStatusStacker;
            }
        }

        public byte SimulationStatus
        {
            get
            {
                return _status.isSimulate;
            }
        }

        public SetupStruct SetupLine
        {
            get { return _setup; }
        }

        public StatusStruct StatusLine
        {
            get { return _status; }
        }

        public uint GripperSpeed
        {
            get
            {
                return _status.GripperSpeedPrHour;
                //    (ushort)((uint)_status.GripperSpeedPrHour / 100) * 100;
            }
        }

        public byte StackerLineId
        {
            get
            {
                byte result = 0;
                for (int i = 0; i < _stackerList.Count; i++)
                {
                    if (_stackerList[i].PosLineID == LineId )
                    {
                        result = _stackerList[i].StackerLineId;
                    }
                }
                return result;
            }
        }

        public byte LabelQueueCount1
        {
            get
            {
                byte result = 0;
                for (int i = 0; i < _stackerList.Count; i++)
                {
                    if (_stackerList[i].PosLineID == LineId && _stackerList[i].TopSheetMain != null)
                    {
                        result = _stackerList[i].TopSheetMain.TopSheetQueue1;
                    }
                }
                return result;
            }
        }

        public byte LabelQueueCount2
        {
            get
            {
                byte result = 0;
                for (int i = 0; i < _stackerList.Count; i++)
                {
                    if (_stackerList[i].PosLineID == LineId && _stackerList[i].TopSheetMain != null)
                    {
                        result = _stackerList[i].TopSheetMain.TopSheetQueue2;
                    }
                }
                return result;
            }
        }

        public uint CopiesMainCounter
        {
            get { return _status.NumCopiesMainCounter; }
        }

        public uint NumCopiesStackerOnWay
        {
            get
            {
                uint result = 0;

                try
                {
                    for (int i = 0; i < _stackerList.Count; i++)
                    {
                        if (_stackerList[i].PosLineID == LineId && _stackerList[i].TopSheetMain != null)
                        {
                            for (int j = 0; j < MAXABLPerGroup; j++)
                            {
                                result = result + _status.StackerLineStatus[i].NumCopiesStackerOnWay[j];
                            }
                        }
                    }
                }
                catch
                {
                }
                return result;
            }
        }

        public bool isAlsStatusProductionStarted
        {
            get
            {
                if (_status.ProductionStarted == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool isAlsReceivedBundle
        {
            get
            {
                if (_status.isReceivedBundleInfo == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public uint NumCopiesStackerFinished
        {
            get
            {
                uint result = 0;

                try
                {
                    for (int i = 0; i < _stackerList.Count; i++)
                    {
                        if (_stackerList[i].PosLineID == LineId && _stackerList[i].TopSheetMain != null)
                        {
                            for (int j = 0; j < MAXABLPerGroup; j++)
                            {
                                result = result + _status.StackerLineStatus[i].NumCopiesStackerFinished[j];
                            }
                        }
                    }
                }
                catch
                {
                }
                return result;
            }
        }

        public uint[] NumCopiesStacker
        {
            get
            {
                uint[] result = new uint[MAXSTACKERLINES];

                try
                {
                    for (int i = 0; i < _stackerList.Count; i++)
                    {
                        result[i] = 0;

                        if (_stackerList[i].PosLineID == LineId && _stackerList[i].TopSheetMain != null)
                        {
                            for (int j = 0; j < MAXABLPerGroup; j++)
                            {
                                result[i] = result[i] + _status.StackerLineStatus[i].NumCopiesStackerFinished[j] + _status.StackerLineStatus[i].NumCopiesStackerOnWay[j];
                            }
                        }
                    }
                }
                catch
                {
                }
                return result;

            }
        }


        public uint[] NumCopiesCounterABLGroup
        {
            get
            {
                uint[] result = new uint[MAXSTACKERLINES];

                try
                {
                    for (int i = 0; i < _stackerList.Count; i++)
                    {
                        result[i] = 0;

                        if (_stackerList[i].PosLineID == LineId && _stackerList[i].TopSheetMain != null)
                        {
                            for (int j = 0; j < MAXABLPerGroup; j++)
                            {
                                result[i] = result[i] + _status.StackerLineStatus[i].CountersCopiesIn[j];
                            }
                        }
                    }
                }
                catch
                {
                }
                return result;

            }
        }


        //public int CopiesBruttoCounter
        //{
        //    get
        //    {
        //        try
        //        {
        //            if (_ttrMain != null)
        //            {
        //                return _ttrMain._status.lBruttoCounter;
        //            }
        //            else
        //            {
        //                return 0;
        //            }
        //        }
        //        catch
        //        {
        //            return 0;
        //        }

        //    }
        //}

        //public int CopiesNettoCounter
        //{
        //    get
        //    {
        //        try
        //        {
        //            if (_ttrMain != null)
        //            {
        //                return _ttrMain._status.lNettoCounter;
        //            }
        //            else
        //            {
        //                return 0;
        //            }
        //        }
        //        catch
        //        {
        //            return 0;
        //        }
        //    }
        //}

        public ulong TaktGripperCounter
        {
            get { return _status.TaktCountGripper; }
        }

        public ulong CopiesFinishedBundle
        {
            get { return _status.NumCopiesFinishedBundle; }
        }

        public ulong CopiesOverflow
        {
            get { return _status.NumCopiesOverflow; }
        }

        //public bool isABLSameLine
        //{
        //    get
        //    {
        //        bool _ablSameLine = false;
        //        try
        //        {
        //            if (_stackerList[0].PosLineID == LineId &&
        //                _stackerList[1].PosLineID == LineId &&
        //                _stackerList[2].PosLineID == LineId &&
        //                _stackerList[3].PosLineID == LineId)
        //            {
        //                _ablSameLine = true;
        //                return _ablSameLine;
        //            }
        //        }
        //        catch
        //        {
        //        }
        //        return _ablSameLine;
        //    }
        //}

        public bool[] isPausedStackerLine
        {
            get
            {
                bool[] result = new bool[NUM_STACKERLINE];
                try
                {
                    if(IcpConnectionState == EnumIcpConnectionStates.Connected)
                    {
                        for (int i = 0; i < NUM_STACKERLINE; i++)
                        {
                            if (_status.StackerLineStatus[i].IsPaused[0] == 1 &&
                                _status.StackerLineStatus[i].IsPaused[1] == 1 &&
                                _status.StackerLineStatus[i].IsPaused[2] == 1)
                            {
                                result[i] = true;
                            }
                            else
                            {
                                result[i] = false;
                            }
                        }
                    }
                }
                catch
                {
                }

                return result;
            }
        }

        public bool[][] isPausedStackerLineABL
        {
            get
            {
                bool[][] result = new bool[NUM_STACKERLINE][];
                try
                {
                    if (IcpConnectionState == EnumIcpConnectionStates.Connected)
                    {
                        for (int i = 0; i < NUM_STACKERLINE; i++)
                        {
                            result[i] = new bool[NUM_ABL];

                            for (int j = 0; j < NUM_ABL; j++)
                            {
                                if (_status.StackerLineStatus[i].IsPaused[j] == 1)
                                {
                                    result[i][j] = true;
                                }
                                else
                                {
                                    result[i][j] = false;
                                }
                            }
                        }
                    }
                }
                catch
                {
                }

                return result;
            }
        }

        //public bool noneABLOnThisLine
        //{
        //    get
        //    {
        //        bool _noneAblOnLine = false;
        //        try
        //        {
        //            if (_stackerList[0].PosLineID != LineId &&
        //                _stackerList[1].PosLineID != LineId &&
        //                _stackerList[2].PosLineID != LineId &&
        //                _stackerList[3].PosLineID != LineId)
        //            {
        //                _noneAblOnLine = true;
        //                return _noneAblOnLine;
        //            }
        //        }
        //        catch
        //        {
        //        }
        //        return _noneAblOnLine;
        //    }
        //}

        public EnumMachineStatus[][] ABLState
        {
            get
            {
                EnumMachineStatus[][] Result = new EnumMachineStatus[NUM_STACKERLINE][];
                try
                {
                    for (int i = 0; i < _stackerList.Count; i++)
                    {
                        Result[i] = new EnumMachineStatus[NUM_ABL];

                        for (int j = 0; j < NUM_ABL; j++)
                        {
                            Result[i][j] = new EnumMachineStatus();

                            if (_stackerList[i].PosLineID == LineId)
                            {
                                if (IcpConnectionState == EnumIcpConnectionStates.Connected)
                                {
                                    if (isPausedStackerLineABL[i][j])
                                    {
                                        Result[i][j] = EnumMachineStatus.AblPaused;
                                    }
                                    else
                                    {
                                        if (_status.StackerLineStatus[i].State[j] == 0 )
                                        {
                                            Result[i][j] = EnumMachineStatus.NotRunning;
                                        }
                                        else if (_status.StackerLineStatus[i].State[j] == 2)
                                        {
                                            Result[i][j] = EnumMachineStatus.NotReady;
                                        }
                                        else
                                        {
                                            Result[i][j] = EnumMachineStatus.Running;
                                        }
                                    }
                                }
                                else
                                {
                                    Result[i][j] = EnumMachineStatus.Unknown;
                                }
                            }
                            else
                            {
                                Result[i][j] = EnumMachineStatus.Unknown;
                            }
                        }
                    }
                    return Result;
                }
                catch
                {
                    return Result;
                }
               
            }
        }
        public EnumMachineStatus[] ABLGroupState
        {
            get
            {
                EnumMachineStatus[] temp = new EnumMachineStatus[NUM_STACKERLINE];

                try
                {
                    for (int i = 0; i < _stackerList.Count; i++)
                    {
                        if (_stackerList[i].PosLineID == LineId)
                        {
                            if (IcpConnectionState == EnumIcpConnectionStates.Connected)
                            {
                                if (isPausedStackerLine[i])
                                {
                                    temp[i] = EnumMachineStatus.AblGroupPaused;
                                }
                                else
                                {
                                    if (_status.StackerLineStatus[i].Status == 0)
                                    {
                                        temp[i] = EnumMachineStatus.NotRunning;
                                    }
                                    else
                                    {
                                        temp[i] = EnumMachineStatus.Running;
                                    }
                                }
                            }
                            else
                            {
                                temp[i] = EnumMachineStatus.Unknown;
                            }
                        }
                    }
                    return temp;
                }
                catch
                {
                    return temp;
                }
            }
        }
        public EnumMachineStatus ALSStatus
        {
            get
            {
                try
                {
                    if (IcpConnectionState == EnumIcpConnectionStates.Connected)
                    {
                        if (_status.ActiveState == 0)
                        {
                            return EnumMachineStatus.NotRunning;
                        }
                        else
                        {
                            return EnumMachineStatus.Running;
                        }
                    }
                    else
                    {
                        return EnumMachineStatus.Unknown;
                    }
                }
                catch
                {
                    return EnumMachineStatus.Unknown;
                }
            }
        }
        public EnumMachineStatus[] STBStatus
        {
            get
            {
                EnumMachineStatus[] temp = new EnumMachineStatus[NUM_STACKERLINE];

                try
                {
                    for (int i = 0; i < NUM_STACKERLINE; i++)
                    {
                        try
                        {
                            if (_stackerList[i].PosLineID == LineId && _stackerList[i].TopSheetMain != null)
                            {
                                temp[i] = _stackerList[i].STBMachineStatus;
                            }
                            else
                            {
                                temp[i] = EnumMachineStatus.Unknown;
                            }
                        }
                        catch
                        {
                        }
                    }

                    //EnumMachineStatus[] temp = 
                    //{
                    //    _stackerList[0].STBMachineStatus,
                    //    _stackerList[1].STBMachineStatus,
                    //    _stackerList[2].STBMachineStatus,
                    //    _stackerList[3].STBMachineStatus
                    //};

                    return temp;
                }
                catch
                {
                    return temp;
                }
                
            }
        }
        public EnumMachineStatus[] PZFStatus
        {
            get
            {
                EnumMachineStatus[] temp = new EnumMachineStatus[NUM_STACKERLINE];

                try
                {
                    for (int i = 0; i < NUM_STACKERLINE; i++)
                    {
                        try
                        {
                            if (_stackerList[i].TopSheetMain != null && _stackerList[i].PosLineID == LineId)
                            {
                                temp[i] = _stackerList[i].PZFMachineStatus;
                            }
                            else
                            {
                                temp[i] = EnumMachineStatus.Unknown;
                            }
                        }
                        catch
                        {
                        }
                    }

                    return temp;
                }
                catch
                {
                    return temp;
                }
                
            }
        }

        public string[] PrinterQueueCount
        {
            get
            {
                string[] temp = new string[NUM_STACKERLINE];

                try
                {
                    for (int i = 0; i < NUM_STACKERLINE; i++)
                    {
                        try
                        {
                            if (_stackerList[i].PosLineID == LineId && _stackerList[i].TopSheetMain != null)
                            {
                                if (_stackerList[i].TopSheetMain.TsBundleQueueAvoidTwoTopAtSametime != null)
                                {
                                    if (_stackerList[i].TopSheetMain.TsBundleQueueAvoidTwoTopAtSametime.Count < 4)
                                    {
                                        temp[i] = _stackerList[i].TopSheetMain.TsBundleQueueAvoidTwoTopAtSametime.Count.ToString();
                                    }
                                    else
                                    {
                                        temp[i] = "MAX";
                                    }
                                }
                            }
                            else
                            {
                                temp[i] = "";
                            }
                        }
                        catch
                        {
                        }
                    }

                    return temp;
                }
                catch
                {
                    return temp;
                }

            }
        }


        public Dictionary<string, string>[][] PrinterMessage
        {
            get
            {
                Dictionary<string, string>[][] result = new Dictionary<string, string>[NUM_STACKERLINE][];

                try
                {
                    for (int i = 0; i < NUM_STACKERLINE; i++)
                    {
                        result[i] = new Dictionary<string, string>[NUM_DBH];
                        if (_stackerList[i].PosLineID == LineId && _stackerList[i].TopSheetMain != null)
                        {
                            result[i] = _stackerList[i].PrinterMessage;
                        }
                        else
                        {
                            for (int j = 0; j < NUM_DBH; j++)
                            {
                                result[i][j] = new Dictionary<string, string>
                                        {
                                            { "PrintingStatus", "0" },
                                            { "PrinterStatus", "0" },
                                            //{ "RibbonLife", "" },
                                            { "StatusMessage", "OFFLINE" }
                                        };
                            }
                        }
                    }

                    return result;
                }
                catch
                {
                    return result;
                }
                
            }
        }

        //public Dictionary<string, string>[] PrinterStatus
        //{
        //    get
        //    {
        //        Dictionary<string, string>[] result = new Dictionary<string, string>[NUM_DBH];
        //        //string tempPrinterState;

        //        for (int i = 0; i < _stackerList.Count; i++)
        //        {
        //            if (_stackerList[i].PosLineID == LineId && _stackerList[i].TopSheetMain != null)
        //            {
        //                result = _stackerList[i].TopSheetMain._printerStatus;
        //            }
        //        }

        //        //for (int j = 0; j < NUM_DBH; j++)
        //        //{
        //        //    tempPrinterState = result[j]["StatusMessage"].ToUpper().Trim('.');
        //        //    if (tempPrinterState.Contains("OFFLINE"))
        //        //    {
        //        //        ProductionPrinterStatus[j] = EnumMachineStatus.Unknown;
        //        //    }
        //        //    else if (tempPrinterState.Contains("READY TO PRINT") || tempPrinterState.Contains("PROCESSING"))
        //        //    {
        //        //        ProductionPrinterStatus[j] = EnumMachineStatus.Running;
        //        //    }
        //        //    else
        //        //    {
        //        //        ProductionPrinterStatus[j] = EnumMachineStatus.NotRunning;
        //        //    }
        //        //}

        //        return result;
        //    }
        //}

        public EnumMachineStatus[][] PrinterStatus
        {
            get
            {
                EnumMachineStatus[][] result = new EnumMachineStatus[NUM_STACKERLINE][];

                try
                {
                    for (int i = 0; i < NUM_STACKERLINE; i++)
                    {
                        result[i] = new EnumMachineStatus[NUM_DBH];
                        for (int j = 0; j < NUM_DBH; j++)
                        {
                            if (_stackerList[i].PosLineID == LineId && _stackerList[i].TopSheetMain != null)
                            {

                                result[i][j] = new EnumMachineStatus();
                                _stackerList[i].UpdatePrinterMachineStatus();
                                result[i] = _stackerList[i].PrinterMachineStatus;
                            }
                            else
                            {
                                result[i][j] = EnumMachineStatus.Unknown;
                            }
                        }
                    }

                    return result;
                }
                catch
                {
                    return result;
                }
                
            }
        }
        public ushort[] LabelPosBundleId
        {
            get
            {
                ushort[] result = new ushort[NUM_DBH];
                for (int i = 0; i < _stackerList.Count; i++)
                {
                    if (_stackerList[i].PosLineID == LineId && _stackerList[i].TopSheetMain != null)
                    {
                        for (int j = 0; j < NUM_DBH; j++)
                        {
                            result[j] = _stackerList[i].TopSheetMain.LabelPositionBundleId[j];

                        }
                    }
                }
                return result;
            }
        }
        public ushort ApplicatorPosBundleId
        {
            get
            {
                ushort result = 0;
                for (int i = 0; i < _stackerList.Count; i++)
                {
                    if (_stackerList[i].PosLineID == LineId && _stackerList[i].TopSheetMain != null)
                    {
                        result = _stackerList[i].TopSheetMain.ApplicatorPositionBundleId;
                    }
                }
                return result;
            }
        }

        #endregion

        #region Public methods
        public void TestPrinter(byte PrinterID, byte StackerLineId)
        {
            try
            {
                if (_stackerList[StackerLineId].TopSheetMain != null)
                {
                    _stackerList[StackerLineId].TopSheetMain.TestPrint(PrinterID);
                }
                else
                {
                    System.Windows.MessageBox.Show("Line " + StackerLineId + " Connect Fail!");
                }
            }
            catch
            {
            }
        }

        //public void SetStackerLineStartedStatus(int isStackerLineGroupIndex)
        //{
        //    try
        //    {
        //        for (int i = 0; i < MAXSTACKERLINES; i++)
        //        {
        //            _stackerList[i].IsStackerLineGroupIndex = isStackerLineGroupIndex;
        //        }
        //    }
        //    catch
        //    {
        //    }
        //}


        public void SimulationMode(IcpTelegramTypes IcpType, byte _stackerLineId)
        {
            try
            {
                if (_stackerList[_stackerLineId].TopSheetMain != null)
                {
                    _stackerList[_stackerLineId].TopSheetMain.IcpGetOrAddTelegram(IcpType, null, true);
                }
            }
            catch
            {
            }
        }

        public void PaperTransport(byte stackerLine,byte printerId,bool isStart)
        {
            try
            {
                if (_stackerList[stackerLine].TopSheetMain != null)
                {
                    if (isStart)
                    {
                        _stackerList[stackerLine].TopSheetMain.IcpGetOrAddTelegram(IcpTelegramTypes.StartPaperTransport, printerId, true);
                    }
                    else
                    {
                        _stackerList[stackerLine].TopSheetMain.IcpGetOrAddTelegram(IcpTelegramTypes.StopPaperTransport, printerId, true);
                    }

                }
            }
            catch
            {
            }
        }

        public void PaperCuter(byte stackerLine, byte printerId)
        {
            try
            {
                if (_stackerList[stackerLine].TopSheetMain != null)
                {
                    _stackerList[stackerLine].TopSheetMain.IcpGetOrAddTelegram(IcpTelegramTypes.EnableCutter, printerId, true);
                }
            }
            catch
            {
            }
        }

        public void SetArmParameter(byte stackerLine, int ArmDownParameter)
        {
            try
            {
                if (_stackerList[stackerLine].TopSheetMain != null)
                {
                    _stackerList[stackerLine].TopSheetMain.IcpGetOrAddTelegram(IcpTelegramTypes.ArmDownParameterSet, ArmDownParameter, true);
                }
            }
            catch
            {
            }
        }


        public void Start()
        {
            try
            {
                if (IsStarted)
                {
                    Logging.AddToLog(LogLevels.Warning, LineId, "Line controller already Started, try to stop now");
                    Stop();
                }

                //_doReconnectGripperPAC = true;
                IsStarted = true;
                IsStatusCheckStarted = true;

                //TopSheetControlLib.TopSheetMain.IsLineControllerStarted = true;

                _icpStackerThread = new Thread(new ThreadStart(PacControl))
                {
                    Priority = ThreadPriority.Highest
                };

                _dbThreadServer = new Thread(new ThreadStart(DbControlServer));

                _productionStatusCheckThread = new Thread(new ThreadStart(ProductionStatusControl));
                _gripperInfoSaveThread = new Thread(new ThreadStart(GripperInfoSaveControl));
                _productionCurrentModeThread = new Thread(new ThreadStart(GetCurrentMode));
                _firstLastCopySaveThread = new Thread(new ThreadStart(FirstLastCopySaveControl));

                _icpStackerThread.Start();
                _dbThreadServer.Start();
                _productionStatusCheckThread.Start();
                _gripperInfoSaveThread.Start();
                _productionCurrentModeThread.Start();
                _firstLastCopySaveThread.Start();

               

                /*Wait ICP CONTROL STARTED*/
                DateTime CurTime = DateTime.Now;
                while (IcpConnectionState != EnumIcpConnectionStates.Connected)
                {
                    if (DateTime.Now >= CurTime.AddSeconds(60))
                    {//timeout start production
                        Logging.AddToLog(LogLevels.Warning, LineId, "Start linecontroller Timeout");
                        break;
                    }
                }//cheng

                if ((DateTime.Now < CurTime.AddSeconds(60)) && (IcpConnectionState == EnumIcpConnectionStates.Connected))
                {
                    StopProduction();
                    Logging.AddToLog(LogLevels.Warning, LineId, "Start linecontroller....");
                    StartLineController();

                    if (EnableTopSheet) //for test with or without topsheet
                    {
                        _TopsheetThread = new Thread(new ThreadStart(TopSheetControl));
                        _TopsheetThread.Start();
                    }
                }
                else
                {
                    //IsStarted = false;
                    Stop();
                }
            }
            catch (Exception ex)
            {
                Logging.AddToLog(LogLevels.Error, LineId, "Start linecontroller Error: " + ex.ToString());
            }

        }
        

        public void StartProduction()
        {
            try
            {
                if (!IsStarted)
                {
                    Start();
                    Thread.Sleep(800);
                }

                if (IsProductionStarted)
                {
                    StopProduction();
                    Thread.Sleep(2000);
                }
                _status.ProductionStarted = 1;// 1;//0; cheng

                Logging.AddToLog(LogLevels.Debug, LineId, "Start production");

                _lastBadABLBundleIdList.Clear();

                for (int i = 0; i < NUM_STACKERLINE; i++)
                {
                    if (_stackerList[i] != null)
                    {
                        //_stackerList[i].FirstLastRouteBundles.Clear();
                        _stackerList[i].RouteBundleSortindexList.Clear();
                    }
                }

                isFirstCopySet = false;

                //Send Start Production Command to ICP, Wait until it is received.
                DateTime CurTime = DateTime.Now;
                //_ProductionStartReceived = false;
                DbAddRequestedCommand(EnumDataBaseCommands.dbCommGetProductionData);
                //IcpGetOrAddTelegramStacker(EnumIcpTelegramTypes.StartProduction, null, true);
                //Reset STB and PZF
                ResetBABLC();
                ResetPZF();

                PZFSimulationStatusClear();

                //_ProductionStartReceived = true;

                //while (!_ProductionStartReceived)
                //{
                //    if (DateTime.Now >= CurTime.AddSeconds(90))
                //    {//timeout start production
                //        Logging.AddToLog(LogLevels.Warning, LineId, "Start production timeout");
                //        break;
                //    }
                //    Thread.Sleep(2000);
                //}


                //while (true)
                //{
                //    if (DateTime.Now >= CurTime.AddSeconds(1))
                //    {//timeout start production
                //        Logging.AddToLog(LogLevels.Warning, LineId, "Start production wait timeout");
                //        break;
                //    }
                //    //Thread.Sleep(2000);
                //}

                _ProductionStartThread = new Thread(new ThreadStart(ProductionStartControl));
                _ProductionStartThread.Start();


            }
            catch(Exception ex)
            {
                Logging.AddToLog(LogLevels.Error, LineId, "Start Production Error: " + ex.ToString());
            }

        }

        private void PZFSimulationStatusClear()
        {
            try
            {
                for (int i = 0; i < MAXSTACKERLINES; i++)
                {
                    if (_stackerList[i].TopSheetMain != null && _stackerList[i].PosLineID == LineId)
                    {
                        _stackerList[i].TopSheetMain.EnableDisableSimulation(SimulationStatus);
                    }
                }
            }
            catch
            {
            }
        }

        public void StartStopLineStatus(int LineId, int Status)
        {
            string _ConnectionString = @"Data Source=" + _connectionString[0] +
                   ";Initial Catalog=" + _connectionString[1] +
                   ";User ID=" + _connectionString[2] +
                   ";Password=" + _connectionString[3] + "";

            try
            {
                using (SqlConnection connection = new SqlConnection(_ConnectionString))
                {
                    SqlCommand command = new SqlCommand("[UniStack_StartStopLineUpdateStatus]", connection)
                    {
                        CommandText = "[UniStack_StartStopLineUpdateStatus]",
                        CommandType = CommandType.StoredProcedure
                    };
                    // Add the input parameter and set its properties.
                    command.Parameters.Add(new SqlParameter("@LineId", LineId));
                    command.Parameters.Add(new SqlParameter("@Status", Status));
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    reader.Close();
                    connection.Close();

                }
            }
            catch
            {
            }
        }

        public void StopProduction()
        {
            try
            {
                ResetProduction();
                IcpGetOrAddTelegramStacker(EnumIcpTelegramTypes.StopProduction, null, true);
                _LCCStatus = EnumControlStates.Running;

                if (IsProductionStarted)
                {
                    if (_ProductionControlAllThread != null)
                    {
                        _ProductionControlAllThread.Interrupt();
                    }

                    //if (_wakeUpPrinterThread != null)
                    //{
                    //    _wakeUpPrinterThread.Interrupt();
                    //}

                    if(_ProductionStartThread != null)
                    {
                        _ProductionStartThread.Interrupt();
                    }

                    if (_SimulationControlThread != null)
                    {
                        _SimulationControlThread.Interrupt();
                    }

                    for (int i = 0; i < MAXSTACKERLINES; i++)
                    {
                        if (_stackerList[i].PosLineID == LineId && _stackerList[i].TopSheetMain != null)
                        {
                            _stackerList[i].TopSheetMain.StopProduction();
                            _stackerList[i].TopSheetMain.IsProductionStarted = false;
                            //_stackerList[i].FirstLastRouteBundles.Clear();
                            _stackerList[i].RouteBundleSortindexList.Clear();
                        }
                    }
                }

                IsProductionStarted = false;

                Logging.AddToLog(LogLevels.Normal, LineId, "StopProduction()");
            }
            catch (Exception ex)
            {
                Logging.AddToLog(LogLevels.Error, LineId, "StopProduction Error: " + ex.ToString());
            }
        }
       

        public void StartLineController()
        {
            try
            {
                IcpGetOrAddTelegramStacker(EnumIcpTelegramTypes.StartLineController, null, true);
                _LCCStatus = EnumControlStates.Running;
            }
            catch
            {
            }
        }

        public void StopLineController()
        {
            try
            {
                for (int i = 0; i < MAXSTACKERLINES; i++)
                {
                    if (_stackerList[i].PosLineID == LineId &&
                        _stackerList[i].TopSheetMain != null &&
                        _stackerList[i].TopSheetMain.IcpConnectionState == IcpConnectionStates.Connected)
                    {
                        for (int j = 0; j < NUM_DBH; j++)
                        {
                            PaperTransport((byte)(i),(byte)(j+1),false);
                            Logging.AddToLog(LogLevels.Warning, LineId, "StackerLine: " + (i+1).ToString() + " Printer: " + (j+1).ToString() + " PaperTransport Stopped!");
                        }
                    }
                }

                IcpGetOrAddTelegramStacker(EnumIcpTelegramTypes.StopLineController, null, true);
                _LCCStatus = EnumControlStates.NotRunning;
                
                _productionStatusCheckThread.Interrupt();
                _gripperInfoSaveThread.Interrupt();
                _productionCurrentModeThread.Interrupt();
                _firstLastCopySaveThread.Interrupt();

                IsStatusCheckStarted = false;

            }
            catch
            {
            }
        }


        public void ResetBABLC()
        {
            try
            {
                if (TopSheetControlState == EnumControlStates.Running)
                {
                    for (int i = 0; i < _stackerList.Count; i++)
                    {
                        if (_stackerList[i].PosLineID == LineId && _stackerList[i].TopSheetMain != null)
                        {
                            _stackerList[i].TopSheetMain.ResetDone = false;
                            Logging.AddToLog(LogLevels.Warning, LineId, "ResetBABLC Reset Applicator: " + i);
                            _stackerList[i].TopSheetMain.ResetApplicator(true);
                            Thread.Sleep(10);
                        }
                    }
                }
            }
            catch
            {
            }
        }



        public void ResetOneBABLC(byte StackerLineId)
        {
            try
            {
                if (TopSheetControlState == EnumControlStates.Running)
                {
                    if (StackerLineId >= 0 && StackerLineId < _stackerList.Count)
                    {
                        if (_stackerList[StackerLineId].PosLineID == LineId && _stackerList[StackerLineId].TopSheetMain != null)
                        {
                            _stackerList[StackerLineId].TopSheetMain.ResetDone = false;
                            Logging.AddToLog(LogLevels.Warning, LineId, "ResetOneBABLC Reset Applicator: " + StackerLineId);

                            _stackerList[StackerLineId].TopSheetMain.ResetApplicator(true);
                            Thread.Sleep(10);
                        }
                    }
                }
            }
            catch
            {

            }
        }

        public void ResetPZF()
        {
            try
            {
                if (TopSheetControlState == EnumControlStates.Running)
                {

                    for (int i = 0; i < MAXSTACKERLINES; i++)
                    {
                        if (_stackerList[i].PosLineID == LineId && _stackerList[i].TopSheetMain != null)
                        {
                            _stackerList[i].TopSheetMain.PZFResetDone = false;
                            Logging.AddToLog(LogLevels.Warning, LineId, "ResetPZF: " + i);
                            _stackerList[i].TopSheetMain.ResetPZF(true);
                            Thread.Sleep(10);
                        }
                    }
                }
            }
            catch
            {

            }
        }

        public void ResetOnePZF(byte StackerLineId)
        {
            try
            {
                if (TopSheetControlState == EnumControlStates.Running)
                {
                    if (_stackerList[StackerLineId].PosLineID == LineId && _stackerList[StackerLineId].TopSheetMain != null)
                    {
                        _stackerList[StackerLineId].TopSheetMain.PZFResetDone = false;
                        Logging.AddToLog(LogLevels.Warning, LineId, "ResetPZF: " + StackerLineId);
                        _stackerList[StackerLineId].TopSheetMain.ResetPZF(true);
                        Thread.Sleep(10);
                    }
                }
            }
            catch
            {

            }
        }

        public void EnableSimulation()
        {
            try
            {
                if (TopSheetControlState == EnumControlStates.Running)
                {
                    for (int i = 0; i < _stackerList.Count; i++)
                    {
                        if (_stackerList[i].PosLineID == LineId && _stackerList[i].TopSheetMain != null)
                        {
                            _stackerList[i].TopSheetMain.EnableSimulation();
                            Thread.Sleep(10);
                        }
                    }
                }
            }
            catch
            {

            }
        }

        
        //public void StopPZFControl()
        //{

        //    if (PZFControlState == EnumControlStates.Running)
        //    {
        //        _pzfThread.Interrupt();

        //        for (int i = 0; i < _stackerList.Count; i++)
        //        {
        //            if (_stackerList[i].PZFMain != null)
        //            {
        //                _stackerList[i].PZFMain.Stop();
        //                Thread.Sleep(10);
        //            }
        //        }

        //        PZFControlState = EnumControlStates.NotRunning;
        //    }
        //}

        public void StopTopSheetControl()
        {
            if (TopSheetControlState == EnumControlStates.Running)
            {
                _TopsheetThread.Interrupt();

                for (int i = 0; i < _stackerList.Count; i++)
                {
                    if (_stackerList[i].TopSheetMain != null)
                    {
                        _stackerList[i].TopSheetMain.Stop();
                        Thread.Sleep(10);
                    }
                }

                TopSheetControlState = EnumControlStates.NotRunning;
            }
        }

        public void StopOneTopSheetControl(int StackerIndex)
        {
            try
            {
                if (StackerIndex > 0 && StackerIndex < _stackerList.Count)
                {
                    if (_stackerList[StackerIndex].TopSheetMain != null)
                    {
                        _stackerList[StackerIndex].TopSheetMain.Stop();
                        Thread.Sleep(10);
                    }
                }
            }
            catch
            {
            }
        }

        public void Stop()
        {
            try
            {

                if (IcpConnectionState == EnumIcpConnectionStates.Connected)
                {
                    if (IsProductionStarted)
                    {
                        StopProduction();
                    }

                    Logging.AddToLog(LogLevels.Warning, LineId, "Stop linecontroller....");
                    StoppedStatus();

                    //if(_ttrMain != null)
                    //{
                    //    _ttrMain.Stop();
                    //}

                    StopLineController();
                }

                Thread.Sleep(2000);

                IsStarted = false;
                //TopSheetControlLib.TopSheetMain.IsLineControllerStarted = false;

                Thread.Sleep(300);

                StopTopSheetControl();
                //StopPZFControl();

                int counter = 0;
                while (++counter < 20 &&
                    (ProductionControlState != EnumControlStates.NotRunning ||
                    TopSheetControlState != EnumControlStates.NotRunning ||
                    IcpConnectionState != EnumIcpConnectionStates.NotRunning ||
                    DbConnectionStateServer != EnumDbConnectionStates.DbUnknown
                    ))
                {
                    Thread.Sleep(100);
                }
            }
            catch
            {

            }
            

            if (IcpConnectionState != EnumIcpConnectionStates.NotRunning)
            {
                PacDisconnectGripper();

                try
                {
                    if (_icpStackerThread != null)// && _icpThread.ThreadState == ThreadState.Running)
                    {
                        _icpStackerThread.Interrupt();
                    }
                }
                catch
                {
                }
            }

            if (DbConnectionStateServer != EnumDbConnectionStates.DbUnknown)
            {
                DbDisconnectServer();

                try
                {
                    if (_dbThreadServer != null)// && _dbThread.ThreadState == ThreadState.Running)
                    {
                        _dbThreadServer.Interrupt();
                    }
                }
                catch
                {
                }
            }

            OleDbConnection con = null;

            try
            {
                //lock (_lockDbCommandListServer)
                {
                    _dbCommandListServer.Clear();
                }

                string sql = GetNextCommandString();

                EnumDataBaseCommands currentCommand = EnumDataBaseCommands.dbCommGetNextCommand;

                con = new OleDbConnection(_connectionStringServer);
                con.Open();
                DbHandleCommand(con, sql, currentCommand);
            }
            catch
            {
            }
            finally
            {
                if (con != null && con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }
        }

        //public void RequestResetStacker(byte StackerID)
        //{
        //    IcpGetOrAddTelegramStacker(EnumIcpTelegramTypes.Reset, StackerID, true);
        //}

        public void RequestPauseStacker(byte StackerID)
        {
            IcpGetOrAddTelegramStacker(EnumIcpTelegramTypes.PauseStacker, StackerID, true);
        }

        public void RequestResumeStacker(byte StackerID)
        {
            IcpGetOrAddTelegramStacker(EnumIcpTelegramTypes.ResumeStacker, StackerID, true);
            //Reset BA when ResumeStacker
            //ResetOneBABLC(StackerID);
        }

        public void RequestStartStacker(int StackerID)
        {
            try
            {
                //if (!_stackerList[StackerID - 1].TopSheetMain != null)//.IsProductionStarted)
                //{
                //if (_stackerList[StackerID - 1].PosLineID == LineId && _stackerList[StackerID - 1].TopSheetMain != null)
                //{
                //_stackerList[StackerID - 1].TopSheetMain.StartProduction();
                //_stackerList[StackerID - 1].TopSheetMain.IsProductionStarted = true;
                //}
                _stackerList[StackerID - 1].isStackerLineStoped = false;

                IcpGetOrAddTelegramStacker(EnumIcpTelegramTypes.StartStacker, (byte)StackerID, true);
                IcpGetOrAddTelegramStacker(EnumIcpTelegramTypes.ResumeStacker, (byte)StackerID, true);

                //return true;
                //}
                //else
                //{
                //    return false;
                //}
            }
            catch
            {
                //return false;
            }
        }

        public void RequestStopStacker(int StackerID)
        {
            try
            {
                //if (_stackerList[StackerID - 1].TopSheetMain.IsProductionStarted)
                //{
                if (_stackerList[StackerID - 1].PosLineID == LineId && _stackerList[StackerID - 1].TopSheetMain != null)
                {
                    _stackerList[StackerID - 1].isStackerLineStoped = true;
                    _stackerList[StackerID - 1].production_list.Clear();
                    _stackerList[StackerID - 1]._newProductionQueue.Clear();
                    //_stackerList[StackerID - 1].TopSheetMain.StopProduction();
                    //_stackerList[StackerID - 1].TopSheetMain.IsProductionStarted = false;
                    _stackerList[StackerID - 1].RouteBundleSortindexList.Clear();
                    _stackerList[StackerID - 1].TopSheetMain.UpdateBundleStatus(StackerID);

                }
                IcpGetOrAddTelegramStacker(EnumIcpTelegramTypes.StopStacker, (byte)StackerID, true);
                IcpGetOrAddTelegramStacker(EnumIcpTelegramTypes.PauseStacker, (byte)StackerID, true);


                //    return true;
                //}
                //else
                //{
                //    return false;
                //}
            }
            catch
            {
                //return false;
            }
        }

        public void RequestPauseOneStacker(byte StackerLineId, byte AblId)
        {
            ABL _abl = new ABL(AblId)
            {
                AblGroupId = StackerLineId,
                AblId = AblId
            };

            IcpGetOrAddTelegramStacker(EnumIcpTelegramTypes.PauseOneStacker, _abl, true);
        }

        public void RequestResumeOneStacker(byte StackerLineId, byte AblId)
        {
            ABL _abl = new ABL(AblId)
            {
                AblGroupId = StackerLineId,
                AblId = AblId
            };

            IcpGetOrAddTelegramStacker(EnumIcpTelegramTypes.ResumeOneStacker, _abl, true);
            //Reset BA when ResumeStacker
            //ResetOneBABLC(StackerID);
        }


        public void RequestClearCounters()
        {
            IcpGetOrAddTelegramStacker(EnumIcpTelegramTypes.ClearCounters, null, true);
            //if (_ttrMain != null)
            //{
            //    _ttrMain.IcpGetOrAddTelegram(UTRMain.IcpTelegramTypes.Reset, null, true);
            //}
            Logging.AddToLog(LogLevels.Warning, LineId, "Line Controller ClearCounter Success!");

            //IcpGetOrAddTelegramStacker(EnumIcpTelegramTypes.TestImajePrint, null, true);
            //if (_topSheetMain != null)
            //{
            //_topSheetMain.PrintBundle(11111);
            //    _topSheetMain.BundleEjected(10001);
            //}
        }

        public void RequestSendLayerPlanToICP()
        {
            IcpGetOrAddTelegramStacker(EnumIcpTelegramTypes.SendLayerCommand, null, true);
        }


        public void RequestProductionDataSendToStacker()
        {
            //IcpGetOrAddTelegramStacker(EnumIcpTelegramTypes.StartProduction, CurProductionData, true);
            IcpGetOrAddTelegramStacker(EnumIcpTelegramTypes.StartProduction, null, true);
        }

        public void RequestSetupSendToStacker()
        {
            IcpGetOrAddTelegramStacker(EnumIcpTelegramTypes.SendSetup, null, true);
        }

        public string[] GetStackerIds()
        {
            string[] result = null;
            try
            {
                string sql = "select distinct StackerId from StackerSetupTable order by StackerId";
                OleDbCommand cmd = new OleDbCommand(sql, _conServer);
                OleDbDataReader dataReader = cmd.ExecuteReader();
                List<string> idList = new List<string>();
                while (dataReader.Read())
                {
                    idList.Add((string)dataReader[0]);
                }
                result = idList.ToArray();
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.ToString());
            }
            return result;
        }

        #endregion

        #region TopSheetControl
        private void TopSheetControl()
        {
            try
            {
                //TopSheetControlLib.TopSheetMain.TopSheetApplicatorStates[] LastTopsheetStatus = new TopSheetControlLib.TopSheetMain.TopSheetApplicatorStates[MAXSTACKERLINES];
                //TopSheetControlLib.TopSheetMain.TopSheetPrinterStates[,] LastTopSheetPrinterState = 
                //    new TopSheetControlLib.TopSheetMain.TopSheetPrinterStates[NUM_DBH,MAXSTACKERLINES];
                TopSheetControlState = EnumControlStates.Running;

                DateTime _lastTopsheetStatusReset = new DateTime();
                int _lastPosLineId = 0;
                _lastTopsheetStatusReset = DateTime.MinValue;
                //bool IsFirstRest = true;

                while (IsStarted && _TopsheetThread != null && _TopsheetThread.ManagedThreadId == Thread.CurrentThread.ManagedThreadId)
                {
                    try
                    {
                        for (int i = 0; i < _stackerList.Count; i++)
                        {
                            StackerLine CurStackerLine = _stackerList[i];
                            //byte stackerNumber = stacker.StackerID;

                            if (CurStackerLine.PosLineID == LineId)// && CurStackerLine.IsStackerLineStarted)
                            {
                                _lastPosLineId = CurStackerLine.PosLineID;
                                if (CurStackerLine.TopSheetMain == null)// || !_topSheetMain.IsStarted)
                                {
                                    //LastTopsheetStatus[i] = TopSheetControlLib.TopSheetMain.TopSheetApplicatorStates.ApplicatorUnknown;

                                    CurStackerLine.TopSheetMain = new TopSheetMain(_connectionStringServer, i + 1,
                                        _ioIpTopSheet[i], _ioPortTopSheet, false,//true,
                                        _printerName1[i], _printerName2[i], //_printerManual1[i], _printerManual2[i],
                                        _printerIp1[i], _printerIp2[i], _printerPort1, _printerPort2,
                                        162, _connectionString, _ioIpPZF[i], _ioPortPZF,
                                        CurStackerLine.DefaultStackerLinePrinter, CurStackerLine.BackupStackerLinePrinter);

                                    //DbSetApplicatorStatus(_stackerList[i].TopSheetApplicatorState, _stackerList[i].StackerLineId); //Cheng 23/09/2019 SPH
                                    //Set printer status

                                    //for(int j=0; j<NUM_DBH;j++) //Cheng 23/09/2019 SPH
                                    //{
                                    //    LastTopSheetPrinterState[j,i] = TopSheetControlLib.TopSheetMain.TopSheetPrinterStates.PrinterUnknown;
                                    //    //DbSetPrinterStatus(_stackerList[i].TopSheetMain.PrinterStates[j], _stackerList[i].StackerLineId, (byte)(j + 1));
                                    //}
                                }
                                //Update Status here //Cheng 23/09/2019 SPH
                                //if (LastTopsheetStatus[i] != _stackerList[i].TopSheetApplicatorState)
                                //{
                                //    LastTopsheetStatus[i] = _stackerList[i].TopSheetApplicatorState;
                                //    DbSetApplicatorStatus(_stackerList[i].TopSheetApplicatorState, _stackerList[i].StackerLineId);
                                //}

                                //for (int j = 0; j < NUM_DBH; j++)
                                //{
                                //    if(LastTopSheetPrinterState[j, i] != _stackerList[i].TopSheetMain.PrinterStates[j])
                                //    {
                                //        //DbSetPrinterStatus(_stackerList[i].TopSheetMain.PrinterStates[j], _stackerList[i].StackerLineId, (byte)(j + 1));
                                //    }
                                //} //cheng 23/09/2019 SPH Combine in SaveStackerline

                                if (IsStarted && !CurStackerLine.TopSheetMain.IsStarted)
                                {
                                    CurStackerLine.TopSheetMain.Start(CurStackerLine.StackerLineId, LineId);
                                }

                                //if (IsProductionStarted)
                                //{
                                //    if (IcpConnectionState != EnumIcpConnectionStates.Connected || _status.StackerLineStatus[i].Status == 0) //|| _setup.IsPaused == 1) //|| stacker.ResetFromStacker //|| _status.StackerButtonResetState > 0
                                //    {
                                //        //stacker.ResetFromStacker = false;
                                //        if (IsFirstRest)
                                //        {
                                //            _lastTopsheetStatusReset = DateTime.Now;
                                //            IsFirstRest = false;
                                //        }
                                //        else
                                //        {
                                //            if (DateTime.Now >= _lastTopsheetStatusReset.AddSeconds(3))
                                //            {
                                //                IsFirstRest = true;
                                //                _lastTopsheetStatusReset = DateTime.Now;

                                //                if (CurStackerLine.TopSheetMain != null && CurStackerLine.TopSheetMain.PrintQueueWaiting > 0)
                                //                {
                                //                    CurStackerLine.TopSheetMain.ClearPrintQueueWaiting();
                                //                }
                                //                //Logging.AddToLog(LogLevels.Warning, LineId, "Topsheet " + CurStackerLine.StackerLineId +" Control Reset Applicator!");
                                //                CurStackerLine.TopSheetMain.ResetApplicator(true);
                                //            }
                                //        }
                                //        Thread.Sleep(500);
                                //    }
                                //}

                                CurStackerLine.TopSheetMain.PcStatus = 2;//label with tracking
                            }
                            else if (CurStackerLine.PosLineID != LineId )//&& !CurStackerLine.IsStackerLineStarted)
                            { //if not attached to current line, try to stop topsheetmain if it has been started
                                if (CurStackerLine.TopSheetMain != null)
                                {
                                    CurStackerLine.TopSheetMain.StopPZF();
                                    if (CurStackerLine.TopSheetMain.IsStarted)
                                    {
                                        CurStackerLine.TopSheetMain.Stop();
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logging.AddToLog(LogLevels.Warning, LineId, ex.ToString());
                    }
                    finally
                    {
                        Thread.Sleep(500);
                    }
                }
                TopSheetControlState = EnumControlStates.NotRunning;
            }
            catch (Exception ex)
            {
                Logging.AddToLog(LogLevels.Warning, LineId, ex.ToString());
            }
        }

        #endregion

        #region production start thread
        private void ProductionStartControl()
        {
            DateTime CurTime = DateTime.Now;
            while (IsStarted &&
                _ProductionStartThread != null &&
                _ProductionStartThread.ManagedThreadId == Thread.CurrentThread.ManagedThreadId)
            {
                if(DateTime.Now > CurTime.AddSeconds(3) || (_status.ProductionStarted == 1))
                {
                    //if (_status.ProductionStarted == 1) //cheng 09-20 SPH Value sometimes is 0, sometime is 1
                    //{
                        IsProductionStarted = true;
                        _ProductionControlAllThread = new Thread(new ThreadStart(ProductionControlAll));
                        _ProductionControlAllThread.Start();
                        //TestPrinter(1, StackerLineId);
                        //TestPrinter(2, StackerLineId);
                        //StartLine(LineId);

                        _SimulationControlThread = new Thread(new ThreadStart(SimulationControl));
                        _SimulationControlThread.Start();

                        Logging.AddToLog(LogLevels.Debug, LineId, "Start production Success");

                        //SPH Printer do not need Wake up! 20190812 cheng 
                        //_wakeUpPrinterThread = new Thread(new ThreadStart(WakeUpPrinterControl));
                        //_wakeUpPrinterThread.Start();

                        //Logging.AddToLog(LogLevels.Debug, LineId, "Wake up Printer Success");

                        for (int i = 0; i < MAXSTACKERLINES; i++)
                        {
                            if (_stackerList[i].PosLineID == LineId && _stackerList[i].TopSheetMain != null)
                            {
                                _stackerList[i].TopSheetMain.StartProduction();
                                _stackerList[i].TopSheetMain.IsProductionStarted = true;
                            }
                        }

                        _LCCStatus = EnumControlStates.Producing;
                        //DbStartLines();
                    //}
                    //else
                    //{
                    //    IsProductionStarted = false;
                    //    Logging.AddToLog(LogLevels.Warning, LineId, "Start production Failed, _status.ProductionStarted = " + _status.ProductionStarted);
                    //}

                    break;
                }
            }
        }
        #endregion

        #region Wake Up Printer Control
        //private void WakeUpPrinterControl()
        //{

        //    while (IsStarted &&
        //        _wakeUpPrinterThread != null &&
        //        _wakeUpPrinterThread.ManagedThreadId == Thread.CurrentThread.ManagedThreadId)
        //    {
        //        try
        //        {
        //            for (int i = 0; i < _stackerList.Count; i++)
        //            {
        //                if (_stackerList[i].PosLineID == LineId && 
        //                    _stackerList[i].TopSheetMain != null &&
        //                    CurrentMode == 1 && IsProductionStarted
        //                    && STBStatus[i] == EnumMachineStatus.Running)
        //                {
        //                    // Wake up Printer
        //                    for (uint j = 0; j < NUM_DBH; j++)
        //                    {
        //                        if (PrinterMessage[i][j]["StatusMessage"].ToUpper().Trim('.').Contains("SLEEPING"))
        //                        {
        //                            _stackerList[i].TopSheetMain.TestPrint(j + 1, true);
        //                            Logging.AddToLog(LogLevels.Debug, LineId, string.Format("Line {0}, StackerLine {1}, Printer {2} wake up. TimeStamp: {2} !", LineId, i+1, j + 1, DateTime.Now));
        //                        }
        //                    }
        //                    Thread.Sleep(5000);
        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            Logging.AddToLog(LogLevels.Error, LineId, string.Format("Line {0}, Printer Wake Up Error :{1}, !", LineId, ex.ToString()));
        //        }
        //        finally
        //        {
        //            Thread.Sleep(20000);
        //        }
        //    }
        //}

        #endregion

        #region Simulation Control
        private void SimulationControl()
        {
            try
            {
                byte lastSimulationCode = 0;
                Logging.AddToLog(LogLevels.Normal, LineId, "Simulation Control Started");

                while (IsStarted //&& IsProductionStarted 
                    && _SimulationControlThread != null
                    && _SimulationControlThread.ManagedThreadId == Thread.CurrentThread.ManagedThreadId)
                {
                    if (SimulationStatus != lastSimulationCode)
                    {
                        lastSimulationCode = SimulationStatus;
                        Logging.AddToLog(LogLevels.Normal, LineId, "Simulation Status: " + SimulationStatus);

                        for (int i = 0; i < MAXSTACKERLINES; i++)
                        {
                            if (_stackerList[i].TopSheetMain != null && _stackerList[i].PosLineID == LineId)
                            {
                                _stackerList[i].TopSheetMain.EnableDisableSimulation(SimulationStatus);
                            }
                        }
                    }
                    

                    Thread.Sleep(5000);
                }
            }
            catch
            {
            }
            finally
            {
                Thread.Sleep(10);
            }
        }

        #endregion

        #region Production control
        private void ProductionControlAll()
        {
            try
            {
                //DateTime[] lastBundleCheck = new DateTime[MAXSTACKERLINES];
                //for (int i = 0; i < MAXSTACKERLINES; i++)
                //{
                //    lastBundleCheck[i] = DateTime.MinValue;
                //}
                //DateTime lastStatusUpdate = DateTime.Now;

                ProductionControlState = EnumControlStates.Running;
                Logging.AddToLog(LogLevels.Normal, LineId, "ProductionControlAll started");
                ResetProduction();

                while (IsStarted && IsProductionStarted 
                    && _ProductionControlAllThread != null 
                    && _ProductionControlAllThread.ManagedThreadId == Thread.CurrentThread.ManagedThreadId)
                {
                    try
                    {
                        try
                        {
                            for (int i = 0; i < MAXSTACKERLINES; i++)
                            {
                                if (_stackerList[i].PosLineID == LineId &&
                                   _stackerList[i].TopSheetMain != null)
                                {
                                    //INSERT NEW BUNDLES TO PRODUCTION LISTS - BEGIN            
                                    lock (_stackerList[i]._lockNewProductionQueue)
                                    {
                                        if (_stackerList[i]._newBundlesReceived || _stackerList[i]._newProductionQueue.Count > 0)
                                        {
                                            if (_stackerList[i]._newProductionQueue.Count > 0)
                                            {
                                                lock (_stackerList[i]._lockProductionList)
                                                {
                                                    //if (_newProductionQueue.Count > 0)
                                                    while (_stackerList[i]._newProductionQueue.Count > 0)
                                                    {
                                                        Thread.Sleep(1);
                                                        _stackerList[i].production_list.Add(_stackerList[i]._newProductionQueue[0]);
                                                        _stackerList[i]._newProductionQueue.RemoveAt(0);
                                                        //production_list.AddRange(_newProductionQueue);
                                                    }
                                                    Logging.AddToLog(LogLevels.Debug, LineId, string.Format("New bundles -> StackerLine = {0} Production list - new size = {1}", i, _stackerList[i].production_list.Count));
                                                    //_newProductionQueue.Clear();                                                 
                                                }
                                            }
                                            _stackerList[i]._newBundlesReceived = false;
                                        }
                                    }
                                }
                            }

                            for (int i = 0; i < MAXSTACKERLINES; i++)
                            {
                                if (_stackerList[i].PosLineID == LineId &&
                                    _stackerList[i].TopSheetMain != null &&
                                    (ABLState[i][0] == EnumMachineStatus.Running ||
                                     ABLState[i][1] == EnumMachineStatus.Running ||
                                     ABLState[i][2] == EnumMachineStatus.Running )) // If ABL Not Running, Do Not Need Assign Bundle To StackerLine
                                {
                                    if (!_stackerList[i]._newBundlesReceived &&
                                        _stackerList[i]._newProductionQueue.Count == 0 &&
                                        DateTime.Now > _stackerList[i].lastBundleCheck.AddSeconds(3)) // && production_list[stackerIndex].Count < 5)
                                    {
                                        _stackerList[i].lastBundleCheck = DateTime.Now;

                                        if (_stackerList[i].production_list != null)
                                        {
                                            if (_stackerList[i].production_list.Count < 3 && IsProductionStarted && !_stackerList[i].isStackerLineStoped) // 9 //cheng SPH production list 27/09/2019
                                            {
                                                DbGetBundleList((ushort)(3 - _stackerList[i].production_list.Count), (byte)(i + 1)); //4
                                            }
                                        }
                                    }
                                }
                            }

                            for (int i = 0; i < MAXSTACKERLINES; i++)
                            {
                                if (_stackerList[i].PosLineID == LineId &&
                                   _stackerList[i].TopSheetMain != null)
                                {
                                    //PRODUCTION CONTROL - SEND LAYERS AND ADDRESSES - BEGIN
                                    if (_statusReceived && (IcpConnectionState == EnumIcpConnectionStates.Connected))
                                    {
                                        if (_stackerList[i].production_list != null)
                                        {
                                            if (_stackerList[i].production_list.Count > 0)
                                            {
                                                ProductionControl(_status.StackerLineStatus[i], i);
                                            }
                                        }
                                        
                                        //if (production_list.Count > 0)
                                        //{
                                        //        ProductionControl(_status);
                                        //}
                                    }
                                    //PRODUCTION CONTROL - SEND LAYERS AND ADDRESSES - END
                                }

                            }
                        }
                        catch (Exception ex)
                        {
                            Logging.AddToLog(LogLevels.Warning, LineId, ex.ToString());
                        }
                        finally
                        {
                            Thread.Sleep(500);
                        }
                    }
                    catch
                    {
                    }
                }

                for (int i = 0; i < _stackerList.Count; i++)
                {
                    if (_stackerList[i].PosLineID == LineId)
                    {
                        DbGetBundleList(0, (byte)(i + 1));
                    }
                }

                ProductionControlState = EnumControlStates.NotRunning;
                Logging.AddToLog(LogLevels.Normal, LineId, "ProductionControlAll() stopped, isStarted: " + IsStarted);
            }
            catch (Exception ex)
            {
                Logging.AddToLog(LogLevels.Error, LineId, ex.ToString());
            }
        }

        //private List<AddressData> GetAddressListOfALayer(byte StackerNr, BundleData _CurBundle, byte CurLayerNr)
        //{
        //    List<AddressData> CurAddressList = new List<AddressData>();
        //    if (_CurBundle.NextLayerIndex < _CurBundle.LayerCommandList.Count)
        //    {
        //        for (byte i = _CurBundle.NextAddressIndex; i < _CurBundle.NextAddressIndex + (byte)(_CurBundle.LayerCommandList[_CurBundle.NextLayerIndex].NumAddress); i++)
        //        {
        //            CurAddressList.Add(_CurBundle.AddressList[i]);
        //        }
        //    }
        //    return CurAddressList;
        //}

        private int ProductionControl(StackerLineStatusStruct stackerStatus, int stackerIndex)
        {
            try
            {
                BundleData bundleData = _stackerList[stackerIndex].production_list[0];

                if ((stackerStatus.NumRequestLayer > bundleData.NumLayersInBundle + 5) && 
                    (_stackerList[stackerIndex].production_list.Count > 0)) //Reserved 4 layers for safe.
                {
                    if (_stackerList[stackerIndex]._lastBundleIDSent == stackerStatus.LastReceivedBundleID)
                    { //receive the bundle, we can remove this bundle can send the next one.

                        lock (_stackerList[stackerIndex]._lockProductionList)
                        {
                            _stackerList[stackerIndex].production_list.RemoveAt(0);

                            if (_stackerList[stackerIndex].production_list.Count > 0)
                            {
                                bundleData = _stackerList[stackerIndex].production_list[0]; //Get next bundle                        
                            }
                            else
                            {
                                _stackerList[stackerIndex]._lastBundleIDSent = 10; //Reset bundleID, ready for reproduce.
                                return 1; //Finished production list for curStackerID
                            }
                        }

                        Logging.AddToLog(LogLevels.Debug, LineId,
                            string.Format("Bundle {0} NumLayer {1} " +
                            "Send to Stacker {2} NumRequestLayer {3} "+
                            "productListCount:{4} lastBundleIDSent: {5}",
                            bundleData.BundleId,
                            bundleData.NumLayersInBundle,
                            stackerIndex,
                            stackerStatus.NumRequestLayer,
                            _stackerList[stackerIndex].production_list.Count,
                            _stackerList[stackerIndex]._lastBundleIDSent
                            ));

                        IcpGetOrAddTelegramStacker(EnumIcpTelegramTypes.SendBundleData, bundleData, true);
                        _stackerList[stackerIndex]._lastBundleIDSent = bundleData.BundleId;
                        _stackerList[stackerIndex]._lastProductionDataSent = DateTime.Now;

                       
                    }
                    else if (DateTime.Now > _stackerList[stackerIndex]._lastProductionDataSent.AddSeconds(4))
                    { //resend

                        Logging.AddToLog(LogLevels.Debug, LineId,
                            string.Format("TitleGroup {0} Bundle {1} NumLayer {2} ReSend NumRequestLayer {3}" +
                            "productListCount:{4} lastBundleIDSent: {5}",
                            CurProductionData.TitleGroupId,
                            bundleData.BundleId, bundleData.NumLayersInBundle,
                            stackerStatus.NumRequestLayer, _stackerList[stackerIndex].production_list.Count, _stackerList[stackerIndex]._lastBundleIDSent
                            ));

                        IcpGetOrAddTelegramStacker(EnumIcpTelegramTypes.SendBundleData, bundleData, true);
                        _stackerList[stackerIndex]._lastBundleIDSent = bundleData.BundleId;
                        _stackerList[stackerIndex]._lastProductionDataSent = DateTime.Now;
                    }
                }

                Thread.Sleep(100);
                return 0;
            }
            catch (Exception ex)
            {
                Logging.AddToLog(LogLevels.Error, LineId, ex.ToString());
                return -1;
            }
        }

        // private int LayerControl(int StackerID, StackerStatusStruct stackerStatus, bool IsFirst)
        // {
        //    int result = 0;
        //    bool IsLastLayerInBundle = false;
        //    LayerData CurLayer = null;
        //    List<AddressData> CurAddressList = new List<AddressData>();

        //    if (production_list[StackerID-1].Count > 0)
        //    {
        //        BundleData bundleData = production_list[StackerID-1][0];

        //        if ((bundleData.BundleId == stackerStatus.LastReceivedBundleID) && (bundleData.NextLayerIndex == stackerStatus.LastReceivedLayerID))
        //        {
        //            SetBundleStatus(EnumBundleStatus.bundleStatusReservedLCC, (byte)(StackerID+1), bundleData.BundleId, 0);
        //            //bundleData.NextLayerIndex == 
        //            if (stackerStatus.LastReceivedLayerID == bundleData.NumLayersInBundle)
        //            {
        //                lock (_lockProductionList[StackerID-1])
        //                {
        //                    production_list[StackerID-1].RemoveAt(0);
        //                    if (production_list[StackerID-1].Count > 0)
        //                    {
        //                        bundleData = production_list[StackerID-1][0]; //Get next bundle                        
        //                    } else
        //                    {
        //                        return 1; //Finished production list for curStackerID
        //                    }
        //                }
        //            }
        //        } else {  //Wait until ICP received the bundleID and LayerID
        //            //Retransmit last bundleID and layerID when timeout, Todo
        //            if (!IsFirst && DateTime.Now < _lastProductionDataSent[StackerID-1].AddSeconds(3))
        //            {//Retransmit                    
        //                return 0;
        //            }   else {
        //                _lastProductionDataSent[StackerID-1] = DateTime.Now;
        //            }
        //        }

        //        if (stackerStatus.NumRequestLayer > 0 )  //&& (stackerStatus.NumRequestAddress > bundleData.LayerCommandList[bundleData.NextLayerIndex].NumAddress))
        //        {//In this case we can send this layer
        //            CurLayer = bundleData.GetNextLayerCommand(out IsLastLayerInBundle);
        //            IcpGetOrAddTelegramStacker(EnumIcpTelegramTypes.SendLayerCommand, CurLayer, true);
        //            CurAddressList = bundleData.GetAddressListOfCurLayer(CurLayer.LayerNumber-1);
        //            //GetAddressListOfALayer(stacker.StackerID, bundleData, bundleData.NextLayerIndex);

        //            if (CurAddressList.Count > 0)
        //            {
        //               // IcpGetOrAddTelegramStacker(EnumIcpTelegramTypes.SendAddressList, CurAddressList, true);
        //                lock (_lockAddressSendList[StackerID-1])
        //                {
        //                    for (int i = 0; i < CurAddressList.Count; i++)
        //                    {
        //                        _addressSendList[StackerID-1].Add(CurAddressList[i]);
        //                    }
        //                }
        //            }
        //            result = 2;
        //            Thread.Sleep(100);
        //        }
        //    }
        //    return result;
        //}

        public void SetBundleStatus(EnumBundleStatus status, byte StackerLineId, byte AblId, uint bundleNumber, int copyNumber)
        {
            StackerLine CurStackerLine = GetStackerLine(StackerLineId);
            //bool isNoTopSheet = false;

            //if (bundleNumber >= 50000)
            //{
            //    isNoTopSheet = true;
            //}

            if (CurStackerLine != null)
            {
                //if (status == EnumBundleStatus.bundleStatusInStacker && bundleNumber > 0)
                if(status == EnumBundleStatus.bundleStatusStackerFailed)
                {
                    Logging.AddToLog(LogLevels.Warning, LineId, "bundleStatusStackerFailed");
                }

                if (status == EnumBundleStatus.bundleStatusReleaseLast && bundleNumber > 0)
                {
                    //if (!isNoTopSheet && CurStackerLine.TopSheetMain != null &&
                    //    CurStackerLine.TopSheetMain.IsStarted)
                    //{
                    //    CurStackerLine.TopSheetMain.PrintBundle(bundleNumber);
                    //    //stacker.TopSheetMain.BundleEjected(bundleNumber);
                    //}

                    if(CurStackerLine.TopSheetMain.PZFIsStarted)
                    {
                        //EnumBundleInRoute BundleSequence = EnumBundleInRoute.Normal;

                        //for (int i = 0; i < CurStackerLine.FirstLastRouteBundles.Count(); i++)
                        //{
                        //    if (bundleNumber == CurStackerLine.FirstLastRouteBundles[i].BundleId &&
                        //        CurStackerLine.FirstLastRouteBundles[i].BundleSeqInRoute != EnumBundleInRoute.Normal)
                        //    {
                        //        if (CurStackerLine.FirstLastRouteBundles[i].BundleSeqInRoute == EnumBundleInRoute.FirstBundle)
                        //        {
                        //            BundleSequence = EnumBundleInRoute.FirstBundle;
                        //        }
                        //        else
                        //        {
                        //            BundleSequence = EnumBundleInRoute.LastBundle;
                        //        }

                        //        CurStackerLine.FirstLastRouteBundles.RemoveAt(i);
                        //    }
                        //}

                        BundleData BundleToPZF = new BundleData();
                        BundleToPZF.BundleId = bundleNumber;

                        if (bundleNumber < 60000)
                        {
                            for (int i = 0; i < CurStackerLine.RouteBundleSortindexList.Count(); i++)
                            {
                                if (bundleNumber == CurStackerLine.RouteBundleSortindexList[i].BundleId)
                                {
                                    BundleToPZF.BundleSeqInRoute = CurStackerLine.RouteBundleSortindexList[i].BundleSeqInRoute;
                                    BundleToPZF.RouteSortIndex = CurStackerLine.RouteBundleSortindexList[i].RouteSortIndex;
                                    BundleToPZF.BundleSortIndex = CurStackerLine.RouteBundleSortindexList[i].BundleSortIndex;
                                    BundleToPZF.IsNoTopsheet = CurStackerLine.RouteBundleSortindexList[i].IsNoTopsheet;
                                    BundleToPZF.IsSTDBundle = CurStackerLine.RouteBundleSortindexList[i].IsSTDBundle;
                                    BundleToPZF.RouteModeId = CurStackerLine.RouteBundleSortindexList[i].RouteModeId;

                                    CurStackerLine.RouteBundleSortindexList.RemoveAt(i);
                                }
                            }

                            CurStackerLine.TopSheetMain.PutIntoEjectQueue(CurProductionData.TitleGroupId, CurProductionData.IssueDate,
                                AblId, 0, BundleToPZF.BundleId, BundleToPZF.RouteSortIndex,
                                BundleToPZF.BundleSortIndex, BundleToPZF.BundleSeqInRoute, BundleToPZF.IsNoTopsheet, BundleToPZF.IsSTDBundle, BundleToPZF.RouteModeId);
                        }
                        else
                        {
                            CurStackerLine.TopSheetMain.PutIntoEjectQueue(CurProductionData.TitleGroupId, CurProductionData.IssueDate,
                                AblId, 0, BundleToPZF.BundleId, 999, 999, EnumBundleInRoute.OverFlow , true, true, 5);
                        }

                        //CurStackerLine.TopSheetMain.PutIntoEjectQueue((byte)CurProductionData.TitleGroupId, CurProductionData.IssueDate, bundleNumber, AblId, 0, BundleSequence);

                    }
                }
                else if(status == EnumBundleStatus.bundleStatusReleaseFirst && bundleNumber > 0)
                {
                    try
                    {
                        //Send BundleMessage to PZF to hold on Bundle follow the Bundle Sequence
                        if (StackerList[StackerLineId - 1].RouteBundleSortindexList != null)
                        {
                            if (StackerList[StackerLineId - 1].RouteBundleSortindexList.Count > 0)
                            {
                                for (int j = 0; j < StackerList[StackerLineId - 1].RouteBundleSortindexList.Count; j++)
                                {
                                    if (StackerList[StackerLineId - 1].RouteBundleSortindexList[j].BundleId == bundleNumber)
                                    {
                                        StackerList[StackerLineId - 1].TopSheetMain.LCCSendBundleMessage
                                            (StackerList[StackerLineId - 1].RouteBundleSortindexList[j].BundleId,
                                            StackerList[StackerLineId - 1].RouteBundleSortindexList[j].RouteSortIndex,
                                            StackerList[StackerLineId - 1].RouteBundleSortindexList[j].BundleSortIndex,
                                            AblId);

                                    }

                                }
                            }
                        }
                        
                    }
                    catch
                    {
                    }
                }
                else if (status == EnumBundleStatus.bundleStatusInStacker && bundleNumber > 0)
                {
                    //Check Bundle if isNoTopSheet
                    //If bundleId is more than 50000, then no topsheet.
                    //if (!isNoTopSheet && CurStackerLine.TopSheetMain != null && CurStackerLine.TopSheetMain.IsStarted)
                    //{
                    //    //stacker.TopSheetMain.PrintBundle(bundleNumber);
                    //    CurStackerLine.TopSheetMain.BundleEjected(bundleNumber);
                    //}
                    //PrintServerSendBundleReleasedToStacker(stackerNumber, bundleNumber);
                }
                //else if (status == EnumBundleStatus.bundleStatusAfterStacker && bundleNumber > 0)
                //{
                //    if (stacker.TopSheetMain != null && stacker.TopSheetMain.IsStarted)
                //    {
                //        //_topSheetMain.PrintBundle(bundleNumber);
                //       //stacker.TopSheetMain.BundleEjected(bundleNumber);
                //    }
                //    //PrintServerSendProducedBundle(stackerNumber, bundleNumber);
                //}
                //else if ((status == EnumBundleStatus.bundleStatusBundleError
                //    || status == EnumBundleStatus.bundleStatusStackerFailed
                //    || status == EnumBundleStatus.bundleStatusInkFailed)
                //    && bundleNumber > 0)
                //{
                //    if (CurStackerLine.TopSheetMain != null)
                //    {
                //        CurStackerLine.TopSheetMain.RemoveFromPrintQueueWaiting(bundleNumber);
                //    }
                //}
            }
            DbSetBundleStatus(status, StackerLineId, AblId, bundleNumber, copyNumber);
        }

        private StackerLine GetStackerLine(byte StackerLineID)
        {
            StackerLine result = null;
            int index = -1;

            try
            {
                while (result == null && ++index < _stackerList.Count)
                {
                    if (StackerLineID == _stackerList[index].StackerLineId)
                    {
                        result = _stackerList[index];
                    }
                }

                return result;
            }
            catch
            {
            }
            return result;
        }

        public List<StackerLine> StackerList { get { return _stackerList; } }

        private byte[] GetSetupStructBytes()//SetupStruct setup)
        {
            //int len = 0;// _setupStructSize;// Marshal.SizeOf(_setup);
            byte[] result;// = new byte[len];

            //IntPtr ptr = Marshal.AllocHGlobal(len);
            //Marshal.StructureToPtr(_setup, ptr, true);
            //Marshal.Copy(ptr, result, 0, len);

            //Marshal.FreeHGlobal(ptr);

            long tick = Environment.TickCount;

            List<byte> byteList = new List<byte>();
            List<FieldInfo> fieldList = new List<FieldInfo>();
            List<object> fieldOwnerList = new List<object>();
            object fieldOwner;
            FieldInfo fi;

            FieldInfo[] fiArray = typeof(SetupStruct).GetFields();
            IEnumerable<FieldInfo> fiSorted = fiArray.OrderBy(f => f.MetadataToken);
            IEnumerator<FieldInfo> fiEnumerator = fiSorted.GetEnumerator();

            //for (int i = 0; i < fiArray.Length; i++)
            while (fiEnumerator.MoveNext())
            {
                //fi = fiArray[i];
                fi = fiEnumerator.Current;

                if (fi.FieldType.IsArray)
                {
                    Array arrayField = (Array)fi.GetValue(_setup);
                    for (int i2 = 0; i2 < arrayField.Length; i2++)
                    {
                        fieldOwner = arrayField.GetValue(i2);
                        FieldInfo[] fiSubArray = fieldOwner.GetType().GetFields();
                        IEnumerable<FieldInfo> fiSubSorted = fiSubArray.OrderBy(f => f.MetadataToken);
                        IEnumerator<FieldInfo> fiSubEnumerator = fiSubSorted.GetEnumerator();

                        //for (int i3 = 0; i3 < fiSubArray.Length; i3++)
                        while (fiSubEnumerator.MoveNext())
                        {
                            //fi = fiSubArray[i3];
                            fi = fiSubEnumerator.Current;

                            fieldList.Add(fi);
                            fieldOwnerList.Add(fieldOwner);
                        }
                    }
                }
                else
                {
                    fieldOwner = _setup;
                    fieldList.Add(fi);
                    fieldOwnerList.Add(fieldOwner);
                }
            }

            tick = Environment.TickCount - tick;
            long tick2 = Environment.TickCount;

            for (int i = 0; i < fieldList.Count; i++)
            {
                try
                {
                    fi = fieldList[i];
                    fieldOwner = fieldOwnerList[i];

                    if (fi.FieldType.Equals(typeof(byte)) || fi.FieldType.IsEnum)
                    {
                        byte fieldValue = (byte)fi.GetValue(fieldOwner);
                        byteList.Add(fieldValue);
                    }
                    else if (fi.FieldType.Equals(typeof(short)))
                    {
                        short fieldValue = (short)fi.GetValue(fieldOwner);
                        byteList.Add((byte)(fieldValue % 256));
                        byteList.Add((byte)(fieldValue / 256));
                    }
                    else if (fi.FieldType.Equals(typeof(ushort)))
                    {
                        ushort fieldValue = (ushort)fi.GetValue(fieldOwner);
                        byteList.Add((byte)(fieldValue % 256));
                        byteList.Add((byte)(fieldValue / 256));
                    }
                    else if (fi.FieldType.Equals(typeof(int)))
                    {
                        int fieldValue = (int)fi.GetValue(fieldOwner);
                        byteList.Add((byte)(fieldValue % 256));
                        byteList.Add((byte)((fieldValue >> 8) & 0xFF));
                        byteList.Add((byte)((fieldValue >> 16) & 0xFF));
                        byteList.Add((byte)((fieldValue >> 24) & 0xFF));
                    }
                    else if (fi.FieldType.Equals(typeof(uint)))
                    {
                        uint fieldValue = (uint)fi.GetValue(fieldOwner);
                        byteList.Add((byte)(fieldValue % 256));
                        byteList.Add((byte)((fieldValue >> 8) & 0xFF));
                        byteList.Add((byte)((fieldValue >> 16) & 0xFF));
                        byteList.Add((byte)((fieldValue >> 24) & 0xFF));
                    }
                    else if (fi.FieldType.Equals(typeof(long)))
                    {
                        long fieldValue = (long)fi.GetValue(fieldOwner);
                        byteList.Add((byte)(fieldValue % 256));
                        byteList.Add((byte)((fieldValue >> 8) & 0xFF));
                        byteList.Add((byte)((fieldValue >> 16) & 0xFF));
                        byteList.Add((byte)((fieldValue >> 24) & 0xFF));

                        byteList.Add((byte)((fieldValue >> 32) & 0xFF));
                        byteList.Add((byte)((fieldValue >> 40) & 0xFF));
                        byteList.Add((byte)((fieldValue >> 48) & 0xFF));
                        byteList.Add((byte)((fieldValue >> 56) & 0xFF));
                    }
                    else if (fi.FieldType.Equals(typeof(ulong)))
                    {
                        ulong fieldValue = (ulong)fi.GetValue(fieldOwner);
                        byteList.Add((byte)(fieldValue % 256));
                        byteList.Add((byte)((fieldValue >> 8) & 0xFF));
                        byteList.Add((byte)((fieldValue >> 16) & 0xFF));
                        byteList.Add((byte)((fieldValue >> 24) & 0xFF));

                        byteList.Add((byte)((fieldValue >> 32) & 0xFF));
                        byteList.Add((byte)((fieldValue >> 40) & 0xFF));
                        byteList.Add((byte)((fieldValue >> 48) & 0xFF));
                        byteList.Add((byte)((fieldValue >> 56) & 0xFF));
                    }
                    else
                    {
                    }
                }
                catch// (Exception ex)
                {
                }
            }

            result = byteList.ToArray();

            tick2 = Environment.TickCount - tick2;

            return result;
        }

        #endregion

        #region PZFControl

        //private void PZFControl()
        //{
        //    try
        //    {
        //        TopSheetControlLib.TopSheetMain.PZFApplicatorStates[] LastPZFStatus = new TopSheetControlLib.TopSheetMain.PZFApplicatorStates[MAXSTACKERLINES];
        //        TopSheetControlLib.TopSheetMain.STBStates[] StbState = new TopSheetControlLib.TopSheetMain.STBStates[MAXSTACKERLINES];
        //        PZFControlState = EnumControlStates.Running;

        //        DateTime _PZFStatusReset = new DateTime();
        //        _PZFStatusReset = DateTime.MinValue;

        //        while (IsStarted &&
        //            _pzfThread != null &&
        //            _pzfThread.ManagedThreadId == Thread.CurrentThread.ManagedThreadId)
        //        {
        //            try
        //            {
        //                for (int i = 0; i < _stackerList.Count; i++)
        //                {
        //                    StackerLine CurStackerLine = _stackerList[i];
        //                    if (CurStackerLine.PosLineID == LineId)
        //                    {
        //                        if (CurStackerLine.PZFMain == null)
        //                        {
        //                            StbState[i] = TopSheetControlLib.TopSheetMain.STBStates.STBUnknown;
        //                            LastPZFStatus[i] = TopSheetControlLib.TopSheetMain.PZFApplicatorStates.ApplicatorUnknown;

        //                            CurStackerLine.PZFMain = new TopSheetControlLib.PZFMain(_connectionStringServer, i + 1,
        //                                _ioIpPZF[i], _ioPortPZF, _connectionString);
        //                        } else
        //                        {
        //                            if (IsStarted && !CurStackerLine.PZFMain.IsStarted)
        //                            {
        //                                CurStackerLine.PZFMain.Start(CurStackerLine.StackerLineId, _setup.CurrentLineId);
        //                            }

        //                            CurStackerLine.PZFMain.PcStatus = 2;
        //                        }
        //                    }
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                Logging.AddToLog(LogLevels.Warning, LineId, ex.ToString());
        //            }
        //            finally
        //            {
        //                Thread.Sleep(500);
        //            }
        //        }
        //        PZFControlState = EnumControlStates.NotRunning;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logging.AddToLog(LogLevels.Warning, LineId, ex.ToString());
        //    }
        //}

        #endregion

        #region Icp communication
        private void PacControl()
        {
            try
            {
                IcpConnectionState = EnumIcpConnectionStates.Disconnected;
                int readLength = _statusStructSize;
                BundleMessageStruct bundleMessage;

                //enumInkStatus LastInkStatus = enumInkStatus.Unknown;
                //enumStackerStatus[,] LastStackerStatus = new enumStackerStatus[MAXSTACKERLINES, MAXABLPerGroup];
                byte[] LastStackerPosId = new byte[MAXSTACKERLINES];

                for (int i = 0; i < MAXSTACKERLINES; i++)
                {
                    //for (int j = 0; j < MAXABLPerGroup; j++)
                    //{
                    //    LastStackerStatus[i, j] = enumStackerStatus.Unknown;
                    //}
                    LastStackerPosId[i] = 0;
                }

                Logging.AddToLog(LogLevels.Normal, LineId, "PacControl started" + _ioIpGripperLine);

                 DateTime lastPACCycleTime = DateTime.Now;
                //bool doSendStatusToDb = DateTime.Now > _lastLineStatusSent.AddSeconds(3);

                while (IsStarted && 
                    _icpStackerThread != null && 
                    _icpStackerThread.ManagedThreadId == Thread.CurrentThread.ManagedThreadId)
                {
                    
                    try
                    {
                        lastPACCycleTime = DateTime.Now;
                        try
                        {
                            if (_tcpClientIcpStacker == null || 
                                _tcpClientIcpStacker.Client == null || 
                                !_tcpClientIcpStacker.Client.Connected)
                            {
                                //for (int i = 0; i < MAXSTACKERLINES; i++)
                                //{
                                //    _stackerList[i].Status = enumStackerLineStatus.Connecting;
                                //}

                                //for (int i = 0; i < _stackerList.Count; i++)
                                //{
                                //    for (int j = 0; j < MAXABLPerGroup; j++)
                                //    {
                                //        if (LastStackerStatus[i, j] != _stackerList[i].ABLList[j].Status)
                                //        {
                                //            LastStackerStatus[i, j] = _stackerList[i].ABLList[j].Status;
                                //            DbSetStackerStatus(_stackerList[i].ABLList[j].Status, (byte)(i + 1), (byte)(j + 1));
                                //        }
                                //    }
                                //}

                                IcpConnectionState = EnumIcpConnectionStates.Disconnected;
                                _statusReceived = false;
                                //_layerCommandsReceived = true;

                                //AddToLog(true, LogLevels.Debug, "Icp connecting ...");
                                Logging.AddToLog(LogLevels.Debug, LineId, "PAC connecting ... " + _ioIpGripperLine + ":" + _ioPortGripperLine);//true, LogLevels.Warning, ex.ToString());

                                //if (_doReconnectGripperPAC)
                                //{
                                //    _doReconnectGripperPAC = false;
                                //}

                                _tcpClientIcpStacker = new TcpClient
                                {
                                    ReceiveTimeout = 1000,
                                    SendTimeout = 1000
                                };

                                //_tcpClientIcp.LingerState = new LingerOption(true, 0);

                                IcpConnectionState = EnumIcpConnectionStates.Connecting;

                                try
                                {
                                    //if (pingTest(_ioIpGripperLine))
                                    {
                                        _tcpClientIcpStacker.Connect(_ioIpGripperLine, _ioPortGripperLine);

                                        _networkStreamIcpStacker = _tcpClientIcpStacker.GetStream();
                                        _networkStreamIcpStacker.ReadTimeout = 1000;
                                        _networkStreamIcpStacker.WriteTimeout = 1000;

                                        _binaryReaderIcpStacker = new BinaryReader(_networkStreamIcpStacker);
                                        _binaryWriterIcpStacker = new BinaryWriter(_networkStreamIcpStacker);

                                        IcpConnectionState = EnumIcpConnectionStates.Connected;

                                        //RequestSetupSendToStackerHauk(GetHaukSetupStructure());
                                        //IcpGetOrAddTelegramStacker(EnumIcpTelegramTypes.SendSetup, null, true);

                                        Logging.AddToLog(LogLevels.Debug, LineId, "ALS PAC connected " + _ioIpGripperLine);//true, LogLevels.Warning, LineId, ex.ToString());

                                        //int bytesCleared = 0;
                                        //try
                                        //{
                                        //    while (_tcpClientIcpStacker.Client != null && _tcpClientIcpStacker.Client.Connected)
                                        //    {
                                        //        _binaryReaderIcpStacker.ReadByte();
                                        //        ++bytesCleared;
                                        //    }
                                        //}
                                        //catch (Exception ex)
                                        //{
                                        //    Logging.AddToLog(LogLevels.Warning, LineId, ex.ToString() + " clear bytes " + bytesCleared.ToString());//true, LogLevels.Warning, ex.ToString());
                                        //    Thread.Sleep(100);
                                        //}
                                    }
                                }
                                catch
                                {
                                    Thread.Sleep(3000);
                                }

                            }

                            if (_tcpClientIcpStacker != null && _tcpClientIcpStacker.Client != null && _tcpClientIcpStacker.Client.Connected)
                            {
                                byte[] telegram;
                                //bool isLayerCommand = false;

                                lock (_lockTelegramListIcp)
                                {

                                    if (_telegramListIcpStacker.Count > 0)
                                    {
                                        telegram = _telegramListIcpStacker[0];
                                        _telegramListIcpStacker.RemoveAt(0);

                                        //if (telegram[3] == (byte)'C')
                                        //{
                                        //    isLayerCommand = true;
                                        //}
                                    }
                                    else
                                    {
                                        telegram = IcpGetOrAddTelegramStacker(EnumIcpTelegramTypes.RequestStatus, null, false);

                                    }
                                }

                                //if (telegram[3] == (byte)'C')
                                //{
                                //    _layerCommandsReceived = true;
                                //}
                                _binaryWriterIcpStacker.Write(telegram);
                                _binaryWriterIcpStacker.Flush();

                                //byte btest = _binaryReader.ReadByte();

                                byte[] response = _binaryReaderIcpStacker.ReadBytes(readLength);
                                if (response.Length == readLength
                                    && response[0] == telegram[0]
                                    && response[1] == telegram[1]
                                    && response[2] == telegram[3]
                                    && response[readLength - 1] == 4)
                                {
                                    //lock (_lockStatus)
                                    {
                                        GCHandle gch = GCHandle.Alloc(response, GCHandleType.Pinned);
                                        try
                                        {
                                            _status = (StatusStruct)Marshal.PtrToStructure(gch.AddrOfPinnedObject(), typeof(StatusStruct));
                                        }
                                        finally
                                        {
                                            gch.Free();
                                        }
                                    }

                                    _statusReceived = true;

                                    //if (_status.ActiveState == 0)
                                    //{
                                    //    StopProduction();
                                    //}   cheng 09-20

                                    if (telegram[3] == (byte)'J')
                                    {
                                        ABLGroupCommandReceived = true;
                                    }

                                    //if (telegram[3] == (byte)'D')
                                    //{
                                    //    _ProductionStartReceived = true;
                                    //}

                                    //_status.TelegramLength = (ushort)readLength; cheng 18-09-10

                                    //STATUS UPDATE BEGIN
                                    //lastStatusUpdate = DateTime.Now;
                                    //bool doSendStatusToDb = DateTime.Now > _lastLineStatusSent.AddSeconds(3);


                                    for (int i = 0; i < _stackerList.Count; i++)
                                    {
                                        for (int j = 0; j < MAXABLPerGroup; j++)
                                        {
                                            //if (isPausedStackerLineABL[i][j])
                                            //{
                                            //    _stackerList[i].ABLList[j].MachineStatus = EnumMachineStatus.AblPaused;
                                            //}
                                            //else
                                            //{
                                            //    if (_status.StackerLineStatus[i].State[j] == 0)
                                            //    {
                                            //        _stackerList[i].ABLList[j].MachineStatus = EnumMachineStatus.NotRunning;
                                            //    }
                                            //    else if(_status.StackerLineStatus[i].State[j] == 2)
                                            //    {
                                            //        _stackerList[i].ABLList[j].MachineStatus = EnumMachineStatus.NotReady;
                                            //    }
                                            //    else if(_status.StackerLineStatus[i].State[j] == 1)
                                            //    {
                                            //        _stackerList[i].ABLList[j].MachineStatus = EnumMachineStatus.Running;
                                            //    }

                                            //}

                                            _stackerList[i].ABLList[j].MachineStatus = ABLState[i][j];

                                            //if (_status.StackerLineStatus[i].State[j] == 0)
                                            //{
                                            //    _stackerList[i].ABLList[j].MachineStatus = EnumMachineStatus.NotRunning;
                                            //}
                                            //else
                                            //{
                                            //    //CurStackerLine.ABLList[j].Status = enumStackerStatus.Ready;
                                            //    _stackerList[i].ABLList[j].MachineStatus = EnumMachineStatus.Running;
                                            //}


                                            //if (_status.StackerLineStatus[i].State[j] == 2)
                                            //{
                                            //    //CurStackerLine.ABLList[j].Status = enumStackerStatus.NotReady;
                                            //    _stackerList[i].ABLList[j].MachineStatus = EnumMachineStatus.NotRunning;
                                            //}

                                            _stackerList[i].PosLineID = _status.StackerLineStatus[i].PosLineId;

                                            //CurStackerLine.PosLineID = _status.StackerLineStatus[i].PosLineId;
                                            //CopiesStacker[i] = _status.StackerStatus[i].CountersCopiesIn;

                                            //if (_stackerList[i].LastBundleMessage > DateTime.Now.AddSeconds(-15))
                                            //{
                                            //    CurStackerLine.Status = enumStackerLineStatus.Producing;
                                            //}

                                            //if (LastStackerStatus[i, j] != CurStackerLine.ABLList[j].Status)
                                            //{
                                            //    DbSetStackerStatus(CurStackerLine.ABLList[j].Status, (byte)(i + 1), (byte)(j + 1)); // _StackerLineId);
                                            //    //DbSetApplicatorStatus(stacker.TopSheetApplicatorState, stacker.StackerID);
                                            //    //DbSetPrinterStatus(stacker.TopSheetPrinterState1, stacker.StackerID, 1);     
                                            //    LastStackerStatus[i, j] = CurStackerLine.ABLList[j].Status;
                                            //}

                                            //
                                            //if (LineId == 2 && LastStackerPosId[i] != _stackerList[i].PosLineID)
                                            //if (LastStackerPosId[i] != _stackerList[i].PosLineID)
                                            //{
                                            //    LastStackerPosId[i] = _stackerList[i].PosLineID;
                                            //    DbSetStackerPosID(_stackerList[i].PosLineID, _stackerList[i].StackerLineId);
                                            //}
                                        }
                                    }
                                    //STATUS UPDATE END
                                    //Inkjet Parameters and Faults
                                    //UpdateInkjetRunningStatus();
                                    //Inkjet Parameters and Faults

                                    for (int i = 0; i < _status.BundleMessageCount; i++)
                                    {
                                        bundleMessage = _status.BundleMessage[i]; //bundleMessage.BundleId += 10000;
                                        string sMessage = "BundleMessage from PAC "
                                                + " - StackerLineId: " + bundleMessage.StackerLineId.ToString()
                                                + " - AblId: " + bundleMessage.ABLId.ToString()
                                                + " - Status: " + ((byte)bundleMessage.Status).ToString()
                                                + " - TitleGroup: " + CurProductionData.TitleGroupId.ToString()
                                                + " - Bundle: " + bundleMessage.BundleId.ToString()
                                                + " - Code: " + bundleMessage.ExtraInfo.ToString();

                                        if ((byte)bundleMessage.Status >= 50)
                                        {
                                            Logging.AddToLog(LogLevels.Normal, LineId, sMessage);
                                        }
                                        else
                                        {
                                            Logging.AddToLog(LogLevels.Normal, LineId, sMessage);
                                        }

                                        if (bundleMessage.BundleId > 0)
                                        {
                                            switch (bundleMessage.Status)
                                            {
                                                //case EnumMessageStatusTypes.COUNTFIRST:
                                                //    SetBundleStatus(EnumBundleStatus.bundleStatusProducing, bundleMessage.StackerID, bundleMessage.BundleId, 0);
                                                //    break;

                                                case EnumMessageStatusTypes.COUNTLAST:
                                                    SetBundleStatus(EnumBundleStatus.bundleStatusCounting, bundleMessage.StackerLineId, bundleMessage.ABLId, bundleMessage.BundleId, 0);
                                                    break;

                                                case EnumMessageStatusTypes.RELEASEFIRST:
                                                    SetBundleStatus(EnumBundleStatus.bundleStatusReleaseFirst, bundleMessage.StackerLineId, bundleMessage.ABLId, bundleMessage.BundleId, 0);
                                                    break;

                                                case EnumMessageStatusTypes.RELEASELAST:
                                                    SetBundleStatus(EnumBundleStatus.bundleStatusReleaseLast, bundleMessage.StackerLineId, bundleMessage.ABLId, bundleMessage.BundleId, 0);
                                                    break;

                                                //case EnumMessageStatusTypes.ENTRANCEFIRST:
                                                //  _lastBundleMessage = DateTime.Now;
                                                //  SetBundleStatus(EnumBundleStatus.bundleStatusProducing, bundleMessage.StackerID, bundleMessage.BundleId, 0);
                                                //  SetBundleStatus(EnumBundleStatus.bundleStatusInStacker, _setup.StackerLineNumber, bundleMessage.BundleId, 0);
                                                //  break;

                                                //case EnumMessageStatusTypes.ENTRANCELAST:
                                                //    SetBundleStatus(EnumBundleStatus.bundleStatusInStacker, bundleMessage.StackerLineId, bundleMessage.ABLId, bundleMessage.BundleId, 0);
                                                //    break;

                                                //case EnumMessageStatusTypes.BUNDLEINTABLE:
                                                //case EnumMessageStatusTypes.COUNTLAST:
                                                //    SetBundleStatus(EnumBundleStatus.bundleStatusInStacker, bundleMessage.StackerID, bundleMessage.BundleId, 0);
                                                //    break;

                                                //case EnumMessageStatusTypes.BUNDLEEJECTED:
                                                //    SetBundleStatus(EnumBundleStatus.bundleStatusAfterStacker, bundleMessage.StackerLineId, bundleMessage.ABLId, bundleMessage.BundleId, 0);
                                                //    break;

                                                case EnumMessageStatusTypes.LOSTBEFORESTACKER: //Reproduce if lost before stacker
                                                    SetBundleStatus(EnumBundleStatus.bundleStatusStackerFailed, //bundleStatusReady,
                                                        bundleMessage.StackerLineId, bundleMessage.ABLId,
                                                        bundleMessage.BundleId, 0);

                                                    Logging.AddToLog(LogLevels.Warning, LineId, string.Format("LOSTBEFORESTACKER - Manual Print Bundle ID: {0}, StackerLineId: {1}, TitleGroup: {2}", bundleMessage.BundleId,bundleMessage.StackerLineId, CurProductionData.TitleGroupId));

                                                    if(!_lastBadABLBundleIdList.Contains(bundleMessage.BundleId))
                                                    {
                                                        TopSheetItem queueItem = new TopSheetItem
                                                        {
                                                            BundleId = bundleMessage.BundleId,
                                                            LabelState = 51,
                                                            TitleGroupId = CurProductionData.TitleGroupId,
                                                            IssueDateString = CurProductionData.IssueDate,
                                                            TopSheetType = TopSheetTypes.NORMAL
                                                        };

                                                        if (bundleMessage.StackerLineId > 0 &&
                                                            bundleMessage.StackerLineId <= NUM_STACKERLINE)
                                                        {
                                                            StackerLine CurStackerLine = _stackerList[bundleMessage.StackerLineId - 1];

                                                            if (CurStackerLine.PosLineID == LineId)
                                                            {
                                                                if (CurStackerLine.TopSheetMain != null)
                                                                {
                                                                    if (queueItem.BundleId >= 10000 &&
                                                                        queueItem.BundleId < 50000)
                                                                    {
                                                                        //_stackerList[bundleMessage.StackerLineId - 1].TopSheetMain._topSheetEjectedQueue.Add(queueItem);

                                                                        _stackerList[bundleMessage.StackerLineId - 1].TopSheetMain.AddIntoBundleQueue(queueItem);

                                                                        //_stackerList[bundleMessage.StackerLineId - 1].TopSheetMain.TsBundleQueue.Enqueue(queueItem);

                                                                        Logging.AddToLog(LogLevels.Warning, LineId, 
                                                                            string.Format("LOSTBEFORESTACKER - TsBundleQueue Add Eject Bundle ID: {0}, StackerLineId: {1}, LableState: {2}", 
                                                                            queueItem.BundleId.ToString(), 
                                                                            bundleMessage.StackerLineId.ToString(), 
                                                                            queueItem.LabelState.ToString()));
                                                                    }
                                                                }
                                                            }
                                                        }

                                                        _lastBadABLBundleIdList.Add(bundleMessage.BundleId);

                                                        //SetBundleStatus(EnumBundleStatus.bundleStatusStackerFailed,
                                                        //    bundleMessage.StackerLineId, bundleMessage.ABLId,
                                                        //    bundleMessage.BundleId, 0);
                                                    }

                                                    break;

                                                //default:
                                                //    if ((byte)bundleMessage.Status >= 50)
                                                //    {
                                                //        SetBundleStatus(EnumBundleStatus.bundleStatusStackerFailed,
                                                //            bundleMessage.StackerLineId, bundleMessage.ABLId,
                                                //            bundleMessage.BundleId, 0);
                                                //    }
                                                //    break;
                                                    //case EnumMessageStatusTypes.INCOMPLETETRANSFER:
                                                    //case EnumMessageStatusTypes.LAYERCONTROLABORT:
                                                    //case EnumMessageStatusTypes.LCCABORT:
                                                    //case EnumMessageStatusTypes.LOSTBEFOREEJECTED:
                                                    //case EnumMessageStatusTypes.LOSTINTERCEPTWINDOW:
                                                    //case EnumMessageStatusTypes.PRODUCTIONQUEUEFULL:
                                                    //case EnumMessageStatusTypes.STACKERABORT:
                                                    //    SetBundleStatus(EnumBundleStatus.bundleStatusStackerFailed,
                                                    //        _setup.StackerLineNumber,
                                                    //        bundleMessage.BundleId, 0);
                                                    //    break;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    Logging.AddToLog(LogLevels.Warning, LineId, string.Format("Error in ICP response header - not in sync length {0} != {1}", response.Length, readLength));
                                    PacDisconnectGripper();
                                }

                                //if (_doReconnectGripperPAC)
                                //{
                                //    _doReconnectGripperPAC = false;
                                //    PacDisconnectGripper();
                                //}
                            }
                        }
                        catch (Exception ex)
                        {
                            Logging.AddToLog(LogLevels.Warning, LineId, ex.ToString());//true, LogLevels.Warning, ex.ToString());
                            PacDisconnectGripper();
                            Thread.Sleep(1000);
                        }
                        finally
                        {
                            Thread.Sleep(100);
                        }

                        if (DateTime.Now > lastPACCycleTime.AddSeconds(3))
                        {
                            Logging.AddToLog(LogLevels.Warning, LineId, "3 seconds timeout " + lastPACCycleTime.ToString());//true, LogLevels.Warning, ex.ToString());
                            Thread.Sleep(100);
                        }
                    }
                    catch (Exception ex)
                    {
                        Logging.AddToLog(LogLevels.Warning, LineId, ex.ToString());//true, LogLevels.Warning, ex.ToString());
                        Thread.Sleep(100);
                    }
                }

                PacDisconnectGripper();
                IcpConnectionState = EnumIcpConnectionStates.NotRunning;
                Logging.AddToLog(LogLevels.Warning, LineId, "PacControl stopped");//true, LogLevels.Warning, LineId, ex.ToString());
            }
            catch (Exception ex)
            {
                Logging.AddToLog(LogLevels.Warning, LineId, ex.ToString());//true, LogLevels.Warning, ex.ToString());
                Thread.Sleep(100);
            }
        }

        //public int GetStackerParameter()
        //{
        //    String connsql = "Data Source=172.22.22.150;Initial Catalog=UniMail_Sydost;Integrated Security=False;User ID=sa;Password=UniMail2017";
        //    SqlConnection conn = new SqlConnection(connsql);
        //    SqlCommand cmd = conn.CreateCommand();

        //    String sql = "Select *from UniStack_StackerSetupTable";
        //    cmd.CommandText = sql;
        //    conn.Open();
        //    cmd.ExecuteNonQuery();
        //    SqlDataReader dr = cmd.ExecuteReader();
        //    if (dr.Read())
        //    {
        //        Console.WriteLine("SUCCESS");
        //        Console.WriteLine(dr[1].ToString());
        //    }
        //    else { Console.WriteLine("Wrong!"); }
        //    conn.Close();
        //    return 1;
        //}

        //public HaukSetupStructure[] GetHaukSetupStructure()
        //{
        //    uint[] DPulseEntranceToIntercept = new uint[4];
        //    uint[] DPulseMarginRelease = new uint[4];
        //    uint[] DPulseReleaseToEntrance = new uint[4];
        //    ushort[] ReleaseDelay = new ushort[4];
        //    ushort[] ReleaseDistance = new ushort[4];
        //    ushort[] ReleaseStopDelay = new ushort[4]; ;
        //    byte[] ReleaseUpDownFlag = new byte[4];
        //    ushort[] StackerEjectMSDelay = new ushort[4];
        //    ushort[] StackerEjectMSTimeout = new ushort[4];

        //    DataTable dt1 = GetStackerParameterTable(1);
        //    DataTable dt2 = GetStackerParameterTable(2);
        //    DataTable dt3 = GetStackerParameterTable(3);
        //    DataTable dt4 = GetStackerParameterTable(4);
        //    DataTable[] dtArr = new DataTable[4];
        //    dtArr[0] = dt1;
        //    dtArr[1] = dt2;
        //    dtArr[2] = dt3;
        //    dtArr[3] = dt4;

        //    for (int i = 0; i < 4; i++)
        //    {
        //        DPulseEntranceToIntercept[i] = Convert.ToUInt32(dtArr[i].Rows[0][2].ToString());
        //        DPulseMarginRelease[i] = Convert.ToUInt32(dtArr[i].Rows[1][2].ToString());
        //        DPulseReleaseToEntrance[i] = Convert.ToUInt32(dtArr[i].Rows[2][2].ToString());
        //        ReleaseDelay[i] = Convert.ToUInt16(dtArr[i].Rows[3][2].ToString());
        //        ReleaseDistance[i] = Convert.ToUInt16(dtArr[i].Rows[4][2].ToString()); ;
        //        ReleaseStopDelay[i] = Convert.ToUInt16(dtArr[i].Rows[5][2].ToString());
        //        ReleaseUpDownFlag[i] = Convert.ToByte(dtArr[i].Rows[6][2].ToString()); ;
        //        StackerEjectMSDelay[i] = Convert.ToUInt16(dtArr[i].Rows[7][2].ToString());
        //        StackerEjectMSTimeout[i] = Convert.ToUInt16(dtArr[i].Rows[8][2].ToString());
        //    }



        //    HaukSetupStructure[] HaukSetup = new HaukSetupStructure[4];
        //    for (int i = 0; i < 4; i++)
        //    {

        //        HaukSetup[i] = new HaukSetupStructure();
        //        HaukSetup[i].DPulseEntranceToIntercept = DPulseEntranceToIntercept[i];
        //        HaukSetup[i].DPulseMarginRelease = DPulseMarginRelease[i];
        //        HaukSetup[i].DPulseReleaseToEntrance = DPulseReleaseToEntrance[i];
        //        HaukSetup[i].ReleaseDelay = ReleaseDelay[i];
        //        HaukSetup[i].ReleaseDistance = ReleaseDistance[i];
        //        HaukSetup[i].ReleaseStopDelay = ReleaseStopDelay[i];
        //        HaukSetup[i].ReleaseUpDownFlag = ReleaseUpDownFlag[i];
        //        HaukSetup[i].StackerEjectMSDelay = StackerEjectMSDelay[i];
        //        HaukSetup[i].StackerEjectMSTimeout = StackerEjectMSTimeout[i];

        //        Console.WriteLine(HaukSetup[i].GetString() + "\r\n");

        //    }
        //    return HaukSetup;
        //}


        //public DataTable GetStackerParameterTable(int StackerId)
        //{
        //    String connsql = "Data Source=172.22.22.150;Initial Catalog=UniMail_Sydost;Integrated Security=False;User ID=sa;Password=UniMail2017";
        //    String sql = "Select *from UniStack_StackerSetupTable where StackerId =";
        //    sql += StackerId.ToString();
        //    DataTable dt = new DataTable();


        //    try
        //    {
        //        using (SqlConnection conn = new SqlConnection())
        //        {
        //            conn.ConnectionString = connsql;
        //            conn.Open();
        //            SqlDataAdapter myda = new SqlDataAdapter(sql, conn);
        //            myda.Fill(dt);
        //            return dt;
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //    }

        //    return dt;


        //}

        //public void SendHaukCommand(HaukSetupStructure[] HaukP)
        //{
        //    HaukSetupStructure[] haukSetupStructure = HaukP;
        //    haukSetupStructure[0].GetString();
        //    Console.WriteLine(haukSetupStructure[0].GetString());
        //    List<byte> byteList = new List<byte>();

        //    for (int i = 0; i < 4; i++)
        //    {
        //        byte[] h2 = new byte[2];
        //        byte[] h3 = new byte[2];
        //        byte[] h4 = new byte[2];
        //        byte[] h5 = new byte[2];
        //        byte[] h6 = new byte[2];
        //        byte[] h7 = new byte[4];
        //        byte[] h8 = new byte[4];
        //        byte[] h9 = new byte[4];

        //        byte ReleaseUpDownFlag = haukSetupStructure[i].ReleaseUpDownFlag;
        //        byte h1 = ReleaseUpDownFlag;
        //        byteList.Add(h1);

        //        ushort ReleaseDelay = haukSetupStructure[i].ReleaseDelay;
        //        ConvertInt16TobyteListay(ReleaseDelay, ref h2);
        //        byteList.Add(h2[0]);
        //        byteList.Add(h2[1]);


        //        ushort ReleaseDistance = haukSetupStructure[i].ReleaseDistance;
        //        ConvertInt16TobyteListay(ReleaseDistance, ref h3);
        //        byteList.Add(h3[0]);
        //        byteList.Add(h3[1]);


        //        ushort ReleaseStopDelay = haukSetupStructure[i].ReleaseStopDelay;
        //        ConvertInt16TobyteListay(ReleaseStopDelay, ref h4);
        //        byteList.Add(h4[0]);
        //        byteList.Add(h4[1]);

        //        ushort StackerEjectMSDelay = haukSetupStructure[i].StackerEjectMSDelay;
        //        ConvertInt16TobyteListay(StackerEjectMSDelay, ref h5);
        //        byteList.Add(h5[0]);
        //        byteList.Add(h5[1]);

        //        ushort StackerEjectMSTimeout = haukSetupStructure[i].StackerEjectMSTimeout;
        //        ConvertInt16TobyteListay(StackerEjectMSTimeout, ref h6);
        //        byteList.Add(h6[0]);
        //        byteList.Add(h6[1]);



        //        uint DPulseEntranceToIntercept = haukSetupStructure[i].DPulseEntranceToIntercept;
        //        ConvertInt32TobyteListay(DPulseEntranceToIntercept, ref h7);
        //        byteList.Add(h7[0]);
        //        byteList.Add(h7[1]);
        //        byteList.Add(h7[2]);
        //        byteList.Add(h7[3]);

        //        uint DPulseMarginRelease = haukSetupStructure[i].DPulseMarginRelease;
        //        ConvertInt32TobyteListay(DPulseMarginRelease, ref h8);
        //        byteList.Add(h8[0]);
        //        byteList.Add(h8[1]);
        //        byteList.Add(h8[2]);
        //        byteList.Add(h8[3]);


        //        uint DPulseReleaseToEntrance = haukSetupStructure[i].DPulseReleaseToEntrance;
        //        ConvertInt32TobyteListay(DPulseReleaseToEntrance, ref h9);
        //        byteList.Add(h9[0]);
        //        byteList.Add(h9[1]);
        //        byteList.Add(h9[2]);
        //        byteList.Add(h9[3]);


        //    }

        //    foreach (byte b in byteList)
        //    {
        //        Console.WriteLine(b);
        //    }
        //}



        static bool ConvertInt32TobyteListay(uint ui, ref byte[] arry)
        {
            if (arry == null) return false;
            if (arry.Length < 4) return false;

            arry[0] = (byte)(ui & 0xFF);
            arry[1] = (byte)((ui & 0xFF00) >> 8);
            arry[2] = (byte)((ui & 0xFF) >> 16);
            arry[3] = (byte)((ui >> 24) & 0xFF);

            return true;

        }

        static bool ConvertInt16TobyteListay(uint ui, ref byte[] arry)
        {
            if (arry == null) return false;
            if (arry.Length < 2) return false;

            arry[0] = (byte)(ui & 0xFF);
            arry[1] = (byte)((ui & 0xFF00) >> 8);
            return true;
        }


        public void RequestSetupSendToStackerHauk(HaukSetupStructure[] HaukP, object isSimulate)
        {
            IcpGetOrAddTelegramStackerHauk(EnumIcpTelegramTypes.SendSetup, isSimulate, true, HaukP);
        }


        public void TestStacker()
        {
            IcpGetOrAddTelegramStacker(EnumIcpTelegramTypes.SendSetup, null, true);
        }

        public byte[] IcpGetOrAddTelegramStackerHauk(EnumIcpTelegramTypes icpTelegramType, object oParameters, bool doAddToList, HaukSetupStructure[] HaukP)
        {
            HaukSetupStructure[] haukSetupStructure = HaukP;

            short[] Simulate = { 1, 1, 800, 1, 0 };
            short[] tmp = (short[])oParameters;
            if (oParameters != null) { Simulate = (short[])oParameters; }
            byte[] result = null;
            List<byte> byteList;
            int size;
            byte sizeL;
            byte sizeH;
            byte pcStatus = PcStatusStacker;/* _pcStatusStacker;*/

            if (_telegramIdStacker < 99)
            {
                ++_telegramIdStacker;
            }
            else
            {
                _telegramIdStacker = 1;
            }

            switch (icpTelegramType)
            {
                case EnumIcpTelegramTypes.SendSetup://B
                    size = 7 + 99 + 32; //bytes.Length; //7+99 + 32 
                    sizeL = (byte)(size % 256);
                    sizeH = (byte)(size / 256);
                    byteList = new List<byte>
                    {
                        SOH,
                        _telegramIdStacker,
                        pcStatus,
                        (byte)'B',
                        sizeL,
                        sizeH
                    };

                    for (int i = 0; i < 4; i++)
                    {
                        byte[] h2 = new byte[2];
                        byte[] h3 = new byte[2];
                        byte[] h4 = new byte[2];
                        byte[] h5 = new byte[2];
                        byte[] h6 = new byte[2];
                        byte[] h7 = new byte[4];
                        byte[] h8 = new byte[4];
                        byte[] h9 = new byte[4];

                        byte[] h10 = new byte[2];
                        byte[] h11 = new byte[2];
                        byte[] h12 = new byte[2];
                        byte[] h13 = new byte[2];


                        byte ReleaseUpDownFlag = haukSetupStructure[i].ReleaseUpDownFlag;
                        byte h1 = ReleaseUpDownFlag;
                        byteList.Add(h1);

                        ushort ReleaseDelay = haukSetupStructure[i].ReleaseDelay;
                        ConvertInt16TobyteListay(ReleaseDelay, ref h2);
                        byteList.Add(h2[0]);
                        byteList.Add(h2[1]);


                        ushort ReleaseDistance = haukSetupStructure[i].ReleaseDistance;
                        ConvertInt16TobyteListay(ReleaseDistance, ref h3);
                        byteList.Add(h3[0]);
                        byteList.Add(h3[1]);


                        ushort ReleaseStopDelay = haukSetupStructure[i].ReleaseStopDelay;
                        ConvertInt16TobyteListay(ReleaseStopDelay, ref h4);
                        byteList.Add(h4[0]);
                        byteList.Add(h4[1]);

                        ushort StackerEjectMSDelay = haukSetupStructure[i].StackerEjectMSDelay;
                        ConvertInt16TobyteListay(StackerEjectMSDelay, ref h5);
                        byteList.Add(h5[0]);
                        byteList.Add(h5[1]);

                        ushort StackerEjectMSTimeout = haukSetupStructure[i].StackerEjectMSTimeout;
                        ConvertInt16TobyteListay(StackerEjectMSTimeout, ref h6);
                        byteList.Add(h6[0]);
                        byteList.Add(h6[1]);



                        uint DPulseEntranceToIntercept = haukSetupStructure[i].DPulseEntranceToIntercept;
                        ConvertInt32TobyteListay(DPulseEntranceToIntercept, ref h7);
                        byteList.Add(h7[0]);
                        byteList.Add(h7[1]);
                        byteList.Add(h7[2]);
                        byteList.Add(h7[3]);

                        uint DPulseMarginRelease = haukSetupStructure[i].DPulseMarginRelease;
                        ConvertInt32TobyteListay(DPulseMarginRelease, ref h8);
                        byteList.Add(h8[0]);
                        byteList.Add(h8[1]);
                        byteList.Add(h8[2]);
                        byteList.Add(h8[3]);


                        uint DPulseReleaseToEntrance = haukSetupStructure[i].DPulseReleaseToEntrance;
                        ConvertInt32TobyteListay(DPulseReleaseToEntrance, ref h9);
                        byteList.Add(h9[0]);
                        byteList.Add(h9[1]);
                        byteList.Add(h9[2]);
                        byteList.Add(h9[3]);

                        ushort ReleaseDelayMedium = haukSetupStructure[i].ReleaseDelayMedium;
                        ConvertInt16TobyteListay(ReleaseDelayMedium, ref h10);
                        byteList.Add(h10[0]);
                        byteList.Add(h10[1]);

                        ushort ReleaseDelayHigh = haukSetupStructure[i].ReleaseDelayHigh;
                        ConvertInt16TobyteListay(ReleaseDelayHigh, ref h11);
                        byteList.Add(h11[0]);
                        byteList.Add(h11[1]);


                        ushort ReleaseStopDelayMedium = haukSetupStructure[i].ReleaseStopDelayMedium;
                        ConvertInt16TobyteListay(ReleaseStopDelayMedium, ref h12);
                        byteList.Add(h12[0]);
                        byteList.Add(h12[1]);

                        ushort ReleaseStopDelayHigh = haukSetupStructure[i].ReleaseStopDelayHigh;
                        ConvertInt16TobyteListay(ReleaseStopDelayHigh, ref h13);
                        byteList.Add(h13[0]);
                        byteList.Add(h13[1]);


                    }
                    byteList.Add((byte)Simulate[4]); //Simulation flag

                    byteList.Add((byte)Simulate[0]); //Enalbe/Diable InkJet
                    byteList.Add((byte)Simulate[1]); //Print All Copy

                    byte[] inkp1 = new byte[2]; // inkjet parameter1
                    ConvertInt16TobyteListay((uint)Simulate[2], ref inkp1);

                    byteList.Add(inkp1[0]);
                    byteList.Add(inkp1[1]);
                    byte[] inkp2 = new byte[2];// inkjet parameter2
                    ConvertInt16TobyteListay((uint)Simulate[3], ref inkp2);

                    byteList.Add(inkp2[0]);
                    byteList.Add(inkp2[1]);



                    Console.WriteLine("Simulation : " + (byte)Simulate[4]);
                    Console.WriteLine("Inkjet: " + (byte)Simulate[0]);
                    Console.WriteLine("Print All: " + (byte)Simulate[1]);
                    Console.WriteLine("Inkjet p1: " + Simulate[2]);
                    Console.WriteLine("Inkjet p2: " + Simulate[3]);

                    //  Console.WriteLine("Simulation type: " + Simulate[4]);


                    byteList.Add(EOT);

                    //foreach (byte b in byteList)
                    //{
                    //    Console.WriteLine(b);
                    //}
                    result = byteList.ToArray();
                    break;

            }

            IcpAddTelegramStacker(result);

            return result;
        }


        public byte[] IcpGetOrAddTelegramStacker(EnumIcpTelegramTypes icpTelegramType, object oParameters, bool doAddToList)
        {
            byte[] result = null;
            List<byte> byteList;
            int size;
            byte sizeL;
            byte sizeH;
            //CopyBufferStruct[] copyBuffer;
            //LayerData layerData;
            byte[] bytes;
            byte StackerID;
            //byte pcStatus = _isPaused ? (byte)2 : _pcStatus;
            //AddressData[] addressList;
            byte pcStatus = PcStatusStacker;/* _pcStatusStacker;*/
            ABL tempABL;

            if (_telegramIdStacker < 99)
            {
                ++_telegramIdStacker;
            }
            else
            {
                _telegramIdStacker = 1;
            }

            byte ABLGroupId;
            byte AblId;
            switch (icpTelegramType)
            {
                case EnumIcpTelegramTypes.RequestStatus: //A
                    if (_telegramRequestStatus == null)
                    {
                        int expectedStructSize = _statusStructSize - 6;
                        //int size1, size2;

                        //size1 = Marshal.SizeOf(typeof(BundleMessageStruct));
                        //size2 = Marshal.SizeOf(typeof(StackerLineStatusStruct));
                        _telegramRequestStatus = new byte[] {
                                SOH, _telegramIdStacker, pcStatus, (byte)'A', 9, 0,
                                (byte)(expectedStructSize % 256),
                                (byte)(expectedStructSize / 256),
                                 EOT };
                    }
                    else
                    {
                        _telegramRequestStatus[1] = _telegramIdStacker;
                        _telegramRequestStatus[2] = pcStatus;
                    }

                    result = _telegramRequestStatus;
                    break;

                case EnumIcpTelegramTypes.SendSetup://B
                    if (_setup.Mode == EnumModes.STANDARD)
                    {
                        _setup.IsPaused = 0;
                    }

                    bytes = GetSetupStructBytes();
                    size = 7 + bytes.Length;
                    sizeL = (byte)(size % 256);
                    sizeH = (byte)(size / 256);
                    byteList = new List<byte>
                    {
                        SOH,
                        _telegramIdStacker,
                        pcStatus,
                        (byte)'B',
                        sizeL,
                        sizeH
                    };

                    byteList.AddRange(bytes);

                    byteList.Add(EOT);


                    result = byteList.ToArray();
                    break;

                case EnumIcpTelegramTypes.SendBundleData://C
                    BundleData CurBundle = (BundleData)(oParameters);
                    List<byte> LayerByteList = new List<byte>();

                    for (int i = 0; i < CurBundle.NumLayersInBundle; i++)
                    {
                        LayerData CurLayer = CurBundle.LayerCommandList[i];
                        LayerByteList.Add(CurLayer.LayerNumber); //LayerID
                        LayerByteList.Add(CurLayer.Flags);
                        LayerByteList.Add(CurLayer.NumCopiesInLayer);
                        // LayerByteList.Add(CurLayer.NumAddress);
                    }

                    result = new byte[LayerByteList.Count + 15];
                    result[0] = SOH;
                    result[1] = _telegramIdStacker;
                    result[2] = pcStatus;
                    result[3] = (byte)'C';
                    result[4] = (byte)(result.Length & 0xFF);
                    result[5] = (byte)(result.Length >> 8);
                    result[6] = CurBundle.StackerLineId;
                    //result[7] = (byte)(CurBundle.EditionTableId & 0xFF);
                    result[7] = (byte)(CurBundle.BundleId % 256);
                    result[8] = (byte)(CurBundle.BundleId / 256);
                    result[9] = (byte)(CurBundle.NumCopies % 256);
                    result[10] = (byte)(CurBundle.NumCopies / 256);
                    result[11] = CurBundle.NumLayersInBundle;
                    //Add standard bundle information. Jie 2018.03.11
                    result[12] = CurBundle.StandardCopiesInLayer;
                    result[13] = CurBundle.StandardNumBatch;

                    Array.Copy(LayerByteList.ToArray(), 0, result, 14, LayerByteList.Count);
                    result[result.Length - 1] = EOT;
                    _stackerList[CurBundle.StackerLineId - 1]._lastProductionDataSent = DateTime.Now;
                    break;

                //case EnumIcpTelegramTypes.SendLayerCommand://C
                //    layerData = (LayerData)oParameters;
                //    result = new byte[] {
                //        SOH,
                //        _telegramIdStacker,
                //        pcStatus,
                //        (byte)'C',
                //        (byte)0,//set length low after
                //        (byte)0,//set length high after
                //        (byte)layerData.StackerID,
                //        (byte)(layerData.BundleId % 256),
                //        (byte)(layerData.BundleId / 256),
                //        (byte)layerData.LayerNumber,
                //        (byte)layerData.Flags,
                //        (byte)layerData.NumCopiesInLayer,
                //        (byte)(layerData.NumCopiesInBundle % 256),
                //        (byte)(layerData.NumCopiesInBundle / 256),
                //        (byte)layerData.NumLayersInBundle,
                //        (byte)layerData.NumAddress,
                //        EOT
                //        };

                //    result[4] = (byte)result.Length;
                //    break;

                //case EnumIcpTelegramTypes.SendLayerCommand://C
                //    layerData = (LayerData)oParameters;
                //    result = new byte[] {
                //        SOH,
                //        _telegramIdStacker,
                //        pcStatus,
                //        (byte)'C',
                //        (byte)0,//set length low after
                //        (byte)0,//set length high after
                //        (byte)(layerData.BundleId % 256),
                //        (byte)(layerData.BundleId / 256),
                //        (byte)layerData.LayerNumber,
                //        (byte)layerData.GripperNumber,
                //        (byte)layerData.Flags,
                //        (byte)(layerData.NumCopiesInLayer % 256),
                //        (byte)(layerData.NumCopiesInLayer / 256),
                //        (byte)(layerData.NumCopiesInBundle % 256),
                //        (byte)(layerData.NumCopiesInBundle / 256),
                //        (byte)layerData.NumLayersInBundle,
                //        EOT
                //        };

                //    result[4] = (byte)result.Length;
                //    break;

                /*case EnumIcpTelegramTypes.SendBundleData:
                    copyBuffer = (CopyBufferStruct[])oParameters;
                    size = 8 + _copyStructSize * copyBuffer.Length;
                    sizeL = (byte)(size % 256);
                    sizeH = (byte)(size / 256);
                    byteList = new List<byte>();

                    byteList.Add(SOH);
                    byteList.Add(_telegramId);
                    byteList.Add(_pcStatus);
                    byteList.Add((byte)'C');
                    byteList.Add(sizeL);
                    byteList.Add(sizeH);

                    byteList.Add((byte)copyBuffer.Length);

                    for (int i = 0; i < copyBuffer.Length; i++)
                    {
                        bytes = GetCopyStructBytes(copyBuffer[i]);
                        byteList.AddRange(bytes);
                    }

                    byteList.Add(EOT);

                    result = byteList.ToArray();
                    break;*/

                case EnumIcpTelegramTypes.StartProduction://D
                                                          //_telegramStartProduction = new byte[] { SOH, _telegramIdStacker, pcStatus, (byte)'D', 7, 0, EOT };

                    _telegramStartProduction = new byte[10]; //11; //Length

                    _telegramStartProduction[0] = SOH;
                    _telegramStartProduction[1] = _telegramIdStacker;
                    _telegramStartProduction[2] = pcStatus;

                    _telegramStartProduction[3] = (byte)'D';
                    _telegramStartProduction[4] = 10; //11; //Length
                    _telegramStartProduction[5] = 0;

                    //_telegramStartProduction[6] = CurProductionData.NumCopiesLayer;
                    _telegramStartProduction[6] = (byte)(CurProductionData.StandardBundle % 256);
                    _telegramStartProduction[7] = (byte)(CurProductionData.StandardBundle / 256);
                    _telegramStartProduction[8] = CurProductionData.NumBatch;

                    //_telegramStartProduction[8] = (byte)(CurProductionData.NumLayerStacker2 % 256);
                    //_telegramStartProduction[9] = (byte)(CurProductionData.NumLayerStacker2 / 256);
                    _telegramStartProduction[9] = EOT;

                    result = _telegramStartProduction;
                    //_ProductionStartReceived = true;
                    //Reset BA & BLC & PZF
                    //ResetBABLC();
                    //ResetPZF();
                    break;

                case EnumIcpTelegramTypes.StopProduction://E
                    if (_telegramStopProduction == null)
                    {
                        _telegramStopProduction = new byte[] { SOH, _telegramIdStacker, pcStatus, (byte)'E', 7, 0, EOT };
                    }
                    else
                    {
                        _telegramStopProduction[1] = _telegramIdStacker;
                        _telegramStopProduction[2] = pcStatus;
                    }

                    result = _telegramStopProduction;
                    break;

                case EnumIcpTelegramTypes.Reset://F
                    StackerID = (byte)oParameters;
                    if (_telegramReset == null)
                    {
                        _telegramReset = new byte[] { SOH, _telegramIdStacker, pcStatus, (byte)'F', 8, 0, StackerID, EOT };
                    }
                    else
                    {
                        _telegramReset[1] = _telegramIdStacker;
                        _telegramReset[2] = pcStatus;
                    }

                    result = _telegramReset;
                    break;

                case EnumIcpTelegramTypes.ClearCounters: //G
                    if (_telegramClearCounters == null)
                    {
                        _telegramClearCounters = new byte[] { SOH, _telegramIdStacker, pcStatus, (byte)'G', 7, 0, EOT };
                    }
                    else
                    {
                        _telegramClearCounters[1] = _telegramIdStacker;
                        _telegramClearCounters[2] = pcStatus;
                    }

                    result = _telegramClearCounters;
                    break;

                case EnumIcpTelegramTypes.SendStandardBundleSize: //H

                    _telegramBundleSetting = new byte[14];  //Length

                    _telegramBundleSetting[0] = SOH;
                    _telegramBundleSetting[1] = _telegramIdStacker;
                    _telegramBundleSetting[2] = pcStatus;

                    _telegramBundleSetting[3] = (byte)'H';
                    _telegramBundleSetting[4] = 13; //Length
                    _telegramBundleSetting[5] = 0;

                    //_telegramBundleSetting[6] = (byte)(StandardBundleSize / NumberofStandardLayer); //Batch
                    _telegramBundleSetting[6] = (byte)(StandardBundleSize % 256);
                    _telegramBundleSetting[7] = (byte)(StandardBundleSize / 256);

                    _telegramBundleSetting[8] = (byte)(NumberofStandardLayer); //Layer
                    _telegramBundleSetting[9] = (byte)(NumberofPages);
                    _telegramBundleSetting[10] = (byte)(CopyToPressLimit);
                    _telegramBundleSetting[11] = (byte)(CopiePerGripper);
                    _telegramBundleSetting[12] = (byte)(EdgeSupportPulseValue);

                    _telegramBundleSetting[13] = EOT;

                    result = _telegramBundleSetting;

                    break;

                case EnumIcpTelegramTypes.SendABLGroupSize: //J

                    _telegramBundleSetting = new byte[20];  //Length
                    _telegramBundleSetting[0] = SOH;
                    _telegramBundleSetting[1] = _telegramIdStacker;
                    _telegramBundleSetting[2] = pcStatus;
                    _telegramBundleSetting[3] = (byte)'J';
                    _telegramBundleSetting[4] = 20; //Length
                    _telegramBundleSetting[5] = 0;
                    _telegramBundleSetting[6] = (byte)(StackerLineIdForABLGroupSetting);
                    _telegramBundleSetting[7] = (byte)(ABLStopJam);
                    _telegramBundleSetting[8] = (byte)(UTRStopJam);
                    _telegramBundleSetting[9] = (byte)(ReleaseStartDelay);
                    _telegramBundleSetting[10] = (byte)(ReleaseStopDelay);
                    _telegramBundleSetting[11] = (byte)(StrikeStartDelay);
                    _telegramBundleSetting[12] = (byte)(StrikeStopDelay);
                    _telegramBundleSetting[13] = (byte)(LayerCycleTime % 256);
                    _telegramBundleSetting[14] = (byte)(LayerCycleTime / 256);
                    _telegramBundleSetting[15] = (byte)(BundleCycleTime % 256);
                    _telegramBundleSetting[16] = (byte)(BundleCycleTime / 256);
                    _telegramBundleSetting[17] = (byte)(CountABLDistance % 256);
                    _telegramBundleSetting[18] = (byte)(CountABLDistance / 256);
                    _telegramBundleSetting[19] = EOT;
                    result = _telegramBundleSetting;

                    break;

                //case EnumIcpTelegramTypes.SendAddressList: //I
                //    break;


                case EnumIcpTelegramTypes.StartLineController://L
                    if (_telegramStartLineController == null)
                    {
                        _telegramStartLineController = new byte[] { SOH, _telegramIdStacker, pcStatus, (byte)'L', 7, 0, EOT };
                    }
                    else
                    {
                        _telegramStartLineController[1] = _telegramIdStacker;
                        _telegramStartLineController[2] = pcStatus;
                    }

                    result = _telegramStartLineController;
                    break;

                case EnumIcpTelegramTypes.StopLineController://M
                    if (_telegramStopLineController == null)
                    {
                        _telegramStopLineController = new byte[] { SOH, _telegramIdStacker, pcStatus, (byte)'M', 7, 0, EOT };
                    }
                    else
                    {
                        _telegramStopLineController[1] = _telegramIdStacker;
                        _telegramStopLineController[2] = pcStatus;
                    }

                    result = _telegramStopLineController;
                    break;

                case EnumIcpTelegramTypes.PauseStacker://N
                    result = new byte[9];
                    result[0] = SOH;
                    result[1] = _telegramIdStacker;
                    result[2] = pcStatus;
                    result[3] = (byte)'N';
                    result[4] = (byte)(result.Length & 0xFF);
                    result[5] = (byte)(result.Length >> 8);
                    result[6] = (byte)(oParameters); //StackerID
                    result[7] = 1;
                    result[result.Length - 1] = EOT;
                    break;

                case EnumIcpTelegramTypes.ResumeStacker://N
                    result = new byte[9];
                    result[0] = SOH;
                    result[1] = _telegramIdStacker;
                    result[2] = pcStatus;
                    result[3] = (byte)'N';
                    result[4] = (byte)(result.Length & 0xFF);
                    result[5] = (byte)(result.Length >> 8);
                    result[6] = (byte)(oParameters); //StackerID
                    result[7] = 0;
                    result[result.Length - 1] = EOT;
                    break;

                case EnumIcpTelegramTypes.PauseOneStacker://O
                    tempABL = (ABL)(oParameters);
                    ABLGroupId = tempABL.AblGroupId;
                    AblId = tempABL.AblId;

                    result = new byte[10];
                    result[0] = SOH;
                    result[1] = _telegramIdStacker;
                    result[2] = pcStatus;
                    result[3] = (byte)'O';
                    result[4] = (byte)(result.Length & 0xFF);
                    result[5] = (byte)(result.Length >> 8);
                    result[6] = ABLGroupId; //ABLGroupId
                    result[7] = AblId; //ABLId
                    result[8] = 1;
                    result[result.Length - 1] = EOT;
                    break;

                case EnumIcpTelegramTypes.ResumeOneStacker://O
                    tempABL = (ABL)(oParameters);
                    ABLGroupId = tempABL.AblGroupId;
                    AblId = tempABL.AblId;

                    result = new byte[10];
                    result[0] = SOH;
                    result[1] = _telegramIdStacker;
                    result[2] = pcStatus;
                    result[3] = (byte)'O';
                    result[4] = (byte)(result.Length & 0xFF);
                    result[5] = (byte)(result.Length >> 8);
                    result[6] = ABLGroupId; //ABLGroupId
                    result[7] = AblId; //ABLId
                    result[8] = 0;
                    result[result.Length - 1] = EOT;
                    break;

                case EnumIcpTelegramTypes.StopStacker://P
                    result = new byte[9];
                    result[0] = SOH;
                    result[1] = _telegramIdStacker;
                    result[2] = pcStatus;
                    result[3] = (byte)'P';
                    result[4] = (byte)(result.Length & 0xFF);
                    result[5] = (byte)(result.Length >> 8);
                    result[6] = (byte)(oParameters); //StackerID
                    result[7] = 1;
                    result[result.Length - 1] = EOT;
                    break;

                case EnumIcpTelegramTypes.StartStacker://P
                    result = new byte[9];
                    result[0] = SOH;
                    result[1] = _telegramIdStacker;
                    result[2] = pcStatus;
                    result[3] = (byte)'P';
                    result[4] = (byte)(result.Length & 0xFF);
                    result[5] = (byte)(result.Length >> 8);
                    result[6] = (byte)(oParameters); //StackerID
                    result[7] = 0;
                    result[result.Length - 1] = EOT;
                    break;

            }

            if (doAddToList)
            {
                IcpAddTelegramStacker(result);
            }

            return result;
        }

        private void IcpAddTelegramStacker(byte[] telegram)
        {
            if (telegram != null && telegram.Length > 0)
            {
                //if (telegram[3] == (byte)'B')
                //{
                //}
                //lock (_lockTelegramListIcp)
                {
                    _telegramListIcpStacker.Add(telegram);
                    //foreach (byte[] b in _telegramListIcpStacker)
                    //{
                    //    Console.WriteLine(b.ToString());
                    //}

                }
            }
        }

        //private void GetCommand()
        //{
        //    DataTable CurrentCommand = new DataTable();

        //    _connectionStringServer = "Data Source=" + _connectionString[0] +
        //        ";Initial Catalog=" + _connectionString[1] +
        //        ";Integrated Security=False;" +
        //        "User ID=" + _connectionString[2] +
        //        ";Password=" + _connectionString[3] + "";

        //    SqlConnection conn = new SqlConnection(_connectionStringServer);

        //    CurrentMode = 0;

        //    try
        //    {
        //        conn.Open();
        //        String sql = "select top 1 Command FROM [UniMail_NWT].[dbo].[UniStack_LCCCommandTable] order by Registered desc";

        //        SqlDataAdapter da = new SqlDataAdapter(sql, conn);
        //        da.Fill(CurrentCommand);

        //        foreach (DataRow row in CurrentCommand.Rows)
        //        {
        //            CommandNumber = int.Parse(row["Command"].ToString());
        //        }

        //        if (CommandNumber == 24)
        //        {
        //            StartProduction();
        //        }
        //        else
        //        {
        //            StopProduction();
        //        }
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        conn.Close();
        //    }
        //}
    
    

        private void PacDisconnectGripper()
        {
            //AddToLog(true, LogLevels.Warning, _lineControlUI.GetLanguageText("IcpDisconnect"));
            Logging.AddToLog(LogLevels.Debug, LineId, "PAC disconnected " + _ioIpGripperLine + ":" + _ioPortGripperLine);

            IcpConnectionState = EnumIcpConnectionStates.Disconnected;

            _telegramListIcpStacker.Clear();
            //_doReconnectDbLocal = true;
            try
            {
                if (_conServer != null && _conServer.State == ConnectionState.Open)
                {
                    //DbGetSetup();
                }
            }
            catch
            {
            }

            if (_binaryReaderIcpStacker != null)
            {
                try
                {
                    _binaryReaderIcpStacker.Close();
                    _binaryReaderIcpStacker.Dispose();
                }
                catch
                {
                }
                finally
                {
                    _binaryReaderIcpStacker = null;
                }
            }

            if (_binaryWriterIcpStacker != null)
            {
                try
                {
                    _binaryWriterIcpStacker.Close();
                    _binaryWriterIcpStacker.Dispose();
                }
                catch
                {
                }
                finally
                {
                    _binaryWriterIcpStacker = null;
                }
            }

            if (_networkStreamIcpStacker != null)
            {
                try
                {
                    _networkStreamIcpStacker.Close();
                    _networkStreamIcpStacker.Dispose();
                }
                catch
                {
                }
                finally
                {
                    _networkStreamIcpStacker = null;
                }
            }

            if (_tcpClientIcpStacker != null)
            {
                try
                {
                    _tcpClientIcpStacker.Close();
                }
                catch
                {
                }
                finally
                {
                    _tcpClientIcpStacker = null;
                }
            }
        }
        #endregion

        #region Database communication
        private string GetNextCommandString()
        {
            byte UpdateDb = 0;
            string result = "";
            try
            {
                if (lastGripperSpeedUpdate != GripperSpeed ||
                lastMainCopieCounterUpdate != _status.NumCopiesMainCounter ||
                //lastBruttoCounterUpdate != CopiesBruttoCounter ||
                //lastNettoCounterUpdate != CopiesNettoCounter ||
                lastOverflowCounterUpdate != _status.NumCopiesOverflow ||
                lastNumCopiesStackerOnWay != NumCopiesStackerOnWay ||
                lastNumCopiesStackerFinished != NumCopiesStackerFinished)
                {
                    lastMainCopieCounterUpdate = _status.NumCopiesMainCounter;
                    lastOverflowCounterUpdate = _status.NumCopiesOverflow;
                    //lastBruttoCounterUpdate = CopiesBruttoCounter;
                    //lastNettoCounterUpdate = CopiesNettoCounter;
                    lastGripperSpeedUpdate = GripperSpeed;
                    lastNumCopiesStackerOnWay = NumCopiesStackerOnWay;
                    lastNumCopiesStackerFinished = NumCopiesStackerFinished;
                    UpdateDb = 1;
                }

                result = string.Format("DECLARE @return_value int; EXEC @return_value = [dbo].[UniStack_LCCGetNextCommand_SPH] @LineNumber = {0},@CopiesPrHour = {1},@CountersCopiesIn= {2},@CountersOverflow= {3}, @LCCStatusTableId= {4}, @LCCRequestSetMode= {5}, @IsFirst = {6}, @DoUpdateDb = {7}, @NumCopiesStackerOnWay = {8}, @NumCopiesStackerFinished = {9}; SELECT 'Return Value' = @return_value",
                    LineId, GripperSpeed, _status.NumCopiesMainCounter,
                    _status.NumCopiesOverflow, //CopiesBruttoCounter, CopiesNettoCounter,
                    (byte)_LCCStatus, -1, 0, UpdateDb, NumCopiesStackerOnWay, NumCopiesStackerFinished);

                return result;

            }
            catch
            {
            }
            return result;
        }

        private void DbControlServer()
        {
            string sql;
            //OleDbCommand cmd;
            bool ignoreCommand;
            bool doSleepAfterCommand;
            DbCommandStruct dbCommand;
            EnumDataBaseCommands currentCommand;
            //uint currentBundleIndex;
            byte sleepCounter;
            DateTime lastRequestCommand = DateTime.MinValue;
            bool isFirstBundleRequest = true;
            
            bool wasIcpOk = true;
            byte CurStackerLineId = 0;

            Thread.Sleep(2000);

            Logging.AddToLog(LogLevels.Normal, LineId, "DbControlServer started");

            while (IsStarted && _dbThreadServer != null && _dbThreadServer.ManagedThreadId == Thread.CurrentThread.ManagedThreadId)
            {
                try
                {
                    if (IcpConnectionState != EnumIcpConnectionStates.Connected || _status.ActiveState != 1 || _setup.IsPaused == 1)
                    {
                        if (wasIcpOk)
                        {
                             isFirstBundleRequest = true;
                        }

                        wasIcpOk = false;
                    }
                    else
                    {
                        wasIcpOk = false;
                    }

                    if (_conServer == null || _conServer.State != ConnectionState.Open)
                    {
                        DbConnectionStateServer = EnumDbConnectionStates.DbDisconnected;

                        if (_connectionStringServer != null && _connectionStringServer.Length > 0)
                        {
                            _conServer = new OleDbConnection(_connectionStringServer);

                            DbConnectionStateServer = EnumDbConnectionStates.DbConnecting;

                            _conServer.Open();

                            if (_conServer != null && _conServer.State == ConnectionState.Open)
                            {
                                DbConnectionStateServer = EnumDbConnectionStates.DbConnected;
                                Logging.AddToLog(LogLevels.Normal, LineId, "DbControlServer connected");
                            }
                        }
                    }

                    if (_conServer != null && _conServer.State == ConnectionState.Open)
                    {
                        sql = null;
                        ignoreCommand = true;
                        doSleepAfterCommand = true;
                        currentCommand = EnumDataBaseCommands.dbCommNone;
                        int commandListCount;
                        //GetCommand();
                        //lock (_lockStatus)
                        {
                            //lock (_lockDbCommandListServer)
                            {
                                commandListCount = _dbCommandListServer.Count;
                            }

                            if (lastRequestCommand < DateTime.Now.AddMilliseconds(-1000)) //|| (_dbCommandList.Count == 0 && _newProductionQueue.Count == 0))
                            {
                                lastRequestCommand = DateTime.Now;

                                sql = GetNextCommandString();

                                currentCommand = EnumDataBaseCommands.dbCommGetNextCommand;
                                ignoreCommand = false;
                            }
                            else
                            {
                                lock (_lockDbCommandListServer)
                                {
                                    if (_dbCommandListServer.Count > 0)
                                    {
                                        dbCommand = _dbCommandListServer[0];
                                        _dbCommandListServer.RemoveAt(0);
                                        ignoreCommand = false;
                                        doSleepAfterCommand = false;

                                        currentCommand = dbCommand.Command;
                                        CurStackerLineId = dbCommand.StackerLineId;

                                        switch (currentCommand)
                                        {
                                            case EnumDataBaseCommands.dbCommGetSetup:
                                                sql = "select * from UniStack_StackerSetupTable order by FieldName";
                                                break;

                                            case EnumDataBaseCommands.dbCommGetProductionData:
                                                //sql = "select * from UniMail_GetProductionDataInfo(1)";
                                                sql = string.Format("select * from UniMail_GetProductionDataInfo({0})", LineId);
                                                break;

                                            case EnumDataBaseCommands.dbCommGetBundleList:
                                                if (IcpConnectionState == EnumIcpConnectionStates.Connected && 
                                                    _setup.IsPaused == 0) //&& _status.ActiveState == 1 
                                                {
                                                    ushort maxBundles = dbCommand.BundleNumber;
                                                    int StackerId = dbCommand.StackerLineId;
                                                    byte isFirst = (byte)(isFirstBundleRequest ? 1 : 0);

                                                    if (!_stackerList[StackerId - 1]._newBundlesReceived && _stackerList[StackerId - 1]._newProductionQueue.Count == 0)
                                                    {
                                                        isFirstBundleRequest = false;
                                                        //sql = string.Format("EXEC dbo.UniStack_LCCGetBundlesByGripplerLine {0}, {1}, {2}, {3}",// order by SortIndex, BundleId desc",
                                                        //    LineId, maxBundles, 0, isFirst);
                                                        //sql = string.Format("EXEC dbo.UniStack_LCCGetBundlesByGripplerLineWithProductionMode {0}, {1}, {2}, {3}",// order by SortIndex, BundleId desc",
                                                        //    LineId, maxBundles, 0, isFirst);

                                                        sql = string.Format("EXEC dbo.UniStack_LCCGetBundlesSPH {0}, {1}, {2}, {3}, {4}",// order by SortIndex, BundleId desc",
                                                            LineId, maxBundles, 0, StackerId, isFirst);

                                                        //sql = string.Format("EXEC [dbo].[UniStack_LCCGetBundles2] {0}, {1}, {2}, {3}, {4}",// order by SortIndex, BundleId desc",
                                                        //   LineId, 10, 20, StackerID, isFirst);
                                                        // LineId, RequestBundleCount, CurrentBufferBundleCount, StackerID, isFirst);
                                                    }
                                                }
                                                break;

                                            //case EnumDataBaseCommands.dbCommSetLineStatus:
                                            //    //ignoreCommand = true;
                                            //    sql = string.Format("exec UniStack_LCCSetLineStatus {0}, {1}",
                                            //        LineId, dbCommand.Status);
                                            //    break;

                                            case EnumDataBaseCommands.dbCommSetBundleStatus:
                                                //ignoreCommand = true;
                                                sql = string.Format("exec UniStack_LCCSetBundleStatus {0}, {1}, {2}, '{3}', {4}, {5}",
                                                    LineId,
                                                    dbCommand.StackerLineId,
                                                    CurProductionData.TitleGroupId,
                                                    CurProductionData.IssueDate,
                                                    dbCommand.BundleNumber,
                                                    dbCommand.Status);
                                                break;

                                            //case EnumDataBaseCommands.dbCommSetStackerStatus:
                                            //    //ignoreCommand = true;
                                            //    sql = string.Format("exec UniStack_LCCSetStackerStatus {0}, {1}, {2},{3}",
                                            //        LineId, dbCommand.StackerLineId, dbCommand.AblId, dbCommand.Status); //, CopiesStacker[dbCommand.StackerID - 1]);
                                            //    break;
                                            //sql = string.Format("exec UniStack_LCCSetStackerStatus {0}, {1}, {2}",
                                            //    LineId, dbCommand.StackerID, dbCommand.Status);
                                            //break;

                                            //case EnumDataBaseCommands.dbCommSetStackerPosLineId:
                                            //    //ignoreCommand = true;
                                            //    sql = string.Format("exec UniStack_LCCSetStackerPosition {0}, {1}",
                                            //        dbCommand.StackerLineId, dbCommand.LineID);
                                            //    break;

                                            //case EnumDataBaseCommands.dbCommSetTopSheetApplicatorStatus:
                                            //    //ignoreCommand = true;
                                            //    sql = string.Format("exec UniStack_LCCSetApplicatorStatus {0}, {1}, {2}",
                                            //        LineId, dbCommand.StackerLineId, dbCommand.Status);
                                            //    break;

                                            //case EnumDataBaseCommands.dbCommSetTopSheetPrinterStatus:
                                            //    //ignoreCommand = true;
                                            //    sql = string.Format("exec UniStack_LCCSetPrinterStatus {0}, {1}, {2}, {3}",
                                            //        LineId, dbCommand.StackerLineId, dbCommand.TableId, dbCommand.Status
                                            //        );
                                            //    break;

                                            //case EnumDataBaseCommands.dbCommSetNextMachineStatus:
                                            //    //ignoreCommand = true;
                                            //    sql = string.Format("exec UniStack_LCCSetNextMachineStatus {0}, {1}, {2}",
                                            //        LineId, dbCommand.StackerLineId, dbCommand.Status);
                                            //    break;

                                            case EnumDataBaseCommands.dbCommStartLines:
                                                ignoreCommand = true;
                                                //sql = string.Format("exec UniStack_StartLine {0}", LineId);
                                                Logging.AddToLog(LogLevels.Normal, LineId, "dbCommStartLines");
                                                StartProduction();
                                                break;

                                            case EnumDataBaseCommands.dbCommStopLines:
                                                ignoreCommand = true;
                                                //sql = string.Format("exec UniStack_StopLine {0}", LineId);
                                                Logging.AddToLog(LogLevels.Normal, LineId, "dbCommStopLines");
                                                StopProduction();
                                                break;

                                            case EnumDataBaseCommands.dbCommSaveStackerLineStatus:
                                                if(CurStackerLineId > 0)
                                                {
                                                    sql = string.Format("exec [UniStack_LCCSetStackerLineStatus_SPH] {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}",
                                                         CurStackerLineId, LineId,
                                                         lastABL1Status[CurStackerLineId - 1],
                                                         lastABL2Status[CurStackerLineId - 1],
                                                         lastABL3Status[CurStackerLineId - 1],
                                                         lastPZFStatus[CurStackerLineId - 1],
                                                         lastSTBStatus[CurStackerLineId - 1],
                                                         lastPrinter1Status[CurStackerLineId - 1],
                                                         lastPrinter2Status[CurStackerLineId - 1],
                                                         lastNextMachineStatus[CurStackerLineId - 1],
                                                         lastABLCopieCounter[CurStackerLineId - 1]);
                                                }
                                                break;

                                            case EnumDataBaseCommands.dbCommLCCManualPrint:
                                                sql = string.Format("exec [UniStack_LCCGetManualPrintBundles] {0}",
                                                    LineId); 
                                                break;

                                            case EnumDataBaseCommands.dbCommSaveGripperLineStatus:
                                                //ignoreCommand = true;
                                                sql = string.Format("exec UniStack_LCCSetGripperLineStatus {0}, {1}, {2}",
                                                    LineId, lastUTRStatus, lastALSStatus);
                                                break;
                                            //case EnumDataBaseCommands.dbCommLCCGrippleLineInitial:
                                            //    sql = string.Format("exec GrippleLineInitial {0}",LineId);
                                            //    break;

                                            case EnumDataBaseCommands.dbCommSaveGripperInfo:
                                                sql = string.Format("exec [CopyTrackSave_SPH] {0}, {1}, {2}, {3}", LineId, GripperSpeed,CopiesMainCounter,CopiesOverflow);
                                                break;

                                            case EnumDataBaseCommands.dbCommSaveCopyInfo:
                                                sql = string.Format("exec [UniStack_LCCSaveFirstLastCopyInfo] {0}, {1}", LineId, isFirstTimeStamp);
                                                break;

                                        }
                                    }
                                }
                            }
                        }
                        if (sql != null &&
                            !ignoreCommand &&
                            currentCommand != EnumDataBaseCommands.dbCommNone)
                        {
                            int tickCount = Environment.TickCount;
                            if (IsStarted)
                            {
                                DbHandleCommand(_conServer, sql, currentCommand);
                            }
                            tickCount = Environment.TickCount - tickCount;
                            _dbProcessTicksServer.Add(tickCount);
                            if (_dbProcessTicksServer.Count > 5)
                            {
                                _dbProcessTicksServer.RemoveAt(0);
                            }
                        }

                        sleepCounter = 0;
                        do
                        {
                            Thread.Sleep(50);
                            //lock (_lockDbCommandListServer)
                            {
                                commandListCount = _dbCommandListServer.Count;
                            }
                        }
                        while (doSleepAfterCommand && ++sleepCounter < 20 && commandListCount == 0);

                        if (_doReconnectDbServer)
                        {
                            _doReconnectDbServer = false;
                            DbDisconnectServer();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logging.AddToLog(LogLevels.Warning, LineId, ex.ToString());
                    Thread.Sleep(1000);
                    DbDisconnectServer();
                }
                finally
                {
                    Thread.Sleep(10);
                }
            }

            if (_dbThreadServer.ManagedThreadId != Thread.CurrentThread.ManagedThreadId)
            {
                Logging.AddToLog(LogLevels.Debug, LineId, "DbControlServer old thread exited");
            }
            else
            {
                DbDisconnectServer();
                Logging.AddToLog(LogLevels.Normal, LineId, "DbControlServer stopped IsStarted " + IsStarted.ToString());
                DbConnectionStateServer = EnumDbConnectionStates.DbUnknown;
            }
        }

 

        private bool DbHandleCommand(OleDbConnection con, string sql, EnumDataBaseCommands currentCommand)
        {
            bool result = false;
            OleDbCommand cmd;
            OleDbDataReader dr;
            object[] oValues;
            List<object[]> oValuesList;
            uint firstBundleId;
            uint bundleCount;
            int rowIndex;
            int iResult1;
            //byte StackerID;
            //byte subCommand;
            //uint lastReproduceTableId;
            //int tmpIndex;
            //bool isFound;
            //int bundleId;
            //int bundleId2;
            //AddressData addressData;
            //int listCount;

            try
            {
                if (con != null && con.State == ConnectionState.Open)
                {

                    cmd = new OleDbCommand(sql, con);
                    dr = DBTools.HandleBusyExecuteReader(cmd);
                    //dr = cmd.ExecuteReader();
                    oValuesList = new List<object[]>();
                    while (dr.Read())
                    {
                        oValues = new object[dr.FieldCount];
                        dr.GetValues(oValues);
                        oValuesList.Add(oValues);
                    }
                    dr.Close();

                    switch (currentCommand)
                    {
                        case EnumDataBaseCommands.dbCommGetProductionData:
                            if (oValuesList.Count == 1)
                            {
                                CurProductionData.TitleGroupId = (short)(int)oValuesList[0][1];
                                CurProductionData.IssueDate = oValuesList[0][2].ToString();
                                CurProductionData.StandardBundle = (ushort)(int)oValuesList[0][3];
                                CurProductionData.NumBatch = (byte)(int)oValuesList[0][4];

                                if (CurProductionData.NumBatch <= 0 || CurProductionData.NumBatch > 10)
                                {
                                    CurProductionData.NumBatch = 2;
                                }

                                if (CurProductionData.StandardBundle > 200)
                                {
                                    CurProductionData.StandardBundle = 200;
                                }

                                if (CurProductionData.StandardBundle <= 0)
                                {
                                    CurProductionData.StandardBundle = 50;
                                }

                                CurProductionData.NumCopiesLayer = (byte)(CurProductionData.StandardBundle / CurProductionData.NumBatch);

                                RequestProductionDataSendToStacker();
                                //_ProductionStartReceived = true; // cheng 01-20
                            }
                            break;

                        case EnumDataBaseCommands.dbCommLCCManualPrint:
                            int _stackLineId = -1;
                            if (oValuesList.Count > 0)
                            {
                                for (int i = 0; i < oValuesList.Count; i++)
                                {
                                    oValues = oValuesList[i];
                                    _stackLineId = (int)oValues[4];

                                    TopSheetItem queueItem = new TopSheetItem
                                    {
                                        BundleId = (uint)(int)oValues[2],
                                        IsSTDBundle = (bool)oValues[3],
                                        TitleGroupId = (short)(int)oValues[0],
                                        IssueDateString = CurProductionData.IssueDate,
                                        PrinterNumber = (byte)(int)oValues[5],
                                        LabelState = 51,
                                        TopSheetType = TopSheetTypes.MANUALPRINT
                                    };

                                    if (_stackLineId > 0 && _stackLineId <= NUM_STACKERLINE)
                                    {
                                        StackerLine CurStackerLine = _stackerList[_stackLineId - 1];

                                        if (CurStackerLine.PosLineID == LineId)
                                        {
                                            if (CurStackerLine.TopSheetMain != null)
                                            {
                                                
                                                if (queueItem.BundleId >= 10000 && queueItem.BundleId < 50000)
                                                {
                                                    //_stackerList[_stackLineId].TopSheetMain._topSheetEjectedQueue.Add(queueItem);
                                                    //CurStackerLine.TopSheetMain.TsBundleQueue.Enqueue(queueItem);
                                                    CurStackerLine.TopSheetMain.AddIntoBundleQueue(queueItem);


                                                    Logging.AddToLog(LogLevels.Warning, LineId, string.Format("LCC Production ManualPrint -  Add Bundle ID: {0}, StackerLineId: {1}, PrinterNumber: {2}, TitleGroupId: {3} to TsBundleQueue, TsBundleQueueCount: {4}", queueItem.BundleId, _stackLineId, queueItem.PrinterNumber, queueItem.TitleGroupId, _stackerList[_stackLineId - 1].TopSheetMain.TsBundleQueue.Count));
                                                }
                                            }
                                        }
                                    }
                                }

                                if (_stackLineId > 0)
                                {
                                    //TopSheetItem restItem = new TopSheetItem
                                    //{
                                    //    BundleId = 65500,
                                    //    BundleIndex = 0,
                                    //    TopSheetType = TopSheetTypes.WHITEPAPER,
                                    //    LabelState = 50
                                    //};
                                    //_stackerList[_stackLineId - 1].TopSheetMain.TsBundleQueue.Enqueue(restItem);
                                    //_stackerList[_stackLineId - 1].TopSheetMain.TsBundleQueue.Enqueue(restItem);
                                    TopSheetItem restItem = new TopSheetItem
                                    {
                                        BundleId = 65500,
                                        BundleIndex = 0,
                                        TopSheetType = TopSheetTypes.WHITEPAPER,
                                        LabelState = 50
                                    };

                                    _stackerList[_stackLineId - 1].TopSheetMain.AddIntoBundleQueue(restItem);
                                    _stackerList[_stackLineId - 1].TopSheetMain.AddIntoBundleQueue(restItem);


                                    Logging.AddToLog(LogLevels.Warning, LineId, string.Format("LCC ManualPrint -  Add Two White Paper, TsqueueCount: {0}!", _stackerList[_stackLineId - 1].TopSheetMain.TsBundleQueue.Count));
                                }
                            }
                            break;
                        case EnumDataBaseCommands.dbCommGetBundleList:
                            //_newProductionQueue.Clear();
                            //oValuesList.Clear();
                            //if (_newProductionQueue.Count == 0)
                            {
                                firstBundleId = 0;
                                bundleCount = 0;
                                bool isStopped = false;
                                if (oValuesList.Count > 0)
                                {
                                    uint bundleId = (ushort)(int)oValuesList[0][0];
                                    if (bundleId == 0 || bundleId == 1)
                                    {
                                        isStopped = true;
                                        //_newProductionQueue.Add(null);
                                        if (_setup.Mode != EnumModes.STANDARD)
                                        {
                                            _setup.IsPaused = 1;
                                        }
                                    }
                                }

                                if (!isStopped)
                                {
                                    _setup.IsPaused = 0;
                                    for (rowIndex = 0; rowIndex < oValuesList.Count; rowIndex++)
                                    {
                                        oValues = oValuesList[rowIndex];
                                        BundleData newBundle = new BundleData
                                        {
                                            BundleId = (ushort)(int)oValues[0],
                                            NumCopies = (ushort)(int)oValues[1],
                                            StackerLineId = (byte)(int)oValues[2],
                                            CopiesInLayer = (ushort)(int)oValues[3],
                                            MaxCopiesInLayer = (ushort)(int)oValues[4],
                                            MinCopiesInLayer = (ushort)(int)oValues[5],
                                            MinCopiesInBundle = (ushort)(int)oValues[6],
                                            StandardBundle = (ushort)(int)oValues[9],
                                            StandardNumBatch = (byte)(int)oValues[10],
                                            IsNoTopsheet = (bool)oValues[15],
                                            BundleSeqInRoute = (EnumBundleInRoute)(int)oValues[16],
                                            RouteSortIndex = (ushort)(int)oValues[17],
                                            BundleSortIndex = (ushort)(int)oValues[18],
                                            IsSTDBundle = (bool)oValues[19],
                                            RouteModeId = (byte)(int)oValues[20]
                                        };

                                        if (newBundle.StandardBundle == 0 || newBundle.StandardBundle > 200)
                                        {
                                            newBundle.StandardBundle = 50;
                                        }

                                        if (newBundle.StandardNumBatch <= 0 || newBundle.StandardNumBatch > 10)
                                        {
                                            newBundle.StandardNumBatch = 2;
                                        }

                                        _stackerList[newBundle.StackerLineId - 1].RouteBundleSortindexList.Add(newBundle);

                                        Logging.AddToLog(LogLevels.Normal, LineId, 
                                            " Line: " + newBundle.StackerLineId + 
                                            " Bundle: " + newBundle.BundleId.ToString() + 
                                            " RouteIndex: " + newBundle.RouteSortIndex.ToString() +
                                            " BundleSortIndex: " + newBundle.BundleSortIndex.ToString() +
                                            " Status: " + newBundle.BundleSeqInRoute + 
                                            " Topsheet: " + newBundle.IsNoTopsheet +
                                            " IsSTDBundle: " + newBundle.IsSTDBundle +
                                            " Production Mode: " + newBundle.RouteModeId +
                                            " Put into RouteBundleSortindexList");

                                        //if (newBundle.BundleSeqInRoute != EnumBundleInRoute.Normal)
                                        //{
                                        //    _stackerList[newBundle.StackerLineId - 1].FirstLastRouteBundles.Add(newBundle);
                                        //    Logging.AddToLog(LogLevels.Normal, LineId, "Line: " + newBundle.StackerLineId + " Bundle: " + newBundle.BundleId.ToString() + " Status: " + newBundle.BundleSeqInRoute + " Put into FirstLastBundleList;");
                                        //}

                                        newBundle.StandardCopiesInLayer = (byte)(newBundle.StandardBundle / newBundle.StandardNumBatch);
                                        newBundle.Status = EnumBundleStatus.bundleStatusReady;

                                        //if ((ushort)(int)oValues[16] ) //IsFirstBundle
                                        //{
                                        //    firstRouteBundles.Add(newBundle.BundleId);
                                        //    Logging.AddToLog(LogLevels.Normal, LineId, "Bundle: " + newBundle.BundleId.ToString() + " Put into FirstLastBundleList;");
                                        //}

                                        //lock (_stackerList[newBundle.StackerLineId - 1]._lockNewProductionQueue)
                                        {

                                            if (newBundle.CopiesInLayer > 0)
                                            {
                                                bool isOk = newBundle.CreateLayerList();
                                                if (isOk)
                                                {
                                                    string sBundle = string.Format("TitleGroup {0} Bundle {1} added to production queue."
                                                        + " Copies={2}, Layer={3}, Max={4}, Min={5},{6}, Edition IssueDate={7}, " +
                                                        "StackerLine Id={8}, RouteSortIndex={9}, BundleSortIndex={10}, IsSTDBundle={11} "
                                                        , CurProductionData.TitleGroupId
                                                        , newBundle.BundleId
                                                        , newBundle.NumCopies
                                                        , newBundle.CopiesInLayer
                                                        , newBundle.MaxCopiesInLayer
                                                        , newBundle.MinCopiesInLayer
                                                        , newBundle.MinCopiesInBundle
                                                        , CurProductionData.IssueDate
                                                        , newBundle.StackerLineId
                                                        , newBundle.RouteSortIndex
                                                        , newBundle.BundleSortIndex
                                                        , newBundle.IsSTDBundle
                                                        );

                                                    Logging.AddToLog(LogLevels.Normal, LineId, sBundle);

                                                    //if (newBundle.StackerLineId > 0)
                                                    //{
                                                    lock (_stackerList[newBundle.StackerLineId - 1]._lockNewProductionQueue)
                                                    { 
                                                        _stackerList[newBundle.StackerLineId - 1]._newProductionQueue.Add(newBundle);

                                                    }
                                                    _stackerList[newBundle.StackerLineId - 1]._newBundlesReceived = true;
                                                    //}
                                                }
                                                else
                                                {
                                                    SetBundleStatus(EnumBundleStatus.bundleStatusBundleError,
                                                        newBundle.StackerLineId, 0, newBundle.BundleId, 0);

                                                    string sError = string.Format("Unable to add TitleGroup {0} bundle {1} to production queue."
                                                        + " Invalid layer/copy count settings."
                                                        + " Copies={2}, Layer={3}, Max={4}, Min={5},{6}"
                                                        , CurProductionData.TitleGroupId
                                                        , newBundle.BundleId
                                                        , newBundle.NumCopies
                                                        , newBundle.CopiesInLayer
                                                        , newBundle.MaxCopiesInLayer
                                                        , newBundle.MinCopiesInLayer
                                                        , newBundle.MinCopiesInBundle);

                                                    Logging.AddToLog(LogLevels.Warning, LineId, sError);
                                                }
                                            }
                                            if (firstBundleId == 0)
                                            {
                                                firstBundleId = newBundle.BundleId;
                                            }

                                            ++bundleCount;
                                        }
                                    }

                                    //if (_newProductionQueue.Count > 0 && _newProductionQueue[0].BundleId > 0)
                                    //{
                                    //    AddToLog(string.Format("Production bundles received. First bundle id = {0}, Bundle count = {1}", firstBundleId, bundleCount));
                                    //}
                                }
                            }
                            //else
                            //{
                            //    AddToLog("Production bundles received but not ready.");
                            //}
                            break;

                        case EnumDataBaseCommands.dbCommGetNextCommand:
                            if (oValuesList.Count > 0)
                            {
                                oValues = oValuesList[0];
                                iResult1 = (int)oValues[0];
                                if (iResult1 > 0)
                                {
                                    DbAddRequestedCommand((EnumDataBaseCommands)iResult1);

                                    //if (iResult1 < 100)
                                    //{
                                    //    DbAddRequestedCommand((EnumDataBaseCommands)iResult1);
                                    //}
                                    //else
                                    //{
                                    //    StackerID = (byte)(iResult1 / 100);
                                    //    subCommand = (byte)(iResult1 % 100);

                                    //    //AddToLog(string.Format("------------Stacker command received stacker {0}, command {1}------------", StackerID, subCommand));
                                    //    if (subCommand == (byte)EnumDataBaseCommands.dbCommStackerPause)
                                    //    {
                                    //        //_isPaused = true;
                                    //        _pcStatusStacker = 3;
                                    //        RequestPauseStacker(StackerID);

                                    //        Console.WriteLine("Pause Stacker!");
                                    //        Logging.AddToLog(LogLevels.Normal, LineId, string.Format("Stacker {0} paused", StackerID));
                                    //    }
                                    //    else if (subCommand == (byte)EnumDataBaseCommands.dbCommStackerResume)
                                    //    {
                                    //        //_isPaused = false;
                                    //        _pcStatusStacker = 1;
                                    //        RequestResumeStacker(StackerID);
                                    //        Logging.AddToLog(LogLevels.Normal, LineId, string.Format("Stacker {0} resumed", StackerID));
                                    //    }
                                    //}
                                }
                            }
                            break;

                        case EnumDataBaseCommands.dbCommSetBundleStatus:
                            break;

                    }//end switch

                    result = true;
                }
            }
            catch (Exception ex)
            {
                result = false;
                Logging.AddToLog(LogLevels.Error, LineId, ex.ToString() + " " + sql);
            }

            return result;
        }


        private void DbAddCommandToQueueServer(DbCommandStruct dbComm)
        {
            lock (_lockDbCommandListServer)
            {
                dbComm.CommandId = _nextDbCommandIdServer++;
                _dbCommandListServer.Add(dbComm);
            }
        }

        private void DbAddRequestedCommand(EnumDataBaseCommands command)
        {
            DbCommandStruct dbComm = new DbCommandStruct();

            bool isOkCommand = true;

            switch (command)
            {
                //case EnumDataBaseCommands.dbCommGetBundleList:
                //    dbComm.Command = (EnumDataBaseCommands)command;
                //    break;

                case EnumDataBaseCommands.dbCommGetSetup:
                    dbComm.Command = command; // EnumDataBaseCommands.dbCommGetSetup;
                    break;

                case EnumDataBaseCommands.dbCommGetProductionData:
                    dbComm.Command = command; // EnumDataBaseCommands.dbCommGetProductionData;
                    break;

                case EnumDataBaseCommands.dbCommStartLines:
                case EnumDataBaseCommands.dbCommStopLines:
                    dbComm.Command = command;
                    break;

                //case EnumDataBaseCommands.dbCommInkjetPowerOff:
                //case EnumDataBaseCommands.dbCommInkjetPowerOn:
                //case EnumDataBaseCommands.dbCommInkjetResetFaults:
                //case EnumDataBaseCommands.dbCommInkjetJetOff:
                //case EnumDataBaseCommands.dbCommInkjetJetOn:
                //case EnumDataBaseCommands.dbCommInkjetJetRefresh:
                //case EnumDataBaseCommands.dbCommInkjetJetStabilize:
                //case EnumDataBaseCommands.dbCommInkejetJetSolvent:
                //case EnumDataBaseCommands.dbCommInkejetJetUnblock:
                //case EnumDataBaseCommands.dbCommInkjetJetGutter:
                //case EnumDataBaseCommands.dbCommInkjetQueryStatus:
                //case EnumDataBaseCommands.dbCommInkjetEnable:
                //case EnumDataBaseCommands.dbCommInkjetDisable:
                //    dbComm.Command = command;
                //    break;
                case EnumDataBaseCommands.dbCommLCCManualPrint:
                    dbComm.Command = command;
                    break;
                case EnumDataBaseCommands.dbCommClearCounters:
                    RequestClearCounters();
                    isOkCommand = false;
                    break;

                case EnumDataBaseCommands.dbCommSaveStackerLineStatus:
                    dbComm.Command = command;
                    break;

                default:
                    isOkCommand = false;
                    break;
            }

            if (isOkCommand)
            {
                dbComm.StackerLineId = 0;
                dbComm.BundleNumber = 0;
                dbComm.Status = 0;
                dbComm.TableId = 0;
                dbComm.CopyNumber = 0;

                DbAddCommandToQueueServer(dbComm);
            }
        }

        private bool DbCheckCommandToServerExists(EnumDataBaseCommands command, byte StackerLineId)
        {
            bool result = false;
            int index = -1;

            lock (_lockDbCommandListServer)
            {
                while (!result && ++index < _dbCommandListServer.Count)
                {
                    if (_dbCommandListServer[index].Command == EnumDataBaseCommands.dbCommGetBundleList &&
                        _dbCommandListServer[index].StackerLineId == StackerLineId)
                    {
                        result = true;
                    }
                }
            }

            return result;
        }

        private void DbSetBundleStatus(EnumBundleStatus bundleStatus, byte StackerLineId, byte AblId,
            uint bundleNumber, int copyNumber)
        {
            DbCommandStruct dbComm = new DbCommandStruct
            {
                StackerLineId = StackerLineId,
                AblId = AblId,
                BundleNumber = (ushort)(bundleNumber),
                Status = (byte)bundleStatus,
                Command = EnumDataBaseCommands.dbCommSetBundleStatus,
                TableId = 0,
                CopyNumber = copyNumber
            };

            DbAddCommandToQueueServer(dbComm);
        }

        private void DbGetSetup()
        {
            DbCommandStruct dbComm = new DbCommandStruct
            {
                Command = EnumDataBaseCommands.dbCommGetSetup,
                StackerLineId = 0,
                BundleNumber = 0,
                Status = 0,
                TableId = 0,
                CopyNumber = 0
            };

            DbAddCommandToQueueServer(dbComm);
        }

        private void DbGetBundleList(ushort numBundles, byte StackerLineId)
        {
            if (!DbCheckCommandToServerExists(EnumDataBaseCommands.dbCommGetBundleList, StackerLineId))
            {
                DbCommandStruct dbComm = new DbCommandStruct
                {
                    Command = EnumDataBaseCommands.dbCommGetBundleList,
                    StackerLineId = StackerLineId,
                    BundleNumber = numBundles,
                    Status = 0,
                    TableId = 0,
                    CopyNumber = 0
                };

                DbAddCommandToQueueServer(dbComm);
            }
        }

        //private void DbSetStackerStatus(enumStackerStatus stackerStatus, byte StackerLineId,  byte AblId)
        //{
        //    DbCommandStruct dbComm = new DbCommandStruct
        //    {
        //        Command = EnumDataBaseCommands.dbCommSetStackerStatus,
        //        StackerLineId = StackerLineId,
        //        AblId = AblId,
        //        BundleNumber = 0,
        //        Status = (byte)stackerStatus,
        //        TableId = 0,
        //        CopyNumber = 0
        //    };
        //    DbAddCommandToQueueServer(dbComm);
        //}

        //private void DbSetStackerPosID(byte PosLineId, byte StackerLineId)
        //{
        //    DbCommandStruct dbComm = new DbCommandStruct
        //    {
        //        Command = EnumDataBaseCommands.dbCommSetStackerPosLineId,
        //        StackerLineId = StackerLineId,
        //        LineID = PosLineId,
        //        BundleNumber = 0,
        //        Status = 0,
        //        TableId = 0,
        //        CopyNumber = 0
        //    };

        //    DbAddCommandToQueueServer(dbComm);
        //}

        //private void DbSetApplicatorStatus(TopSheetControlLib.TopSheetMain.TopSheetApplicatorStates status,
        //    byte StackerLineId)
        //{
        //    DbCommandStruct dbComm = new DbCommandStruct
        //    {
        //        Command = EnumDataBaseCommands.dbCommSetTopSheetApplicatorStatus,
        //        StackerLineId = StackerLineId,
        //        BundleNumber = 0,
        //        Status = (byte)status,
        //        TableId = 0,
        //        CopyNumber = 0
        //    };

        //    DbAddCommandToQueueServer(dbComm);
        //}

        //private void DbSetPrinterStatus(TopSheetControlLib.TopSheetMain.TopSheetPrinterStates status,
        //    byte StackerLineId, byte printerNumber)
        //{
        //    DbCommandStruct dbComm = new DbCommandStruct
        //    {
        //        Command = EnumDataBaseCommands.dbCommSetTopSheetPrinterStatus,
        //        StackerLineId = StackerLineId,
        //        BundleNumber = 0,
        //        Status = (byte)status,
        //        TableId = printerNumber,
        //        CopyNumber = 0
        //    };

        //    DbAddCommandToQueueServer(dbComm);
        //}

        //private void DbStartLines()
        //{
        //    DbCommandStruct dbComm = new DbCommandStruct
        //    {
        //        Command = EnumDataBaseCommands.dbCommStartLines,
        //        StackerLineId = 0,
        //        BundleNumber = 0,
        //        Status = 0,
        //        TableId = 0,
        //        CopyNumber = 0
        //    };

        //    DbAddCommandToQueueServer(dbComm);
        //}

        //private void DbStopLines()
        //{
        //    DbCommandStruct dbComm = new DbCommandStruct
        //    {
        //        Command = EnumDataBaseCommands.dbCommStopLines,
        //        StackerLineId = 0,
        //        BundleNumber = 0,
        //        Status = 0,
        //        TableId = 0,
        //        CopyNumber = 0
        //    };

        //    DbAddCommandToQueueServer(dbComm);
        //}

        private void DbSaveStackerLineStatus(int _StackerLineIndex)
        {
            DbCommandStruct dbComm = new DbCommandStruct
            {
                Command = EnumDataBaseCommands.dbCommSaveStackerLineStatus,
                StackerLineId = (byte)(_StackerLineIndex+1),
                BundleNumber = 0,
                Status = 0,
                TableId = 0,
                CopyNumber = 0
            };

            DbAddCommandToQueueServer(dbComm);
        }

        private void DbSaveGripperLineStatus()
        {
            DbCommandStruct dbComm = new DbCommandStruct
            {
                Command = EnumDataBaseCommands.dbCommSaveGripperLineStatus,
                StackerLineId = 0, // LineId,
                BundleNumber = 0,
                Status = 0, // (byte)lastALSStatus,
                TableId = 0,
                CopyNumber = 0
            };

            DbAddCommandToQueueServer(dbComm);
        }


        private void DbSaveGripperLineInfo()
        {
            DbCommandStruct dbComm = new DbCommandStruct
            {
                Command = EnumDataBaseCommands.dbCommSaveGripperInfo,
                StackerLineId = 0,
                BundleNumber = 0,
                Status = 0,
                TableId = 0,
                CopyNumber = 0
            };

            DbAddCommandToQueueServer(dbComm);
        }

        //private void DbInitialGrippleLineStatus()
        //{
        //    DbCommandStruct dbComm = new DbCommandStruct
        //    {
        //        Command = EnumDataBaseCommands.dbCommLCCGrippleLineInitial,
        //        StackerLineId = 0,
        //        BundleNumber = 0,
        //        Status = 0,
        //        TableId = 0,
        //        CopyNumber = 0
        //    };
        //    DbAddCommandToQueueServer(dbComm);

        //}

        private void DbSaveCopyInfo()
        {
            DbCommandStruct dbComm = new DbCommandStruct
            {
                Command = EnumDataBaseCommands.dbCommSaveCopyInfo,
                StackerLineId = 0,
                BundleNumber = 0,
                Status = 0,
                TableId = 0,
                CopyNumber = 0
            };

            DbAddCommandToQueueServer(dbComm);
        }

        private void DbDisconnectServer()
        {
            DbConnectionStateServer = EnumDbConnectionStates.DbDisconnected;

            if (_conServer != null)
            {
                try
                {
                    if (_conServer != null && _conServer.State == ConnectionState.Open)
                    {
                        _conServer.Close();
                        Logging.AddToLog(LogLevels.Normal, LineId, "DbControlServer disconnected");
                    }
                }
                catch (Exception ex)
                {
                    Logging.AddToLog(LogLevels.Warning, LineId, ex.ToString());
                    Thread.Sleep(100);
                }
                finally
                {
                    _conServer = null;
                    Thread.Sleep(100);
                }
            }
        }

        #endregion

        #region Production Status Control
        private void ProductionStatusControl()
        {

            while (IsStarted && IsStatusCheckStarted &&
                _productionStatusCheckThread != null &&
                _productionStatusCheckThread.ManagedThreadId == Thread.CurrentThread.ManagedThreadId)
            {
                try
                {
                    if (lastALSStatus != (byte)ALSStatus || lastUTRStatus != (byte)ProductionUTRStatus)
                    {
                        lastALSStatus = (byte)ALSStatus;
                        lastUTRStatus = (byte)ProductionUTRStatus;
                        DbSaveGripperLineStatus();
                    }
                  
                    for (int i = 0; i < _stackerList.Count; i++)
                    {
                        if (_stackerList[i].PosLineID == LineId)
                        {
                            _stackerList[i].UpdatePrinterMachineStatus();

                            if (lastABL1Status[i] != (byte)_stackerList[i].ABLList[0].MachineStatus ||
                                lastABL2Status[i] != (byte)_stackerList[i].ABLList[1].MachineStatus ||
                                lastABL3Status[i] != (byte)_stackerList[i].ABLList[2].MachineStatus ||
                                lastPZFStatus[i] != (byte)_stackerList[i].PZFMachineStatus ||
                                lastSTBStatus[i] != (byte)_stackerList[i].STBMachineStatus ||
                                lastPrinter1Status[i] != (byte)_stackerList[i].PrinterMachineStatus[0] ||
                                lastPrinter2Status[i] != (byte)_stackerList[i].PrinterMachineStatus[1] ||
                                lastNextMachineStatus[i] != (byte)_stackerList[i].NextMachineStatus ||
                                lastABLCopieCounter[i] != NumCopiesCounterABLGroup[i] ||
                                (lastProductionStartStatus[i] == true && !_stackerList[i].TopSheetMain.IsProductionStarted))
                            {
                               
                                lastABL1Status[i] = (byte)_stackerList[i].ABLList[0].MachineStatus;
                                lastABL2Status[i] = (byte)_stackerList[i].ABLList[1].MachineStatus;
                                lastABL3Status[i] = (byte)_stackerList[i].ABLList[2].MachineStatus;
                                lastPZFStatus[i] = (byte)_stackerList[i].PZFMachineStatus;
                                lastSTBStatus[i] = (byte)_stackerList[i].STBMachineStatus;
                                lastPrinter1Status[i] = (byte)_stackerList[i].PrinterMachineStatus[0];
                                lastPrinter2Status[i] = (byte)_stackerList[i].PrinterMachineStatus[1];
                                lastNextMachineStatus[i] = (byte)_stackerList[i].NextMachineStatus;

                                if (lastABLCopieCounter[i] >= 0 && NumCopiesCounterABLGroup[i] > 0)
                                {
                                    lastABLCopieCounter[i] = NumCopiesCounterABLGroup[i];
                                }
                                else if (lastProductionStartStatus[i] == false && _stackerList[i].TopSheetMain.IsProductionStarted)
                                {
                                    lastABLCopieCounter[i] = 0;
                                }

                                lastProductionStartStatus[i] = _stackerList[i].TopSheetMain.IsProductionStarted;


                                DbSaveStackerLineStatus(i);
                            }

                        }
                    }
                }
                catch
                {

                }
                finally
                {
                    Thread.Sleep(5000); //Sleep 5 seconds
                }
            }
        }

        private void StoppedStatus()
        {
            try
            {
                lastALSStatus = (byte)EnumMachineStatus.Unknown;
                lastUTRStatus = (byte)EnumMachineStatus.Unknown;

                for (int i = 0; i < _stackerList.Count; i++)
                {
                    if (_stackerList[i].PosLineID == LineId)
                    {
                        lastABL1Status[i] = (byte)EnumMachineStatus.Unknown;
                        lastABL2Status[i] = (byte)EnumMachineStatus.Unknown;
                        lastABL3Status[i] = (byte)EnumMachineStatus.Unknown;
                        lastPZFStatus[i] = (byte)EnumMachineStatus.Unknown;
                        lastSTBStatus[i] = (byte)EnumMachineStatus.Unknown;
                        lastPrinter1Status[i] = (byte)EnumMachineStatus.Unknown;
                        lastPrinter2Status[i] = (byte)EnumMachineStatus.Unknown;
                        lastNextMachineStatus[i] = (byte)EnumMachineStatus.Unknown;
                        lastABLCopieCounter[i] = 0;
                        DbSaveStackerLineStatus(i);
                    }
                }

                DbSaveGripperLineStatus();
                Logging.AddToLog(LogLevels.Normal, LineId, "Stoped Status Uploaded!");
            }
            catch(Exception ex)
            {
                Logging.AddToLog(LogLevels.Error, LineId, "Stoped Status Uploaded Error: " + ex.ToString());
            }

        }
        #endregion

        #region Save Copie TimeStamp
        private void FirstLastCopySaveControl()
        {
            uint lastMainCounter = 1;
            isFirstCopySet = false;

            while (IsStarted &&
               _firstLastCopySaveThread != null &&
               _firstLastCopySaveThread.ManagedThreadId == Thread.CurrentThread.ManagedThreadId)
            {
                try
                {
                    if(CopiesMainCounter > 0)
                    {
                        if (CopiesMainCounter <= 100 && !isFirstCopySet)
                        {
                            isFirstTimeStamp = 0;
                            Logging.AddToLog(LogLevels.Debug, LineId, "First Copy Maincounter: " + CopiesMainCounter);
                            DbSaveCopyInfo();
                            isFirstCopySet = true;
                        }
                        else
                        {
                            if (lastMainCounter != CopiesMainCounter)
                            {
                                lastMainCounter = CopiesMainCounter;
                                isFirstTimeStamp = 1;
                                DbSaveCopyInfo();
                            }
                        }
                    }
                }
                catch
                {
                }
                finally
                {
                    Thread.Sleep(10000);
                }
            }

        }

        #endregion

        #region Gripper Info Control
        private void GripperInfoSaveControl()
        {
            uint lastGripperSpeed = 1;
            //int lastBruttoCounter = 1;
            //int lastNettoCounter = 1;
            uint lastMainCounter = 1;
            ulong lastOverflowCounter = 1;

            while (IsStarted &&
                _gripperInfoSaveThread != null &&
                _gripperInfoSaveThread.ManagedThreadId == Thread.CurrentThread.ManagedThreadId)
            {
                try
                {
                    if (lastGripperSpeed != GripperSpeed ||
                        lastMainCounter != CopiesMainCounter ||
                        //lastBruttoCounter != CopiesBruttoCounter ||
                        //lastNettoCounter != CopiesNettoCounter ||
                        lastOverflowCounter != CopiesOverflow)
                    {
                        lastGripperSpeed = GripperSpeed;
                        lastOverflowCounter = CopiesOverflow;
                        lastMainCounter = CopiesMainCounter;
                        //lastBruttoCounter = CopiesBruttoCounter;
                        //lastNettoCounter = CopiesNettoCounter;

                        DbSaveGripperLineInfo();
                    }
                }
                catch
                {
                }
                finally
                {
                    Thread.Sleep(20000);
                }
            }
        }

       
        #endregion

        #region Get Current Mode
        private void GetCurrentMode()
        {
            while (IsStarted &&
                _productionCurrentModeThread != null &&
                _productionCurrentModeThread.ManagedThreadId == Thread.CurrentThread.ManagedThreadId)
            {
                try
                {
                    GetProductionCurrentMode();
                }
                catch
                {
                }
                finally
                {
                    Thread.Sleep(5000);
                }
            }
        }

        private void GetProductionCurrentMode()
        {
            string connsql = "Data Source=" + _connectionString[0] + ";Initial Catalog=" + _connectionString[1] + ";Integrated Security=False;User ID=" + _connectionString[2] + ";Password=" + _connectionString[3] + "";

            SqlConnection sqlCon = new SqlConnection(connsql);
            try
            {
                SqlCommand cmd = new SqlCommand("select * from UniMail_LCCGetProductionCurrentMode(" + LineId + ")", sqlCon)
                {
                    CommandType = CommandType.Text
                };
                sqlCon.Open();
                SqlDependency dep = new SqlDependency(cmd);
                SqlDataReader reader = cmd.ExecuteReader();
                List<int> CurrentModeInfo = new List<int>();

                while (reader.Read())
                {
                    CurrentModeInfo.Add(int.Parse(reader["CurrentMode"].ToString()));
                    
                }

                if (CurrentModeInfo.Count != 0)
                {
                    CurrentMode = CurrentModeInfo[0];
                }

                reader.Close();
            }
            catch
            {
            }
            finally
            {
                sqlCon.Close();
            }
        }
        #endregion

        private bool pingTest(string ipAddress)
        {
            Ping ping = new Ping();
            PingReply pingStatus = ping.Send(IPAddress.Parse(ipAddress), 1000);

            if (pingStatus.Status == IPStatus.Success)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
    }
}
