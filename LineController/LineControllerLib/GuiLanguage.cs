﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CN80ControlLib
{
    public class GuiLanguage
    {
        #region Variables

        private List<string> _guiLanguageItems;

        #endregion

        #region Properties

        public List<string> GuiLanguageItems
        {
            get { return _guiLanguageItems; }
            set { _guiLanguageItems = value; }
        }

        #endregion
    }
}
