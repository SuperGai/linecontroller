﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LineControllerLib
{
    public class Setup
    {
        #region Variables

        private int _StackerID;
        private string _serverAddress;

        #endregion

        #region Constructor

        public Setup()
        {
        }

        #endregion

        #region Properties

        private int StackerID
        {
            get { return _StackerID; }
            set { _StackerID = value; }
        }

        private string ServerAddress
        {
            get { return _serverAddress; }
            set { _serverAddress = value; }
        }

        #endregion
    }
}
