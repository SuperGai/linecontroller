﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace LineControllerLib
{
    public enum LogLevels
    {
        Error = 0,
        Warning = 1,
        Normal = 2,
        Debug = 3,
    }

    public class Logging
    {
        #region Variables
        private static object _lockLogFile;
        public static string LogFolder;
        private static ulong _id;
        #endregion

        #region Create log folder/ xsl
        private static void CreateLogCatalogue(ref string logFolder)
        {
            if (!Directory.Exists(logFolder))
            {
                Directory.CreateDirectory(logFolder);
            }

            //logFolder += "\\" + DateTime.Now.ToString("yyyy-MM");
            logFolder += "\\" + DateTime.Now.ToString("yyyy-MM");

            if (!Directory.Exists(logFolder))
            {
                Directory.CreateDirectory(logFolder);
            }

            string xslFile = logFolder + "\\LogStyle.xsl";
            if (!File.Exists(xslFile))
            {
                FileStream fs = null;
                StreamWriter sw = null;
                try
                {
                    string s = GetXSLFileOutput();

                    fs = new System.IO.FileStream(xslFile, FileMode.OpenOrCreate, FileAccess.Write);
                    sw = new StreamWriter(fs);
                    sw.Write(s);
                    sw.Flush();
                }
                catch
                {
                }
                finally
                {
                    if (sw != null)
                    {
                        sw.Close();
                    }

                    //if (fs != null)
                    //{
                    //    fs.Close();
                    //}
                }
            }
        }

        private static string GetXSLFileOutput()
        {
            string s = "<?xml version=\"1.0\"?>\r\n";
            s += "<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">\r\n";
            s += "\r\n";
            s += "<!-- localized strings -->\r\n";
            s += "<xsl:variable name='ColumnHeader_Id'>Id</xsl:variable>\r\n";
            s += "<xsl:variable name='ColumnHeader_TimeStamp'>TimeStamp</xsl:variable>\r\n";
            s += "<xsl:variable name='ColumnHeader_Level'>Level</xsl:variable>\r\n";
            s += "<xsl:variable name='ColumnHeader_Message'>Message</xsl:variable>\r\n";
            s += "\r\n";
            s += "<!-- variables -->\r\n";
            s += "\r\n";
            s += "<xsl:variable name='TableStyle'>font-family:Verdana; font-size:67%;" +
                "text-align:left; vertical-align:top;table-layout:fixed</xsl:variable>\r\n";
            s += "<xsl:variable name='GutterStyle'>width:2ex</xsl:variable>\r\n";
            s += "<xsl:variable name='HeaderStyle'>border-bottom:1 solid black</xsl:variable>\r\n";
            s += "\r\n";
            s += "<xsl:variable name='ColorDebug'>background-color:#FFFFFF; color:#FF3300</xsl:variable>\r\n";
            s += "<xsl:variable name='ColorError'>background-color:#FF3300</xsl:variable>\r\n";
            s += "<xsl:variable name='ColorWarning'>background-color:#FFFF00</xsl:variable>\r\n";
            s += "<xsl:variable name='ColorNormal'>background-color:#FFFFFF</xsl:variable>\r\n";
            s += "\r\n";
            s += "<xsl:variable name='LevelText_Normal'>Normal</xsl:variable>\r\n";
            s += "<xsl:variable name='LevelText_Warning'>Warning</xsl:variable>\r\n";
            s += "<xsl:variable name='LevelText_Error'>Error</xsl:variable>\r\n";
            s += "<xsl:variable name='LevelText_Debug'>Debug</xsl:variable>\r\n";
            s += "\r\n";
            s += "<xsl:template match=\"EventLog\">\r\n";
            s += "\r\n";
            s += "    <html dir='ltr'>\r\n";
            s += "    <head>\r\n";
            s += "        <title>";
            s += "                Eventlog()\r\n";
            s += "        </title>\r\n";
            s += "    </head>\r\n";
            s += "\r\n";
            s += "    <body style='margin:0'>\r\n";
            s += "\r\n";
            s += "        <table id='BodyTable' style=\"{$TableStyle}\" cellspacing='0'>\r\n";
            s += "\r\n";
            s += "            <col style=\"width:11;\"/>\r\n";
            s += "            <col style='{$GutterStyle}' />\r\n";
            s += "            <col style=\"width:30ex;\"/>\r\n";
            s += "            <col style='{$GutterStyle}' />\r\n";
            s += "            <col style=\"width:11ex;\"/>\r\n";
            s += "            <col style='{$GutterStyle}' />";
            s += "            <col style=\"width:150ex;\"/>\r\n";
            s += "\r\n";
            s += "            <thead>\r\n";
            s += "                <tr>\r\n";
            s += "                    <th style=\"{$HeaderStyle}\">\r\n";
            s += "                        <xsl:value-of select=\"$ColumnHeader_Id\"/>\r\n";
            s += "                    </th>\r\n";
            s += "                    <th/>\r\n";
            s += "                    <th style=\"{$HeaderStyle}\">\r\n";
            s += "                        <xsl:value-of select=\"$ColumnHeader_TimeStamp\"/>\r\n";
            s += "                    </th>\r\n";
            s += "                    <th/>\r\n";
            s += "                    <th style=\"{$HeaderStyle}\">\r\n";
            s += "                        <xsl:value-of select=\"$ColumnHeader_Level\"/>\r\n";
            s += "                    </th>\r\n";
            s += "                    <th/>\r\n";
            s += "                    <th style=\"{$HeaderStyle}\">\r\n";
            s += "                        <xsl:value-of select=\"$ColumnHeader_Message\"/>\r\n";
            s += "                    </th>\r\n";
            s += "                </tr>\r\n";
            s += "            </thead>\r\n";
            s += "\r\n";
            s += "            <tbody style='vertical-align:top'>\r\n";
            s += "                <xsl:apply-templates>\r\n";
            s += "                    <xsl:sort select='TimeStamp'  order='descending'/>\r\n";
            s += "                    <xsl:sort select='Id'  order='descending'/>\r\n";
            s += "                </xsl:apply-templates>\r\n";
            s += "            </tbody>\r\n";
            s += "        </table>\r\n";
            s += "    </body>\r\n";
            s += "    </html>\r\n";
            s += "\r\n";
            s += "</xsl:template>\r\n";
            s += "\r\n";
            s += "\r\n";
            s += "<xsl:template match=\"NewEvent\">\r\n";
            s += "    <xsl:variable name='Level'><xsl:apply-templates select=\"Level\"/></xsl:variable>\r\n";
            s += "\r\n";
            s += "    <tr>\r\n";
            s += "        <xsl:if test=\"$Level = 2\">\r\n";
            s += "            <xsl:attribute name=\"style\">\r\n";
            s += "                <xsl:value-of select=\"$ColorNormal\"/>\r\n";
            s += "            </xsl:attribute>\r\n";
            s += "        </xsl:if>\r\n";
            s += "\r\n";
            s += "        <xsl:if test=\"$Level = 1\">\r\n";
            s += "            <xsl:attribute name=\"style\">\r\n";
            s += "                <xsl:value-of select=\"$ColorWarning\"/>\r\n";
            s += "            </xsl:attribute>\r\n";
            s += "        </xsl:if>\r\n";
            s += "\r\n";
            s += "        <xsl:if test=\"$Level = 0\">\r\n";
            s += "            <xsl:attribute name=\"style\">\r\n";
            s += "                <xsl:value-of select=\"$ColorError\"/>\r\n";
            s += "            </xsl:attribute>\r\n";
            s += "        </xsl:if>\r\n";
            s += "\r\n";
            s += "        <xsl:if test=\"$Level = 3\">\r\n";
            s += "            <xsl:attribute name=\"style\">\r\n";
            s += "                <xsl:value-of select=\"$ColorDebug\"/>\r\n";
            s += "            </xsl:attribute>\r\n";
            s += "        </xsl:if>\r\n";
            s += "\r\n";
            s += "\r\n";
            s += "\r\n";
            s += "        <xsl:if test=\"$Level = 2\">\r\n";
            s += "            <td> <xsl:apply-templates select=\"Id\"/> </td>\r\n";
            s += "            <td/>\r\n";
            s += "\r\n";
            s += "            <td> <xsl:apply-templates select=\"TimeStamp\"/> </td>\r\n";
            s += "            <td/>\r\n";
            s += "\r\n";
            s += "               <td> <xsl:value-of select=\"$LevelText_Normal\"/> </td>\r\n";
            s += "            <td/>\r\n";
            s += "\r\n";
            s += "            <td> <xsl:apply-templates select=\"Message\"/> </td>\r\n";
            s += "            <td/>\r\n";
            s += "        </xsl:if>\r\n";
            s += "\r\n";
            s += "        <xsl:if test=\"$Level = 1\">\r\n";
            s += "            <td> <xsl:apply-templates select=\"Id\"/> </td>\r\n";
            s += "            <td/>\r\n";
            s += "\r\n";
            s += "            <td> <xsl:apply-templates select=\"TimeStamp\"/> </td>\r\n";
            s += "            <td/>\r\n";
            s += "\r\n";
            s += "               <td> <xsl:value-of select=\"$LevelText_Warning\"/> </td>\r\n";
            s += "            <td/>\r\n";
            s += "\r\n";
            s += "            <td> <xsl:apply-templates select=\"Message\"/> </td>\r\n";
            s += "            <td/>\r\n";
            s += "        </xsl:if>\r\n";
            s += "\r\n";
            s += "        <xsl:if test=\"$Level = 0\">\r\n";
            s += "            <td> <xsl:apply-templates select=\"Id\"/> </td>\r\n";
            s += "            <td/>\r\n";
            s += "\r\n";
            s += "            <td> <xsl:apply-templates select=\"TimeStamp\"/> </td>\r\n";
            s += "            <td/>\r\n";
            s += "\r\n";
            s += "               <td> <xsl:value-of select=\"$LevelText_Error\"/> </td>\r\n";
            s += "            <td/>\r\n";
            s += "\r\n";
            s += "            <td> <xsl:apply-templates select=\"Message\"/> </td>\r\n";
            s += "            <td/>\r\n";
            s += "        </xsl:if>\r\n";
            s += "\r\n";
            s += "        <xsl:if test=\"$Level = 3\">\r\n";
            s += "            <td> <xsl:apply-templates select=\"Id\"/> </td>\r\n";
            s += "            <td/>\r\n";
            s += "\r\n";
            s += "            <td> <xsl:apply-templates select=\"TimeStamp\"/> </td>\r\n";
            s += "            <td/>\r\n";
            s += "\r\n";
            s += "               <td> <xsl:value-of select=\"$LevelText_Debug\"/> </td>\r\n";
            s += "            <td/>\r\n";
            s += "\r\n";
            s += "            <td> <xsl:apply-templates select=\"Message\"/> </td>\r\n";
            s += "            <td/>\r\n";
            s += "        </xsl:if>\r\n";
            s += "    </tr>\r\n";
            s += "</xsl:template>\r\n";
            s += "\r\n";
            s += "</xsl:stylesheet>\r\n";
            return s;
        }

        #endregion

        #region Add to log

        public static void AddToLog(LogLevels logLevel, string message)
        {
            try
            {
                AddToLog(LogFolder, logLevel, 0, message);
            }
            catch
            {
            }
        }

        public static void AddToLog(LogLevels logLevel, byte LineId, string message)
        {
            try
            {
                AddToLog(LogFolder, logLevel, LineId, message);
            }
            catch
            {
            }
        }

        //public static void AddToLog(LogItem[] logItems)
        //{
        //    try
        //    {
        //        AddToLog(LogFolder, 1, logItems);
        //    }
        //    catch
        //    {
        //    }
        //}

        public static void AddToLog(string logFolder, LogLevels logLevel, byte LineId, string message)
        {
            LogItem logItem = new LogItem();
            logItem.LogLevel = logLevel;
            logItem.Message = message;
            logItem.LogTime = DateTime.Now;
            LogItem[] logItems = new LogItem[] { logItem };

            AddToLog(logFolder, LineId, logItems);
        }

        public static void AddToLog(string logFolder, byte LineId, LogItem[] logItems)
        {
            try
            {
                if (logFolder != null && logFolder.Length > 0)
                {
                    if (_lockLogFile == null)
                    {
                        _lockLogFile = new object();
                    }

                    //if (message != null && !message.Contains("ThreadInterruptedException"))
                    {
                        lock (_lockLogFile)
                        {
                            CreateLogCatalogue(ref logFolder);
                            WriteToTextFile(logFolder, LineId, logItems);
                        }
                    }
                }
            }
            catch
            {
            }
        }

        private static void WriteToTextFile(string logFolder, byte LineId, LogItem[] logItems)
        {
            string fileName = logFolder + "\\L"+ LineId.ToString() + "_" + DateTime.Now.ToString("dd") + ".xml";
            //string fileName = logFolder + "\\L" + LineId.ToString() + "_" + DateTime.Now.ToString("dd") + ".xml";
            FileStream fs = null;
            StreamWriter sw = null;

            try
            {
                try
                {
                    FileInfo fi = new FileInfo(fileName);
                    if (fi.Exists && fi.Length > 100000)
                    {
                        string newName = fi.DirectoryName + "\\" + fi.Name + DateTime.Now.ToString("HHmmss_f") + ".xml";
                        fi.MoveTo(newName);
                    }
                }
                catch
                {
                }

                fs = new FileStream(fileName, FileMode.OpenOrCreate, FileAccess.Write);
                bool isNew = fs.Length == 0;

                if (!isNew)
                {
                    fs.Seek(-12, SeekOrigin.End);
                }

                sw = new StreamWriter(fs);
                string s = "";
                for (int i = 0; i < logItems.Length; i++)
                {
                    LogItem logItem = logItems[i];
                    if (logItem.Message != null && !logItem.Message.Contains("ThreadInterruptedException"))
                    {
                        ++_id;

                        if (isNew && i > 0)
                        {
                            isNew = false;
                        }
                        bool isLast = i >= logItems.Length - 1;

                        s += GetXmlContent(isNew, isLast, logItem.LogLevel, logItem.Message, logItem.LogTime);// + "\r\n";
                    }
                }

                sw.Write(s);
                sw.Flush();
            }
            catch
            {
            }
            finally
            {
                if (sw != null)
                {
                    sw.Close();
                }

                //if (fs != null)
                //{
                //    fs.Close();
                //}
            }
        }

        private static string GetXmlContent(bool isNew, bool isLast,
            LogLevels logLevel, string message, DateTime logTime)
        {
            string s;

            if (isNew)
            {
                s = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
                s += "<?xml-stylesheet type='text/xsl' href='LogStyle.xsl'?>\r\n";
                s += "<EventLog>\r\n";
            }
            else
            {
                s = "\r\n";
            }

            string id = _id <= 999999 ? _id.ToString("000000") : _id.ToString();
            string timeStamp = logTime.ToString("yyyy-MM-dd HH:mm:ss.fff");
            string level = ((int)logLevel).ToString();
            message = message.Replace("&", "&amp;");
            message = message.Replace("'", "&apos;");
            message = message.Replace("\"", "&quot;");
            message = message.Replace("<", "&lt;");
            message = message.Replace(">", "&gt;");

            s += "<NewEvent>" +
                "<Id>" + id + "</Id>" +
                "<TimeStamp>" + timeStamp + "</TimeStamp>" +
                "<Level>" + level + "</Level>" +
                "<Message>" + message + "</Message>" + "</NewEvent>\r\n";

            if (isLast)
            {
                s += "</EventLog>";
            }

            return s;
        }

        #endregion

        #region Show log

        //public static void ShowLog()
        //{
        //    try
        //    {
        //        ShowLog(LogFolder);
        //    }
        //    catch
        //    {
        //    }
        //}

        //public static void ShowLog(string logPath)
        //{
        //    try
        //    {
        //        ShowLog(logPath, false);
        //    }
        //    catch
        //    {
        //    }
        //}

        //private static void ShowLog(string logPath, bool abortOnFail)
        //{
        //    if (logPath != null && logPath.Length > 0)
        //    {
        //        bool isOk = false;

        //        if (Directory.Exists(logPath))
        //        {
        //            logPath += "\\" + DateTime.Now.ToString("yyyy-MM");
        //            if (Directory.Exists(logPath))
        //            {
        //                string fileName = logPath + "\\" + DateTime.Now.ToString("dd") + ".xml";
        //                if (File.Exists(fileName))
        //                {
        //                    isOk = true;
        //                    System.Diagnostics.Process.Start(fileName);
        //                }
        //            }
        //        }

        //        if (!isOk && !abortOnFail)
        //        {
        //            AddToLog(logPath, LogLevels.Normal, "No events logged yet today.");
        //            ShowLog(logPath, true);
        //        }
        //    }
        //}

        #endregion
    }
}
