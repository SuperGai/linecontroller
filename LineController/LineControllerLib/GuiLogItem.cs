﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LineControllerLib
{
    public class GuiLogItem
    {
        public GuiLogItem(DateTime registered, LogLevels logLevel, string logMessage)
        {
            Registered = registered;
            LogLevel = logLevel;
            LogMessage = logMessage;
        }

        public DateTime Registered { get; set; }
        public LogLevels LogLevel { get; set; }
        public string LogMessage { get; set; }
    }
}
