﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LineControllerLib
{
    public class BadBundleItem
    {
        public uint BundleId { get; set; }
        public byte CopyNumber { get; set; }

        public BadBundleItem(uint bundleId, byte copyNumber)
        {
            BundleId = bundleId;
            CopyNumber = copyNumber;
        }
    }
}
