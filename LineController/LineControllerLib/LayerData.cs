﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LineControllerLib
{
    public class LayerData
    {
        public LayerData(byte StackerNr,
            uint bundleId, byte layerNumber, byte LayerFlag,
            byte numCopiesInLayer, ushort numCopiesInBundle, byte numLayersInBundle)
        {
            StackerID = StackerNr;
            BundleId = bundleId;
            LayerNumber = layerNumber;
            Flags = LayerFlag;
            NumCopiesInLayer = numCopiesInLayer;
            NumCopiesInBundle = numCopiesInBundle;
            NumLayersInBundle = numLayersInBundle;
        }

        public byte StackerID;
        public byte SeparationID;
        public uint BundleId;
        public byte LayerNumber;
        public byte Flags;
        public byte NumCopiesInLayer;
        public ushort NumCopiesInBundle;
        public byte NumLayersInBundle;
        public byte NumAddress;
    }
}
