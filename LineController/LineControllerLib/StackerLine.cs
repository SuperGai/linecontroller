﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LineControllerLib
{
    public enum EnumMachineStatus
    {
        Unknown = 0,
        NotRunning = 1,
        Running = 2,
        AblGroupPaused = 3,
        AblPaused = 4,
        NotReady = 5
    }

    public class ABL
    {
        public byte AblId { get; set;}
        public byte AblGroupId { get; set; }
        //public DateTime LastBundleMessage { get; set; }
        public EnumMachineStatus MachineStatus { get; set; }
        //public LineControlMain.enumStackerStatus Status { get; set; }

        public ABL(byte _AblId)
        {
            AblId = _AblId;
        }
    }

    public class StackerLine
    {
        private const byte NUM_ABL = 3;
        private const byte NUM_DBH = 2;
        
        public byte StackerLineId { get; set; }
        public byte DefaultStackerLinePrinter { get; set;}
        public byte BackupStackerLinePrinter { get; set; }
        public List<BundleData> production_list { get; set; }
        public List<BundleData> _newProductionQueue { get; set; }
        //public List<BundleData> FirstLastRouteBundles { get; set; }
        public List<BundleData> RouteBundleSortindexList { get; set; }

        public bool isStackerLineStoped { get; set; }

        public uint _lastBundleIDSent { get; set; }
        public DateTime _lastProductionDataSent { get; set; }
        public bool _newBundlesReceived { get; set; }
        public DateTime lastBundleCheck { get; set; }
        public object _lockNewProductionQueue { get; set; }
        public object _lockProductionList { get; set; }
        public List<ABL> ABLList { get; set; }
        public byte PosLineID { get; set; }
        //public int IsStackerLineGroupIndex { get; set;}
        public DateTime LastBundleMessage { get; set; }
        public TopSheetControlLib.TopSheetMain TopSheetMain { get; set; }
        //public TopSheetControlLib.PZFMain PZFMain { get; set; }
        //public LineControlMain.enumStackerLineStatus Status { get; set; }

        //public EnumMachineStatus PZFStatus { get; set; }
        //public EnumMachineStatus STBStatus { get; set; }
        //public EnumMachineStatus NextMachineStatus { get; set; }

        //public EnumMachineStatus[] ABLMachineStatus = new EnumMachineStatus[NUM_ABL];
        public EnumMachineStatus[] PrinterMachineStatus = new EnumMachineStatus[NUM_DBH];

        #region Constructor
        public StackerLine(byte _stackerLineID)
        {
            StackerLineId = _stackerLineID;
            //ResetFromStacker = false;
            try
            {
                int i;
                ABLList = new List<ABL>();
                for (i = 0; i < NUM_ABL; i++)
                {
                    ABLList.Add(new ABL((byte)(i + 1)));
                    ABLList[i].MachineStatus = EnumMachineStatus.Unknown;
                }

                for (i = 0; i < NUM_DBH; i++)
                {
                    PrinterMachineStatus[i] = EnumMachineStatus.Unknown;
                }
            }
            catch
            {
            }
        }
        #endregion

        //public TopSheetControlLib.TopSheetMain.TopSheetApplicatorStates TopSheetApplicatorState
        //{
        //    get
        //    {
        //        TopSheetControlLib.TopSheetMain.TopSheetApplicatorStates result =
        //            TopSheetControlLib.TopSheetMain.TopSheetApplicatorStates.ApplicatorUnknown;

        //        if (TopSheetMain != null)
        //        {
        //            result = TopSheetMain.TopSheetApplicatorState;
        //            //if (result == TopSheetControlLib.TopSheetMain.TopSheetApplicatorStates.ApplicatorReady
        //            //    && _status.RunningStandardAfterProduction == 1)
        //            //{
        //            //    result = TopSheetControlLib.TopSheetMain.TopSheetApplicatorStates.ApplicatorProductionFinished;
        //            //}
        //        }
        //        return result;
        //    }
        //}

        //public TopSheetControlLib.TopSheetMain.TopSheetPrinterStates TopSheetPrinterState
        //{
        //    get
        //    {
        //        TopSheetControlLib.TopSheetMain.TopSheetPrinterStates result =
        //            TopSheetControlLib.TopSheetMain.TopSheetPrinterStates.PrinterUnknown;

        //        if (TopSheetMain != null)
        //        {
        //            //result = TopSheetMain.TopSheetPrinterState;
        //        }
          
        //        return result;
        //    }
        //}

        public EnumMachineStatus STBMachineStatus
        {
            get
            {
                if (TopSheetMain.IcpConnectionState == TopSheetControlLib.TopSheetMain.IcpConnectionStates.Connected)
                {
                    if (TopSheetMain.STBStatus == 10)
                    {
                        return EnumMachineStatus.Running;
                    }
                    else
                    {
                        return EnumMachineStatus.NotRunning;
                    }
                }
                else
                {
                    return EnumMachineStatus.Unknown;
                }
            }
        }

        public EnumMachineStatus NextMachineStatus
        {
            get
            {
                if (TopSheetMain.IcpConnectionState == TopSheetControlLib.TopSheetMain.IcpConnectionStates.Connected)
                {
                    if (TopSheetMain.Status.cNextMachineReadyState == 1)
                    {
                        return EnumMachineStatus.Running;   //Ready
                    }
                    else
                    {
                        return EnumMachineStatus.NotRunning; //Not Ready
                    }
                }
                else
                {
                    return EnumMachineStatus.Unknown;   //Unknown
                }
            }
        }

        public EnumMachineStatus PZFMachineStatus
        {
            get
            {
                if (TopSheetMain.PZFIcpConnectionState == TopSheetControlLib.TopSheetMain.IcpConnectionStates.Connected)
                {
                    if (TopSheetMain.PZFRunning == 1)
                    {
                        return EnumMachineStatus.Running;
                    }
                    else
                    {
                        return EnumMachineStatus.NotRunning;
                    }
                }
                else
                {
                    return EnumMachineStatus.Unknown;
                }
            }
        }

        public Dictionary<string, string>[] PrinterMessage
        {
            get
            {
                //Dictionary<string, string>[] result = new Dictionary<string, string>[NUM_DBH];
                return TopSheetMain._printerStatus;
            }
        }

        public void UpdatePrinterMachineStatus()
        {
            string tempPrinterState;
            try
            {
                for (int i = 0; i < NUM_DBH; i++)
                {
                    if (TopSheetMain != null)
                    {
                        tempPrinterState = TopSheetMain._printerStatus[i]["StatusMessage"].ToUpper().Trim('.');
                        if (tempPrinterState.Contains("OFFLINE"))
                        {
                            PrinterMachineStatus[i] = EnumMachineStatus.Unknown;
                        }
                        else if (tempPrinterState.Contains("ONLINE") || tempPrinterState.Contains("PROCESSING"))
                        {
                            PrinterMachineStatus[i] = EnumMachineStatus.Running;
                        }
                        else
                        {
                            PrinterMachineStatus[i] = EnumMachineStatus.NotRunning;
                        }
                    }
                }
            }
            catch
            { 
            }
        }

        //public void UpdateABLMachineStatus(byte _AblIndex, EnumMachineStatus _curMachineStatus)
        //{
        //    if (_AblIndex >= 0 && _AblIndex < NUM_ABL)
        //    {
        //        ABLList[_AblIndex].MachineStatus = _curMachineStatus;
        //    }
        //}
    }


    
}
