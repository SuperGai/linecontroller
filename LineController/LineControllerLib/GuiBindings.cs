﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace LineControllerLib
{
    public class GuiBindings// : INotifyPropertyChanged
    {
        #region Variables

        private string _serverContent;
        
        private string _stackerContent;
        private string _stacker2Content;

        private string _applicatorContent;
        private string _printerContent;

        private string _copiesStackerContent;
        private string _copiesInContent;
        private string _copiesOutContent;
        private string _bundlesOutContent;
        private string _copiesPrHourContent;
        private string _productionLineContent;
        private string _gripperNumberContent;
        private string _modeContent;
        private string _simulationCycleTimeContent;
        private string _standardCopiesPrLayerContent;
        private string _standardLayerCountContent;
        private string _standardCopiesContent;
        private string _simulationCopiesPrHourContent;
        //private string _simulationStartStopContent;

        private string _settingsHeader;

        private bool _testPrinterEnabled;

        private Brush _serverBackground;
        private Brush _stackerBackground;
        private Brush _applicatorBackground;
        private Brush _printerBackground;
        private Brush _simulationStartStopBackground;

        private string[] _statusList;

        #endregion

        #region Properties

        public string ServerContent
        {
            get { return _serverContent; }
            set
            {
                _serverContent = value;
                OnPropertyChanged("ServerContent");
            }
        }

        public string StackerContent
        {
            get { return _stackerContent; }
            set
            {
                _stackerContent = value;
                OnPropertyChanged("StackerContent");
            }
        }

        public string Stacker2Content
        {
            get { return _stacker2Content; }
            set
            {
                _stacker2Content = value;
                OnPropertyChanged("Stacker2Content");
            }
        }

        public string ApplicatorContent
        {
            get { return _applicatorContent; }
            set
            {
                _applicatorContent = value;
                OnPropertyChanged("ApplicatorContent");
            }
        }

        public string PrinterContent
        {
            get { return _printerContent; }
            set
            {
                _printerContent = value;
                OnPropertyChanged("PrinterContent");
            }
        }

        public string CopiesStackerContent
        {
            get { return _copiesStackerContent; }
            set
            {
                _copiesStackerContent = value;
                OnPropertyChanged("CopiesStackerContent");
            }
        }

        public string CopiesInContent
        {
            get { return _copiesInContent; }
            set
            {
                _copiesInContent = value;
                OnPropertyChanged("CopiesInContent");
            }
        }

        public string CopiesOutContent
        {
            get { return _copiesOutContent; }
            set
            {
                _copiesOutContent = value;
                OnPropertyChanged("CopiesOutContent");
            }
        }

        public string BundlesOutContent
        {
            get { return _bundlesOutContent; }
            set
            {
                _bundlesOutContent = value;
                OnPropertyChanged("BundlesOutContent");
            }
        }

        public string CopiesPrHourContent
        {
            get { return _copiesPrHourContent; }
            set
            {
                _copiesPrHourContent = value;
                OnPropertyChanged("CopiesPrHourContent");
            }
        }

        public string ProductionLineContent
        {
            get { return _productionLineContent; }
            set
            {
                _productionLineContent = value;
                OnPropertyChanged("ProductionLineContent");
            }
        }

        public string GripperNumberContent
        {
            get { return _gripperNumberContent; }
            set
            {
                _gripperNumberContent = value;
                OnPropertyChanged("GripperNumberContent");
            }
        }

        public string ModeContent
        {
            get { return _modeContent; }
            set
            {
                _modeContent = value;
                OnPropertyChanged("ModeContent");
            }
        }

        public string SimulationCycleTimeContent
        {
            get { return _simulationCycleTimeContent; }
            set
            {
                _simulationCycleTimeContent = value;
                OnPropertyChanged("SimulationCycleTimeContent");
            }
        }

        public string StandardCopiesPrLayerContent
        {
            get { return _standardCopiesPrLayerContent; }
            set
            {
                _standardCopiesPrLayerContent = value;
                OnPropertyChanged("StandardCopiesPrLayerContent");
            }
        }

        public string StandardLayerCountContent
        {
            get { return _standardLayerCountContent; }
            set
            {
                _standardLayerCountContent = value;
                OnPropertyChanged("StandardLayerCountContent");
            }
        }

        public string StandardCopiesContent
        {
            get { return _standardCopiesContent; }
            set
            {
                _standardCopiesContent = value;
                OnPropertyChanged("StandardCopiesContent");
            }
        }

        public string SimulationCopiesPrHourContent
        {
            get { return _simulationCopiesPrHourContent; }
            set
            {
                _simulationCopiesPrHourContent = value;
                OnPropertyChanged("SimulationCopiesPrHourContent");
            }
        }

        //public string SimulationStartStopContent
        //{
        //    get { return _simulationStartStopContent; }
        //    set
        //    {
        //        _simulationStartStopContent = value;
        //        OnPropertyChanged("SimulationStartStopContent");
        //    }
        //}


        public string SettingsHeader
        {
            get { return _settingsHeader; }
            set
            {
                _settingsHeader = value;
                OnPropertyChanged("SettingsHeader");
            }
        }

        public bool TestPrinterEnabled
        {
            get { return _testPrinterEnabled; }
            set
            {
                _testPrinterEnabled = value;
                OnPropertyChanged("TestPrinterEnabled");
            }
        }

        public Brush ServerBackground
        {
            get
            {
                return _serverBackground;
            }
            set
            {
                _serverBackground = value;
                OnPropertyChanged("ServerBackground");
            }
        }

        public Brush StackerBackground
        {
            get
            {
                return _stackerBackground;
            }
            set
            {
                _stackerBackground = value;
                OnPropertyChanged("StackerBackground");
            }
        }

        public Brush ApplicatorBackground
        {
            get
            {
                return _applicatorBackground;
            }
            set
            {
                _applicatorBackground = value;
                OnPropertyChanged("ApplicatorBackground");
            }
        }

        public Brush PrinterBackground
        {
            get
            {
                return _printerBackground;
            }
            set
            {
                _printerBackground = value;
                OnPropertyChanged("PrinterBackground");
            }
        }

        public Brush SimulationStartStopBackground
        {
            get
            {
                return _simulationStartStopBackground;
            }
            set
            {
                _simulationStartStopBackground = value;
                OnPropertyChanged("SimulationStartStopBackground");
            }
        }

        public string[] StatusList
        {
            get
            {
                return _statusList;
            }
            set
            {
                _statusList = value;
                OnPropertyChanged("StatusList");
            }
        }

        #endregion

        #region INotifyPropertyChanged Members

        /*
        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;

        public virtual void OnPropertyChanged(string propertyName)
        {
            try
            {
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
                }

            }
            catch { }
        }
        */

        public void OnPropertyChanged(string propertyName)
        {
        }

        #endregion // INotifyPropertyChanged Members

    }
}
