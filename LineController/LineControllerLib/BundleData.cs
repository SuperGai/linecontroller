﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LineControllerLib
{
    public class BundleData
    {
        public enum LayerFlagTypes : byte
        {
            LAYERFLAG_EMPTY = 0,
            LAYERFLAG_START = 1,
            LAYERFLAG_PAUSE = 2,
            LAYERFLAG_RESUME = 4,
            LAYERFLAG_END = 8,
            LAYERFLAG_ROTATE = 16,
            LAYERFLAG_ENDOFBUNDLE = 32,
            LAYERFLAG_EJECTBACKSIDE = 64,
            LAYERFLAG_ABORT = 128
        }

        public uint BundleId { get; set; }
        //public uint EditionTableId { get; set; }
        public ushort NumCopies { get; set; }
        public ushort NumCopiesCounted { get; set; }
        public byte StackerLineId { get; set; }
        public ushort CopiesInLayer { get; set; }
        public ushort MaxCopiesInLayer { get; set; }
        public ushort MinCopiesInLayer { get; set; }
        public ushort MinCopiesInBundle { get; set; }
        public byte NumLayersInBundle { get; set; }
        public byte NextLayerIndex { get; set; }
        public byte NextAddressIndex { get; set; }
        //For standard bundle information
        public ushort StandardBundle { get; set; }
        public byte StandardNumBatch { get; set; }
        public byte StandardCopiesInLayer { get; set; }
        public bool IsNoTopsheet { get; set; }
        public bool IsSTDBundle { get; set; }
        public ushort RouteSortIndex { get; set;}
        public ushort BundleSortIndex { get; set; }

        public byte RouteModeId { get; set; }

        public TopSheetControlLib.TopSheetMain.EnumBundleInRoute BundleSeqInRoute { get; set; }
        public LineControlMain.EnumBundleStatus Status { get; set; }
        public List<LayerData> LayerCommandList { get; set; }
        public BundleData()
        {
            LayerCommandList = new List<LayerData>();
        }

        //Valid Address put continuously to the last of bundle
        public bool CreateLayerList()
        {
            bool result = false;
            NumLayersInBundle = 0;
            //List<string> testList = new List<string>();
            try
            {
                //MinCopiesInLayer = 20;
                //MaxCopiesInLayer = 59;
                //MinCopiesInBundle = 8;
                //for (NumCopies = 0; NumCopies < 200; NumCopies++)
                {
                    List<LayerData> newLayerList = null;
                    LayerCommandList = null;
                    NextLayerIndex = 0;

                    try
                    {
                        if (NumCopies > 0 && CopiesInLayer > 0)
                        {
                            if (MinCopiesInLayer == 0)
                            {
                                MinCopiesInLayer = CopiesInLayer;
                            }

                            if (MinCopiesInBundle == 0)
                            {
                                MinCopiesInBundle = CopiesInLayer;
                            }

                            if (MaxCopiesInLayer == 0)
                            {
                                MaxCopiesInLayer = (ushort)(CopiesInLayer * 2 - 1);
                            }

                            if (NumCopies > 0 && (NumCopies >= MinCopiesInLayer || 
                                NumCopies >= MinCopiesInBundle) &&
                                CopiesInLayer > 0 && CopiesInLayer >= MinCopiesInLayer &&
                                CopiesInLayer <= MaxCopiesInLayer)
                            {
                                result = true;
                                List<int> copyCountList = new List<int>();
                                int copiesRemaining = NumCopies;
                                int copiesShare = 0;

                                int MyMaxCopyInLayer = MaxCopiesInLayer;
                                int numLayers = 0;

                                if (MyMaxCopyInLayer < 2 * MinCopiesInLayer)
                                {
                                   //In mosjeon, MaxCopyInLayer is around double MinCpoiesInLayer
                                   MyMaxCopyInLayer = 2 * MinCopiesInLayer;
                                }


                                if (NumCopies <= MyMaxCopyInLayer)
                                {
                                    if(MyMaxCopyInLayer > 11)
                                    {
                                        if (NumCopies < 11) //MyMaxCopyInLayer - 10)
                                        {
                                            copyCountList.Add(NumCopies);
                                            copiesRemaining = 0;
                                        }
                                        else
                                        {
                                            copiesShare = (NumCopies + 1) / 2;
                                            copyCountList.Add(copiesShare);
                                            copyCountList.Add(NumCopies - copiesShare);
                                            copiesRemaining = 0;
                                        }
                                    } 
                                    else
                                    {
                                        copyCountList.Add(NumCopies);
                                        copiesRemaining = 0;
                                    }
                                }
                                else {
                                    numLayers = NumCopies / CopiesInLayer;

                                    copiesRemaining = NumCopies - CopiesInLayer * (numLayers-1);

                                    for (int i = 0; i < numLayers-1; i++)
                                    {
                                        copyCountList.Add(CopiesInLayer);
                                    }

                                    if(copiesRemaining <= MaxCopiesInLayer)
                                    {
                                        copyCountList.Add(copiesRemaining);
                                    } else 
                                    {
                                        copiesShare = (copiesRemaining+1) / 2;
                                        copiesRemaining = copiesRemaining - copiesShare;
                                        copyCountList.Add(copiesShare);
                                        copyCountList.Add(copiesRemaining);
                                    }//end of else: CopiesRemaining > 0
                                }// end of else: NumCopies > MyMaxCopyInLayer                                                              

                                int copyCheck = 0;
                                for (int i = 0; i < copyCountList.Count; i++)
                                {
                                    copyCheck += copyCountList[i];
                                }

                                if (copyCheck != NumCopies)
                                {
                                    Logging.AddToLog(LogLevels.Warning, "Layers setup not correct - fixing!");
                                    copyCountList.Clear();
                                    copiesRemaining = NumCopies; //CopiesInLayer;

                                    numLayers = NumCopies / CopiesInLayer;
                                    for (int i = 0; i < numLayers; i++)
                                    {
                                        copyCountList.Add(CopiesInLayer);
                                        copiesRemaining -= CopiesInLayer;
                                    }

                                    if (copiesRemaining > 0)
                                    {
                                        copyCountList[numLayers - 1] += copiesRemaining;
                                    }
                                }

                                numLayers = copyCountList.Count;

                                newLayerList = new List<LayerData>();
                                for (int i = 0; i < copyCountList.Count; i++)
                                {
                                    //byte startFlags = (byte)LayerFlagTypes.LAYERFLAG_START;
                                    //byte endFlags = (byte)LayerFlagTypes.LAYERFLAG_END;
                                    byte layerNumber = (byte)(i + 1);
                                    byte numCopiesInLayer = (byte)copyCountList[i];
                                    byte Flags = 0;

                                    if (i == 0)
                                    {
                                        Flags |= (byte)LayerFlagTypes.LAYERFLAG_START;
                                    }

                                    if (i == copyCountList.Count - 1)
                                    {
                                        Flags |= (byte)LayerFlagTypes.LAYERFLAG_ENDOFBUNDLE;
                                    }

                                    newLayerList.Add(new LayerData(StackerLineId, 
                                        BundleId, (byte)(i + 1), Flags,
                                        numCopiesInLayer, NumCopies, (byte)numLayers));

                                }
                                NumLayersInBundle = (byte)numLayers;
                            }
                           
                        }
                    }
                    catch (Exception ex)
                    {
                        Logging.AddToLog(LogLevels.Error, "Invalid bundle " + BundleId.ToString() + ": " + ex.ToString());
                        newLayerList = null;
                    }

                    if (newLayerList != null && newLayerList.Count > 0)
                    {
                        LayerCommandList = newLayerList;
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }

                    /*
                    string sTest = string.Format("NumCopies={0}, Result={1}, NumLayers={2}," +
                        "{3}{4}{5}{6}{7}{8}{9}"
                        , NumCopies
                        , result ? 1 : 0
                        , newLayerList != null ? (newLayerList.Count / 2).ToString() : ""
                        , newLayerList != null && newLayerList.Count > 0 ? ", L1=" + newLayerList[0].NumCopiesInLayer.ToString() : ""
                        , newLayerList != null && newLayerList.Count > 2 ? ", L2=" + newLayerList[2].NumCopiesInLayer.ToString() : ""
                        , newLayerList != null && newLayerList.Count > 4 ? ", L3=" + newLayerList[4].NumCopiesInLayer.ToString() : ""
                        , newLayerList != null && newLayerList.Count > 6 ? ", L4=" + newLayerList[6].NumCopiesInLayer.ToString() : ""
                        , newLayerList != null && newLayerList.Count > 8 ? ", L5=" + newLayerList[8].NumCopiesInLayer.ToString() : ""
                        , newLayerList != null && newLayerList.Count > 10 ? ", L6=" + newLayerList[10].NumCopiesInLayer.ToString() : ""
                        , newLayerList != null && newLayerList.Count > 12 ? ", L7=" + newLayerList[12].NumCopiesInLayer.ToString() : ""
                        );
                    testList.Add(sTest);
                    */
                }
            }
            catch// (Exception ex)
            {
            }

            return result;
        }

        public LayerData GetNextLayerCommand(out bool IsLast)
        {
            LayerData result = null;

            if (NextLayerIndex < LayerCommandList.Count)
            {
                result = LayerCommandList[NextLayerIndex];
                ++NextLayerIndex;
            }

            IsLast = NextLayerIndex >= LayerCommandList.Count;

            return result;
        }
    }

}
