﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LineControllerLib
{
    public class HaukSetupStructure
    {

        public uint DPulseEntranceToIntercept;
        public uint DPulseMarginRelease;
        public uint DPulseReleaseToEntrance;
        public ushort ReleaseDelay;
        public ushort ReleaseDistance;
        public ushort ReleaseStopDelay;
        public byte ReleaseUpDownFlag;
        public ushort StackerEjectMSDelay;
        public ushort StackerEjectMSTimeout;

        public ushort ReleaseDelayMedium;
        public ushort ReleaseDelayHigh;
        public ushort ReleaseStopDelayMedium;
        public ushort ReleaseStopDelayHigh;

        public String GetString()
        {
            return DPulseEntranceToIntercept + " " + DPulseMarginRelease + " " + DPulseReleaseToEntrance + " " + ReleaseDelay +
                " " + ReleaseDistance + " " + ReleaseStopDelay + " " + ReleaseUpDownFlag + " " + StackerEjectMSDelay + " " + StackerEjectMSTimeout;
        }
    }
}

