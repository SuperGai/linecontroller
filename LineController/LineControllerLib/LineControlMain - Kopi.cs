﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

namespace LineControllerLib
{
    public class LineControlMain
    {
        #region Enums

        public enum enumLanguages
        {
            English = 1,
            French = 2,
            German = 3,
            Norwegian = 4
        }

        public enum enumLineControlStates
        {
            NotRunning = 1,
            Running = 2
        }

        //public enum enumTopSheetControlStates
        //{
        //    NotRunning = 1,
        //    Running = 2
        //}

        public enum enumIcpConnectionStates
        {
            NotRunning = 1,
            Disconnected = 2,
            Connecting = 3,
            Connected = 4
        }

        public enum enumDbConnectionStates
        {
            DbUnknown,
            DbDisconnected,
            DbConnecting,
            DbConnected
        }

        public enum enumIcpTelegramTypes
        {
            RequestStatus,
            SendSetup,
            //SendBundleData,
            SendLayerCommand,
            SendAddressList,
            StartProduction,
            StopProduction,
            Reset,
            ClearCounters,
            StartStopSimulation,
            SendImajeSetup,
            TestImajePrint
        }

        public enum enumDataBaseCommands
        {
            dbCommNone = 0,
            dbCommSetBundleStatus = 1,
            //dbCommGetProductionParameters = 2,
            dbCommGetBundleList = 3,
            dbCommSetLineStatus = 4,
            dbCommSetStackerStatus = 5,
            //dbCommGetProductionStackers = 6,
            //dbCommGetStandardStackers = 7,
            //dbCommSetLineControlVersion = 8,
            //dbCommGetBundleReproduceList = 9,
            //dbCommSetReproduceReceived = 10,
            dbCommStackerPause = 11,
            dbCommStackerResume = 12,
            //dbCommSetSmallBundlesProduced = 13,
            //dbCommSetAllBundlesProduced = 14,
            //dbCommSetProductionListFinished = 15,
            //dbCommGetAddressList = 16,
            //dbCommGetReproduceAddressList = 17,
            //dbCommGetLineNumber = 51,
            dbCommGetNextCommand = 52,
            //dbCommSetAddressStatus = 18,
            dbCommGetSetup = 19,
            dbCommSetTopSheetApplicatorStatus = 20,
            dbCommSetTopSheetPrinterStatus = 21,
            dbCommSetNextMachineStatus = 22,
            dbCommSetInkStatus = 23
        }

        public enum enumBundleStatus
        {
            bundleStatusReady = 0,
            bundleStatusReservedGui = 1,
            bundleStatusReservedLCC = 2,
            bundleStatusProducing = 3,
            bundleStatusInStacker = 4,
            bundleStatusAfterStacker = 5,
            bundleStatusInkFailed = 90,
            bundleStatusStackerFailed = 91,
            bundleStatusBundleError = 99
        }

        public enum enumLineStatus
        {
            elineUnknown = 0,
            elineNotAvailable = 1,
            elineReady = 2,
            elineProducing = 3,
            elineConnecting = 4,
            elineError = 91
        };

        public enum enumFoldingType
        {
            //NotSelected = 1,
            Tabloid = 2,
            //Twofolder = 3,
            //Quarterfolder = 4
        }


        //public enum enumPrintServerCommand
        //{
        //    tcpCommandBundleProduced = 1,
        //    tcpCommandBundleReleasedToStacker = 3
        //}

        public enum enumMessageStatusTypes : byte
        {

            UNKNOWN = 0,
            COUNTFIRST = 1,
            COUNTLAST = 2,
            //ADDRESSFIRST = 3,
            //ADDRESSLAST = 4,
            RELEASEFIRST = 5,
            RELEASELAST = 6,
            INFEEDFIRST = 7,
            INFEEDLAST = 8,
            ENTRANCEFIRST = 9,
            ENTRANCELAST = 10,
            ATINTERCEPTFIRST = 11,
            ATINTERCEPTLAST = 12,
            BUNDLEINTABLE = 13,
            BUNDLEEJECTED = 14,
            PRODUCTIONFINISHED = 20,
            INCOMPLETETRANSFER = 50,
            //LOSTBEFOREINK = 51,
            ERRORINK1 = 52,
            //ERRORINK2 = 53,
            //LOSTBEFORETOSTACKER = 54,
            LOSTBEFORESTACKER = 55,
            LOSTBEFOREEJECTED = 56,
            STACKERABORT = 57,
            PRODUCTIONQUEUEFULL = 58,
            LOSTINTERCEPTWINDOW = 59,
            LAYERCONTROLABORT = 60,
            LCCABORT = 61,
            STACKERCYCLETIMEERROR = 62,
            EJECTARMNOTINPOSITION = 63,
            RELEACECYLINDERNOTINPOSITION = 64,
            SEPERATINGCYLINDERNOTINPOSITION = 65,
            RELEACEORSEPERATINGPOSITIONINCORRECT = 66,
            ARMBACKTIMEOUT = 67,
            ARMFRONTTIMEOUT = 68,
            ARMUPTIMEOUT = 69,
            SEPERATINGCYLINDERRETRACTTIMEOUT = 70,
            SEPERATINGCYLINDEREXTENDTIMEOUT = 71,
            RELEASECYLINDERRETRACTTIMEOUT = 72,
            RELEASECYLINDEREXTENDTIMEOUT = 73,
            TURNTABLELEFTTIMEOUT = 74,
            TURNTABLERIGHTTIMEOUT = 75,
            TURNTABLEPOSITIONUNKNOWN = 76,
            LAYERDATAMISSING = 77,
            RELEACECYLNOTINEXTEND = 78,
            RELEACECYLNOTRETRACTED = 79,
            SEPERATINGCYLNOTINEXTEND = 80,
            SEPERATINGCYLNOTINRETRACTED = 81,
            PAPERJAMPRESSING = 82,
            PAPERJAMCOUNTINGHEAD = 83,
            CONVERTERNOTREADY = 84,
            ARMDOWNTIMEOUT = 85,
            INTERCEPTTIMEOUT = 86,
            RESET = 87,
            CYCLETABLEPREPARENOTREADY = 88,
            CYCLETABLENOTREADY = 89
        }

        public enum enumEjectDirectionTypes : byte
        {
            Unknown = 0,
            Front = 1,
            Back = 2
        }

        public enum enumGripperControlStatus : byte
        {
            NotReady = 0,
            Ready = 1
        }

        public enum enumInkStatus : byte
        {
            Unknown = 0,
            NotAvailable = 1,
            Ready = 2,
            Producing = 3,
            Connecting = 4,
            Paused = 5,
            WaitingForData = 7,
            NotReady = 91
        }

        public enum enumStackerStatus : byte
        {
            Unknown = 0,
            NotAvailable = 1,
            Ready = 2,
            Producing = 3,
            Connecting = 4,
            Paused = 5,
            WaitingForData = 7,
            NotReady = 91
        }

        public enum enumStackerMachineStatus : byte
        {
            NotReady = 0,
            Ready = 1
        }

        public enum enumLayerPositions : byte
        {
            UNKNOWN = 0,
            COUNTER = 1,
            RELEASE = 2,
            INFEED = 3,
            ENTRANCE = 4,
            INTERCEPT = 5,
            TABLE = 6
        }

        public enum enumModes : byte
        {
            STANDARD = 2,
            PROGRAMMED = 7,
        }

        public enum enumGripperLines : byte
        {
            Online,
            Unwinding
        }

        #endregion

        #region Constants

        private const byte SOH = 1;
        private const byte EOT = 4;
        private const byte NUMSLOTS = 4;
        private const byte BUFSIZEGRIPPERMESSAGE = 10;
        private const byte MAXGRIPPERLINES = 1;
        private const byte MAXSTACKERS = 2;
        private const byte MAXBUNDLEMESSAGES = 50;

        #endregion

        #region Structs/ Setup classes

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct BundleMessageStruct
        {
            public byte StackerNumber;
            public ushort BundleId;
            public enumMessageStatusTypes Status;
            public byte ExtraInfo;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct StackerStatusStruct
        {
            public byte State; //0: Not Ready; 1: Ready: Infeedbelt is running
            public byte IsActive;
            public byte IsLayerPlanFinish;

            public byte StackerCopyCounterState;
            public byte StackerInterceptState;

            public byte EnableReleaseState;
            public byte GripperReleaseState;
            public byte StackerEntranceState;
            public byte EjectStackerState;

            public ulong TaktReleasePulse;

            public ulong InfeedbeltPulse;

            //public ushort EntrancePulses; //pulses between copies at stacker entrance

            public ushort LastReceivedBundleID;
            public byte LastReceivedLayerID; // same as layerNumber in linecontroller

            public byte NumRequestLayer; //number of layers request from Line controller
            public byte NumRequestAddress;
            //public byte CurLayerCommandIndex;

            /*
            public byte NumLayerCommands;
            public byte NumLayerCommandsBuffer;
            public byte NumBundlesBuffer;
            */

            /*
	        public byte IsLayerPause;
	        public uint LastEjectTick;
	        public uint BeforeLastEjectTick;	
            */

            public uint CountersCopiesIn;
            public uint CountersCopiesOut;
            public uint CountersBundlesOut;

            public short StackerPulsesPrSecond;
            public ulong LastEntrancePulse;

            public ushort CopiesCountedInfeed;
            public byte LayersCountedInfeed;
            public ushort CopiesCountedEntrance;
            public byte LayersCountedEntrance;

            //public ushort BundleIdNext;
            //public ushort BundleIdEntrance;
            //public ushort BundleIdTable;
            //public ushort NextBundleIdToExit;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct StatusStruct
        {
            public byte StartOfHeader;
            public byte Id;
            public byte Command;
            public ushort TelegramLength;
            //public enumGripperControlStatus GCStatus;

            public byte SimulateState;
            public byte ProductionStarted;
            public byte RunningStandardAfterProduction;
            public byte GripperQueueStarted;

            public uint TaktCountGripper; //0 - forever
            public uint CopyCountGripper;	 //0 - forever
            //public byte FirstGripperNumber;
            //public byte NumGrippers; //0-255 recycle
            //public byte GripperNumberCounter; // 0 - 255
            /*
            public uint TickCountNow;
            public uint TickCountPreviousGripper;
            public uint TickCountBeforePreviousGripper;
            public ushort GripperTime;
            */

            public ushort GripperPulses;
            public ulong LastTaktGripperPulse;
            public ulong GripperPulse;
            public ushort GripperPulsesPrSecond;
            public byte ActiveState; //For whole ICP state
            public byte GripperTaktState;
            public byte GripperCopyCounterState;
            public byte MyCopyCounterState;
            public ushort MyCopyTime;

            public byte NextStandardBundleId;
            public byte CurrentStandardBundleId;

            //public byte LayerIndexEjecting;
            public byte InkJetControlState;
            public byte StartInkState;
            //public byte InkQueueState;
            //public byte InkQueueNumFree;
            //public byte NextMachineNotReady;//NumBundlesNextMachineStopped;

            /*	        public ulong StackerPulse;
                        public ushort StackerPulsesPrSecond;
                        public ulong LastEntrancePulse;
                        public ushort EntrancePulses;//pulses between copies at stacker entrance
                        public ushort MSecPreviousIntercept;//time spent intercepting
                        public ushort MSecIntercept;//time spent intercepting
                        public byte NumLayerCommands;
                        public byte NumLayerCommandsBuffe;
                        public byte NumBundlesBuffer;
                        public byte IsLayerPause;
                        public uint LastEjectTick;
                        public uint BeforeLastEjectTick;*/
            //public byte NumBundleCommands;

            /*  
              public byte DoorTrigged;
              public byte JamPressingTrigged;
              public byte JamCountingTrigged;
              */

            //public uint CountersCopiesIn;
            //public uint CountersCopiesOut;
            //public uint CountersBundlesOut;

            //public byte NextStandardBundleId;
            //public byte CurrentStandardBundleId;
            //public ushort CopiesCountedInfeed;
            //public byte LayersCountedInfeed;
            //public ushort CopiesCountedEntrance;
            //public byte LayersCountedEntrance;
            //public ushort CopyCountInForks;
            //public byte LayerCountInTable;
            //public ushort CopyCountInTable;
            //public uint TaktCountGripper;
            //public uint CopyCountGripper;
            //public byte LayerIndexIntercept;
            //public byte LayerIndexForks;
            //public byte LayerIndexPrepareTable;
            //public byte LayerIndexTable;
            //public byte LayerIndexEjecting;

            //public ushort BundleIdNext;
            //public ushort BundleIdEntrance;
            //public ushort BundleIdTable;

            //public byte LastEjectDirection;

            //public byte FirstGripperNumber;
            //public byte NumGrippers;
            //[MarshalAs(UnmanagedType.ByValArray, SizeConst = BUFSIZEGRIPPERMESSAGE)]
            //public byte[] GripperMessage;

            public byte BundleMessageCount;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = MAXBUNDLEMESSAGES)]
            public BundleMessageStruct[] BundleMessage;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = MAXSTACKERS)]
            public StackerStatusStruct[] Stackers;

            public byte EndOfText;
        }

        //[StructLayout(LayoutKind.Sequential, Pack = 1)]
        //public struct GripperLineSetupStruct
        public class GripperLineSetupStruct
        {
            public byte TaktDISlot = 255;
            public byte TaktDISignal = 255;
            public byte TaktDIOnHigh = 255;

            public byte CounterCopyDISlot = 255;
            public byte CounterCopyDISignal = 255;
            public byte CounterCopyDIOnHigh = 255;

            //public byte ReadyForReleaseDOSlot = 255;
            //public byte ReadyForReleaseDOSignal = 255;

            public byte GripperPercentDelayBeforeDetectCopy = 0;

            public byte PulseChannelGripper = 255;

            public ushort GripperPulsesPrMeter = 240;
            //public uint MMReleaseToInfeed = 0;//distance in millimeters
            //public ushort MarginMMReleaseToInfeed;//margin in millimeters
            public ushort MMCopyDistance = 100;//distance in millimeters between copies in gripper

            //public byte InvertMessage = 0;
            //public byte InvertHorizontal = 0;
            //public byte InvertVertical = 0;
            //public ushort TacoMeterDivision = 8;
            //public byte InvertUpDown = 0;
        }

        //[StructLayout(LayoutKind.Sequential, Pack = 1)]
        //public struct StackerSetupStruct
        public class StackerSetupStruct
        {

            	public byte IsActive;
	            //High Speed Input
                public byte PulseChannelTaktRelease;
                public byte PulseChannelInfeedbelt;
	
	            // 
                public byte EntranceDISlot;
                public byte EntranceDISignal;
                public byte EntranceDIOnHigh;

	            //	char EntrancePulseChannel;	
	
	            //Input
                public byte EjectBundleFinishDISlot;
                public byte EjectBundleFinishDISignal;
                public byte EjectBundleFinishOnHigh;
	
	            //Output
                public byte EnableReleaseDOSlot;
                public byte EnableReleaseDOSignal;

                public byte ReleaseDOSlot;
                public byte ReleaseDOSignal;

                public byte BlockCounterDOSlot;
                public byte BlockCounterDOSignal;

                public byte MakeLayerDOSlot;
                public byte MakeLayerDOSignal;

                public byte EjectBundleDOSlot;
                public byte EjectBundleDOSignal;



            //public byte StackerNumber;

            //public byte ReadyDISlot = 255;
            //public byte ReadyDISignal = 255;
            //public byte ReadyDIOnHigh = 255;

            //public byte ExitDISlot = 255;
            //public byte ExitDISignal = 255;
            //public byte ExitDIOnHigh = 255;

            //public byte ReleaseDOSlot = 255;
            //public byte ReleaseDOSignal = 255;

            //signals from master
            //public byte ReleaseFirstDOSlot = 255;
            //public byte ReleaseFirstDOSignal = 255;
            //public byte ReleaseLastDOSlot = 255;
            //public byte ReleaseLastDOSignal = 255;

            //public byte CounterDistanceInt; //50.54 = 50
            //public byte CounterDistanceHundreds; //50.54 = 54

            //public ushort StackerPulsesPrMeter = 0;
            //public byte PulseChannelStacker = 255;
            //public ushort CycleTimeMSecStacker = 0;
            //public uint MMInfeedToEntrance = 0;//distance in millimeters
            //public ushort MarginMMInfeedToEntrance = 0;//margin in millimeters


	            //Parameters
	            public ushort ReleaseDelay; //number of gripper pulses
	            public ushort StackerEjectMSDelay;
	            public ushort CycleTimeMSecStacker;
	            public ushort StandardCopiesInLayer;
	            public byte StandardLayersInBundle;
	            public byte StandardEjectArmBack;
	
	            public uint MMReleaseToInfeed;	//distance in millimeters	
	            public uint MMInfeedToEntrance;	//distance in millimeters
	            public ushort MMEntranceToIntercept;//distance in millimeters
	            public ushort MarginMMEntranceToIntercept;//margin in millimeters
	            public ushort InfeedBufferSize; 		//How many copies can be stored on the infeed belt of stacker 
	
	            public ushort MarginMMInfeedToEntrance;	//margin in millimeters
	            public ushort StackerPulsesPrMeter;

	            public uint DPulseReleaseToEntrance; //Pulse distance from Release/Infeed to Entrance counter
                public uint DPulseEntranceToIntercept; //Pulse distance from Entrance counter to Intercept
        }

        //[StructLayout(LayoutKind.Sequential, Pack = 1)]
        //public struct SetupStruct
        public class SetupStruct
        {

            public byte StackerNumber;
            
            public byte ProductionLineNumber = 0;
            public enumModes Mode = enumModes.STANDARD;
            public byte CurrentGLNumber = 1;//gripper line (1-3)
            //public byte MaxBundlesNextMachineStopped = 1;
            //public short DelayMsBeforeReleaseBundleAfterStop;

            public byte IsPaused = 0;
            public byte IsSimulatingCopiesInGripper = 0;

            public byte IsInkEnable = 1;

            public byte IsRealcomEnable;

            public byte EjectBundleDOSlot;
            public byte EjectBundleDOSignal;
            public ushort EjectBundleDOMSSignal;
            public ushort EjectBundleMSTimeout;

            public byte EjectFinishDISlot;
            public byte EjectFinishDISignal;
            public byte EjectFinishDIOnHigh;
            public ushort ResetMMBeforeAction; //0 = pulses from laser counter distance

            public byte InkReadyDISlot;
            public byte InkReadyDISignal;
            public byte InkReadyOnHigh;

            public byte InkFinishDISlot;
            public byte InkFinishDISignal;
            public byte InkFinishOnHigh;

            public ushort InkFinishMSTimeout;
            public ushort ReadComMSTimeout;

            public byte StartInkDOSlot;
            public byte StartInkDOSignal;

            public byte EnableInkDOSlot;
            public byte EnableInkDOSignal;

            //public ushort StandardCopiesInLayer = 30;
            //public byte StandardLayersInBundle = 2;
            //public byte StandardEjectArmBack = 0;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = MAXGRIPPERLINES)]
            public GripperLineSetupStruct GripperLines;
            //public GripperLineSetupStruct[] GripperLines;

            public byte NumStackers;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = MAXSTACKERS)]
            public StackerSetupStruct[] Stackers;

            #region Stacker internal control settings - commented
            //public byte ActiveDISlot = 255;
            //public byte ActiveDISignal = 255;
            //public byte ActiveDIOnHigh = 255;

            //public byte StackerReadyDOSlot = 255;
            //public byte StackerReadyDOSignal = 255;

            //public byte ReleasePusherDOSlot = 255;
            //public byte ReleasePusherDOSignal = 255;

            //public byte ButtonResetDISlot = 255;
            //public byte ButtonResetDISignal = 255;
            //public byte ButtonResetDIOnHigh = 255;

            //public byte JamPressingDISlot = 255;
            //public byte JamPressingDISignal = 255;
            //public byte JamPressingDIOnHigh = 255;
            //public byte JamCountingHeadDISlot = 255;
            //public byte JamCountingHeadDISignal = 255;
            //public byte JamCountingHeadDIOnHigh = 255;
            //public byte ConverterReadyDISlot = 255;
            //public byte ConverterReadyDISignal = 255;
            //public byte ConverterReadyDIOnHigh = 255;

            //public byte MasterReleaseFirstDISlot = 255;
            //public byte MasterReleaseFirstDISignal = 255;
            //public byte MasterReleaseFirstDIOnHigh = 255;

            //public byte MasterReleaseLastDISlot = 255;
            //public byte MasterReleaseLastDISignal = 255;
            //public byte MasterReleaseLastDIOnHigh = 255;

            //public byte EntranceDISlot = 255;
            //public byte EntranceDISignal = 255;
            //public byte EntranceDIOnHigh = 255;
            //public byte EntrancePulseChannel = 255;

            //public byte SeperatingRetractedDISlot = 255;
            //public byte SeperatingRetractedDISignal = 255;
            //public byte SeperatingRetractedDIOnHigh = 255;

            //public byte SeperatingExtendedDISlot = 255;
            //public byte SeperatingExtendedDISignal = 255;
            //public byte SeperatingExtendedDIOnHigh = 255;

            //public byte ReleaseRetractedDISlot = 255;
            //public byte ReleaseRetractedDISignal = 255;
            //public byte ReleaseRetractedDIOnHigh = 255;

            //public byte ReleaseExtendedDISlot = 255;
            //public byte ReleaseExtendedDISignal = 255;
            //public byte ReleaseExtendedDIOnHigh = 255;

            //public byte InterceptDISlot = 255;
            //public byte InterceptDISignal = 255;
            //public byte InterceptDIOnHigh = 255;
            //public byte InterceptLockDOSlot = 255;
            //public byte InterceptLockDOSignal = 255;

            //public byte SeperatingRetractDOSlot = 255;
            //public byte SeperatingRetractDOSignal = 255;
            //public short SeperatingRetractMsTimeout = 0;

            //public byte SeperatingExtendDOSlot = 255;
            //public byte SeperatingExtendDOSignal = 255;
            //public short SeperatingExtendMsTimeout = 0;

            //public short DelayMsBeforeForksOpen;
            //public byte OpenForkDOSlot = 255;
            //public byte OpenForkDOSignal = 255;
            //public short DelayMsBeforeForksClose = 0;

            //public short DelayMsBeforeInterceptRelease = 0;

            //public byte ReleaseRetractDOSlot = 255;
            //public byte ReleaseRetractDOSignal = 255;
            //public short ReleaseRetractMsTimeout = 0;

            //public byte ReleaseExtendDOSlot = 255;
            //public byte ReleaseExtendDOSignal = 255;
            //public short ReleaseExtendMsTimeout = 0;

            //public short DelayMsBeforeTableEject = 0;
            //public short DelayMsBeforeTableRotate = 0;
            //public short DelayMsBeforeArmUp = 0;
            //public short DelayMsBeforeArmFront = 0;

            //public byte EjectArmFrontDISlot = 255;
            //public byte EjectArmFrontDISignal = 255;
            //public byte EjectArmFrontDIOnHigh = 255;
            //public byte EjectArmBackDISlot = 255;
            //public byte EjectArmBackDISignal = 255;
            //public byte EjectArmBackDIOnHigh = 255;
            //public byte TableLeftDISlot = 255;
            //public byte TableLeftDISignal = 255;
            //public byte TableLeftDIOnHigh = 255;
            //public byte TableRightDISlot = 255;
            //public byte TableRightDISignal = 255;
            //public byte TableRightDIOnHigh = 255;
            //public byte EjectArmUpDISlot = 255;
            //public byte EjectArmUpDISignal = 255;
            //public byte EjectArmUpDIOnHigh = 255;
            //public byte EjectArmDownDISlot = 255;
            //public byte EjectArmDownDISignal = 255;
            //public byte EjectArmDownDIOnHigh = 255;

            //public byte EjectArmFrontDOSlot = 255;
            //public byte EjectArmFrontDOSignal = 255;
            //public short EjectArmFrontDOMSSignal = 255;
            //public short EjectArmFrontMSTimeout;
            //public byte EjectArmBackDOSlot = 255;
            //public byte EjectArmBackDOSignal = 255;
            //public short EjectArmBackDOMSSignal = 255;
            //public short EjectArmBackMSTimeout = 0;
            //public byte EjectArmUpDOSlot = 255;
            //public byte EjectArmUpDOSignal = 255;
            //public short EjectArmUpDOMSSignal = 255;
            //public short EjectArmUpMSTimeout = 0;
            //public short EjectArmDownMSTimeout = 0;

            //public byte TableLeftDOSlot = 255;
            //public byte TableLeftDOSignal = 255;
            //public short TableLeftDOMSTimeout = 0;
            //public byte TableRightDOSlot = 255;
            //public byte TableRightDOSignal = 255;
            //public short TableRightDOMSTimeout = 0;
            //public byte FlapOpenDOSlot = 255;
            //public byte FlapOpenDOSignal = 255;
            //public short FlapOpenDOMSDelay = 0;

            //public byte ExitDISlot = 255;
            //public byte ExitDISignal = 255;
            //public byte ExitDIOnHigh = 255;

            //public byte EmergencyStopDISlot = 255;
            //public byte EmergencyStopDISignal = 255;
            //public byte EmergencyStopDIOnHigh = 255;

            //public byte DoorOpenDISlot = 255;
            //public byte DoorOpenDISignal = 255;
            //public byte DoorOpenDIOnHigh = 255;

            //public byte GreenLightDOSlot = 255;
            //public byte GreenLightDOSignal = 255;
            //public byte YellowLightDOSlot = 255;
            //public byte YellowLightDOSignal = 255;
            //public byte RedLightDOSlot = 255;
            //public byte RedLightDOSignal = 255;
            //public byte HornDOSlot = 255;
            //public byte HornDOSignal = 255;

            //public short HornOnTime = 0;
            //public short HornOffTime = 0;

            //public short GreenOnTime = 0;
            //public short GreenOffTime = 0;

            //public short YellowOnTime = 0;
            //public short YellowOffTime = 0;

            //public short RedOnTime = 0;
            //public short RedOffTime = 0;

            //public byte PulseChannelStacker = 255;

            //public ushort CycleTimeMSecStacker = 0;
            //public short ResetMMBeforeAction = 0;
            //public uint MMInfeedToEntrance = 0;//distance in millimeters
            //public ushort MarginMMInfeedToEntrance = 0;//margin in millimeters

            //public ushort MMEntranceToIntercept = 0;//distance in millimeters
            //public ushort MarginMMEntranceToIntercept = 50;//margin in millimeters
            //public ushort MMInterceptToDrop = 0;
            ////moved to grippersetup:public ushort MMEntranceCopyDistance;//distance in millimeters between copies at stacker entrance
            //public byte InterceptSpeedFactor = 0;
            ////public ushort MSecStartEjectToRelease;
            ////public ushort MarginMSecStartEjectToRelease;

            //public ushort StackerPulsesPrMeter = 0;
            #endregion
        }

        public struct DbCommandStruct
        {
            public uint CommandId;
            public enumDataBaseCommands Command;
            public byte Status;
            public byte Status2;
            public byte StackerNumber;
            public ushort BundleNumber;
            public uint TableId;
            public int CopyNumber;
        }

        //public struct PrintServerCommandStruct
        //{
        //    public byte command;
        //    //public byte stackerNumber;
        //    public ushort bundleNumber;
        //    public ushort bundleIndex;
        //}

        #endregion

        #region Static variables

        public static enumLanguages SelectedLanguage;

        #endregion

        #region Variables

        //private List<GuiLanguage> _guiLanguageList;
        private byte _overrideProductionLineNumber;
        private LineControlUI _lineControlUI;
        //private TopSheetControlLib.TopSheetMain _topSheetMain;
        //private string _serverAddress;
        private string _connectionStringServer;
        //private string _connectionStringLocal;
        private string _connectionStringLabelDesign;
        private object _lockStatus;
        private object _lockTelegramListIcp;
        //private object _lockTelegramListPrintServer;

        //private enumLanguages _selectedLanguage;

        private OleDbConnection _conServer;
        //private OleDbConnection _conLocal;

        private Thread _lineControlThread;
        private Thread _icpStackerThread;
        private Thread _dbThreadServer;
        //private Thread _dbThreadLocal;
        //private Thread _topSheetThread;

        private TcpClient _tcpClientIcpStacker;
        private NetworkStream _networkStreamIcpStacker;
        private BinaryReader _binaryReaderIcpStacker;
        private BinaryWriter _binaryWriterIcpStacker;
        private List<byte[]> _telegramListIcpStacker;

        //private TcpClient _tcpClientPrintServer;
        //private NetworkStream _networkStreamPrintServer;
        //private BinaryReader _binaryReaderPrintServer;
        //private BinaryWriter _binaryWriterPrintServer;
        private byte _pcStatusStacker;
        private byte _telegramIdStacker;
        private byte[] _telegramRequestStatus;
        private byte[] _telegramStartProduction;
        private byte[] _telegramStopProduction;
        private byte[] _telegramReset;
        private byte[] _telegramClearCounters;
        private byte[] _telegramStartStopSimulation;
        private string _ioIpStacker;
        private int _ioPortStacker;
        private string[] _ioIpTopSheet;
        private int _ioPortTopSheet;
        private string[] _printerName1;
        private string _printerName2;
        private string[] _printerIp1;
        private string _printerIp2;
        private int _printerPort1;
        private int _printerPort2;
        private bool _useTwoPrinters;
        private bool _doReconnectIcpStacker;
        //private bool _doReconnectPrintServer;
        private bool _doReconnectDbServer;

        private bool[] _IsFirstBundle;

        private SetupStruct _setup;
        private StatusStruct _status;
        //private int _setupStructSize;
        private int _statusStructSize;
        private List<DbCommandStruct> _dbCommandListServer;
        private object _lockDbCommandListServer;

        //private List<DbCommandStruct> _dbCommandListLocal;
        private List<BundleData>[] _newProductionQueue;
        private List<BundleData>[] production_list;

        private object[] _lockNewProductionQueue;
        private object[] _lockProductionList;

        private uint _nextDbCommandIdServer;
        //private uint _nextDbCommandIdLocal;
        //private ushort _bundleIndexToTcpServer;
        //private List<PrintServerCommandStruct> _printServerCommandList;
        //private long _paperTimeReal;
        private DateTime[] _lastProductionDataSent;
        private DateTime _lastLineStatusSent;
        //private DateTime[] _last
        private List<int> _dbProcessTicksServer;
        //private List<int> _dbProcessTicksLocal;
        //private List<BadBundleItem> _badBundleList;
        private bool _setupReceived;
        private List<SetupItem> _setupItemList;
        //private List<GuiLanguageItem> _languageItemList;
        private bool _statusReceived;
        //private bool _layerCommandsReceived;
        private bool[] _newBundlesReceived;

        private string _stackerId;
        //private bool _resetFromStacker;

        private object _lockGuiLogItemList;
        private List<GuiLogItem> _guiLogItemList;

        private List<GripperItem> _gripperItemList;
        private object _lockGripperItemList;
        private List<StackerLine> _stackerList;

        //private ushort _paperTimeReal;
        //private bool _deactivateInkPrint;
        //private bool _deactivateInkSendSetup;

        private object _lockAddressSendQueue;
        private List<AddressData> _addressSendQueue;

        #endregion

        #region Constructor

        public LineControlMain(LineControlUI lineControlUI,
            string connectionStringServer,
            string connectionStringLabelDesign
           // string printerName1,
            //string printerName2,
            //string printerIp1,
            //string printerIp2,
            //int printerPort1,
            //int printerPort2,
            //bool useTwoPrinters,
            //byte overrideProductionLineNumber
            )
        {
            string exeName = System.Reflection.Assembly.GetExecutingAssembly().GetModules()[0].FullyQualifiedName;
            string exePath = System.IO.Path.GetDirectoryName(exeName);
            Logging.LogFolder = exePath + "\\Log";
            _lineControlUI = lineControlUI;
            _connectionStringServer = connectionStringServer;
            _connectionStringLabelDesign = connectionStringLabelDesign;
            SelectedLanguage = enumLanguages.English;
            _statusReceived = false;
            //_layerCommandsReceived = false;


            _IsFirstBundle = new bool[2];
            _IsFirstBundle[0] = true;
            _IsFirstBundle[1] = true;

            _lockStatus = new object();
            _lockTelegramListIcp = new object();

            _lockNewProductionQueue = new object[2];
            _lockNewProductionQueue[0] = new object();
            _lockNewProductionQueue[1] = new object();

            _lockProductionList = new object[2];
            _lockProductionList[0] = new object();
            _lockProductionList[1] = new object();
            

            //_lockTelegramListPrintServer = new object();
            _conServer = null;
            IsStarted = false;
            IcpConnectionState = enumIcpConnectionStates.NotRunning;
            DbConnectionStateServer = enumDbConnectionStates.DbUnknown;
            _telegramListIcpStacker = new List<byte[]>();
            _telegramRequestStatus = null;
            //_pcStatus = 1;
            _doReconnectIcpStacker = false;
            _setup = new SetupStruct();
            _setup.GripperLines = new GripperLineSetupStruct(); //[MAXGRIPPERLINES];
            //for (int i = 0; i < _setup.GripperLines.Length; i++)
            //{
            //    _setup.GripperLines[i] = new GripperLineSetupStruct();
            //}

            //_setup.Stackers = new StackerSetupStruct[MAXSTACKERS];
            //for (int i = 0; i < _setup.Stackers.Length; i++)
            //{
            //    _setup.Stackers[i] = new StackerSetupStruct();
            //}

            _status = new StatusStruct();
            _status.ActiveState = 0;
            _status.Stackers = new StackerStatusStruct[2];
            _status.Stackers[0] = new StackerStatusStruct();
            _status.Stackers[1] = new StackerStatusStruct();

            //_setupStructSize = Marshal.SizeOf(typeof(SetupStruct));
            _statusStructSize = Marshal.SizeOf(typeof(StatusStruct));
            _dbCommandListServer = new List<DbCommandStruct>();
            _lockDbCommandListServer = new object();
 
            _newProductionQueue = new List<BundleData>[2];
            _newProductionQueue[0] = new List<BundleData>();
            _newProductionQueue[1] = new List<BundleData>();

            production_list = new List<BundleData>[2];
            production_list[0] = new List<BundleData>();
            production_list[1] = new List<BundleData>();

            //_newProductionQueueReproduce = new List<BundleData>();
            //_bundleIdsInProduction = new List<ushort>();

            _newBundlesReceived = new bool[2];
            _newBundlesReceived[0] = false;
            _newBundlesReceived[1] = false;
            //_setAllBundlesProducedReceived = false;
            //_setSmallBundlesProducedReceived = false;
            //_isPaused = false;
            //_productionSetupReceiving = false;
            //_productionSetupUpdated = false;
            //_printServerCommandList = new List<PrintServerCommandStruct>();
            //_paperTimeReal = 0;
            _lastProductionDataSent = new DateTime[2];
            _lastProductionDataSent[0] = DateTime.MinValue;
            _lastProductionDataSent[1] = DateTime.MinValue;
            //_waitingToSendProductionData = false;
            _lastLineStatusSent = DateTime.MinValue;
            _dbProcessTicksServer = new List<int>();
            //_dbProcessTicksLocal = new List<int>();
            //_badBundleList = new List<BadBundleItem>();
            //_lastBundleMessage = DateTime.MinValue;
            //_bundleIndexToTcpServer = 20000;
            _setupItemList = new List<SetupItem>();
            //_languageItemList = new List<GuiLanguageItem>();

            _ioIpStacker = "32.10.37.13";
            _ioPortStacker = 10000;
            _ioIpTopSheet = new string[2];
            _ioIpTopSheet[0] = "32.10.37.14"; //Small bundle for Stacker1
            _ioIpTopSheet[1] = "32.10.37.15"; //Store bundle for Stacker2
            _ioPortTopSheet = 10000;
            _printerName1 = new string[2];
            _printerName1[0] = "KYD151C5"; //KYD151CC
            _printerName1[1] = "KYD151CC"; //KYD151CC
            _printerName2 = null;
            _printerIp1 = new string[2];
            _printerIp1[0] = "32.10.37.28";
            _printerIp1[1] = "32.10.37.29";
            _printerIp2 = null;
            _printerPort1 = 0; // 9100; //0
            _printerPort2 = 0;
            _useTwoPrinters = false;
            _overrideProductionLineNumber = 0;
            _setup.ProductionLineNumber = 0;

            //_statusReceived = false;
            //_layerCommandsReceived = false;
            //SetSetupValues();

            _pcStatusStacker = 1;//1=normal, 2=no ink, 3=paused, 4=stacker exit blocked

            //AddTestBundles();
            //byte[] testSetup = GetSetupStructBytes();

            _lockGuiLogItemList = new object();
            _guiLogItemList = new List<GuiLogItem>();

            _lockGripperItemList = new object();
            _gripperItemList = new List<GripperItem>();

            _stackerList = new List<StackerLine>();
            StackerLine stackerData = new StackerLine(1);
            _stackerList.Add(stackerData);
            stackerData = new StackerLine(2);
            _stackerList.Add(stackerData);

            _addressSendQueue = new List<AddressData>();
            _lockAddressSendQueue = new object();
        }

        //private void AddTestBundles()
        //{
        //    int firstBundleId = 15001;
        //    int bundleCount = 5;

        //    for (int i = 0; i < bundleCount; i++)
        //    {
        //        if (i % 2 == 0)
        //        {
        //            _newProductionQueue.Add(GetNewBundle((ushort)(firstBundleId + i), 20));
        //        }
        //        else
        //        {
        //            _newProductionQueue.Add(GetNewBundle((ushort)(firstBundleId + i), 20));
        //        }
        //    }

        //    AddToLog(string.Format("Test bundles added. First bundle id = {0}, Bundle count = {1}", firstBundleId, bundleCount));
        //    //_newBundlesReceived = true;

        //}

        private BundleData GetNewBundle(ushort bundleId, ushort numCopies)
        {
            BundleData result = new BundleData();

            result.BundleId = bundleId;
            result.NumCopies = numCopies;
            result.Stacker = 0;
            result.Status = enumBundleStatus.bundleStatusReady;

            return result;
        }

        //private void SetSetupValues()
        //{
        //    UpdateSettingsGui();
        //}

        #endregion

        #region Properties

        public bool IsStarted { get; set; }
        public int ProductionListSize { get; set; }
        public enumLineControlStates LineControlState { get; set; }
        //public enumTopSheetControlStates TopSheetControlState { get; set; }
        public enumIcpConnectionStates IcpConnectionState { get; set; }
        public enumDbConnectionStates DbConnectionStateServer { get; set; }
        //public enumDbConnectionStates DbConnectionStateLocal { get; set; }
        public uint CopiesPrHour { get; set; }

        public enumInkStatus InkStatus { get; set; }

        public byte PcStatusStacker
        {
            get
            {
                byte result = _pcStatusStacker;

                if (_setup == null || _setup.Mode == enumModes.STANDARD)
                {
                    result = 1;
                }
                /*else
                {
                    if (_topSheetMain != null && !_topSheetMain.ReadyToReceive)
                    {
                        result = 4;
                    }
                    else if (result == 1)
                    {
                        if (_setup.IsPaused == 1)
                        {
                            result = 3;
                        }
                        if (_topSheetMain == null || !_topSheetMain.ReadyForProduction)
                        {
                            result = 3;
                        }
                        //else if (_topSheetMain != null && !_topSheetMain.ReadyToReceive)
                        //{
                        //    result = 4;
                        //}
                    }
                }*/

                return result;
            }
        }

        //public string StackerId
        //{
        //    get
        //    {
        //        return _stackerId;
        //    }
        //}

        public SetupStruct SetupLine
        {
            get { return _setup; }
        }

        public StatusStruct StatusLine
        {
            get { return _status; }
        }

        //public byte PaperPageCount
        //{
        //    get { return _paperPageCount; }
        //    set { _paperPageCount = value; }
        //}

        //public enumFoldingType FoldingType
        //{
        //    get { return _foldingType; }
        //    set { _foldingType = value; }
        //}

        #endregion

        #region Public methods

        public void Start()
        {
            if (IsStarted)
            {
                Stop();
            }

            _doReconnectIcpStacker = false;
            _setupReceived = false;

            IsStarted = true;

            _lineControlThread = new Thread(new ThreadStart(LineControl));
            //_topSheetThread = new Thread(new ThreadStart(TopSheetControl));
            _icpStackerThread = new Thread(new ThreadStart(IcpControl));
            _dbThreadServer = new Thread(new ThreadStart(DbControlServer));
            //_dbThreadLocal = new Thread(new ThreadStart(DbControlLocal));

            //_topSheetMain = new TopSheetControlLib.TopSheetMain(_connectionString, _runningStackerLineId);

            _lineControlThread.Start();
            _icpStackerThread.Start();
            _dbThreadServer.Start();
            //_dbThreadLocal.Start();
        }

        public void Stop()
        {
            int waitTime = 0;
            if (IcpConnectionState == enumIcpConnectionStates.Connected)
            {
                if (_status.SimulateState > 0)
                {
                    bool isStartingSimulation;
                    for (int i = 0; i < _stackerList.Count; i++)
                    {
                        StackerLine stacker = _stackerList[i];
                        RequestStartStopSimulation(stacker.StackerNumber, out isStartingSimulation);
                    }
                    waitTime += 1000;
                }
            }

            if (waitTime > 0)
            {
                Thread.Sleep(waitTime);
            }

            IsStarted = false;

            for (int i = 0; i < _stackerList.Count; i++)
            {
                StackerLine stacker = _stackerList[i];
                if (stacker.TopSheetMain != null)
                {
                    stacker.TopSheetMain.Stop();
                    //_topSheetMain = null;
                }
            }

            int counter = 0;
            while (++counter < 20 &&
                (LineControlState != enumLineControlStates.NotRunning ||
                //TopSheetControlState != enumTopSheetControlStates.NotRunning ||
                IcpConnectionState != enumIcpConnectionStates.NotRunning ||
                DbConnectionStateServer != enumDbConnectionStates.DbUnknown

                //||
                //DbConnectionStateLocal != enumDbConnectionStates.DbUnknown
                ))
            {
                Thread.Sleep(100);
            }

            if (IcpConnectionState != enumIcpConnectionStates.NotRunning)
            {
                IcpDisconnectStacker();

                try
                {
                    if (_icpStackerThread != null)// && _icpThread.ThreadState == ThreadState.Running)
                    {
                        _icpStackerThread.Interrupt();
                    }
                }
                catch
                {
                }
            }

            if (DbConnectionStateServer != enumDbConnectionStates.DbUnknown)
            {
                DbDisconnectServer();

                try
                {
                    if (_dbThreadServer != null)// && _dbThread.ThreadState == ThreadState.Running)
                    {
                        _dbThreadServer.Interrupt();
                    }
                }
                catch
                {
                }
            }

            //if (DbConnectionStateLocal != enumDbConnectionStates.DbUnknown)
            //{
            //    DbDisconnectLocal();

            //    try
            //    {
            //        if (_dbThreadLocal != null)// && _dbThread.ThreadState == ThreadState.Running)
            //        {
            //            _dbThreadLocal.Interrupt();
            //        }
            //    }
            //    catch
            //    {
            //    }
            //}

            //if (IcpConnectionState != enumIcpConnectionStates.NotRunning)
            //{
            //    IcpDisconnect();
            //}

            //if (DbConnectionState != enumDbConnectionStates.NotRunning)
            //{
            //    DbDisconnect();
            //}
            OleDbConnection con = null;

            try
            {
                lock (_lockDbCommandListServer)
                {
                    _dbCommandListServer.Clear();
                }

                string sql = string.Format("DECLARE @return_value int; EXEC @return_value = [dbo].[LCCGetNextCommand]"
                    + " @LineNumber = {0}, @CopiesPrHour = {1};SELECT 'Return Value' = @return_value",
                    _setup.ProductionLineNumber + 100, 0);
                enumDataBaseCommands currentCommand = enumDataBaseCommands.dbCommGetNextCommand;

                con = new OleDbConnection(_connectionStringServer);
                con.Open();
                DbHandleCommand(con, sql, currentCommand);
            }
            catch
            {
            }
            finally
            {
                if (con != null && con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }
        }

        public void RequestStopStacker()
        {
        }

        public void RequestResetStacker(byte stackerNumber)
        {
            IcpGetOrAddTelegramStacker(enumIcpTelegramTypes.Reset, stackerNumber, true);
        }

        public void RequestClearCounters()
        {
            //IcpGetOrAddTelegramStacker(enumIcpTelegramTypes.ClearCounters, null, true);

            IcpGetOrAddTelegramStacker(enumIcpTelegramTypes.TestImajePrint, null, true);

            //if (_topSheetMain != null)
            //{
            //    _topSheetMain.PrintBundle(11111);
            //    _topSheetMain.BundleEjected(10001);
            //}
        }

        public void RequestSendLayerPlanToICP()
        {
            IcpGetOrAddTelegramStacker(enumIcpTelegramTypes.SendLayerCommand, null, true);
        }

        public void RequestStartStopSimulation(byte stackerNumber, out bool isStartingSimulation)
        {
            bool doReset = IcpConnectionState == enumIcpConnectionStates.Connected && _status.SimulateState > 0;
            isStartingSimulation = IcpConnectionState == enumIcpConnectionStates.Connected && _status.SimulateState == 0;

            if (IcpConnectionState == enumIcpConnectionStates.Connected)
            {
                IcpGetOrAddTelegramStacker(enumIcpTelegramTypes.StartStopSimulation, null, true);

                if (doReset)
                {
                    IcpGetOrAddTelegramStacker(enumIcpTelegramTypes.Reset, stackerNumber, true);
                }
            }
        }

        public void RequestSetupSendToStacker()
        {
            IcpGetOrAddTelegramStacker(enumIcpTelegramTypes.SendSetup, null, true);
            UpdateStatusGui();
        }

        public string[] GetStackerIds()
        {
            string[] result = null;

            try
            {
                string sql = "select distinct StackerId from StackerSetupTable order by StackerId";
                OleDbCommand cmd = new OleDbCommand(sql, _conServer);
                OleDbDataReader dataReader = cmd.ExecuteReader();
                List<string> idList = new List<string>();
                while (dataReader.Read())
                {
                    idList.Add((string)dataReader[0]);
                }
                result = idList.ToArray();
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.ToString());
            }

            return result;
        }

        public void SetSelectedLanguage(enumLanguages language)
        {
            if (SelectedLanguage != language)
            {
                SelectedLanguage = language;
                UpdateLanguageGui();
                UpdateStatusGui();
            }
        }

        public SetupItem GetSetupItem(string itemName)
        {
            SetupItem result = null;

            if (itemName != null && itemName.Length > 0 && _setupItemList != null)
            {
                int index = -1;
                while (result == null && ++index < _setupItemList.Count)
                {
                    if (itemName.Equals(_setupItemList[index].FieldName))
                    {
                        result = _setupItemList[index];
                    }
                }
            }

            return result;
        }

        #endregion

        #region Line control

        private bool GetTakt(out byte gripperNumber, out byte copies)
        {
            bool result = false;
            gripperNumber = 0;
            copies = 0;

            lock (_lockGripperItemList)
            {
                if (_gripperItemList.Count > 0)
                {
                    if (IcpConnectionState == enumIcpConnectionStates.Connected)
                    {
                        GripperItem gripperItem = _gripperItemList[0];
                        _gripperItemList.RemoveAt(0);

                        gripperNumber = gripperItem.GripperNumber;
                        copies = gripperItem.Copies;
                        result = true;
                    }
                    else
                    {
                        _gripperItemList.Clear();
                    }
                }
            }

            return result;
        }

        private void LineControl()
        {
            try
            {
                DateTime[] lastBundleCheck= new DateTime[2];
                lastBundleCheck[0] = DateTime.MinValue;
                lastBundleCheck[1] = DateTime.MinValue;

                DateTime lastStatusUpdate = DateTime.Now;

                byte wasPaused = 0;
                double refreshScreenRate = LineControlUI.UseBinding ? 500 : 1000;
                List<StackerLine> stacker_list = _stackerList;

                LineControlState = enumLineControlStates.Running;
                AddToLog("LineControl started");

                while (UniDevLib.StaticInfo.IsRunning &&
                    IsStarted && _lineControlThread != null && _lineControlThread.ManagedThreadId == Thread.CurrentThread.ManagedThreadId)
                {
                    try
                    {
                        try
                        {
                            //STATUS UPDATE BEGIN
                            if (DateTime.Now > lastStatusUpdate.AddMilliseconds(refreshScreenRate))
                            {
                                lastStatusUpdate = DateTime.Now;
                                bool doSendStatusToDb = DateTime.Now > _lastLineStatusSent.AddSeconds(3);

                                if (IcpConnectionState != enumIcpConnectionStates.Connected)
                                {
                                    InkStatus = enumInkStatus.NotAvailable;
                                }
                                else if (StatusLine.InkJetControlState >= 100)
                                {
                                    InkStatus = enumInkStatus.Connecting;
                                }
                                else
                                {
                                    InkStatus = enumInkStatus.Ready;
                                }

                                if (doSendStatusToDb)
                                {
                                    _lastLineStatusSent = DateTime.Now;

                                    //DbSetLineStatus(lineStatus);
                                    DbSetInkStatus(InkStatus);
                                }

                                for (int i = 0; i < _stackerList.Count; i++)
                                {
                                    StackerLine stacker = _stackerList[i];

                                    if (_statusReceived)
                                    {
                                        if (_status.Stackers != null)
                                        {
                                            if (_status.Stackers[i].State == 0)
                                            {
                                                stacker.Status = enumStackerStatus.NotReady;
                                            }
                                            else
                                            {
                                                stacker.Status = enumStackerStatus.Ready;
                                            }
                                        }
                                    }

                                    if (IcpConnectionState != enumIcpConnectionStates.Connected) {
                                        stacker.Status = enumStackerStatus.Connecting;
                                    } else if (stacker.LastBundleMessage > DateTime.Now.AddSeconds(-15)) {
                                        stacker.Status = enumStackerStatus.Producing;
                                    } 

                                    if (doSendStatusToDb)
                                    {
                                        _lastLineStatusSent = DateTime.Now;
                                        DbSetStackerStatus(stacker.Status, stacker.StackerNumber);// _stackerLineId);
                                        DbSetApplicatorStatus(stacker.TopSheetApplicatorState, stacker.StackerNumber);
                                        DbSetPrinterStatus(stacker.TopSheetPrinterState1, stacker.StackerNumber, 1);
                                        //DbSetPrinterStatus(stacker.TopSheetPrinterState2, _setup.ProductionLineNumber, 2);
                                        //DbSetNextMachineStatus(stacker.NextMachineState, _setup.ProductionLineNumber);
                                    }
                                }

                                UpdateStatusGui();
                                UpdateLogGui();
                            }
                            //STATUS UPDATE END

                            //INSERT NEW BUNDLES TO PRODUCTION LISTS - BEGIN                           
                            for (int i = 0; i < _stackerList.Count; i++)
                            {
                                lock (_lockNewProductionQueue[i])
                                {
                                    if (_newBundlesReceived[i])
                                    {
                                        if (_newProductionQueue[i].Count > 0)
                                        {
                                            lock (_lockProductionList[i])
                                            {
                                                //if (_newProductionQueue.Count > 0)
                                                while (_newProductionQueue[i].Count > 0)
                                                {
                                                    Thread.Sleep(1);
                                                    production_list[i].Add(_newProductionQueue[i][0]);
                                                    _newProductionQueue[i].RemoveAt(0);
                                                    //production_list.AddRange(_newProductionQueue);
                                                }
                                                AddToLog(string.Format("New bundles inserted to production list - new size = {0}", production_list[i].Count));
                                                //_newProductionQueue.Clear();                                                 
                                            }
                                        }
                                        _newBundlesReceived[i] = false;
                                    }                               
                                }
                            }
                            //INSERT NEW BUNDLES TO PRODUCTION LISTS - END


                            //CREATE TOPSHEETMAIN, CHECK FOR RESET, REQUEST NEW BUNDLES FROM DB - BEGIN
                            for (int stackerIndex = 0; stackerIndex < _stackerList.Count; stackerIndex++)
                            {
                                StackerLine stacker = _stackerList[stackerIndex];
                                byte stackerNumber = stacker.StackerNumber;

                                if (stacker.TopSheetMain == null)// || !_topSheetMain.IsStarted)
                                {
                                    stacker.TopSheetMain = new TopSheetControlLib.TopSheetMain(_connectionStringServer,
                                        _connectionStringLabelDesign, _setup.ProductionLineNumber,
                                        _ioIpTopSheet[stackerIndex], _ioPortTopSheet, _useTwoPrinters,
                                        _printerName1[stackerIndex], _printerName2, _printerIp1[stackerIndex], _printerIp2, _printerPort1, _printerPort2);
                                }

                                if (IsStarted && !stacker.TopSheetMain.IsStarted)
                                {
                                    stacker.TopSheetMain.Start(stackerNumber, _setup.CurrentGLNumber);
                                }

                                if (wasPaused != _setup.IsPaused)
                                {
                                    wasPaused = _setup.IsPaused;

                                    if (_setup.IsPaused == 1)
                                    {
                                        for (int i = 0; i < production_list[stackerIndex].Count; i++)
                                        {
                                            if (production_list[stackerIndex][i] != null && production_list[stackerIndex][i].BundleId > 0)
                                            {
                                                SetBundleStatus(enumBundleStatus.bundleStatusStackerFailed,
                                                    _setup.ProductionLineNumber, production_list[stackerIndex][i].BundleId, 0);
                                            }
                                        }
                                        production_list[stackerIndex].Clear();
                                        IcpGetOrAddTelegramStacker(enumIcpTelegramTypes.StopProduction, null, true);
                                        AddToLog(string.Format("Production stopped"));
                                    }
                                    else
                                    {
                                        IcpGetOrAddTelegramStacker(enumIcpTelegramTypes.StartProduction, null, true);
                                        AddToLog(string.Format("Production started"));
                                    }
                                }

                                if (!_newBundlesReceived[stackerIndex] && 
                                    _newProductionQueue[stackerIndex].Count == 0 &&
                                    DateTime.Now > lastBundleCheck[stackerIndex].AddSeconds(3)) // && production_list[stackerIndex].Count < 5)
                                {
                                    lastBundleCheck[stackerIndex] = DateTime.Now;
                                    if (production_list[stackerIndex].Count < 5)
                                    {
                                        DbGetBundleList((ushort)(6 - production_list[stackerIndex].Count), (byte)(stackerIndex+1));
                                    }
                                    //else
                                    //{
                                    //    DbGetBundleList(0, (byte)(stackerIndex + 1));//just to show alive
                                    //}
                                }

                                if (IcpConnectionState != enumIcpConnectionStates.Connected ||
                                    _status.Stackers[stackerIndex].State == 0 || _setup.IsPaused == 1 ||
                                    stacker.ResetFromStacker
                                    //|| _status.StackerButtonResetState > 0
                                    )
                                {
                                    stacker.ResetFromStacker = false;

                                    Thread.Sleep(1000);

                                    if (stacker.TopSheetMain != null && stacker.TopSheetMain.PrintQueueWaiting > 0)
                                    {
                                        stacker.TopSheetMain.ClearPrintQueueWaiting();
                                    }
                                    stacker.TopSheetMain.ResetApplicator(true);
                                }
                                stacker.TopSheetMain.PcStatus = 2;//label with tracking
                            }
                            //CREATE TOPSHEETMAIN, CHECK FOR RESET, REQUEST NEW BUNDLES FROM DB - END


                            //PRODUCTION CONTROL - SEND LAYERS AND ADDRESSES - BEGIN
                            //int j = 0;
                            for (int i = 0; i < _stackerList.Count; i++)
                            {
                                if (production_list[i].Count > 0)
                                {
                                    if (_statusReceived)
                                    {
                                        if (_status.Stackers != null)
                                        {
                                            //ProductionControl(_stackerList[i], _status.Stackers[i], _IsFirstBundle[i]);
                                            ProductionControl(i, _status.Stackers[i], _IsFirstBundle[i]);

                                            if (_IsFirstBundle[i])
                                            {
                                                _IsFirstBundle[i] = false;                                           
                                            }
                                        }
                                    }
                                    //j++;
                                }
                            }

                            //if (j > 0)
                            //{
                            //    _layerCommandsReceived = false;
                            //}
                            //else {
                            //    _layerCommandsReceived = true;
                            //}
                            //_layerCommandsReceived = false;
                            //PRODUCTION CONTROL - SEND LAYERS AND ADDRESSES - END
                        }
                        catch (Exception ex)
                        {
                            AddToLog(true, LogLevels.Warning, ex.ToString());
                        }
                        finally
                        {
                            Thread.Sleep(500);
                        }
                    }
                    catch
                    {
                    }
                }
                LineControlState = enumLineControlStates.NotRunning;
                AddToLog("LineControl stopped");
            }
            catch
            {
            }
        }


        //private List<AddressData> GetAddressListOfALayer(byte StackerNr, BundleData _CurBundle, byte CurLayerNr)
        //{
        //    List<AddressData> CurAddressList = new List<AddressData>();

        //    if (_CurBundle.NextLayerIndex < _CurBundle.LayerCommandList.Count)
        //    {
        //        for (byte i = _CurBundle.NextAddressIndex; i < _CurBundle.NextAddressIndex + (byte)(_CurBundle.LayerCommandList[_CurBundle.NextLayerIndex].NumAddress); i++)
        //        {
        //            CurAddressList.Add(_CurBundle.AddressList[i]);
        //        }
        //    }
        //    return CurAddressList;
        //}

        //private int ProductionControl(StackerLine stacker, StackerStatusStruct stackerStatus, List<BundleData> _productionList)
        //private int ProductionControl(StackerLine stacker, StackerStatusStruct stackerStatus, bool IsFirst)
        private int ProductionControl(int StackerID, StackerStatusStruct stackerStatus, bool IsFirst)
        {
            int result = 0;
            //int StackerID = stacker.StackerNumber - 1;
            bool IsLastLayerInBundle = false;
            LayerData CurLayer = null;

            List<AddressData> CurAddressList = new List<AddressData>();

            if (production_list[StackerID].Count > 0)
            {
                BundleData bundleData = production_list[StackerID][0];

                if ((bundleData.BundleId == stackerStatus.LastReceivedBundleID) && (bundleData.NextLayerIndex == stackerStatus.LastReceivedLayerID))
                {
                    SetBundleStatus(enumBundleStatus.bundleStatusReservedLCC, (byte)(StackerID+1), bundleData.BundleId, 0);
                    //bundleData.NextLayerIndex == 
                    if (stackerStatus.LastReceivedLayerID == bundleData.NumLayersInBundle)
                    {
                        lock (_lockProductionList[StackerID])
                        {
                            production_list[StackerID].RemoveAt(0);
                            if (production_list[StackerID].Count > 0)
                            {
                                bundleData = production_list[StackerID][0]; //Get next bundle                        
                            }
                            else
                            {
                                return 1; //Finished production list for curStackerID
                            }                    
                        }
                    }
                }
                else {  //Wait until ICP received the bundleID and LayerID
                    //Retransmit last bundleID and layerID when timeout, Todo
                    if (!IsFirst && DateTime.Now < _lastProductionDataSent[StackerID].AddSeconds(3))
                    {//Retransmit                    
                        return 0;
                    }   else {
                        _lastProductionDataSent[StackerID] = DateTime.Now;
                    }
                }

                if ((stackerStatus.NumRequestLayer) > 0 && 
                    (stackerStatus.NumRequestAddress > bundleData.LayerCommandList[bundleData.NextLayerIndex].NumAddress))
                {//In this case we can send both this layer and its address

                    CurLayer = bundleData.GetNextLayerCommand(out IsLastLayerInBundle);
                    IcpGetOrAddTelegramStacker(enumIcpTelegramTypes.SendLayerCommand, CurLayer, true);
                    CurAddressList = bundleData.GetAddressListOfCurLayer(CurLayer.LayerNumber-1);
                    //GetAddressListOfALayer(stacker.StackerNumber, bundleData, bundleData.NextLayerIndex);

                    if (CurAddressList.Count > 0)
                    {
                        IcpGetOrAddTelegramStacker(enumIcpTelegramTypes.SendAddressList, CurAddressList, true);                    
                    }
                    
                    result = 2;
                    Thread.Sleep(500);
                }
            }
            return result;
        }

        private void SetBundleStatus(enumBundleStatus status, byte stackerNumber, ushort bundleNumber, int copyNumber)
        {
            StackerLine stacker = GetStackerLine(stackerNumber);

            if (stacker != null)
            {
                //if (status == enumBundleStatus.bundleStatusInStacker && bundleNumber > 0)
                if (status == enumBundleStatus.bundleStatusInStacker && bundleNumber > 0)
                {
                    if (stacker.TopSheetMain != null && stacker.TopSheetMain.IsStarted)
                    {
                        stacker.TopSheetMain.PrintBundle(bundleNumber);
                    }

                    //PrintServerSendBundleReleasedToStacker(stackerNumber, bundleNumber);
                }
                else if (status == enumBundleStatus.bundleStatusAfterStacker && bundleNumber > 0)
                {
                    if (stacker.TopSheetMain != null && stacker.TopSheetMain.IsStarted)
                    {
                        //_topSheetMain.PrintBundle(bundleNumber);

                        stacker.TopSheetMain.BundleEjected(bundleNumber);
                    }
                    //PrintServerSendProducedBundle(stackerNumber, bundleNumber);
                }
                else if ((status == enumBundleStatus.bundleStatusBundleError
                    || status == enumBundleStatus.bundleStatusStackerFailed
                    || status == enumBundleStatus.bundleStatusInkFailed)
                    && bundleNumber > 0)
                {
                    if (stacker.TopSheetMain != null)
                    {
                        stacker.TopSheetMain.RemoveFromPrintQueueWaiting(bundleNumber);
                    }
                }
            }

            DbSetBundleStatus(status, stackerNumber, bundleNumber, copyNumber);
        }

        private StackerLine GetStackerLine(byte stackerNumber)
        {
            StackerLine result = null;
            int index = -1;

            while (result == null && ++index < _stackerList.Count)
            {
                if (stackerNumber == _stackerList[index].StackerNumber)
                {
                    result = _stackerList[index];
                }
            }

            return result;
        }

        public List<StackerLine> StackerList { get { return _stackerList; } }

        private byte[] GetSetupStructBytes()//SetupStruct setup)
        {
            //int len = 0;// _setupStructSize;// Marshal.SizeOf(_setup);
            byte[] result;// = new byte[len];

            //IntPtr ptr = Marshal.AllocHGlobal(len);
            //Marshal.StructureToPtr(_setup, ptr, true);
            //Marshal.Copy(ptr, result, 0, len);

            //Marshal.FreeHGlobal(ptr);

            long tick = Environment.TickCount;

            List<byte> byteList = new List<byte>();
            List<FieldInfo> fieldList = new List<FieldInfo>();
            List<object> fieldOwnerList = new List<object>();
            object fieldOwner;
            FieldInfo fi;

            FieldInfo[] fiArray = typeof(SetupStruct).GetFields();
            IEnumerable<FieldInfo> fiSorted = fiArray.OrderBy(f => f.MetadataToken);
            IEnumerator<FieldInfo> fiEnumerator = fiSorted.GetEnumerator();

            //for (int i = 0; i < fiArray.Length; i++)
            while (fiEnumerator.MoveNext())
            {
                //fi = fiArray[i];
                fi = fiEnumerator.Current;

                if (fi.FieldType.IsArray)
                {
                    Array arrayField = (Array)fi.GetValue(_setup);
                    for (int i2 = 0; i2 < arrayField.Length; i2++)
                    {
                        fieldOwner = arrayField.GetValue(i2);
                        FieldInfo[] fiSubArray = fieldOwner.GetType().GetFields();
                        IEnumerable<FieldInfo> fiSubSorted = fiSubArray.OrderBy(f => f.MetadataToken);
                        IEnumerator<FieldInfo> fiSubEnumerator = fiSubSorted.GetEnumerator();

                        //for (int i3 = 0; i3 < fiSubArray.Length; i3++)
                        while (fiSubEnumerator.MoveNext())
                        {
                            //fi = fiSubArray[i3];
                            fi = fiSubEnumerator.Current;

                            fieldList.Add(fi);
                            fieldOwnerList.Add(fieldOwner);
                        }
                    }
                }
                else
                {
                    fieldOwner = _setup;
                    fieldList.Add(fi);
                    fieldOwnerList.Add(fieldOwner);
                }
            }

            tick = Environment.TickCount - tick;
            long tick2 = Environment.TickCount;

            for (int i = 0; i < fieldList.Count; i++)
            {
                try
                {
                    fi = fieldList[i];
                    fieldOwner = fieldOwnerList[i];

                    if (fi.FieldType.Equals(typeof(byte)) || fi.FieldType.IsEnum)
                    {
                        byte fieldValue = (byte)fi.GetValue(fieldOwner);
                        byteList.Add(fieldValue);
                    }
                    else if (fi.FieldType.Equals(typeof(short)))
                    {
                        short fieldValue = (short)fi.GetValue(fieldOwner);
                        byteList.Add((byte)(fieldValue % 256));
                        byteList.Add((byte)(fieldValue / 256));
                    }
                    else if (fi.FieldType.Equals(typeof(ushort)))
                    {
                        ushort fieldValue = (ushort)fi.GetValue(fieldOwner);
                        byteList.Add((byte)(fieldValue % 256));
                        byteList.Add((byte)(fieldValue / 256));
                    }
                    else if (fi.FieldType.Equals(typeof(int)))
                    {
                        int fieldValue = (int)fi.GetValue(fieldOwner);
                        byteList.Add((byte)(fieldValue % 256));
                        byteList.Add((byte)((fieldValue >> 8) & 0xFF));
                        byteList.Add((byte)((fieldValue >> 16) & 0xFF));
                        byteList.Add((byte)((fieldValue >> 24) & 0xFF));
                    }
                    else if (fi.FieldType.Equals(typeof(uint)))
                    {
                        uint fieldValue = (uint)fi.GetValue(fieldOwner);
                        byteList.Add((byte)(fieldValue % 256));
                        byteList.Add((byte)((fieldValue >> 8) & 0xFF));
                        byteList.Add((byte)((fieldValue >> 16) & 0xFF));
                        byteList.Add((byte)((fieldValue >> 24) & 0xFF));
                    }
                    else if (fi.FieldType.Equals(typeof(long)))
                    {
                        long fieldValue = (long)fi.GetValue(fieldOwner);
                        byteList.Add((byte)(fieldValue % 256));
                        byteList.Add((byte)((fieldValue >> 8) & 0xFF));
                        byteList.Add((byte)((fieldValue >> 16) & 0xFF));
                        byteList.Add((byte)((fieldValue >> 24) & 0xFF));

                        byteList.Add((byte)((fieldValue >> 32) & 0xFF));
                        byteList.Add((byte)((fieldValue >> 40) & 0xFF));
                        byteList.Add((byte)((fieldValue >> 48) & 0xFF));
                        byteList.Add((byte)((fieldValue >> 56) & 0xFF));
                    }
                    else if (fi.FieldType.Equals(typeof(ulong)))
                    {
                        ulong fieldValue = (ulong)fi.GetValue(fieldOwner);
                        byteList.Add((byte)(fieldValue % 256));
                        byteList.Add((byte)((fieldValue >> 8) & 0xFF));
                        byteList.Add((byte)((fieldValue >> 16) & 0xFF));
                        byteList.Add((byte)((fieldValue >> 24) & 0xFF));

                        byteList.Add((byte)((fieldValue >> 32) & 0xFF));
                        byteList.Add((byte)((fieldValue >> 40) & 0xFF));
                        byteList.Add((byte)((fieldValue >> 48) & 0xFF));
                        byteList.Add((byte)((fieldValue >> 56) & 0xFF));
                    }
                    else
                    {
                    }
                }
                catch// (Exception ex)
                {
                }
            }

            result = byteList.ToArray();

            tick2 = Environment.TickCount - tick2;

            return result;
        }

        #endregion

        #region Topsheet/print server communication - commented

        //private void TopSheetControl()
        //{
        //    TopSheetControlState = enumTopSheetControlStates.NotRunning;

        //    AddToLog("TopSheetControl started");

        //    while (IsStarted && _topSheetThread.ManagedThreadId == Thread.CurrentThread.ManagedThreadId)
        //    {
        //        try
        //        {
        //            if (_topSheetMain == null || !_topSheetMain.IsStarted)
        //            {
        //                    TopSheetControlState = enumTopSheetControlStates.Running;

        //                    AddToLog(true, LogLevels.Debug, "TopSheetControl running");
        //            }

        //            if (_topSheetMain != null && _topSheetMain.IsStarted)
        //            {
        //                if (_doReconnectTopSheet)
        //                {
        //                    _doReconnectTopSheet = false;
        //                    TopSheetControlStop();
        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            AddToLog(ex);//true, LogLevels.Warning, ex.ToString());
        //            TopSheetControlStop();
        //            Thread.Sleep(1000);
        //        }
        //        finally
        //        {
        //            Thread.Sleep(200);
        //        }
        //    }

        //    TopSheetControlStop();
        //    TopSheetControlState = enumTopSheetControlStates.NotRunning;
        //    AddToLog("TopSheetControl stopped");
        //}

        //private void TopSheetControlStop()
        //{
        //    if (_topSheetMain != null)
        //    {
        //        _topSheetMain.Stop();
        //        _topSheetMain = null;
        //    }
        //}

        //private void PrintServerSendBundleReleasedToStacker(byte stackerNumber, ushort bundleNumber)
        //{
        //    PrintServerCommandStruct cmd = new PrintServerCommandStruct();
        //    cmd.command = (byte)enumPrintServerCommand.tcpCommandBundleReleasedToStacker;
        //    //cmd.stackerNumber = stackerNumber;
        //    cmd.bundleNumber = bundleNumber;
        //    cmd.bundleIndex = _bundleIndexToTcpServer++;
        //    if (_bundleIndexToTcpServer < 20000)
        //    {
        //        _bundleIndexToTcpServer = 20000;
        //    }

        //    lock (_lockTelegramListPrintServer)
        //    {
        //        _printServerCommandList.Add(cmd);
        //    }
        //}

        //private void PrintServerSendProducedBundle(byte stackerNumber, ushort bundleNumber)
        //{
        //    PrintServerCommandStruct cmd = new PrintServerCommandStruct();
        //    cmd.command = (byte)enumPrintServerCommand.tcpCommandBundleProduced;
        //    //cmd.stackerNumber = stackerNumber;
        //    cmd.bundleNumber = bundleNumber;
        //    cmd.bundleIndex = _bundleIndexToTcpServer++;
        //    if (_bundleIndexToTcpServer < 20000)
        //    {
        //        _bundleIndexToTcpServer = 20000;
        //    }

        //    lock (_lockTelegramListPrintServer)
        //    {
        //        _printServerCommandList.Add(cmd);
        //    }
        //}

        //private void PrintServerControl()
        //{
        //    PrintServerConnectionState = enumPrintServerConnectionStates.Disconnected;
        //    int readLength = 7 + _statusStructSize;
        //    byte[] pingTelegram = Encoding.ASCII.GetBytes("<00000000000>");
        //    byte[] telegram;
        //    string sTelegram;
        //    int sleepCounter;
        //    //int printServerPort = 11000 +_gripperLineId * 10 + _stackerLineId;

        //    AddToLog("PrintServerControl started");


        //    while (IsStarted)
        //    {
        //        try
        //        {
        //            if (_tcpClientPrintServer == null || _tcpClientPrintServer.Client == null || !_tcpClientPrintServer.Client.Connected)
        //            {
        //                PrintServerConnectionState = enumPrintServerConnectionStates.Disconnected;

        //                //if (_serverAddress != null && _serverAddress.Length > 0 && printServerPort > 0)
        //                {
        //                    AddToLog(true, LogLevels.Debug, "PrintServer connecting ...");

        //                    if (_doReconnectPrintServer)
        //                    {
        //                        _doReconnectPrintServer = false;
        //                    }

        //                    _tcpClientPrintServer = new TcpClient();
        //                    _tcpClientPrintServer.ReceiveTimeout = 1000;
        //                    _tcpClientPrintServer.SendTimeout = 1000;

        //                    PrintServerConnectionState = enumPrintServerConnectionStates.Connecting;

        //                    //_tcpClientPrintServer.Connect(_serverAddress, printServerPort);

        //                    _networkStreamPrintServer = _tcpClientPrintServer.GetStream();
        //                    _networkStreamPrintServer.ReadTimeout = 1000;
        //                    _networkStreamPrintServer.WriteTimeout = 1000;

        //                    _binaryReaderPrintServer = new BinaryReader(_networkStreamPrintServer);
        //                    _binaryWriterPrintServer = new BinaryWriter(_networkStreamPrintServer);

        //                    PrintServerConnectionState = enumPrintServerConnectionStates.Connected;


        //                    //PrintServerCommandStruct printCommand = new PrintServerCommandStruct();
        //                    //printCommand.bundleIndex = 20000;
        //                    //printCommand.bundleNumber = 36208;
        //                    //printCommand.command = 3;
        //                    //_printServerCommandList.Add(printCommand);


        //                    AddToLog(true, LogLevels.Debug, "PrintServer connected");
        //                }
        //            }

        //            if (_tcpClientPrintServer != null && _tcpClientPrintServer.Client != null && _tcpClientPrintServer.Client.Connected)
        //            {
        //                lock (_lockTelegramListPrintServer)
        //                {
        //                    if (_printServerCommandList.Count > 0)
        //                    {
        //                        //<33620820000>
        //                        sTelegram = "<" + _printServerCommandList[0].command.ToString()
        //                            + _printServerCommandList[0].bundleNumber.ToString().PadLeft(5, '0')
        //                            + _printServerCommandList[0].bundleIndex.ToString().PadLeft(5, '0')
        //                            + ">";

        //                        telegram = Encoding.ASCII.GetBytes(sTelegram);
        //                        _printServerCommandList.RemoveAt(0);
        //                    }
        //                    else
        //                    {
        //                        telegram = pingTelegram;
        //                    }
        //                }

        //                _binaryWriterPrintServer.Write(telegram);
        //                _binaryWriterPrintServer.Flush();

        //                if (_doReconnectPrintServer)
        //                {
        //                    _doReconnectPrintServer = false;
        //                    PrintServerDisconnect();
        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            AddToLog(true, LogLevels.Warning, ex.ToString());
        //            PrintServerDisconnect();
        //        }
        //        finally
        //        {
        //            sleepCounter = 0;
        //            while (++sleepCounter < 20 && _printServerCommandList.Count == 0)
        //            {
        //                Thread.Sleep(50);
        //            }
        //        }
        //    }

        //    PrintServerDisconnect();
        //    PrintServerConnectionState = enumPrintServerConnectionStates.NotRunning;
        //    AddToLog("PrintServerControl stopped");
        //}

        //private void PrintServerDisconnect()
        //{
        //    AddToLog(true, LogLevels.Warning, "PrintServerDisconnect");

        //    PrintServerConnectionState = enumPrintServerConnectionStates.Disconnected;

        //    if (_binaryReaderPrintServer != null)
        //    {
        //        try
        //        {
        //            _binaryReaderPrintServer.Close();
        //            if (_binaryReaderPrintServer != null)
        //            {
        //                _binaryReaderPrintServer.Dispose();
        //            }
        //        }
        //        catch
        //        {
        //        }
        //        finally
        //        {
        //            _binaryReaderPrintServer = null;
        //        }
        //    }

        //    if (_binaryWriterPrintServer != null)
        //    {
        //        try
        //        {
        //            _binaryWriterPrintServer.Close();
        //            if (_binaryWriterPrintServer != null)
        //            {
        //                _binaryWriterPrintServer.Dispose();
        //            }
        //        }
        //        catch
        //        {
        //        }
        //        finally
        //        {
        //            _binaryWriterPrintServer = null;
        //        }
        //    }

        //    if (_networkStreamPrintServer != null)
        //    {
        //        try
        //        {
        //            _networkStreamPrintServer.Close();
        //            if (_networkStreamPrintServer != null)
        //            {
        //                _networkStreamPrintServer.Dispose();
        //            }
        //        }
        //        catch
        //        {
        //        }
        //        finally
        //        {
        //            _networkStreamPrintServer = null;
        //        }
        //    }

        //    if (_tcpClientPrintServer != null)
        //    {
        //        try
        //        {
        //            _tcpClientPrintServer.Close();
        //        }
        //        catch
        //        {
        //        }
        //        finally
        //        {
        //            _tcpClientPrintServer = null;
        //        }
        //    }
        //}

        #endregion

        #region Icp communication

        private void IcpControl()
        {
            try
            {
                IcpConnectionState = enumIcpConnectionStates.Disconnected;
                int readLength = _statusStructSize;
                BundleMessageStruct bundleMessage;

                AddToLog("IcpControl started");

                while (UniDevLib.StaticInfo.IsRunning &&
                    IsStarted && _icpStackerThread != null && _icpStackerThread.ManagedThreadId == Thread.CurrentThread.ManagedThreadId)
                {
                    try
                    {
                        try
                        {
                            if (_tcpClientIcpStacker == null || _tcpClientIcpStacker.Client == null || !_tcpClientIcpStacker.Client.Connected)
                            {
                                IcpConnectionState = enumIcpConnectionStates.Disconnected;
                                _statusReceived = false;
                                //_layerCommandsReceived = true;

                                //if (_setupReceived)// && _ioIpStacker != null && _ioIpStacker.Length > 0 && _ioPortStacker > 0)
                                {
                                    //AddToLog(true, LogLevels.Debug, "Icp connecting ...");

                                    if (_doReconnectIcpStacker)
                                    {
                                        _doReconnectIcpStacker = false;
                                    }

                                    _tcpClientIcpStacker = new TcpClient();
                                    _tcpClientIcpStacker.ReceiveTimeout = 1000;
                                    _tcpClientIcpStacker.SendTimeout = 1000;

                                    //_tcpClientIcp.LingerState = new LingerOption(true, 0);

                                    IcpConnectionState = enumIcpConnectionStates.Connecting;

                                    try
                                    {
                                        _tcpClientIcpStacker.Connect(_ioIpStacker, _ioPortStacker);

                                        _networkStreamIcpStacker = _tcpClientIcpStacker.GetStream();
                                        _networkStreamIcpStacker.ReadTimeout = 1000;
                                        _networkStreamIcpStacker.WriteTimeout = 1000;

                                        _binaryReaderIcpStacker = new BinaryReader(_networkStreamIcpStacker);
                                        _binaryWriterIcpStacker = new BinaryWriter(_networkStreamIcpStacker);

                                        IcpConnectionState = enumIcpConnectionStates.Connected;
                                        //IcpGetOrAddTelegramStacker(enumIcpTelegramTypes.SendSetup, null, true);

                                        AddToLog(true, LogLevels.Debug, _lineControlUI.GetLanguageText("Icp connected"));

                                        int bytesCleared = 0;
                                        try
                                        {
                                            while (_tcpClientIcpStacker.Client != null && _tcpClientIcpStacker.Client.Connected)
                                            {
                                                _binaryReaderIcpStacker.ReadByte();
                                                ++bytesCleared;
                                            }
                                        }
                                        catch
                                        {
                                        }

                                        if (bytesCleared > 0)
                                        {
                                            AddToLog(_lineControlUI.GetLanguageText("Icp buffer cleared - num bytes")
                                                + " = " + bytesCleared.ToString());
                                        }
                                    }
                                    catch
                                    {
                                        Thread.Sleep(3000);
                                    }
                                }
                            }

                            if (_tcpClientIcpStacker != null && _tcpClientIcpStacker.Client != null && _tcpClientIcpStacker.Client.Connected)
                            {
                                byte[] telegram;
                                //bool isLayerCommand = false;

                                lock (_lockTelegramListIcp)
                                {
                                    if (_telegramListIcpStacker.Count > 0)
                                    {
                                        telegram = _telegramListIcpStacker[0];
                                        _telegramListIcpStacker.RemoveAt(0);

                                        //if (telegram[3] == (byte)'C')
                                        //{
                                        //    isLayerCommand = true;
                                        //}
                                    }
                                    else
                                    {
                                        telegram = IcpGetOrAddTelegramStacker(enumIcpTelegramTypes.RequestStatus, null, false);
                                    }
                                }

                                //if (telegram[3] == (byte)'C')
                                //{
                                //    _layerCommandsReceived = true;
                                //}
                                _binaryWriterIcpStacker.Write(telegram);
                                _binaryWriterIcpStacker.Flush();

                                //byte btest = _binaryReader.ReadByte();

                                byte[] response = _binaryReaderIcpStacker.ReadBytes(readLength);
                                if (response.Length == readLength
                                    && response[0] == telegram[0]
                                    && response[1] == telegram[1]
                                    && response[2] == telegram[3]
                                    && response[readLength - 1] == 4)
                                {
                                    //lock (_lockStatus)
                                    {
                                        GCHandle gch = GCHandle.Alloc(response, GCHandleType.Pinned);
                                        try
                                        {
                                            _status = (StatusStruct)Marshal.PtrToStructure(gch.AddrOfPinnedObject(), typeof(StatusStruct));
                                        }
                                        finally
                                        {
                                            gch.Free();
                                        }
                                    }

                                    _statusReceived = true;
                                    //if (telegram[3] == (byte)'C')
                                    //{
                                    //    _layerCommandsReceived = true;
                                    //}

                                    _status.TelegramLength = (ushort)readLength;

                                    lock (_lockAddressSendQueue)
                                    {
                                        if (_addressSendQueue.Count > 0)
                                        {
                                            IcpGetOrAddTelegramStacker(enumIcpTelegramTypes.SendAddressList, _addressSendQueue.ToArray(), true);
                                            _addressSendQueue.Clear();
                                        }
                                    }

                                    //if (_status.NumGrippers > 0)
                                    //{
                                    //    int byteIndex = 0;
                                    //    int bitIndex = 0;
                                    //    byte gripperNumber = _status.FirstGripperNumber;

                                    //    for (int i = 0; i < _status.NumGrippers; i++)
                                    //    {
                                    //        GripperItem gripperItem = new GripperItem();
                                    //        gripperItem.GripperNumber = gripperNumber;
                                    //        //gripperItem.Copies = (byte)((_status.GripperMessage[byteIndex] >> bitIndex) & 1);
                                    //        lock (_lockGripperItemList)
                                    //        {
                                    //            _gripperItemList.Add(gripperItem);
                                    //        }

                                    //        ++bitIndex;
                                    //        if (bitIndex > 7)
                                    //        {
                                    //            bitIndex = 0;
                                    //            ++byteIndex;
                                    //        }

                                    //        if (gripperNumber < 255)
                                    //        {
                                    //            ++gripperNumber;
                                    //        }
                                    //        else
                                    //        {
                                    //            gripperNumber = 0;
                                    //        }
                                    //    }
                                    //}

                                    


                                    for (int i = 0; i < _status.BundleMessageCount; i++)
                                    {
                                        bundleMessage = _status.BundleMessage[i];
                                        //bundleMessage.BundleId += 10000;

                                        string sMessage = "Message from Line IO"
                                                + " - StackerNumber: " + bundleMessage.StackerNumber.ToString()
                                                + " - " + ((byte)bundleMessage.Status).ToString()
                                                + " - " + _lineControlUI.GetLanguageText(bundleMessage.Status.ToString())
                                                + " - Bundle: " + bundleMessage.BundleId.ToString()
                                                + " - Code: " + bundleMessage.ExtraInfo.ToString();

                                        if (bundleMessage.Status == enumMessageStatusTypes.RESET)
                                        {
                                            if (bundleMessage.StackerNumber > 0)
                                            {
                                                StackerList[bundleMessage.StackerNumber - 1].ResetFromStacker = true;
                                            }
                                        }

                                        if ((byte)bundleMessage.Status >= 50)
                                        {
                                            //if (bundleMessage.Status == enumMessageStatusTypes.STACKERCYCLETIMEERROR)
                                            //{
                                            //}
                                            AddToLog(true, LogLevels.Warning, sMessage);
                                        }
                                        else
                                        {
                                            AddToLog(sMessage);
                                        }

                                        if (bundleMessage.BundleId > 0)
                                        {
                                            switch (bundleMessage.Status)
                                            {
                                                case enumMessageStatusTypes.COUNTFIRST:
                                                    SetBundleStatus(enumBundleStatus.bundleStatusProducing, bundleMessage.StackerNumber, bundleMessage.BundleId, 0);
                                                    break;

                                                case enumMessageStatusTypes.ENTRANCEFIRST:
                                                //_lastBundleMessage = DateTime.Now;
                                                    SetBundleStatus(enumBundleStatus.bundleStatusProducing, bundleMessage.StackerNumber, bundleMessage.BundleId, 0);
                                                //SetBundleStatus(enumBundleStatus.bundleStatusInStacker, _setup.ProductionLineNumber, bundleMessage.BundleId, 0);
                                                    break;

                                                case enumMessageStatusTypes.ENTRANCELAST:
                                                    SetBundleStatus(enumBundleStatus.bundleStatusInStacker, bundleMessage.StackerNumber, bundleMessage.BundleId, 0);
                                                    break;

                                                //case enumMessageStatusTypes.BUNDLEINTABLE:
                                                //case enumMessageStatusTypes.COUNTLAST:
                                                //    SetBundleStatus(enumBundleStatus.bundleStatusInStacker, bundleMessage.StackerNumber, bundleMessage.BundleId, 0);
                                                //    break;

                                                case enumMessageStatusTypes.BUNDLEEJECTED:
                                                    SetBundleStatus(enumBundleStatus.bundleStatusAfterStacker, bundleMessage.StackerNumber, bundleMessage.BundleId, 0);
                                                    break;

                                                default:
                                                    if ((byte)bundleMessage.Status >= 50)
                                                    {
                                                        if (bundleMessage.Status == enumMessageStatusTypes.ERRORINK1)
                                                        {
                                                            SetBundleStatus(enumBundleStatus.bundleStatusInkFailed,
                                                                bundleMessage.StackerNumber,
                                                                bundleMessage.BundleId, 0);
                                                        }
                                                        else
                                                        {
                                                            SetBundleStatus(enumBundleStatus.bundleStatusStackerFailed,
                                                                bundleMessage.StackerNumber,
                                                                bundleMessage.BundleId, 0);
                                                        }
                                                    }
                                                    break;
                                                //case enumMessageStatusTypes.INCOMPLETETRANSFER:
                                                //case enumMessageStatusTypes.LAYERCONTROLABORT:
                                                //case enumMessageStatusTypes.LCCABORT:
                                                //case enumMessageStatusTypes.LOSTBEFOREEJECTED:
                                                //case enumMessageStatusTypes.LOSTBEFORESTACKER:
                                                //case enumMessageStatusTypes.LOSTINTERCEPTWINDOW:
                                                //case enumMessageStatusTypes.PRODUCTIONQUEUEFULL:
                                                //case enumMessageStatusTypes.STACKERABORT:
                                                //    SetBundleStatus(enumBundleStatus.bundleStatusStackerFailed,
                                                //        _setup.ProductionLineNumber,
                                                //        bundleMessage.BundleId, 0);
                                                //    break;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    IcpDisconnectStacker();
                                }

                                if (_doReconnectIcpStacker)
                                {
                                    _doReconnectIcpStacker = false;
                                    IcpDisconnectStacker();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            AddToLog(ex);//true, LogLevels.Warning, ex.ToString());
                            IcpDisconnectStacker();
                            Thread.Sleep(1000);
                        }
                        finally
                        {
                            Thread.Sleep(100);
                        }
                    }
                    catch
                    {
                    }
                }

                IcpDisconnectStacker();
                IcpConnectionState = enumIcpConnectionStates.NotRunning;
                AddToLog(_lineControlUI.GetLanguageText("IcpControl stopped"));
            }
            catch
            {
            }
        }

        private byte[] IcpGetOrAddTelegramStacker(enumIcpTelegramTypes icpTelegramType, object oParameters, bool doAddToList)
        {
            byte[] result = null;
            List<byte> byteList;
            int size;
            byte sizeL;
            byte sizeH;
            //CopyBufferStruct[] copyBuffer;
            LayerData layerData;
            byte[] bytes;
            byte stackerNumber;
            byte[] imajeTelegram;
            //byte pcStatus = _isPaused ? (byte)2 : _pcStatus;
            //AddressData[] addressList;
            List<AddressData> addressList;
            List<byte> addressByteList;

            byte pcStatus = PcStatusStacker;/* _pcStatusStacker;
            if (pcStatus == 1 && _setup != null && _setup.Mode == enumModes.PROGRAMMED)
            {
                if (_setup.IsPaused == 1)
                {
                    pcStatus = 3;
                }
                else if (_topSheetMain != null && !_topSheetMain.ReadyToReceive)
                {
                    pcStatus = 4;
                }
            }*/

            if (_telegramIdStacker < 99)
            {
                ++_telegramIdStacker;
            }
            else
            {
                _telegramIdStacker = 1;
            }

            switch (icpTelegramType)
            {
                case enumIcpTelegramTypes.RequestStatus://A
                    if (_telegramRequestStatus == null)
                    {
                        _telegramRequestStatus = new byte[] { SOH, _telegramIdStacker, pcStatus, (byte)'A', 7, 0, EOT };
                    }
                    else
                    {
                        _telegramRequestStatus[1] = _telegramIdStacker;
                        _telegramRequestStatus[2] = pcStatus;
                    }

                    result = _telegramRequestStatus;
                    break;

                case enumIcpTelegramTypes.SendSetup://B
                    if (_setup.Mode == enumModes.STANDARD)
                    {
                        _setup.IsPaused = 0;
                    }

                    bytes = GetSetupStructBytes();
                    size = 7 + bytes.Length;
                    sizeL = (byte)(size % 256);
                    sizeH = (byte)(size / 256);
                    byteList = new List<byte>();

                    byteList.Add(SOH);
                    byteList.Add(_telegramIdStacker);
                    byteList.Add(pcStatus);
                    byteList.Add((byte)'B');
                    byteList.Add(sizeL);
                    byteList.Add(sizeH);

                    byteList.AddRange(bytes);

                    byteList.Add(EOT);

                    result = byteList.ToArray();
                    break;

                case enumIcpTelegramTypes.SendLayerCommand://C
                    layerData = (LayerData)oParameters;
                    result = new byte[] {
                        SOH,
                        _telegramIdStacker,
                        pcStatus,
                        (byte)'C',
                        (byte)0,//set length low after
                        (byte)0,//set length high after
                        (byte)layerData.StackerNumber,
                        (byte)(layerData.BundleId % 256),
                        (byte)(layerData.BundleId / 256),
                        (byte)layerData.LayerNumber,
                        (byte)layerData.Flags,
                        (byte)layerData.NumCopiesInLayer,
                        //(byte)(layerData.NumCopiesInLayer % 256),
                        //(byte)(layerData.NumCopiesInLayer / 256),
                        (byte)(layerData.NumCopiesInBundle % 256),
                        (byte)(layerData.NumCopiesInBundle / 256),
                        (byte)layerData.NumLayersInBundle,
                        (byte)layerData.NumAddress,
                        EOT
                        };

                    result[4] = (byte)result.Length;
                    break;

                //case enumIcpTelegramTypes.SendLayerCommand://C
                //    layerData = (LayerData)oParameters;
                //    result = new byte[] {
                //        SOH,
                //        _telegramIdStacker,
                //        pcStatus,
                //        (byte)'C',
                //        (byte)0,//set length low after
                //        (byte)0,//set length high after
                //        (byte)(layerData.BundleId % 256),
                //        (byte)(layerData.BundleId / 256),
                //        (byte)layerData.LayerNumber,
                //        (byte)layerData.GripperNumber,
                //        (byte)layerData.Flags,
                //        (byte)(layerData.NumCopiesInLayer % 256),
                //        (byte)(layerData.NumCopiesInLayer / 256),
                //        (byte)(layerData.NumCopiesInBundle % 256),
                //        (byte)(layerData.NumCopiesInBundle / 256),
                //        (byte)layerData.NumLayersInBundle,
                //        EOT
                //        };

                //    result[4] = (byte)result.Length;
                //    break;

                /*case enumIcpTelegramTypes.SendBundleData:
                    copyBuffer = (CopyBufferStruct[])oParameters;
                    size = 8 + _copyStructSize * copyBuffer.Length;
                    sizeL = (byte)(size % 256);
                    sizeH = (byte)(size / 256);
                    byteList = new List<byte>();

                    byteList.Add(SOH);
                    byteList.Add(_telegramId);
                    byteList.Add(_pcStatus);
                    byteList.Add((byte)'C');
                    byteList.Add(sizeL);
                    byteList.Add(sizeH);

                    byteList.Add((byte)copyBuffer.Length);

                    for (int i = 0; i < copyBuffer.Length; i++)
                    {
                        bytes = GetCopyStructBytes(copyBuffer[i]);
                        byteList.AddRange(bytes);
                    }

                    byteList.Add(EOT);

                    result = byteList.ToArray();
                    break;*/

                case enumIcpTelegramTypes.StartProduction://D
                    if (_telegramStartProduction == null)
                    {
                        _telegramStartProduction = new byte[] { SOH, _telegramIdStacker, pcStatus, (byte)'D', 7, 0, EOT };
                    }
                    else
                    {
                        _telegramStartProduction[1] = _telegramIdStacker;
                        _telegramStartProduction[2] = pcStatus;
                    }

                    result = _telegramStartProduction;
                    break;

                case enumIcpTelegramTypes.StopProduction://E
                    if (_telegramStopProduction == null)
                    {
                        _telegramStopProduction = new byte[] { SOH, _telegramIdStacker, pcStatus, (byte)'E', 7, 0, EOT };
                    }
                    else
                    {
                        _telegramStopProduction[1] = _telegramIdStacker;
                        _telegramStopProduction[2] = pcStatus;
                    }

                    result = _telegramStopProduction;
                    break;

                case enumIcpTelegramTypes.Reset://F
                    stackerNumber = (byte)oParameters;
                    if (_telegramReset == null)
                    {
                        _telegramReset = new byte[] { SOH, _telegramIdStacker, pcStatus, (byte)'F', 8, 0, stackerNumber, EOT };
                    }
                    else
                    {
                        _telegramReset[1] = _telegramIdStacker;
                        _telegramReset[2] = pcStatus;
                    }

                    result = _telegramReset;
                    break;

                case enumIcpTelegramTypes.ClearCounters://G
                    if (_telegramClearCounters == null)
                    {
                        _telegramClearCounters = new byte[] { SOH, _telegramIdStacker, pcStatus, (byte)'G', 7, 0, EOT };
                    }
                    else
                    {
                        _telegramClearCounters[1] = _telegramIdStacker;
                        _telegramClearCounters[2] = pcStatus;
                    }

                    result = _telegramClearCounters;
                    break;


                //case enumIcpTelegramTypes.SendImajeSetup://H
                //    imajeTelegram = GetImajeSetupTelegram(
                //        _setup.GripperLines.InvertMessage == 1, _setup.GripperLines.InvertHorizontal == 1,
                //        _setup.GripperLines.InvertVertical == 1, _setup.GripperLines.TacoMeterDivision);
                //    result = new byte[imajeTelegram.Length + 7];
                //    result[0] = SOH;
                //    result[1] = _telegramIdStacker;
                //    result[2] = pcStatus;
                //    result[3] = (byte)'H';
                //    result[4] = (byte)(result.Length >> 8);
                //    result[5] = (byte)(result.Length & 0xFF);
                //    Array.Copy(imajeTelegram, 0, result, 6, imajeTelegram.Length);
                //    result[result.Length - 1] = EOT;

                //    break;

                case enumIcpTelegramTypes.SendAddressList://I
                    //addressList = (AddressData[])oParameters;
                    addressList = (List<AddressData>) (oParameters);
                    addressByteList = new List<byte>();

                    addressByteList.Add((byte)addressList.Count);
                    for (int i = 0; i < addressList.Count; i++)
                    {
                        AddressData address = addressList[i];
                        byte addressWidth;
                        imajeTelegram = AdressToStringImajeLinesWithoutLibary(
                            address.AddressLine1, address.AddressLine2, address.AddressLine3, address.AddressLine4, out addressWidth);

                        addressByteList.Add(address.State);
                        addressByteList.Add(address.StackerNumber);
                        //addressByteList.Add(address.AddressID);
                        addressByteList.Add(address.LayerID);
                        addressByteList.Add((byte)(address.BundlID & 0xff));
                        addressByteList.Add((byte)((address.BundlID >> 8) & 0xff));
                        addressByteList.Add(addressWidth);
                        addressByteList.Add((byte)imajeTelegram.Length);
                        addressByteList.AddRange(imajeTelegram);
                    }

                    result = new byte[addressByteList.Count + 8];
                    result[0] = SOH;
                    result[1] = _telegramIdStacker;
                    result[2] = pcStatus;
                    result[3] = (byte)'I';
                    result[4] = (byte)(result.Length & 0xFF);
                    result[5] = (byte)(result.Length >> 8);
                    result[6] = (byte)(addressList.Count);
                    Array.Copy(addressByteList.ToArray(), 0, result, 6, addressByteList.Count);
                    result[result.Length - 1] = EOT;
                    break;

                case enumIcpTelegramTypes.TestImajePrint: //J
                    AddressData testAddress = new AddressData();
                    testAddress.AddressLine1 = "Industriveien 1";
                    testAddress.AddressLine2 = "Realcom AS";
                    testAddress.AddressLine3 = "Skedsmokorset";
                    testAddress.AddressLine4 = "2020";
                    testAddress.BundlID = 10001;
                    //addressList = new AddressData[] { testAddress };
                    addressList = new List<AddressData>() { testAddress };
                    addressByteList = new List<byte>();

                    addressByteList.Add((byte)addressList.Count);
                    for (int i = 0; i < addressList.Count; i++)
                    {
                        AddressData address = addressList[i];
                        byte addressWidth;
                        imajeTelegram = AdressToStringImajeLinesWithoutLibary(
                            address.AddressLine1, address.AddressLine2, address.AddressLine3, address.AddressLine4, out addressWidth);

                        addressByteList.Add(address.State);
                        //addressByteList.Add(address.GripperNr);
                        //addressByteList.Add(address.AddressID);
                        addressByteList.Add(address.LayerID);
                        addressByteList.Add((byte)(address.BundlID & 0xff));
                        addressByteList.Add((byte)((address.BundlID >> 8) & 0xff));
                        addressByteList.Add(addressWidth);
                        addressByteList.Add((byte)imajeTelegram.Length);
                        addressByteList.AddRange(imajeTelegram);
                    }

                    result = new byte[addressByteList.Count + 7];
                    result[0] = SOH;
                    result[1] = _telegramIdStacker;
                    result[2] = pcStatus;
                    result[3] = (byte)'J';
                    result[4] = (byte)(result.Length & 0xFF);
                    result[5] = (byte)(result.Length >> 8);
                    Array.Copy(addressByteList.ToArray(), 0, result, 6, addressByteList.Count);
                    result[result.Length - 1] = EOT;
                    break;

                case enumIcpTelegramTypes.StartStopSimulation://S
                    if (_telegramStartStopSimulation == null)
                    {
                        _telegramStartStopSimulation = new byte[] { SOH, _telegramIdStacker, pcStatus, (byte)'S', 7, 0, EOT };
                    }
                    else
                    {
                        _telegramStartStopSimulation[1] = _telegramIdStacker;
                        _telegramStartStopSimulation[2] = pcStatus;
                    }

                    result = _telegramStartStopSimulation;
                    break;

            }

            if (doAddToList)
            {
                IcpAddTelegramStacker(result);
            }

            return result;
        }

        private void IcpAddTelegramStacker(byte[] telegram)
        {
            if (telegram != null && telegram.Length > 0)
            {
                //if (telegram[3] == (byte)'B')
                //{
                //}
                lock (_lockTelegramListIcp)
                {
                    _telegramListIcpStacker.Add(telegram);
                }
            }
        }

        private void IcpDisconnectStacker()
        {
            AddToLog(true, LogLevels.Warning, _lineControlUI.GetLanguageText("IcpDisconnect"));

            IcpConnectionState = enumIcpConnectionStates.Disconnected;

            _telegramListIcpStacker.Clear();
            //_doReconnectDbLocal = true;
            _setupReceived = false;
            try
            {
                if (_conServer != null && _conServer.State == ConnectionState.Open)
                {
                    DbGetSetup();
                }
            }
            catch
            {
            }

            if (_binaryReaderIcpStacker != null)
            {
                try
                {
                    _binaryReaderIcpStacker.Close();
                    _binaryReaderIcpStacker.Dispose();
                }
                catch
                {
                }
                finally
                {
                    _binaryReaderIcpStacker = null;
                }
            }

            if (_binaryWriterIcpStacker != null)
            {
                try
                {
                    _binaryWriterIcpStacker.Close();
                    _binaryWriterIcpStacker.Dispose();
                }
                catch
                {
                }
                finally
                {
                    _binaryWriterIcpStacker = null;
                }
            }

            if (_networkStreamIcpStacker != null)
            {
                try
                {
                    _networkStreamIcpStacker.Close();
                    _networkStreamIcpStacker.Dispose();
                }
                catch
                {
                }
                finally
                {
                    _networkStreamIcpStacker = null;
                }
            }

            if (_tcpClientIcpStacker != null)
            {
                try
                {
                    _tcpClientIcpStacker.Close();
                }
                catch
                {
                }
                finally
                {
                    _tcpClientIcpStacker = null;
                }
            }
        }

        #region Ink jet (Imaje)


        #region SendImajeSetup

        public bool SendImajeSetup()
        {
            bool result = false;

            if (IcpConnectionState == enumIcpConnectionStates.Connected && SetupLine.IsInkEnable == 1)// _deactivateInkPrint)// && !_deactivateInkSendSetup)
            {
                IcpGetOrAddTelegramStacker(enumIcpTelegramTypes.SendImajeSetup, null, true);

                result = true;
            }

            return result;
        }

        #endregion

        #region Imaje converting

        public byte[] AdressToStringImajeLinesWithoutLibary(string Line1, string Line2, string Line3, string Line4,
            out byte paperAddressWidth)
        {
            byte[] result = null;
            paperAddressWidth = 0;
            int jetNumber = 1;
            string sMessage = "";
            string sMessageLinje12;
            string sMessageLinje34;
            string sLength;
            string sData;
            string sCheckSum;
            string sId;
            //char checks2;

            try
            {
                //if (_setup.GripperLines.InvertUpDown == 1)
                //{
                    string temp;
                    temp = Line1; Line1 = Line2; Line2 = temp; //exchange line 1 and 2
                    temp = Line3; Line3 = Line4; Line4 = temp; //exchange line 3 and 4
                //}

                Line1 = Line1.ToUpper().Trim();
                Line2 = Line2.ToUpper().Trim();
                Line3 = Line3.ToUpper().Trim();
                Line4 = Line4.ToUpper().Trim();

                //Line1 = Line1.PadRight(30, ' ').Substring(0, 30);
                //Line2 = Line2.PadRight(30, ' ').Substring(0, 30);
                //Line3 = Line3.PadRight(30, ' ').Substring(0, 30);
                //Line4 = Line4.PadRight(30, ' ').Substring(0, 30);

                int maxLength = 0;
                //int currentInsertLineNumber = 1;
                //int currentLineNumber = 0;
                //while (++currentLineNumber <= 4)
                //{
                //    string line = "";
                //    switch (currentLineNumber)
                //    {
                //        case 1: line = Line1; break;
                //        case 2: line = Line2; break;
                //        case 3: line = Line3; break;
                //        case 4: line = Line4; break;
                //    }

                //    if (line.Length > 0)
                //    {
                //        maxLength = Math.Max(line.Length, maxLength);
                //        switch (currentInsertLineNumber)
                //        {
                //            case 1: Line1 = line; break;
                //            case 2: Line2 = line; break;
                //            case 3: Line3 = line; break;
                //            case 4: Line4 = line; break;
                //        }
                //        currentInsertLineNumber++;
                //    }
                //}


                maxLength = Math.Max(Line1.Length, Line2.Length);
                maxLength = Math.Max(Line3.Length, maxLength);
                maxLength = Math.Max(Line4.Length, maxLength);

                if (maxLength > 31)
                {
                    maxLength = 31;
                }

                for (int i = 1; i <= 4; i++)
                {
                    switch (i)
                    {
                        case 1: Line1 = Line1.PadRight(maxLength, ' ').Substring(0, maxLength); break;
                        case 2: Line2 = Line2.PadRight(maxLength, ' ').Substring(0, maxLength); break;
                        case 3: Line3 = Line3.PadRight(maxLength, ' ').Substring(0, maxLength); break;
                        case 4: Line4 = Line4.PadRight(maxLength, ' ').Substring(0, maxLength); break;
                    }
                }

                //for (int i = currentInsertLineNumber; i <= 4; i++)
                //{
                //    switch (i)
                //    {
                //        case 1: Line1 = ""; break;
                //        case 2: Line2 = ""; break;
                //        case 3: Line3 = ""; break;
                //        case 4: Line4 = ""; break;
                //    }
                //}

                //Line1 = Line1.PadRight(29, ' ').Substring(0, 29).ToUpper() + "0";
                //Line2 = Line2.PadRight(29, ' ').Substring(0, 29).ToUpper() + "0";
                //Line3 = Line3.PadRight(29, ' ').Substring(0, 29).ToUpper() + "0";
                //Line4 = Line4.PadRight(29, ' ').Substring(0, 29).ToUpper() + "0";

                sId = ((char)0xA).ToString();
                sData = ((char)jetNumber).ToString() +
                    (char)0xa + ((char)0x1).ToString() + ((char)0x2C).ToString() +
                    ReplaceSpecialCharacters(Line1) + (char)0xa + ((char)0x1).ToString() +
                    ((char)0x2C).ToString() + ReplaceSpecialCharacters(Line2) + (char)0xD;
                sLength = UniDevLib.DataConverting.StringConverting.ConvertIntToBinaryString(sData.Length, 2);
                sCheckSum = CalculateCheckSum(sId + sLength + sData).ToString();
                //checks2 = Convert.ToChar(sCheckSum); //CalculateCheckSum(sId + sLength + sData);
                sMessageLinje12 = sId + sLength + sData + sCheckSum;
                //sMessageLinje12 = ((char)(byte)sMessageLinje12.Length).ToString() + sMessageLinje12;

                jetNumber++;

                //if (Line3.Length > 0 || Line4.Length > 0)
                {
                    sId = ((char)0xA).ToString();
                    sData = ((char)jetNumber).ToString() +
                        (char)0xa + ((char)0x1).ToString() + ((char)0x2C).ToString() +
                        ReplaceSpecialCharacters(Line3) + (char)0xa + ((char)0x1).ToString() +
                        ((char)0x2C).ToString() + ReplaceSpecialCharacters(Line4) + (char)0xD;
                    sLength = UniDevLib.DataConverting.StringConverting.ConvertIntToBinaryString(sData.Length, 2);
                    sCheckSum = CalculateCheckSum(sId + sLength + sData).ToString();
                    //checks2 = Convert.ToChar(sCheckSum); //CalculateCheckSum(sId + sLength + sData);
                    sMessageLinje34 = sId + sLength + sData + sCheckSum;
                }
                sMessage = sMessageLinje12 + sMessageLinje34;// +sMessageLinje56 + sMessageLinje78;
                paperAddressWidth = (byte)maxLength;
            }
            catch (Exception ex)
            {
                AddToLog(ex);
            }

            result = new byte[sMessage.Length];
            for (int i = 0; i < sMessage.Length; i++)
            {
                result[i] = (byte)sMessage[i];
            }

            return result;
        }

        public string ReplaceSpecialCharacters(string sData)
        {
            string sBuffer = null;

            try
            {
                sBuffer = sData;
                sBuffer = sBuffer.Replace('Æ', (char)146);
                sBuffer = sBuffer.Replace('Ø', (char)158);
                sBuffer = sBuffer.Replace('Å', (char)143);
                sBuffer = sBuffer.Replace('æ', (char)145);
                sBuffer = sBuffer.Replace('ø', (char)155);
                sBuffer = sBuffer.Replace('å', (char)134);

                bool isChange = false;
                char[] buffer = sBuffer.ToCharArray();
                for (int i = 0; i < buffer.Length; i++)
                {
                    byte b = (byte)buffer[i];
                    if (b < 32 || (b > 126
                        && b != 146 && b != 158 && b != 143 && b != 145 && b != 155 && b != 134))
                    {
                        buffer[i] = '?';
                        isChange = true;
                    }
                }

                if (isChange)
                {
                    AddToLog("Unrecognized characters in string replaced with '?': " + sData);
                    sBuffer = new string(buffer);
                }
            }
            catch (Exception ex)
            {
                AddToLog(ex);
            }

            return sBuffer;

        }

        private char CalculateCheckSum(string sData)
        {
            char cReturnChar = '\0';
            try
            {
                for (int ix = 0; ix < sData.Length; ix++)
                {
                    cReturnChar = (char)(cReturnChar ^ sData[ix]);
                }
            }
            catch
            {

            }
            return cReturnChar;
        }

        private byte CalculateCheckSum(byte[] bData)
        {
            byte result = 0;
            try
            {
                for (int ix = 0; ix < bData.Length; ix++)
                {
                    result = (byte)(result ^ bData[ix]);
                }
            }
            catch
            {

            }
            return result;
        }

        private byte[] GetImajeSetupTelegram(bool doInvertMessage, bool doInvertHorizontal,
            bool doInvertVertical, UInt16 tacoMeterDivision)
        {
            byte[] result;

            byte generalFirstByte = 16;//bit4 = tachometer mode

            if (!doInvertMessage)
            {
                generalFirstByte |= 128;
            }
            if (!doInvertHorizontal)
            {
                generalFirstByte |= 64;
            }
            if (!doInvertVertical)
            {
                generalFirstByte |= 32;
            }
            //System.Windows.MessageBox.Show("Setup sent:" + generalFirstByte.ToString());

            byte tacoMeterDivisionHigh = (byte)(tacoMeterDivision >> 8);
            byte tacoMeterDivisionLow = (byte)(tacoMeterDivision & 0xFF);

            byte variableFirstByte = 1;//bit3-0 = Number of figures printed
            byte b0 = (byte)'0';
            byte b1 = (byte)'1';

            List<byte> jetByteList = new List<byte>();
            jetByteList.Add(0x1B);//esc
            jetByteList.Add(0x00);//length high
            jetByteList.Add(0x26);//length low
            byte[] messageData = new byte[38] {//38 = 0x26
                //Jet number:
                0x01,

                //Message general:
                generalFirstByte,
                tacoMeterDivisionHigh,//Printing speed/ tachometer division high
                tacoMeterDivisionLow,//Printing speed/ tachometer division low
                0x00,//Forward margin mm high
                0x01,//Forward margin mm low
                0x00,//Return margin mm high
                0x01,//Return margin mm low
                0x00,//Interval mm high
                0x01,//Interval mm low
                0x01,//Object top filter high
                0x00,//Object top filter low
                
                //Variable elements:
                variableFirstByte,
                b0, b0, b0, b0, b0, b0, b0, b0, b0,//Initial value ascii "000000000"
                b0, b0, b0, b0, b0, b0, b0, b0, b0,//Final value ascii "000000000"
                b0, b1,//Step counter ascii "01"
                0x00, 0x00, 0x01,//Lot counter
                0x00, 0x00//Postdate interval
                };

            jetByteList.AddRange(messageData);

            byte[] bJetBytes = jetByteList.ToArray();
            byte controlByte = CalculateCheckSum(bJetBytes);

            List<byte> byteList = new List<byte>();
            byteList.AddRange(bJetBytes);
            byteList.Add(controlByte);

            bJetBytes[3] = 0x02;
            controlByte = CalculateCheckSum(bJetBytes);
            byteList.AddRange(bJetBytes);
            byteList.Add(controlByte);

            //bJetBytes[3] = 0x03;
            //controlByte = CalculateCheckSum(bJetBytes);
            //byteList.AddRange(bJetBytes);
            //byteList.Add(controlByte);

            //bJetBytes[3] = 0x04;
            //controlByte = CalculateCheckSum(bJetBytes);
            //byteList.AddRange(bJetBytes);
            //byteList.Add(controlByte);

            result = byteList.ToArray();

            return result;
        }

        /*
        private byte[] GetImajePrintAckTelegram()
        {
            byte[] result;

            List<byte> jetByteList = new List<byte>();
            jetByteList.Add(0x41);//identification
            jetByteList.Add(0x00);//length high
            jetByteList.Add(0x02);//length low
            jetByteList.Add(0x01);//Jet number
            jetByteList.Add((byte)226);//0x80 = 128 (bit 7 ack for each object printed)

            byte[] bJetBytes = jetByteList.ToArray();
            byte controlByte = CalculateCheckSum(bJetBytes);

            List<byte> byteList = new List<byte>();
            byteList.AddRange(bJetBytes);
            byteList.Add(controlByte);

            bJetBytes[3] = 0x02;
            controlByte = CalculateCheckSum(bJetBytes);
            byteList.AddRange(bJetBytes);
            byteList.Add(controlByte);

            bJetBytes[3] = 0x03;
            controlByte = CalculateCheckSum(bJetBytes);
            byteList.AddRange(bJetBytes);
            byteList.Add(controlByte);

            bJetBytes[3] = 0x04;
            controlByte = CalculateCheckSum(bJetBytes);
            byteList.AddRange(bJetBytes);
            byteList.Add(controlByte);

            result = byteList.ToArray();

            return result;
        }
        */

        #endregion

        #endregion

        #endregion

        #region Database communication

        private void DbControlServer()
        {
            string sql;
            //OleDbCommand cmd;
            bool ignoreCommand;
            bool doSleepAfterCommand;
            DbCommandStruct dbCommand;
            enumDataBaseCommands currentCommand;
            //uint currentBundleIndex;
            byte sleepCounter;
            DateTime lastRequestCommand = DateTime.MinValue;
            bool isFirstBundleRequest = true;
            bool wasIcpOk = true;
            //bool isLanguageUpdated = false;
            //int maxBundles = 10;

            Thread.Sleep(2000);

            AddToLog("DbControlServer started");

            while (UniDevLib.StaticInfo.IsRunning &&
                IsStarted && _dbThreadServer != null && _dbThreadServer.ManagedThreadId == Thread.CurrentThread.ManagedThreadId)
            {
                try
                {
                    if (IcpConnectionState != enumIcpConnectionStates.Connected || _status.ActiveState != 1 || _setup.IsPaused == 1)
                    {
                        if (wasIcpOk)
                        {
                            isFirstBundleRequest = true;
                        }

                        wasIcpOk = false;
                    }
                    else
                    {
                        wasIcpOk = false;
                    }

                    if (_conServer == null || _conServer.State != System.Data.ConnectionState.Open)
                    {
                        DbConnectionStateServer = enumDbConnectionStates.DbDisconnected;

                        if (_connectionStringServer != null && _connectionStringServer.Length > 0)
                        {
                            _conServer = new OleDbConnection(_connectionStringServer);

                            DbConnectionStateServer = enumDbConnectionStates.DbConnecting;

                            _conServer.Open();

                            if (_conServer != null && _conServer.State == System.Data.ConnectionState.Open)
                            {
                                DbConnectionStateServer = enumDbConnectionStates.DbConnected;
                                AddToLog("DbControlServer connected");

                                //_setupReceived = false;
                                //_doReconnectIcpStacker = true;

                                //DbGetSetup();
                            }
                        }
                    }

                    if (_conServer != null && _conServer.State == System.Data.ConnectionState.Open)
                    {
                        sql = null;
                        ignoreCommand = true;
                        doSleepAfterCommand = true;
                        currentCommand = enumDataBaseCommands.dbCommNone;
                        int commandListCount;

                        //lock (_lockStatus)
                        {
                            lock (_lockDbCommandListServer)
                            {
                                commandListCount = _dbCommandListServer.Count;
                            }

                            if (lastRequestCommand < DateTime.Now.AddMilliseconds(-1000))
                            //    || (_dbCommandList.Count == 0 && _newProductionQueue.Count == 0))
                            {
                                lastRequestCommand = DateTime.Now;

                                sql = string.Format("DECLARE @return_value int; EXEC @return_value = [dbo].[LCCGetNextCommand]"
                                    + " @LineNumber = {0}, @CopiesPrHour = {1};SELECT 'Return Value' = @return_value",
                                    _setup.ProductionLineNumber, CopiesPrHour);
                                currentCommand = enumDataBaseCommands.dbCommGetNextCommand;

                                ignoreCommand = false;
                            }
                            //else if (commandListCount == 0 &&
                            //    _conLocal != null && _conLocal.State == ConnectionState.Open && !isLanguageUpdated)
                            //{
                            //    isLanguageUpdated = true;
                            //    RequestLanguageLoadFromServer();
                            //}
                            else
                            {
                                lock (_lockDbCommandListServer)
                                {
                                    if (_dbCommandListServer.Count > 0)
                                    {
                                        dbCommand = _dbCommandListServer[0];
                                        _dbCommandListServer.RemoveAt(0);
                                        ignoreCommand = false;
                                        doSleepAfterCommand = false;

                                        currentCommand = dbCommand.Command;

                                        switch (currentCommand)
                                        {
                                            case enumDataBaseCommands.dbCommGetSetup:
                                                sql = "select * from StackerSetupTable order by FieldName";
                                                break;

                                            case enumDataBaseCommands.dbCommGetBundleList:
                                                if (IcpConnectionState == enumIcpConnectionStates.Connected && _status.ActiveState == 1 && _setup.IsPaused == 0)
                                                {
                                                    ushort maxBundles = dbCommand.BundleNumber;
                                                    byte isFirst = (byte)(isFirstBundleRequest ? 1 : 0);
                                                    byte stackerNumber = dbCommand.StackerNumber;
                                                    if (stackerNumber > 0)
                                                    {
                                                        if (!_newBundlesReceived[stackerNumber - 1] && _newProductionQueue[stackerNumber-1].Count == 0)
                                                        {
                                                            isFirstBundleRequest = false;
                                                            sql = string.Format("EXEC [dbo].[GetBundlesToLineControl] {0}, {1}, {2}",// order by SortIndex, BundleId desc",
                                                                stackerNumber, maxBundles, isFirst);                                                      
                                                        }
                                                    }
                                                }
                                                break;
                                            case enumDataBaseCommands.dbCommSetLineStatus:
                                                //ignoreCommand = true;
                                                sql = string.Format("exec LCCSetLineStatus {0}, {1}",
                                                    _setup.ProductionLineNumber, dbCommand.Status);
                                                break;
                                            case enumDataBaseCommands.dbCommSetBundleStatus:
                                                //ignoreCommand = true;
                                                sql = string.Format("exec LCCSetBundleStatus {0}, {1}, {2}, {3}, {4}",
                                                    _setup.ProductionLineNumber, dbCommand.StackerNumber, dbCommand.BundleNumber,
                                                    dbCommand.Status, dbCommand.CopyNumber);
                                                break;
                                            case enumDataBaseCommands.dbCommSetInkStatus:
                                                //ignoreCommand = true;
                                                sql = string.Format("exec LCCSetInkStatus {0}, {1}, {2}",
                                                    _setup.ProductionLineNumber, dbCommand.StackerNumber, dbCommand.Status);
                                                break;
                                            case enumDataBaseCommands.dbCommSetStackerStatus:
                                                //ignoreCommand = true;
                                                sql = string.Format("exec LCCSetStackerStatus {0}, {1}, {2}",
                                                    _setup.ProductionLineNumber, dbCommand.StackerNumber, dbCommand.Status);
                                                break;
                                            case enumDataBaseCommands.dbCommSetTopSheetApplicatorStatus:
                                                //ignoreCommand = true;
                                                sql = string.Format("exec LCCSetApplicatorStatus {0}, {1}, {2}",
                                                    _setup.ProductionLineNumber, dbCommand.StackerNumber, dbCommand.Status);
                                                break;
                                            case enumDataBaseCommands.dbCommSetTopSheetPrinterStatus:
                                                //ignoreCommand = true;
                                                sql = string.Format("exec LCCSetPrinterStatus {0}, {1}, {2}, {3}",
                                                    _setup.ProductionLineNumber, dbCommand.StackerNumber,
                                                    dbCommand.TableId, dbCommand.Status
                                                    );
                                                break;//
                                            case enumDataBaseCommands.dbCommSetNextMachineStatus:
                                                //ignoreCommand = true;
                                                sql = string.Format("exec LCCSetNextMachineStatus {0}, {1}, {2}",
                                                    _setup.ProductionLineNumber, dbCommand.StackerNumber, dbCommand.Status);
                                                break;
                                        }
                                    }
                                }
                            }
                        }

                        if (sql != null && !ignoreCommand && currentCommand != enumDataBaseCommands.dbCommNone)
                        {
                            int tickCount = Environment.TickCount;
                            if (IsStarted)
                            {
                                DbHandleCommand(_conServer, sql, currentCommand);
                            }
                            tickCount = Environment.TickCount - tickCount;
                            _dbProcessTicksServer.Add(tickCount);
                            if (_dbProcessTicksServer.Count > 5)
                            {
                                _dbProcessTicksServer.RemoveAt(0);
                            }
                        }

                        sleepCounter = 0;
                        do
                        {
                            Thread.Sleep(50);
                            lock (_lockDbCommandListServer)
                            {
                                commandListCount = _dbCommandListServer.Count;
                            }
                        }
                        while (doSleepAfterCommand && ++sleepCounter < 20 && commandListCount == 0);

                        if (_doReconnectDbServer)
                        {
                            _doReconnectDbServer = false;
                            DbDisconnectServer();
                        }
                    }
                }
                catch (Exception ex)
                {
                    AddToLog(ex);
                    Thread.Sleep(1000);
                    DbDisconnectServer();
                }
                finally
                {
                    Thread.Sleep(10);
                }
            }

            if (_dbThreadServer.ManagedThreadId != Thread.CurrentThread.ManagedThreadId)
            {
                AddToLog(true, LogLevels.Debug, "DbControlServer old thread exited");
            }
            else
            {
                DbDisconnectServer();
                AddToLog("DbControlServer stopped");
                DbConnectionStateServer = enumDbConnectionStates.DbUnknown;
            }
        }

        private bool DbHandleCommand(OleDbConnection con, string sql, enumDataBaseCommands currentCommand)
        {
            bool result = false;

            OleDbCommand cmd;
            OleDbDataReader dr;
            object[] oValues;
            List<object[]> oValuesList;
            uint firstBundleId;
            uint bundleCount;
            int rowIndex;
            int iResult1;
            byte stackerNumber;
            byte subCommand;
            //uint lastReproduceTableId;
            //int tmpIndex;
            //bool isFound;
            //int bundleId;
            //int bundleId2;
            //AddressData addressData;
            //int listCount;

            try
            {
                if (con != null && con.State == ConnectionState.Open)
                {
                    cmd = new OleDbCommand(sql, con);
                    dr = DBTools.HandleBusyExecuteReader(cmd);
                    //dr = cmd.ExecuteReader();
                    oValuesList = new List<object[]>();
                    while (dr.Read())
                    {
                        oValues = new object[dr.FieldCount];
                        dr.GetValues(oValues);
                        oValuesList.Add(oValues);
                    }
                    dr.Close();

                    switch (currentCommand)
                    {
                        case enumDataBaseCommands.dbCommGetSetup:
                            long tick = Environment.TickCount;
                            int oldLine = _setup.ProductionLineNumber;
                            int oldGL = _setup.CurrentGLNumber;
                            _setup = new SetupStruct();
                            _setup.GripperLines = new GripperLineSetupStruct(); //[MAXGRIPPERLINES];
                            //for (int i = 0; i < _setup.GripperLines.Length; i++)
                            //{
                            //    _setup.GripperLines[i] = new GripperLineSetupStruct();
                            //}

                            _setupItemList.Clear();
                            //_setupItemList.Add(new SetupItem(null, null, "Settings update: " + DateTime.Now.ToString("d'/'M HH:mm:ss")));
                            //_setupItemList.Add(new SetupItem(null, null, ""));


                            for (int i = 0; i < oValuesList.Count; i++)
                            {
                                try
                                {
                                    string stackerId = (string)oValuesList[i][0];
                                    if (i == 0)
                                    {
                                        _stackerId = stackerId;
                                    }

                                    string fieldNameLong = (string)oValuesList[i][1];
                                    string fieldName = fieldNameLong;
                                    object fieldValue = oValuesList[i][2];
                                    object fieldValueToICP;
                                    object fieldValueMin = oValuesList[i][3];
                                    object fieldValueMax = oValuesList[i][4];
                                    string fieldTextEN = null;
                                    string fieldTextF = null;
                                    string fieldTextD = null;

                                    //if (fieldName == "Mode")
                                    //{ 
                                    //}
                                    fieldTextEN = (string)oValuesList[i][6];
                                    fieldTextF = (string)oValuesList[i][7];
                                    fieldTextD = (string)oValuesList[i][8];

                                    if (_overrideProductionLineNumber > 0 && fieldName.Equals("ProductionLineNumber"))
                                    {
                                        if (_overrideProductionLineNumber != Convert.ToInt32(fieldValue))
                                        {
                                            fieldValue = _overrideProductionLineNumber;
                                        }
                                        //_overrideProductionLineNumber = 0;
                                    }

                                    //switch (_selectedLanguage)
                                    //{
                                    //    case enumLanguages.English:
                                    //        fieldText = (string)oValuesList[i][6];
                                    //        break;
                                    //    case enumLanguages.French:
                                    //        fieldText = (string)oValuesList[i][7];
                                    //        break;
                                    //    case enumLanguages.German:
                                    //        fieldText = (string)oValuesList[i][8];
                                    //        break;
                                    //}

                                    string arrayItemFieldName = null;
                                    int arrayIndex = -1;

                                    int splitIndex = fieldName.IndexOf('_');
                                    if (splitIndex > 0 && splitIndex < fieldName.Length - 1)
                                    {
                                        string firstPart = fieldName.Substring(0, splitIndex);
                                        string rest = fieldName.Substring(splitIndex + 1);
                                        splitIndex = rest.IndexOf('_');
                                        if (splitIndex > 0 && splitIndex < rest.Length - 1)
                                        {
                                            string sIndex = rest.Substring(0, splitIndex);
                                            if (int.TryParse(sIndex, out arrayIndex))
                                            {
                                                --arrayIndex;
                                                arrayItemFieldName = rest.Substring(splitIndex + 1);
                                                fieldName = firstPart;
                                            }
                                        }
                                    }

                                    bool isFound = false;
                                    FieldInfo fi = typeof(SetupStruct).GetField(fieldName);
                                    if (fi != null)
                                    {
                                        if (arrayItemFieldName != null && arrayItemFieldName.Length > 0)
                                        {
                                            if (fi.FieldType.IsArray)
                                            {
                                                Array array = fi.GetValue(_setup) as Array;
                                                if (array != null)
                                                {
                                                    object arrayItem = array.GetValue(arrayIndex);
                                                    if (arrayItem != null)
                                                    {
                                                        fi = arrayItem.GetType().GetField(arrayItemFieldName);
                                                        if (fi != null)
                                                        {
                                                            isFound = true;

                                                            if (Convert.ToInt32(fieldValue) < 0 && fi.FieldType.Equals(typeof(byte)))
                                                            {
                                                                fieldValueToICP = (byte)255;
                                                            }
                                                            else
                                                            {
                                                                fieldValueToICP = Convert.ChangeType(fieldValue, fi.FieldType);
                                                            }

                                                            fi.SetValue(arrayItem, fieldValueToICP);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            isFound = true;

                                            if (fi.FieldType.IsEnum)
                                            {
                                                fieldValueToICP = Convert.ChangeType(fieldValue, typeof(byte));
                                            }
                                            else if (Convert.ToInt32(fieldValue) < 0 && fi.FieldType.Equals(typeof(byte)))
                                            {
                                                fieldValueToICP = (byte)255;
                                            }
                                            else
                                            {
                                                fieldValueToICP = Convert.ChangeType(fieldValue, fi.FieldType);
                                            }

                                            fi.SetValue(_setup, fieldValueToICP);
                                        }
                                    }

                                    if (!isFound)
                                    {
                                        AddToLog("Field not found: " + fieldNameLong);
                                    }
                                    else
                                    {
                                        if (arrayItemFieldName != null && arrayItemFieldName.Length > 0)
                                        {
                                            _setupItemList.Add(new SetupItem(stackerId, fieldNameLong, fieldValue, fieldValueMin, fieldValueMax,
                                                fieldTextEN, fieldTextF, fieldTextD,
                                                fieldName, arrayIndex, arrayItemFieldName));
                                        }
                                        else
                                        {
                                            if (fieldName.Equals("ProductionLineNumber"))
                                            {
                                                _setupItemList.Insert(0, new SetupItem(stackerId, fieldName, fieldValue, fieldValueMin, fieldValueMax,
                                                    fieldTextEN, fieldTextF, fieldTextD));
                                            }
                                            else
                                            {
                                                _setupItemList.Add(new SetupItem(stackerId, fieldName, fieldValue, fieldValueMin, fieldValueMax,
                                                    fieldTextEN, fieldTextF, fieldTextD));
                                            }
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    AddToLog(ex);
                                }
                            }

                            //if (_topSheetMain != null)
                            //{
                            //    _topSheetMain.Stop();
                            //}

                            //if (_topSheetMain != null)
                            //{
                            //    if (oldLine != _setup.ProductionLineNumber ||
                            //        oldGL != _setup.CurrentGLNumber)
                            //    {
                            //        _topSheetMain.Start(_setup.ProductionLineNumber, _setup.CurrentGLNumber);//.DbReconnect();
                            //        //_topSheetMain.SetStackerPosition(_setup.CurrentGLNumber);
                            //    }
                            //}

                            UpdateSettingsGui();
                            RequestSetupSendToStacker();

                            _setupReceived = true;

                            //byte[] testBytes = GetSetupStructBytes();

                            tick = Environment.TickCount - tick;
                            break;

                        case enumDataBaseCommands.dbCommGetBundleList:
                            //_newProductionQueue.Clear();
                            //oValuesList.Clear();
                            //if (_newProductionQueue.Count == 0)
                            {
                                firstBundleId = 0;
                                bundleCount = 0;
                                bool isStopped = false;
                                if (oValuesList.Count > 0)
                                {
                                    ushort bundleId = (ushort)(int)oValuesList[0][0];
                                    if (bundleId == 0)
                                    {
                                        isStopped = true;
                                        //_newProductionQueue.Add(null);
                                        if (_setup.Mode != enumModes.STANDARD)
                                        {
                                            _setup.IsPaused = 1;
                                        }
                                    }
                                }

                                if (!isStopped)
                                {
                                    _setup.IsPaused = 0;
                                    for (rowIndex = 0; rowIndex < oValuesList.Count; rowIndex++)
                                    {
                                        oValues = oValuesList[rowIndex];
                                        BundleData newBundle = new BundleData();
                                        newBundle.BundleId = (ushort)(int)oValues[0];
                                        newBundle.NumCopies = (ushort)(int)oValues[1];
                                        newBundle.Stacker = (byte)(int)oValues[2];
                                        newBundle.CopiesInLayer = (ushort)(int)oValues[3];
                                        newBundle.MaxCopiesInLayer = (ushort)(int)oValues[4];
                                        newBundle.MinCopiesInLayer = (ushort)(int)oValues[5];
                                        newBundle.MinCopiesInBundle = (ushort)(int)oValues[6];
                                        string addressData = (string)oValues[7];
                                        newBundle.SetAddressList(addressData);
                                        newBundle.Status = enumBundleStatus.bundleStatusReady;

                                        lock (_lockNewProductionQueue)
                                        {

                                            if (newBundle.CopiesInLayer > 0)
                                            {
                                                bool isOk = newBundle.CreateLayerList();
                                                if (isOk)
                                                {
                                                    //update address
                                                    newBundle.UpdateAddressList();
                                                    string sBundle = string.Format("Bundle {0} added to production queue."
                                                        + " Copies={1}, Layer={2}, Max={3}, Min={4},{5}"
                                                        , newBundle.BundleId
                                                        , newBundle.NumCopies
                                                        , newBundle.CopiesInLayer
                                                        , newBundle.MaxCopiesInLayer
                                                        , newBundle.MinCopiesInLayer
                                                        , newBundle.MinCopiesInBundle);
                                                    AddToLog(sBundle);

                                                    if (newBundle.Stacker > 0)
                                                    {
                                                        _newProductionQueue[newBundle.Stacker - 1].Add(newBundle);
                                                        _newBundlesReceived[newBundle.Stacker - 1] = true;
                                                    }
                                                }
                                                else
                                                {
                                                    SetBundleStatus(enumBundleStatus.bundleStatusBundleError,
                                                        newBundle.Stacker, newBundle.BundleId, 0);

                                                    string sError = string.Format("Unable to add bundle {0} to production queue."
                                                        + " Invalid layer/copy count settings."
                                                        + " Copies={1}, Layer={2}, Max={3}, Min={4},{5}"
                                                        , newBundle.BundleId
                                                        , newBundle.NumCopies
                                                        , newBundle.CopiesInLayer
                                                        , newBundle.MaxCopiesInLayer
                                                        , newBundle.MinCopiesInLayer
                                                        , newBundle.MinCopiesInBundle
                                                        );

                                                    AddToLog(sError);
                                                }                                        
                                        }

                                            if (firstBundleId == 0)
                                            {
                                                firstBundleId = newBundle.BundleId;
                                            }

                                            ++bundleCount;
                                        }
                                        
                                    }

                                    //if (_newProductionQueue.Count > 0 && _newProductionQueue[0].BundleId > 0)
                                    //{
                                    //    AddToLog(string.Format("Production bundles received. First bundle id = {0}, Bundle count = {1}", firstBundleId, bundleCount));
                                    //}
                                }
                            }
                            //else
                            //{
                            //    AddToLog("Production bundles received but not ready.");
                            //}
                            break;
                        case enumDataBaseCommands.dbCommGetNextCommand:
                            if (oValuesList.Count > 0)
                            {
                                oValues = oValuesList[0];
                                iResult1 = (int)oValues[0];
                                if (iResult1 > 0)
                                {
                                    if (iResult1 < 100)
                                    {
                                        //if (iResult1 == (int)enumDataBaseCommands.dbCommSetAllBundlesProduced)
                                        //{
                                        //    _setAllBundlesProducedReceived = true;
                                        //}
                                        //else if (iResult1 == (int)enumDataBaseCommands.dbCommSetSmallBundlesProduced)
                                        //{
                                        //    _setSmallBundlesProducedReceived = true;
                                        //}
                                        //else
                                        {
                                            DbAddRequestedCommand((enumDataBaseCommands)iResult1);
                                        }
                                    }
                                    else
                                    {
                                        stackerNumber = (byte)(iResult1 / 100);
                                        subCommand = (byte)(iResult1 % 100);

                                        //AddToLog(string.Format("------------Stacker command received stacker {0}, command {1}------------", stackerNumber, subCommand));

                                        if (subCommand == (byte)enumDataBaseCommands.dbCommStackerPause)
                                        {
                                            //_isPaused = true;
                                            _pcStatusStacker = 3;
                                            AddToLog(string.Format("Stacker {0} paused", stackerNumber));
                                        }
                                        else if (subCommand == (byte)enumDataBaseCommands.dbCommStackerResume)
                                        {
                                            //_isPaused = false;
                                            _pcStatusStacker = 1;
                                            AddToLog(string.Format("Stacker {0} resumed", stackerNumber));
                                        }
                                    }
                                }
                            }
                            break;
                        case enumDataBaseCommands.dbCommSetLineStatus:
                            break;
                        case enumDataBaseCommands.dbCommSetBundleStatus:
                            break;
                        case enumDataBaseCommands.dbCommSetStackerStatus:
                            break;
                    }//end switch

                    result = true;
                }
            }
            catch (Exception ex)
            {
                result = false;
                AddToLog(ex);
            }

            return result;
        }


        private void DbAddCommandToQueueServer(DbCommandStruct dbComm)
        {
            lock (_lockDbCommandListServer)
            {
                dbComm.CommandId = _nextDbCommandIdServer++;
                _dbCommandListServer.Add(dbComm);
            }
        }

        //private void DbAddCommandToQueueLocal(DbCommandStruct dbComm)
        //{
        //    dbComm.CommandId = _nextDbCommandIdLocal++;
        //    _dbCommandListLocal.Add(dbComm);
        //}

        private void DbAddRequestedCommand(enumDataBaseCommands command)
        {
            DbCommandStruct dbComm = new DbCommandStruct();

            bool isOkCommand = true;

            switch (command)
            {
                //case enumDataBaseCommands.dbCommGetBundleList:
                //    dbComm.Command = (enumDataBaseCommands)command;
                //    break;
                case enumDataBaseCommands.dbCommGetSetup:
                    dbComm.Command = enumDataBaseCommands.dbCommGetSetup;
                    break;

                default:
                    isOkCommand = false;
                    break;
            }

            if (isOkCommand)
            {
                dbComm.StackerNumber = 0;
                dbComm.BundleNumber = 0;
                dbComm.Status = 0;
                dbComm.TableId = 0;
                dbComm.CopyNumber = 0;

                DbAddCommandToQueueServer(dbComm);
            }
        }

        private bool DbCheckCommandToServerExists(enumDataBaseCommands command, byte stackerNumber)
        {
            bool result = false;
            int index = -1;

            lock (_lockDbCommandListServer)
            {
                while (!result && ++index < _dbCommandListServer.Count)
                {
                    if (_dbCommandListServer[index].Command == enumDataBaseCommands.dbCommGetBundleList &&
                        _dbCommandListServer[index].StackerNumber == stackerNumber)
                    {
                        result = true;
                    }
                }
            }

            return result;
        }

        private void DbSetBundleStatus(enumBundleStatus bundleStatus, byte stackerNumber,
            ushort bundleNumber, int copyNumber)
        {
            DbCommandStruct dbComm = new DbCommandStruct();
            dbComm.StackerNumber = stackerNumber;
            dbComm.BundleNumber = (ushort)(bundleNumber);
            dbComm.Status = (byte)bundleStatus;
            dbComm.Command = enumDataBaseCommands.dbCommSetBundleStatus;
            dbComm.TableId = 0;
            dbComm.CopyNumber = copyNumber;

            DbAddCommandToQueueServer(dbComm);
        }

        private void DbGetSetup()
        {
            DbCommandStruct dbComm = new DbCommandStruct();
            dbComm.Command = enumDataBaseCommands.dbCommGetSetup;
            dbComm.StackerNumber = 0;
            dbComm.BundleNumber = 0;
            dbComm.Status = 0;
            dbComm.TableId = 0;
            dbComm.CopyNumber = 0;

            DbAddCommandToQueueServer(dbComm);
        }

        private void DbGetBundleList(ushort numBundles, byte stackerNumber)
        {
            if (!DbCheckCommandToServerExists(enumDataBaseCommands.dbCommGetBundleList, stackerNumber))
            {
                DbCommandStruct dbComm = new DbCommandStruct();
                dbComm.Command = enumDataBaseCommands.dbCommGetBundleList;
                dbComm.StackerNumber = stackerNumber;
                dbComm.BundleNumber = numBundles;
                dbComm.Status = 0;
                dbComm.TableId = 0;
                dbComm.CopyNumber = 0;

                DbAddCommandToQueueServer(dbComm);
            }
        }

        private void DbSetInkStatus(enumInkStatus inkStatus)
        {
            DbCommandStruct dbComm = new DbCommandStruct();
            dbComm.Command = enumDataBaseCommands.dbCommSetInkStatus;
            dbComm.StackerNumber = 0;
            dbComm.BundleNumber = 0;
            dbComm.Status = (byte)inkStatus;
            dbComm.TableId = 0;
            dbComm.CopyNumber = 0;

            DbAddCommandToQueueServer(dbComm);
        }

        private void DbSetStackerStatus(enumStackerStatus stackerStatus, byte stackerNumber)
        {
            DbCommandStruct dbComm = new DbCommandStruct();
            dbComm.Command = enumDataBaseCommands.dbCommSetStackerStatus;
            dbComm.StackerNumber = stackerNumber;
            dbComm.BundleNumber = 0;
            dbComm.Status = (byte)stackerStatus;
            dbComm.TableId = 0;
            dbComm.CopyNumber = 0;

            DbAddCommandToQueueServer(dbComm);
        }

        private void DbSetApplicatorStatus(TopSheetControlLib.TopSheetMain.TopSheetApplicatorStates status,
            byte stackerNumber)
        {
            DbCommandStruct dbComm = new DbCommandStruct();
            dbComm.Command = enumDataBaseCommands.dbCommSetTopSheetApplicatorStatus;
            dbComm.StackerNumber = stackerNumber;
            dbComm.BundleNumber = 0;
            dbComm.Status = (byte)status;
            dbComm.TableId = 0;
            dbComm.CopyNumber = 0;

            DbAddCommandToQueueServer(dbComm);
        }

        private void DbSetPrinterStatus(TopSheetControlLib.TopSheetMain.TopSheetPrinterStates status,
            byte stackerNumber, byte printerNumber)
        {
            DbCommandStruct dbComm = new DbCommandStruct();
            dbComm.Command = enumDataBaseCommands.dbCommSetTopSheetPrinterStatus;
            dbComm.StackerNumber = stackerNumber;
            dbComm.BundleNumber = 0;
            dbComm.Status = (byte)status;
            dbComm.TableId = printerNumber;
            dbComm.CopyNumber = 0;

            DbAddCommandToQueueServer(dbComm);
        }

        private void DbSetNextMachineStatus(TopSheetControlLib.TopSheetMain.NextMachineStates status,
            byte stackerNumber)
        {
            DbCommandStruct dbComm = new DbCommandStruct();
            dbComm.Command = enumDataBaseCommands.dbCommSetNextMachineStatus;
            dbComm.StackerNumber = stackerNumber;
            dbComm.BundleNumber = 0;
            dbComm.Status = (byte)status;
            dbComm.TableId = 0;
            dbComm.CopyNumber = 0;

            DbAddCommandToQueueServer(dbComm);
        }

        private void DbSetLineStatus(enumLineStatus lineStatus)
        {
            DbCommandStruct dbComm = new DbCommandStruct();
            dbComm.Command = enumDataBaseCommands.dbCommSetLineStatus;
            dbComm.StackerNumber = 0;
            dbComm.BundleNumber = 0;
            dbComm.Status = (byte)lineStatus;
            dbComm.TableId = 0;
            dbComm.CopyNumber = 0;

            DbAddCommandToQueueServer(dbComm);
        }

        private void DbDisconnectServer()
        {
            DbConnectionStateServer = enumDbConnectionStates.DbDisconnected;

            if (_conServer != null)
            {
                try
                {
                    if (_conServer != null && _conServer.State == ConnectionState.Open)
                    {
                        _conServer.Close();
                        AddToLog("DbControlServer disconnected");
                    }
                }
                catch
                {
                }
                finally
                {
                    _conServer = null;
                }
            }
        }

        #endregion

        #region UpdateSettingsGui, UpdateStatusGui, UpdateLanguageGui

        private void UpdateSettingsGui()
        {
            /*
            System.Reflection.FieldInfo[] fiArray = typeof(SetupStruct).GetFields();
            for (int i = 0; i < fiArray.Length; i++)
            {
                object o = fiArray[i].GetValue(_setup);
                string sValue = o != null ? o.ToString() : "NULL";

                if (o != null && fiArray[i].FieldType.IsEnum)
                {
                    sValue = ((byte)o).ToString() + " - " + sValue;
                }

                if (o != null && fiArray[i].FieldType.IsArray)
                {
                    Array array = (Array)o;
                    System.Reflection.FieldInfo[] arrayFiArray = fiArray[i].FieldType.GetElementType().GetFields();
                    _setupItemList.Add(new SetupItem(null, null, fiArray[i].Name.PadRight(padding).Substring(0, padding) + " [" + array.Length.ToString() + "]"));

                    for (int i2 = 0; i2 < array.Length; i2++)
                    {
                        object oElement = array.GetValue(i2);

                        itemList.Add(new SetupItem(null, null, "  [" + i2.ToString() + "]"));

                        for (int i3 = 0; i3 < arrayFiArray.Length; i3++)
                        {
                            System.Reflection.FieldInfo fi = arrayFiArray[i3];

                            o = fi.GetValue(oElement);
                            sValue = o != null ? o.ToString() : "NULL";

                            if (o != null && fi.FieldType.IsEnum)
                            {
                                sValue = ((byte)o).ToString() + " - " + sValue;
                            }

                            itemList.Add(new SetupItem(fiArray[i].Name + "_" + i2.ToString() + "_" + fi.Name,
                                o, ("    " + fi.Name).PadRight(padding).Substring(0, padding) + " " + sValue,
                                fiArray[i].Name, i2, fi.Name));
                        }
                    }
                }
                else
                {
                    itemList.Add(new SetupItem(fiArray[i].Name, o, fiArray[i].Name.PadRight(padding).Substring(0, padding) + " " + sValue));
                }
            }
            */
            SetupItem[] items = _setupItemList.ToArray();
            _lineControlUI.InvokeSetSettingsList(items);
        }

        private void UpdateStatusGui()
        {
            List<string> sItemsList = new List<string>();

            //lock (_lockStatus)
            {
                int padding = 35;

                int[] dbProcessTicksServer = _dbProcessTicksServer.ToArray();
                int sumTicksServer = 0;
                for (int i = 0; i < dbProcessTicksServer.Length; i++)
                {
                    sumTicksServer += dbProcessTicksServer[i];
                }
                int avgTicksServer = dbProcessTicksServer.Length > 0 ? sumTicksServer / dbProcessTicksServer.Length : 0;

                //int[] dbProcessTicksLocal = _dbProcessTicksLocal.ToArray();
                //int sumTicksLocal = 0;
                //for (int i = 0; i < dbProcessTicksLocal.Length; i++)
                //{
                //    sumTicksLocal += dbProcessTicksLocal[i];
                //}
                //int avgTicksLocal = dbProcessTicksLocal.Length > 0 ? sumTicksLocal / dbProcessTicksLocal.Length : 0;

                sItemsList.Add("Status update:".PadRight(padding) + " " + DateTime.Now.ToString("HH:mm:ss"));

                sItemsList.Add("");

                sItemsList.Add("PC status:".PadRight(padding) + " " + PcStatusStacker.ToString());

                sItemsList.Add("DB connection server:".PadRight(padding) + " "
                    + (_conServer != null ? _conServer.State.ToString() : "Disconnected"));

                int countCommandsServer;
                lock (_lockDbCommandListServer)
                {
                    countCommandsServer = _dbCommandListServer.Count;
                }

                sItemsList.Add("DB Command queue server: ".PadRight(padding) + " " + countCommandsServer.ToString()
                    + " - avg time=" + avgTicksServer.ToString() + "ms");

                //sItemsList.Add("DB connection local:".PadRight(padding) + " "
                //    + (_conLocal != null ? _conLocal.State.ToString() : "Disconnected"));
                //sItemsList.Add("DB Command queue local: ".PadRight(padding) + " " + _dbCommandListLocal.Count.ToString()
                //    + " - avg time=" + avgTicksLocal.ToString() + "ms");

                sItemsList.Add("Icp connection:".PadRight(padding) + " " +
                    (_tcpClientIcpStacker != null && _tcpClientIcpStacker.Client != null &&
                    _tcpClientIcpStacker.Client.Connected ? "Connected" : "Disconnected"));

                for (int i = 0; i < _stackerList.Count; i++)
                {
                    StackerLine stacker = _stackerList[i];

                    sItemsList.Add("");
                    sItemsList.Add("Stacker line " + stacker.StackerNumber.ToString());
                    sItemsList.Add("TopSheet control:".PadRight(padding) + " " +
                        (stacker.TopSheetMain != null && stacker.TopSheetMain.IsStarted ? "Started" : "Stopped"));

                    sItemsList.Add("TopSheet applicator:".PadRight(padding) + " " + stacker.TopSheetApplicatorState.ToString());

                    sItemsList.Add("TopSheet printer1:".PadRight(padding) + " " + stacker.TopSheetPrinterState1.ToString());
                    if (_useTwoPrinters)
                    {
                        sItemsList.Add("TopSheet printer2:".PadRight(padding) + " " + stacker.TopSheetPrinterState2.ToString());
                    }

                    //sItemsList.Add("Layer count in table:".PadRight(padding) + " " + _status.LayerCountInTable.ToString());
                    //sItemsList.Add("Copies counting:".PadRight(padding) + " " + _status.CopiesCountedEntrance.ToString());
                    sItemsList.Add("Copies stacker total:".PadRight(padding) + " "
                        + (_status.Stackers[0].CopiesCountedEntrance + _status.Stackers[1].CopiesCountedEntrance).ToString());

                    sItemsList.Add("");
                    byte gripperLine = _setup != null ? _setup.CurrentGLNumber : (byte)0;
                    int bundleAtExit = stacker.TopSheetMain != null && stacker.TopSheetMain.BundleAtStackerExit ? 1 : 0;
                    sItemsList.Add("Gripper line:".PadRight(padding) + " " + gripperLine.ToString());
                    sItemsList.Add("Bundle at exit:".PadRight(padding) + " " + bundleAtExit.ToString());
                    // sItemsList.Add("Exit not ready:".PadRight(padding) + " " + _status.NextMachineNotReady.ToString());
                    if (stacker.TopSheetMain != null && stacker.TopSheetMain.IcpConnectionState ==
                        TopSheetControlLib.TopSheetMain.IcpConnectionStates.Connected)
                    {
                        if (stacker.TopSheetMain.Setup.BundlePositionCount > 0)
                        {
                            short minTime = stacker.TopSheetMain.Setup.BundlePositions[0].ReceiveExpected;
                            short maxTime = stacker.TopSheetMain.Setup.BundlePositions[0].ReceiveTimeout;
                            sItemsList.Add("Bundle travel time:".PadRight(padding) + " " + minTime.ToString() + " - " + maxTime.ToString());
                        }
                    }
                }

                #region Add status struct commented

                //if (_tcpClientIcpStacker != null && _tcpClientIcpStacker.Client != null &&
                //    _tcpClientIcpStacker.Client.Connected)
                //{
                //    sItemsList.Add("");

                //    //sItemsList.Add("PaperTime".PadRight(padding).Substring(0, padding) + " " + _paperTimeReal.ToString());
                //    //sItemsList.Add("GripperPulses".PadRight(padding).Substring(0, padding) + " " +
                //    //    (_status.InkPulseLowPreviousGripper - _status.InkPulseLowBeforePreviousGripper).ToString());

                //    System.Reflection.FieldInfo[] fiArray = typeof(StatusStruct).GetFields();
                //    for (int i = 4; i < fiArray.Length - 2; i++)
                //    {
                //        object o = fiArray[i].GetValue(_status);
                //        string sValue = o != null ? o.ToString() : "NULL";

                //        if (o != null && fiArray[i].FieldType.IsEnum)
                //        {
                //            sValue = ((byte)o).ToString() + " - " + sValue;
                //        }

                //        if (o != null && fiArray[i].FieldType.IsArray)
                //        {
                //            Array array = (Array)o;
                //            System.Reflection.FieldInfo[] arrayFiArray = fiArray[i].FieldType.GetElementType().GetFields();
                //            sItemsList.Add(fiArray[i].Name.PadRight(padding).Substring(0, padding) + " [" + array.Length.ToString() + "]");

                //            for (int i2 = 0; i2 < array.Length; i2++)
                //            {
                //                object oElement = array.GetValue(i2);

                //                sItemsList.Add("  [" + i2.ToString() + "]");

                //                for (int i3 = 0; i3 < arrayFiArray.Length; i3++)
                //                {
                //                    System.Reflection.FieldInfo fi = arrayFiArray[i3];

                //                    o = fi.GetValue(oElement);
                //                    sValue = o != null ? o.ToString() : "NULL";

                //                    if (o != null && fi.FieldType.IsEnum)
                //                    {
                //                        sValue = ((byte)o).ToString() + " - " + sValue;
                //                    }

                //                    sItemsList.Add(("    " + fi.Name).PadRight(padding).Substring(0, padding) + " " + sValue);
                //                }
                //            }
                //        }
                //        else
                //        {
                //            sItemsList.Add(fiArray[i].Name.PadRight(padding).Substring(0, padding) + " " + sValue);
                //        }
                //    }
                //}

                #endregion
            }

            string[] sItems = sItemsList.ToArray();
            _lineControlUI.InvokeSetStatusList(sItems);
            //_lineControlUI.SetStatusList(sItems);
        }

        private void UpdateLogGui()
        {
            try
            {
                GuiLogItem[] logItems = null;

                lock (_lockGuiLogItemList)
                {
                    try
                    {
                        if (_guiLogItemList.Count > 0)
                        {
                            logItems = _guiLogItemList.ToArray();
                            _guiLogItemList.Clear();
                        }
                    }
                    catch
                    {
                    }
                }

                if (logItems != null && logItems.Length > 0)
                {
                    _lineControlUI.InvokeAddToLog(logItems);
                }
            }
            catch
            {
            }
        }

        private void UpdateLanguageGui()
        {
            try
            {
                if (_conServer != null && _conServer.State == ConnectionState.Open)
                {
                    List<GuiLanguageItem> languageItemList = new List<GuiLanguageItem>();

                    string sqlSelect = "select * from StackerLanguageTable";
                    OleDbDataAdapter da = new OleDbDataAdapter(sqlSelect, _conServer);
                    DataTable dt = new DataTable();
                    DBTools.HandleBusyFillSchema(da, dt, SchemaType.Source);
                    DBTools.HandleBusyFill(da, dt);

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dataRow = dt.Rows[i];
                        GuiLanguageItem item = new GuiLanguageItem();
                        item.ItemName = (string)dataRow["FieldName"];
                        item.ItemTextEN = (string)dataRow["FieldTextEN"];
                        item.ItemTextFR = (string)dataRow["FieldTextFR"];
                        item.ItemTextDE = (string)dataRow["FieldTextDE"];
                        languageItemList.Add(item);
                    }

                    GuiLanguageItem[] items = languageItemList.ToArray();
                    _lineControlUI.InvokeUpdateLanguage(items);
                }
            }
            catch
            {

            }
        }

        #endregion

        #region Logging

        public void AddToLog(Exception ex)
        {
            try
            {
                if (ex != null)
                {
                    AddToLog(false, LogLevels.Error, ex.ToString());
                }
            }
            catch
            {
            }
        }

        public void AddToLog(string message)
        {
            try
            {
                if (message != null)
                {
                    AddToLog(true, LogLevels.Normal, message);
                }
            }
            catch
            {
            }
        }

        public void AddToLog(bool doShowInGui, LogLevels logLevel, string message)
        {
            try
            {
                //object o = new object[] { doShowInGui, logLevel, message };
                //new Thread(new ParameterizedThreadStart(AddToLogThread)).Start(o);

                //Logging.AddToLog(logLevel, message);

                if (doShowInGui)
                {
                    //_lineControlUI.InvokeAddToLog(logLevel, message);
                    lock (_lockGuiLogItemList)
                    {
                        _guiLogItemList.Add(new GuiLogItem(DateTime.Now, logLevel, message));
                    }
                    //if (_guiLogItemList.Count > 100)
                    //{
                    //    _guiLogItemList.RemoveAt(100);
                    //}
                }
            }
            catch
            {
            }
        }

        //public void AddToLogThread(object o)
        //{
        //    try
        //    {
        //        object[] oParams = (object[])o;
        //        bool doShowInGui = (bool)oParams[0];
        //        LogLevels logLevel = (LogLevels)oParams[1];
        //        string message = (string)oParams[2];

        //        Logging.AddToLog(logLevel, message);

        //        if (doShowInGui)
        //        {
        //            //_lineControlUI.InvokeAddToLog(logLevel, message);
        //            _guiLogItemList.Insert(0, new GuiLogItem(DateTime.Now, logLevel, message));
        //            if (_guiLogItemList.Count > 100)
        //            {
        //                _guiLogItemList.RemoveAt(100);
        //            }
        //        }
        //    }
        //    catch
        //    {
        //    }
        //}

        #endregion
    }
}
