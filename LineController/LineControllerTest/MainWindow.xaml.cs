﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LineControllerLib;

namespace LineControlTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private double _orignalWidth;
        private double _originalHeight;

        private LineControlMain _lineControlMain1;
        private LineControlMain _lineControlMain2;

        private string _connectionStringServer;
        private string[] LineIPString;
        
        public MainWindow()
        {
            InitializeComponent();
            _orignalWidth = this.Width;
            _originalHeight = this.Height;

            this.SizeChanged += MainWindow_SizeChanged;
            this.WindowState = System.Windows.WindowState.Normal; //System.Windows.WindowState.Maximized;
            this.Loaded += MainWindow_Loaded;
            this.Closed += MainWindow_Closed;

            string DBServerIP = "172.22.22.150"; //"10.45.6.129"; //172.22.22.150
            _connectionStringServer = "Provider=SQLOLEDB.1; Server=" + DBServerIP + "; Database=UniMail_Sydost; Uid=sa; Pwd=UniMail2017";

            LineIPString = new string[2];
            LineIPString[0] = "172.22.22.180";
            LineIPString[1] = "172.22.22.181";

            _lineControlMain1 = new LineControlMain(1, LineIPString[0], _connectionStringServer);
            _lineControlMain2 = new LineControlMain(2, LineIPString[1], _connectionStringServer);
        }

        private void MainWindow_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            try
            {
                double currentScaleX = 1;
                double currentScaleY = 1;

                bool isChange = false;

                if (e.WidthChanged)// && e.PreviousSize.Width > 0)
                {
                    currentScaleX = e.NewSize.Width / _orignalWidth;
                    isChange = true;
                }

                if (e.HeightChanged)// && e.PreviousSize.Height > 0)
                {
                    currentScaleY = e.NewSize.Height / _originalHeight;
                    isChange = true;
                }

                if (isChange)
                {
                    uxMainGrid1.LayoutTransform = new ScaleTransform(currentScaleX, currentScaleY);
                    uxMainGrid2.LayoutTransform = new ScaleTransform(currentScaleX, currentScaleY);
                }
            }
            catch
            {
            }
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {


                //uxLineControl.SetSelectedLanguage(2);
                //uxLineControl.Start(connectionString, connectionStringServer);
                   // "Printer1", "Printer2", "32.10.37.28", "32.10.37.29", 9100, 9100, true, 1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        public void Stop()
        {
            if (_lineControlMain1 != null)
            {
                _lineControlMain1.Stop();
            }

            if (_lineControlMain2 != null)
            {
                _lineControlMain2.Stop();
            }

        }

        private void MainWindow_Closed(object sender, EventArgs e)
        {
            try
            {
                _lineControlMain1.Stop();
                _lineControlMain2.Stop();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }


        private void UxStopLineController1_Click(object sender, RoutedEventArgs e)
        {
            if (_lineControlMain1 != null)
            {
                _lineControlMain1.Stop();
            }
        }

        private void UxStopLineController2_Click(object sender, RoutedEventArgs e)
        {
            if (_lineControlMain2 != null)
            {
                _lineControlMain2.Stop();
            }
        }


        private void UxStartLineControllerButton1_Click(object sender, RoutedEventArgs e)
        {
            if (_lineControlMain1 == null)
            {
                _lineControlMain1 = new LineControlMain(1, LineIPString[0], _connectionStringServer);
            }
            _lineControlMain1.Start();
        }

        private void UxStartLineControllerButton2_Click(object sender, RoutedEventArgs e)
        {
            if (_lineControlMain2 == null)
            {
                _lineControlMain2 = new LineControlMain(2, LineIPString[1], _connectionStringServer);
            }
            _lineControlMain2.Start();
        }

        private void UxProductionButton1_Click(object sender, RoutedEventArgs e)
        {
            if (_lineControlMain1 == null)
            {
                _lineControlMain1 = new LineControlMain(1, LineIPString[0], _connectionStringServer);
            }

            //if (_lineControlMain1.IsStarted)
            if (_lineControlMain1.IsStarted)
            {
                _lineControlMain1.StartProduction();
            }
            else
            {
                _lineControlMain1.Start();
                _lineControlMain1.StartProduction();
            }
        }

        private void UxProductionButton2_Click(object sender, RoutedEventArgs e)
        {
            if (_lineControlMain2 == null)
            {
                _lineControlMain2 = new LineControlMain(2, LineIPString[1], _connectionStringServer);
            }

            //if (_lineControlMain1.IsStarted)
            if (_lineControlMain2.IsStarted)
            {
                _lineControlMain2.StartProduction();
            }
            else
            {
                _lineControlMain2.Start();
                _lineControlMain2.StartProduction();
            }
        }


        private void UxStopProductionButton1_Click(object sender, RoutedEventArgs e)
        {
            if (_lineControlMain1 != null && _lineControlMain1.IsProductionStarted)
            {
                _lineControlMain1.StopProduction();
            }
        }

        private void UxStopProductionButton2_Click(object sender, RoutedEventArgs e)
        {
            if (_lineControlMain2 != null && _lineControlMain2.IsProductionStarted)
            {
                _lineControlMain2.StopProduction();
            }
        }

        private void UxTestInkjetButton1_Click(object sender, RoutedEventArgs e)
        {
            //byte[] MyimajeTelegram;
            //string AddressLine1 = "Line1";
            //string AddressLine2 = "Line2";
            //string AddressLine3 = "Line3";
            //string AddressLine4 = "Line4";

            //byte addressWidth;

            //LineControllerLib.Inkjet _InkjetMain = new LineControllerLib.Inkjet();

            //MyimajeTelegram = _InkjetMain.AdressToStringImajeLinesWithoutLibary(
            //    AddressLine1, AddressLine2, AddressLine3, AddressLine4, out addressWidth);
        }

        private void UxTestInkjetButton2_Click(object sender, RoutedEventArgs e)
        {
            //byte[] MyimajeTelegram;
            //string AddressLine1 = "Line1";
            //string AddressLine2 = "Line2";
            //string AddressLine3 = "Line3";
            //string AddressLine4 = "Line4";

            //byte addressWidth;

            //LineControllerLib.Inkjet _InkjetMain = new LineControllerLib.Inkjet();

            //MyimajeTelegram = _InkjetMain.AdressToStringImajeLinesWithoutLibary(
            //    AddressLine1, AddressLine2, AddressLine3, AddressLine4, out addressWidth);

            _lineControlMain2.InkjetPowerOff();
        }

        private void UxTestPrinter1Button2_Click(object sender, RoutedEventArgs e)
        {
            _lineControlMain2.TestPrinter(1);
        }

        private void UxTestPrinter2Button2_Click(object sender, RoutedEventArgs e)
        {
            _lineControlMain2.TestPrinter(2);
        }

        private void UxTestPrinter3Button2_Click(object sender, RoutedEventArgs e)
        {
            _lineControlMain2.TestPrinter(3);
        }

        private void UxTestPrinter4Button2_Click(object sender, RoutedEventArgs e)
        {
            _lineControlMain2.TestPrinter(4);
        }

    }
}
