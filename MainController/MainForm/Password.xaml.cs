﻿using LineControllerLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MainForm
{
    /// <summary>
    /// Interaction logic for Password.xaml
    /// </summary>
    public partial class Password : Window
    {
        //private bool isRightPassword = false;
        private LineControlMain lineControlMain;
        private int  Line;
        private String type;
        public Password(LineControlMain l,int i,String t)
        {
           
            InitializeComponent();
            lineControlMain = l;
            Line = i;
            type = t;
            this.InputPassword.Focus();
        }


        private void Confirm_Click(object sender, RoutedEventArgs e)
        {
            if (this.InputPassword.Password == "UniMail2017")
            {
               // MessageBox.Show("Good!");
                //isRightPassword = true;
                this.Close();
                if (type =="Stacker") { 
                SetupStacker s = new SetupStacker(lineControlMain,Line);
                s.Show();
                } 

                if(type == "BA")
                {
                    SetupBA BlueLineBA = new SetupBA(lineControlMain, 2);
                    BlueLineBA.Show();
                }
            }
            else
            {
                MessageBox.Show("Wrong! Try Again!");
                this.InputPassword.Password ="";
                this.InputPassword.Focus();
            };
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
