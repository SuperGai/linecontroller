﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using LineControllerLib;

namespace MainForm
{
    /// <summary>
    /// Interaction logic for Maintenance.xaml
    /// </summary>
    public partial class Maintenance : Window
    {
        public LineControlMain _lineControlMain;
        public const int MAX_STACKERLINE = 4;
        public const int MAX_GRID_ROWDEFINITION = 9;
        public const int NUM_DBH = 2;
        bool[] _transStart = { false, false};

        Label LabelStackerLine;
        Button buttonResetSTB;
        Button buttonResetPZF;
        Button buttonEnableSimulation;
        Button buttonDisableSimulation;
        Button buttonPrinter;
        Button buttonTrans;
        Button buttonCuter;
        TextBox armPress;
        Button armPressSet;
        Grid StackerLineGrid;


        public Maintenance()
        {
            InitializeComponent();
            InitializeMaintenanceAreaLayout();
        }

        private void InitializeMaintenanceAreaLayout()
        {
            try
            {
                string[] stackerNo = {"A","B","C","D"};

                for (int i = 0; i < MAX_STACKERLINE; i++)
                {
                    StackerLineGrid = new Grid
                    {
                        Name = "Stackerline" + i + "Area",
                        Margin = new Thickness(1, 1, 1, 1),
                        Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#e7e6e6"))
                    };

                    for (int j = 0; j < MAX_GRID_ROWDEFINITION; j++)
                    {
                        RowDefinition row = new RowDefinition();
                        StackerLineGrid.RowDefinitions.Add(row);
                    }

                    for (int k = 0; k < NUM_DBH; k++)
                    {
                        ColumnDefinition column = new ColumnDefinition();
                        StackerLineGrid.ColumnDefinitions.Add(column);
                    }

                    LabelStackerLine = new Label
                    {
                        Name = "LabelStackerline" + i,
                        Content = "Stackerline " + stackerNo[i],
                        FontSize = 25,
                        BorderThickness = new Thickness(0),
                        Background = Brushes.Transparent,
                        Margin = new Thickness(0, 0, 0, 0),
                        FontFamily = new FontFamily("Calibri"),
                        HorizontalAlignment = HorizontalAlignment.Center,
                        VerticalAlignment = VerticalAlignment.Top
                    };
                    LabelStackerLine.SetValue(Grid.RowProperty, 0);
                    LabelStackerLine.SetValue(Grid.ColumnSpanProperty, 2);

                    StackerLineGrid.Children.Add(LabelStackerLine);

                    buttonResetSTB = new Button();
                    buttonResetSTB = (Button)CreateControl(buttonResetSTB, i, "ResetSTB");
                    buttonResetSTB.Click += new RoutedEventHandler(RestSTB_Click);
                    buttonResetSTB.SetValue(Grid.RowProperty, 1);
                    buttonResetSTB.SetValue(Grid.ColumnSpanProperty, 2);
                    StackerLineGrid.Children.Add(buttonResetSTB);

                    buttonResetPZF = new Button();
                    buttonResetPZF = (Button)CreateControl(buttonResetPZF, i, "ResetPZF");
                    buttonResetPZF.Click += new RoutedEventHandler(RestPZF_Click);
                    buttonResetPZF.SetValue(Grid.RowProperty, 2);
                    buttonResetPZF.SetValue(Grid.ColumnSpanProperty, 2);
                    StackerLineGrid.Children.Add(buttonResetPZF);


                    buttonEnableSimulation = new Button();
                    buttonEnableSimulation = (Button)CreateControl(buttonEnableSimulation, i, "EnableSimulation");
                    buttonEnableSimulation.Click += new RoutedEventHandler(SimulationStart_Click);
                    buttonEnableSimulation.SetValue(Grid.RowProperty, 3);
                    buttonEnableSimulation.SetValue(Grid.ColumnSpanProperty, 2);
                    StackerLineGrid.Children.Add(buttonEnableSimulation);

                    buttonDisableSimulation = new Button();
                    buttonDisableSimulation = (Button)CreateControl(buttonDisableSimulation, i, "DisableSimulation");
                    buttonDisableSimulation.Click += new RoutedEventHandler(SimulationStop_Click);
                    buttonDisableSimulation.SetValue(Grid.RowProperty, 4);
                    buttonDisableSimulation.SetValue(Grid.ColumnSpanProperty, 2);
                    StackerLineGrid.Children.Add(buttonDisableSimulation);

                    for (int printerNo = 0; printerNo < NUM_DBH; printerNo++)
                    {
                        buttonPrinter = new Button();
                        buttonPrinter = (Button)CreateControl(buttonPrinter, i, "TestPrinter_" + (printerNo + 1).ToString());
                        buttonPrinter.Click += new RoutedEventHandler(TestPrinter_Click);
                        buttonPrinter.SetValue(Grid.RowProperty, 5);
                        buttonPrinter.SetValue(Grid.ColumnProperty, printerNo);
                        StackerLineGrid.Children.Add(buttonPrinter);

                        buttonTrans = new Button();
                        buttonTrans = (Button)CreateControl(buttonTrans, i, "PaperTrans" + (printerNo + 1).ToString());
                        buttonTrans.Click += new RoutedEventHandler(PrinterTransport_Click);
                        buttonTrans.SetValue(Grid.RowProperty, 6);
                        buttonTrans.SetValue(Grid.ColumnProperty, printerNo);
                        StackerLineGrid.Children.Add(buttonTrans);

                        buttonCuter = new Button();
                        buttonCuter = (Button)CreateControl(buttonCuter, i, "Cutter" + (printerNo + 1).ToString());
                        buttonCuter.Click += new RoutedEventHandler(PrinterCuter_Click);
                        buttonCuter.SetValue(Grid.RowProperty, 7);
                        buttonCuter.SetValue(Grid.ColumnProperty, printerNo);
                        StackerLineGrid.Children.Add(buttonCuter);

                    }

                    armPress = new TextBox()
                    {
                        Name = "ArmPress_" + i,
                        Text = "80",
                        FontSize = 19,
                        Width = 180,
                        Height = 30,
                        BorderBrush = (SolidColorBrush)(new BrushConverter().ConvertFrom("#767171")),
                        BorderThickness = new Thickness(2),
                        Background = Brushes.White,
                        Margin = new Thickness(10, 0, 10, 0),
                        FontFamily = new FontFamily("Calibri"),
                        HorizontalAlignment = HorizontalAlignment.Center,
                        VerticalAlignment = VerticalAlignment.Top,
                        HorizontalContentAlignment = HorizontalAlignment.Center,
                        VerticalContentAlignment = VerticalAlignment.Center
                    };
                    armPress.SetValue(Grid.RowProperty, 8);
                    armPress.SetValue(Grid.ColumnProperty, 0);
                    StackerLineGrid.Children.Add(armPress);

                    armPressSet = new Button();
                    armPressSet  = (Button)CreateControl(armPressSet, i, "ArmPressSet_");
                    armPressSet.Click += new RoutedEventHandler(ArmPressSet_Click);
                    armPressSet.SetValue(Grid.RowProperty, 8);
                    armPressSet.SetValue(Grid.ColumnProperty, 1);
                    StackerLineGrid.Children.Add(armPressSet);


                    MaintenanceArea.Children.Add(StackerLineGrid);

                    Grid.SetColumn(StackerLineGrid, i);
                }
            }
            catch
            { 
            }
        }

        

        public object CreateControl(object value, int stackerLineId, string controlName)
        {
            try
            {
                value = new Button
                {
                    Name = controlName + stackerLineId,
                    Content = controlName,
                    FontSize = 19,
                    Width = 180,
                    BorderBrush = (SolidColorBrush)(new BrushConverter().ConvertFrom("#767171")),
                    BorderThickness = new Thickness(2),
                    Background = Brushes.White,
                    Margin = new Thickness(10, 0, 10, 0),
                    FontFamily = new FontFamily("Calibri"),
                    HorizontalAlignment = HorizontalAlignment.Center,
                    VerticalAlignment = VerticalAlignment.Top
                };
            }
            catch
            { 
            }
            return value;
        }

        private async void RestSTB_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                FrameworkElement source = e.OriginalSource as FrameworkElement;
                byte stackerLineId = byte.Parse(source.Name.Substring(source.Name.Length - 1, 1));

                await Task.Run(() => _lineControlMain.ResetOneBABLC(stackerLineId));
            }
            catch
            {
            }
        }

        private async void TestPrinter_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                FrameworkElement source = e.OriginalSource as FrameworkElement;
                byte stackerLineId = byte.Parse(source.Name.Substring(source.Name.Length - 1, 1));
                byte printerNo = byte.Parse(source.Name.Substring(source.Name.Length - 2, 1));

                if (_lineControlMain != null)
                {
                    await Task.Run(
                        () => _lineControlMain.TestPrinter(printerNo, stackerLineId)
                    );
                }
                else
                {
                    MessageBox.Show("LCC is not running!");
                }
            }
            catch
            {
            }
        }

        private async void RestPZF_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                FrameworkElement source = e.OriginalSource as FrameworkElement;
                byte stackerLineId = byte.Parse(source.Name.Substring(source.Name.Length - 1, 1));
                if (_lineControlMain != null)
                {
                    await Task.Run(
                        () => _lineControlMain.ResetOnePZF(stackerLineId)
                    );
                }
                else
                {
                    MessageBox.Show("LCC is not running!");
                }
            }
            catch
            {
            }
        }
        private async void SimulationStart_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                FrameworkElement source = e.OriginalSource as FrameworkElement;
                byte stackerLineId = byte.Parse(source.Name.Substring(source.Name.Length - 1, 1));

                if (_lineControlMain != null)
                {
                    await Task.Run(
                        () => _lineControlMain.SimulationMode(TopSheetControlLib.TopSheetMain.IcpTelegramTypes.EnableSimulationMode, stackerLineId)
                    );
                }
                else
                {
                    MessageBox.Show("LCC is not running!");
                }
            }
            catch
            { 
            }
            
        }

        private async void SimulationStop_Click(object sender, RoutedEventArgs e)
        {
            FrameworkElement source = e.OriginalSource as FrameworkElement;
            byte stackerLineId = byte.Parse(source.Name.Substring(source.Name.Length - 1, 1));
            if (_lineControlMain != null)
            {
                await Task.Run(
                        () => _lineControlMain.SimulationMode(TopSheetControlLib.TopSheetMain.IcpTelegramTypes.DisableSimulationMode, stackerLineId)
                    );
            }
            else
            {
                MessageBox.Show("LCC is not running!");
            }
        }

        private void Drag_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                if (e.LeftButton == MouseButtonState.Pressed)
                {
                    DragMove();
                }
            }
            catch { }
        }

        private void PrinterTransport_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                FrameworkElement source = e.OriginalSource as FrameworkElement;
                byte stackerLineId = byte.Parse(source.Name.Substring(source.Name.Length - 1, 1));
                byte printerNo = byte.Parse(source.Name.Substring(source.Name.Length - 2, 1));
                if (_lineControlMain != null)
                {
                    if (!_transStart[printerNo - 1])
                    {
                        _lineControlMain.PaperTransport(stackerLineId, printerNo, true);
                        _transStart[printerNo - 1] = true;
                    }
                    else
                    {
                        _lineControlMain.PaperTransport(stackerLineId, printerNo, false);
                        _transStart[printerNo - 1] = false;
                    }
                }
                else
                {
                    MessageBox.Show("LCC is not running!");
                }
            }
            catch
            {
            }
        }

        private void ArmPressSet_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //FrameworkElement source = e.OriginalSource as FrameworkElement;
                //byte stackerLineId = byte.Parse(source.Name.Split('_')[1]);

                //object _oPrinterStateColor = MaintenanceArea.FindName("ArmPress_" + (stackerLineId + 1));

                ////if (_oPrinterStateColor is Button PrinterStateRec)
                ////{
                ////}



                ////byte timeDelay = 1;

                //if (_lineControlMain != null)
                //{
                //    //_lineControlMain.PaperCuter(stackerLineId, printerNo);
                //}
                //else
                //{
                //    MessageBox.Show("LCC is not running!");
                //}
            }
            catch
            { 
            }
        }
        private void PrinterCuter_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                FrameworkElement source = e.OriginalSource as FrameworkElement;
                byte stackerLineId = byte.Parse(source.Name.Substring(source.Name.Length - 1, 1));
                byte printerNo = byte.Parse(source.Name.Substring(source.Name.Length - 2, 1));
                if (_lineControlMain != null)
                {
                    _lineControlMain.PaperCuter(stackerLineId, printerNo);
                    _transStart[printerNo - 1] = false;
                }
                else
                {
                    MessageBox.Show("LCC is not running!");
                }
            }
            catch
            { 
            }
        }
        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Close();
            }
            catch
            {
            }
        }
    }
}
