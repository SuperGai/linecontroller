﻿using MainForm;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LineController
{

    static class LoginInfo
    {
        public static string ServerName;
        public static string DatabaseName;
        public static string UserNameLogin;
        public static string LoginPassword;
    }
    public class UserInfo
    {
        public string Name { get; set; }

        public string DataSource { get; set; }

        public string InitialCatalog { get; set; }

        public string IntegratedSecurity { get; set; }

        public string UserID { get; set; }

        public string Password { get; set; }

        public static List<UserInfo> Users
        {
            get
            {
                return new List<UserInfo>
                {
                     new UserInfo
                    {
                        Name = "SPH",
                        DataSource = "10.110.0.111",
                        InitialCatalog = "UniMail_SPH",
                        IntegratedSecurity="False",
                        UserID="sa",
                        Password = "DinCare2019"
                    },
                     new UserInfo
                    {
                        Name = "Local",
                        DataSource = "localhost",
                        InitialCatalog = "UniMail_SPH",
                        IntegratedSecurity="False",
                        UserID="sa",
                        Password = "DinCare2019"
                    }
                };
            }
        }
    }

    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        public static byte _lineId;

        public Login(byte LineId)
        {
            InitializeComponent();

            _lineId = LineId;
            UserComboBoxSelector.ItemsSource = UserInfo.Users;
            UserComboBoxSelector.DisplayMemberPath = "Name";

        }


        private void sLoginButton_Click(object sender, RoutedEventArgs e)
        {
            LoginInfo.ServerName = sSververNameText.Text;
            LoginInfo.DatabaseName = sDatabaseText.Text;
            LoginInfo.UserNameLogin = sUserNameText.Text;
            LoginInfo.LoginPassword = sPasswordText.Password;

            string connsql = "Data Source=" + LoginInfo.ServerName + ";Initial Catalog=" + LoginInfo.DatabaseName + ";Integrated Security=False;User ID=" + LoginInfo.UserNameLogin + ";Password=" + LoginInfo.LoginPassword + "";
            SqlConnection conn = new SqlConnection(connsql);
            try
            {
                conn.Open();
                if (conn.State != ConnectionState.Open)
                {
                    MessageBox.Show("Database Can not Connect, Please Try again!");
                }
                else
                {
                    MainWindow MainWindowShow = new MainWindow(_lineId);
                    Close();
                    MainWindowShow.ShowDialog();
                }
            }
            catch
            {
            }
            finally
            {
                conn.Close();
            }
        }

        private void UserComboBoxSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            //Get the Value from ComboBox(using in secection changed event)
            object obj = e.AddedItems;
            string ComboBoxValue = Convert.ToString(((UserInfo)(((object[])(obj))[0])).Name);

            //string ComboBoxValue = UserComboBoxSelector.SelectedValue.ToString();
            foreach (UserInfo userInfo in UserInfo.Users)
            {
                if (userInfo.Name == ComboBoxValue)
                {
                    sSververNameText.Text = userInfo.DataSource;
                    sDatabaseText.Text = userInfo.InitialCatalog;
                    sUserNameText.Text = userInfo.UserID;
                    sPasswordText.Password = userInfo.Password;
                }
            }

        }

        private void CloseButton1_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MessageBoxResult result = MessageBox.Show("Do you really want to Exit?", "DinCare File Import", MessageBoxButton.OKCancel, MessageBoxImage.Question);
                if (result == MessageBoxResult.OK)
                {
                    Close();
                }
                else
                {
                    return;
                }
            }
            catch
            {
            }
        }
    }
}
