﻿using LineController;
using System.Diagnostics;
using System.Windows;

namespace MainForm
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            string exeFileName = System.Reflection.Assembly.GetExecutingAssembly().GetModules()[0].Name.ToLower().Replace(".exe", "");
            bool isRunning = false;
            Process[] processes = Process.GetProcesses();
            int processIndex = -1;
            while (!isRunning && ++processIndex < processes.Length)
            {
                Process processRunning = processes[processIndex];
                if (processRunning.Id != Process.GetCurrentProcess().Id)
                {
                    string processName = processRunning.ProcessName.ToLower().Replace(".exe", "");
                    isRunning = processName.Equals(exeFileName);
                }
            }

            if (isRunning)
            {
                MessageBox.Show("Cannot Start - LineController is Already Running " + exeFileName);
                Current.Shutdown();
            }
        }

        private void App_Startup(object sender, StartupEventArgs e)
        {
            // Application is running
            // Process command line args
            //bool startMinimized = false;
            byte CurLineId = 4;
            bool isBackup = false;
            int LineParameter = 0;

            for (int i = 0; i != e.Args.Length; ++i)
            {
                if (e.Args[i] != "")
                {
                    LineParameter = int.Parse(e.Args[i]);
                }
                else
                {
                    LineParameter = 0;
                }

                if (LineParameter > 10)
                {
                    isBackup = true;
                    CurLineId = (byte)(LineParameter - 10);
                }
                else
                {
                    CurLineId = (byte)LineParameter;
                }
               
                //if (int.Parse(e.Args[i]) > 10)
                //{
                //    isBackup = true;
                //    if (e.Args[i] == "11") { CurLineId = 1; }
                //    else if (e.Args[i] == "12") { CurLineId = 2; }
                //    else if (e.Args[i] == "13") { CurLineId = 3; }
                //    else if (e.Args[i] == "14") { CurLineId = 4; }
                //    else if (e.Args[i] == "15") { CurLineId = 5; }
                //    else if (e.Args[i] == "16") { CurLineId = 6; }
                //    else if (e.Args[i] == "17") { CurLineId = 7; }
                //    else if (e.Args[i] == "18") { CurLineId = 8; }
                //    else { CurLineId = 1; }
                //}
                //else
                //{
                //    if (e.Args[i] == "1") { CurLineId = 1; }
                //    else if (e.Args[i] == "2") { CurLineId = 2; }
                //    else if (e.Args[i] == "3") { CurLineId = 3; }
                //    else if (e.Args[i] == "4") { CurLineId = 4; }
                //    else if (e.Args[i] == "5") { CurLineId = 5; }
                //    else if (e.Args[i] == "6") { CurLineId = 6; }
                //    else if (e.Args[i] == "7") { CurLineId = 7; }
                //    else if (e.Args[i] == "8") { CurLineId = 8; }
                //    else { CurLineId = 1; }
                //}
                
            }

            //StackerLineSelector _stackerSelect = new StackerLineSelector(CurLineId);
            //_stackerSelect.ShowDialog
            if (isBackup)
            {
                Login LoginWindow = new Login(CurLineId);
                LoginWindow.Show();
            }
            else
            {
                MainWindow mainWindow = new MainWindow(CurLineId);
                mainWindow.Show();
            }

        }
    }
}
