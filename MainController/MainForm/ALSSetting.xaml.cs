﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using LineControllerLib;

namespace MainForm
{
    /// <summary>
    /// Interaction logic for ALSSetting.xaml
    /// </summary>
    public partial class ALSSetting : Window
    {
        public string conn = MainWindow._connectionStringServer;
        private LineControlMain _lineControlMain;
        public byte curLineId;
        public const int MAXSTACKERLINE = 4;
        public Dictionary<int, List<string>> ABLGroupInfo = new Dictionary<int, List<string>>();

        public enum ResultType
        {
            OK = 0,
            UnknownError = 1,
            DatabaseError = 2,
            LCCNotStarted = 3,
            ALSError = 4,
            TimeOut = 5
        }

        public enum ControlType
        {
            Label = 0,
            TextBox = 1
        }

        public ALSSetting(byte _curLineId, LineControlMain _line)
        {
            InitializeComponent();
            curLineId = _curLineId;
            _lineControlMain = _line;

            if (_lineControlMain != null)
            {
                _lineControlMain.ABLGroupCommandReceived = false;
            }

            //ABLGroupShow();  // Modify Layout and Get the Data From DB

            for (int i = 1; i <= MAXSTACKERLINE; i++)
            {
                ALSParameterGet(i);
            }
            ALSParameterShowFromDB();
            ALSParameterShowFromALS();
        }

        //public void ABLGroupShow()
        //{
        //    switch (curLineId)
        //    {
        //        case 1: //Line C
        //            ABLGroup1.Visibility = Visibility.Visible;
        //            ABLGroup2.Visibility = Visibility.Collapsed;
        //            ABLGroup3.Visibility = Visibility.Collapsed;
        //            ABLGroup4.Visibility = Visibility.Collapsed;
        //            Width = 530;

        //            ALSParameterGet(1);
        //            break;

        //        case 2: //Line B (3 Groups Visible)
        //            ABLGroup1.Visibility = Visibility.Collapsed;
        //            ABLGroup2.Visibility = Visibility.Visible;
        //            ABLGroup3.Visibility = Visibility.Visible;
        //            ABLGroup4.Visibility = Visibility.Visible;

        //            ABLGroup4.Margin = new Thickness(220, 0, 0, 0);
        //            ABLGroup3.Margin = new Thickness(525, 0, 0, 0);
        //            ABLGroup2.Margin = new Thickness(830, 0, 0, 0);
        //            for (int i = 2; i <= 4; i++)
        //            {
        //                ALSParameterGet(i);
        //            }

        //            break;

        //        case 3: // Line A (2 Groups Visible)

        //            ABLGroup1.Visibility = Visibility.Collapsed;
        //            ABLGroup2.Visibility = Visibility.Collapsed;
        //            ABLGroup3.Visibility = Visibility.Visible;
        //            ABLGroup4.Visibility = Visibility.Visible;
        //            Width = 835;

        //            ABLGroup4.Margin = new Thickness(220, 0, 0, 0);
        //            ABLGroup3.Margin = new Thickness(525, 0, 0, 0);

        //            for (int i = 3; i <= 4; i++)
        //            {
        //                ALSParameterGet(i);
        //            }
        //            break;

        //        default:
        //            break;

        //    }
        //}

        private void Drag_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                if (e.LeftButton == MouseButtonState.Pressed)
                {
                    this.DragMove();
                }
            }
            catch { }
        }

        private void ALSParameterGet(int stackerLineId)
        {
            SqlConnection sqlCon = new SqlConnection(conn);

            try
            {
                SqlCommand cmd = new SqlCommand("select * from dbo.UniStack_LCCGetABLGroupParameters(" + curLineId + "," + stackerLineId + ")", sqlCon)
                {
                    CommandType = CommandType.Text
                };
                sqlCon.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                List<string> _tempList = new List<string>();

                while (reader.Read())
                {
                    _tempList.Add(reader["CountABLDistance"].ToString());
                    _tempList.Add(reader["ReleaseStartDelay"].ToString());
                    _tempList.Add(reader["ReleaseStopDelay"].ToString());
                    _tempList.Add(reader["StrikeStartDelay"].ToString());
                    _tempList.Add(reader["StrikeStopDelay"].ToString());
                    _tempList.Add(reader["LayerCycleTime"].ToString());
                    _tempList.Add(reader["BundleCycleTime"].ToString());
                    _tempList.Add(reader["ABLStopJam"].ToString());
                    _tempList.Add(reader["TTRStopJam"].ToString());

                    if (ABLGroupInfo.ContainsKey(stackerLineId))
                    {
                        ABLGroupInfo[stackerLineId] = _tempList;
                    }
                    else
                    {
                        ABLGroupInfo.Add(stackerLineId, _tempList);
                    }
                }

                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Line " + curLineId + " Bundle Show Error: " + ex.ToString());
            }
            finally
            {
                sqlCon.Close();
            }
        }

        private void ALSParameterShowFromDB()
        {
            List<object> ControlListForDB = new List<object>();
            try
            {
                foreach (int key in ABLGroupInfo.Keys)
                {
                    ControlListForDB = FindControlInMainForm(key - 1, ControlType.TextBox);

                    for (int i = 0; i < ControlListForDB.Count; i++)
                    {
                        if (ControlListForDB[i] is TextBox _tempText)
                        {
                            _tempText.Text = ABLGroupInfo[key][i];
                        }
                    }
                }
            }
            catch
            {
            }
        }

        private List<object> FindControlInMainForm(int _index, ControlType _controlType)
        {
            List<object> _controlList = new List<object>();
            if (_controlType == ControlType.TextBox)
            {
                _controlList.Add(MainInfo.FindName("ABLDistanceTextBox" + _index));
                _controlList.Add(MainInfo.FindName("ReleaseStartTextBox" + _index));
                _controlList.Add(MainInfo.FindName("ReleaseStopTextBox" + _index));
                _controlList.Add(MainInfo.FindName("StrikeStartTextBox" + _index));
                _controlList.Add(MainInfo.FindName("StrikeStopTextBox" + _index));
                _controlList.Add(MainInfo.FindName("LayerCycleTextBox" + _index));
                _controlList.Add(MainInfo.FindName("BundleCycleTextBox" + _index));
                _controlList.Add(MainInfo.FindName("ABLJamTextBox" + _index));
                _controlList.Add(MainInfo.FindName("UTRJamTextBox" + _index));
                
            }
            else if (_controlType == ControlType.Label)
            {
                _controlList.Add(MainInfo.FindName("ABLDistanceLabel" + _index));
                _controlList.Add(MainInfo.FindName("ReleaseStartLabel" + _index));
                _controlList.Add(MainInfo.FindName("ReleaseStopLabel" + _index));
                _controlList.Add(MainInfo.FindName("StrikeStartLabel" + _index));
                _controlList.Add(MainInfo.FindName("StrikeStopLabel" + _index));
                _controlList.Add(MainInfo.FindName("LayerCycleLabel" + _index));
                _controlList.Add(MainInfo.FindName("BundleCycleLabel" + _index));
                _controlList.Add(MainInfo.FindName("ABLJamLabel" + _index));
                _controlList.Add(MainInfo.FindName("UTRJamLabel" + _index));
            }
            return _controlList;
        }

        private void ALSParameterShowFromALS()
        {
            try
            {
                List<object> ControlListForALS = new List<object>();

                for (int i = 0; i < MAXSTACKERLINE; i++)
                {
                    ControlListForALS = FindControlInMainForm(i, ControlType.Label);

                    if (_lineControlMain != null)
                    {
                        if (//_lineControlMain.StackerList[i].TopSheetMain != null &&
                            _lineControlMain.StackerList[i].PosLineID == curLineId)
                        {

                            if (ControlListForALS[0] is Label ABLDistance)
                            {
                                ABLDistance.Content = _lineControlMain._status.ABLGroupParameter[i].iDistToFirstABL;
                            }

                            if (ControlListForALS[1] is Label ReleaseStart)
                            {
                                ReleaseStart.Content = _lineControlMain._status.ABLGroupParameter[i].iReleaseStartDelay;
                            }

                            if (ControlListForALS[2] is Label ReleaseStop)
                            {
                                ReleaseStop.Content = _lineControlMain._status.ABLGroupParameter[i].iReleaseStopDelay;
                            }

                            if (ControlListForALS[3] is Label StrikeStart)
                            {
                                StrikeStart.Content = _lineControlMain._status.ABLGroupParameter[i].iStrikeStartDelay;
                            }

                            if (ControlListForALS[4] is Label StrikeStop)
                            {
                                StrikeStop.Content = _lineControlMain._status.ABLGroupParameter[i].iStrikeStopDelay;
                            }

                            if (ControlListForALS[5] is Label LayerCycle)
                            {
                                LayerCycle.Content = _lineControlMain._status.ABLGroupParameter[i].iCycleTimeLayer;
                            }

                            if (ControlListForALS[6] is Label BundleCycle)
                            {
                                BundleCycle.Content = _lineControlMain._status.ABLGroupParameter[i].iCycleTimeBundle;
                            }

                            if (ControlListForALS[7] is Label ABLJamStop)
                            {
                                ABLJamStop.Content = _lineControlMain._status.ABLGroupParameter[i].cJamGrippersToStopABL;
                            }

                            if (ControlListForALS[8] is Label UTRJamStop)
                            {
                                UTRJamStop.Content = _lineControlMain._status.ABLGroupParameter[i].cJamGrippersToStopGripper.ToString();
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Please Check ALS: " + ex.ToString());
            }
            
        }
        
        private ResultType SaveABLGroupParameterToDB(int stackeLineId)
        {
            ResultType result = ResultType.UnknownError;

            string _tempABLDistance = "";
            string _tempReleaseStart = "";
            string _tempReleaseStop = "";
            string _tempStrikeStart = "";
            string _tempStrikeStop = "";
            string _tempLayerCycle = "";
            string _tempBundleCycle = "";
            string _tempABLJamStop = "";
            string _tempUTRJamStop = "";
            List<object> ControlList = new List<object>();

            try
            {
                using (SqlConnection connection = new SqlConnection(conn))
                {
                    SqlCommand command = new SqlCommand("[dbo].[UniStack_LCCSetABLGroupParameters]", connection)
                    {
                        CommandText = "[UniStack_LCCSetABLGroupParameters]",
                        CommandType = CommandType.StoredProcedure
                    };

                    ControlList = FindControlInMainForm(stackeLineId - 1, ControlType.TextBox);

                    if (ControlList[0] is TextBox ABLDistance)
                    {
                        _tempABLDistance = ABLDistance.Text;
                    }

                    if (ControlList[1] is TextBox ReleaseStart)
                    {
                        _tempReleaseStart = ReleaseStart.Text;
                    }

                    if (ControlList[2] is TextBox ReleaseStop)
                    {
                        _tempReleaseStop = ReleaseStop.Text;
                    }

                    if (ControlList[3] is TextBox StrikeStart)
                    {
                        _tempStrikeStart = StrikeStart.Text;
                    }

                    if (ControlList[4] is TextBox StrikeStop)
                    {
                        _tempStrikeStop = StrikeStop.Text;
                    }

                    if (ControlList[5] is TextBox LayerCycle)
                    {
                        _tempLayerCycle = LayerCycle.Text;
                    }

                    if (ControlList[6] is TextBox BundleCycle)
                    {
                        _tempBundleCycle = BundleCycle.Text;
                    }

                    if (ControlList[7] is TextBox ABLJamStop)
                    {
                        _tempABLJamStop = ABLJamStop.Text;
                    }

                    if (ControlList[8] is TextBox UTRJamStop)
                    {
                        _tempUTRJamStop = UTRJamStop.Text;
                    }

                    command.Parameters.Add(new SqlParameter("@LineId", curLineId));
                    command.Parameters.Add(new SqlParameter("@StackerLineId", stackeLineId));
                    command.Parameters.Add(new SqlParameter("@CountABLDistance", _tempABLDistance));
                    command.Parameters.Add(new SqlParameter("@ReleaseStartDelay", _tempReleaseStart));
                    command.Parameters.Add(new SqlParameter("@ReleaseStopDelay", _tempReleaseStop));
                    command.Parameters.Add(new SqlParameter("@StrikeStartDelay", _tempStrikeStart));
                    command.Parameters.Add(new SqlParameter("@StrikeStopDelay", _tempStrikeStop));
                    command.Parameters.Add(new SqlParameter("@LayerCycleTime", _tempLayerCycle));
                    command.Parameters.Add(new SqlParameter("@BundleCycleTime", _tempBundleCycle));
                    command.Parameters.Add(new SqlParameter("@ABLStopJam", _tempABLJamStop));
                    command.Parameters.Add(new SqlParameter("@TTRStopJam", _tempUTRJamStop));

                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    reader.Close();
                } //From DB
            }
            catch
            {
                result = ResultType.DatabaseError;
            }

            try
            {
                if (_lineControlMain != null) // Send To ALS
                {
                    _lineControlMain.StackerLineIdForABLGroupSetting = stackeLineId;
                    _lineControlMain.CountABLDistance = int.Parse(_tempABLDistance);
                    _lineControlMain.ReleaseStartDelay = int.Parse(_tempReleaseStart);
                    _lineControlMain.ReleaseStopDelay = int.Parse(_tempReleaseStop);
                    _lineControlMain.StrikeStartDelay = int.Parse(_tempStrikeStart);
                    _lineControlMain.StrikeStopDelay = int.Parse(_tempStrikeStop);
                    _lineControlMain.LayerCycleTime = int.Parse(_tempLayerCycle);
                    _lineControlMain.BundleCycleTime = int.Parse(_tempBundleCycle);
                    _lineControlMain.ABLStopJam = int.Parse(_tempABLJamStop);
                    _lineControlMain.UTRStopJam = int.Parse(_tempUTRJamStop);

                    _lineControlMain.IcpGetOrAddTelegramStacker(LineControlMain.EnumIcpTelegramTypes.SendABLGroupSize, null, true);

                    DateTime _sendTime = DateTime.Now;

                    while (!_lineControlMain.ABLGroupCommandReceived)
                    {
                        if (DateTime.Now > _sendTime.AddSeconds(3))
                        {
                            result = ResultType.TimeOut;
                            return result;
                        }
                    }

                    result = ResultType.OK;
                    MessageBox.Show("Update Ok!");
                    //Close();

                    //ALSSetting ALSSettingPage = new ALSSetting(curLineId, _lineControlMain);
                    //try
                    //{
                    //    ALSSettingPage.ShowDialog();
                    //}
                    //catch
                    //{
                    //}
                }
                else
                {
                    result = ResultType.LCCNotStarted;
                }
            }
            catch
            {
                result = ResultType.ALSError;
            }

            return result;
        }

        #region Click Event
        private void Group1Save_Click(object sender, RoutedEventArgs e)
        {
            ResultType result = SaveABLGroupParameterToDB(1);
            if (result != ResultType.OK)
            {
                MessageBox.Show("Not Successful: " + result, "DinCare LineController");
            }
            else
            {
                UpdateABLGroupParameters(1);
            }
        }

        private void UpdateABLGroupParameters(int _stackerLineId)
        {
            List<object> ControlListLabel = new List<object>();
            List<object> ControlListTextBox = new List<object>();

            ControlListLabel = FindControlInMainForm(_stackerLineId - 1, ControlType.Label);
            ControlListTextBox = FindControlInMainForm(_stackerLineId - 1, ControlType.TextBox);

            if (ControlListLabel.Count == ControlListTextBox.Count)
            {
                for (int i = 0; i < ControlListTextBox.Count; i++)
                {
                    if (ControlListLabel[i] is Label _tempLabel)
                    {
                        if (ControlListTextBox[i] is TextBox _tempTextBox)
                        {
                            _tempLabel.Content = _tempTextBox.Text;
                        }
                    }
                }
            }
        }

        private void Group2Save_Click(object sender, RoutedEventArgs e)
        {
            ResultType result = SaveABLGroupParameterToDB(2);
            if (result != ResultType.OK)
            {
                MessageBox.Show("Not Successful: " + result, "DinCare LineController");
            }
            else
            {
                UpdateABLGroupParameters(2);
            }
        }

        private void Group3Save_Click(object sender, RoutedEventArgs e)
        {
            ResultType result = SaveABLGroupParameterToDB(3);
            if (result != ResultType.OK)
            {
                MessageBox.Show("Not Successful: " + result, "DinCare LineController");
            }
            else
            {
                UpdateABLGroupParameters(3);
            }
        }

        private void Group4Save_Click(object sender, RoutedEventArgs e)
        {
            ResultType result = SaveABLGroupParameterToDB(4);
            if (result != ResultType.OK)
            {
                MessageBox.Show("Not Successful: " + result, "DinCare LineController");
            }
            else
            {
                UpdateABLGroupParameters(4);
            }
        }

        private void Group1Cancer_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Group2Cancer_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Group3Cancer_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Group4Cancer_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
        #endregion

        private void Refesh_Click(object sender, RoutedEventArgs e)
        {
            ALSParameterShowFromALS();
        }
    }
}
