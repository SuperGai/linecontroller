﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using SnmpSharpNet;
using System.Net;

namespace MainForm
{
    public class SetupBAStructure
    {
        //public int[] test;
        public short[] LabelReceiveExpected { get; set; }
        public short[] LabelReceiveTimeout { get; set; }
        public short[] LabelStartDealy { get; set; }
        public short[] LabelStopDelayLeave { get; set; }
        public short[] BundleReceiveExpected { get; set; }
        public short[] BundleReceiveTimeout { get; set; }
        public short[] BundleStopDelay { get; set; }

        public short[] Arm { get; set; }

        //short ArmDownMinTime;
        //short ArmDownTimeout;
        //short ArmUpTimeout;

        //short RejectDownTime;

    }


    class HaukDB
    {
        public static string connsql = 
            "Data Source=" + MainWindow.ConnectionString[0] + ";" +
            "Initial Catalog=" + MainWindow.ConnectionString[1] + ";" +
            "Integrated Security=False;" +
            "User ID=" + MainWindow.ConnectionString[2] + ";" +
            "Password=" + MainWindow.ConnectionString[3] + "";

        public static string LastProductionLineStatusSql = null;


        static byte[] ConvertInt16TobyteListay(short ui)
       {
            byte[] arry = new byte[2];
            arry[0] = (byte)(ui & 0xFF);
            arry[1] = (byte)((ui & 0xFF00) >> 8);

            return arry;

       }

        public static List<byte> SendSetupToBA(int BAID)
        {
            List<byte> byteList = new List<byte>();

            //setup.test = new int[] { 1, 2 };
            //short[] ll = new short[4] {1,2,3,4 };
            //setup.LabelReceiveExpected = new short[] { 1, 2, 3 };
            //setup.LabelReceiveExpected[1] = 1;//position1
            //setup.LabelReceiveExpected[1] = 2;
            //setup.LabelReceiveExpected[1] = 3;
            //setup.LabelReceiveExpected[1] = 4;
            //String connsql = "Data Source=172.22.22.150;Initial Catalog=UniMail_Sydost;Integrated Security=False;User ID=sa;Password=UniMail2017";
            SqlConnection conn = new SqlConnection(connsql);
            String sql = "select BaId,TypeId,FieldName,Position1Value, Position2Value, Position3Value, Position4Value from UniStack_BASetupTable where BaId = " + BAID + " order by TypeId asc";
            SqlDataAdapter myda = new SqlDataAdapter(sql, conn);
            // DataSet ds = new DataSet();
            // myda.Fill(ds, "BA");
            // DataTable dt = ds.Tables["BA"];
            //  SqlCommand cmd = new SqlCommand(sql, conn);

            DataTable dt = new DataTable();
            myda.Fill(dt);
            //byte[] b1 = new byte[2];
            //byte[] b2 = new byte[2];
            //byte[] b3 = new byte[2];
            //byte[] b4 = new byte[2];
            //byte[] b5 = new byte[2];
            //byte[] b6 = new byte[2];
            //byte[] b7 = new byte[2];
            //byte[] b8 = new byte[2];

            SetupBAStructure setup = new SetupBAStructure();
            setup.LabelReceiveExpected = new short[] {
            Convert.ToInt16(dt.Rows[0][3]),
            Convert.ToInt16(dt.Rows[0][4]),
            Convert.ToInt16(dt.Rows[0][5]),
            Convert.ToInt16(dt.Rows[0][6]) };

            setup.LabelReceiveTimeout = new short[] {
            Convert.ToInt16(dt.Rows[1][3]),
            Convert.ToInt16(dt.Rows[1][4]),
            Convert.ToInt16(dt.Rows[1][5]),
            Convert.ToInt16(dt.Rows[1][6])};

            setup.LabelStartDealy = new short[] {
            Convert.ToInt16(dt.Rows[2][3]),
            Convert.ToInt16(dt.Rows[2][4]),
            Convert.ToInt16(dt.Rows[2][5]),
            Convert.ToInt16(dt.Rows[2][6])};

            setup.LabelStopDelayLeave = new short[]{
            Convert.ToInt16(dt.Rows[3][3]),
            Convert.ToInt16(dt.Rows[3][4]),
            Convert.ToInt16(dt.Rows[3][5]),
            Convert.ToInt16(dt.Rows[3][6])};


            byte[] b;
            for (int i=0; i<4; i++)
            {
                b = ConvertInt16TobyteListay(setup.LabelReceiveExpected[i]); byteList.Add(b[0]); byteList.Add(b[1]);
                b = ConvertInt16TobyteListay(setup.LabelReceiveTimeout[i]); byteList.Add(b[0]); byteList.Add(b[1]);
                b = ConvertInt16TobyteListay(setup.LabelStartDealy[i]); byteList.Add(b[0]); byteList.Add(b[1]);
                b = ConvertInt16TobyteListay(setup.LabelStopDelayLeave[i]); byteList.Add(b[0]); byteList.Add(b[1]);
            }


            setup.BundleReceiveTimeout = new short[]{
            Convert.ToInt16(dt.Rows[4][3]),
            Convert.ToInt16(dt.Rows[4][4]),
            Convert.ToInt16(dt.Rows[4][5]),
            Convert.ToInt16(dt.Rows[4][6])};

            setup.LabelStopDelayLeave = new short[] {
            Convert.ToInt16(dt.Rows[5][3]),
            Convert.ToInt16(dt.Rows[5][4]),
            Convert.ToInt16(dt.Rows[5][5]),
            Convert.ToInt16(dt.Rows[5][6])};

            setup.BundleStopDelay = new short[]{
            Convert.ToInt16(dt.Rows[6][3]),
            Convert.ToInt16(dt.Rows[6][4]),
            Convert.ToInt16(dt.Rows[6][5]),
            Convert.ToInt16(dt.Rows[6][6]) };

            for(int i=0;  i<4; i++)
            {
                b = ConvertInt16TobyteListay(setup.BundleReceiveTimeout[i]); byteList.Add(b[0]); byteList.Add(b[1]);
                b = ConvertInt16TobyteListay(setup.LabelStopDelayLeave[i]); byteList.Add(b[0]); byteList.Add(b[1]);
                b = ConvertInt16TobyteListay(setup.BundleStopDelay[i]); byteList.Add(b[0]); byteList.Add(b[1]);
            }

            setup.Arm = new short[] {
            Convert.ToInt16(dt.Rows[7][3]),
            Convert.ToInt16(dt.Rows[8][3]),
            Convert.ToInt16(dt.Rows[9][3]),
            Convert.ToInt16(dt.Rows[10][3]) };

            b = ConvertInt16TobyteListay(setup.Arm[0]); byteList.Add(b[0]); byteList.Add(b[1]);
            b = ConvertInt16TobyteListay(setup.Arm[1]); byteList.Add(b[0]); byteList.Add(b[1]);
            b = ConvertInt16TobyteListay(setup.Arm[2]); byteList.Add(b[0]); byteList.Add(b[1]);
            b = ConvertInt16TobyteListay(setup.Arm[3]); byteList.Add(b[0]); byteList.Add(b[1]);

            //SqlCommandBuilder cb = new SqlCommandBuilder(myda);   
            //dt.Rows[7][3] = 12345;
            //myda.Update(dt);
            //conn.Close();

            //foreach (byte i in byteList)
            //{
            //    Console.WriteLine(i);
            //}

            return byteList;
        }

        public static DataTable GetTLCParameter(int id)
        {
            DataTable dt = new DataTable();
            SqlConnection conn = new SqlConnection(connsql);
            String sql = "select * from UniStack_TlcSetupTable where TlcId = " + id ;
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            da.Fill(dt);
            conn.Close();
            return dt;
        }

        public static void SavePrinterStatus(String ip)
        {
            try
            {
                List<String> paraList = GetPrinterStatus(ip);
                foreach (String s in paraList)
                {
                    Console.WriteLine(s);
                }
                float f1 = Convert.ToInt32(paraList[0]);
                float f2 = Convert.ToInt32(paraList[1]);
                float toner = 0;
                if(f2 != 0)
                {
                    toner = f1 / f2;
                }

                float f3 = Convert.ToInt32(paraList[2]);
                float f4 = Convert.ToInt32(paraList[3]);
                float tray1 = f3 / 500;
                float tray2 = f4 / 500;
                //if( paraList[0] == "Ready")
                // {
                //     ready = 3; 
                // }
                //String connsql = "Data Source=172.22.22.150;Initial Catalog=UniMail_Sydost;Integrated Security=False;User ID=sa;Password=UniMail2017";
                SqlConnection conn = new SqlConnection(connsql);
                String sql = "update [dbo].[UniStack_PrinterTable] set [TonerLevel] = " + toner * 100
                    + ",[PaperTray1]= " + tray1 * 100 + ",[PaperTray2] =" + tray2 * 100 + ",[PrinterStatus]='" 
                    + paraList[4] + "', [LastPrinterStatusUpdate] = '" + DateTime.Now.ToString() + "'where [PrinterIP] = '" + ip + "'";
                Console.WriteLine(sql);
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.ExecuteNonQuery();
                conn.Close();

                
            }
            catch
            {

            }
        }

        
        public static List<string> GetPrinterStatus(string ip)
        {
            List<string> statusList = new List<string>();
            try
            {
                OctetString community = new OctetString("public");

                AgentParameters param = new AgentParameters(community);
                param.Version = SnmpVersion.Ver1;
                IpAddress agent = new IpAddress(ip);

                // Construct target  
                UdpTarget target = new UdpTarget((IPAddress)agent, 161, 2000, 1);

                // Pdu class used for all requests  
                Pdu pdu = new Pdu(PduType.Get);
                //pdu.VbList.Add("1.3.6.1.2.1.1.1.0"); //sysDescr  
                //pdu.VbList.Add("1.3.6.1.2.1.1.2.0"); //sysObjectID  
                //pdu.VbList.Add("1.3.6.1.2.1.1.3.0"); //sysUpTime  
                //pdu.VbList.Add("1.3.6.1.2.1.1.4.0"); //sysContact  
                //pdu.VbList.Add("1.3.6.1.2.1.1.5.0"); //sysName  

                // pdu.VbList.Add("1.3.6.1.2.1.43.16.5.1.2.1.1");  //Ready
                // pdu.VbList.Add("1.3.6.1.2.1.25.3.2.1.5.1");
                // pdu.VbList.Add("1.3.6.1.2.1.43.11.1.1.6.1.1"); //Toner container

                pdu.VbList.Add("1.3.6.1.2.1.43.11.1.1.9.1.1"); //current Toner 
                pdu.VbList.Add("1.3.6.1.2.1.43.11.1.1.8.1.1");//Max Toner
                                                              //pdu.VbList.Add("1.3.6.1.2.1.43.5.1.1.1.1");//
                pdu.VbList.Add(".1.3.6.1.2.1.43.8.2.1.10.1.2"); //Cassette 1 paper
                pdu.VbList.Add(".1.3.6.1.2.1.43.8.2.1.10.1.3");  //Cassette 2 paper
                pdu.VbList.Add(".1.3.6.1.4.1.1347.43.18.2.1.2.1.1");//Is Ready
                // Make SNMP request  
                SnmpV1Packet result = (SnmpV1Packet)target.Request(pdu, param);

                if (result != null)
                {
                    // ErrorStatus other then 0 is an error returned by   
                    // the Agent - see SnmpConstants for error definitions  
                    if (result.Pdu.ErrorStatus != 0)
                    {
                        // agent reported an error with the request  
                        //   txtContent.Text += string.Format("Error in SNMP reply. Error {0} index {1} \r\n",
                        //   result.Pdu.ErrorStatus,
                        //    result.Pdu.ErrorIndex);
                    }
                    else
                    {
                        // Reply variables are returned in the same order as they were added  
                        //  to the VbList  
                        for (int i = 0; i < result.Pdu.VbList.Count; i++)
                        {
                            //  txtContent.Text += string.Format("({0}) ({1}): {2} \r\n",
                            //    result.Pdu.VbList[i].Oid.ToString(), SnmpConstants.GetTypeName(result.Pdu.VbList[i].Value.Type),
                            //   result.Pdu.VbList[i].Value.ToString());

                            statusList.Add(result.Pdu.VbList[i].Value.ToString());
                        }


                    }
                }
                else
                {
                    // txtContent.Text += string.Format("No response received from SNMP agent. \r\n");
                }
                target.Dispose();

                return statusList;

            }
            catch
            {
                return statusList;
            }

        }

    }

}
