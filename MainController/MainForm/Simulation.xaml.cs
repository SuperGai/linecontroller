﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using LineControllerLib;
namespace MainForm
{
    /// <summary>
    /// Interaction logic for Simulation.xaml
    /// </summary>
    public partial class Simulation : Window
    {
        private LineControlMain line;
        private int LineId;
        public Simulation(LineControlMain l, int id)
        {

            InitializeComponent();
            line = l;
            LineId = id;
            this.InkFinishPulseTimeout.Text = "800";
            this.InkMsDelayAfterFinish.Text = "1";
        }

        private void StartSimulation_Click(object sender, RoutedEventArgs e)

        {
            short[] sh = new short [5]{ 0,0,0,0,0};

            if (InkComboBox.SelectedIndex == 0) { sh[0] = (short)1; }

            if (PrintAllCopyCheckBox.IsChecked == true)
            {
                sh[1] = 1;
            }
            
              
                sh[2] = Convert.ToInt16(InkFinishPulseTimeout.Text);
                sh[3] = Convert.ToInt16(InkMsDelayAfterFinish.Text);

            



          
            sh[4] = (short)(CopyComboBox.SelectedIndex + 1);


            SetupStacker s = new SetupStacker(line, LineId);
            line.RequestSetupSendToStackerHauk(s.GetHaukSetupStructure(), sh);
            this.StartSimulation.IsEnabled = false;
            Console.WriteLine("------Start Simulation-----");
        }

        private void StopSimulation_Click(object sender, RoutedEventArgs e)
        {

            SetupStacker s = new SetupStacker(line, LineId);
            line.RequestSetupSendToStackerHauk(s.GetHaukSetupStructure(), null);
            this.InkComboBox.SelectedIndex = 0;
            this.StartSimulation.IsEnabled = true;
            Console.WriteLine("------Stop Simulation-----");
        }

        private void InkComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //if(this.InkComboBox.SelectedIndex == 1)
            //{
            //    this.InkFinishPulseTimeout.IsEnabled = false;
            //    this.InkMsDelayAfterFinish.IsEnabled = false;
            //}

            //if (this.InkComboBox.SelectedIndex == 0)
            //{
            //    this.InkFinishPulseTimeout.IsEnabled = true;
            //    this.InkMsDelayAfterFinish.IsEnabled = true;
            //}
        }
    }
}
