﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using LineControllerLib;
namespace MainForm
{
    /// <summary>
    /// Interaction logic for BA_Test.xaml
    /// </summary>
    public partial class BA_Test : Window
    {
        public class Signal : INotifyPropertyChanged
        {
            public int signalValue { get; set; }
            public Brush signalColor { get; set; }


            public int SignalValue
            {
                get { return signalValue; }
                set
                {
                    signalValue = value;
                    if (PropertyChanged != null)
                    {
                        this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("SignalValue"));
                    }
                }
            }

            public Brush SignalColor
            {
                get { return signalColor; }
                set
                {
                    signalColor = value;
                    if (PropertyChanged != null)
                    {
                        this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("SignalColor"));
                    }
                }
            }

            public event PropertyChangedEventHandler PropertyChanged;
        }

        private int OutputSignal1 = 0;
        private int OutputSignal2 = 0;
        private int OutputSignal3 = 0;
        private int OutputSignal4 = 0;
        private int OutputSignal5 = 0;
        private int OutputSignal6 = 0;
        private int OutputSignal7 = 0;
        private int OutputSignal8 = 0;
        ushort us1;
        ushort us2;
        private int BA;
        private LineControlMain line;
        Signal[] sigArr1;
        Signal[] sigArr2;
        List<Ellipse> eList1 = new List<Ellipse>();
        List<Ellipse> eList2 = new List<Ellipse>();
        Thread refreshThread;

        public BA_Test(int BAID, LineControlMain lineId)
        {
            InitializeComponent();
            BA = BAID;
            line = lineId;
            Ellipse e;
            Thickness myThickness = new Thickness();
            Canvas c;
            for (int i = 0; i < 8; i++)
            {
                e = new Ellipse();
                myThickness.Left = (200 * i - 711);
                myThickness.Top = -500;
                e.Margin = myThickness;
                c = new Canvas();
                c.Width = 45;
                c.Height = 45;
                c.Margin = myThickness;
                c.Background = Brushes.Black;
                e.Width = 40;
                e.Height = 40;
                e.Stroke = Brushes.Black;
                e.Fill = Brushes.Red;
                eList1.Add(e);

                MainGrid.Children.Add(c);
                MainGrid.Children.Add(e);
            }

            for (int i = 0; i < 8; i++)
            {
                e = new Ellipse();
                myThickness.Left = (200 * i - 711);
                myThickness.Top = -350;
                e.Margin = myThickness;
                c = new Canvas();
                c.Width = 45;
                c.Height = 45;
                c.Margin = myThickness;
                c.Background = Brushes.Black;
                e.Width = 40;
                e.Height = 40;
                e.Stroke = Brushes.Black;
                e.Fill = Brushes.Red;
                eList1.Add(e);

                MainGrid.Children.Add(c);
                MainGrid.Children.Add(e);
            }


            for (int i = 0; i < 8; i++)
            {
                e = new Ellipse();
                myThickness.Left = (200 * i - 711);
                myThickness.Top = -150;
                e.Margin = myThickness;

                c = new Canvas();
                c.Width = 45;
                c.Height = 45;
                c.Margin = myThickness;
                c.Background = Brushes.Black;
                e.Width = 40;
                e.Height = 40;
                e.Stroke = Brushes.Black;
                e.Fill = Brushes.Red;
                eList2.Add(e);

                MainGrid.Children.Add(c);
                MainGrid.Children.Add(e);
            }

            for (int i = 0; i < 8; i++)
            {
                e = new Ellipse();
                myThickness.Left = (200 * i - 711);
                myThickness.Top = -0;
                e.Margin = myThickness;

                c = new Canvas();
                c.Width = 45;
                c.Height = 45;
                c.Margin = myThickness;
                c.Background = Brushes.Black;
                e.Width = 40;
                e.Height = 40;
                e.Stroke = Brushes.Black;
                e.Fill = Brushes.Red;
                eList2.Add(e);

                MainGrid.Children.Add(c);
                MainGrid.Children.Add(e);
            }

            Binding bind = new Binding();
            sigArr1 = new Signal[16];
            sigArr2 = new Signal[16];
            for (int i = 0; i < 16; i++)
            {
                sigArr1[i] = new Signal();
                sigArr1[i].SignalColor = Brushes.Gray;
                bind = new Binding();
                bind.Source = sigArr1[i];
                bind.Path = new PropertyPath("SignalColor");
                eList1[i].SetBinding(Ellipse.FillProperty, bind);
            }

            for (int i = 0; i < 16; i++)
            {
                sigArr2[i] = new Signal();
                sigArr2[i].SignalColor = Brushes.Gray;
                bind = new Binding();
                bind.Source = sigArr2[i];
                bind.Path = new PropertyPath("SignalColor");
                eList2[i].SetBinding(Ellipse.FillProperty, bind);
            }

            refreshThread = new Thread(Refresh);
            try
            {
                refreshThread.Start();
            }
            catch
            {

            }
           
        }

        public void Refresh()
        {
            us1 = line.GetInputSignal(0, BA);
            us2 = line.GetInputSignal(1, BA);
            while (true)
            {
                for (int i = 0; i < 16; i++)
                {
                    if ((us1 >> i & 1) == 1)
                    {
                        sigArr1[i].SignalColor = Brushes.LimeGreen;
                    }
                    else
                    {
                        sigArr1[i].SignalColor = Brushes.Gray;

                    }
                }
                for (int i = 0; i < 16; i++)
                {
                    if ((us2 >> i & 1) == 1)
                    {
                        sigArr2[i].SignalColor = Brushes.LimeGreen;
                    }
                    else
                    {
                        sigArr2[i].SignalColor = Brushes.Gray;

                    }
                }
                us1 = line.GetInputSignal(0, BA);
                us2 = line.GetInputSignal(1, BA);
                Thread.Sleep(200);
            }
        }

        private void OutputButton1_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            List<byte> byteList = new List<byte>(); ;
            byteList.Add(1);
            byteList.Add(0);
            if (OutputSignal1 == 0)
            {
                OutputButton1.Source = new BitmapImage(new Uri("/on.png", UriKind.Relative));
                OutputSignalLight1.Fill = Brushes.Red;
                OutputSignal1 = 1;
                byteList[1] = 1;
                line.SendTestSignalToBA(BA, byteList);

            }
            else
            {
                OutputButton1.Source = new BitmapImage(new Uri("/off.png", UriKind.Relative));
                OutputSignal1 = 0;
                byteList[1] = 0;
                line.SendTestSignalToBA(BA, byteList);
                OutputSignalLight1.Fill = Brushes.White;
            }
        }

        private void OutputButton2_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            List<byte> byteList = new List<byte>();
            byteList.Add(2);
            byteList.Add(0);
            if (OutputSignal2 == 0)
            {
                OutputButton2.Source = new BitmapImage(new Uri("/on.png", UriKind.Relative));
                OutputSignalLight2.Fill = Brushes.Red;
                OutputSignal2 = 1;
                byteList[1] = 1;
                line.SendTestSignalToBA(BA, byteList);


            }
            else
            {
                OutputButton2.Source = new BitmapImage(new Uri("/off.png", UriKind.Relative));
                OutputSignal2 = 0;
                byteList[1] = 0;
                line.SendTestSignalToBA(BA, byteList);
                OutputSignalLight2.Fill = Brushes.White;
            }
        }

        private void OutputButton3_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            List<byte> byteList = new List<byte>(); ;
            byteList.Add(3); byteList.Add(0);
            if (OutputSignal3 == 0)
            {
                OutputButton3.Source = new BitmapImage(new Uri("/on.png", UriKind.Relative));
                OutputSignalLight3.Fill = Brushes.Red;
                OutputSignal3 = 1;
                byteList[1] = 1;
                line.SendTestSignalToBA(BA, byteList);

            }
            else
            {
                OutputButton3.Source = new BitmapImage(new Uri("/off.png", UriKind.Relative));
                OutputSignal3 = 0;
                byteList[1] = 0;
                line.SendTestSignalToBA(BA, byteList);
                OutputSignalLight3.Fill = Brushes.White;
            }
        }

        private void OutputButton4_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            List<byte> byteList = new List<byte>(); ;
            byteList.Add(4); byteList.Add(0);
            if (OutputSignal4 == 0)
            {
                OutputButton4.Source = new BitmapImage(new Uri("/on.png", UriKind.Relative));
                OutputSignalLight4.Fill = Brushes.Red;
                OutputSignal4 = 1;
                byteList[1] = 1;
                line.SendTestSignalToBA(BA, byteList);

            }
            else
            {
                OutputButton4.Source = new BitmapImage(new Uri("/off.png", UriKind.Relative));
                OutputSignal4 = 0;
                byteList[1] = 0;
                line.SendTestSignalToBA(BA, byteList);
                OutputSignalLight4.Fill = Brushes.White;
            }
        }

        private void OutputButton5_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            List<byte> byteList = new List<byte>(); ;
            byteList.Add(5); byteList.Add(0);
            if (OutputSignal5 == 0)
            {
                OutputButton5.Source = new BitmapImage(new Uri("/on.png", UriKind.Relative));
                OutputSignalLight5.Fill = Brushes.Red;
                OutputSignal5 = 1;
                byteList[1] = 1;
                line.SendTestSignalToBA(BA, byteList);

            }
            else
            {
                OutputButton5.Source = new BitmapImage(new Uri("/off.png", UriKind.Relative));
                OutputSignal5 = 0;
                byteList[1] = 0;
                line.SendTestSignalToBA(BA, byteList);
                OutputSignalLight5.Fill = Brushes.White;
            }
        }

        private void OutputButton6_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            List<byte> byteList = new List<byte>(); ;
            byteList.Add(6); byteList.Add(0);
            if (OutputSignal6 == 0)
            {
                OutputButton6.Source = new BitmapImage(new Uri("/on.png", UriKind.Relative));
                OutputSignalLight6.Fill = Brushes.Red;
                OutputSignal6 = 6;
                byteList[1] = 1;
                line.SendTestSignalToBA(BA, byteList);

            }
            else
            {
                OutputButton6.Source = new BitmapImage(new Uri("/off.png", UriKind.Relative));
                OutputSignal6 = 0;
                byteList[1] = 0;
                line.SendTestSignalToBA(BA, byteList);
                OutputSignalLight6.Fill = Brushes.White;
            }
        }

        private void OutputButton7_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            List<byte> byteList = new List<byte>(); ;
            byteList.Add(7); byteList.Add(0);
            if (OutputSignal7 == 0)
            {
                OutputButton7.Source = new BitmapImage(new Uri("/on.png", UriKind.Relative));
                OutputSignalLight7.Fill = Brushes.Red;
                OutputSignal7 = 1;
                byteList[1] = 1;
                line.SendTestSignalToBA(BA, byteList);

            }
            else
            {
                OutputButton7.Source = new BitmapImage(new Uri("/off.png", UriKind.Relative));
                OutputSignal7 = 0;
                byteList[1] = 0;
                line.SendTestSignalToBA(BA, byteList);
                OutputSignalLight7.Fill = Brushes.White;
            }
        }

        private void OutputButton8_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            List<byte> byteList = new List<byte>(); ;
            byteList.Add(8); byteList.Add(0);
            if (OutputSignal8 == 0)
            {
                OutputButton8.Source = new BitmapImage(new Uri("/on.png", UriKind.Relative));
                OutputSignalLight8.Fill = Brushes.Red;
                OutputSignal8 = 1;
                byteList[1] = 1;
                line.SendTestSignalToBA(BA, byteList);

            }
            else
            {
                OutputButton8.Source = new BitmapImage(new Uri("/off.png", UriKind.Relative));
                OutputSignal8 = 0;
                byteList[1] = 0;
                line.SendTestSignalToBA(BA, byteList);
                OutputSignalLight8.Fill = Brushes.White;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void Window_Closed(object sender, EventArgs e)
        {
            try
            {
            refreshThread.Abort();
            }
            catch
            {
            }
        }
    }
}
