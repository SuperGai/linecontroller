﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using LineControllerLib;
using System.Data.SqlClient;
using System.Data;

namespace MainForm
{
    /// <summary>
    /// Interaction logic for BA_Parameter.xaml
    /// </summary>
    /// 
    delegate List<byte> haukDB(int BAID);
    public partial class BA_Parameter : Window
    {
        public String connsql = 
            "Data Source="+MainWindow.ConnectionString[0]+";" +
            "Initial Catalog=" + MainWindow.ConnectionString[1] + ";" +
            "Integrated Security=False;" +
            "User ID=" + MainWindow.ConnectionString[2] + ";" +
            "Password=" + MainWindow.ConnectionString[3] + "";

        public void SetBAParameter(int BAID)
        {
            //String connsql = "Data Source=172.22.22.150;Initial Catalog=UniMail_Sydost;Integrated Security=False;User ID=sa;Password=UniMail2017";
            SqlConnection conn = new SqlConnection(connsql);
            String sql = "select BaId,TypeId,FieldName,Position1Value, Position2Value, Position3Value, Position4Value from UniStack_BASetupTable where BaId = " + BAID + " order by TypeId asc";
            SqlDataAdapter myda = new SqlDataAdapter(sql, conn);
            DataSet ds = new DataSet();
            // myda.Fill(ds, "BA");
            // DataTable dt = ds.Tables["BA"];
            //  SqlCommand cmd = new SqlCommand(sql, conn);

            DataTable dt = new DataTable();
            myda.Fill(dt);

            dt.Rows[0][3] = this.LReceiveExpectedP1.Text;
            dt.Rows[0][4] = this.LReceiveExpectedP2.Text;
            dt.Rows[0][5] = this.LReceiveExpectedP3.Text;
            dt.Rows[0][6] = this.LReceiveExpectedP4.Text;

            dt.Rows[1][3] = this.LReceiveTimeoutP1.Text;
            dt.Rows[1][4] = this.LReceiveTimeoutP2.Text;
            dt.Rows[1][5] = this.LReceiveTimeoutP3.Text;
            dt.Rows[1][6] = this.LReceiveTimeoutP4.Text;

            dt.Rows[2][3] = this.LStartDelayP1.Text;
            dt.Rows[2][4] = this.LStartDelayP2.Text;
            dt.Rows[2][5] = this.LStartDelayP3.Text;
            dt.Rows[2][6] = this.LStartDelayP4.Text;

            dt.Rows[3][3] = this.LStopDelayLeaveP1.Text;
            dt.Rows[3][4] = this.LStopDelayLeaveP2.Text;
            dt.Rows[3][5] = this.LStopDelayLeaveP3.Text;
            dt.Rows[3][6] = this.LStopDelayLeaveP4.Text;

            dt.Rows[4][3] = this.BReceiveExpectedP1.Text;
            dt.Rows[4][4] = this.BReceiveExpectedP2.Text;
            dt.Rows[4][5] = this.BReceiveExpectedP3.Text;
            dt.Rows[4][6] = this.BReceiveExpectedP4.Text;

            dt.Rows[5][3] = this.BReceiveTimeoutP1.Text;
            dt.Rows[5][4] = this.BReceiveTimeoutP2.Text;
            dt.Rows[5][5] = this.BReceiveTimeoutP3.Text;
            dt.Rows[5][6] = this.BReceiveTimeoutP4.Text;

            dt.Rows[6][3] = this.BStopDelayP1.Text;
            dt.Rows[6][4] = this.BStopDelayP2.Text;
            dt.Rows[6][5] = this.BStopDelayP3.Text;
            dt.Rows[6][6] = this.BStopDelayP4.Text;



            dt.Rows[7][3] = this.ArmDownMinTimeout.Text;
            dt.Rows[8][3] = this.ArmDownTimeout.Text;
            dt.Rows[9][3] = this.ArmUpTimeout.Text;
            dt.Rows[10][3] = this.Label_Copy29.Text;
            SqlCommandBuilder cb = new SqlCommandBuilder(myda);




           int result = myda.Update(dt);

           

            conn.Close();
            MessageBox.Show(result + " Rows changed");
            //  GetBAParameter(BAID);
        }

        public void GetBAParameter(int BAID)
        {
            //String connsql = "Data Source=172.22.22.150;Initial Catalog=UniMail_Sydost;Integrated Security=False;User ID=sa;Password=UniMail2017";
            SqlConnection conn = new SqlConnection(connsql);
            String sql = "select BaId,TypeId,FieldName,Position1Value, Position2Value, Position3Value, Position4Value from UniStack_BASetupTable where BaId = " + BAID + " order by TypeId asc";
            SqlDataAdapter myda = new SqlDataAdapter(sql, conn);
            DataSet ds = new DataSet();
            // myda.Fill(ds, "BA");
            // DataTable dt = ds.Tables["BA"];
            //  SqlCommand cmd = new SqlCommand(sql, conn);

            DataTable dt = new DataTable();
            myda.Fill(dt);


            this.LReceiveExpectedP1.Text = dt.Rows[0][3].ToString();
            this.LReceiveExpectedP2.Text = dt.Rows[0][4].ToString();
            this.LReceiveExpectedP3.Text = dt.Rows[0][5].ToString();
            this.LReceiveExpectedP4.Text = dt.Rows[0][6].ToString();

            this.LReceiveTimeoutP1.Text = dt.Rows[1][3].ToString();
            this.LReceiveTimeoutP2.Text = dt.Rows[1][4].ToString();
            this.LReceiveTimeoutP3.Text = dt.Rows[1][5].ToString();
            this.LReceiveTimeoutP4.Text = dt.Rows[1][6].ToString();

            this.LStartDelayP1.Text = dt.Rows[2][3].ToString();
            this.LStartDelayP2.Text = dt.Rows[2][4].ToString();
            this.LStartDelayP3.Text = dt.Rows[2][5].ToString();
            this.LStartDelayP4.Text = dt.Rows[2][6].ToString();

            this.LStopDelayLeaveP1.Text = dt.Rows[3][3].ToString();
            this.LStopDelayLeaveP2.Text = dt.Rows[3][4].ToString();
            this.LStopDelayLeaveP3.Text = dt.Rows[3][5].ToString();
            this.LStopDelayLeaveP4.Text = dt.Rows[3][6].ToString();


            this.BReceiveExpectedP1.Text = dt.Rows[4][3].ToString();
            this.BReceiveExpectedP2.Text = dt.Rows[4][4].ToString();
            this.BReceiveExpectedP3.Text = dt.Rows[4][5].ToString();
            this.BReceiveExpectedP4.Text = dt.Rows[4][6].ToString();

            this.BReceiveTimeoutP1.Text = dt.Rows[5][3].ToString();
            this.BReceiveTimeoutP2.Text = dt.Rows[5][4].ToString();
            this.BReceiveTimeoutP3.Text = dt.Rows[5][5].ToString();
            this.BReceiveTimeoutP4.Text = dt.Rows[5][6].ToString();

            this.BStopDelayP1.Text = dt.Rows[6][3].ToString();
            this.BStopDelayP2.Text = dt.Rows[6][4].ToString();
            this.BStopDelayP3.Text = dt.Rows[6][5].ToString();
            this.BStopDelayP4.Text = dt.Rows[6][6].ToString();

            this.ArmDownMinTimeout.Text = dt.Rows[7][3].ToString();
            this.ArmDownTimeout.Text = dt.Rows[8][3].ToString(); ;
            this.ArmUpTimeout.Text = dt.Rows[9][3].ToString();
            this.Label_Copy29.Text = dt.Rows[10][3].ToString();

            //SqlCommandBuilder cb = new SqlCommandBuilder(myda);   
            //dt.Rows[7][3] = 12345;
            //myda.Update(dt);
            conn.Close();

        }


        private LineControlMain line;
        private int BAID;
        public BA_Parameter(LineControlMain Line, int BaId)
        {
            InitializeComponent();
            line = Line;
            BAID = BaId;
            GetBAParameter(BaId);
            this.Title = "BA " + BaId.ToString();

        }

        private void Reset_Click(object sender, RoutedEventArgs e)
        {
            try { 
            //String connsql = "Data Source=172.22.22.150;Initial Catalog=UniMail_Sydost;Integrated Security=False;User ID=sa;Password=UniMail2017";
            SqlConnection conn = new SqlConnection(connsql);
            String sql = "select BaId,TypeId,FieldName,Position1Value, Position2Value, Position3Value, Position4Value from UniStack_BASetupTable where BaId = " + BAID + " order by TypeId asc";
            SqlDataAdapter myda = new SqlDataAdapter(sql, conn);
            DataSet ds = new DataSet();
            // myda.Fill(ds, "BA");
            // DataTable dt = ds.Tables["BA"];
            //  SqlCommand cmd = new SqlCommand(sql, conn);

            DataTable dt = new DataTable();
            myda.Fill(dt);

            dt.Rows[0][3] = this.LReceiveExpectedP1.Text;
            dt.Rows[0][4] = this.LReceiveExpectedP2.Text;
            dt.Rows[0][5] = this.LReceiveExpectedP3.Text;
            dt.Rows[0][6] = this.LReceiveExpectedP4.Text;

            dt.Rows[1][3] = this.LReceiveTimeoutP1.Text;
            dt.Rows[1][4] = this.LReceiveTimeoutP2.Text;
            dt.Rows[1][5] = this.LReceiveTimeoutP3.Text;
            dt.Rows[1][6] = this.LReceiveTimeoutP4.Text;

            dt.Rows[2][3] = this.LStartDelayP1.Text;
            dt.Rows[2][4] = this.LStartDelayP2.Text;
            dt.Rows[2][5] = this.LStartDelayP3.Text;
            dt.Rows[2][6] = this.LStartDelayP4.Text;

            dt.Rows[3][3] = this.LStopDelayLeaveP1.Text;
            dt.Rows[3][4] = this.LStopDelayLeaveP2.Text;
            dt.Rows[3][5] = this.LStopDelayLeaveP3.Text;
            dt.Rows[3][6] = this.LStopDelayLeaveP4.Text;

            dt.Rows[4][3] = this.BReceiveExpectedP1.Text;
            dt.Rows[4][4] = this.BReceiveExpectedP2.Text;
            dt.Rows[4][5] = this.BReceiveExpectedP3.Text;
            dt.Rows[4][6] = this.BReceiveExpectedP4.Text;

            dt.Rows[5][3] = this.BReceiveTimeoutP1.Text;
            dt.Rows[5][4] = this.BReceiveTimeoutP2.Text;
            dt.Rows[5][5] = this.BReceiveTimeoutP3.Text;
            dt.Rows[5][6] = this.BReceiveTimeoutP4.Text;

            dt.Rows[6][3] = this.BStopDelayP1.Text;
            dt.Rows[6][4] = this.BStopDelayP2.Text;
            dt.Rows[6][5] = this.BStopDelayP3.Text;
            dt.Rows[6][6] = this.BStopDelayP4.Text;



            dt.Rows[7][3] = this.ArmDownMinTimeout.Text;
            dt.Rows[7][4] = this.ArmDownTimeout.Text;
            dt.Rows[7][5] = this.ArmUpTimeout.Text;
            dt.Rows[7][6] = this.Label_Copy29.Text;
            SqlCommandBuilder cb = new SqlCommandBuilder(myda);


            

            myda.Update(dt);

            conn.Close();

            GetBAParameter(BAID);
            }
            catch
            {

            }

        }

        private void Apply_Click(object sender, RoutedEventArgs e)
        {
            SetBAParameter(BAID);

            List<byte> bl = HaukDB.SendSetupToBA(BAID);
            String result = line.SendSetupBAHauk(BAID, bl);
            if (result != null)
            {
                MessageBox.Show(result);
            }
            else
            {
                MessageBox.Show("SUCCESS!");
            }

            //line.TestSendSetupBAHauk();
        }

        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            this.SetBaParameter.Close();
        }

        private void TestSignalButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {

           
            BA_Test baTest = new BA_Test(BAID,line);
            baTest.Show();
            }
            catch //(Exception ex)
            {
                MessageBox.Show("The BA is not running on current Linecontroller!");
            }
        }
    }
}
