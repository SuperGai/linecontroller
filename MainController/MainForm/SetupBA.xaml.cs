﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using LineControllerLib;
namespace MainForm
{
    /// <summary>
    /// Interaction logic for SetupBA.xaml
    /// </summary>
    public partial class SetupBA : Window
    {
        LineControlMain _line;
        public SetupBA(LineControlMain Line, int LineId)
        {
            InitializeComponent();
            _line = Line;

        }

        private void BA1_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BA_Parameter BA1 = new BA_Parameter(_line, 1);
                BA1.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BA2_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BA_Parameter BA2 = new BA_Parameter(_line, 2);
                BA2.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BA3_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BA_Parameter BA3 = new BA_Parameter(_line, 3);
                BA3.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BA4_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BA_Parameter BA4 = new BA_Parameter(_line, 4);
                BA4.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
