﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Data.SqlClient;
using LineControllerLib;


namespace MainForm
{
    /// <summary>
    /// Interaction logic for SetupStacker.xaml
    /// </summary>
    /// 
    public partial class SetupStacker : Window
    {
        //public void send
        //IcpGetOrAddTelegramStacker(enumIcpTelegramTypes.ClearCounters, null, true);
        public static String connsql =
            "Data Source=" + MainWindow.ConnectionString[0] + ";" +
            "Initial Catalog=" + MainWindow.ConnectionString[1] + ";" +
            "Integrated Security=False;" +
            "User ID=" + MainWindow.ConnectionString[2] + ";" +
            "Password=" + MainWindow.ConnectionString[3] + "";

        private LineControlMain line;
        private int lineId;

        //private string _connectionStringServer;

        //private byte IsEnableInkjetDefault = 1;
        private short InkFinishPulseTimeoutDefault = 800;
        private short InkMsDelayAfterFinishDefault = 1;
        private byte[] InkjetParamter = new byte[5];

        public int GetStackerParameter(int LineId)
        {
            //String connsql = "Data Source=localhost;Initial Catalog=UniMail_Milano_Karlstad;Integrated Security=False;User ID=sa;Password=DinCare2018";
            SqlConnection conn = new SqlConnection(connsql);
            SqlCommand cmd = conn.CreateCommand();

            String sql = "Select * from UniStack_StackerSetupTable where LineId =" + LineId.ToString();

            Console.WriteLine(sql);
            cmd.CommandText = sql;
            conn.Open();
            cmd.ExecuteNonQuery();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                Console.WriteLine("SUCCESS");
                Console.WriteLine(dr[1].ToString());
            }
            else { Console.WriteLine("Wrong!"); }
            conn.Close();
            return 1;
        }

        public HaukSetupStructure[] GetHaukSetupStructure()
        {
            uint[] DPulseEntranceToIntercept = new uint[4];
            uint[] DPulseMarginRelease = new uint[4];
            uint[] DPulseReleaseToEntrance = new uint[4];
            ushort[] ReleaseDelay = new ushort[4];
            ushort[] ReleaseDistance = new ushort[4];
            ushort[] ReleaseStopDelay = new ushort[4]; ;
            byte[] ReleaseUpDownFlag = new byte[4];
            ushort[] StackerEjectMSDelay = new ushort[4];
            ushort[] StackerEjectMSTimeout = new ushort[4];

            ushort[] ReleaseDelayMedium = new ushort[4];
            ushort[] ReleaseDelayHigh = new ushort[4];

            ushort[] ReleaseStopDelayMedium = new ushort[4];
            ushort[] ReleaseStopDelayHigh = new ushort[4];

            DataTable dt1 = GetStackerParameterTable(1, lineId);
            DataTable dt2 = GetStackerParameterTable(2, lineId);
            DataTable dt3 = GetStackerParameterTable(3, lineId);
            DataTable dt4 = GetStackerParameterTable(4, lineId);
            DataTable[] dtArr = new DataTable[4];
            dtArr[0] = dt1;
            dtArr[1] = dt2;
            dtArr[2] = dt3;
            dtArr[3] = dt4;

            for (int i = 0; i < 4; i++)
            {
                DPulseEntranceToIntercept[i] = Convert.ToUInt32(dtArr[i].Rows[0][2].ToString());
                DPulseMarginRelease[i] = Convert.ToUInt32(dtArr[i].Rows[1][2].ToString());
                DPulseReleaseToEntrance[i] = Convert.ToUInt32(dtArr[i].Rows[2][2].ToString());
                ReleaseDelay[i] = Convert.ToUInt16(dtArr[i].Rows[3][2].ToString());
                ReleaseDistance[i] = Convert.ToUInt16(dtArr[i].Rows[4][2].ToString()); ;
                ReleaseStopDelay[i] = Convert.ToUInt16(dtArr[i].Rows[5][2].ToString());
                ReleaseUpDownFlag[i] = Convert.ToByte(dtArr[i].Rows[6][2].ToString()); ;
                StackerEjectMSDelay[i] = Convert.ToUInt16(dtArr[i].Rows[7][2].ToString());
                StackerEjectMSTimeout[i] = Convert.ToUInt16(dtArr[i].Rows[8][2].ToString());

                ReleaseDelayMedium[i] = Convert.ToUInt16(dtArr[i].Rows[9][2].ToString());
                ReleaseDelayHigh[i] = Convert.ToUInt16(dtArr[i].Rows[10][2].ToString());

                ReleaseStopDelayMedium[i] = Convert.ToUInt16(dtArr[i].Rows[11][2].ToString());
                ReleaseStopDelayHigh[i] = Convert.ToUInt16(dtArr[i].Rows[12][2].ToString());

            }



            HaukSetupStructure[] HaukSetup = new HaukSetupStructure[4];
            for (int i = 0; i < 4; i++)
            {

                HaukSetup[i] = new HaukSetupStructure();
                HaukSetup[i].DPulseEntranceToIntercept = DPulseEntranceToIntercept[i];
                HaukSetup[i].DPulseMarginRelease = DPulseMarginRelease[i];
                HaukSetup[i].DPulseReleaseToEntrance = DPulseReleaseToEntrance[i];
                HaukSetup[i].ReleaseDelay = ReleaseDelay[i];
                HaukSetup[i].ReleaseDistance = ReleaseDistance[i];
                HaukSetup[i].ReleaseStopDelay = ReleaseStopDelay[i];
                HaukSetup[i].ReleaseUpDownFlag = ReleaseUpDownFlag[i];
                HaukSetup[i].StackerEjectMSDelay = StackerEjectMSDelay[i];
                HaukSetup[i].StackerEjectMSTimeout = StackerEjectMSTimeout[i];

                HaukSetup[i].ReleaseDelayMedium = ReleaseDelayMedium[i];
                HaukSetup[i].ReleaseDelayHigh = ReleaseDelayHigh[i];

                HaukSetup[i].ReleaseStopDelayMedium = ReleaseStopDelayMedium[i];
                HaukSetup[i].ReleaseStopDelayHigh = ReleaseStopDelayHigh[i];
                //  Console.WriteLine(HaukSetup[i].GetString() + "\r\n");

            }
            return HaukSetup;
        }

        public void loadForm()
        {
            try
            {
            DataTable dt1 = GetStackerParameterTable(1, lineId);
            this.Stacker1DPulseEntranceToIntercept.Text = dt1.Rows[0][2].ToString();
            this.Stacker1DPulseMarginRelease.Text = dt1.Rows[1][2].ToString();
            this.Stacker1DPulseReleaseToEntrance.Text = dt1.Rows[2][2].ToString();
            this.Stacker1ReleaseDelay.Text = dt1.Rows[3][2].ToString();
            this.Stacker1ReleaseDistance.Text = dt1.Rows[4][2].ToString();
            this.Stacker1ReleaseStopDelay.Text = dt1.Rows[5][2].ToString();
            this.Stacker1ReleaseUpDownFlag.Text = dt1.Rows[6][2].ToString();
            this.Stacker1EjectMSDelay.Text = dt1.Rows[7][2].ToString();
            this.Stacker1EjectMSTimeout.Text = dt1.Rows[8][2].ToString();

            this.Stacker1ReleaseDelayMedium.Text = dt1.Rows[9][2].ToString();
            this.Stacker1ReleaseDelayHigh.Text = dt1.Rows[10][2].ToString();

            this.Stacker1ReleaseStopDelayMedium.Text = dt1.Rows[11][2].ToString();
            this.Stacker1ReleaseStopDelayHigh.Text = dt1.Rows[12][2].ToString();

            DataTable dt2 = GetStackerParameterTable(2, lineId);
            this.Stacker2DPulseEntranceToIntercept.Text = dt2.Rows[0][2].ToString();
            this.Stacker2DPulseMarginRelease.Text = dt2.Rows[1][2].ToString();
            this.Stacker2DPulseReleaseToEntrance.Text = dt2.Rows[2][2].ToString();
            this.Stacker2ReleaseDelay.Text = dt2.Rows[3][2].ToString();
            this.Stacker2ReleaseDistance.Text = dt2.Rows[4][2].ToString();
            this.Stacker2ReleaseStopDelay.Text = dt2.Rows[5][2].ToString();
            this.Stacker2ReleaseUpDownFlag.Text = dt2.Rows[6][2].ToString();
            this.Stacker2EjectMSDelay.Text = dt2.Rows[7][2].ToString();
            this.Stacker2EjectMSTimeout.Text = dt2.Rows[8][2].ToString();

            this.Stacker2ReleaseDelayMedium.Text = dt2.Rows[9][2].ToString();
            this.Stacker2ReleaseDelayHigh.Text = dt2.Rows[10][2].ToString();
     
            this.Stacker2ReleaseStopDelayMedium.Text = dt2.Rows[11][2].ToString();
            this.Stacker2ReleaseStopDelayHigh.Text = dt2.Rows[12][2].ToString();


            DataTable dt3 = GetStackerParameterTable(3, lineId);
            this.Stacker3DPulseEntranceToIntercept.Text = dt3.Rows[0][2].ToString();
            this.Stacker3DPulseMarginRelease.Text = dt3.Rows[1][2].ToString();
            this.Stacker3DPulseReleaseToEntrance.Text = dt3.Rows[2][2].ToString();
            this.Stacker3ReleaseDelay.Text = dt3.Rows[3][2].ToString();
            this.Stacker3ReleaseDistance.Text = dt3.Rows[4][2].ToString();
            this.Stacker3ReleaseStopDelay.Text = dt3.Rows[5][2].ToString();
            this.Stacker3ReleaseUpDownFlag.Text = dt3.Rows[6][2].ToString();
            this.Stacker3EjectMSDelay.Text = dt3.Rows[7][2].ToString();
            this.Stacker3EjectMSTimeout.Text = dt3.Rows[8][2].ToString();

            this.Stacker3ReleaseDelayMedium.Text = dt3.Rows[9][2].ToString();
            this.Stacker3ReleaseDelayHigh.Text = dt3.Rows[10][2].ToString();
            this.Stacker3ReleaseStopDelayMedium.Text = dt3.Rows[11][2].ToString();
            this.Stacker3ReleaseStopDelayHigh.Text = dt3.Rows[12][2].ToString();

            DataTable dt4 = GetStackerParameterTable(4, lineId);
            this.Stacker4DPulseEntranceToIntercept.Text = dt4.Rows[0][2].ToString();
            this.Stacker4DPulseMarginRelease.Text = dt4.Rows[1][2].ToString();
            this.Stacker4DPulseReleaseToEntrance.Text = dt4.Rows[2][2].ToString();
            this.Stacker4ReleaseDelay.Text = dt4.Rows[3][2].ToString();
            this.Stacker4ReleaseDistance.Text = dt4.Rows[4][2].ToString();
            this.Stacker4ReleaseStopDelay.Text = dt4.Rows[5][2].ToString();
            this.Stacker4ReleaseUpDownFlag.Text = dt4.Rows[6][2].ToString();
            this.Stacker4EjectMSDelay.Text = dt4.Rows[7][2].ToString();
            this.Stacker4EjectMSTimeout.Text = dt4.Rows[8][2].ToString();

            this.Stacker4ReleaseDelayMedium.Text = dt4.Rows[9][2].ToString();
            this.Stacker4ReleaseDelayHigh.Text = dt4.Rows[10][2].ToString();
            this.Stacker4ReleaseStopDelayMedium.Text = dt4.Rows[11][2].ToString();
            this.Stacker4ReleaseStopDelayHigh.Text = dt4.Rows[12][2].ToString();
            }
            catch
            {

            }
        }

        public DataTable GetStackerParameterTable(int StackerId, int LineId)
        {
            String sql = "Select *from UniStack_StackerSetupTable where StackerId = " + StackerId.ToString() + " and  LineId = " + LineId.ToString();
            DataTable dt = new DataTable();

            try
            {
                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = connsql;
                    conn.Open();
                    SqlDataAdapter myda = new SqlDataAdapter(sql, conn);
                    myda.Fill(dt);
                    return dt;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error:" + ex.Message, "DB error!");
            }

            return dt;


        }

        public String SqlString(String value, String parameter, String StackerId, int LineId)
        {
            return "Update  UniStack_StackerSetupTable set FieldValue= " + value + " where  Fieldname = '" + parameter + "' and StackerId = " + StackerId + "and LineId = " + LineId.ToString();
        }
        
        // Reset One Stacker
        //public int ResetStackerParameter(int StackerId,int LineId)
        //{
        //    String connsql = "Data Source=172.22.22.150;Initial Catalog=UniMail_Sydost;Integrated Security=False;User ID=sa;Password=UniMail2017";
        //    String sql = "update UniStack_StackerSetupTable set FieldValue = FieldValueDefault where  StackerId = " + StackerId.ToString() + "and  LineId = " + LineId.ToString();
        //    sql += StackerId.ToString();
        //    int r =0;
        //    try {
        //        using (SqlConnection conn = new SqlConnection())
        //        {
        //            conn.ConnectionString = connsql;
        //            conn.Open();
        //            SqlCommand cmd= new SqlCommand(sql, conn);
        //         r = cmd.ExecuteNonQuery();

        //            Console.WriteLine(r); 
        //        }
        //    } catch (Exception ex)
        //    {
        //        MessageBox.Show("Error:" + ex.Message, "DB error!");
        //    }

        //    return r ;
        //}

        public int ResetStackerParameterAll(int LineId)
        {
            //String connsql = "Data Source=localhost;Initial Catalog=UniMail_Milano_Karlstad;Integrated Security=False;User ID=sa;Password=DinCare2018";
            String sql = "update UniStack_StackerSetupTable set FieldValue = FieldValueDefault where LineId = " + LineId.ToString();

            int r = 0;
            try
            {
                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = connsql;
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(sql, conn);
                    r = cmd.ExecuteNonQuery();

                    // Console.WriteLine(r);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error:" + ex.Message, "DB error!");
            }

            return r;
        }

        public int SetStackerParameter(int StackerId)
        {
            //String connsql = "Data Source=172.22.22.150;Initial Catalog=UniMail_Sydost;Integrated Security=False;User ID=sa;Password=UniMail2017";
            //String connsql = "Data Source=localhost;Initial Catalog=UniMail_Milano_Karlstad;Integrated Security=False;User ID=sa;Password=DinCare2018";


            //String DPulseEntranceToIntercept = this.Stacker1DPulseEntranceToIntercept.Text;
            //String DPulseMarginRelease = this.Stacker1DPulseMarginRelease.Text;
            //String DPulseReleaseToEntrance = this.Stacker1DPulseReleaseToEntrance.Text;
            //String ReleaseDelay = this.Stacker1ReleaseDelay.Text;
            //String ReleaseDistance = this.Stacker1ReleaseDistance.Text;
            //String ReleaseStopDelay = this.Stacker1ReleaseStopDelay.Text;
            //String ReleaseUpDownFlag = this.Stacker1ReleaseUpDownFlag.Text;
            //String StackerEjectMSDelay = this.Stacker1EjectMSDelay.Text;
            //String StackerEjectMSTimeout = this.Stacker1EjectMSTimeout.Text;

            String[] DPulseEntranceToInterceptArr = new String[4] { this.Stacker1DPulseEntranceToIntercept.Text, this.Stacker2DPulseEntranceToIntercept.Text,
                this.Stacker3DPulseEntranceToIntercept.Text,this.Stacker4DPulseEntranceToIntercept.Text };

            String[] DPulseMarginReleaseArr = new String[4] { this.Stacker1DPulseMarginRelease.Text,  this.Stacker2DPulseMarginRelease.Text,
                this.Stacker3DPulseMarginRelease.Text, this.Stacker4DPulseMarginRelease.Text };

            String[] DPulseReleaseToEntranceArr = new String[4] { this.Stacker1DPulseReleaseToEntrance.Text,  this.Stacker2DPulseReleaseToEntrance.Text,
               this.Stacker3DPulseReleaseToEntrance.Text, this.Stacker4DPulseReleaseToEntrance.Text };

            String[] ReleaseDelayArr = new String[4] { this.Stacker1ReleaseDelay.Text,  this.Stacker2ReleaseDelay.Text,
               this.Stacker3ReleaseDelay.Text, this.Stacker4ReleaseDelay.Text };

            String[] ReleaseDistanceArr = new String[4] { this.Stacker1ReleaseDistance.Text,  this.Stacker2ReleaseDistance.Text,
               this.Stacker3ReleaseDistance.Text, this.Stacker4ReleaseDistance.Text };

            String[] ReleaseStopDelayArr = new String[4] { this.Stacker1ReleaseStopDelay.Text,  this.Stacker2ReleaseStopDelay.Text,
               this.Stacker3ReleaseStopDelay.Text, this.Stacker4ReleaseStopDelay.Text };

            String[] ReleaseUpDownFlagArr = new String[4] { this.Stacker1ReleaseUpDownFlag.Text,  this.Stacker2ReleaseUpDownFlag.Text,
               this.Stacker3ReleaseUpDownFlag.Text, this.Stacker4ReleaseUpDownFlag.Text };

            String[] StackerEjectMSDelayArr = new String[4] { this.Stacker1EjectMSDelay.Text,  this.Stacker2EjectMSDelay.Text,
               this.Stacker3EjectMSDelay.Text, this.Stacker4EjectMSDelay.Text };

            String[] StackerEjectMSTimeoutArr = new String[4] { this.Stacker1EjectMSTimeout.Text,  this.Stacker2EjectMSTimeout.Text,
               this.Stacker3EjectMSTimeout.Text, this.Stacker4EjectMSTimeout.Text };


            String[] StackerReleaseDelayMediumArr = new String[4] { this.Stacker1ReleaseDelayMedium.Text,  this.Stacker2ReleaseDelayMedium.Text,
               this.Stacker3ReleaseDelayMedium.Text, this.Stacker4ReleaseDelayMedium.Text };
            String[] StackerReleaseDelayHighArr = new String[4] { this.Stacker1ReleaseDelayHigh.Text,  this.Stacker2ReleaseDelayHigh.Text,
               this.Stacker3ReleaseDelayHigh.Text, this.Stacker4ReleaseDelayHigh.Text };

            String[] StackerReleaseStopDelayMediumArr= new String[4] { this.Stacker1ReleaseStopDelayMedium.Text,  this.Stacker2ReleaseStopDelayMedium.Text,
               this.Stacker3ReleaseStopDelayMedium.Text, this.Stacker4ReleaseStopDelayMedium.Text };
            String[] StackerReleaseStopDelayHighArr = new String[4] { this.Stacker1ReleaseStopDelayHigh.Text,  this.Stacker2ReleaseStopDelayHigh.Text,
               this.Stacker3ReleaseStopDelayHigh.Text, this.Stacker4ReleaseStopDelayHigh.Text };


            String SqlDPulseEntranceToIntercept = SqlString(DPulseEntranceToInterceptArr[StackerId - 1], "DPulseEntranceToIntercept", StackerId.ToString(), lineId);
            String SqlDPulseMarginRelease = SqlString(DPulseMarginReleaseArr[StackerId - 1], "DPulseMarginRelease", StackerId.ToString(), lineId);
            String SqlDPulseReleaseToEntrance = SqlString(DPulseReleaseToEntranceArr[StackerId - 1], "DPulseReleaseToEntrance", StackerId.ToString(), lineId);
            String SqlReleaseDelay = SqlString(ReleaseDelayArr[StackerId - 1], "ReleaseDelay", StackerId.ToString(), lineId);
            String SqlReleaseDistance = SqlString(ReleaseDistanceArr[StackerId - 1], "ReleaseDistance", StackerId.ToString(), lineId);
            String SqlReleaseStopDelay = SqlString(ReleaseStopDelayArr[StackerId - 1], "ReleaseStopDelay", StackerId.ToString(), lineId);
            String SqlReleaseUpDownFlag = SqlString(ReleaseUpDownFlagArr[StackerId - 1], "ReleaseUpDownFlag", StackerId.ToString(), lineId);
            String SqlEjectMSDelay = SqlString(StackerEjectMSDelayArr[StackerId - 1], "StackerEjectMSDelay", StackerId.ToString(), lineId);
            String SqlSEjectMSTimeout = SqlString(StackerEjectMSTimeoutArr[StackerId - 1], "StackerEjectMSTimeout", StackerId.ToString(), lineId);

            String SqlReleaseDelayMedium = SqlString(StackerReleaseDelayMediumArr[StackerId - 1], "ReleaseDelayMedium", StackerId.ToString(), lineId);
            String SqlReleaseDelayHigh = SqlString(StackerReleaseDelayHighArr[StackerId - 1], "ReleaseDelayHigh", StackerId.ToString(), lineId);

            String SqlReleaseStopDelayMedium = SqlString(StackerReleaseStopDelayMediumArr[StackerId - 1], "ReleaseStopDelayMedium", StackerId.ToString(), lineId);
            String SqlReleaseStopDelayHigh = SqlString(StackerReleaseStopDelayHighArr[StackerId - 1], "ReleaseStopDelayHigh", StackerId.ToString(), lineId);



            // Console.WriteLine(Stacker1EjectMSTimeout);
            int r = 0;
            try
            {
                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = connsql;
                    SqlCommand cmd1 = new SqlCommand(SqlDPulseEntranceToIntercept, conn);
                    SqlCommand cmd2 = new SqlCommand(SqlDPulseMarginRelease, conn);
                    SqlCommand cmd3 = new SqlCommand(SqlDPulseReleaseToEntrance, conn);
                    SqlCommand cmd4 = new SqlCommand(SqlReleaseDelay, conn);
                    SqlCommand cmd5 = new SqlCommand(SqlReleaseDistance, conn);
                    SqlCommand cmd6 = new SqlCommand(SqlReleaseStopDelay, conn);
                    SqlCommand cmd7 = new SqlCommand(SqlReleaseUpDownFlag, conn);
                    SqlCommand cmd8 = new SqlCommand(SqlEjectMSDelay, conn);
                    SqlCommand cmd9 = new SqlCommand(SqlSEjectMSTimeout, conn);

                    SqlCommand cmd10 = new SqlCommand(SqlReleaseDelayMedium, conn);
                    SqlCommand cmd11 = new SqlCommand(SqlReleaseDelayHigh, conn);
                    SqlCommand cmd12 = new SqlCommand(SqlReleaseStopDelayMedium, conn);
                    SqlCommand cmd13 = new SqlCommand(SqlReleaseStopDelayHigh, conn);
                    conn.Open();
                    int r1 = cmd1.ExecuteNonQuery();
                    int r2 = cmd2.ExecuteNonQuery();
                    int r3 = cmd3.ExecuteNonQuery();
                    int r4 = cmd4.ExecuteNonQuery();
                    int r5 = cmd5.ExecuteNonQuery();
                    int r6 = cmd6.ExecuteNonQuery();
                    int r7 = cmd7.ExecuteNonQuery();
                    int r8 = cmd8.ExecuteNonQuery();
                    int r9 = cmd9.ExecuteNonQuery();
                    int r10 = cmd10.ExecuteNonQuery();
                    int r11 = cmd11.ExecuteNonQuery();
                    int r12 = cmd12.ExecuteNonQuery();
                    int r13 = cmd13.ExecuteNonQuery();

                    r = 1;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error:" + ex.Message, "DB error!");
            }

            return r;


        }

        public SetupStacker(LineControlMain LineP, int LineId)
        {
            InitializeComponent();

            // GetStackerParameter();

            //string DBServerIP = "10.110.0.102";//"172.22.22.150"; //"10.45.6.129"; //172.22.22.150
            //string DBServerIP = "localhost";
            //_connectionStringServer = "Provider=SQLOLEDB.1; Server=" + DBServerIP + "; Database=UniMail_Mialno_Karlstad; Uid=sa; Pwd=DinCare2018";


            line = LineP;
            lineId = LineId;
            this.InkjetCheckBox.IsChecked = true;
            this.InkFinishPulseTimeout.Text = InkFinishPulseTimeoutDefault.ToString();
            this.InkMsDelayAfterFinish.Text = InkMsDelayAfterFinishDefault.ToString();



        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void StackerParameter_Loaded(object sender, RoutedEventArgs e)
        {

            loadForm();
        }

        private void ResetStackerAll_Click(object sender, RoutedEventArgs e)
        {

            MessageBoxResult result = MessageBox.Show("Are sure to Reset the parameter?", "Warning!", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {

                int re = ResetStackerParameterAll(lineId);
                if (re >= 1)
                {
                    loadForm();

                    line.RequestSetupSendToStackerHauk(GetHaukSetupStructure(), null);
                    MessageBox.Show("SUCCESS!");
                }
                else
                {
                    MessageBox.Show("Failed!");
                }

            }
            else
            {
                loadForm();
            }

        }

        private void ApplyAll_Click(object sender, RoutedEventArgs e)
        {

            short[] sh = new short[5] { 0,0, 0, 0, 0 };
            if (PrintAllCopyCheckBox.IsChecked == true) { sh[1] = 1; }

            if (InkjetCheckBox.IsChecked == true) {sh[0] = 1; }

            sh[2] = Convert.ToInt16(InkFinishPulseTimeout.Text);
            sh[3] = Convert.ToInt16(InkMsDelayAfterFinish.Text);


            MessageBoxResult result = MessageBox.Show("Are sure to modify the parameter?", "Warning!", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {

                int r1 = SetStackerParameter(1);
                int r2 = SetStackerParameter(2);
                int r3 = SetStackerParameter(3);
                int r4 = SetStackerParameter(4);

                if (r1 > 0 || r2 > 0 || r3 > 0 || r4 > 0)
                {
                    loadForm();

                    line.RequestSetupSendToStackerHauk(GetHaukSetupStructure(), sh);
                    MessageBox.Show("SUCCESS!");
                }
                else
                {
                    MessageBox.Show("Failed!");
                }


            }
            else
            {
                loadForm();
            }






            //line2.RequestSetupSendToStacker();


        }

        private void Title_Loaded(object sender, RoutedEventArgs e)
        {
            if (lineId == 1)
            {
                this.CurTitle.Content = "Gray Line";
                this.CurTitle.Background = Brushes.Gray;


            }
            else if (lineId == 2)
            {
                this.CurTitle.Content = "Blue Line";
                this.CurTitle.Background = Brushes.RoyalBlue;
            }
        }

        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {

            this.StackerParameter.Close();
        }

        private void Simulate_Click(object sender, RoutedEventArgs e)
        {

            Simulation s = new Simulation(line, lineId);
            s.Show();
        }

        private void InkjetCheckBox_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void Stacker1ReleaseDelay_Copy_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
