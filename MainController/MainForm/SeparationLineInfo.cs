﻿using System.Collections.Generic;

namespace MainForm
{
    public class SeparationLineInfo
    {
        public string Name { get; set; }
        public byte Id { get; set; }
        public int index { get; set; }

        public static bool isOneLine { get; set; }

        public static int curLineId { get; set; }

        public static List<SeparationLineInfo> SeparationLineIndex
        {
            get
            {

                if (!isOneLine)
                {
                    return new List<SeparationLineInfo>
                    {
                        new SeparationLineInfo { Name = "P2211" , Id = 15, index = 0 },
                        new SeparationLineInfo { Name = "P2_1_" , Id = 16, index = 1 },
                        new SeparationLineInfo { Name = "P2_11" , Id = 17, index = 2 },
                        new SeparationLineInfo { Name = "P221_" , Id = 18, index = 3 }
                    };
                }
                else
                {
                    if (curLineId % 2 == 0)
                    {
                        return new List<SeparationLineInfo>
                        {
                            new SeparationLineInfo { Name = "P2222" , Id = 8, index = 0 },
                            new SeparationLineInfo { Name = "P2___" , Id = 9, index = 1 },
                            new SeparationLineInfo { Name = "P2_2_" , Id = 10, index = 2 },
                            new SeparationLineInfo { Name = "P2_22" , Id = 11, index = 3 },
                            new SeparationLineInfo { Name = "P222_" , Id = 12, index = 4 },
                            new SeparationLineInfo { Name = "P22__" , Id = 13, index = 5 },
                            new SeparationLineInfo { Name = "P2__2" , Id = 14, index = 6 }
                        };
                    }
                    else
                    {
                        return new List<SeparationLineInfo>
                        {
                            new SeparationLineInfo { Name = "P1111" , Id = 1, index = 0 },
                            new SeparationLineInfo { Name = "P1___" , Id = 2, index = 1 },
                            new SeparationLineInfo { Name = "P1_1_" , Id = 3, index = 2 },
                            new SeparationLineInfo { Name = "P1_11" , Id = 4, index = 3 },
                            new SeparationLineInfo { Name = "P111_" , Id = 5, index = 4 },
                            new SeparationLineInfo { Name = "P11__" , Id = 6, index = 5 },
                            new SeparationLineInfo { Name = "P1__1" , Id = 7, index = 6 }
                        };
                    }

                }

            }
        }
    }
}
