﻿using System;
using System.Windows;
using System.Windows.Threading;
using System.Threading;
using LineControllerLib;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Media;
using System.Collections.Generic;
using System.Windows.Data;
using System.Windows.Controls;
using System.Windows.Shapes;
using System.Threading.Tasks;
using System.Windows.Markup;
using LineController;

namespace MainForm
{
    public partial class MainWindow : Window
    {
        private const byte NUM_ABL = 3;
        private const byte NUM_DBH = 2;
        private const byte NUM_STACKERLINE = 4;

        

        public static string RunningStatusColor = "#548938";
        public static string NotRunningStatusColor = "#FFCB0909";
        public static string UnknownStatusColor = "#857f7f";
        public static string UnknownABLStatusColor = "#585858";
        public static string NotRunningABLStatusColor = "#ea4335";
        public static string PausedABLGroupStatusColor = "#f7ac4d";
        public static string PausedABLStatusColor = "#d77649";
        public static string RunningABLGroupColor = "#fddf63";

        private DispatcherTimer ShowTimer;
        private LineControlMain lineControlMain; //LCC1

        public static string _connectionStringServer;

        public bool _isClickedButton;
        public bool _isDuringStarting;
        public bool _isSTDWithoutTop;
        public bool _isNoTopsheet;
        //public bool _isOneLine; // Is ALL Abl On The Same Gripple Line

        private readonly string[] LineIPString;
        bool[] isStoppedABL;
        public static byte CurLineId { get; set; } //Current Gripper Line Id
        public static byte CurStackerLineId { get; set; }
        //public static int PrinterTonerState { get; set; }
        //public static int PrinterTopPaperState { get; set; }
        //public static int PrinterBottomPaperState { get; set; }
        public static string PrintingStatus { get; set; }
        //public static string RibbonState { get; set; }
        public static string StatusMessage { get; set; }
        

        public static string PrinterState { get; set; }
        public static bool ProductionTag;
        public static bool ModeTag;

        public string[] stackerNo = { "A", "B", "C", "D" };

        public static string [] ConnectionString = new string[]
        {
            LoginInfo.ServerName,//"10.110.0.111",//"localhost" ,
            LoginInfo.DatabaseName,//"UniMail_SPH",
            LoginInfo.UserNameLogin,//"sa",
            LoginInfo.LoginPassword//"DinCare2019"      
        };

        

        public string GrippleLabel { get; set; }

        public int _isStackerLineGroupIndex = 0;
        bool isSimulationStatusVisable = false;


        public MainWindow(byte _CurLineId)
        {
            InitializeComponent();

            uxVersion.Text = "V1.4.8 Build 2021.05.20";

            if (ConnectionString[0] == null)
            {
                ConnectionString[0] = "10.110.0.111";
                ConnectionString[1] = "UniMail_SPH";
                ConnectionString[2] = "sa";
                ConnectionString[3] = "DinCare2019";
            }

            CurLineId = _CurLineId; //Set Line Id = 1;
            LayoutDesign(CurLineId);

            _isClickedButton = false;
            _isSTDWithoutTop = false;
            _isDuringStarting = false;
            _isNoTopsheet = false;
            //_isOneLine = true;

            isStoppedABL = new bool[NUM_STACKERLINE];
            for (int i = 0; i < NUM_STACKERLINE; i++)
            {
                isStoppedABL[i] = false;
            }

            //Show time
            ShowTimer = new DispatcherTimer();
            ShowTimer.Tick += new EventHandler(ProductionStatus);
            ShowTimer.Interval = new TimeSpan(0, 0, 0, 1, 0);
            ShowTimer.Start();

            Closed += Window_Closed;

            _connectionStringServer = "Data Source=" + ConnectionString[0] +
               ";Initial Catalog=" + ConnectionString[1] +
               ";Integrated Security=False" +
               ";User ID=" + ConnectionString[2] +
               ";Password=" + ConnectionString[3] + "";

            LineIPString = new string[2];
            LineIPString[1] = "10.110.1.91";
            LineIPString[0] = "10.110.1.92";

            


            //SetSeparationStatus();
        }

        //public DataTable ComboBoxShow = new DataTable();

        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Code Quality", "IDE0068:Use recommended dispose pattern", Justification = "<Pending>")]
        //private void SetSeparationStatus()
        //{
        //    try
        //    {
        //        ComboBoxShow = SeparationListShow();
        //        uxSeparationLineInfo.ItemsSource = ComboBoxShow.DefaultView;
        //        uxSeparationLineInfo.DisplayMemberPath = "SeparationDescription";
        //    }
        //    catch
        //    {
        //    }
        //}

        private void LayoutDesign(byte curLineId)
        {
            switch (curLineId)
            {
                case 1: //Line 501 (4 Group Visable)
                    uxLineControl.Text = "UniMail Line Controller 501";
                    MainWindow1.Title = "LineController 501";
                    break;
                case 2: //Line 502 (4 Groups Visible)
                    uxLineControl.Text = "UniMail Line Controller 502";
                    MainWindow1.Title = "LineController 502";
                    break;
                case 3: //Line 201 (4 Groups Visible)
                    uxLineControl.Text = "UniMail Line Controller 201";
                    MainWindow1.Title = "LineController 201";
                    break;
                case 4: //Line 202 (4 Group Visable)
                    uxLineControl.Text = "UniMail Line Controller 202";
                    MainWindow1.Title = "LineController 202";
                    break;
                case 5: //Line 301 (4 Groups Visible)
                    uxLineControl.Text = "UniMail Line Controller 301";
                    MainWindow1.Title = "LineController 302";
                    break;
                case 6: //Line 302 (4 Groups Visible)
                    uxLineControl.Text = "UniMail Line Controller 302";
                    MainWindow1.Title = "LineController 302";
                    break;
                case 7: //Line 401 (4 Group Visable)
                    uxLineControl.Text = "UniMail Line Controller 401";
                    MainWindow1.Title = "LineController 401";
                    break;
                case 8: //Line 402 (4 Groups Visible)
                    uxLineControl.Text = "UniMail Line Controller 402";
                    MainWindow1.Title = "LineController 402";
                    break;
                default:
                    break;
            }

            Button AblGroupPauseButton;
            Button AblGroupStartStopButton;
            Button ABLPauseButton;
            TextBlock GroupABLStatusText;

            for (int i = 1; i <= NUM_STACKERLINE; i++)
            {
                object _oStackerLineGrid = MainInfo.FindName("Group" + i + "Grid");

                AblGroupStartStopButton = new Button();
                AblGroupStartStopButton = (Button)CreateABLGroupStartStopButtonControl(AblGroupStartStopButton, i - 1);
                AblGroupStartStopButton.Click += new RoutedEventHandler(GroupABLStartStop_Click);

                MainInfo.RegisterName(AblGroupStartStopButton.Name, AblGroupStartStopButton);
                MainInfo.Children.Add(AblGroupStartStopButton);

                if (_oStackerLineGrid is Grid StackerLineGrid)
                {
                    NameScope.SetNameScope(StackerLineGrid, new NameScope());

                    AblGroupPauseButton = new Button();
                    AblGroupPauseButton = (Button)CreateABLGroupButtonControl(AblGroupPauseButton, i, "ABLGroupState");
                    AblGroupPauseButton.Click += new RoutedEventHandler(GroupABLGroupState_Click);

                    StackerLineGrid.RegisterName(AblGroupPauseButton.Name, AblGroupPauseButton);
                    StackerLineGrid.Children.Add(AblGroupPauseButton);

                    //FontFamily = "Calibri" FontWeight = "Bold" 
                    //FontSize = "15" HorizontalAlignment = "Left" 
                    //Margin = "80,2,0,0" TextWrapping = "Wrap" 
                    //Text = "ABL Group A" VerticalAlignment = "Top"

                    GroupABLStatusText = new TextBlock
                    {
                        Text = "ABL Group " + stackerNo[i-1],
                        FontSize = 15,
                        FontWeight = FontWeights.Bold,
                        TextWrapping = TextWrapping.Wrap,
                        Margin = new Thickness(82, 2, 0, 0),
                        FontFamily = new FontFamily("Calibri"),
                        HorizontalAlignment = HorizontalAlignment.Left,
                        VerticalAlignment = VerticalAlignment.Top
                    };
                    StackerLineGrid.Children.Add(GroupABLStatusText);

                    for (int j = 0; j < NUM_ABL; j++)
                    {
                        ABLPauseButton = new Button();
                        ABLPauseButton = (Button)CreateABLButtonControl(ABLPauseButton, i, j, "ABLState");
                        ABLPauseButton.Click += new RoutedEventHandler(GroupABLState_Click);

                        StackerLineGrid.RegisterName(ABLPauseButton.Name, ABLPauseButton);
                        StackerLineGrid.Children.Add(ABLPauseButton);
                    }
                }
            }
        }

        private object CreateABLButtonControl(object value, int stackerLineId, int ablId, string controlName)
        {
            try
            {
                int left = 0;

                switch (ablId)
                {
                    case 0:
                        left = 171;
                        break;
                    case 1:
                        left = 102;
                        break;
                    case 2:
                        left = 33;
                        break;
                }

                //Group1ABLState0
                value = new Button
                {
                    Name = "Group" + stackerLineId + controlName + ablId,
                    FontSize = 22,
                    Width = 36,
                    Height = 40,
                    Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#585858")),
                    Margin = new Thickness(left, 25, 20, 0),
                    FontFamily = new FontFamily("Calibri"),
                    HorizontalAlignment = HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Top,
                    Style = Resources["ButtonStyle3"] as Style
                };
            }
            catch
            {
            }
            return value;
        }

        private object CreateABLGroupButtonControl(object value, int stackerLineId, string controlName)
        {
            try
            {
                //Background="#857f7f" FontFamily="Calibri" FontSize="22" HorizontalAlignment="Left"  Margin="0,0,0,0" VerticalAlignment="Top" Width="243" Height="81" Click="Group1ABLGroupState_Click
                value = new Button
                {
                    Name = "Group" + stackerLineId + controlName,
                    FontSize = 22,
                    Width = 243,
                    Height = 81,
                    Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#857f7f")),
                    Margin = new Thickness(0, 0, 0, 0),
                    FontFamily = new FontFamily("Calibri"),
                    HorizontalAlignment = HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Top,
                    Style = Resources["ButtonStyle3"] as Style
                };
            }
            catch
            {
            }
            return value;
        }

        private object CreateABLGroupStartStopButtonControl(object value, int stackerLineId)
        {
            try
            {
                int leftMargin = 834 - 268 * stackerLineId;
                //Background="#857f7f" FontFamily="Calibri" FontSize="22" HorizontalAlignment="Left"  Margin="0,0,0,0" VerticalAlignment="Top" Width="243" Height="81" Click="Group1ABLGroupState_Click
                value = new Button
                {
                    Name = "uxSeparation_" + stackerLineId,
                    Content = "Separation " + stackerNo[stackerLineId],
                    FontSize = 17,
                    Width = 243,
                    Height = 25,
                    Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#afabab")),
                    Margin = new Thickness(leftMargin, 140, 0, 0),
                    FontFamily = new FontFamily("Calibri"),
                    HorizontalAlignment = HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Top,
                    Style = Resources["ButtonStyle3"] as Style
                };
            }
            catch
            {
            }
            return value;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                //SqlDependency.Start(_connectionStringServer);
                //GetCurrentGripperLineInfo();
            }
            catch
            {
            }
        }



        public void ProductionStatus(object sender, EventArgs e)
        {
            Tt.Content = DateTime.Now.ToString("dddd", new System.Globalization.CultureInfo("en-SG"));
            Tt.Content += " ";
            Tt.Content += DateTime.Now.ToString("dd-MM-yyyy");
            Tt.Content += " ";
            Tt.Content += DateTime.Now.ToString("HH:mm");

            try
            {
                if (lineControlMain != null && _isClickedButton)
                {
                    GripperSpeed.Text = (lineControlMain.GripperSpeed / 1000 * 1000).ToString();
                    GripperCounter.Text = lineControlMain.TaktGripperCounter.ToString();
                    //BruttoCounter.Text = lineControlMain.CopiesBruttoCounter.ToString();
                    //NettoCounter.Text = lineControlMain.CopiesNettoCounter.ToString();
                    CopyCounter.Text = lineControlMain.CopiesMainCounter.ToString();
                    Overflow.Text = lineControlMain.CopiesOverflow.ToString();

                    if (lineControlMain.IsStarted)
                    {
                        UpdateMachineStatus(true); // STB, PZF, ABL, ALS Status
                        PrinterStatusChange(true);
                    }
                    
                    byte _simulationCode = lineControlMain.SimulationStatus;

                   
                    
                    if (_simulationCode == 0)
                    {
                        SimulationStatus.Visibility = Visibility.Collapsed;
                        isSimulationStatusVisable = false;
                    }
                    else if (_simulationCode == 1)
                    {
                        if (!isSimulationStatusVisable)
                        {
                            SimulationStatus.Visibility = Visibility.Visible;
                            SimulationStatus.Text = "Simulation With Continuous Copy";
                            isSimulationStatusVisable = true;
                        }
                        else
                        {
                            SimulationStatus.Visibility = Visibility.Collapsed;
                            isSimulationStatusVisable = false;
                        }
                        
                    }
                    else
                    {
                        if (!isSimulationStatusVisable)
                        {
                            SimulationStatus.Visibility = Visibility.Visible;
                            SimulationStatus.Text = "Simulation With Copy Gap";
                            isSimulationStatusVisable = true;
                        }
                        else
                        {
                            SimulationStatus.Visibility = Visibility.Collapsed;
                            isSimulationStatusVisable = false;
                        }
                       
                    }
                }
            }
            catch
            {
            }
        }

        void OnDependencyChange(object sender, SqlNotificationEventArgs e)
        {
            GetCurrentGripperLineInfo();
        }

        private void GetCurrentGripperLineInfo()
        {
            try
            {
                Dispatcher.Invoke(new Action(() =>
                {
                    SqlConnection sqlCon = new SqlConnection(_connectionStringServer);
                    SqlCommand cmd = new SqlCommand("select * from [UniMail_GetLineInfo](5, "+ CurLineId + ")", sqlCon)
                    {
                        CommandType = CommandType.Text
                    };
                    sqlCon.Open();
                    SqlDependency dep = new SqlDependency(cmd);
                    dep.OnChange += new OnChangeEventHandler(OnDependencyChange);
                    SqlDataReader reader = cmd.ExecuteReader();
                    List<string> GripperLineDes = new List<string>();

                    while (reader.Read())
                    {
                        //GripperLineDes.Add(Convert.ToDateTime(reader["IssueDate"]).ToString("dd/MM/yy").Split(' ')[0]);
                        if (reader["IssueDate"].ToString() == "")
                        {
                            GripperLineDes.Add(reader["IssueDate"].ToString());
                        }
                        else
                        {
                            GripperLineDes.Add(Convert.ToDateTime(reader["IssueDate"]).ToString("dd/MM/yy").Split(' ')[0]);
                        }

                        GripperLineDes.Add(reader["CurTitleGroup"].ToString().Replace('\r', ' ').Replace('\n', ' '));
                        GripperLineDes.Add(reader["GripperLineDescription"].ToString().Replace('\r', ' ').Replace('\n', ' '));
                    }

                    GrippleLabel = GripperLineDes[0].ToString() + "  " + GripperLineDes[1].ToString() + "  " + GripperLineDes[2].ToString();
                    reader.Close();
                    sqlCon.Close();

                    Binding ToolTipNameBind1 = new Binding { Source = this, Path = new PropertyPath("GrippleLabel") };
                    BindingOperations.SetBinding(GripperLineInfo, ContentProperty, ToolTipNameBind1);
                }));
            }
            catch
            {
            }
        }

        private void PrinterStatusChange(bool isRunning)
        {
            try
            {
                for(int i = 0; i < NUM_STACKERLINE; i++)
                {
                    for(int j = 0; j < NUM_DBH; j++)
                    {
                        object _oPrinterStateColor = MainInfo.FindName("Group"+ (i+1) + "PrinterState" + j);

                        if (_oPrinterStateColor is Button PrinterStateRec)
                        {
                            SetMachineStatusColor(lineControlMain.PrinterStatus[i][j], PrinterStateRec, 0, isRunning);
                        }
                    }
                }

                for (int i = 0; i < NUM_STACKERLINE; i++)
                {
                    for (int j = 0; j < NUM_DBH; j++)
                    {
                        PrinterState = lineControlMain.PrinterMessage[i][j]["PrinterStatus"];
                        PrintingStatus = lineControlMain.PrinterMessage[i][j]["PrintingStatus"].ToString();
                        //RibbonState = lineControlMain.PrinterMessage[i][j]["RibbonLife"].ToString();
                        StatusMessage = lineControlMain.PrinterMessage[i][j]["StatusMessage"].ToString();

                        object _oStatusMessage = MainInfo.FindName("Group" + (i+1) + "PrinterStatus" + j);
                        object _oPrintingStatus = MainInfo.FindName("Group" + (i+1) + "PrinterPage" + j);
                        //object _oRibbonLife = MainInfo.FindName("Group" + (i+1) + "PrinterToner" + j);


                        if (_oStatusMessage is Label _lStatusMessage)
                        {
                            _lStatusMessage.Content = StatusMessage;
                        }

                        if (_oPrintingStatus is Label _lPrintingStatus)
                        {
                            switch(PrintingStatus)
                            {
                                case "1":
                                    _lPrintingStatus.Content = "OTHER";
                                    break;
                                case "2":
                                    _lPrintingStatus.Content = "UNKNOWN";
                                    break;
                                case "3":
                                    _lPrintingStatus.Content = "IDLE";
                                    break;
                                case "4":
                                    _lPrintingStatus.Content = "PRINTING";
                                    break;
                                case "5":
                                    _lPrintingStatus.Content = "WARMUP";
                                    break;
                                default:
                                    _lPrintingStatus.Content = "";
                                    break;
                            }

                        }

                        //if (_oRibbonLife is Label _lRibbonLife)
                        //{
                        //    _lRibbonLife.Content = RibbonState.Trim();
                        //}
                    }
                }
            }
            catch
            {
            }
        }

        public void UpdateMachineStatus(bool isRunning)
        {
            try
            {
                #region STB Status
                for (int i = 0; i < NUM_STACKERLINE; i++)
                {
                    object _oSTBStatus = MainInfo.FindName("Group" + (i + 1) + "STBState");

                    if (_oSTBStatus is Button STBStatusRec)
                    {
                        SetMachineStatusColor(lineControlMain.STBStatus[i], STBStatusRec, 0, isRunning);
                    }
                }
                #endregion

                #region Printer Queue Status
                for (int i = 0; i < NUM_STACKERLINE; i++)
                {
                    object _oPrinterQueue= MainInfo.FindName("Group" + (i + 1) + "PrintQueueState");

                    if (_oPrinterQueue is Button PrinterQueueRec)
                    {
                        if (lineControlMain.PrinterQueueCount[i] != "")
                        {
                            if (lineControlMain.PrinterQueueCount[i] != "MAX")
                            {
                                PrinterQueueRec.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom(RunningStatusColor));
                            }
                            else
                            {
                                PrinterQueueRec.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom(PausedABLStatusColor));
                            }
                        }

                        PrinterQueueRec.Content = "Queue: " + lineControlMain.PrinterQueueCount[i];
                    }
                }
                #endregion

                #region PZF Status
                for (int i = 0; i < NUM_STACKERLINE; i++)
                {
                    object _oPZFStatus = MainInfo.FindName("Group" + (i + 1) + "PZFState");

                    if (_oPZFStatus is Button PZFStatusRec)
                    {
                        SetMachineStatusColor(lineControlMain.PZFStatus[i], PZFStatusRec, 0, isRunning);
                    }
                }

                #endregion

                #region ALS Status
                SetMachineStatusColor(lineControlMain.ALSStatus, uxALState, 0, isRunning);
                #endregion

                #region ABL Status
                for (int i = 0; i < NUM_STACKERLINE; i++)
                {
                    object _oGridStacker = MainInfo.FindName("Group" + (i + 1) + "Grid");

                    if (_oGridStacker is Grid StackerGrid)
                    {
                        object _oABLGroupStatus = StackerGrid.FindName("Group" + (i + 1) + "ABLGroupState");

                        if (_oABLGroupStatus is Button ABLGroupStatusRec)
                        {
                            SetMachineStatusColor(lineControlMain.ABLGroupState[i], ABLGroupStatusRec, 1, isRunning);
                        }

                        for (int j = 0; j < NUM_ABL; j++)
                        {
                            object _oABLStateColor = StackerGrid.FindName("Group" + (i + 1) + "ABLState" + j);

                            if (_oABLStateColor is Button ABLStateRec)
                            {
                                SetMachineStatusColor(lineControlMain.ABLState[i][j], ABLStateRec, 2, isRunning);
                            }
                        }
                    }
                }

                #endregion

                #region ABL Group Counter
                try
                {
                    for (int i = 0; i < NUM_STACKERLINE; i++)
                    {
                        object _oCounterArea = MainInfo.FindName("uxSeparation_" + i);

                        if (_oCounterArea is Button GroupCounterText)
                        {
                            GroupCounterText.Content = "Sep. " + stackerNo[i] + " : " + lineControlMain.NumCopiesCounterABLGroup[i];
                        }
                    }
                }
                catch
                {
                }

                #endregion

                #region ProductionStart

                if (lineControlMain.isAlsStatusProductionStarted)
                {
                    ProductionStarted.Fill = (SolidColorBrush)(new BrushConverter().ConvertFrom(RunningStatusColor));
                }
                else
                {
                    ProductionStarted.Fill = (SolidColorBrush)(new BrushConverter().ConvertFrom(NotRunningStatusColor));
                }

                if (lineControlMain.isAlsReceivedBundle)
                {
                    BundleReceived.Fill = (SolidColorBrush)(new BrushConverter().ConvertFrom(RunningStatusColor));
                }
                else
                {
                    BundleReceived.Fill = (SolidColorBrush)(new BrushConverter().ConvertFrom(NotRunningStatusColor));
                }

                #endregion
            }
            catch
            {
            }
        }

        private void SetMachineStatusColor(EnumMachineStatus enumMachineStatus, Button MachineStatus,byte DifferentColor,bool isLccRunning) //0: normal 1: ABL Group Running Diff Color 2: ABL Unknown Diff Color
        {
            try
            {
                if (isLccRunning)
                {
                    if (enumMachineStatus == EnumMachineStatus.Running)
                    {
                        if (DifferentColor == 1)
                        {
                            MachineStatus.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom(RunningABLGroupColor));
                        }
                        else
                        {
                            MachineStatus.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom(RunningStatusColor));
                        }
                    }
                    else if (enumMachineStatus == EnumMachineStatus.NotRunning || enumMachineStatus == EnumMachineStatus.NotReady)
                    {
                        if (DifferentColor == 2)
                        {
                            MachineStatus.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom(NotRunningABLStatusColor));

                        }
                        else
                        {
                            MachineStatus.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom(NotRunningStatusColor));
                        }
                    }
                    else if (enumMachineStatus == EnumMachineStatus.AblGroupPaused)
                    {
                        MachineStatus.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom(PausedABLGroupStatusColor));
                    }
                    else if (enumMachineStatus == EnumMachineStatus.AblPaused)
                    {
                        MachineStatus.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom(PausedABLStatusColor));
                    }
                    else
                    {
                        if (DifferentColor == 2)
                        {
                            MachineStatus.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom(UnknownABLStatusColor));
                        }
                        else
                        {
                            MachineStatus.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom(UnknownStatusColor));
                        }
                    }
                }
                else
                {
                    MachineStatus.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom(UnknownStatusColor));
                }
            }
            catch
            {
            }
        }

        public void LccStop()
        {
            try
            {
                if (lineControlMain != null)
                {
                    
                    lineControlMain.Stop();
                    
                }
            }
            catch
            {
            }
            
        }

        private async void Window_Closed(object sender, EventArgs e)
        {
            try
            {
                await Task.Run(
                    () => LccStop()
                );
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            Environment.Exit(0);
        }

        private void CloseButton1_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MessageBoxResult result = MessageBox.Show("Do you really want to Exit?", "DinCare LineController", MessageBoxButton.OKCancel, MessageBoxImage.Question);
                if (result == MessageBoxResult.OK)
                {
                    result = MessageBox.Show("Exiting without stopping production will have a lot of overflow, Continue?", "DinCare LineController", MessageBoxButton.OKCancel, MessageBoxImage.Question);

                    if (result == MessageBoxResult.OK)
                    {
                        //await Task.Run(
                        //    () => LccStop()
                        //);
                        Close();
                        Environment.Exit(0);
                    }
                }
                else
                {
                    return;
                }
            }
            catch
            {
            }
        }

        private async void LccStartButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!_isClickedButton)
                {
                    LccStartButton.Content = "ULCC Starting";
                    _isClickedButton = true;
                    _isDuringStarting = true;

                    SqlDependency.Start(_connectionStringServer);
                    GetCurrentGripperLineInfo();

                    LccStartButton.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#548235"));
                    LccStopButton.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FFCB0909"));

                    if (lineControlMain == null)
                    {
                        lineControlMain = new LineControlMain(CurLineId, LineIPString[CurLineId % 2], ConnectionString, _isSTDWithoutTop, _isNoTopsheet);
                    }

                    if (lineControlMain != null)
                    {
                        lineControlMain.EnableTopSheet = true;

                        await Task.Run(
                            () => lineControlMain.Start()
                        );

                        CurStackerLineId = lineControlMain.StackerLineId;
                        Thread.Sleep(1000);

                        #region Separation Status

                        //_isOneLine = lineControlMain.isABLSameLine;
                        //SetSeparationStatus();
                        //uxSeparationLineInfo.SelectedIndex = -1;

                        #endregion

                        LccStartButton.Content = "ULCC Started";
                        _isDuringStarting = false;
                    }
                }
                else
                {
                    MessageBox.Show("ULCC Already Started!");
                }
            }
            catch
            {
            }
        }

        private async void LccStopButton_Click(object sender, RoutedEventArgs e)
        {
            if (_isClickedButton)
            {
                MessageBoxResult result = MessageBox.Show("UniMail Line Controller is going to Stop and Exit.", "Warning", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
                if (result == MessageBoxResult.OK)
                {
                    if (lineControlMain != null)
                    {
                        UpdateMachineStatus(false);
                        PrinterStatusChange(false);

                        for (int i = 0; i < 4; i++) // When Stop LCC, Make All Paused ABL resume //Cheng 20210520
                        {
                            if (lineControlMain.isPausedStackerLine[i])
                            {
                                lineControlMain.RequestResumeStacker((byte)(i+1));
                            }
                        }

                        await Task.Run(
                            () => LccStop()
                        );
                    }
                    Close();
                    Environment.Exit(0);

                }
                else
                {
                    return;
                }
            }
            else
            {
                Close();
                Environment.Exit(0);
            }
        }

        //private void LccStopButton_Click(object sender, RoutedEventArgs e)
        //{
        //    try
        //    {
        //        if (_isClickedButton)
        //        {
        //            _isClickedButton = false;
        //            if (lineControlMain != null)
        //            {
        //                LccStop();
        //            }
        //            //LCCSwitchCheckbox.IsChecked = false;
        //        }
        //    }
        //    catch
        //    {
        //    }
        //}

        //private void ProgramableRadioButton_Checked(object sender, RoutedEventArgs e)
        //{
        //    try
        //    {
        //        if (lineControlMain != null && lineControlMain.CurrentMode == 0)
        //        {
        //            CurrentMode = 1;
        //            lineControlMain.CurrentMode = CurrentMode;
        //            ChangeCurrentMode(CurrentMode,CurLineId);
        //        }
        //    }
        //    catch
        //    {
        //    }
        //}

        //private void StandardRadioButton_Checked(object sender, RoutedEventArgs e)
        //{
        //    try
        //    {
        //        if (lineControlMain != null && lineControlMain.CurrentMode == 1)
        //        {
        //            CurrentMode = 0;
        //            lineControlMain.CurrentMode = CurrentMode;
        //            ChangeCurrentMode(CurrentMode,CurLineId);
        //        }
        //    }
        //    catch
        //    {
        //    }

        //}

        private void BundleSetting_Click(object sender, RoutedEventArgs e)
        {
            BundleSettings bundleSetting = new BundleSettings(lineControlMain);
            try
            {
                bundleSetting.ShowDialog();
            }
            catch
            {
            }
        }

        private void Maintenance_Click(object sender, RoutedEventArgs e)
        {
            Maintenance MaintenancePage = new Maintenance();
            try
            {
                MaintenancePage._lineControlMain = lineControlMain;
                MaintenancePage.ShowDialog();
            }
            catch
            {
            }
        }

        private void ClearCounter_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MessageBoxResult result = MessageBox.Show("Do you want to clear the count??", "DinCare LineController", MessageBoxButton.OKCancel, MessageBoxImage.Question);
                if (result == MessageBoxResult.OK)
                {
                    MessageBoxResult result2 = MessageBox.Show("Not Allow ClearCounter During Production, Continue?", "DinCare LineController", MessageBoxButton.OKCancel, MessageBoxImage.Question);
                    if (result == MessageBoxResult.OK)
                    {
                        CmdToLCCClearCounters(CurLineId);
                    }
                    else
                    {
                        return;
                    }
                    
                }
                else
                {
                    return;
                }
            }
            catch
            {
            }
        }

        public void CmdToLCCClearCounters(byte GripperLineTableId)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionStringServer))
                {
                    SqlCommand command = new SqlCommand("[dbo].[UniStack_CmdToLCCClearCounters]", connection)
                    {
                        CommandText = "[UniStack_CmdToLCCClearCounters]",
                        CommandType = CommandType.StoredProcedure
                    };
                    command.Parameters.Add(new SqlParameter("@GripperLineTableId", GripperLineTableId));
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    reader.Close();
                    connection.Close();
                }
            }
            catch
            {
            }
        }

        private void MinButton_Click(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        //private void UxSeparationLineInfo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    try
        //    {
        //        //if (lineControlMain != null && 
        //        //    lineControlMain.IcpConnectionState == LineControlMain.EnumIcpConnectionStates.Connected)
        //        //{
        //        //    if (lineControlMain.noneABLOnThisLine)
        //        //    {
        //        //        if (uxSeparationLineInfo.SelectedIndex != -1)
        //        //        {
        //        //            uxSeparationLineInfo.SelectedIndex = -1;
        //        //            MessageBoxResult result = MessageBox.Show("No ABL On This Line, Please Check ABL Position!!", "DinCare LineController", MessageBoxButton.OK, MessageBoxImage.Warning);
        //        //            return;
        //        //        }
        //        //    }
        //        //    else
        //        //    {
        //                //object obj = e.AddedItems;
        //                //_isStackerLineGroupIndex = int.Parse(((DataRowView)(((object[])(obj))[0])).Row.ItemArray[0].ToString());
        //                //string _separationDescription = ((DataRowView)(((object[])(obj))[0])).Row.ItemArray[1].ToString();
        //                //SeparationStatusInitial();
        //                //SetStackerLineGroupLayout(_isStackerLineGroupIndex);
        //                //SeparationStatusSave(CurLineId, _separationDescription);
        //        //    }
        //        //}
        //        //else
        //        //{
        //        //    if (uxSeparationLineInfo.SelectedIndex != -1)
        //        //    {
        //        //        uxSeparationLineInfo.SelectedIndex = -1;
        //        //        MessageBoxResult result = MessageBox.Show("Please Start LCC First!!", "DinCare LineController", MessageBoxButton.OK, MessageBoxImage.Warning);
        //        //        return;
        //        //    }

        //        //}

        //    }
        //    catch
        //    {
        //    }
        //}

        ////private void SeparationStatusInitial()
        //{
        //    try
        //    {
        //        for (int i = 0; i < NUM_STACKERLINE; i++)
        //        {
        //            object _oStatusMessage = MainInfo.FindName("uxSeparation" + i);

        //            if (_oStatusMessage is Button SeparationButton)
        //            {
        //                SeparationButton.Visibility = Visibility.Visible;
        //                SeparationButton.Width = 243;
        //                SeparationButton.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#afabab"));
        //            }

        //        }

        //        uxSeparation0.Margin = new Thickness(834, 140, 0, 0);
        //        uxSeparation1.Margin = new Thickness(566, 140, 0, 0);
        //        uxSeparation2.Margin = new Thickness(298, 140, 0, 0);
        //        uxSeparation3.Margin = new Thickness(30, 140, 0, 0);
        //    }
        //    catch
        //    {
        //    }
        //}

        //private void SetStackerLineGroupLayout(int isStackerLineGroupIndex)
        //{
        //    try
        //    {
        //        //if (_isOneLine)
        //        {
        //            switch (isStackerLineGroupIndex)
        //            {
        //                case 0: //Line 501 (4 Group Visable)
        //                    uxSeparation0.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#548235"));
        //                    uxSeparation1.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#548235"));
        //                    uxSeparation2.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#548235"));
        //                    uxSeparation3.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#548235"));

        //                    break;
        //                case 1: //Line 501 (4 Group Visable)
        //                    uxSeparation0.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#548235"));
        //                    uxSeparation0.Width = 1047;
        //                    uxSeparation0.Margin = new Thickness(30, 140, 0, 0);
        //                    uxSeparation1.Visibility = Visibility.Collapsed;
        //                    uxSeparation2.Visibility = Visibility.Collapsed;
        //                    uxSeparation3.Visibility = Visibility.Collapsed;

        //                    break;
        //                case 2: //Line 501 (4 Group Visable)
        //                    uxSeparation0.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#548235"));
        //                    uxSeparation0.Width = 511;
        //                    uxSeparation0.Margin = new Thickness(566, 140, 0, 0);
        //                    uxSeparation1.Visibility = Visibility.Collapsed;

        //                    uxSeparation2.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#548235"));
        //                    uxSeparation2.Width = 511;
        //                    uxSeparation2.Margin = new Thickness(30, 140, 0, 0);
        //                    uxSeparation3.Visibility = Visibility.Collapsed;
        //                    break;
        //                case 3: //Line 501 (4 Group Visable)
        //                    uxSeparation0.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#548235"));
        //                    uxSeparation0.Width = 511;
        //                    uxSeparation0.Margin = new Thickness(566, 140, 0, 0);
        //                    uxSeparation1.Visibility = Visibility.Collapsed;
        //                    uxSeparation2.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#548235"));
        //                    uxSeparation3.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#548235"));

        //                    break;
        //                case 4: //Line 501 (4 Group Visable)
        //                    uxSeparation0.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#548235"));
        //                    uxSeparation1.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#548235"));
        //                    uxSeparation2.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#548235"));
        //                    uxSeparation2.Width = 511;
        //                    uxSeparation2.Margin = new Thickness(30, 140, 0, 0);
        //                    uxSeparation3.Visibility = Visibility.Collapsed;


        //                    break;
        //                case 5: //Line 501 (4 Group Visable)
        //                    uxSeparation0.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#548235"));
        //                    uxSeparation1.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#548235"));
        //                    uxSeparation1.Width = 779;
        //                    uxSeparation1.Margin = new Thickness(30, 140, 0, 0);
        //                    uxSeparation2.Visibility = Visibility.Collapsed;
        //                    uxSeparation3.Visibility = Visibility.Collapsed;


        //                    break;
        //                case 6: //Line 501 (4 Group Visable)
        //                    uxSeparation3.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#548235"));
        //                    uxSeparation0.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#548235"));
        //                    uxSeparation0.Width = 779;
        //                    uxSeparation0.Margin = new Thickness(298, 140, 0, 0);
        //                    uxSeparation1.Visibility = Visibility.Collapsed;
        //                    uxSeparation2.Visibility = Visibility.Collapsed;
        //                    break;
        //                default:
        //                    break;

        //            }
        //        }
        //        else if (!_isOneLine && CurLineId % 2 == 0)
        //        {
        //            switch (isStackerLineGroupIndex)
        //            {
        //                case 0: //Line 502 (P2211)
        //                    uxSeparation0.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#548235"));
        //                    uxSeparation1.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#548235"));
        //                    uxSeparation2.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#afabab"));
        //                    uxSeparation3.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#afabab"));

        //                    break;
        //                case 1: //Line 502 (P2_1_)
        //                    uxSeparation0.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#548235"));
        //                    uxSeparation0.Width = 511;
        //                    uxSeparation0.Margin = new Thickness(566, 140, 0, 0);
        //                    uxSeparation1.Visibility = Visibility.Collapsed;
        //                    uxSeparation2.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#afabab"));
        //                    uxSeparation2.Width = 511;
        //                    uxSeparation2.Margin = new Thickness(30, 140, 0, 0);
        //                    uxSeparation3.Visibility = Visibility.Collapsed;
        //                    break;
        //                case 2: //Line 502 (P2_11)
        //                    uxSeparation0.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#548235"));
        //                    uxSeparation0.Width = 511;
        //                    uxSeparation0.Margin = new Thickness(566, 140, 0, 0);
        //                    uxSeparation1.Visibility = Visibility.Collapsed;
        //                    uxSeparation2.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#afabab"));
        //                    uxSeparation3.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#afabab"));
        //                    break;
        //                case 3: //Line 502 (P221_)
        //                    uxSeparation0.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#548235"));
        //                    uxSeparation1.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#548235"));
        //                    uxSeparation2.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#afabab"));
        //                    uxSeparation2.Width = 511;
        //                    uxSeparation2.Margin = new Thickness(30, 140, 0, 0);
        //                    uxSeparation3.Visibility = Visibility.Collapsed;
        //                    break;

        //            }
        //        }
        //        else if (!_isOneLine && CurLineId % 2 != 0)
        //        {
        //            switch (isStackerLineGroupIndex)
        //            {
        //                case 0: //Line 501 (P2211)
        //                    uxSeparation2.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#548235"));
        //                    uxSeparation3.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#548235"));
        //                    uxSeparation0.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#afabab"));
        //                    uxSeparation1.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#afabab"));
        //                    break;
        //                case 1: //Line 501 (P2_1_)
        //                    uxSeparation2.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#548235"));
        //                    uxSeparation2.Width = 511;
        //                    uxSeparation2.Margin = new Thickness(30, 140, 0, 0);
        //                    uxSeparation3.Visibility = Visibility.Collapsed;

        //                    uxSeparation0.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#afabab"));
        //                    uxSeparation0.Width = 511;
        //                    uxSeparation0.Margin = new Thickness(566, 140, 0, 0);
        //                    uxSeparation1.Visibility = Visibility.Collapsed;
        //                    break;
        //                case 2: //Line 501 (P2_11)
        //                    uxSeparation0.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#afabab"));
        //                    uxSeparation0.Width = 511;
        //                    uxSeparation0.Margin = new Thickness(566, 140, 0, 0);
        //                    uxSeparation1.Visibility = Visibility.Collapsed;
        //                    uxSeparation2.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#548235"));
        //                    uxSeparation3.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#548235"));
        //                    break;
        //                case 3: //Line 501 (P2211)
        //                    uxSeparation2.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#548235"));
        //                    uxSeparation2.Width = 511;
        //                    uxSeparation2.Margin = new Thickness(30, 140, 0, 0);
        //                    uxSeparation3.Visibility = Visibility.Collapsed;

        //                    uxSeparation0.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#afabab"));
        //                    uxSeparation1.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#afabab"));
        //                    break;
        //            }
        //        }
        //    }
        //    catch
        //    {
        //    }
        //}

        //private void SeparationStatusSave(byte curLineId, string separationDescription)
        //{
        //    try
        //    {
        //        using (SqlConnection connection = new SqlConnection(_connectionStringServer))
        //        {
        //            SqlCommand command = new SqlCommand("[dbo].[SaveProductionLineInfo]", connection)
        //            {
        //                CommandText = "[SaveProductionLineInfo]",
        //                CommandType = CommandType.StoredProcedure
        //            };
        //            command.Parameters.Add(new SqlParameter("@LineId", curLineId));
        //            command.Parameters.Add(new SqlParameter("@SeparationDescription", separationDescription));

        //            connection.Open();
        //            SqlDataReader reader = command.ExecuteReader();
        //            reader.Close();
        //            connection.Close();
        //        }
        //    }
        //    catch
        //    {
        //    }
        //}

        //public DataTable SeparationListShow()
        //{
        //    DataTable SeparationTable = new DataTable();

        //    int oneLine = _isOneLine ? 1 : 0;

        //    string GetSeparationInfoQuery = "select * from UniLoad_GetSeparationList("+CurLineId+","+ oneLine + ")";
        //    try
        //    {
        //        using (SqlConnection conn = new SqlConnection(_connectionStringServer))
        //        {
        //            conn.Open();
        //            if (conn.State == ConnectionState.Open)
        //            {
        //                using (SqlCommand cmd = conn.CreateCommand())
        //                {
        //                    SqlDataAdapter da = new SqlDataAdapter(GetSeparationInfoQuery, conn);
        //                    da.Fill(SeparationTable);
        //                }
        //            }
        //            conn.Close();
        //        }
        //        return SeparationTable;
        //    }
        //    catch
        //    {
        //    }
        //    return SeparationTable;
        //}

        private void ALSSetting_Click(object sender, RoutedEventArgs e)
        {
            ALSSetting ALSSettingPage = new ALSSetting(CurLineId, lineControlMain);
            try
            {
                ALSSettingPage.ShowDialog();
            }
            catch
            {
            }
        }

        private void STBWithoutTop_Checked(object sender, RoutedEventArgs e)
        {
            
            try
            {
                if (_isNoTopsheet)
                {
                    MessageBox.Show("All Bundle No Topsheet Already Checked. Do Not Need This Function!");
                    STBWithoutTop.IsChecked = false;
                }
                else
                {
                    if (SetBundleTopsheetStatus(3))
                    {
                        _isSTDWithoutTop = true;
                        BundleWithoutTop.IsEnabled = false;
                    }
                    else
                    {
                        MessageBox.Show("Datebase Connect Error, Setup failed!");
                    }

                }
            }
            catch
            {
            }
        }

        private void STBWithoutTop_Unchecked(object sender, RoutedEventArgs e)
        {
            //_isSTDWithoutTop = false;
            //BundleWithoutTop.IsEnabled = true;

            //SetBundleTopsheetStatus(4);

            if (SetBundleTopsheetStatus(4))
            {
                _isSTDWithoutTop = false;
                BundleWithoutTop.IsEnabled = true;
            }
            else
            {
                MessageBox.Show("Datebase Connect Error, Setup failed!");
            }

        }

        private void BundleWithoutTop_Checked(object sender, RoutedEventArgs e)
        {
            //_isNoTopsheet = true;
            //STBWithoutTop.IsEnabled = false;

            //SetBundleTopsheetStatus(1);

            if (SetBundleTopsheetStatus(1))
            {
                _isNoTopsheet = true;
                STBWithoutTop.IsEnabled = false;
            }
            else
            {
                MessageBox.Show("Datebase Connect Error, Setup failed!");
            }

        }

        private void BundleWithoutTop_Unchecked(object sender, RoutedEventArgs e)
        {
            //_isNoTopsheet = false;
            //STBWithoutTop.IsEnabled = true;

            //SetBundleTopsheetStatus(2);

            if (SetBundleTopsheetStatus(2))
            {
                _isNoTopsheet = false;
                STBWithoutTop.IsEnabled = true;
            }
            else
            {
                MessageBox.Show("Datebase Connect Error, Setup failed!");
            }

        }


        public bool SetBundleTopsheetStatus(int ModeId)
        {
            bool result = false;
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionStringServer))
                {
                    SqlCommand command = new SqlCommand("[dbo].[UniMail_SetBundleNoTopsheet]", connection)
                    {
                        CommandText = "[UniMail_SetBundleNoTopsheet]",
                        CommandType = CommandType.StoredProcedure
                    };
                    command.Parameters.Add(new SqlParameter("@LineId", CurLineId));
                    command.Parameters.Add(new SqlParameter("@ModeCode", ModeId));

                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    reader.Close();
                    connection.Close();
                    result = true;
                }
            }
            catch
            {
                result = false;
            }
            return result;
        }

        //private void ABLGroupSmulator_Checked(object sender, RoutedEventArgs e)
        //{
        //    _isOneLine = true;
        //    SetStackerLineStatus();
        //    uxSeparationLineInfo.SelectedIndex = -1;

        //}

        //private void ABLGroupSmulator_Unchecked(object sender, RoutedEventArgs e)
        //{
        //    _isOneLine = false;
        //    SetStackerLineStatus();
        //    uxSeparationLineInfo.SelectedIndex = -1;

        //}

        #region Lock Function 

        //private void UxSeparation0_Click(object sender, RoutedEventArgs e)
        //{
        //    try
        //    {
        //        MessageBoxResult result = MessageBox.Show("Separation A Pause?", "DinCare LineController", MessageBoxButton.OKCancel, MessageBoxImage.Question);
        //        if (result == MessageBoxResult.OK)
        //        {
        //            return;
        //        }
        //        else
        //        {
        //            return;
        //        }
        //    }
        //    catch
        //    {
        //    }
        //}

        //private void UxSeparation1_Click(object sender, RoutedEventArgs e)
        //{
        //    try
        //    {
        //        MessageBoxResult result = MessageBox.Show("Separation B Pause?", "DinCare LineController", MessageBoxButton.OKCancel, MessageBoxImage.Question);
        //        if (result == MessageBoxResult.OK)
        //        {
        //            return;
        //        }
        //        else
        //        {
        //            return;
        //        }
        //    }
        //    catch
        //    {
        //    }
        //}

        //private void UxSeparation2_Click(object sender, RoutedEventArgs e)
        //{
        //    try
        //    {
        //        MessageBoxResult result = MessageBox.Show("Separation C Pause?", "DinCare LineController", MessageBoxButton.OKCancel, MessageBoxImage.Question);
        //        if (result == MessageBoxResult.OK)
        //        {
        //            return;
        //        }
        //        else
        //        {
        //            return;
        //        }
        //    }
        //    catch
        //    {
        //    }
        //}

        //private void UxSeparation3_Click(object sender, RoutedEventArgs e)
        //{
        //    try
        //    {
        //        MessageBoxResult result = MessageBox.Show("Separation D Pause?", "DinCare LineController", MessageBoxButton.OKCancel, MessageBoxImage.Question);
        //        if (result == MessageBoxResult.OK)
        //        {
        //            return;
        //        }
        //        else
        //        {
        //            return;
        //        }
        //    }
        //    catch
        //    {
        //    }
        //}

        //private void UxALState_Click(object sender, RoutedEventArgs e)
        //{
        //    try
        //    {
        //        MessageBoxResult result = MessageBox.Show("Module Pause?", "DinCare LineController", MessageBoxButton.OKCancel, MessageBoxImage.Question);
        //        if (result == MessageBoxResult.OK)
        //        {
        //            return;
        //        }
        //        else
        //        {
        //            return;
        //        }
        //    }
        //    catch
        //    {
        //    }
        //}
        #endregion

        private void GroupABLGroupState_Click(object sender, RoutedEventArgs e)
        {
            FrameworkElement source = e.OriginalSource as FrameworkElement;
            //Group1ABLGroupState
            int stackerLineId = int.Parse(source.Name.Substring(5, 1));

            try
            {
                if (lineControlMain != null)
                {
                    if (isStoppedABL[stackerLineId - 1])
                    {
                        MessageBox.Show("Separation Stopped, Please Start First.");
                    }
                    else
                    {
                        if (!lineControlMain.isPausedStackerLine[stackerLineId - 1])
                        {
                            MessageBoxResult result = MessageBox.Show("Group " + stackerLineId + " Pause?", "DinCare LineController", MessageBoxButton.OKCancel, MessageBoxImage.Question);
                            if (result == MessageBoxResult.OK)
                            {
                                if (lineControlMain != null)
                                {
                                    lineControlMain.RequestPauseStacker((byte)(stackerLineId));
                                }
                                else
                                {
                                    MessageBox.Show("LCC Not Running.");
                                }
                                return;
                            }
                            else
                            {
                                return;
                            }
                        }
                        else
                        {
                            MessageBoxResult result = MessageBox.Show("Group " + stackerLineId + " Resume?", "DinCare LineController", MessageBoxButton.OKCancel, MessageBoxImage.Question);
                            if (result == MessageBoxResult.OK)
                            {
                                if (lineControlMain != null)
                                {
                                    lineControlMain.RequestResumeStacker((byte)(stackerLineId));
                                }
                                else
                                {
                                    MessageBox.Show("LCC Not Running.");
                                }
                                return;
                            }
                            else
                            {
                                return;
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show("LCC Not Running.");
                }
                return;
                
            }
            catch
            {
            }
        }

        private void GroupABLState_Click(object sender, RoutedEventArgs e)
        {
            FrameworkElement source = e.OriginalSource as FrameworkElement;
            //Group1ABLState1
            int stackerLineId = int.Parse(source.Name.Substring(5, 1));
            int ablId = int.Parse(source.Name.Substring(source.Name.Length - 1, 1));

            try
            {
                if (lineControlMain != null)
                {
                    if (isStoppedABL[stackerLineId - 1])
                    {
                        MessageBox.Show("Separation Stopped, Please Start First.");
                    }
                    else
                    {
                        if (!lineControlMain.isPausedStackerLineABL[stackerLineId - 1][ablId])
                        {
                            MessageBoxResult result = MessageBox.Show("Group " + stackerLineId + " ABL " + (ablId + 1) + " Pause?", "DinCare LineController", MessageBoxButton.OKCancel, MessageBoxImage.Question);
                            if (result == MessageBoxResult.OK)
                            {
                                if (lineControlMain != null)
                                {
                                    lineControlMain.RequestPauseOneStacker((byte)(stackerLineId), (byte)(ablId + 1));
                                }
                                else
                                {
                                    MessageBox.Show("LCC Not Running.");
                                }
                                return;
                            }
                            else
                            {
                                return;
                            }
                        }
                        else
                        {
                            MessageBoxResult result = MessageBox.Show("Group " + stackerLineId + " ABL " + (ablId + 1) + " Resume?", "DinCare LineController", MessageBoxButton.OKCancel, MessageBoxImage.Question);
                            if (result == MessageBoxResult.OK)
                            {
                                if (lineControlMain != null)
                                {
                                    lineControlMain.RequestResumeOneStacker((byte)(stackerLineId), (byte)(ablId + 1));
                                }
                                else
                                {
                                    MessageBox.Show("LCC Not Running.");
                                }
                                return;
                            }
                            else
                            {
                                return;
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show("LCC Not Running.");
                }
                return;
            }
            catch
            {
            }
        }

        private void GroupABLStartStop_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                FrameworkElement source = e.OriginalSource as FrameworkElement;
                //Group1ABLGroupState
                int stackerLineId = int.Parse(source.Name.Split('_')[1]);

                if (lineControlMain != null)
                {
                    if (isStoppedABL[stackerLineId]) //Stopped
                    {
                        MessageBoxResult result = MessageBox.Show("Group " + stackerNo[stackerLineId] + " Start Production?", "DinCare LineController", MessageBoxButton.OKCancel, MessageBoxImage.Question);
                        if (result == MessageBoxResult.OK)
                        {
                            //if (lineControlMain != null)//(lineControlMain.RequestStartStacker((stackerLineId + 1)))
                            //{
                                isStoppedABL[stackerLineId] = false;
                            if (stackerLineId == 0)
                            {
                                GroupAStartStop.Content = "";
                            }
                            else if (stackerLineId == 1)
                            {
                                GroupBStartStop.Content = "";

                            }
                            else if (stackerLineId == 2)
                            {
                                GroupCStartStop.Content = "";

                            }
                            else if (stackerLineId == 3)
                            {
                                GroupDStartStop.Content = "";

                            }

                            lineControlMain.RequestStartStacker((stackerLineId + 1));
                            //}
                            //else
                            //{
                            //    MessageBox.Show("LCC Not Running!");
                            //}
                            
                            return;
                        }
                        else
                        {
                            return;
                        }
                    }
                    else
                    {
                        MessageBoxResult result = MessageBox.Show("Group " + stackerNo[stackerLineId] + " Stop Production?", "DinCare LineController", MessageBoxButton.OKCancel, MessageBoxImage.Question);
                        if (result == MessageBoxResult.OK)
                        {
                            //if (lineControlMain != null)//.RequestStopStacker((stackerLineId + 1)))
                            //{
                            isStoppedABL[stackerLineId] = true;

                            if (stackerLineId == 0)
                            {
                                GroupAStartStop.Content = "ABL Group A Stopped!";
                            }
                            else if (stackerLineId == 1)
                            {
                                GroupBStartStop.Content = "ABL Group B Stopped!";

                            }
                            else if (stackerLineId == 2)
                            {
                                GroupCStartStop.Content = "ABL Group C Stopped!";

                            }
                            else if (stackerLineId == 3)
                            {
                                GroupDStartStop.Content = "ABL Group D Stopped!";

                            }

                            lineControlMain.RequestStopStacker((stackerLineId + 1));
                            //}
                            //else
                            //{
                            //    MessageBox.Show("LCC Not Running!");
                            //}
                            
                            return;
                        }
                        else
                        {
                            return;
                        }
                    }
                }
                else
                {
                    MessageBox.Show("LCC Not Running.");
                }
                return;
            }
            catch
            {
            }
        }

        
    }
}
