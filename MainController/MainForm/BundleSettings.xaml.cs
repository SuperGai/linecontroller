﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Windows;
using System.Windows.Input;
using LineControllerLib;

namespace MainForm
{
    /// <summary>
    /// Interaction logic for BundleSettings.xaml
    /// </summary>
    public partial class BundleSettings : Window
    {
        public string conn = MainWindow._connectionStringServer;
        private LineControlMain lineControlMain;

        public BundleSettings(LineControlMain _line)
        {
            InitializeComponent();
            lineControlMain = _line;

            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            BundleSettingShow(MainWindow.CurLineId);
        }

        private void BundleSettingShow(byte curLineId)
        {
            SqlConnection sqlCon = new SqlConnection(conn);
            
            try
            {
                SqlCommand cmd = new SqlCommand("select * from dbo.UniMail_LCCGetStandardBundleSize(" + MainWindow.CurLineId + ")", sqlCon)
                {
                    CommandType = CommandType.Text
                };
                sqlCon.Open();
                SqlDependency dep = new SqlDependency(cmd);
                SqlDataReader reader = cmd.ExecuteReader();
                List<string> BundleSizeInfo = new List<string>();

                while (reader.Read())
                {
                    BundleSizeInfo.Add(reader["StandardPaperPageCount"].ToString());
                    BundleSizeInfo.Add(reader["StandardCopiesInBundle"].ToString());
                    BundleSizeInfo.Add(reader["StandardLayersInBundle"].ToString());
                    BundleSizeInfo.Add(reader["StandardCopiesPrGripper"].ToString());
                    BundleSizeInfo.Add(reader["CopyLimitToPress"].ToString());
                    BundleSizeInfo.Add(reader["EdgeSupportPulse"].ToString());
                }

                if (BundleSizeInfo.Count != 0)
                {
                    NumberOfPagesText.Text = BundleSizeInfo[0];
                    StandardBundleText.Text = BundleSizeInfo[1];
                    StandardLayersText.Text = BundleSizeInfo[2];
                    CopiePerGripper.Text = BundleSizeInfo[3];
                    CopiesToPressText.Text = BundleSizeInfo[4];
                    EdgeSupportPulseText.Text = BundleSizeInfo[5];
                }

                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Line " + curLineId + " Bundle Show Error: " + ex.ToString());
            }
            finally
            {
                sqlCon.Close();
            }
        }


        private void Drag_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                if (e.LeftButton == MouseButtonState.Pressed)
                {
                    DragMove();
                }
            }
            catch { }
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //MessageBoxResult result = MessageBox.Show("Do you really want to Exit?", "DinCare LineController", MessageBoxButton.OKCancel, MessageBoxImage.Question);
                //if (result == MessageBoxResult.OK)
                //{
                Close();
                //}
                //else
                //{
                //    return;
                //}
            }
            catch
            {
            }
        }

        private void ButtonOk_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(conn))
                {
                    SqlCommand command = new SqlCommand("[dbo].[UniMail_LCCSetStandardBundleSize]",connection)
                    {
                        CommandText = "[UniMail_LCCSetStandardBundleSize]",
                        CommandType = CommandType.StoredProcedure
                    };
                    command.Parameters.Add(new SqlParameter("@LineId", MainWindow.CurLineId));
                    command.Parameters.Add(new SqlParameter("@Pages", NumberOfPagesText.Text));
                    command.Parameters.Add(new SqlParameter("@StandardBundle", StandardBundleText.Text));
                    command.Parameters.Add(new SqlParameter("@MaxBundle", StandardBundleText.Text));
                    command.Parameters.Add(new SqlParameter("@NumLayers", StandardLayersText.Text));
                    command.Parameters.Add(new SqlParameter("@CopiesPerGripper", CopiePerGripper.Text));
                    command.Parameters.Add(new SqlParameter("@CopiesToPress", CopiesToPressText.Text));
                    command.Parameters.Add(new SqlParameter("@EdgeSupport", EdgeSupportPulseText.Text));

                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    reader.Close();
                }

                if (lineControlMain != null)
                {
                    lineControlMain.NumberofPages = int.Parse(NumberOfPagesText.Text);
                    lineControlMain.NumberofStandardLayer = int.Parse(StandardLayersText.Text);
                    lineControlMain.StandardBundleSize = int.Parse(StandardBundleText.Text);
                    lineControlMain.CopyToPressLimit = int.Parse(CopiesToPressText.Text);
                    lineControlMain.CopiePerGripper = int.Parse(CopiePerGripper.Text);
                    lineControlMain.EdgeSupportPulseValue = int.Parse(EdgeSupportPulseText.Text);
                    lineControlMain.IcpGetOrAddTelegramStacker(LineControlMain.EnumIcpTelegramTypes.SendStandardBundleSize, null, true);

                    BundleSettingShow(MainWindow.CurLineId);

                    MessageBoxResult result = MessageBox.Show("Standard BundleSize Updated", "DinCare LineController", MessageBoxButton.OK, MessageBoxImage.Information);
                    if (result == MessageBoxResult.OK)
                    {
                        Close();
                    }
                }
                else
                {
                    MessageBox.Show("Please Start LCC First!");
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Bundle Size Setting Error: " + ex.ToString());
            }
        }
    }
}
